import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";

export default function LoadingSpiner() {
  return (
    <div>
      <Skeleton variant="text" />
      <Skeleton variant="circle" width={40} height={40} />
      <Skeleton variant="rect" height={100}/>
    </div>
  );
}

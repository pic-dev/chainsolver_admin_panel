import React, { Component,useState,useEffect } from "react";
import ReactPlayer from "react-player/youtube";

export default function VideoInicio(props) {
 
  return (
    <ReactPlayer
      url="https://www.youtube.com/watch?v=qFP1j86IiOk"
      className="react-player mx-auto video-youtube"
      key="presentación"
      width="100%"
      height="100%"
      config={{
        youtube: {
          playerVars: {
            autoplay: "0",
            origin: `${window.location.hostname}`,
          },
        },
      }}
    />
  );
}

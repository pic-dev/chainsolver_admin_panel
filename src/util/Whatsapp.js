import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Whatsapp extends Component {
	constructor(props) {
		super();

		this.toggle = this.toggle.bind(this);
		this.changeThemeColor = this.changeThemeColor.bind(this);
		this.addEvents = this.addEvents.bind(this);
		this.removeEvents = this.removeEvents.bind(this);
		this.handleDocumentClick = this.handleDocumentClick.bind(this);
		this.getContainer = this.getContainer.bind(this);

		this.state = {
			isOpen: false,
			selectedColor:localStorage.getItem('themeColor')
		};
	}

	getContainer() {
		return ReactDOM.findDOMNode(this);
	}

	toggle(e) {
		e.preventDefault();
		const isOpen = this.state.isOpen;
		if (!isOpen) {
			this.addEvents();
		} else {
			this.removeEvents();
		}
		this.setState({
			isOpen: !isOpen
		})
	}
	changeThemeColor(e, color) {
		e.preventDefault();
		localStorage.setItem('themeColor', color)
		this.toggle(e);
		setTimeout(()=>{
			window.location.reload();
		},500)
	}

	componentWillMount() {
		this.removeEvents();
	}


	addEvents() {
		['click', 'touchstart'].forEach(event =>
			document.addEventListener(event, this.handleDocumentClick, true)
		);
	}
	removeEvents() {
		['click', 'touchstart'].forEach(event =>
			document.removeEventListener(event, this.handleDocumentClick, true)
		);
	}

	handleDocumentClick(e) {
		const container = this.getContainer();
		if ((container.contains(e.target) || container === e.target)) {
			return;
		}
		this.toggle(e);
	}

	render() {
		
		return (
			<div>
	
				<a href={this.props.pag == "importadores" ? "https://wa.link/xbr57j" : 
                            "https://api.whatsapp.com/send?phone=51920301745" +
                            "&text=Saludos,%20me%20gustar%C3%ADa%20cotizar%20ID:" +
                            localStorage.token +
                            ",%20%20Tipo Flete:%20" +
                            localStorage.transporte +
                            ",%20%20%20Tipo Conte:%20" +
                            localStorage.tipocon +
                            ",%20%20%20Flete Origen:%20" +
                            localStorage.puerto +
                            ",%20%20%20Flete Destino:%20" +
                            localStorage.puertoD +
                            ",%20%20%20Contenido Flete:%20" +
                            localStorage.contenedoresDes +
                            ",%20%20%20Valor Flete:%20" +
                            this.props.subtotalResumen +
                            ",%20%20%20%20%20gastos:%20" +
                            this.props.gastos +
                            ",%20%20%20%20%20Impuestos:%20" +
                            this.props.Impuestos +
                            ",%20%20%20%20%20Transporte:%20" +
                            this.props.Transporte +
                            ",%20%20%20%20%20Seguro:%20" +
							this.props.Seguro +
							",%20%20%20%20%20ImpuestoBajo:%20" +
							this.props.taxesResumen[0] +
							",%20%20%20%20%20ImpuestoMedio:%20" +
							this.props.taxesResumen[2] +
							",%20%20%20%20%20ImpuestoAlto:%20" +
                            this.props.taxesResumen[1]
                          }  rel="noopener noreferrer" target="_blank"  > <i className="whaIcon"></i> </a>
		
			</div>
		);
	}
}

export default Whatsapp;


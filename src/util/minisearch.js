import React, { useState, useEffect, useRef } from 'react'
import MiniSearch from 'minisearch'
import axios from "axios";
import { Typeahead } from "react-bootstrap-typeahead";
const App = ({portOrigin}) => {
  // A collection of documents for our examples
const [selected,setSelected] = useState([]);
const [searchResults,setSearchResults] = useState([]);
  let miniSearch = new MiniSearch({
    fields: ['puerto'], // fields to index for full-text search
    storeFields: ['puerto'] , // fields to return with search results
     searchOptions: {
    fuzzy: 0.7
  }
    
  })
  
  // Index all documents
  miniSearch.addAll(portOrigin)
  
  // Search with default options
  let results = miniSearch.search('alemania')
  // => [
  //   { id: 2, title: 'Zen and the Art of Motorcycle Maintenance', category: 'fiction', score: 2.77258, match: { ... } },
  //   { id: 4, title: 'Zen and the Art of Archery', category: 'non-fiction', score: 1.38629, match: { ... } }
  // ]
  const handleSearchChange = (event) => {
    setSearchResults(event)
  }

  const handleSelect = (event) => {
    setSelected(event)
  }

  return (
    <div className='App'>
      <article className='main'>
      <Typeahead
                       
                          flip={true}
                          id="prueba"
                          labelKey={(option) => `${option.puerto}`}
                          renderMenuItemChildren={(option) => (
                            <div>{option.puerto}</div>
                          )}
                          multiple={false}
                          options={portOrigin}
                          placeholder="Pais,Ciudad o Puerto"
                          onChange={(portOriginSelected) => {
                            handleSearchChange(portOriginSelected)
                          }}
                        
                          selected={selected}
                        />

      </article>
    </div>
  )



}


export default App;

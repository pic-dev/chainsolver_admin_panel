import React, { Component } from "react";
import Carousel from "react-elastic-carousel";
import ReactPlayer from "react-player/lazy";


const item = [
  {
    id:
      "https://www.youtube.com/watch?v=PJIrFmZJU64",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=jRAIP-_WMcY",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=jrr9iTxQ4jI",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=dGvXICBrRkk",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=vo0nmkcayQU",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=je6afWI1uSw",
    status: "importador",
  },
  {
    id:
      "https://firebasestorage.googleapis.com/v0/b/pic-calculadora.appspot.com/o/fotos%2F03112020.mp4?alt=media&token=0ca883d5-9afb-4403-94b3-89592cc7b247",
    status: "importador",
  },

  {
    id:
      "https://www.youtube.com/watch?v=0GlrHRngkvs",
    status: "importador",
  },
  {
    id:
      "https://www.youtube.com/watch?v=HRvApW4FiGg",
    status: "importador",
  },
];

class testimonios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
    };
    this.breakPoints = [
      { width: 1, itemsToShow: 1 },
      { width: 550, itemsToShow: 2, itemsToScroll: 1 },
      { width: 850, itemsToShow: 3, pagination: false, showArrows: false },
      {
        width: 1150,
        itemsToShow: 4,
        itemsToScroll: 1,
        pagination: false,
        showArrows: false,
      },
      { width: 1450, itemsToShow: 4, pagination: false, showArrows: false },
      { width: 1750, itemsToShow: 4, pagination: false, showArrows: false },
    ];

  }

  render() {
    const opts = {
      playerVars: {
        youtube: {
          playerVars: { 
            'controls': 0,
            'origin': `${window.location.hostname}`
        },
        },
      },
    };

    return (
      <Carousel breakPoints={this.breakPoints}>
        {item.map((item) => (
          <ReactPlayer
            controls
            light={true}
            url={item.id}
            className="react-player video-youtube-Tras"
            key={item.id}
            width="100%"
            height="100%"
            config={opts}
          />
        ))}
      </Carousel>
    );
  }
}

export default testimonios;

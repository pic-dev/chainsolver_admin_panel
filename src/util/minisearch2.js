import React, { useState, useEffect, useRef } from "react";
import { useMiniSearch } from "react-minisearch";
import axios from "axios";
const App = ({ documents }) => {
  const {
    search,
    searchResults,
    clearSearch,
    autoSuggest,
    suggestions,
    clearSuggestions,
    isIndexing,
  } = useMiniSearch(documents, miniSearchOptions);

  const [query, setQuery] = useState("");
  const [selectedSuggestion, selectSuggestion] = useState(-1);
  const [fromYear, setFromYear] = useState(1965);
  const [toYear, setToYear] = useState(2015);
  const [searchOptions, setSearchOptions] = useState({
    fuzzy: 0.2,
    prefix: true,
    fields: ["puerto"],
    combineWith: "OR",
    filter: null,
  });

  // Perform search when the query changes
  useEffect(() => {
    if (query.length > 1) {
      search(query, searchOptions);
    } else {
      clearSearch();
    }
  }, [query, searchOptions]);

  // Manage selection of auto-suggestions
  useEffect(() => {
    if (selectedSuggestion >= 0) {
      const suggestionItem = suggestions[selectedSuggestion];
      setQuery(suggestionItem.suggestion);
    } else {
      clearSuggestions();
    }
  }, [selectedSuggestion]);

  // Update the search options if the year range is changed
  useEffect(() => {
    if (fromYear <= 1965 && toYear >= 2015) {
      setSearchOptions({ ...searchOptions, filter: null });
    } else {
      const filter = ({ year }) => {
        year = parseInt(year, 10);
        return year >= fromYear && year <= toYear;
      };
      setSearchOptions({ ...searchOptions, filter });
    }
  }, [fromYear, toYear]);

  const searchInputRef = useRef(null);

  const deselectSuggestion = () => {
    selectSuggestion(-1);
  };

  const topSuggestions = suggestions ? suggestions.slice(0, 5) : [];

  const autoSuggestOptions = {
    ...searchOptions,
    prefix: (term, i, terms) => i === terms.length - 1,
    boost: { artist: 5 },
  };

  const handleChange = ({ target: { value } }) => {
    setQuery(value);
    if (value.length > 1) {
      autoSuggest(value, autoSuggestOptions);
    } else {
      clearSuggestions();
    }
    deselectSuggestion();
  };

  const handleKeyDown = ({ which, key, keyCode }) => {
    if (key === "ArrowDown") {
      selectSuggestion(
        Math.min(selectedSuggestion + 1, topSuggestions.length - 1)
      );
    } else if (key === "ArrowUp") {
      selectSuggestion(Math.max(-1, selectedSuggestion - 1));
    } else if (key === "Enter" || key === "Escape") {
      deselectSuggestion();
      clearSuggestions();
      searchInputRef.current.blur();
    }
  };

  const handleSuggestionClick = (i) => {
    setQuery(topSuggestions[i].suggestion);
    deselectSuggestion();
    clearSuggestions();
  };

  const handleSearchClear = () => {
    setQuery("");
    deselectSuggestion();
    clearSuggestions();
  };

  const handleAppClick = () => {
    deselectSuggestion();
    clearSuggestions();
  };

  const setSearchOption = (option, valueOrFn) => {
    if (typeof valueOrFn === "function") {
      setSearchOptions({
        ...searchOptions,
        [option]: valueOrFn(searchOptions[option]),
      });
    } else {
      setSearchOptions({ ...searchOptions, [option]: valueOrFn });
    }
    search(query, searchOptions);
  };

  const selectFromYear = (year) => {
    setFromYear(parseInt(year, 10));
  };

  const selectToYear = (year) => {
    setToYear(parseInt(year, 10));
  };

  return (
    <div className="App" onClick={handleAppClick}>
      <article className="main">
        {
          <Header
            onChange={handleChange}
            onKeyDown={handleKeyDown}
            selectedSuggestion={selectedSuggestion}
            onSuggestionClick={handleSuggestionClick}
            onSearchClear={handleSearchClear}
            value={query}
            suggestions={topSuggestions}
            searchInputRef={searchInputRef}
            searchOptions={searchOptions}
            setSearchOption={setSearchOption}
            setFromYear={selectFromYear}
            setToYear={selectToYear}
            fromYear={fromYear}
            toYear={toYear}
          />
        }
        {searchResults && searchResults.length > 0 ? (
          <SongList songs={searchResults} />
        ) : isIndexing ? (
          <Loader text="Indexing..." />
        ) : (
          <Explanation />
        )}
      </article>
    </div>
  );
};

const SongList = ({ songs }) => (
  <ul className="SongList">
    {songs.map(({ id, ...props }) => (
      <Song {...props} key={id} />
    ))}
  </ul>
);

const Song = ({ puerto }) => (
  <li className="Song">
    <h3>{capitalize(puerto)}</h3>
    <dl>
      <dt>puerto:</dt> <dd>{capitalize(puerto)}</dd>
    </dl>
  </li>
);

const Header = (props) => (
  <header className="Header">
  
    <SearchBox {...props} />
  </header>
);

const SearchBox = ({
  onChange,
  onKeyDown,
  onSuggestionClick,
  onSearchClear,
  value,
  suggestions,
  selectedSuggestion,
  searchInputRef,
  searchOptions,
  setSearchOption,
  setFromYear,
  setToYear,
  fromYear,
  toYear,
}) => (
  <div className="SearchBox">
    <div className="Search">
      <input
      className="rbt-input-main form-control rbt-input  is-valid"
        type="text"
        value={value}
        onChange={onChange}
        onKeyDown={onKeyDown}
        ref={searchInputRef}
        autoComplete="none"
        autoCorrect="none"
        autoCapitalize="none"
        spellCheck="false"
      />
      
    </div>
    {suggestions && suggestions.length > 0 && (
      <SuggestionList
        items={suggestions}
        selectedSuggestion={selectedSuggestion}
        onSuggestionClick={onSuggestionClick}
      />
    )}

  </div>
);

const SuggestionList = ({ items, selectedSuggestion, onSuggestionClick }) => (
  <ul className="SuggestionList">
    {items.map(({ suggestion }, i) => (
      <Suggestion
        value={suggestion}
        selected={selectedSuggestion === i}
        onClick={(event) => onSuggestionClick(i, event)}
        key={i}
      />
    ))}
  </ul>
);

const Suggestion = ({ value, selected, onClick }) => (
  <li className={`Suggestion ${selected ? "selected" : ""}`} onClick={onClick}>
    {value}
  </li>
);



const Explanation = () => (
  <p>
    This example demonstrates search (with prefix and fuzzy match) and
    auto-completion.
  </p>
);

const Loader = ({ text }) => (
  <div className="Loader">{text || "loading..."}</div>
);

const capitalize = (string) =>
  string.replace(/(\b\w)/gi, (char) => char.toUpperCase());

const stopWords = new Set(["the", "a", "an", "and"]);

const years = [];
for (let y = 1965; y <= 2015; y++) {
  years.push(y);
}

const miniSearchOptions = {
  fields: ["puerto"],
  processTerm: (term, _fieldName) =>
    term.length <= 1 || stopWords.has(term) ? null : term.toLowerCase(),
};

// Fetch the JSON documents

const WithDocuments = (props) => {
  if (props.portOrigin && props.portOrigin.length > 0) {
    return <App {...props} documents={props.portOrigin} />;
  } else {
    return <Loader />;
  }
};

export default WithDocuments;

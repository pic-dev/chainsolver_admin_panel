import React, { Component, Fragment } from "react";

import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";

class banderas extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className=" banderas  mx-auto ">
        <i className="flag-usa banderasSize" />
        <i className="flag-panama banderasSize " />
        <i className="flag-china banderasSize " />
        <i className="flag-peru banderasSize " />
        <i className="flag-vnzla banderasSize" />
      </div>
    );
  }
}

export default banderas;

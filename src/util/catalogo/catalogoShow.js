import React, { Component } from "react";
import CustomNavigation from "./Navigation";
import PDFViewer from "pdf-viewer-reactjs";
import { PDFReader } from 'react-read-pdf';
import { MobilePDFReader } from 'react-read-pdf';
import "./estilo.css";

class catalogo extends Component {
  constructor(props) {
    super(props);
  }

  renderPDF() {
    if (screen.width > 1023) {
      return (
        <PDFViewer
        document={{
          url: this.props.link,
        }}
        canvasCss="customCanvas"
        navbarOnTop
        navigation={CustomNavigation}
        loader={<div className="loading" />}
        alert={(err) => (
          <div
            style={{
              color: "#fa5b35",
            }}
          >
            <h3 style={{ fontWeight: "bolder" }}>
              Error al cargar catalogo
            </h3>
            <h6>{err.message}</h6>
          </div>
        )}
      />
        );
    }else{
     window.open(this.props.link, "_blank");
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row"></div>
        {this.props.link ? (
          <div className="col-sm-12 text-center">
            <div
              style={{ width: "100%" }}
              className="text-white arial bg-info"
            >
              <span   style={{ fontSize: "1.2rem" }}>
                Catalogo{" "}
                <i
                  style={{
                    position: "absolute",
                    right: "1em",
                    cursor: "poiner",
                    top: "5px"
                  }}
                  className="text-right simple-icon-close"
                  onClick={() => {
                    this.props.close();
                  }} />
              </span>
            </div>
            <div>
            {this.renderPDF()}
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
export default catalogo;

import React, { useState } from "react";
import Pdf from "@mikecousins/react-pdf";

const catalogo = (props) => {
  const [page, setPage] = useState(1);

  const [pages, setPages] = useState(null);
  const onDocumentError = (err) => {
    console.error("pdf viewer error:", err);
  };

  const onDocumentComplete = (numPages) => {
    setPages(numPages);
  };

  return (
    <div className="col-sm-12 text-center">
      <div style={{ width: "100%" }} className="text-white arial bg-info">
        <span style={{ fontSize: "1.2rem" }}>
          Catalogo{" "}
          <i
            style={{
              position: "absolute",
              right: "1em",
              cursor: "poiner",
              top: "5px",
            }}
            className="text-right simple-icon-close"
            onClick={() => {
              props.close();
            }}
          />
        </span>
      </div>
      <div>
        <Pdf
          onDocumentComplete={onDocumentComplete}
          onDocumentError={onDocumentError}
          file={props.link}
          page={page}
        >
          {({ pdfDocument, pdfPage, canvas }) => (
            <>
              {Boolean(pdfDocument && pdfDocument.numPages) && (
                <div className="headerPdf">
                  <button
                    disabled={page === 1}
                    onClick={() => setPage(page - 1)}
                  >
                    <i className="simple-icon-arrow-left" />
                  </button>

                  <div className="ml-2">
                    Pagina {page} / {pages}
                  </div>

                  <button
                    disabled={page === pdfDocument.numPages}
                    onClick={() => setPage(page + 1)}
                  >
                    {" "}
                    <i className="simple-icon-arrow-right" />
                  </button>
                </div>
              )}
              {!pdfDocument && <div className="loading" />}
              <div className="pdfWrapper"> {canvas} </div>
            </>
          )}
        </Pdf>
      </div>
    </div>
  );
};

export default catalogo;

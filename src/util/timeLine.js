import React, { Component, Fragment } from "react";

import { Row } from "reactstrap";

import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";

class timeLine extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let bottomChange = this.props.data == true ? "timelineBotton" : "fixed-bottom";
    let iconChange = this.props.data == true ? " icon-serviceGreen" : " icon-serviceGrey";
   
    return (
      <div>
        {/*timeline mobile */}
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className="my-auto mx-auto NoPadding display-sm "
          style={{
            display: localStorage.page === "3" ? "none" : "",
            textAlign: "center"
          }}>
          <div className="linea2 pt-2"></div>
          <div className="my-auto mx-auto timeLine">
            <i  style={{
            display: localStorage.transporte === "MARITÍMO" ? "" : "none"
          }} className="icon-shipBlue" /> 
            <i  style={{
            display:  localStorage.transporte === "AÉREO"  ? "" : "none"
          }} className="icon-airplainBlue" /> 
            <i className="linea3" />
            <i className="icon-worldBlue" />
            <i className="linea4" />
            <i   className={`${iconChange}`}  />
            <i className="linea4" />
            <i className="icon-moneyGrey" />
          </div>
          <div className="my-auto mx-auto timeLine ">
            <span
              className="mr-5"
              style={{
                color: "#102a73"
              }}>
              1
            </span>
            <span
              className="ml-3 mr-5"
              style={{
                color: "#102a73"
              }}>
              2
            </span>
            <span
              className="ml-3 mr-5"
              style={{
                color: "#8a8a8a"
              }}>
              3
            </span>
            <span
              className="ml-3"
              style={{
                color: "#8a8a8a"
              }}>
              4
            </span>
          </div>

          <div className="linea2 mb-3"></div>
        </Colxx>
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className="my-auto mx-auto NoPadding display-sm"
          style={{
            display: localStorage.page === "2" ? "none" : "",
            textAlign: "center"
          }}>
          <div className="linea2 pt-2"></div>
          <div className="my-auto mx-auto timeLine">
            {" "}
            <i  style={{
            display: localStorage.transporte === "MARITÍMO" ? "" : "none"
          }} className="icon-shipBlue" /> 
            <i  style={{
            display:  localStorage.transporte === "AÉREO"  ? "" : "none"
          }} className="icon-airplainBlue" /> 
            <i className="linea3" />
            <i className="icon-worldBlue" />
            <i className="linea3" />
            <i className="icon-serviceBlue" />
            <i className="linea4" />
            <i className="icon-moneyGrey" />
          </div>
          <div className="my-auto mx-auto timeLine ">
            <span
              className="mr-5"
              style={{
                color: "#8a8a8a"
              }}>
              1
            </span>
            <span
              className="ml-3 mr-5"
              style={{
                color: "#8a8a8a"
              }}>
              2
            </span>
            <span
              className="ml-3 mr-5"
              style={{
                color: "#102a73"
              }}>
              3
            </span>
            <span
              className="ml-3"
              style={{
                color: "#102a73"
              }}>
              4
            </span>
          </div>

          <div className="linea2 mb-3"></div>
        </Colxx>

        {/*timeline web */}
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className={`mx-auto NoPadding display-large ${bottomChange}`}
          style={{
            display: localStorage.page === "3" ? "none" : "",
            textAlign: "center"
          }}>
          <div className="linea2"></div>
          <div className="my-auto mx-auto timeLine">
          <i  style={{
            display: localStorage.transporte === "MARITÍMO" ? "" : "none"
          }} className="icon-shipBlue" /> 
            <i  style={{
            display:  localStorage.transporte === "AÉREO"  ? "" : "none"
          }} className="icon-airplainBlue" /> 
            <i className="linea3" />
            <i className="icon-worldBlue" />
            <i className="linea4" />
            <i   className={`${iconChange}`}  />
            <i className="linea4" />
            <i className="icon-moneyGrey" />
          </div>
          <div className="my-auto mx-auto timeLine ">
            <span
              className="ml-4 mr-5"
              style={{
                color: "#102a73"
              }}>
              SERVICIOS
            </span>
            <span
              className="ml-5 mr-5"
              style={{
                color: "#102a73"
              }}>
              RUTA
            </span>
            <span
              className="ml-4 mr-5"
              style={{
                color: "#8a8a8a"
              }}>
              SERVICIOS EXTRAS
            </span>
            <span
              style={{
                color: "#8a8a8a"
              }}>
              PRESUPUESTO
            </span>
          </div>

          <div className="linea2 mb-3"></div>
        </Colxx>
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className="pt-2 mx-auto NoPadding display-large"
          style={{
            display: localStorage.page === "2" ? "none" : "",
            textAlign: "center"
          }}>
          <div className="linea2"></div>
          <div className="my-auto mx-auto timeLine">
            {" "}
            <i  style={{
            display: localStorage.transporte === "MARITÍMO" ? "" : "none"
          }} className="icon-shipBlue" /> 
            <i  style={{
            display:  localStorage.transporte === "AÉREO"  ? "" : "none"
          }} className="icon-airplainBlue" /> 
            <i className="linea3" />
            <i className="icon-worldBlue" />
            <i className="linea3" />
            <i className="icon-serviceBlue" />
            <i className="linea4" />
            <i className="icon-moneyGrey" />
          </div>
          <div className="my-auto mx-auto timeLine ">
            <span
              className="ml-4 mr-5"
              style={{
                color: "#102a73"
              }}>
              SERVICIOS
            </span>
            <span
              className="ml-5 mr-5"
              style={{
                color: "#102a73"
              }}>
              RUTA
            </span>
            <span
              className="ml-4 mr-5"
              style={{
                color: "#102a73"
              }}>
              SERVICIOS EXTRAS
            </span>
            <span
              style={{
                color: "#8a8a8a"
              }}>
              PRESUPUESTO
            </span>
          </div>

          <div className="linea2 mb-3"></div>
        </Colxx>
      </div>
    );
  }
}

export default timeLine;

import React, { useEffect,useState } from "react";
import usePWA from "react-pwa-install-prompt";
import { useCookies } from 'react-cookie';
import { firebaseConf } from './Firebase';
import { getCurrentTime, getDateWithFormat } from "./Utils";
import { Event } from "./tracking";
const Example = () => {
  const { isStandalone, isInstallPromptSupported, promptInstall } = usePWA();
  const [isInstalled, setIsInstalled] = useState(true);
  const [cookies, setCookie] = useCookies(['pwaInstalled']);

  const onClickInstall = async () => {
    const didInstall = await promptInstall();
    if (didInstall) {
      Event("App instalada", "App instalada", "INICIO");
      const record = {
        estatus:true,
        IP: sessionStorage.IP,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("instalacionPWA");
      const newInstall = dbRef.push();
      newInstall.set(record);

    }else{

      setCookie('pwaNotInstalled', true, { path: '/',maxAge: 3600 * 8},);
    }
  };

   
  function onChange() {
    setIsInstalled(false);
    setCookie('pwaNotInstalled', true, { path: '/',maxAge: 3600 * 8},);
  }


  const renderInstallButton = () => {
    if (isInstallPromptSupported && !isStandalone && isInstalled)
      return (
        <div className="install_page benefits-modal-wrapper">
          <div className="pwa-modal">
           
            <div className="pwa-modal-content">
              <div className="i-header_div">
                <b className="i_header"> Instalar App</b>
              </div>
              <ul className="i-benefits-div">
               <li className="i_benefit"><i className="simple-icon-check mr-1"/> Cotiza Tu Aduana y Flete.</li>
                <li className="i_benefit"><i className="simple-icon-check mr-1"/>  Talleres de importación Online.</li>
                 <li className="i_benefit"><i className="simple-icon-check mr-1"/>  No requiere registro.</li>
                <li className="i_benefit"><i className="simple-icon-check mr-1"/> Chatea con Charlie Bot y Aprende de Logistica.</li>
              </ul>
              <div className="i-msg-div">
         
              </div>
              <div className="i-btn-div">
              <button
                  onClick={() => onChange()}
                  type="button"
                  className="mr-3"
                    style={{ color: "black",  background: "linear-gradient(90deg, #d7d7d7 0%, #d7d7d7 35%, #6c757d 100%)" }}
                >
                  <span >
                    <span>
                      No, Gracias 
                    </span>
                  </span>
                </button>
                <button
                  onClick={onClickInstall}
                  type="button"
                    style={{ color: "#FFFFFF",  background: "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)" }}
                >
                  <span >
                    <span>
                      Instalar App
                    </span>
                  </span>
                </button>
           
              </div>
            </div>
          </div>
        </div>
      );
    return null;
  };


  useEffect(() => {
   

    if (isInstallPromptSupported && !isStandalone && isInstalled) {



    }
  }, [isStandalone, isInstallPromptSupported,isInstalled]);


  return renderInstallButton();

};

export default Example;

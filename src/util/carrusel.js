import React, { Component } from "react";
import Carousel, { consts } from "react-elastic-carousel";
import Likes from "./likes";
import { Button } from "reactstrap";
import { paisImport } from "./Utils";
import { NavLink } from "react-router-dom";
var timer;
var vacio;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
      autoplay: true,
      showArrows: false,
    };
    this.breakPoints = [
      {
        width: 1,
        itemsToShow: 1,
        showArrows: true,
        pagination: false,
        enableAutoPlay: false,
      },
      {
        width: 550,
        itemsToShow: 2,
        itemsToScroll: 2,
        pagination: false,
        showArrows: true,
        enableAutoPlay: false,
      },
      { width: 850, itemsToShow: 3, showArrows: true, enableAutoPlay: false },
      { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
      { width: 1450, itemsToShow: 5 },
      { width: 1750, itemsToShow: 6 },
    ];
    this.handleBoxToggle = this.handleBoxToggle.bind(this);
  }

  componentDidMount() {
    if (screen.width > 1023) {
      this.setState({
        autoplay: true,
      });
    } else {
      this.setState({
        autoplay: false,
      });
    }
  }

  onNextStart = (currentItem, nextItem) => {
    var tamaño = this.props.product.filter((item) => {
      if (item.TipoMerca == this.props.tipo) {
        return item;
      }
    });
    if (currentItem.index + 1 === tamaño.length) {
      // we hit the last item, go to first item
      timer = setTimeout(() => {
        this.carousel.goTo(Number(0));
      }, 5000);
    }
  };
  renderCarousel() {
    vacio = this.props.product.filter((item) => {
      if (item.TipoMerca == this.props.tipo) {
        return item;
      }
    });

    if (vacio.length > 0) {
      return this.props.product
        .filter((item) => {
          if (item.TipoMerca == this.props.tipo) {
            return item;
          }
        })
        .map((item, index) => {
          return (
            <div
              className="p-1 bg-white"
              style={{
                alignItems: "center",
                borderRadius: "0.5rem",
                border: "#d7d7d7 solid 1px",
              }}
              key={index + "a"}
            >
              <div>
              <NavLink to={`/SeccionImportadores/${item.key}?Name=${item.NombreProducto}`}>
                <img
                  key={index + "img"}
                  src={item.Multimedia[0].imagen[0].img}
                  className="imagenImportadores"
                  alt="logo"
                />
                 </NavLink>
              </div>
              <div
                className="color-black"
                style={{
                  backgroundColor: "#e9ecef",
                  padding: "5px",
                  width: "100%",
                  fontWeight: "bold",
                  fontSize: "0.7rem",
                }}
              >
                {item.NombreProducto.toUpperCase()}
              </div>
              <div style={{ display: "flex" }}>
                <Likes item={item.key} />{" "}
                <div
                  style={{
                    paddingTop: "5px",
                    width: "100%",
                    display: item.TipoMerca == "Proximas" ? "none" : "",
                  }}
                >
                  <i
                    style={{
                      height: "1rem",
                    }}
                    className={`mr-2 ${paisImport(item.PaisDesti)}`}
                  />
                  {item.PaisDesti !== undefined ? item.PaisDesti : "Perú"}
                </div>
              </div>

              <div
                style={{
                  paddingTop: "5px",
                  width: "100%",
                  display: item.TipoMerca == "transito" ? "" : "none",
                }}
              >
                <span>
                  <button className="transito" type="button"></button>
                </span>
                <span style={{ fontWeight: "bold" }}>Llegada estimada: </span>{" "}
                <span>{item.date}</span>
              </div>
              <NavLink to={`/SeccionImportadores/${item.key}?Name=${item.NombreProducto}`}>
                <div style={{ display: "flex", width: "100%" }}>
                  {" "}
                  <Button
                    className="btn-style-blue m-1 btn btn-success btn-sm"
                    style={{ fontSize: "0.8rem", width: "100%" }}
                  >
                    {item.TipoMerca == "Proximas"
                      ? "VER DETALLES "
                      : "VER IMPORTADORES "}
                  </Button>
                </div>
              </NavLink>
            </div>
          );
        });
    } else {
      return <span>No hay mercancia en transito actualmente</span>;
    }
  }

  handleBoxToggle(e) {
    this.setState({
      autoplay: e,
      showArrows: !e,
    });
  }

  render() {
    return (
      <div
        style={{ width: "100%" }}
        onMouseLeave={() => this.handleBoxToggle(true)}
        onMouseEnter={() => this.handleBoxToggle(false)}
      >
        <Carousel
          className="pl-2 pr-2"
          showArrows={this.state.showArrows}
          ref={(ref) => (this.carousel = ref)}
          itemPosition={consts.CENTER}
          autoPlaySpeed={5000}
          enableAutoPlay={this.state.autoplay}
          breakPoints={this.breakPoints}
          onChange={(tamaño, pageIndex) => this.onNextStart(tamaño, pageIndex)}
        >
          {this.renderCarousel()}
        </Carousel>
      </div>
    );
  }
}
export default App;

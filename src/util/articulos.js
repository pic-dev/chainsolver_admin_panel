import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AddOutlined from "@material-ui/icons/AddOutlined";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import InfoIcon from "@material-ui/icons/Info";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  gridList: {
    width: 1000,
    height: 1000,
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function Articulo({
  ArticuloSelect,
  articulos,
  status,
  JuniorData,
  modalDetalleArticuloOpen
}) {
  const classes = useStyles();



  return (

            <GridListTile key={articulos.id}>
              <img
                onClick={() => modalDetalleArticuloOpen(articulos)}
                src={articulos.image}
                alt={articulos.name}
              />
              <GridListTileBar
                title={articulos.name}
                subtitle={
                  status
                    ? JuniorData.length > 1
                      ? articulos.precio_venta
                      : articulos.precio_junior
                    : ""
                }
                actionIcon={
                  <IconButton
                    aria-label={`info about ${articulos.name}`}
                    className={classes.icon}
                  >
                    {status && (
                      <AddOutlined onClick={() => ArticuloSelect(articulos)} />
                    )}
                    <InfoIcon onClick={() => modalDetalleArticuloOpen(articulos)} />
                  </IconButton>
                }
              />
            </GridListTile>
      


  );
}

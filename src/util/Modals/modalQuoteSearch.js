import React, { Component } from "react";
import {
  Input,
  ModalHeader,
  FormGroup,
  ModalFooter,
  Button,
  Modal,
  ModalBody,
  Form,
  Row,
  Table,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import axios from "axios";
import logo from "../../assets/img/login/icons/logoDesglose.png";
import {
  conver_conten,
  igv,
  igvMasTotal,
  totalGeneral,
  totalGeneralMasIGVGanancias,
  totalGeneralCostos,
  totalGeneralMasIgvVEnta,
} from "../Utils";
class quoteSearch extends Component {
  constructor(props) {
    super(props);

    this.apiPost = this.apiPost.bind(this);
    this.limpiar = this.limpiar.bind(this);
    this.printDocument = this.printDocument.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      desgloseKey: [],
      desglose: false,
      desgloseValues: [],
      desgloseValuesCostos: [],
      resumen: [],
      show: false,
      flete: [],
      impuestosBajos: [],
      totalVenta: [],
      impuestosMedios: [],
      impuestosAltos: [],
      honorarios: [],
      servicioLogistico: [],
      Serviciologis: [],
      impuestosAltosTotal: [],
      impuestosBajosTotal: [],
      impuestosMediosTotal: [],
      servicioLogisticoTotal: [],
      venta: [],
      token: [],
      tipo: [],
      totalIGVFleteCostos: [],
      totalIGVCostos: [],
      activar: false,
      cliente: "",
      contacto: "",
      correo: "",
      ejecutivo: "",
      naviera: [],
    };
  }

  apiPost(e) {
    e.preventDefault();

    const { tk, tkManual, tipo } = e.target.elements;

    var url2;
    var ur;
    var res;
    var tipotk;
    var token;

    if (tk.value.length > 0) {
      res = tk.value.split("-");
      tipotk = res[1].trim();
      token = res[0].trim();
    } else {
      token = tkManual.value.trim();
      tipotk = tipo.value.trim();
    }

    var tk1 = token + "-" + tipotk;
    var transportes;
    var tipoCon;

    switch (tipotk) {
      case "FCL":
        transportes = "FLETE MARÍTIMO";
        tipoCon = "Maritimo - Completo";
        url2 = this.props.api + "maritimo_v2/get_ctz_fcl_resumen";

        ur = this.props.api + "maritimo_v2/get_busca_cot_fcl";
        break;
      case "LCL":
        transportes = "FLETE MARÍTIMO";

        tipoCon = "Maritimo - Compartido";
        url2 = this.props.api + "maritimo_v2/get_ctz_lcl_resumen";

        ur = this.props.api + "maritimo_v2/get_busca_cot_lcl";

        break;
      case "AER":
        transportes = "FLETE AÉREO";
        tipoCon = "Aereo";
        url2 = this.props.api + "aereo_v2/get_ctz_aereo_resumen";

        ur = this.props.api + "aereo_v2/get_busca_cot_aereo";
        break;

      default:
    }

    let token1 = token.toString();

    let datos1 = {
      token: token1,
    };
    (async () => {
      axios
        .post(url2, await datos1)
        .then((res) => {
          let resumen2 = res.data.data.r_dat;

          let impuestosBajosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          let impuestosMediosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          let impuestosAltosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });
          var Serviciologistico;
          let preServiciologistico = resumen2
            .filter(function (item) {
              if (
                item.tipo == "SERVICIO LOGÍSTICO" ||
                item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
              ) {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          if (preServiciologistico.length > 1) {
            Serviciologistico = preServiciologistico
              .reduce(function (accumulator, current) {
                return [+accumulator + +current];
              })
              .map(function (duration) {
                return duration.toFixed(2);
              });
          } else {
            Serviciologistico = preServiciologistico;
          }

          let venta = resumen2.filter(function (item) {
            if (
              item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA MEDIOS"
            ) {
              return item;
            }
          });

          this.setState({
            impuestosAltosTotal: impuestosAltosTotal,
            impuestosMediosTotal: impuestosMediosTotal,
            impuestosBajosTotal: impuestosBajosTotal,
            Serviciologis: Serviciologistico,
            venta: venta,
            tipo: tipoCon,
            token: tk1.toString(),
          });
        })
        .catch((error) => {
          console.log(error);
        });
    })();
    (async () => {
      axios
        .post(ur, await datos1)
        .then((res) => {
          let response = res.data.data.r_dat;
          let flete = response
            .filter(function (item) {
              if (item.tipo == transportes) {
                return item;
              }
            })
            .filter(function (item) {
              if (item.valor > 0) {
                return item;
              }
            });

          let impuestosBajos = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let IPM = response
            .filter(function (item) {
              if (
                item.tipo == "IMPUESTOS DE ADUANA BAJOS" &&
                item.nombre == "IPM"
              ) {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let impuestosMedios = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let impuestosAltos = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let transporte = response.filter(function (item) {
            if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
              return item;
            } else {
              return false;
            }
          });

          let honorarios = response.filter(function (item) {
            if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
              return item;
            } else {
              return false;
            }
          });

          let seguro = response.filter(function (item) {
            if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
              return item;
            } else {
              return false;
            }
          });

          let servicioLogistico = response.filter(function (item) {
            if (
              item.tipo == "SERVICIO LOGÍSTICO" ||
              item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
              item.detalles == "Ganancia"
            ) {
              return item;
            } else {
              return false;
            }
          });

          let desgloseValuesCostos = response.filter(function (item) {
            if (
              item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
              item.tipo !== transportes &&
              item.tipo !== "SERVICIO LOGÍSTICO" &&
              item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
              item.detalles !== "Ganancia" &&
              item.tipo !== "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA"
            ) {
              if (item.valor > 0) {
                return item;
              }
            }
          });

          let desgloseValues = response.filter(function (item) {
            if (
              item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
              item.tipo !== transportes &&
              item.tipo !== "SERVICIO LOGÍSTICO" &&
              item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
              item.detalles !== "Ganancia"
            ) {
              if (item.valor !== 0) {
                return item;
              }
            }
          });

          let PretotalIGVCostos, totalIGVCostos;

          switch (flete[0].detalles_calculos.pais_destino) {
            case "PERU":
              PretotalIGVCostos = response.filter(function (item) {
                if (
                  item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
                  item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
                  item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
                  item.tipo !== "SERVICIO LOGÍSTICO" &&
                  item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
                  item.tipo !== transportes &&
                  item.tipo !== "FLETE" &&
                  item.detalles !== "Ganancia" &&
                  item.tipo !== "THCD" &&
                  item.tipo !== "TCHD DOCUMENTACION" &&
                  item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO"
                ) {
                  if (item.valor > 0) {
                    return item;
                  }
                }
              });
              totalIGVCostos = [0];
    
              if (PretotalIGVCostos.length > 1) {
                totalIGVCostos = PretotalIGVCostos.map(function (duration) {
                  return (duration.valor * 0.18).toFixed(2);
                })
                  .reduce(function (accumulator, current) {
                    return [+accumulator + +current];
                  })
                  .map(function (duration) {
                    return duration.toFixed(2);
                  });
              }
    
              break;
            case "PANAMA":
              PretotalIGVCostos = response.filter(function (item) {
                if (item.tipo == "PORT CHARGES" || item.tipo == "MANEJOS") {
                  if (item.valor > 0) {
                    return item;
                  }
                }
              });
              totalIGVCostos = [0];
    
              if (PretotalIGVCostos.length > 1) {
                totalIGVCostos = PretotalIGVCostos.map(function (duration) {
                  return (duration.valor * 0.07).toFixed(2);
                })
                  .reduce(function (accumulator, current) {
                    return [+accumulator + +current];
                  })
                  .map(function (duration) {
                    return duration.toFixed(2);
                  });
              }
    
              break;
            case "VENEZUELA":
              totalIGVCostos = [0];
              break;
    
            default:
          }
    
          let desgloseTotal = response
            .filter(function (item) {
              if (
                item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
                item.tipo !== "SERVICIO LOGÍSTICO" &&
                item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
                item.detalles !== "Ganancia"
              ) {
                if (item.valor > 0) {
                  return item;
                }
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            })
            .reduce(function (accumulator, current) {
              return [+accumulator + +current];
            });

          let preservicioLogisticoTotal = response
            .filter(function (item) {
              if (
                item.tipo == "SERVICIO LOGÍSTICO" ||
                item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
                item.detalles == "Ganancia"
              ) {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor;
            });
          var servicioLogisticoTotal;
          if (preservicioLogisticoTotal.length > 1) {
            servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (
              accumulator,
              current
            ) {
              return [+accumulator + +current];
            });
          } else {
            servicioLogisticoTotal = preservicioLogisticoTotal;
          }

          let totalIGVFleteCostos =
            Number(desgloseTotal[0]) + Number(totalIGVCostos[0]);

          var totalVenta =
            Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);

          var naviera;
          switch (tipoCon) {
            case "Maritimo - Completo":
              {
                naviera = flete[0].detalles_calculos.naviera;
              }
              break;
            case "Maritimo - Compartido":
              {
                naviera = flete[0].detalles_calculos.agentes_lcl_nombre;
              }
              break;
            case "Aereo":
              {
                naviera = flete[0].detalles_calculos.agentes_aereo_nombre;
              }
              break;

            default:
          }
          this.setState({
            desgloseValues: desgloseValues,
            desgloseValuesCostos: desgloseValuesCostos,
            show: true,
            flete: flete,
            impuestosAltos: impuestosAltos,
            impuestosMedios: impuestosMedios,
            impuestosBajos: impuestosBajos,
            servicioLogistico: servicioLogistico,
            transporte: transporte,
            seguro: seguro,
            honorarios: honorarios,
            naviera: naviera,
            totalIGVFleteCostos: totalIGVFleteCostos.toFixed(2),
            totalIGVCostos: totalIGVCostos[0],
            servicioLogisticoTotal: servicioLogisticoTotal,
            desgloseTotal: desgloseTotal[0],
            totalVenta: totalVenta,
          });
        })

        .catch(() => {
          this.setState({
            servicioLogistico: [],
            transporte: [],
            seguro: [],
            honorarios: [],
            desgloseValues: [],
            desgloseValuesCostos: [],
            show: false,
            flete: [],
            impuestosAltos: [],
            totalIGVFleteCostos: [],
            totalIGVCostos: [],
            impuestosMedios: [],
            impuestosBajos: [],
            servicioLogistico: [],
            servicioLogisticoTotal: [],
            desgloseTotal: [],
            Serviciologis: [],
            impuestosAltosTotal: [],
            impuestosMediosTotal: [],
            impuestosBajosTotal: [],
            venta: [],
            naviera: [],
            tipo: [],
            token: [],
          });
          alert("token no valido");
        });
    })();
  }

  limpiar() {
    this.setState({
      servicioLogistico: [],
      transporte: [],
      seguro: [],
      honorarios: [],
      desgloseValues: [],
      desgloseValuesCostos: [],
      show: false,
      flete: [],
      totalIGVFleteCostos: [],
      totalIGVCostos: [],
      impuestosAltos: [],
      impuestosMedios: [],
      impuestosBajos: [],
      servicioLogistico: [],
      servicioLogisticoTotal: [],
      Serviciologis: [],
      desgloseTotal: [],
      impuestosAltosTotal: [],
      impuestosMediosTotal: [],
      impuestosBajosTotal: [],
      naviera: [],
      tipo: [],
      token: [],
    });
  }

  toggleModal() {
    this.setState((ps) => ({
      activar: !ps.activar,
    }));
  }

  printDocument() {
    const input = document.getElementById("divToPrint");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF({
        orientation: "landscape",
        unit: "mm",
      });
      pdf.addImage(imgData, "PNG", 7, 7, 280, 200);
      pdf.addImage(logo, "PNG", 7, 5, 42, 12);
      var nombre = this.state.token;
      pdf.save(nombre + ".pdf");
    });
    this.toggleModal();
  }
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={false}
          style={{ maxWidth: "max-content" }}
        >
          {this.state.desgloseKey == "piccargo1" ? (
            <ModalBody style={{ padding: "0" }} className="mx-auto">
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />
              <Row className="mx-auto">
                {this.state.show == false ? (
                  <Row className="mx-auto">
                    <Form className=" login-form" onSubmit={this.apiPost}>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        <Input
                          name="tk"
                          type="text"
                          placeholder="token 84524-fcl"
                        />
                      </Colxx>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        -----o------
                      </Colxx>
                      <Row>
                        <Colxx
                          xxs="12"
                          md="6"
                          sm="12"
                          className="p-3 text-center"
                        >
                          <Input
                            name="tkManual"
                            type="text"
                            placeholder="token 43542145654215"
                          />
                        </Colxx>
                        <Colxx
                          xxs="12"
                          md="6"
                          sm="12"
                          className="p-3 text-center"
                        >
                          <Input type="select" name="tipo" id="exampleSelect">
                            <option>FCL</option>
                            <option>LCL</option>
                            <option>AEREO</option>
                          </Input>
                        </Colxx>
                      </Row>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        <Button
                          type="submit"
                          className="btn-style-blue btn btn-success btn-sm"
                        >
                          Buscar
                        </Button>
                      </Colxx>
                    </Form>
                  </Row>
                ) : (
                  <Row className="mx-auto">
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="mx-auto pt-3 font2 pb-3 text-center"
                    >
                      <Button
                        className="btn-style-blue"
                        onClick={this.toggleModal}
                      >
                        Descargar
                      </Button>
                    </Colxx>
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="mx-auto pt-3 font2 pb-3 text-center"
                    >
                      <Button className="btn-style" onClick={this.limpiar}>
                        Buscar otra
                      </Button>
                    </Colxx>
                    <Modal
                      isOpen={this.state.activar}
                      toggle={this.toggleModal}
                    >
                      <ModalHeader className="mx-auto p-3 font text-center">
                        Descargar Presupuesto
                      </ModalHeader>
                      <ModalBody>
                        <Colxx
                          xxs="12"
                          md="12"
                          sm="12"
                          className="mx-auto p-2 text-center"
                        >
                          <FormGroup className="mb-1">
                            <span
                              style={{
                                fontWeight: "bold",
                              }}
                            >
                              NOMBRE DEL CLIENTE
                            </span>
                            - (Opcional)
                          </FormGroup>
                          <FormGroup className="mb-2">
                            <Input
                              name="cliente"
                              type="text"
                              placeholder="Ingrese aqui el nombre del cliente"
                              value={this.state.cliente}
                              onChange={(e) => {
                                this.setState({
                                  cliente: e.target.value,
                                });
                              }}
                            />
                          </FormGroup>
                          <FormGroup className="mb-1">
                            <span
                              style={{
                                fontWeight: "bold",
                              }}
                            >
                              CONTACTO
                            </span>
                            - (Opcional)
                          </FormGroup>
                          <FormGroup className="mb-2">
                            <Input
                              name="contacto"
                              type="text"
                              placeholder="Ingrese aqui el nombre del CONTACTO"
                              value={this.state.contacto}
                              onChange={(e) => {
                                this.setState({
                                  contacto: e.target.value,
                                });
                              }}
                            />
                          </FormGroup>
                          <FormGroup className="mb-1">
                            <span
                              style={{
                                fontWeight: "bold",
                              }}
                            >
                              CORREO
                            </span>
                            - (Opcional)
                          </FormGroup>
                          <FormGroup className="mb-2">
                            <Input
                              name="correo"
                              type="text"
                              placeholder="Ingrese aqui el CORREO del CONTACTO"
                              value={this.state.correo}
                              onChange={(e) => {
                                this.setState({
                                  correo: e.target.value,
                                });
                              }}
                            />
                          </FormGroup>
                          <FormGroup className="mb-1">
                            <span
                              style={{
                                fontWeight: "bold",
                              }}
                            >
                              EJECUTIVO PIC CARGO
                            </span>
                            - (Opcional)
                          </FormGroup>
                          <FormGroup className="mb-2">
                            <Input
                              name="ejecutivo"
                              type="text"
                              placeholder="Ingrese aqui el nombre del ejecutivo"
                              value={this.state.ejecutivo}
                              onChange={(e) => {
                                this.setState({
                                  ejecutivo: e.target.value,
                                });
                              }}
                            />
                          </FormGroup>
                        </Colxx>
                      </ModalBody>
                      <ModalFooter className="mx-auto p-3 font2 pb-3 text-center">
                        <Button
                          className="btn-style-blue"
                          onClick={this.printDocument}
                        >
                          Descargar
                        </Button>
                      </ModalFooter>
                    </Modal>
                    <div
                      id="divToPrint"
                      className="p-2"
                      style={{
                        width: "330mm",
                        minHeight: "210mm",
                        marginLeft: "auto",
                        marginRight: "auto",
                      }}
                    >
                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      style={{ color: "black", fontSize: "1rem" }}
                      className="pb-4  font text-right"
                    >
                      Numero de cotización #: ID {this.state.token}
                    </Colxx>
                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      style={{ overflow: "auto" }}
                      className="p-2 text-justify"
                    >
                      <Row>
                        <Colxx style={{ padding: "0" }} xxs="5" md="5" sm="5">
                          <div
                            className="color-black"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#eae6e6",
                            }}
                          >
                            DATOS DEL CLIENTE
                          </div>
                        </Colxx>
                        <Colxx
                          className="font color-black"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#fdfbb7",
                          }}
                          xxs="7"
                          md="7"
                          sm="7"
                        >
                          PROFIT:{" "}
                          {this.state.servicioLogisticoTotal[0].toFixed(2)}
                        </Colxx>
                        <Colxx xxs="5" md="5" sm="5">
                          <ul
                            className="p-1"
                            style={{ fontSize: "0.8rem", listStyle: "none" }}
                          >
                            <li>CLIENTE: {this.state.cliente}</li>
                            <li>CONTACTO:{this.state.contacto}</li>
                            <li>CORREO: {this.state.correo}</li>
                            <li>Ejecutivo PIC CARGO: {this.state.ejecutivo}</li>
                          </ul>
                        </Colxx>
                        <Colxx xxs="7" md="7" sm="7" style={{ padding: "0" }}>
                          <div
                            className="color-black"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#eae6e6",
                            }}
                          >
                            INFORMACIÓN DE LA CARGA
                          </div>
                          <Row>
                            <Colxx xxs="6" md="6" sm="6" className="p-1">
                              <ul
                                style={{
                                  fontSize: "0.8rem",
                                  listStyle: "none",
                                }}
                              >
                                <li>Transporte:{this.state.tipo}</li>
                                <li>
                                  Ruta Origen:
                                  {this.state.flete[0].detalles_calculos
                                    .pais_origen +
                                    " - " +
                                    this.state.flete[0].detalles_calculos
                                      .puerto_origen}
                                </li>
                                <li>
                                  Ruta Destino:
                                  {this.state.flete[0].detalles_calculos
                                    .pais_destino +
                                    " - " +
                                    this.state.flete[0].detalles_calculos
                                      .puerto_destino}
                                </li>
                                <li>
                                  Contenido de carga :
                                  {this.state.tipo === "Maritimo - Completo"
                                    ? conver_conten(this.state.flete)
                                    : this.state.flete[0]
                                        .input_cantidad_bultos +
                                      " Bultos, " +
                                      this.state.flete[0].input_peso +
                                      " Kl, " +
                                      this.state.flete[0].input_volumen +
                                      " M3 "}
                                </li>
                                <li>
                                  Gastos portuarios:
                                  {this.state.flete[0]
                                    .input_calcular_gastos_y_almacen_aduanero
                                    ? " SI"
                                    : " NO"}
                                </li>
                              </ul>
                            </Colxx>
                            <Colxx xxs="6" md="6" sm="6" className="p-1">
                              <ul
                                style={{
                                  fontSize: "0.8rem",
                                  listStyle: "none",
                                }}
                              >
                                <li>
                                  Valor Mercancia:
                                  {this.state.flete[0]
                                    .input_calcular_inpuestos_y_permisos
                                    ? this.state.flete[0].input_valor_mercancia
                                    : " NO COTIZADO"}
                                </li>
                                <li>
                                  Servicios / Impuestos de Aduana:
                                  {this.state.flete[0]
                                    .input_calcular_inpuestos_y_permisos
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>
                                  Transporte a Domicilio:
                                  {this.state.flete[0].input_calcular_transporte
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>
                                  Seguro de Mercancia:
                                  {this.state.flete[0]
                                    .input_calcular_seguro_mercancia
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>Naviera / Agente: {this.state.naviera}</li>

                                <li
                                  style={{
                                    display: this.state.flete[0]
                                      .input_calcular_inpuestos_y_permisos
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  {this.state.flete[0].input_ha_cotizado
                                    ? " Segunda Importación"
                                    : " Primera Importación"}
                                </li>
                              </ul>
                            </Colxx>
                          </Row>
                        </Colxx>
                      </Row>
                      <Colxx
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084",
                        }}
                        className="font2 color-white"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        GANANCIA
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>SUB ITEM</th>
                            <th>DETALLES</th>
                            <th>PROFIT</th>
                            <th>IGV</th>
                            <th>PROFIT+IGV</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.servicioLogistico.map((item, index) => (
                            <tr key={index + "a"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.nombre}</td>
                              <td>{item.detalles}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td></td>
                              <td></td>
                            </tr>
                          ))}
                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">--</th>
                            <td>--</td>
                            <td>TOTAL</td>
                            <td>
                              {this.state.servicioLogisticoTotal[0].toFixed(2)}
                            </td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (
                                    (this.state.Serviciologis[0] / 3) *
                                    2 *
                                    0.18
                                  ).toFixed(2)
                                : "n/a"}
                            </td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (
                                    (this.state.Serviciologis[0] / 3) *
                                      2 *
                                      0.18 +
                                    this.state.servicioLogisticoTotal[0]
                                  ).toFixed(2)
                                : "n/a"}
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084",
                        }}
                        className="font2 color-white"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        VENTA
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>VALOR</th>
                            <th>IGV</th>
                            <th>VALOR+IGV</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.venta.map((item, index) => (
                            <tr key={index + "b"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>
                                {igv(
                                  item,
                                  this.state.flete[0].detalles_calculos
                                    .pais_destino,
                                  this.state.totalIGVCostos
                                )}
                              </td>
                              <td>
                                {igvMasTotal(
                                  item,
                                  this.state.flete[0].detalles_calculos
                                    .pais_destino,
                                  this.state.totalIGVCostos
                                )}
                              </td>
                            </tr>
                          ))}

                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">TOTAL:</th>
                            <td>{this.state.totalVenta.toFixed(2)}</td>
                            <td>
                              {totalGeneralMasIGVGanancias(
                                this.state.servicioLogisticoTotal[0],
                                this.state.totalIGVCostos,
                                this.state.flete[0].detalles_calculos
                                  .pais_destino,
                                this.state.totalIGVCostos
                              )}
                            </td>
                            <td>
                              {totalGeneralMasIgvVEnta(
                                this.state.servicioLogisticoTotal[0],
                                this.state.totalIGVFleteCostos,
                                this.state.flete[0].detalles_calculos
                                  .pais_destino,
                                this.state.totalIGVCostos,
                                this.state.totalVenta
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084",
                        }}
                        className="font2 color-white"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        COSTOS
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>SUB ITEM</th>
                            <th>TOTAL</th>
                            <th>--</th>
                            <th>IGV</th>
                            <th>--</th>
                            <th>TOTAL+IGV</th>
                            <th>DETALLES</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.flete.map((item, index) => (
                            <tr key={index + "c"}>
                              <th scope="row">{item.nombre}</th>
                              <td>{item.nombre}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>--</td>
                              <td>n/a</td>
                              <td>--</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>{item.detalles}</td>
                            </tr>
                          ))}
                          {this.state.desgloseValues.map((item, index) => (
                            <tr key={index + "d"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.nombre}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>--</td>
                              <td>
                                {igv(
                                  item,
                                  this.state.flete[0].detalles_calculos
                                    .pais_destino,
                                  this.state.totalIGVCostos
                                )}
                              </td>
                              <td>--</td>
                              <td>
                                {igvMasTotal(
                                  item,
                                  this.state.flete[0].detalles_calculos
                                    .pais_destino,
                                  this.state.totalIGVCostos
                                )}
                              </td>
                              <td>{item.detalles}</td>
                            </tr>
                          ))}
                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">--</th>
                            <td>TOTAL</td>
                            <td>
                              {Number(this.state.desgloseTotal).toFixed(2)}
                            </td>
                            <td>--</td>
                            <td>
                              {totalGeneralCostos(
                                this.state.totalIGVCostos,
                                this.state.flete[0].detalles_calculos
                                  .pais_destino
                              )}
                            </td>
                            <td>--</td>
                            <td>
                              {totalGeneralCostos(
                                this.state.totalIGVFleteCostos,
                                this.state.flete[0].detalles_calculos
                                  .pais_destino
                              )}
                            </td>
                            <td>--</td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        style={{
                          textAlign: "center",
                          padding: "0rem",
                        }}
                        className="mx-auto text-center"
                      >
                        <Colxx
                          style={{
                            textAlign: "center",
                            backgroundColor: "#b33641",
                          }}
                          className="font2 color-white"
                          xxs="12"
                          md="12"
                          sm="12"
                        >
                          IMPUESTOS DE ADUANA
                        </Colxx>
                        <Table
                          style={{
                            textAlign: "center",
                          }}
                          responsive
                        >
                          <thead>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                TIPO
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS BAJOS
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#d0f7d1",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS MEDIOS
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#eac3c3",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS ALTOS
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                Percepciones 1era vez
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[0]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[0]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[0]}
                              </td>
                            </tr>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                ISC
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[1]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[1]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[1]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                IPM
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[2]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[2]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[2]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                IGV
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[3]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[3]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[3]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                AD valorem
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[4]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[4]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[4]}
                              </td>
                            </tr>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                TOTAL
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosBajosTotal[0] > 0
                                  ? "$ " + this.state.impuestosBajosTotal[0]
                                  : "No cotizado"}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosMediosTotal[0] > 0
                                  ? "$ " + this.state.impuestosMediosTotal[0]
                                  : "No cotizado"}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosAltosTotal[0] > 0
                                  ? "$ " + this.state.impuestosAltosTotal[0]
                                  : "No cotizado"}
                              </td>
                            </tr>
                          </tbody>
                        </Table>
                      </Colxx>
                    </Colxx>
                 </div>
                  </Row>
                )}
              </Row>
            </ModalBody>
          ) : (
            <ModalBody>
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />

              <Row>
                <Colxx xxs="12" md="12" sm="12" className="p-3 text-center">
                  <Input
                    name="clave"
                    type="text"
                    placeholder="Clave"
                    value={this.state.desgloseKey}
                    onChange={(e) => {
                      this.setState({
                        desgloseKey: e.target.value,
                      });
                    }}
                  />
                </Colxx>
              </Row>
            </ModalBody>
          )}
        </Modal>
      </div>
    );
  }
}

export default quoteSearch;

import React, { Component } from "react";
import { Input, Button, Modal, ModalBody, Form, Row, Table } from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import axios from "axios";
import logo from "../../assets/img/login/icons/logoDesglose.png";
class quoteSearch extends Component {
  constructor(props) {
    super(props);

    this.apiPost = this.apiPost.bind(this);
    this.limpiar = this.limpiar.bind(this);
    this.printDocument = this.printDocument.bind(this);
    this.state = {
      desgloseKey: [],
      desglose: false,
      desgloseValues: [],
      resumen: [],
      show: false,
      flete: [],
      impuestosBajos: [],
      totalVenta: [],
      impuestosMedios: [],
      impuestosAltos: [],
      honorarios: [],
      servicioLogistico: [],
      impuestosAltosTotal: [],
      impuestosBajosTotal: [],
      impuestosMediosTotal: [],
      servicioLogisticoTotal: [],
      token: [],
      tipo: [],
    };
  }

  apiPost(e) {
    e.preventDefault();

    const { tk, tkManual, tipo } = e.target.elements;

    var url2;
    var ur;
    var res;
    var tipotk;
    var token;
    if (tk.value.length > 0) {
      res = tk.value.split("-");
      tipotk = res[1];
      token = res[0];
    } else {
      token = tkManual.value;
      tipotk = tipo.value;
    }

    var tk1 = token + "-" + tipotk;
    var transportes;
    var tipoCon;
    if (tipotk == "FCL") {
      transportes = "FLETE MARÍTIMO";
      tipoCon = "Maritimo - Completo";
      url2 =
        this.props.api + "maritimo_v2/get_ctz_fcl_resumen";

      ur =
        this.props.api + "maritimo_v2/get_busca_cot_fcl";
    } else if (tipotk == "LCL") {
      transportes = "FLETE MARÍTIMO";

      tipoCon = "Maritimo - Compartido";
      url2 =
        this.props.api + "maritimo_v2/get_ctz_lcl_resumen";

      ur =
        this.props.api + "maritimo_v2/get_busca_cot_lcl";
    } else {
      transportes = "FLETE AÉREO";
      tipoCon = "Aereo";
      url2 =
        this.props.api + "aereo_v2/get_ctz_aereo_resumen";

      ur =
        this.props.api + "aereo_v2/get_busca_cot_aereo";
    }

    let token1 = token.toString();

    let datos1 = {
      token: token1,
    };
    (async () => {
      axios
        .post(url2, await datos1)
        .then((res) => {
          let resumen2 = res.data.data.r_dat;

          let impuestosBajosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          let impuestosMediosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          let impuestosAltosTotal = resumen2
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });

          let servicioLogisticoTotal = resumen2
            .filter(function (item) {
              if (
                item.tipo == "SERVICIO LOGÍSTICO" ||
                item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
                item.detalles == "Ganancia"
              ) {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            });
          var desgloseTotal = resumen2
            .filter(function (item) {
              if (
                item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
                item.tipo !== "SERVICIO LOGÍSTICO" &&
                item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
                item.detalles !== "Ganancia"
              ) {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed(2);
            })
            .reduce(function (accumulator, current) {
              return [+accumulator + +current];
            });

          var totalVenta =
            Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);

          this.setState({
            servicioLogisticoTotal: servicioLogisticoTotal,
            desgloseTotal: desgloseTotal,
            impuestosAltosTotal: impuestosAltosTotal,
            impuestosMediosTotal: impuestosMediosTotal,
            impuestosBajosTotal: impuestosBajosTotal,
            tipo: tipoCon,
            token: tk1.toString(),
            totalVenta: totalVenta,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    })();
    (async () => {
      axios
        .post(ur, await datos1)
        .then((res) => {
          let response = res.data.data.r_dat;

          let flete = response
            .filter(function (item) {
              if (item.tipo == transportes) {
                return item;
              }
            })
            .filter(function (item) {
              if (item.valor > 0) {
                return item;
              }
            });

          let impuestosBajos = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let impuestosMedios = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let impuestosAltos = response
            .filter(function (item) {
              if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                return item;
              } else {
                return false;
              }
            })
            .map(function (duration) {
              if (duration !== false) {
                return duration.valor.toFixed(2);
              } else {
                return duration;
              }
            });

          let transporte = response.filter(function (item) {
            if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
              return item;
            } else {
              return false;
            }
          });

          let honorarios = response.filter(function (item) {
            if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
              return item;
            } else {
              return false;
            }
          });

          let seguro = response.filter(function (item) {
            if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
              return item;
            } else {
              return false;
            }
          });

          let servicioLogistico = response.filter(function (item) {
            if (
              item.tipo == "SERVICIO LOGÍSTICO" ||
              item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
              item.detalles == "Ganancia"
            ) {
              return item;
            } else {
              return false;
            }
          });

          let desgloseValues = response.filter(function (item) {
            if (
              item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
              item.tipo !== transportes &&
              item.tipo !== "SERVICIO LOGÍSTICO" &&
              item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
              item.detalles !== "Ganancia"
            ) {
              if (item.valor > 0) {
                return item;
              }
            }
          });

          this.setState((ps) => ({
            desgloseValues: desgloseValues,
            show: true,
            flete: flete,
            impuestosAltos: impuestosAltos,
            impuestosMedios: impuestosMedios,
            impuestosBajos: impuestosBajos,
            servicioLogistico: servicioLogistico,
          }));
        })

        .catch((error) => {
          this.setState((ps) => ({
            desgloseValues: [],
            show: false,
            flete: [],
            impuestosAltos: [],
            impuestosMedios: [],
            impuestosBajos: [],
            servicioLogistico: [],
            servicioLogisticoTotal: [],
            desgloseTotal: [],
            impuestosAltosTotal: [],
            impuestosMediosTotal: [],
            impuestosBajosTotal: [],
            tipo: [],
            token: [],
          }));
          alert("token no valido");
        });
    })();
  }

  limpiar() {
    this.setState((ps) => ({
      desgloseValues: [],
      show: false,
      flete: [],
      impuestosAltos: [],
      impuestosMedios: [],
      impuestosBajos: [],
      servicioLogistico: [],
      servicioLogisticoTotal: [],
      desgloseTotal: [],
      impuestosAltosTotal: [],
      impuestosMediosTotal: [],
      impuestosBajosTotal: [],
      tipo: [],
      token: [],
    }));
  }

  printDocument() {
    const input = document.getElementById("divToPrint");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF({
        orientation: "landscape",
        unit: "mm",
      });
      pdf.addImage(imgData, "png", 0, 7, 297, 200);
      pdf.addImage(logo, "png", 5, 5, 35, 10);
      var nombre = this.state.token;
      // pdf.output('dataurlnewwindow');
      pdf.save(nombre + ".pdf");
    });
  }
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={false}
          size="lg"
          style={{ maxWidth: "fit-content" }}
        >
          {this.state.desgloseKey == "piccargo1" ? (
            <ModalBody style={{ padding: "0" }} className="mx-auto">
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />

              <Row className="mx-auto">
                {this.state.show == false ? (
                  <Row className="mx-auto">
                    <Form className=" login-form" onSubmit={this.apiPost}>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        <Input
                          name="tk"
                          type="text"
                          placeholder="token 84524-fcl"
                        />
                      </Colxx>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        -----o------
                      </Colxx>
                      <Row>
                        <Colxx
                          xxs="12"
                          md="6"
                          sm="12"
                          className="p-3 text-center"
                        >
                          <Input
                            name="tkManual"
                            type="text"
                            placeholder="token 43542145654215"
                          />
                        </Colxx>
                        <Colxx
                          xxs="12"
                          md="6"
                          sm="12"
                          className="p-3 text-center"
                        >
                          <Input type="select" name="tipo" id="exampleSelect">
                            <option>FCL</option>
                            <option>LCL</option>
                            <option>AEREO</option>
                          </Input>
                        </Colxx>
                      </Row>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="p-3 text-center"
                      >
                        <Button
                          type="submit"
                          className="btn-style-blue btn btn-success btn-sm"
                        >
                          Buscar
                        </Button>
                      </Colxx>
                    </Form>
                  </Row>
                ) : (
                  <Row className="mx-auto">
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="mx-auto pt-3 font2 pb-3 text-center"
                    >
                      {" "}
                      <button
                        className="btn-style-blue"
                        onClick={this.printDocument}
                      >
                        Descargar
                      </button>
                    </Colxx>
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="mx-auto pt-3 font2 pb-3 text-center"
                    >
                      {" "}
                      <button className="btn-style" onClick={this.limpiar}>
                        Buscar otra
                      </button>
                    </Colxx>
                    <div
                      id="divToPrint"
                      className="p-2"
                      style={{
                        width: "330mm",
                        minHeight: "210mm",
                        marginLeft: "auto",
                        marginRight: "auto",
                      }}
                    >
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        style={{ fontSize: "1rem" }}
                        className="pb-4  font text-right color-black"
                      >
                        Numero de cotización #: ID {this.state.token}
                      </Colxx>

                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        style={{overflow: "auto"}}
                        className="p-2 text-justify"
                      >
                        {" "}
                        <Row>
                          <Colxx style={{ padding: "0" }} xxs="5" md="5" sm="5">
                            <div
                            className="color-black"
                              style={{
                                textAlign: "center",
                                backgroundColor: "#eae6e6",
                              }}
                            >
                              DATOS DEL CLIENTE
                            </div>
                          </Colxx>

                          <Colxx
                            className="font color-black"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#fdfbb7",
                            }}
                            xxs="7"
                            md="7"
                            sm="7"
                          >
                            PROFIT: {this.state.servicioLogisticoTotal[0]}
                          </Colxx>
                          <Colxx xxs="5" md="5" sm="5">
                            <ul
                              className="p-1"
                              style={{ fontSize: "0.8rem", listStyle: "none" }}
                            >
                              <li>CLIENTE: </li>
                              <li>CONTACTO:</li>
                              <li>CORREO:</li>
                              <li>Ejecutivo PIC CARGO:</li>
                            </ul>
                          </Colxx>

                          <Colxx xxs="7" md="7" sm="7" style={{ padding: "0" }}>
                            {" "}
                            <div
                            className="color-black"
                              style={{
                                textAlign: "center",
                                backgroundColor: "#eae6e6"
                              }}
                            >
                              INFORMACIÓN DE LA CARGA
                            </div>
                            <Row>
                              <Colxx xxs="6" md="6" sm="6" className="p-1">
                                <ul
                                  style={{
                                    fontSize: "0.8rem",
                                    listStyle: "none",
                                  }}
                                >
                                  <li>Transporte:{this.state.tipo}</li>
                                  <li>Bultos / Peso / M3 :</li>
                                  <li>
                                    Gastos portuarios:
                                    {this.state.flete[0]
                                      .input_calcular_gastos_y_almacen_aduanero
                                      ? " SI"
                                      : " NO"}
                                  </li>
                                  <li>
                                    Valor Mercancia:{" "}
                                    {this.state.flete[0]
                                      .input_calcular_inpuestos_y_permisos
                                      ? this.state.flete[0]
                                          .input_valor_mercancia
                                      : " NO COTIZADO"}
                                  </li>
                                </ul>
                              </Colxx>{" "}
                              <Colxx xxs="6" md="6" sm="6" className="p-1">
                                {" "}
                                <ul
                                  style={{
                                    fontSize: "0.8rem",
                                    listStyle: "none",
                                  }}
                                >
                                  <li>
                                    Servicios / Impuestos de Aduana:{" "}
                                    {this.state.flete[0]
                                      .input_calcular_inpuestos_y_permisos
                                      ? " SI"
                                      : " NO"}
                                  </li>
                                  <li>
                                    Transporte a Domicilio:{" "}
                                    {this.state.flete[0]
                                      .input_calcular_transporte
                                      ? " SI"
                                      : " NO"}
                                  </li>
                                  <li>
                                    Seguro de Mercancia:{" "}
                                    {this.state.flete[0]
                                      .input_calcular_seguro_mercancia
                                      ? " SI"
                                      : " NO"}
                                  </li>
                                </ul>
                              </Colxx>
                            </Row>
                          </Colxx>
                        </Row>
        
                        <Colxx
                           className="color-white"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#0d5084"
                          }}
                          className="font2"
                          xxs="12"
                          md="12"
                          sm="12"
                        >
                          INGRESOS
                        </Colxx>
                        <Table
                          style={{
                            textAlign: "center",
                            margin: "0rem",
                          }}
                          responsive
                        >
                          <thead
                            style={{
                              backgroundColor: "#cfe7fe",
                              fontWeight: "bold",
                            }}
                          >
                            <tr>
                              <th>ITEM</th>
                              <th>SUB ITEM</th>
                              <th>DETALLES</th>
                              <th>PROFIT</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.servicioLogistico.map((item) => (
                              <tr>
                                <th scope="row">{item.tipo}</th>
                                <td>{item.nombre}</td>
                                <td>{item.detalles}</td>
                                <td>{item.valor.toFixed(2)}</td>
                              </tr>
                            ))}
                            <tr
                              style={{
                                backgroundColor: "#d0e4f7",
                                fontWeight: "bold",
                              }}
                            >
                              <th scope="row">--</th>
                              <td>--</td>
                              <td>TOTAL</td>
                              <td>{this.state.servicioLogisticoTotal[0]}</td>
                            </tr>
                          </tbody>
                        </Table>
                        <Colxx
                           className="color-white"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#0d5084"
                          }}
                          className="font2"
                          xxs="12"
                          md="12"
                          sm="12"
                        >
                          VENTA
                        </Colxx>
                        <Table
                          style={{
                            textAlign: "center",
                            margin: "0rem",
                          }}
                          responsive
                        >
                          <thead
                            style={{
                              backgroundColor: "#cfe7fe",
                              fontWeight: "bold",
                            }}
                          >
                            <tr>
                              <th>ITEM</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.flete.map((item) => (
                              <tr>
                                <th scope="row">{item.tipo}</th>
                              </tr>
                            ))}
                            {this.state.desgloseValues.map((item) => (
                              <tr>
                                <th scope="row">
                                  {item.tipo !==
                                  "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA"
                                    ? item.tipo == "GASTOS DE ALMACEN/PUERTO"
                                      ? item.nombre
                                      : item.tipo
                                    : ""}
                                </th>
                              </tr>
                            ))}
                            <tr
                              style={{
                                backgroundColor: "#d0e4f7",
                                fontWeight: "bold",
                              }}
                            >
                              <th scope="row">
                                TOTAL: {this.state.totalVenta}
                              </th>
                            </tr>
                          </tbody>
                        </Table>
                        <Colxx
                           className="color-white"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#0d5084"
                          }}
                          className="font2"
                          xxs="12"
                          md="12"
                          sm="12"
                        >
                          COSTOS
                        </Colxx>
                        <Table
                          style={{
                            textAlign: "center",
                            margin: "0rem",
                          }}
                          responsive
                        >
                          <thead
                            style={{
                              backgroundColor: "#cfe7fe",
                              fontWeight: "bold",
                            }}
                          >
                            <tr>
                              <th>ITEM</th>
                              <th>SUB ITEM</th>
                              <th>PROVEDOR</th>
                              <th>VALOR</th>
                              <th>DETALLES</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.flete.map((item) => (
                              <tr>
                                <th scope="row">{item.tipo}</th>
                                <td>{item.nombre}</td>
                                <td>PIC CARGO</td>
                                <td>{item.valor.toFixed(2)}</td>
                                <td>{item.detalles}</td>
                              </tr>
                            ))}
                            {this.state.desgloseValues.map((item) => (
                              <tr>
                                <th scope="row">{item.tipo}</th>
                                <td>{item.nombre}</td>
                                <td>PIC CARGO</td>
                                <td>{item.valor.toFixed(2)}</td>
                                <td>{item.detalles}</td>
                              </tr>
                            ))}
                            <tr
                              style={{
                                backgroundColor: "#d0e4f7",
                                fontWeight: "bold",
                              }}
                            >
                              <th scope="row">--</th>
                              <td>--</td>
                              <td>TOTAL</td>
                              <td>{this.state.desgloseTotal[0]}</td>
                              <td>--</td>
                            </tr>
                          </tbody>
                        </Table>
                        <Colxx
                          xxs="12"
                          md="12"
                          sm="12"
                          style={{
                            textAlign: "center",
                            padding: "0rem",
                            display:
                              this.state.impuestosBajos == false ? "none" : "",
                          }}
                          className="mx-auto text-center"
                        >
                          <Colxx
                             className="color-white"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#b33641"
                            }}
                            className="font2"
                            xxs="12"
                            md="12"
                            sm="12"
                          >
                            IMPUESTOS DE ADUANA
                          </Colxx>
                          <Table
                            style={{
                              textAlign: "center",
                            }}
                            responsive
                          >
                            <thead>
                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                >
                                  TIPO
                                </th>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                >
                                  IMPUESTOS BAJOS
                                </th>
                                <th
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                    fontWeight: "bold",
                                  }}
                                >
                                  IMPUESTOS MEDIOS
                                </th>
                                <th
                                  style={{
                                    backgroundColor: "#eac3c3",
                                    fontWeight: "bold",
                                  }}
                                >
                                  IMPUESTOS ALTOS
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  Percepciones 1era vez
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                  }}
                                >
                                  {this.state.impuestosBajos[0]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                  }}
                                >
                                  {this.state.impuestosMedios[0]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                  }}
                                >
                                  {this.state.impuestosAltos[0]}
                                </td>
                              </tr>
                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  ISC
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                  }}
                                >
                                  {this.state.impuestosBajos[1]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                  }}
                                >
                                  {this.state.impuestosMedios[1]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                  }}
                                >
                                  {this.state.impuestosAltos[1]}
                                </td>
                              </tr>

                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  IPM
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                  }}
                                >
                                  {this.state.impuestosBajos[2]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                  }}
                                >
                                  {this.state.impuestosMedios[2]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                  }}
                                >
                                  {this.state.impuestosAltos[2]}
                                </td>
                              </tr>

                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  IGV
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                  }}
                                >
                                  {this.state.impuestosBajos[3]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                  }}
                                >
                                  {this.state.impuestosMedios[3]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                  }}
                                >
                                  {this.state.impuestosAltos[3]}
                                </td>
                              </tr>

                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  AD valorem
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                  }}
                                >
                                  {this.state.impuestosBajos[4]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                  }}
                                >
                                  {this.state.impuestosMedios[4]}
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                  }}
                                >
                                  {this.state.impuestosAltos[4]}
                                </td>
                              </tr>
                              <tr>
                                <th
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                  scope="row"
                                >
                                  TOTAL
                                </th>
                                <td
                                  style={{
                                    backgroundColor: "#d0e4f7",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {this.state.impuestosBajosTotal[0] > 0 ? ("$ " + this.state.impuestosBajosTotal[0]) : ("No cotizado")} 
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#d0f7d1",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {this.state.impuestosMediosTotal[0] > 0 ? ("$ " + this.state.impuestosMediosTotal[0]) : ("No cotizado")} 
                              
                                </td>
                                <td
                                  style={{
                                    backgroundColor: "#eac3c3",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {this.state.impuestosAltosTotal[0] > 0 ? ("$ " + this.state.impuestosAltosTotal[0]) : ("No cotizado")} 
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </Colxx>
                      </Colxx>
                    </div>
                  </Row>
                )}
                <Colxx
                  xxs="12"
                  md="12"
                  sm="12"
                  className="pt-3  pb-3"
                  style={{ textAlign: "center" }}
                >
                  <Button
                    color="success"
                    onClick={this.props.toggle}
                    className="btn-style"
                    size="sm"
                  >
                    {" "}
                    VOLVER
                  </Button>
                </Colxx>
              </Row>
            </ModalBody>
          ) : (
            <ModalBody>
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />

              <Row>
                <Colxx xxs="12" md="12" sm="12" className="p-3 text-center">
                  <Input
                    name="clave"
                    type="text"
                    placeholder="Clave"
                    value={this.state.desgloseKey}
                    onChange={(e) => {
                      this.setState({
                        desgloseKey: e.target.value,
                      });
                    }}
                  />
                </Colxx>
              </Row>
            </ModalBody>
          )}
        </Modal>
      </div>
    );
  }
}

export default quoteSearch;

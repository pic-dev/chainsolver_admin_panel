import React, { Component } from "react";
import {
  Row,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  CustomInput,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import {taxesAlert} from "../Utils";

class ctzSegura extends Component {
  constructor(props) {
    super(props);
  }
  SelectedOnceCTZ() {
    sessionStorage.SelectedOnceCTZ = "1";
  }
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          size="md"
          className="theme-color-blueHeader2 modalInfo"
        >
          <ModalBody
            className="theme-color-blueHeader2"
            style={{ padding: "0rem" }}
          >
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="fontTitle text-center pt-2 pb-2 arial theme-color-blue"
            >
              <div>
                {" "}
                <i className="icon-escudo" />
              </div>
              <div>¡IMPORTANTE!</div>
            </Colxx>

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="fontText1 text-center pt-2 pb-2 arial theme-color-orange"
            >
              La oferta de sevicio a continuación es 100% REAL y confiable
            </Colxx>
      
          </ModalBody>
          <ModalFooter className="theme-color-blueFooter">
            <Row>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                style={{
                  fontSize: "1rem",
                  fontWeight: "bold",
                }}
                className="FooterText1 pl-4 pr-4 mb-2 text-center"
              >
                Los escenarios de impuestos dependerán de una descripción
                detallada de tu producto.
              </Colxx>
              <Colxx xxs="12" md="12" sm="12" className="mb-2">
                <CustomInput
                  id="SelectedOnce"
                  type="checkbox"
                  onChange={this.SelectedOnceCTZ}
                  label="No volver a mostrar este mensaje"
                />
              </Colxx>
              <Colxx xxs="12" md="12" sm="12" style={{ textAlign: "center" }}>
                <Button
                  color="success"
                  onClick={this.props.toggle}
                  className="btn-style-bluelight"
                  size="sm"
                >
                  IR A PRESUPUESTO
                </Button>
              </Colxx>
            </Row>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default ctzSegura;

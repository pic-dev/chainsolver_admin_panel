import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import { isMovil } from "Util/Utils";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Pedido from "Routes/pages/seccionImportadores/pedidoMovil";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" in={props.open} ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
    TextField: {
      margin: theme.spacing(1),
      width: "25ch",
    },
    appBar: {
      position: "relative",
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    buttons: {
      display: "flex",
      justifyContent: "flex-end",
    },
    button: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(1),
    },
    "& img": {
      maxWidth: "min-content",
      maxHeight: "35rem",

      width: "100%",
      width: "-moz-available",
      width: "-webkit-fill-available",
      width: "fill-available",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      [theme.breakpoints.up("xs")]: {
        maxHeight: "25rem",
      },
    },
  },
  DialogContent: {
    padding: theme.spacing(3, 1),
  },
}));

export default function ModaPedidoMovil({
  toggle,
  open,
  JuniorData,
  catalogo,
  articulo,
  setArticuloSelected,
  tipo
}) {
  const classes = useStyles();

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        fullWidth={true}
        fullScreen={isMovil()}
        maxWidth={"lg"}
        keepMounted
        onClose={() => toggle(!open)}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >

        {isMovil() && (
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={() => toggle(!open)}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
        )}
        <DialogTitle id="alert-dialog-slide-title">{"Pedido"}</DialogTitle>
        <DialogContent className={classes.DialogContent}>
        <Pedido
          JuniorData={JuniorData}
          catalogo={catalogo}
          articulo={articulo}
          setArticuloSelected={setArticuloSelected}
          tipo={tipo}
          toggle={toggle}
        />
          <DialogActions>
            <Button onClick={() => toggle(!open)} color="primary">
              Volver
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

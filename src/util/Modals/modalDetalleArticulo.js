import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import { makeStyles } from "@material-ui/core/styles";
import SaveIcon from "@material-ui/icons/Save";
import Grid from "@material-ui/core/Grid";
import { isMovil } from "Util/Utils";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import Tooltip from "@material-ui/core/Tooltip";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" in={props.open} ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
    TextField: {
      margin: theme.spacing(1),
      width: "25ch",
    },
    appBar: {
      position: "relative",
      fontSize: "1rem",
      fontWeight: "bold",
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    buttons: {
      display: "flex",
      justifyContent: "flex-end",
    },
    button: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(1),
    },
    "& img": {
      maxWidth: "min-content",
      maxHeight: "35rem",

      width: "100%",
      width: "-moz-available",
      width: "-webkit-fill-available",
      width: "fill-available",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      [theme.breakpoints.up("xs")]: {
        maxHeight: "25rem",
      },
    },
  },
  DialogContent: {
    padding: theme.spacing(3, 1),
  },
  titleBar: {
    backgroundColor: "#59bae8",
    color: "white",
    fontWeight: "bold",
  },
  AddShoppingCartIcon: {
    color: "#000000",
    backgroundColor: "#ec6729",
    borderRadius: " 50%",
    fontWeight: "bold",
  },
}));

export default function ModalDetalleArticulo({
  toggle,
  addArticulo,
  open,
  status,
  articulo,
  toggleModal,
  JuniorData,
  changeProducto,
  productoPage,
  limite,
  loadingNextPage,
}) {
  const classes = useStyles();
  const toggleAdd = (articulo) => {
    toggle(!open);
    addArticulo(articulo);

  };

  const togglePreOrden = () => {
    toggle(!open);
    toggleModal();
  };

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        fullWidth={true}
        fullScreen={isMovil()}
        maxWidth={"lg"}
        keepMounted
        onClose={() => toggle(!open)}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle className={classes.titleBar} id="alert-dialog-slide-title">
          <Grid container>
            <Grid item xs={6} sm={6} md={6}>
              <Typography style={{ width: "100%" }} component="div">
                <Box
                  fontWeight="fontWeightBold"
                  textAlign="left"
                  fontSize="h5.fontSize"
                  color="text-primary"
                  m={0.5}
                >
                  {" "}
                  {articulo.name}
                </Box>
                {articulo.codigo !== null && (
                  <Box textAlign="left">Codigo: {articulo.codigo}</Box>
                )}
              </Typography>
            </Grid>
            {status && (
              <Grid item xs={5} sm={5} md={5}>
                <Typography style={{ width: "100%" }} component="div">
                  <Box
                    fontWeight="fontWeightBold"
                    fontSize="h5.fontSize"
                    color="text-primary"
                    m={0.5}
                    textAlign="right"
                  >
                    {JuniorData.length > 1
                      ? "$" + articulo.precio_venta
                      : "$" + articulo.precio_junior}
                  </Box>
                </Typography>
              </Grid>
            )}
            <Grid
              style={{ position: "absolute", right: 0 }}
              item
              xs={1}
              sm={1}
              md={1}
            >
              <IconButton
                edge="start"
                color="inherit"
                onClick={() => toggle(!open)}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>{" "}
        </DialogTitle>
        <DialogContent className={classes.DialogContent}>
          <Grid container className={classes.root} spacing={4}>
            <Grid item xs={12} sm={12} md={6}>
              <img
                className={classes.imagen}
                alt={articulo.name}
                src={articulo.image}
              />
              <Grid className="text-center" item xs={12} sm={12} md={12}>
                <IconButton
                  edge="start"
                  color="primary"
                  aria-label="ArrowBackIcon"
                  disabled={productoPage == 0}
                  onClick={() => changeProducto("prev", articulo.id)}
                >
                  <ArrowBackIcon />
                </IconButton>
                <IconButton color="inherit" aria-label="number">
                  Articulo {productoPage + 1}/{limite}
                </IconButton>
                <IconButton
                  edge="end"
                  color="primary"
                  aria-label="ArrowForwardIcon"
                  disabled={loadingNextPage || productoPage + 1 == limite}
                  onClick={() => changeProducto("next", articulo.id)}
                >
                  {loadingNextPage ? (
                    <CircularProgress />
                  ) : (
                    <ArrowForwardIcon />
                  )}
                </IconButton>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <Typography style={{ width: "100%" }} component="div">
                <Box
                  fontWeight="fontWeightBold"
                  textAlign="center"
                  fontSize="h6.fontSize"
                  color="text-primary"
                  m={0.5}
                ></Box>
                <Box fontWeight="fontWeightBold" textAlign="left" m={0.5}>
                  Caracteristicas
                </Box>
                {articulo.color !== null && (
                  <Box textAlign="left">Color: {articulo.color}</Box>
                )}
                {articulo.genero !== null && (
                  <Box textAlign="left">Genero: {articulo.genero}</Box>
                )}
                {articulo.temporada !== null && (
                  <Box textAlign="left">Temporada: {articulo.temporada}</Box>
                )}
                {articulo.escala !== null && (
                  <Box textAlign="left">Escala: {articulo.escala}</Box>
                )}
                {articulo.codigo !== null && (
                  <Box textAlign="left">Codigo: {articulo.codigo}</Box>
                )}

                {status && (
                  <Box textAlign="left">
                    Precio:{" "}
                    {JuniorData.length > 1
                      ? articulo.precio_venta + " USD"
                      : articulo.precio_junior + " USD"}
                  </Box>
                )}

                <Box fontWeight="fontWeightBold" textAlign="left" m={0.5}>
                  Descripción
                </Box>
                <TextareaAutosize
                  rowsMax={6}
                  style={{ width: "100%" }}
                  aria-label="empty textarea"
                  variant="outlined"
                  value={articulo.caracteristicas}
                  disabled
                />
              </Typography>
              {!status && (
                <Typography style={{ width: "100%" }} component="div">
                  {" "}
                  <Box textAlign="left">
                    Inicia sesión para ver los precios
                    <Tooltip title="Los Precio se brindan en los grupos cerrados">
                      <IconButton aria-label="Wha">
                        <a
                          style={{ padding: "0px" }}
                          href="https://wa.link/xbr57j"
                          rel="noopener noreferrer"
                          target="_blank"
                        >
                          <WhatsAppIcon color="secondary" />
                        </a>
                      </IconButton>
                    </Tooltip>
                  </Box>{" "}
                </Typography>
              )}
            </Grid>
          </Grid>{" "}
          <DialogActions>
            <IconButton
              color="inherit"
              aria-label="addIcon"
              onClick={() => toggle(!open)}
            >
              <ArrowBackIcon />
            </IconButton>
            {!status && (
              <IconButton
                color="inherit"
                aria-label="addIcon"
                className={classes.AddShoppingCartIcon}
                onClick={() => togglePreOrden()}
              >
                <SaveIcon />
              </IconButton>
            )}

            {status && (
              <IconButton
                color="inherit"
                aria-label="addIcon"
                className={classes.AddShoppingCartIcon}
                onClick={() => toggleAdd(articulo)}
              >
                <AddShoppingCartIcon />
              </IconButton>
            )}
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

import React, { Component } from "react";
import {
  Row,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  CustomInput,
  UncontrolledCollapse,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";

class ctzSegura extends Component {
  constructor(props) {
    super(props);
  }
  SelectedOnceCTZ() {
    sessionStorage.SelectedOnceCTZ = "1";
  }
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          size="md"
          className="theme-color-blueHeader2 modalInfo"
        >
          <ModalBody style={{ padding: "0rem" }}>
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="fontTitle text-center pt-2 pb-2 arial theme-color-blue"
            >
              <div>
                {" "}
                <i className="icon-escudo" />
              </div>
              <div>Preguntas Frecuentes</div>
            </Colxx>
            <Colxx xxs="12" md="12" sm="12" className="text-justify  ">
              <ListGroup className="m-2">
                <ListGroupItem className="p-2" action>
                  <div id="toggler12" className="CommonQuestStyle">
                    ¿COMO SE QUE ESTO NO ES UNA ESTAFA?
                  </div>

                  <UncontrolledCollapse toggler="#toggler12">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      1. En el listado de productos en stock y productos en
                      tránsito están los nombres y números de teléfono de todos
                      los importadores que trabajan con nosotros, sientase libre
                      de llamar a cualquiera y pedir referencia sobre nuestra
                      empresa.{" "}
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      2. Puede dirigirse a nuestras oficinas y reunirse con{" "}
                      <a
                        rel="noopener noreferrer"
                        href="https://www.pic-cargo.com/es/contact/"
                        target="_blank"
                      >
                        {" "}
                        <span style={{ fontWeight: "bold" }}>nosotros</span>
                      </a>{" "}
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      3. Revisar los videos en youtube donde hablamos sobre
                      todos los temas logisticos y de aduana:
                      <a
                        rel="noopener noreferrer"
                        href="https://www.youtube.com/channel/UCaAprzcpP0PrafQzMDfJ2uA "
                        target="_blank"
                      >
                        <span style={{ fontWeight: "bold" }}>
                          {" "}
                          Link al canal
                        </span>
                      </a>
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler11" className="CommonQuestStyle">
                    ¿PUEDO ESCOGER EL PRODUCTO O TENGO QUE COMPRAR LO QUE
                    USTEDES TENGAN EN LAS LISTAS?
                  </div>
                  <UncontrolledCollapse toggler="#toggler11">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Precisamente lo que nos diferencia de la competencia que
                      nuestros importadores pueden sugerir los productos que se
                      van a comprar, estos se someten a votación en el grupo y
                      si hay un consenso se importa.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler" className="CommonQuestStyle">
                    ¿CUANTO ES EL MÍNIMO DE LA INVERSIÓN Y QUE INCLUYE?{" "}
                  </div>

                  <UncontrolledCollapse toggler="#toggler">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      En la mayoría de los casos el mínimo es de USD 2.000,
                      aunque dependiendo del producto existen montos superiores.
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      <span style={{ fontWeight: "bold" }}>
                        {" "}
                        La inversión incluye:{" "}
                      </span>{" "}
                      la compra del producto, logística internacional y aduana
                      de importación e impuestos.
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      <span style={{ fontWeight: "bold" }}> NOTA: </span>{" "}
                      algunas veces la aduana realiza lo que se llama ajuste de
                      valor y puede variar los impuestos estimados, para esto se
                      enviará un soporte respaldado por la misma aduana.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler2" className="CommonQuestStyle">
                    ¿CÓMO OBTENGO LOS PRECIOS Y LAS NOVEDADES QUE VAN A
                    IMPORTAR?{" "}
                    <span className="color-red">(aplica para Perú) </span>{" "}
                  </div>

                  <UncontrolledCollapse toggler="#toggler2">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Se debe inscribir en los grupos cerrados, aquí los
                      participantes hacen propuestas de los productos, análisis
                      de mercado y factibilidad del negocio.{" "}
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      (El precio de inscripción para Perú es de 100 soles hasta
                      el 30 de octubre y de 200 soles a partir de noviembre){" "}
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler3" className="CommonQuestStyle">
                    ¿PORQUE PIDEN UNA INSCRIPCIÓN PARA DAR PRECIOS?{" "}
                    <span className="color-red"> (aplica para Perú) </span>
                  </div>

                  <UncontrolledCollapse toggler="#toggler3">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Lo hacemos con la finalidad de resguardar la información
                      de nuestros costos de la competencia.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler4" className="CommonQuestStyle">
                    ¿QUE SUCEDE CON EL DINERO QUE DOY EN LA INSCRIPCIÓN?{" "}
                    <span className="color-red"> (aplica para Perú) </span>
                  </div>

                  <UncontrolledCollapse toggler="#toggler4">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Se usa para reducir el costo de tu primera importación, y
                      si no compras tendrás acceso a otros grupos con otros
                      productos.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler5" className="CommonQuestStyle">
                    ¿CUÁNDO DEBO REALIZAR EL PAGO DE LA INVERSIÓN?{" "}
                  </div>

                  <UncontrolledCollapse toggler="#toggler5">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Está dividida en 2 o 3 partes de acuerdo con las
                      condiciones del envío, fabricación y tiempos de llegada de
                      la mercancía.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler6" className="CommonQuestStyle">
                    ¿DEBO TENER RUC/RIF O CONSTITUIDA UNA EMPRESA PARA IMPORTAR?
                  </div>

                  <UncontrolledCollapse toggler="#toggler6">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      No es necesario porque nosotros PIC-CARGO.COM, nos
                      encargamos de apoyar al importador siendo los responsables
                      legales y poniendo nuestro nombre y una vez que la
                      mercancía arriba al país la separamos y entregamos a cada
                      uno de los participantes.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler7" className="CommonQuestStyle">
                    ¿A NOMBRE DE QUIEN VIENE LA IMPORTACIÓN Y QUIEN ES EL
                    RESPONSABLE?
                  </div>

                  <UncontrolledCollapse toggler="#toggler7">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      La importación viene a nombre de PIC-CARGO.COM y somos los
                      únicos responsables de todo el proceso.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler8" className="CommonQuestStyle">
                    ¿USTEDES HACEN LA ENTREGA EN MÍ PROVINCIA, REGIÓN O ESTADO?
                  </div>

                  <UncontrolledCollapse toggler="#toggler8">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Sí, estas se hacen a través de una agencia interprovincial
                      o internas y con un previo acuerdo con nosotros.
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler9" className="CommonQuestStyle">
                    ¿CUÁL ES EL TIEMPO DE ENTREGA?
                  </div>

                  <UncontrolledCollapse toggler="#toggler9">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Los tiempos de entrega dependerán de varios factores,
                      algunos de estos son:
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      1.- Condiciones de entrega del producto por parte del
                      fabricante. Es decir, si hay productos en stock la entrega
                      y envío es inmediato, si no hay productos en stock la
                      fábrica se demora un tiempo en producir (puede ser de 7,
                      15 o 30 días)
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      2.- El tiempo de entrega también dependerá del tipo de
                      envío (marítimo o aéreo)
                      https://www.pic-cargo.com/es/noticias/flete-aereo-o-flete-maritimo/
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "1rem" }}>
                      3.- Otros de los factores que influyen va a ser el país de
                      cual importemos la mercancía (estamos trabajando
                      principalmente con USA, China o Panamá y estos tardan
                      entre 30 a 45)
                    </div>
                    <div
                      style={{
                        textIndent: "40px",
                        marginBottom: "0.5rem",
                        textAlign: "center",
                        fontWeight: "bold",
                      }}
                    >
                      Para los que no me conocen mi nombre es Carlos Ramírez
                      fundador y CEO de la empresa PIC CARGO y somos los
                      organizadores de la ASOCIACIÓN DE IMPORTADORES GRUPALES
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
                <ListGroupItem className="p-2" action>
                  <div id="toggler10" className="CommonQuestStyle">
                    ¿QUIÉNES SOMOS?
                  </div>

                  <UncontrolledCollapse toggler="#toggler10">
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      En PIC-CARGO.COM prestamos el servicio de agencias de
                      aduanas o corredor de aduanas, transporte y logística
                      internacional desde distintos orígenes como China, Estados
                      Unidos y países de Europa a América latina, tenemos
                      oficinas propias en Perú, Panamá y Venezuela.
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Dirección de nuestras oficinas:
                    </div>
                    <div style={{ marginBottom: "0.5rem", fontWeight: "bold" }}>
                      Lima:
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Av. Jose Pardo, Cuadra 1 y Calle Martir Olaya, Edif.
                      Empresarial Pardo 129, ofic. 1005, Miraflores, Lima, Perú.
                    </div>
                    <div style={{ marginBottom: "0.5rem" }}>
                      <ul>
                        <li>
                          Correos electrónicos:
                          <span style={{ fontWeight: "bold" }}>
                              asesoramiento@pic-cargo.com
                          </span>{" "}
                        </li>
                        <li>Números telefónicos: +511-326-9130</li>
                        <li>Whatsapp: +51 972 530 303</li>
                      </ul>
                    </div>
                    <div style={{ marginBottom: "0.5rem", fontWeight: "bold" }}>
                      Panamá:
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Ave. Ricardo J. Alfaro, Edif. Century Tower, piso 8, ofic.
                      811, Ciudad de Panamá.
                    </div>
                    <div style={{ marginBottom: "0.5rem" }}>
                      <ul>
                        <li>
                          Correos electrónicos:
                          <span style={{ fontWeight: "bold" }}>
                            asesoramientopa@pic-cargo.com
                          </span>{" "}
                        </li>
                        <li>Números telefónicos: +507-393 5412 / 5416</li>
                        <li>Whatsapp: +507-699-72126</li>
                      </ul>
                    </div>
                    <div style={{ marginBottom: "0.5rem", fontWeight: "bold" }}>
                      VENEZUELA:
                    </div>
                    <div style={{ textIndent: "40px", marginBottom: "0.5rem" }}>
                      Av. Francisco de Solano, Edif. Banvenez, piso 14, ofic.
                      CD, Ciudad de Caracas, CP: 1050, Distrito Capital,
                      Venezuela.
                    </div>
                    <div style={{ marginBottom: "0.5rem" }}>
                      <ul>
                        <li>
                          Correos electrónicos:
                          <span style={{ fontWeight: "bold" }}>
                            asesoramientove@pic-cargo.com
                          </span>{" "}
                        </li>
                        <li>
                          Números telefónicos: (+58212) 763-2240 / 762-6178
                        </li>
                        <li>Whatsapp: +58 424115-3525</li>
                      </ul>
                    </div>
                  </UncontrolledCollapse>
                </ListGroupItem>
              </ListGroup>
            </Colxx>

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="m-3"
              style={{ textAlign: "center" }}
            >
              <Button
                color="success"
                onClick={this.props.toggle}
                className="btn-style-blue"
                size="sm"
              >
                Volver
              </Button>
            </Colxx>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default ctzSegura;

import React, { Component } from "react";
import {
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Collapse,
  Button,
  ListGroup,
  ListGroupItem,
  Table,
} from "reactstrap";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import ReactPlayer from "react-player/lazy";
class ProductDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var item = this.props.item;

    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          size="md"
          className="theme-color-blueHeader2 modalInfo"
        >
          <ModalBody
            className="theme-color-blueHeader2"
            style={{ padding: "0rem" }}
          >
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />

            <Row className="mx-auto m-2 p-2">
              <Col
                 xs="12"
                 md="12"
                 lg="10"
                 sm="12"
                className="font2 text-center mx-auto m-2"
              >
                PRODUCTOS ACTUALES
              </Col>

              <Col xs="12" md="12" className="mb-2">
                <Card
                  className="p-1"
                  style={{
                    alignItems: "center",
                  }}
                >
                  <Carousel
                    infiniteLoop={true}
                    autoPlay={false}
                    showThumbs={false}
                    showIndicators={false}
                    stopOnHover={true}
                    showStatus={false}
                    className="carrouselWidth"
                  >
                    {item[0].Multimedia[0].imagen.map((picture, index) => (
                      <div>
                        <img
                          key={index + "img"}
                          src={picture.img}
                          className="imgDetails p-2"
                          alt="logo"
                        />
                      </div>
                    ))}

                    {item[0].Multimedia[1]
                      ? item[0].Multimedia[1].video.map((picture, index) => (
                          <div>
                            <ReactPlayer
                              url={picture.img}
                              className="react-player imgDetails"
                              key={index + "video"}
                              id={"my-video" + index}
                              width="100%"
                              height="100%"
                              controls
                            />
                          </div>
                        ))
                      : ""}
                  </Carousel>
              
                  <CardBody
                    style={{
                      padding: "1rem 0 1rem 0",
                      width: "100%",
                      textAlign: "center",
                    }}
                  >
                    <CardTitle style={{ fontWeight: "bold" }}>
                      Producto: {item[0].NombreProducto}
                    </CardTitle>
                    <CardSubtitle style={{ fontWeight: "bold" }}>
                      Especificaciones:
                    </CardSubtitle>
                    <CardText>
                      <Table>
                        <tbody>
                          {item[0].Caracteristicas.map((item) => (
                            <tr className="p-0 text-center" key={item.key}>
                              <th
                                scope="row"
                                style={{
                                  fontWeight: "bold",
                                  backgroundColor: "#80808038",
                                }}
                              >
                                {item.nombre}
                              </th>
                              <td>{item.descripción}</td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </CardText>
                    <Button
                      className="btn-style m-1 btn btn-success btn-sm"
                      onClick={this.props.toggle}
                    >
                      Volver
                    </Button>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default ProductDetails;

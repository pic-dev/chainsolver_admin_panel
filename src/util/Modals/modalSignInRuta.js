import React, { Component, createRef } from "react";
import {
  Button,
  Row,
  FormGroup,
  Modal,
  ModalBody,
  Form,
  Input,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";
import {
  firebaseConf,
  googleAuthProvider,
  facebookAuthProvider,
  persistencia,
} from "../Firebase";
import { errores, ErrorEspa, telefonoValid } from "../Utils";
import Errores from "../Errores";
import { Event } from "../tracking";

const ref = createRef();


class signIn extends Component {
  constructor(props) {
    super(props);
    this.socialLogin = this.socialLogin.bind(this);
    this.correoClave = this.correoClave.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.setsignup = this.setsignup.bind(this);
    this.seterror = this.seterror.bind(this);
    this.invitado = this.invitado.bind(this);
    this.redirect = this.redirect.bind(this);
    this.inicio = this.inicio.bind(this);
    this.state = {
      Signup: false,
      signInChoose: false,
      Choose: true,
      agotadoChoose: false,
      error: ""
    };
  }

  componentDidMount() {
    this.inicio();
  }

  inicio = () => {
    var empty = [];
    var vacio = empty.map(function (item) {
      return item;
    });
    localStorage.distrito = vacio;
    localStorage.regulador = vacio;
    localStorage.tipoMercancia = vacio;
    localStorage.distritoN = vacio;
    localStorage.ProvinciaN = vacio;
    localStorage.monto = vacio;
    localStorage.value4 = false;
    localStorage.value5 = false;
    localStorage.value6 = false;
    localStorage.value7 = false;
    localStorage.value8 = false;
    localStorage.seguro = true;

    localStorage.check3 = false;
    localStorage.check = false;
    localStorage.check2 = false;
    localStorage.check1 = false;
    localStorage.check4 = true;

    /*switch (localStorage.tipocon) {
      case "Contenedor completo":
        Event(
          "2.7.1 Calcular Cotizacion FCL Directo",
          "2.7.1 Calcular Cotizacion FCL Directo",
          "SERVICIOS"
        );
        break;
      case "Contenedor compartido":
        Event(
          "1.8.1 Calcular Cotizacion LCL Directo",
          "1.8.1 Calcular Cotizacion LCL Directo",
          "SERVICIOS"
        );
        break;
      case "Aereo":
        Event(
          "3.8.1 Calcular Cotizacion Aereo Directo",
          "3.8.1 Calcular Cotizacion Aereo Directo",
          "SERVICIOS"
        );
        break;

      default:
    }*/

    //orifginal
    let data = JSON.parse(sessionStorage.getItem("user"));

    if (sessionStorage.usuario == "1" || data[1] == "Invitado") {
      if (sessionStorage.agotado == undefined) {
        sessionStorage.agotado = "1";
      } else if (sessionStorage.agotado == "1") {
        sessionStorage.agotado = "2";
      } else if (sessionStorage.agotado == "2") {
        sessionStorage.agotado = "3";
      } else {
        sessionStorage.agotado = "4";
      }
      localStorage.validLogin = "1";
      if (sessionStorage.usuario == "2" && sessionStorage.agotado !== "4") {
        this.redirect();
      }
    } else {
      this.redirect();
    }
  };
  redirect() {
    if (this.props.bandera) {
      this.props.redirect();
    }
  }
  setsignup(e) {
    this.setState(() => ({
      signup: e,
    }));
  }
  setsignIn(e) {
    this.setState(() => ({
      signIn: e,
    }));
  }
  seterror(e) {
    this.setState(() => ({
      error: e,
    }));
  }
  invitado = () => {
    //Event("Ingresar como invitado", "Ingresar como invitado", "SERVICIOS");
    if (sessionStorage.usuario == undefined || sessionStorage.usuario !== "2") {
      let user = [
        ["PIC"],
        ["Invitado"],
        ["Invitado"],
        ["/assets/img/profile-pic-l.png"],
      ];
      sessionStorage.setItem("user", JSON.stringify(user));
    }
    sessionStorage.usuario = "2";
    this.props.toggle;
    this.redirect();
    window.location.reload();
  };
  correoClave = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { usuario, clave } = e.target.elements;
    if (usuario.value.length > 0 && clave.value.length > 0) {
      await firebaseConf
        .auth()
        .signInWithEmailAndPassword(usuario.value, clave.value)
        .then((result) => {
          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));
          this.props.toggle;
          timer = setTimeout(() => {
            window.location.reload();
            this.redirect();
          }, 1000);
        })
        .catch((error) => {
          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));
          this.props.toggle;
          timer = setTimeout(() => {
            window.location.reload();
            this.redirect();
          }, 1000);

          errores("Logincorreo", error);
        });
    } else {
      this.seterror("Debe llenar todos los campos requeridos");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    }
  };
  handleSignUp = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { telefono, usuario, clave, Confirmacion } = e.target.elements;

    if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value === Confirmacion.value
    ) {
      await firebaseConf
        .auth()
        .createUserWithEmailAndPassword(usuario.value, clave.value)
        .then((result) => {
          if (telefono.value.length > 0) {
            if (telefonoValid(telefono.value)) {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              this.props.toggle;
              localStorage.telefono = telefono.value;

              timer = setTimeout(() => {
                window.location.reload();
                this.redirect();
              }, 1000);
            } else {
              var alerta = "Verifique numero de telefono";
              alert(alerta);
            }
          } else {
            sessionStorage.usuario = "2";
            let user = [
              ["PIC"],
              [usuario.value],
              [usuario.value],
              ["/assets/img/profile-pic-l.png"],
            ];
            sessionStorage.setItem("user", JSON.stringify(user));
            this.props.toggle;
            timer = setTimeout(() => {
              window.location.reload();
              this.redirect();
            }, 1000);
          }
        })
        .catch((error) => {
          if (telefono.value.length > 0) {
            if (telefono.value.length > 0 && telefonoValid(telefono.value)) {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));

              this.props.toggle;
              localStorage.telefono = telefono.value;

              timer = setTimeout(() => {
                window.location.reload();
                this.redirect();
              }, 1000);
            } else {
              alerta = "Verifique numero de telefono";
              alert(alerta);
            }
          } else {
            sessionStorage.usuario = "2";
            let user = [
              ["PIC"],
              [usuario.value],
              [usuario.value],
              ["/assets/img/profile-pic-l.png"],
            ];
            sessionStorage.setItem("user", JSON.stringify(user));

            this.props.toggle;
            timer = setTimeout(() => {
              window.location.reload();
              this.redirect();
            }, 1000);
          }

          errores("RegistroCorreo", error);
          this.seterror(ErrorEspa(error));
        });
    } else if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value !== Confirmacion.value
    ) {
      this.seterror("La contraseña ingresada no coincide con la confimación");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    } else {
      this.seterror("Debe llenar todos los campos requeridos");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    }
  };
  socialLogin = (provider,e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    firebaseConf
      .auth()
      .setPersistence(persistencia)
      .then(() => {
        (async () => {
          await firebaseConf
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
              /*Event(
                "GoogleLoginHeader",
                "Usuario ingresó con botton de google",
                "SERVICIOS"
              );*/
              let user = [
                [result.additionalUserInfo.profile.id],
                [result.additionalUserInfo.profile.name],
                [result.additionalUserInfo.profile.email],
                [result.additionalUserInfo.profile.picture],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              sessionStorage.usuario = "2";

              this.redirect();
              window.location.reload();
            })
            .catch((error) => {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                ["Invitado"],
                ["Invitado"],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));

              this.redirect();
              window.location.reload();
            });
        })();
      })
      .catch((error) => {});
  };

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={false}
          size="md"
          className="modalConfirmatinoWidth"
          style={{ borderRadius: "1.1rem", marginTop: "65px !important" }}
        >
          {sessionStorage.agotado !== "4" ? (
            <ModalBody style={{ padding: "0rem" }}>
              {!this.state.signInChoose ? (
                <Row style={{ padding: "0rem" }}>
                  <i
                    onClick={this.props.toggle}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡IMPORTANTE!</span>
                    </div>
                  </Colxx>

                  <Row className="p-2 text-center">
                    <Colxx xs="12">
                      <Form className=" login-form">
                        <div style={{ fontSize: "1.3rem" }}>
                          <FormGroup
                            style={{ fontWeight: "bold" }}
                            className="mt-2 mb-1"
                          >
                            <div>
                              <i className="icon-list" />
                              <span>
                                <span className="color-blue">Para&nbsp;</span>
                                <span className="color-green">
                                  formalizar&nbsp;
                                </span>
                                <span className="color-blue">
                                  tu oferta&nbsp;
                                </span>
                                <span className="color-orange">
                                  REGÍSTRATE&nbsp;
                                </span>
                              </span>
                            </div>
                          </FormGroup>
                          <hr className="ten" />
                          <FormGroup className="mt-1 mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Sin Formularios
                              </span>
                            </div>
                          </FormGroup>
                          <FormGroup className=" mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Sin TDC ni pagos
                              </span>
                            </div>
                          </FormGroup>
                          <FormGroup className=" mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Solo con tu email
                              </span>
                            </div>
                          </FormGroup>
                          <hr className="ten" />
                          <Colxx
                            xxs="12"
                            md="12"
                            sm="12"
                            className="p-2"
                            style={{ textAlign: "center" }}
                          >
                            <Button
                              color="success"
                              onClick={this.invitado}
                              className="btn-style-grey mr-1"
                              size="sm"
                            >
                              INGRESAR COMO INVITADO
                            </Button>
                            <Button
                              color="success"
                              onClick={() =>
                                this.setState(() => ({
                                  signInChoose: true,
                                }))
                              }
                              className="btn-style"
                              size="sm"
                            >
                              REGISTRARME
                            </Button>
                          </Colxx>
                          <hr className="ten" />
                          <FormGroup
                            style={{ fontSize: "0.9rem" }}
                            className="text-center mb-1"
                          >
                            Ya tengo una cuenta -&nbsp;
                            <span
                              className="arial"
                              onClick={() =>
                                this.setState(() => ({
                                  signInChoose: true,
                                }))
                              }
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </div>
                      </Form>
                    </Colxx>
                  </Row>
                </Row>
              ) : (
                <Row style={{ padding: "0rem" }}>
                  <i
                    onClick={this.props.toggle}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡INGRESA&nbsp;</span>
                      <span
                        className="bggradien-orange color-white"
                        style={{
                          fontWeight: "bold",
                          borderRadius: "5px",
                        }}
                      >
                        &nbsp;GRATIS!&nbsp;
                      </span>
                    </div>
                  </Colxx>

                  <Row className="mx-auto p-2">
                    <Colxx xs="12">
                      {this.state.signup ? (
                        <Form
                          className="login-form"
                          onSubmit={this.correoClave}
                        >
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                          <div
                            className="paddingLogin text-center"
                            style={{ textAling: "center" }}
                          >
                            <FormGroup className="mb-2 font2 color-blue">
                              <span>Iniciar sesión</span>
                            </FormGroup>
                     
                            <FormGroup className="mb-2">
                              <Button
                                className="GLogin "
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider,e)
                                }
                              >
                                <i className="icon-google" />
                                Ingresa con Google
                              </Button>
                            </FormGroup>
                            <FormGroup className="mb-2 arial">
                              <span>-o-</span>
                            </FormGroup>
                            <FormGroup>
                              <Input
                                type="email"
                                name="usuario"
                                placeholder="Correo electrónico"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Input
                                name="clave"
                                type="password"
                                placeholder="Clave"
                              />
                            </FormGroup>
                            <FormGroup className="login-form-button">
                              <Button
                                type="submit"
                                className="btn-style-blue btn btn-success btn-sm"
                              >
                                Iniciar sesión
                              </Button>
                              &nbsp; O &nbsp;
                              <span
                                className="arial"
                                onClick={() => {
                                  this.setsignup(false);
                                }}
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  color: "rgb(54, 94, 153)",
                                  textDecoration: "underline",
                                }}
                              >
                                Registrate
                              </span>
                            </FormGroup>
                          </div>
                        </Form>
                      ) : (
                        <Form
                          className="paddingLogin text-center"
                          onSubmit={this.handleSignUp}
                        >
                          {this.state.Choose ? (
                            <div>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2  "
                              >
                                <span className="color-blue">INGRESA </span>{" "}
                                <span
                                className="color-black"
                                >
                                  TUS DATOS
                                </span>
                              </FormGroup>
                              {this.state.error ? (
                                <FormGroup>
                                  <Errores mensaje={this.state.error} />
                                </FormGroup>
                              ) : null}
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="arial color-orange ">
                                  PASO 1 -{" "}
                                </span>
                                <span>Número de teléfono -</span>{" "}
                                <span className="color-grey">(Opcional)</span>
                              </FormGroup>
                              <FormGroup className="mb-3">
                                <Input
                                  type="text"
                                  name="telefono"
                                  placeholder="Ingrese aqui su número de telefono"
                                />
                              </FormGroup>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="arial color-orange">
                                  PASO 2 -{" "}
                                </span>
                                <span>Correo electrónico</span>
                              </FormGroup>
                             
                              <FormGroup className="mb-2">
                                <Button
                                  style={{
                                    borderRadius: "13px",
                                    fontSize: "14px",
                                  }}
                                  className="GLogin"
                                  onClick={(e) =>
                                    this.socialLogin(googleAuthProvider,e)
                                  }
                                >
                                  <i className="icon-gmail" />
                                  <span>Gmail,&nbsp;</span>{" "}
                                  <span
                                    className="color-orange"
                                    style={{
                                      fontWeight: "bold",
                                      cursor: "pointer",
                                      textDecoration: "underline",
                                    }}
                                  >
                                    solo con un click
                                  </span>
                                </Button>
                              </FormGroup>
                              <FormGroup className="mb-2">
                                <span>o con otro </span>
                                <span
                                  onClick={() =>
                                    this.setState(() => ({
                                      Choose: false,
                                    }))
                                  }
                                  className="color-blue"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  correo electrónico
                                </span>
                              </FormGroup>
                              <hr className="ten" />
                              <FormGroup className="mt-2 mb-2">
                                &nbsp; Ya tengo una cuenta - &nbsp;
                                <span
                                  onClick={() => {
                                    this.setsignup(true);
                                  }}
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  INICIAR SESIÓN
                                </span>
                              </FormGroup>
                            </div>
                          ) : (
                            <div>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2 font2 "
                              >
                                <span className="color-blue">INGRESA </span>{" "}
                                <span
                                    className="color-black"
                                >
                                  {" "}
                                  TUS DATOS
                                </span>
                              </FormGroup>
                              {this.state.error ? (
                                <FormGroup>
                                  <Errores mensaje={this.state.error} />
                                </FormGroup>
                              ) : null}
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="color-orange ">PASO 1 - </span>
                                <span>Número de teléfono -</span>{" "}
                                <span className="color-grey">(Opcional)</span>
                              </FormGroup>
                              <FormGroup className="mb-3">
                                <Input
                                  type="text"
                                  name="telefono"
                                  placeholder="Ingrese aqui su número de telefono"
                                />
                              </FormGroup>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2"
                              >
                                <span className="color-orange">PASO 2 - </span>
                                <span>Correo electrónico</span>
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  type="email"
                                  name="usuario"
                                  placeholder="Correo electrónico"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  name="clave"
                                  type="password"
                                  placeholder="Clave"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  name="Confirmacion"
                                  type="password"
                                  placeholder="Confirma tu Clave"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Button
                                  type="submit"
                                  className="btn-style-blue btn btn-success btn-sm"
                                >
                                  Registrate
                                </Button>
                              </FormGroup>
                              <hr className="ten" />
                              <FormGroup className="mt-2 mb-2">
                                &nbsp; Ya tengo una cuenta - &nbsp;
                                <span
                                  onClick={() => {
                                    this.setsignup(true);
                                  }}
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  INICIAR SESIÓN
                                </span>
                              </FormGroup>
                            </div>
                          )}
                        </Form>
                      )}
                    </Colxx>
                  </Row>
                </Row>
              )}
            </ModalBody>
          ) : (
            <ModalBody style={{ padding: "0rem" }}>
              {!this.state.agotadoChoose ? (
                <div style={{ padding: "0rem" }}>
                  <i
                    onClick={this.props.toggle}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <div>
                        Has agotado el máximo de consultas como invitado
                      </div>
                    </div>
                  </Colxx>

                  <Row className="p-2 text-center">
                    <Colxx xs="12">
                      <Form className=" login-form">
                        <div style={{ fontSize: "0.9rem" }}>
                          <FormGroup className="mt-1 mb-2">
                            <div className="mb-3">
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Opción 1 -
                              </span>{" "}
                              Consultas ilimitadas Registrate{" "}
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                {" "}
                                GRATIS
                              </span>
                            </div>
                            <div className="mb-1">
                              <span
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                Nombre de empresa
                              </span>{" "}
                              - (Opcional)
                            </div>
                            <div className="mb-1">
                              <Input
                                type="text"
                                name="Empresa "
                                placeholder="Ingrese aqui el nommbre de su empresa"
                              />
                            </div>
                            <div className="mb-1">
                              <span
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                Número de teléfono
                              </span>{" "}
                              - (Opcional)
                            </div>
                            <div className="mb-2">
                              <Input
                                type="text"
                                name="telefono "
                                placeholder="Ingrese aqui su número de telefono"
                              />
                            </div>
                          

                            <div>
                              <Button
                                style={{
                                  borderRadius: "13px",
                                  fontSize: "14px",
                                }}
                                className="GLogin"
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider,e)
                                }
                              >
                                <i className="icon-gmail" />
                                <span>Gmail,&nbsp;</span>{" "}
                                <span
                                  className="color-orange"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  solo con un click
                                </span>
                              </Button>
                            </div>

                            <div className="text-center mt-2 mb-2">
                              <span style={{ fontSize: "0.9rem" }}>
                                {" "}
                                o con otro&nbsp;
                                <span
                                  className="arial"
                                  onClick={() =>
                                    this.setState(() => ({
                                      agotadoChoose: true,
                                    }))
                                  }
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  correo electrónico
                                </span>
                              </span>
                            </div>
                          </FormGroup>

                          <FormGroup className="mb-2">
                            <div className="mb-3">
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Opción 2 -&nbsp;
                              </span>
                              <span>
                                Regresa en 15 días y goza de 3 nuevas consultas
                                sin registro.
                              </span>
                            </div>
                          </FormGroup>

                          <hr className="ten" />
                          <FormGroup
                            style={{ fontSize: "0.9rem" }}
                            className="text-center m-3"
                          >
                            Ya tengo una cuenta -&nbsp;
                            <span
                              className="arial"
                              onClick={() =>
                                this.setState(() => ({
                                  agotadoChoose: true,
                                }))
                              }
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </div>
                      </Form>
                    </Colxx>
                  </Row>
                </div>
              ) : (
                <div style={{ padding: "0rem" }}>
                  <i
                    onClick={this.props.toggle}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡INGRESA&nbsp;</span>
                      <span
                        className="bggradien-orange color-white"
                        style={{
                          fontWeight: "bold",
                          borderRadius: "5px",
                        }}
                      >
                        &nbsp;GRATIS!&nbsp;
                      </span>
                    </div>
                  </Colxx>

                  <Row className="p-2">
                    <Colxx xs="12">
                      <hr className="ten" />
                      {!this.state.signup ? (
                        <Form
                          className=" login-form"
                          onSubmit={this.correoClave}
                        >
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                          <div
                            className="paddingLogin text-center"
                            style={{ textAling: "center" }}
                          >
                            <FormGroup className="mb-2 arial">
                              <span>Iniciar sesión</span>
                            </FormGroup>
                         
                            <FormGroup className="mb-2">
                              <Button
                                style={{
                                  borderRadius: "13px",
                                  fontSize: "14px",
                                }}
                                className="GLogin"
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider,e)
                                }
                              >
                                <i className="icon-gmail" />
                                <span>Gmail,&nbsp;</span>{" "}
                                <span
                                  className="color-orange"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  solo con un click
                                </span>
                              </Button>
                            </FormGroup>
                            <FormGroup className="mb-2 arial">
                              <span>-o-</span>
                            </FormGroup>
                            <FormGroup>
                              <Input
                                type="email"
                                name="usuario"
                                placeholder="Correo electrónico"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Input
                                name="clave"
                                type="password"
                                placeholder="Clave"
                              />
                            </FormGroup>
                            <FormGroup className="login-form-button">
                              <Button
                                type="submit"
                                className="btn-style-blue btn btn-success btn-sm"
                              >
                                Iniciar sesión
                              </Button>
                              &nbsp; O &nbsp;
                              <span
                                className="arial"
                                onClick={() => {
                                  this.setsignup(true);
                                }}
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  color: "rgb(54, 94, 153)",
                                  textDecoration: "underline",
                                }}
                              >
                                Registrate
                              </span>
                            </FormGroup>
                          </div>
                        </Form>
                      ) : (
                        <Form
                          className="paddingLogin text-center"
                          onSubmit={this.handleSignUp}
                        >
                          <FormGroup className="mb-2 arial">
                            <span>Registro</span>
                          </FormGroup>
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                        
                          <FormGroup className="mb-2">
                            <Button
                              style={{
                                borderRadius: "13px",
                                fontSize: "14px",
                              }}
                              className="GLogin"
                              onClick={(e) =>
                                this.socialLogin(googleAuthProvider,e)
                              }
                            >
                              <i className="icon-gmail" />
                              <span>Gmail,&nbsp;</span>{" "}
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  textDecoration: "underline",
                                }}
                              >
                                solo con un click
                              </span>
                            </Button>
                          </FormGroup>
                          <FormGroup className="mb-2 arial">
                            <span>-o-</span>
                          </FormGroup>
                          <FormGroup>
                            <Input
                              type="email"
                              name="usuario"
                              placeholder="Correo electrónico"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Input
                              name="clave"
                              type="password"
                              placeholder="Clave"
                            />
                          </FormGroup>

                          <FormGroup>
                            <Input
                              name="Confirmacion"
                              type="password"
                              placeholder="Confirma tu Clave"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Button
                              type="submit"
                              className="btn-style-blue btn btn-success btn-sm"
                            >
                              Registrate
                            </Button>
                            &nbsp; O &nbsp;
                            <span
                              onClick={() => {
                                this.setsignup(false);
                              }}
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </Form>
                      )}
                    </Colxx>

                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      className="p-1"
                      style={{ textAlign: "center" }}
                    >
                      <hr className="ten" />
                      <Button
                        color="success"
                        onClick={this.props.toggle}
                        className="btn-style"
                        size="sm"
                      >
                        {" "}
                        VOLVER
                      </Button>
                    </Colxx>
                  </Row>
                </div>
              )}
            </ModalBody>
          )}
        </Modal>
      </div>
    );
  }
}

export default signIn;

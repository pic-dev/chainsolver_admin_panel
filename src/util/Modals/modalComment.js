import React, { Component } from "react";
import {
  Input,
  Button,
  Modal,
  ModalBody,
  Form,
  Row,
  ListGroup,
  ListGroupItem,
  CustomInput,
  FormGroup,
  Label,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import {firebaseConf} from "../Firebase";
import { getCurrentTime, getDateWithFormat, errores } from "../Utils";
let timer;
class comment extends Component {
  constructor(props) {
    super(props);
    this.SelectedOnceComment = this.SelectedOnceComment.bind(this);
    this.sendCommentary = this.sendCommentary.bind(this);
    this.handleFormState = this.handleFormState.bind(this);
    this.state = {
      showMessage: false,
      commentEmpty: false,
      Commentform: [
        { Label: "paso1", value: "no problema" },
        { Label: "paso2", value: "no problema" },
        { Label: "paso3", value: "no problema" },
        { Label: "paso4", value: "no problema" },
        { Label: "recomendarias", value: "" },
        { Label: "inversion", value: "" },
        { Label: "adicional", value: "sin comentarios" },
        { Label: "costo", value: "" },
      ],
    };
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  SelectedOnceComment = () => {
    sessionStorage.CommentNone = "1";
  }
  sendCommentary() {
    if (
      (this.state.Commentform[0].value.length > 0 ||
        this.state.Commentform[1].value.length > 0 ||
        this.state.Commentform[2].value.length > 0 ||
        this.state.Commentform[3].value.length > 0) &&
      this.state.Commentform[4].value.length > 0 &&
      this.state.Commentform[5].value.length > 0
    ) {
      let data = JSON.parse(sessionStorage.getItem("user"));
      const params = {
        nombre: data[1].toString(),
        email: data[2].toString(),
        fecha: getDateWithFormat() + " " + getCurrentTime(),
        pais: sessionStorage.country,
        paso1: this.state.Commentform[0].value,
        paso2: this.state.Commentform[1].value,
        paso3: this.state.Commentform[2].value,
        paso4: this.state.Commentform[3].value,
        recomendarias: this.state.Commentform[4].value,
        inversion: this.state.Commentform[5].value,
        adicional: this.state.Commentform[6].value,
        costo: this.state.Commentform[7].value,
      };

      firebaseConf
        .database()
        .ref("estadisticas")
        .push(params)
        .then(() => {
          this.props.toggle();
          this.setState((ps) => ({ showMessage: true }));
          timer = setTimeout(() => {
            this.setState((ps) => ({ showMessage: false }));
          }, 4000);
        })
        .catch((err) => {
          this.props.toggle();
          this.setState((ps) => ({ showMessage: true }));
          timer = setTimeout(() => {
            this.setState((ps) => ({ showMessage: false }));
          }, 4000);
          errores("EnviarComentario_HEADER", error);
        });
    } else {
      this.setState((ps) => ({ commentEmpty: true }));

      timer = setTimeout(() => {
        this.setState((ps) => ({ commentEmpty: false }));
      }, 4000);
    }
  }
  handleFormState(input) {
    let newState = this.state.Commentform;
    newState[input.id].value = input.value;
    this.setState({
      Commentform: newState,
    });
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={true}
          className="survey"
        >
          <ModalBody style={{ padding: "0" }}>
            <Form>
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                }}
              />
              <Row>
                <Colxx
                  xxs="12"
                  md="12"
                  sm="12"
                  className="arial mx-auto"
                  style={{
                    textAlign: "center",
                    padding: "0",
                  }}
                >
                  <ListGroup>
                    <ListGroupItem
                    className="color-white"
                      style={{
                        textAlign: "center",
                        backgroundColor: "#0d5084",
                      }}
                    >
                      <Row>
                        <Colxx xxs="4" md="4" className="mt-3  arial mx-auto">
                          <i className="icon-regalo" />
                        </Colxx>
                        <Colxx
                          xxs="8"
                          md="8"
                          className="mt-3 arial surveyTitle mx-auto"
                        >
                          ¡AYÚDANOS A MEJORAR
                        </Colxx>
                      </Row>
                    </ListGroupItem>
                  </ListGroup>
                </Colxx>
                {this.state.commentEmpty ? (
                  <div className="alert alert-danger  arial  mx-auto" role="alert">
                    Favor completar la encuesta, tus comentario nos seran de
                    mucha ayuda para mejorar.
                  </div>
                ) : (
                  ``
                )}

<Colxx className="text-center mt-2 mb-2" xxs="12" md="12">
  <div>Ingrese numero de Contacto (Opcional)</div>
<Input className="text-center mt-1 mb-1"
                                        name="number"
                                        type="text"
                                        placeholder="numero de contacto"
                                      />
                                        <div>Ingrese nombre de Contacto (Opcional)</div>
                                      <Input
                                      className="text-center mt-1 mb-1"
                                        name="nombre"
                                        type="text"
                                        placeholder="nombre"
                                      />
                </Colxx>

                <Colxx className="text-center mt-2 mb-2" xxs="12" md="12">
                  <div
                    className="mb-2"
                    style={{
                      fontWeight: "bold",
                      fontSize: "1.2em",
                      textAlign: "left",
                    }}
                  >
                    1.-Indica el nivel de satisfacción con las tarifas del{" "}
                    <span className="color-blue">
                      SERVICIO LOGíSTICO SIN IMPUESTOS
                    </span>
                  </div>
                  <div>
                    <i
                      value="alta"
                      onClick={(e) => {
                        this.handleFormState({
                          id: 7,
                          value: "alta",
                        });
                      }}
                      className="icon-happy mr-3"
                    />
                    <i
                      value="regular"
                      onClick={(e) => {
                        this.handleFormState({
                          id: 7,
                          value: "regular",
                        });
                      }}
                      className="icon-neutral mr-3"
                    />
                    <i
                      value="excelente"
                      onClick={(e) => {
                        this.handleFormState({
                          id: 7,
                          value: "excelente",
                        });
                      }}
                      className="icon-sad"
                    />
                  </div>
                </Colxx>
                <Colxx xxs="12" md="12" sm="12" className="mt-2  ">
                  <FormGroup>
                    <Label for="exampleCheckbox">
                      <span
                        style={{
                          fontWeight: "bold",
                          fontSize: "1.2em",
                        }}
                      >
                        2.-Indicanos ¿en cual de los siguientes pasos presentó
                        alguna dificultad?
                      </span>
                    </Label>
                    <div>
                      <CustomInput
                        type="checkbox"
                        id="ruta"
                        label="Paso 1: Indica tú ruta y tipo de contenedor."
                        value="paso 1"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 0,
                            value: e.target.value || "",
                          });
                        }}
                      />
                      <CustomInput
                        type="checkbox"
                        id="paso2"
                        label="Paso 2: Uso de la calculadora"
                        value="Uso de la calculadora"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 1,
                            value: e.target.value || "",
                          });
                        }}
                      />
                      <CustomInput
                        type="checkbox"
                        id="paso3"
                        label="Paso 3: Agregar servicios adicionales"
                        value="Agregar servicios adicionales"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 2,
                            value: e.target.value || "",
                          });
                        }}
                      />
                      <CustomInput
                        type="checkbox"
                        id="paso4"
                        label="Paso 4: Entendimiento del presupuesto"
                        value="Entendimiento del presupuesto"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 3,
                            value: e.target.value || "",
                          });
                        }}
                      />
                    </div>
                  </FormGroup>
                </Colxx>
                <Colxx xxs="12" md="12" sm="12" className="mt-2  ">
                  <FormGroup>
                    <div>
                      {" "}
                      <span
                        style={{
                          fontWeight: "bold",
                          fontSize: "1.2em",
                        }}
                      >
                        3.-¿Recomendarias esta aplicación a otro usuario?
                      </span>{" "}
                      &nbsp;{" "}
                      <CustomInput
                        type="radio"
                        id="RecomendacionRadio"
                        name="RecomendacionRadio"
                        label="Si"
                        value="Si"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 4,
                            value: e.target.value || "",
                          });
                        }}
                        inline
                      />
                      <CustomInput
                        type="radio"
                        id="RecomendacionRadio2"
                        name="RecomendacionRadio"
                        label="No"
                        value="No"
                        onChange={(e) => {
                          this.handleFormState({
                            id: 4,
                            value: e.target.value || "",
                          });
                        }}
                        inline
                      />
                    </div>
                  </FormGroup>
                </Colxx>
                <Colxx xxs="12" md="12" sm="12" className="mt-2  ">
                  <Label for="exampleCheckbox">
                    <span
                      style={{
                        fontWeight: "bold",
                        fontSize: "1.2em",
                      }}
                    >
                      {" "}
                      4.-¿Cuánto estaría dispuesto a invertir por cada consulta?{" "}
                    </span>{" "}
                    <span
                      style={{
                        color: "gray",
                      }}
                    >
                      (Dólar americado)
                    </span>
                  </Label>

                  <FormGroup className="text-center ">
                    <div>
                      <CustomInput
                        type="radio"
                        id="invertirRadio"
                        name="invertirRadio"
                        value="1"
                        label="1 usd"
                        inline
                        onChange={(e) => {
                          this.handleFormState({
                            id: 5,
                            value: e.target.value || "",
                          });
                        }}
                      />
                      <CustomInput
                        type="radio"
                        id="invertirRadio2"
                        name="invertirRadio"
                        value="3"
                        label="3 usd"
                        inline
                        onChange={(e) => {
                          this.handleFormState({
                            id: 5,
                            value: e.target.value || "",
                          });
                        }}
                      />
                      <CustomInput
                        type="radio"
                        id="invertirRadio3"
                        name="invertirRadio"
                        value="5"
                        label="5 usd"
                        inline
                        onChange={(e) => {
                          this.handleFormState({
                            id: 5,
                            value: e.target.value || "",
                          });
                        }}
                      />
                    </div>
                  </FormGroup>
                </Colxx>

                <Colxx xxs="12" md="12">
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "1.2em",
                    }}
                  >
                    Comentario adicional
                  </span>
                  <Input
                    style={{ lineHeight: "13px", textAlign: "justify" }}
                    type="textarea"
                    placeholder="Comentanos tú experiencia con nuestro Sistema de Cotización Online..."
                    rows={7}
                    onChange={(e) => {
                      this.handleFormState({
                        id: 6,
                        value: e.target.value || "",
                      });
                    }}
                  />
                </Colxx>
                <Colxx xxs="12" md="12" sm="12" className="mt-4  mb-2">
                  <CustomInput
                    id="notShow2"
                    type="checkbox"
                    onChange={this.SelectedOnceComment}
                    label="No volver a mostrar este mensaje"
                  />
                </Colxx>
                <Colxx className="mb-4 text-center" xxs="12" md="12">
                  <Button
                    color="primary"
                    className="btn-style-Green"
                    onClick={this.sendCommentary}
                  >
                    ENVIAR
                  </Button>
                </Colxx>
              </Row>
            </Form>
          </ModalBody>
        </Modal>
        {this.state.showMessage ? (
          <div
            className="alertSuccess  arial alert alert-success col-12 col-md-5 mx-auto"
            role="alert"
          >
            ¡Comentario enviado satisfactoriamente!
          </div>
        ) : (
          ``
        )}
      </div>
    );
  }
}

export default comment;

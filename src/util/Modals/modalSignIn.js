import React, { Component, Suspense, lazy } from "react";
import { Row, Button, Modal, ModalBody } from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";

const Login = React.lazy(() => import("../LoginForm"));
class signIn extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={false}
          size="md"
          style={{ borderRadius: "1.1rem" }}
        >
          <ModalBody style={{ padding: "1rem 0px 1rem" }}>
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <div xxs="12" md="12" sm="12" className="text-center font">
              INICIAR SESIÒN
            </div>
            <hr className="ten" />
            <Row>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="pt-3  pb-3"
                style={{ textAlign: "center" }}
              >
                <React.Suspense fallback={<div className="loading"></div>}>
                  <Login
                    setUserSeniorData={this.props.setUserSeniorData}
                    handleLogin={this.props.handleLogin}
                    toggle={this.props.toggle}
                  />
                </React.Suspense>
              </Colxx>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="pt-3  pb-3"
                style={{ textAlign: "center" }}
              >
                <hr className="ten" />
                <Button
                  color="success"
                  onClick={this.props.toggle}
                  className="btn-style"
                  size="sm"
                >
                  {" "}
                  VOLVER
                </Button>
              </Colxx>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default signIn;

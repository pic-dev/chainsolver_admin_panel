import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalBody,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  CustomInput,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
class comment extends Component {
  constructor(props) {
    super(props);

    this.SelectedOnceImportant = this.SelectedOnceImportant.bind(this);
  }
  SelectedOnceImportant() {
    sessionStorage.ImportantNone = "1";
  }
  render() {

    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={true}
          style={{ borderRadius: "2.1rem",maxWidth: "27em !important" }}
          className="fontAtentionStyle" 
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
              }}
            />
            <Row>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="arial mb-4 mt-3 mx-auto"
                style={{
                  textAlign: "center",
                }}
              >
                <span
                  className="color-white"
                  style={{
                    fontSize: "24px",
                    fontWeight: "bold",
                  }}
                ></span>
                <ListGroup>
                  <ListGroupItem
                    className="color-white"
                    style={{
                      textAlign: "center",
                      backgroundColor: "#0d5084",
                    }}
                  >
                    <Row>
                      <Colxx xxs="4" md="3" className="arial mx-auto">
                        <i className="icon-atencion" />
                      </Colxx>
                      <Colxx xxs="8" md="9" className="arial mx-auto">
                        <h2>IMPORTANTE | ¿Cómo interpretar mi oferta?</h2>
                      </Colxx>
                    </Row>
                  </ListGroupItem>
                </ListGroup>
              </Colxx>
              {/*version web origen peru*/}
              <Row>
                <Colxx
                  md="12"
                  lg="4"
                  sm="12"
                  className=" NoPaddingCost mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" my-auto mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <i className="icon-ICO-1" />
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" arial mb-2 mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <ListGroup>
                      <ListGroupItem className="listTittle">
                        Escenario mínimo, medio y máximo
                      </ListGroupItem>
                    </ListGroup>
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" my-auto mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <div
                      style={{
                        margin: "0.5em",

                        borderRadius: "1em",
                        padding: "0.5em",
                        height: "8em",
                      }}
                    >
                      &nbsp;&nbsp;La{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        {" "}
                        &nbsp;UNICA DIFERENCIA &nbsp;
                      </span>
                      entre un escenario y otro, es el valor de los{" "}
                      <span style={{ fontWeight: "bold", color: "red" }}>
                        {" "}
                        &nbsp;IMPUESTOS DE ADUANA{" "}
                      </span>
                      . <br />
                      <span style={{ fontWeight: "bold" }}> Nota:</span> El
                      Servicio Logistico se mantiene igual en los 3 escenarios.
                    </div>
                  </Colxx>
                </Colxx>
                <Colxx
                  md="12"
                  lg="4"
                  sm="12"
                  className=" NoPaddingCost mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" arial mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <i className="icon-ICO-2 " />
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" arial mb-2 mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <ListGroup>
                      <ListGroupItem className="listTittle">
                        ¿De que depende un Escenario MÍNIMO o MÁXIMO?
                      </ListGroupItem>
                    </ListGroup>
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" my-auto mx-auto"
                    style={{
                      textAlign: "justify",
                    }}
                  >
                    <div
                      style={{
                        margin: "0.5em",

                        borderRadius: "1em",
                        padding: "0.5em",
                        height: "8em",
                      }}
                    >
                      <div>
                        &nbsp;&nbsp;Dependerá de los siguientes detalles de su
                        producto:
                      </div>
                      <ul style={{ fontWeight: "bold" }}>
                        <li>Descripción</li>
                        <li>Composición</li>
                        <li>Uso final</li>
                      </ul>{" "}
                    </div>
                  </Colxx>
                </Colxx>
                <Colxx
                  md="12"
                  lg="4"
                  sm="12"
                  className="NoPaddingCost mx-auto"
                  style={{
                    textAlign: "center",
                    display:
                    localStorage.paisDestino == "PERU"
                        ? ""
                        : "none",
                  }}
                >
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" arial mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <i className="icon-Sunat " />
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" arial mb-2 mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <ListGroup>
                      <ListGroupItem className="listTittle">
                        Los Impuestos fueron calculados de la siguiente mandera:
                      </ListGroupItem>
                    </ListGroup>
                  </Colxx>
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" my-auto mx-auto"
                    style={{
                      textAlign: "justify",
                    }}
                  >
                    <div
                      style={{
                        margin: "0.5em",
                        borderRadius: "1em",
                        padding: "0.5em",
                      }}
                    >
                      <Table
                        style={{
                          fontSize: "0.7em",
                          textAlign: "center",
                        }}
                        striped
                      >
                        <thead>
                          <tr>
                            <th>CONCEPTO</th>
                            <th>ESCENARIO MÍNIMO</th>
                            <th>ESCENARIO MEDIO</th>
                            <th>ESCENARIO MÁXIMO</th>
                          </tr>
                        </thead>
                        <tbody
                          style={{
                            fontSize: "0.6em",
                          }}
                        >
                          <tr>
                            <th scope="row">IGV</th>
                            <td>16%</td>
                            <td>16%</td>
                            <td>16%</td>
                          </tr>
                          <tr>
                            <th scope="row">IPM</th>
                            <td>2%</td>
                            <td>2%</td>
                            <td>2%</td>
                          </tr>
                          <tr>
                            <th scope="row">PERCEPCIÓN</th>
                            <td>
                              {localStorage.value4 == "false" ? "10%" : "3,50%"}
                            </td>
                            <td>
                              {localStorage.value4 == "false" ? "10%" : "3,50%"}
                            </td>
                            <td>
                              {localStorage.value4 == "false" ? "10%" : "3,50%"}
                            </td>
                          </tr>
                          <tr style={{ fontSize: "0.7rem", color: "red" }}>
                            <th scope="row">AD VALOREM</th>
                            <td>0%</td>
                            <td>6%</td>
                            <td>11%</td>
                          </tr>
                        </tbody>
                      </Table>
                      <ul
                        className="pt-1"
                        style={{
                          fontSize: "0.75rem",
                          paddingInlineStart: "0px",
                          listStyle: "none",
                        }}
                      >
                        <li>
                          <span style={{ fontWeight: "bold" }}>Nota 1</span>: La
                          percepción aumenta a 10% cuando es la primera
                          importanción.
                        </li>
                        <li>
                          <span style={{ fontWeight: "bold" }}>Nota 2</span>:
                          Algunos productos, como zapatillas, vehiculos o telas
                          tienen un cálculo diferente de impuestos dependiendo
                          de su clasificación.
                        </li>
                        <li>
                          <span style={{ fontWeight: "bold" }}>Nota 3</span>:
                          Los valores son referenciales para la toma de
                          decisiones rápidas.
                        </li>
                      </ul>{" "}
                    </div>
                  </Colxx>
                </Colxx>
              </Row>
            </Row>
            <Row>
              <Colxx md="12" md="12" lg="6">
                <CustomInput
                  id="notShow"
                  type="checkbox"
                  onClick={this.SelectedOnceImportant}
                  label="No volver a mostrar este mensaje"
                />
              </Colxx>
              <Colxx
                md="12"
                md="12"
                lg="6"
                style={{
                  cursor: "pointer",
                }}
              >
                <Button
                  color="primary"
                  className="mb-2 mr-2 float-right btn-style"
                  onClick={this.props.toggle}
                >
                  IR A PRESUPUESTO
                </Button>
              </Colxx>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default comment;

import React, { Component } from "react";
import {
  Input,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  FormGroup,
  Form,
  Row,
  Table,
} from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import axios from "axios";
import logo from "../../assets/img/login/icons/logoDesglose.png";
import { conver_conten } from "../Utils";
class quoteSearchResumen extends Component {
  constructor(props) {
    super(props);

    this.apiPost = this.apiPost.bind(this);
    this.limpiar = this.limpiar.bind(this);
    this.printDocument = this.printDocument.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      desgloseKey: [],
      desglose: false,
      desgloseValues: [],
      desgloseValuesCostos: [],
      resumen: [],
      show: false,
      flete: [],
      impuestosBajos: [],
      totalVenta: [],
      impuestosMedios: [],
      impuestosAltos: [],
      honorarios: [],
      servicioLogistico: [],
      Serviciologis: [],
      impuestosAltosTotal: [],
      impuestosBajosTotal: [],
      impuestosMediosTotal: [],
      servicioLogisticoTotal: [],
      venta: [],
      token: [],
      tipo: [],
      totalIGVFleteCostos: [],
      totalIGVCostos: [],
      activar: false,
      cliente: "",
      contacto: "",
      correo: "",
      ejecutivo: "",
      naviera: [],
    };
  }

  componentDidMount() {
    this.apiPost();
  }

  apiPost() {
    if (this.props.token.length > 0) {
      var tk = this.props.token;

      var res;
      var tipotk;
      var token;

      res = tk.split("-");
      tipotk = res[1].trim();
      token = res[0].trim();

      var tk1 = token + "-" + tipotk;
      var transportes;
      var tipoCon;

      switch (tipotk) {
        case "FCL":
          transportes = "FLETE MARÍTIMO";
          tipoCon = "Maritimo - Completo";

          break;
        case "LCL":
          transportes = "FLETE MARÍTIMO";

          tipoCon = "Maritimo - Compartido";

          break;
        case "AER":
          transportes = "FLETE AÉREO";
          tipoCon = "Aereo";

          break;

        default:
      }

      let resumen2 = this.props.resumen;

      let impuestosBajosTotal = resumen2
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
            return item;
          }
        })
        .map(function (duration) {
          return duration.valor.toFixed(2);
        });

      let impuestosMediosTotal = resumen2
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
            return item;
          }
        })
        .map(function (duration) {
          return duration.valor.toFixed(2);
        });

      let impuestosAltosTotal = resumen2
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
            return item;
          }
        })
        .map(function (duration) {
          return duration.valor.toFixed(2);
        });
      var Serviciologistico;
      let preServiciologistico = resumen2
        .filter(function (item) {
          if (
            item.tipo == "SERVICIO LOGÍSTICO" ||
            item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
          ) {
            return item;
          }
        })
        .map(function (duration) {
          return duration.valor.toFixed(2);
        });

      if (preServiciologistico.length > 1) {
        Serviciologistico = preServiciologistico
          .reduce(function (accumulator, current) {
            return [+accumulator + +current];
          })
          .map(function (duration) {
            return duration.toFixed(2);
          });
      } else {
        Serviciologistico = preServiciologistico;
      }

      let venta = resumen2.filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS"
        ) {
          return item;
        }
      });

      let response = this.props.desglose;
      let flete = response
        .filter(function (item) {
          if (item.tipo == transportes) {
            return item;
          }
        })
        .filter(function (item) {
          if (item.valor > 0) {
            return item;
          }
        });

      let impuestosBajos = response
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
            return item;
          } else {
            return false;
          }
        })
        .map(function (duration) {
          if (duration !== false) {
            return duration.valor.toFixed(2);
          } else {
            return duration;
          }
        });

      let impuestosMedios = response
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
            return item;
          } else {
            return false;
          }
        })
        .map(function (duration) {
          if (duration !== false) {
            return duration.valor.toFixed(2);
          } else {
            return duration;
          }
        });

      let impuestosAltos = response
        .filter(function (item) {
          if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
            return item;
          } else {
            return false;
          }
        })
        .map(function (duration) {
          if (duration !== false) {
            return duration.valor.toFixed(2);
          } else {
            return duration;
          }
        });

      let transporte = response.filter(function (item) {
        if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
          return item;
        } else {
          return false;
        }
      });

      let honorarios = response.filter(function (item) {
        if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
          return item;
        } else {
          return false;
        }
      });

      let seguro = response.filter(function (item) {
        if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
          return item;
        } else {
          return false;
        }
      });

      let servicioLogistico = response.filter(function (item) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
          item.detalles == "Ganancia"
        ) {
          return item;
        } else {
          return false;
        }
      });

      let desgloseValuesCostos = response.filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
          item.tipo !== transportes &&
          item.tipo !== "SERVICIO LOGÍSTICO" &&
          item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
          item.detalles !== "Ganancia" &&
          item.tipo !== "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA"
        ) {
          if (item.valor > 0) {
            return item;
          }
        }
      });

      let desgloseValues = response.filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
          item.tipo !== transportes &&
          item.tipo !== "SERVICIO LOGÍSTICO" &&
          item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
          item.detalles !== "Ganancia"
        ) {
          if (item.valor !== 0) {
            return item;
          }
        }
      });

      let PretotalIGVCostos = response.filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
          item.tipo !== "SERVICIO LOGÍSTICO" &&
          item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
          item.tipo !== transportes &&
          item.tipo !== "FLETE" &&
          item.detalles !== "Ganancia" &&
          item.tipo !== "THCD" &&
          item.tipo !== "TCHD DOCUMENTACION" &&
          item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO"
        ) {
          if (item.valor > 0) {
            return item;
          }
        }
      });
      let totalIGVCostos = [0];

      if (PretotalIGVCostos.length > 1) {
        totalIGVCostos = PretotalIGVCostos.map(function (duration) {
          return (duration.valor * 0.18).toFixed(2);
        })
          .reduce(function (accumulator, current) {
            return [+accumulator + +current];
          })
          .map(function (duration) {
            return duration.toFixed(2);
          });
      }

      let desgloseTotal = response
        .filter(function (item) {
          if (
            item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
            item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
            item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
            item.tipo !== "SERVICIO LOGÍSTICO" &&
            item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
            item.detalles !== "Ganancia"
          ) {
            if (item.valor > 0) {
              return item;
            }
          }
        })
        .map(function (duration) {
          return duration.valor.toFixed(2);
        })
        .reduce(function (accumulator, current) {
          return [+accumulator + +current];
        });

      let preservicioLogisticoTotal = response
        .filter(function (item) {
          if (
            item.tipo == "SERVICIO LOGÍSTICO" ||
            item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
            item.detalles == "Ganancia"
          ) {
            return item;
          }
        })
        .map(function (duration) {
          return duration.valor;
        });
      var servicioLogisticoTotal;
      if (preservicioLogisticoTotal.length > 1) {
        servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (
          accumulator,
          current
        ) {
          return [+accumulator + +current];
        });
      } else {
        servicioLogisticoTotal = preservicioLogisticoTotal;
      }

      let totalIGVFleteCostos =
        Number(desgloseTotal[0]) + Number(totalIGVCostos[0]);

      var totalVenta =
        Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);

      var naviera;
      switch (tipoCon) {
        case "Maritimo - Completo":
          {
            naviera = flete[0].detalles_calculos.naviera;
          }
          break;
        case "Maritimo - Compartido":
          {
            naviera = flete[0].detalles_calculos.agentes_lcl_nombre;
          }
          break;
        case "Aereo":
          {
            naviera = flete[0].detalles_calculos.agentes_aereo_nombre;
          }
          break;

        default:
      }
      this.setState((ps) => ({
        impuestosAltosTotal: impuestosAltosTotal,
        impuestosMediosTotal: impuestosMediosTotal,
        impuestosBajosTotal: impuestosBajosTotal,
        Serviciologis: Serviciologistico,
        venta: venta,
        tipo: tipoCon,
        token: tk1.toString(),
        desgloseValues: desgloseValues,
        desgloseValuesCostos: desgloseValuesCostos,
        show: true,
        flete: flete,
        impuestosAltos: impuestosAltos,
        impuestosMedios: impuestosMedios,
        impuestosBajos: impuestosBajos,
        servicioLogistico: servicioLogistico,
        transporte: transporte,
        seguro: seguro,
        honorarios: honorarios,
        naviera: naviera,
        totalIGVFleteCostos: totalIGVFleteCostos.toFixed(2),
        totalIGVCostos: totalIGVCostos[0],
        servicioLogisticoTotal: servicioLogisticoTotal,
        desgloseTotal: desgloseTotal[0],
        totalVenta: totalVenta,
      }));
    }
  }

  limpiar() {
    this.setState((ps) => ({
      servicioLogistico: [],
      transporte: [],
      seguro: [],
      honorarios: [],
      desgloseValues: [],
      desgloseValuesCostos: [],
      show: false,
      flete: [],
      totalIGVFleteCostos: [],
      totalIGVCostos: [],
      impuestosAltos: [],
      impuestosMedios: [],
      impuestosBajos: [],
      servicioLogistico: [],
      servicioLogisticoTotal: [],
      Serviciologis: [],
      desgloseTotal: [],
      impuestosAltosTotal: [],
      impuestosMediosTotal: [],
      impuestosBajosTotal: [],
      naviera: [],
      venta: [],
      tipo: [],
      token: [],
    }));
  }

  toggleModal() {
    this.setState((ps) => ({
      activar: !ps.activar,
    }));
  }

  printDocument() {
    if (this.state.desgloseKey.length > 0) {
      const input = document.getElementById("divToPrint2");
      html2canvas(input).then((canvas) => {
        const imgData = canvas.toDataURL("image/png");
        const pdf = new jsPDF("l", "mm", "a4");
        pdf.addImage(imgData, "PNG", 7, 7, 280, 200);
        pdf.addImage(logo, "PNG", 7, 5, 42, 12);
        var nombre = this.state.token;
        pdf.save(nombre + ".pdf");
      });
      this.toggleModal();
    }
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          backdrop={false}
          style={{ maxWidth: "max-content" }}
        >
          {this.state.desgloseKey == "piccargo1" ? (
            <ModalBody style={{ padding: "0" }} className="mx-auto">
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />

              <Row className="mx-auto">
                <Row className="mx-auto">
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className="mx-auto pt-3 font2 pb-3 text-center"
                  >
                    <Button
                      className="btn-style-blue"
                      onClick={this.toggleModal}
                    >
                      Descargar
                    </Button>
                  </Colxx>
                  <Modal isOpen={this.state.activar} toggle={this.toggleModal}>
                    <ModalHeader className="mx-auto p-3 font text-center">
                      Descargar Presupuesto
                    </ModalHeader>
                    <ModalBody>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        className="mx-auto p-2 text-center"
                      >
                        <FormGroup className="mb-1">
                          <span
                            style={{
                              fontWeight: "bold",
                            }}
                          >
                            NOMBRE DEL CLIENTE
                          </span>
                          - (Opcional)
                        </FormGroup>
                        <FormGroup className="mb-2">
                          <Input
                            name="cliente"
                            type="text"
                            placeholder="Ingrese aqui el nombre del cliente"
                            value={this.state.cliente}
                            onChange={(e) => {
                              this.setState({
                                cliente: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                        <FormGroup className="mb-1">
                          <span
                            style={{
                              fontWeight: "bold",
                            }}
                          >
                            CONTACTO
                          </span>
                          - (Opcional)
                        </FormGroup>
                        <FormGroup className="mb-2">
                          <Input
                            name="contacto"
                            type="text"
                            placeholder="Ingrese aqui el nombre del CONTACTO"
                            value={this.state.contacto}
                            onChange={(e) => {
                              this.setState({
                                contacto: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                        <FormGroup className="mb-1">
                          <span
                            style={{
                              fontWeight: "bold",
                            }}
                          >
                            CORREO
                          </span>
                          - (Opcional)
                        </FormGroup>
                        <FormGroup className="mb-2">
                          <Input
                            name="correo"
                            type="text"
                            placeholder="Ingrese aqui el CORREO del CONTACTO"
                            value={this.state.correo}
                            onChange={(e) => {
                              this.setState({
                                correo: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                        <FormGroup className="mb-1">
                          <span
                            style={{
                              fontWeight: "bold",
                            }}
                          >
                            EJECUTIVO PIC CARGO
                          </span>
                          - (Opcional)
                        </FormGroup>
                        <FormGroup className="mb-2">
                          <Input
                            name="ejecutivo"
                            type="text"
                            placeholder="Ingrese aqui el nombre del ejecutivo"
                            value={this.state.ejecutivo}
                            onChange={(e) => {
                              this.setState({
                                ejecutivo: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                      </Colxx>
                    </ModalBody>
                    <ModalFooter className="mx-auto p-3 font2 pb-3 text-center">
                      <Button
                        className="btn-style-blue"
                        onClick={this.printDocument}
                      >
                        Descargar
                      </Button>
                    </ModalFooter>
                  </Modal>
                  <div
                    id="divToPrint2"
                    className="p-2"
                    style={{
                      width: "330mm",
                      minHeight: "210mm",
                      marginLeft: "auto",
                      marginRight: "auto",
                    }}
                  >
                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      style={{ color: "black", fontSize: "1rem" }}
                      className="pb-4  font text-right"
                    >
                      Numero de cotización #: ID {this.state.token}
                    </Colxx>
                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      style={{ overflow: "auto" }}
                      className="p-2 text-justify"
                    >
                      <Row>
                        <Colxx style={{ padding: "0" }} xxs="5" md="5" sm="5">
                          <div
                              className="color-black"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#eae6e6"
                            }}
                          >
                            DATOS DEL CLIENTE
                          </div>
                        </Colxx>
                        <Colxx
                          className="font color-black"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#fdfbb7"
                          }}
                          xxs="7"
                          md="7"
                          sm="7"
                        >
                          PROFIT: {this.state.servicioLogisticoTotal[0]}
                        </Colxx>
                        <Colxx xxs="5" md="5" sm="5">
                          <ul
                            className="p-1"
                            style={{ fontSize: "0.8rem", listStyle: "none" }}
                          >
                            <li>CLIENTE: {this.state.cliente}</li>
                            <li>CONTACTO:{this.state.contacto}</li>
                            <li>CORREO: {this.state.correo}</li>
                            <li>Ejecutivo PIC CARGO: {this.state.ejecutivo}</li>
                          </ul>
                        </Colxx>
                        <Colxx xxs="7" md="7" sm="7" style={{ padding: "0" }}>
                          <div
                                  className="color-black"
                            style={{
                              textAlign: "center",
                              backgroundColor: "#eae6e6"
                            }}
                          >
                            INFORMACIÓN DE LA CARGA
                          </div>
                          <Row>
                            <Colxx xxs="6" md="6" sm="6" className="p-1">
                              <ul
                                style={{
                                  fontSize: "0.8rem",
                                  listStyle: "none",
                                }}
                              >
                                <li>Transporte:{this.state.tipo}</li>
                                <li>
                                  Ruta Origen:
                                  {this.state.flete[0].detalles_calculos
                                    .pais_origen +
                                    " - " +
                                    this.state.flete[0].detalles_calculos
                                      .puerto_origen}
                                </li>
                                <li>
                                  Ruta Destino:
                                  {this.state.flete[0].detalles_calculos
                                    .pais_destino +
                                    " - " +
                                    this.state.flete[0].detalles_calculos
                                      .puerto_destino}
                                </li>
                                <li>
                                  Contenido de carga :
                                  {this.state.tipo === "Maritimo - Completo"
                                    ? conver_conten(this.state.flete)
                                    : this.state.flete[0]
                                        .input_cantidad_bultos +
                                      " Bultos, " +
                                      this.state.flete[0].input_peso +
                                      " Kl, " +
                                      this.state.flete[0].input_volumen +
                                      " M3 "}
                                </li>
                                <li>
                                  Gastos portuarios:
                                  {this.state.flete[0]
                                    .input_calcular_gastos_y_almacen_aduanero
                                    ? " SI"
                                    : " NO"}
                                </li>
                              </ul>
                            </Colxx>
                            <Colxx xxs="6" md="6" sm="6" className="p-1">
                              <ul
                                style={{
                                  fontSize: "0.8rem",
                                  listStyle: "none",
                                }}
                              >
                                <li>
                                  Valor Mercancia:
                                  {this.state.flete[0]
                                    .input_calcular_inpuestos_y_permisos
                                    ? this.state.flete[0].input_valor_mercancia
                                    : " NO COTIZADO"}
                                </li>
                                <li>
                                  Servicios / Impuestos de Aduana:
                                  {this.state.flete[0]
                                    .input_calcular_inpuestos_y_permisos
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>
                                  Transporte a Domicilio:
                                  {this.state.flete[0].input_calcular_transporte
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>
                                  Seguro de Mercancia:
                                  {this.state.flete[0]
                                    .input_calcular_seguro_mercancia
                                    ? " SI"
                                    : " NO"}
                                </li>
                                <li>Naviera / Agente: {this.state.naviera}</li>

                                <li
                                  style={{
                                    display: this.state.flete[0]
                                      .input_calcular_inpuestos_y_permisos
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  {this.state.flete[0].input_ha_cotizado
                                    ? " Segunda Importación"
                                    : " Primera Importación"}
                                </li>
                              </ul>
                            </Colxx>
                          </Row>
                        </Colxx>
                      </Row>
                      <Colxx
                         className="color-white"
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084",
                        }}
                        className="font2"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        GANANCIA
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>SUB ITEM</th>
                            <th>DETALLES</th>
                            <th>PROFIT</th>
                            <th>IGV</th>
                            <th>PROFIT+IGV</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.servicioLogistico.map((item, index) => (
                            <tr key={index + "a"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.nombre}</td>
                              <td>{item.detalles}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td></td>
                              <td></td>
                            </tr>
                          ))}
                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">--</th>
                            <td>--</td>
                            <td>TOTAL</td>
                            <td>{this.state.servicioLogisticoTotal[0]}</td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (
                                    (this.state.Serviciologis[0] / 3) *
                                    2 *
                                    0.18
                                  ).toFixed(2)
                                : "n/a"}
                            </td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (this.state.Serviciologis[0] / 3) * 2 * 0.18 +
                                  this.state.servicioLogisticoTotal[0]
                                : "n/a"}
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                         className="color-white"
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084",
                        }}
                        className="font2"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        VENTA
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>VALOR</th>
                            <th>IGV</th>
                            <th>VALOR+IGV</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.venta.map((item, index) => (
                            <tr key={index + "b"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>
                                {item.tipo != "THCD" &&
                                item.tipo != "TCHD DOCUMENTACION" &&
                                item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
                                item.tipo != "FLETE" &&
                                item.tipo != "FLETE MARÍTIMO" &&
                                item.tipo != "FLETE AÉREO" &&
                                this.state.flete[0].detalles_calculos
                                  .pais_destino == "PERU" &&
                                this.state.totalIGVCostos > 0
                                  ? item.tipo == "SERVICIO LOGÍSTICO" ||
                                    item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
                                    ? ((item.valor / 3) * 2 * 0.18).toFixed(2)
                                    : (item.valor * 0.18).toFixed(2)
                                  : "n/a"}
                              </td>
                              <td>
                                {item.tipo != "THCD" &&
                                item.tipo != "TCHD DOCUMENTACION" &&
                                item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
                                item.tipo != "FLETE" &&
                                item.tipo != "FLETE MARÍTIMO" &&
                                item.tipo != "FLETE AÉREO" &&
                                this.state.flete[0].detalles_calculos
                                  .pais_destino == "PERU" &&
                                this.state.totalIGVCostos > 0
                                  ? item.tipo == "SERVICIO LOGÍSTICO" ||
                                    item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
                                    ? (
                                        (item.valor / 3) * 2 * 0.18 +
                                        item.valor
                                      ).toFixed(2)
                                    : (item.valor * 1.18).toFixed(2)
                                  : item.valor.toFixed(2)}
                              </td>
                            </tr>
                          ))}

                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">TOTAL:</th>
                            <td>{this.state.totalVenta.toFixed(2)}</td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (
                                    Number(this.state.totalIGVCostos) +
                                    Number(
                                      (this.state.servicioLogisticoTotal[0] /
                                        3) *
                                        2 *
                                        0.18
                                    )
                                  ).toFixed(2)
                                : "n/a"}
                            </td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU" &&
                              this.state.totalIGVCostos > 0
                                ? (
                                    Number(this.state.totalIGVFleteCostos) +
                                    Number(
                                      (this.state.servicioLogisticoTotal[0] /
                                        3) *
                                        2 *
                                        0.18 +
                                        this.state.servicioLogisticoTotal[0]
                                    )
                                  ).toFixed(2)
                                : "n/a"}
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                         className="color-white"
                        style={{
                          textAlign: "center",
                          backgroundColor: "#0d5084"
                        }}
                        className="font2"
                        xxs="12"
                        md="12"
                        sm="12"
                      >
                        COSTOS
                      </Colxx>
                      <Table
                        style={{
                          textAlign: "center",
                          margin: "0rem",
                        }}
                        responsive
                      >
                        <thead
                          style={{
                            backgroundColor: "#cfe7fe",
                            fontWeight: "bold",
                          }}
                        >
                          <tr>
                            <th>ITEM</th>
                            <th>SUB ITEM</th>
                            <th>TOTAL</th>
                            <th>--</th>
                            <th>IGV</th>
                            <th>--</th>
                            <th>TOTAL+IGV</th>
                            <th>DETALLES</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.flete.map((item, index) => (
                            <tr key={index + "c"}>
                              <th scope="row">{item.nombre}</th>
                              <td>{item.nombre}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>--</td>
                              <td>n/a</td>
                              <td>--</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>{item.detalles}</td>
                            </tr>
                          ))}
                          {this.state.desgloseValues.map((item, index) => (
                            <tr key={index + "d"}>
                              <th scope="row">{item.tipo}</th>
                              <td>{item.nombre}</td>
                              <td>{item.valor.toFixed(2)}</td>
                              <td>--</td>
                              <td>
                                {item.tipo != "THCD" &&
                                item.tipo != "TCHD DOCUMENTACION" &&
                                item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
                                item.tipo != "FLETE" &&
                                this.state.flete[0].detalles_calculos
                                  .pais_destino == "PERU" &&
                                this.state.totalIGVCostos > 0
                                  ? (item.valor * 0.18).toFixed(2)
                                  : "n/a"}
                              </td>
                              <td>--</td>
                              <td>
                                {item.tipo != "THCD" &&
                                item.tipo != "TCHD DOCUMENTACION" &&
                                item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
                                item.tipo != "FLETE" &&
                                this.state.flete[0].detalles_calculos
                                  .pais_destino == "PERU" &&
                                this.state.totalIGVCostos > 0
                                  ? (item.valor * 1.18).toFixed(2)
                                  : item.valor.toFixed(2)}
                              </td>
                              <td>{item.detalles}</td>
                            </tr>
                          ))}
                          <tr
                            style={{
                              backgroundColor: "#d0e4f7",
                              fontWeight: "bold",
                            }}
                          >
                            <th scope="row">--</th>
                            <td>TOTAL</td>
                            <td>{Number(this.state.desgloseTotal).toFixed(2)}</td>
                            <td>--</td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU"
                                ? this.state.totalIGVCostos
                                : ""}
                            </td>
                            <td>--</td>
                            <td>
                              {this.state.flete[0].detalles_calculos
                                .pais_destino == "PERU"
                                ? this.state.totalIGVFleteCostos
                                : ""}
                            </td>
                            <td>--</td>
                          </tr>
                        </tbody>
                      </Table>
                      <Colxx
                        xxs="12"
                        md="12"
                        sm="12"
                        style={{
                          textAlign: "center",
                          padding: "0rem",
                        }}
                        className="mx-auto text-center"
                      >
                        <Colxx
                           className="color-white"
                          style={{
                            textAlign: "center",
                            backgroundColor: "#b33641"
                          }}
                          className="font2"
                          xxs="12"
                          md="12"
                          sm="12"
                        >
                          IMPUESTOS DE ADUANA
                        </Colxx>
                        <Table
                          style={{
                            textAlign: "center",
                          }}
                          responsive
                        >
                          <thead>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                TIPO
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS BAJOS
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#d0f7d1",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS MEDIOS
                              </th>
                              <th
                                style={{
                                  backgroundColor: "#eac3c3",
                                  fontWeight: "bold",
                                }}
                              >
                                IMPUESTOS ALTOS
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                Percepciones 1era vez
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[0]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[0]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[0]}
                              </td>
                            </tr>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                ISC
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[1]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[1]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[1]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                IPM
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[2]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[2]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[2]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                IGV
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[3]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[3]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[3]}
                              </td>
                            </tr>

                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                AD valorem
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                }}
                              >
                                {this.state.impuestosBajos[4]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                }}
                              >
                                {this.state.impuestosMedios[4]}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                }}
                              >
                                {this.state.impuestosAltos[4]}
                              </td>
                            </tr>
                            <tr>
                              <th
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                                scope="row"
                              >
                                TOTAL
                              </th>
                              <td
                                style={{
                                  backgroundColor: "#d0e4f7",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosBajosTotal[0] > 0
                                  ? "$ " + this.state.impuestosBajosTotal[0]
                                  : "No cotizado"}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#d0f7d1",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosMediosTotal[0] > 0
                                  ? "$ " + this.state.impuestosMediosTotal[0]
                                  : "No cotizado"}
                              </td>
                              <td
                                style={{
                                  backgroundColor: "#eac3c3",
                                  fontWeight: "bold",
                                }}
                              >
                                {this.state.impuestosAltosTotal[0] > 0
                                  ? "$ " + this.state.impuestosAltosTotal[0]
                                  : "No cotizado"}
                              </td>
                            </tr>
                          </tbody>
                        </Table>
                      </Colxx>
                    </Colxx>
                  </div>
                </Row>

                <Colxx
                  xxs="12"
                  md="12"
                  sm="12"
                  className="pt-3  pb-3"
                  style={{ textAlign: "center" }}
                >
                  <hr className="ten" />
                  <Button
                    color="success"
                    onClick={this.props.toggle}
                    className="btn-style"
                    size="sm"
                  >
                    VOLVER
                  </Button>
                </Colxx>
              </Row>
            </ModalBody>
          ) : (
            <ModalBody>
              <i
                onClick={this.props.toggle}
                className="plusLump  simple-icon-close "
                style={{
                  fontSize: "2em",
                  position: "absolute",
                  right: "0.5em",
                  top: "0.5em",
                  cursor: "pointer",
                  zIndex: "999",
                }}
              />
              <div xxs="12" md="12" sm="12" className="text-center font">
                Resumen
              </div>
              <hr className="ten" />

              <Row>
                <Colxx xxs="12" md="12" sm="12" className="p-3 text-center">
                  <Input
                    name="clave"
                    type="text"
                    placeholder="Clave"
                    value={this.state.desgloseKey}
                    onChange={(e) => {
                      this.setState({
                        desgloseKey: e.target.value,
                      });
                    }}
                  />
                </Colxx>
              </Row>
            </ModalBody>
          )}
        </Modal>
      </div>
    );
  }
}

export default quoteSearchResumen;

import React, { Component } from "react";
import {
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Collapse,
  Button,
  ListGroup,
  Table,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  UncontrolledTooltip,
  NavLink,
} from "reactstrap";
import classnames from "classnames";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import "react-inner-image-zoom/lib/InnerImageZoom/styles.css";
import InnerImageZoom from "react-inner-image-zoom";
import Likes from "../likes";

class ProductDetailsList extends Component {
  constructor(props) {
    super(props);
    this.tab = this.tab.bind(this);
    this.state = {
      pdf: false,
      pdfLink: "",
      activeTab: "2",
    };

    this.openCatalogo = this.openCatalogo.bind(this);
    this.closeCatalogo = this.closeCatalogo.bind(this);
  }

  componentDidMount() {
    this.setState({
      pdf: false,
      pdfLink: "",
    });
  }

  componentDidUpdate(nextProps) {
    if (nextProps.item !== this.props.item) {
      this.setState({
        activeTab: "2",
        pdf: false,
        pdfLink: "",
      });
    }
  }
  tab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  openCatalogo(link) {
    if (screen.width > 1023) {
      this.setState({
        pdfLink: link,
        pdf: true,
      });
    } else {
      window.open(link, "_blank");
    }
  }
  closeCatalogo() {
    this.setState({
      pdfLink: "",
      pdf: false,
    });
  }
  render() {
    var item = this.props.item;
    var key = item[0].key;
    var clientes = this.props.ClienteStock[0];
    var typeProduct = this.props.typeProduct;
    return (
      <div>
        <Modal
          isOpen={this.props.active}
          toggle={this.props.toggle}
          className="modal-dialog2 theme-color-blueHeader2"
        >
          <ModalBody
            className="theme-color-blueHeader2 pt-5"
            style={{ padding: "0rem" }}
          >
            <i
              onClick={this.props.toggle}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            {!this.state.pdf ? (
              <Row className="mx-auto">
                <Col xs="12" md="6">
                  <Card
                    className="p-1"
                    style={{
                      alignItems: "center",
                    }}
                  >
                    <Carousel
                      infiniteLoop={true}
                      autoPlay={false}
                      showThumbs={false}
                      showIndicators={false}
                      stopOnHover={true}
                      showStatus={false}
                      className="carrouselWidth"
                    >
                      {item[0].Multimedia[1] ? (
                        item[0].Multimedia[1].video.map((picture, index) => (
                          <div>
                            <video
                              className="react-player imgDetails"
                              key={index + "video"}
                              id={"my-video" + index}
                              controls
                              width="100%"
                              height="100%"
                              data-setup="{}"
                              autoPlay
                              muted
                            >
                              <source
                                src={picture.img}
                                type={picture.fileType}
                              />
                            </video>
                          </div>
                        ))
                      ) : (
                        <InnerImageZoom
                          moveType="drag"
                          src={item[0].Multimedia[0].imagen[0].img}
                          className="imgDetails p-2"
                          alt="logo"
                        />
                      )}

                      {item[0].Multimedia[0].imagen.map((picture, index) => (
                        <div>
                          <InnerImageZoom
                            moveType="drag"
                            src={picture.img}
                            className="imgDetails p-2"
                            alt="logo"
                          />
                        </div>
                      ))}
                    </Carousel>
                    <Likes item={key} />
                    <CardBody
                      style={{
                        padding: "1rem 0px 0px",
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      <CardTitle
                        className="mb-1"
                        style={{ fontWeight: "bold" }}
                      >
                        Producto: {item[0].NombreProducto.toUpperCase()}
                      </CardTitle>
                      <a
                        style={{ padding: "0px" }}
                        href="https://wa.link/xbr57j"
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        {" "}
                        <i className="whatsapp-contacto" />{" "}
                      </a>
                    </CardBody>
                  </Card>
                </Col>

                {typeProduct == "Proximas" ? (
                  <Col xs="12" md="6">
                    {item[0].Multimedia[2] ? (
                      <div
                        style={{
                          padding: "1rem 0 1rem 0",
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {" "}
                        <div
                          style={{ width: "100%" }}
                          className="text-white arial m-2 bg-info"
                        >Catalogos Disponibles
                        </div>
                        <Table className="m-2">
                          <tbody>
                            {item[0].Multimedia[2].pdf.map((item, index) => (
                              <tr
                                className="p-1 text-center"
                                key={index + "catalogos"}
                              >
                                <th
                                  scope="row"
                                  style={{
                                    verticalAlign: "middle",
                                    backgroundColor: "#80808038",
                                  }}
                                >
                                  Catalogo: {index + 1}
                                </th>
                                <td>
                                  {" "}
                                  <Button
                                    className="btn-style-Green btn btn-success btn-sm"
                                    onClick={() => { window.open(item.img, "_blank");
                                    }}
                                  >
                                    Descargar <i className="simple-icon-arrow-down-circle" />
                                  </Button>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                        <hr />
                        <div
                          style={{ width: "100%" }}
                          className="mb-2"
                        ><a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://forms.gle/oMW3maYfm6pfkWXY6"
                          ><Button style={{width: "90%",padding: "2px",height: "3rem"}} className="btn-style-bluelight mr-2 btn btn-success btn-sm">
                              Ingresa a la encuesta de productos Aquí
                            </Button>
                          </a>
                          <i
                            id="survey"
                            className="simple-icon-question question-style2"
                          />
                        </div>
                        <UncontrolledTooltip placement="right" target="survey">
                          <div className="color-blue text-justify">
                            La finalidad de la encuesta es saber que productos
                            son del interés del publico para poder negociar con
                            los proveedores,
                          </div>
                        </UncontrolledTooltip>
                        <div
                          style={{ width: "100%" }}
                          className="mb-2"
                        ><a
                            target="_blank"
                            rel="noopener noreferrer"
                            href="https://fb.me/e/1Ua4kHkWT"
                          ><Button style={{width: "90%", padding: "2px",height: "3rem"}} className="btn-style-blue mr-2 btn btn-success btn-sm">
                              Únete aquí al taller GRATUITO <br/> de Importación del 23-01
                            </Button>
                          </a>
                          <i
                            id="event"
                            className="simple-icon-question question-style2"
                          />
                        </div>
                        <UncontrolledTooltip placement="right" target="event">
                         <div className="color-blue text-justify">
                            Taller online donde se darán tips para tú primera
                            importación
                          </div>
                        </UncontrolledTooltip>
                        <div
                          style={{ width: "100%" }}
                          className="text-white arial m-2 bg-danger"
                        >Importante : Cierre de pedidos Lunes 25 de enero
                        </div>
                      </div>
                    ) : (
                      <Row>
                        <Col
                          xs="12"
                          md="12"
                          className={`text-center arial tab1 ${classnames({
                            active2: this.state.activeTab === "2",
                          })}`}
                          onClick={() => {
                            this.tab("2");
                          }}
                        >Especificaciones
                        </Col>
                        <TabContent
                          className="mx-auto"
                          activeTab={this.state.activeTab}
                        ><TabPane tabId="2">
                            <Row className="mx-auto pt-2">
                              <Col className="p-0" xs="12" md="12">
                                <div
                                  className="p-1"
                                  style={{
                                    alignItems: "center",
                                  }}
                                ><div
                                    style={{
                                      padding: "1rem 0 1rem 0",
                                      width: "100%",
                                      textAlign: "center",
                                    }}
                                  >
                                    <Table>
                                      <tbody>
                                        {item[0].Caracteristicas.map((item) => (
                                          <tr
                                            className="p-0 text-center"
                                            key={item.key + "Especificaciones2"}
                                          >
                                            <th
                                              scope="row"
                                              style={{
                                                fontWeight: "bold",
                                                backgroundColor: "#80808038",
                                                width: "7rem",
                                              }}
                                            >
                                              {item.nombre}
                                            </th>
                                            <td>{item.descripción}</td>
                                          </tr>
                                        ))}
                                      </tbody>
                                    </Table>
                                  </div>
                                </div>
                              </Col>
                            </Row>{" "}
                          </TabPane>
                        </TabContent>
                      </Row>
                    )}
                  </Col>
                ) : (
                  <Col xs="12" md="6">
                    <Row>
                      <Col
                        xs="6"
                        md="6"
                        className={`text-center arial tab1 ${classnames({
                          active2: this.state.activeTab === "2",
                        })}`}
                        onClick={() => {
                          this.tab("2");
                        }}
                      >
                        Importadores asociados
                      </Col>
                      <Col
                        xs="6"
                        md="6"
                        className={`text-center arial tab1 ${classnames({
                          active2: this.state.activeTab === "1",
                        })}`}
                        onClick={() => {
                          this.tab("1");
                        }}
                      >
                        Especificaciones
                      </Col>
                    </Row>
                    <TabContent
                      className="mx-auto "
                      activeTab={this.state.activeTab}
                    >
                      <TabPane tabId="1">
                        <Row className="mx-auto pt-2">
                          <Col className="p-0" xs="12" md="12">
                            <div
                              className="p-1"
                              style={{
                                alignItems: "center",
                              }}
                            >
                              {" "}
                              <div
                                style={{
                                  padding: "1rem 0 1rem 0",
                                  width: "100%",
                                  textAlign: "center",
                                }}
                              >
                                <div>
                                  <Table>
                                    <tbody>
                                      {item[0].Caracteristicas.map((item) => (
                                        <tr
                                          className="p-0 text-center"
                                          key={item.key + "Especificaciones1"}
                                        >
                                          <th
                                            scope="row"
                                            style={{
                                              fontWeight: "bold",
                                              backgroundColor: "#80808038",
                                            }}
                                          >
                                            {item.nombre}
                                          </th>
                                          <td>{item.descripción}</td>
                                        </tr>
                                      ))}
                                    </tbody>
                                  </Table>
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>{" "}
                      </TabPane>
                      <TabPane tabId="2">
                        <Row className="mx-auto pt-2">
                          <Col
                            className="p-0"
                            xs="12"
                            md="12"
                            className="text-center"
                          >
                            <Table>
                              <thead>
                                <tr>
                                  <th>Contacto</th>
                                  <th>País</th>
                                  <th>Provincia</th>
                                  <th>Telefono</th>
                                </tr>
                              </thead>
                              <tbody>
                                {clientes.map((item) => (
                                  <tr
                                    className="p-0 text-center"
                                    key={item.key + "clientes"}
                                  >
                                    <th
                                      scope="row"
                                      style={{
                                        fontWeight: "bold",
                                        backgroundColor: "#80808038",
                                      }}
                                    >
                                      {item.contacto}
                                    </th>
                                    <td>{item.Pais}</td>
                                    <td>{item.Provincia}</td>
                                    <td>{item.telefono}</td>
                                  </tr>
                                ))}
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </TabPane>
                    </TabContent>
                  </Col>
                )}
                <Col xs="12" md="12" className="p-2 text-center">
                  {" "}
                  <Button
                    className="btn-style m-1 btn btn-success btn-sm"
                    onClick={this.props.toggle}
                  >
                    Volver
                  </Button>
                </Col>
              </Row>
            ) : ("")}
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default ProductDetailsList;

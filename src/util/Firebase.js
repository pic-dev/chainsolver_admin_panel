import firebase from 'firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/storage"

const config = {
  apiKey: "AIzaSyA_JThaMiCsZDoFesLmjE3rSfsZ9TVhKVM",
  authDomain: "pic-calculadora.firebaseapp.com",
  databaseURL: "https://pic-calculadora.firebaseio.com",
  projectId: "pic-calculadora",
  storageBucket: "pic-calculadora.appspot.com",
  messagingSenderId: "1034730132272",
  appId: "1:1034730132272:web:0e027385f7e43ca9a5eef5",
  measurementId: "G-3365D6E05J"
  };
const firebaseConf = firebase.initializeApp(config);
const database = firebase.database();

const persistencia = firebase.auth.Auth.Persistence.LOCAL;
 const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();


export {firebaseConf,googleAuthProvider,facebookAuthProvider, persistencia};
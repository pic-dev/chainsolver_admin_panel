import MiniSearch, { Options, SearchOptions, SearchResult, Suggestion } from 'minisearch';
import React, { PropsWithChildren } from 'react';
export interface UseMiniSearch<T = any> {
    search: (query: string, options?: SearchOptions) => void;
    searchResults: T[] | null;
    rawResults: SearchResult[] | null;
    autoSuggest: (query: string, options?: SearchOptions) => void;
    suggestions: Suggestion[] | null;
    add: (document: T) => void;
    addAll: (documents: T[]) => void;
    addAllAsync: (documents: T[], options?: {
        chunkSize?: number;
    }) => Promise<void>;
    remove: (document: T) => void;
    removeById: (id: any) => void;
    removeAll: (documents?: T[]) => void;
    isIndexing: boolean;
    clearSearch: () => void;
    clearSuggestions: () => void;
    miniSearch: MiniSearch<T>;
}
export declare function useMiniSearch<T = any>(documents: T[], options: Options<T>): UseMiniSearch<T>;
export declare function withMiniSearch<OwnProps, T = any>(documents: T[], options: Options<T>, Component: React.ComponentType<OwnProps & UseMiniSearch<T>>): React.FC<OwnProps>;
export interface WithMiniSearchProps<T = any> {
    documents: T[];
    options: Options<T>;
    children: (props: UseMiniSearch<T>) => JSX.Element | null;
}
export declare const WithMiniSearch: <T>({ documents, options, children }: React.PropsWithChildren<WithMiniSearchProps<T>>) => JSX.Element;

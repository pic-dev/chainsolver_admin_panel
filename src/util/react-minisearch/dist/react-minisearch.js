(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('minisearch'), require('react')) :
    typeof define === 'function' && define.amd ? define(['exports', 'minisearch', 'react'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['react-minisearch'] = {}, global.MiniSearch, global.React));
}(this, (function (exports, MiniSearch, React) { 'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var MiniSearch__default = /*#__PURE__*/_interopDefaultLegacy(MiniSearch);
    var React__default = /*#__PURE__*/_interopDefaultLegacy(React);

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function useMiniSearch(documents, options) {
        var miniSearch = React.useState(new MiniSearch__default['default'](options))[0];
        var _a = React.useState(null), rawResults = _a[0], setRawResults = _a[1];
        var _b = React.useState(null), searchResults = _b[0], setSearchResults = _b[1];
        var _c = React.useState(null), suggestions = _c[0], setSuggestions = _c[1];
        var _d = React.useState({}), documentById = _d[0], setDocumentById = _d[1];
        var _e = React.useState(false), isIndexing = _e[0], setIsIndexing = _e[1];
        var idField = options.idField || MiniSearch__default['default'].getDefault('idField');
        var extractField = options.extractField || MiniSearch__default['default'].getDefault('extractField');
        var gatherById = function (documents) { return documents.reduce(function (byId, doc) {
            var id = extractField(doc, idField);
            byId[id] = doc;
            return byId;
        }, {}); };
        React.useEffect(function () {
            addAll(documents);
        }, []);
        var search = function (query, searchOptions) {
            var results = miniSearch.search(query, searchOptions);
            var searchResults = results.map(function (_a) {
                var id = _a.id;
                return documentById[id];
            });
            setSearchResults(searchResults);
            setRawResults(results);
        };
        var autoSuggest = function (query, searchOptions) {
            var suggestions = miniSearch.autoSuggest(query, searchOptions);
            setSuggestions(suggestions);
        };
        var add = function (document) {
            var _a;
            setDocumentById(__assign(__assign({}, documentById), (_a = {}, _a[extractField(document, idField)] = document, _a)));
            miniSearch.add(document);
        };
        var addAll = function (documents) {
            var byId = gatherById(documents);
            setDocumentById(Object.assign({}, documentById, byId));
            miniSearch.addAll(documents);
        };
        var addAllAsync = function (documents, options) {
            var byId = gatherById(documents);
            setDocumentById(Object.assign({}, documentById, byId));
            setIsIndexing(true);
            return miniSearch.addAllAsync(documents, options || {}).then(function () {
                setIsIndexing(false);
            });
        };
        var remove = function (document) {
            miniSearch.remove(document);
            setDocumentById(removeFromMap(documentById, extractField(document, idField)));
        };
        var removeById = function (id) {
            var document = documentById[id];
            if (document == null) {
                throw new Error("react-minisearch: document with id " + id + " does not exist in the index");
            }
            miniSearch.remove(document);
            setDocumentById(removeFromMap(documentById, id));
        };
        var removeAll = function (documents) {
            if (documents === void 0) { documents = null; }
            documents ? miniSearch.removeAll(documents) : miniSearch.removeAll();
        };
        var clearSearch = function () {
            setSearchResults(null);
            setRawResults(null);
        };
        var clearSuggestions = function () {
            setSuggestions(null);
        };
        return {
            search: search,
            searchResults: searchResults,
            rawResults: rawResults,
            autoSuggest: autoSuggest,
            suggestions: suggestions,
            add: add,
            addAll: addAll,
            addAllAsync: addAllAsync,
            remove: remove,
            removeById: removeById,
            removeAll: removeAll,
            isIndexing: isIndexing,
            clearSearch: clearSearch,
            clearSuggestions: clearSuggestions,
            miniSearch: miniSearch
        };
    }
    function removeFromMap(map, keyToRemove) {
        var newMap = Object.assign({}, map);
        delete newMap[keyToRemove];
        return newMap;
    }
    function getDisplayName(Component) {
        return Component.displayName || Component.name || 'Component';
    }
    function withMiniSearch(documents, options, Component) {
        var WithMiniSearch = function (props) {
            var miniSearchProps = useMiniSearch(documents, options);
            return React__default['default'].createElement(Component, __assign({}, miniSearchProps, props));
        };
        WithMiniSearch.displayName = "WithMiniSearch(" + getDisplayName(Component) + ")";
        return WithMiniSearch;
    }
    var WithMiniSearch = function (_a) {
        var documents = _a.documents, options = _a.options, children = _a.children;
        var miniSearchProps = useMiniSearch(documents, options);
        return children(miniSearchProps);
    };

    exports.WithMiniSearch = WithMiniSearch;
    exports.useMiniSearch = useMiniSearch;
    exports.withMiniSearch = withMiniSearch;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=react-minisearch.js.map

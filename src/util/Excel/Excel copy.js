import React from "react";
import ReactExport from "react-data-export";
import { Button } from "reactstrap";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
//import multiDataSet from "./data";

var prueba = "DATOS DEL CLIENTE";
var prueba2 = "Denis osuna";

class Excel extends React.Component {
  state = {
    multiDataSet: [],
    isFetching: null,
    data: [],
    token: [],
  };

  componentDidMount() {
    this.getDatosExcel();
  }

  getDatosExcel = async () => {
    const formatter = new Intl.NumberFormat("de-DE");



    var tk = this.props.token;

    var res;
    var tipotk;
    var token;

    res = tk.split("-");
    tipotk = res[1].trim();
    token = res[0].trim();

 
    var transportes;
    var tipoCon;

    switch (tipotk) {
      case "FCL":
        transportes = "FLETE MARÍTIMO";
        tipoCon = "Maritimo - Completo";

        break;
      case "LCL":
        transportes = "FLETE MARÍTIMO";

        tipoCon = "Maritimo - Compartido";

        break;
      case "AER":
        transportes = "FLETE AÉREO";
        tipoCon = "Aereo";

        break;

      default:
    }

    let resumen2 = this.props.resumen;

    let impuestosBajosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    let impuestosMediosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    let impuestosAltosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });
    var Serviciologistico;
    let preServiciologistico = resumen2
      .filter(function (item) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
        ) {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    if (preServiciologistico.length > 1) {
      Serviciologistico = preServiciologistico
        .reduce(function (accumulator, current) {
          return [+accumulator + +current];
        })
        .map(function (duration) {
          return duration.toFixed(2);
        });
    } else {
      Serviciologistico = preServiciologistico;
    }

    let venta = resumen2.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS"
      ) {
        return item;
      }
    });

    let response = this.props.data;

    let flete = response
      .filter(function (item) {
        if (item.tipo == transportes) {
          return item;
        }
      })
      .filter(function (item) {
        if (item.valor > 0) {
          return item;
        }
      });

    let impuestosBajos = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let impuestosMedios = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let impuestosAltos = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let transporte = response.filter(function (item) {
      if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
        return item;
      } else {
        return false;
      }
    });

    let honorarios = response.filter(function (item) {
      if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
        return item;
      } else {
        return false;
      }
    });

    let seguro = response.filter(function (item) {
      if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
        return item;
      } else {
        return false;
      }
    });

    let servicioLogistico = response.filter(function (item) {
      if (
        item.tipo == "SERVICIO LOGÍSTICO" ||
        item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
        item.detalles == "Ganancia"
      ) {
        return item;
      } else {
        return false;
      }
    });

    let desgloseValuesCostos = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== transportes &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.detalles !== "Ganancia" &&
        item.tipo !== "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA"
      ) {
        if (item.valor > 0) {
          return item;
        }
      }
    });

    let desgloseValues = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== transportes &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.detalles !== "Ganancia"
      ) {
        if (item.valor !== 0) {
          return item;
        }
      }
    });

    let PretotalIGVCostos = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.tipo !== transportes &&
        item.tipo !== "FLETE" &&
        item.detalles !== "Ganancia" &&
        item.tipo !== "THCD" &&
        item.tipo !== "TCHD DOCUMENTACION" &&
        item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO"
      ) {
        if (item.valor > 0) {
          return item;
        }
      }
    });
    let totalIGVCostos = [0];

    if (PretotalIGVCostos.length > 1) {
      totalIGVCostos = PretotalIGVCostos.map(function (duration) {
        return (duration.valor * 0.18).toFixed(2);
      })
        .reduce(function (accumulator, current) {
          return [+accumulator + +current];
        })
        .map(function (duration) {
          return duration.toFixed(2);
        });
    }

    let desgloseTotal = response
      .filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
          item.tipo !== "SERVICIO LOGÍSTICO" &&
          item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
          item.detalles !== "Ganancia"
        ) {
          if (item.valor > 0) {
            return item;
          }
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      })
      .reduce(function (accumulator, current) {
        return [+accumulator + +current];
      });

    let preservicioLogisticoTotal = response
      .filter(function (item) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
          item.detalles == "Ganancia"
        ) {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor;
      });
    var servicioLogisticoTotal;
    if (preservicioLogisticoTotal.length > 1) {
      servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (
        accumulator,
        current
      ) {
        return [+accumulator + +current];
      });
    } else {
      servicioLogisticoTotal = preservicioLogisticoTotal;
    }

    let totalIGVFleteCostos =
      Number(desgloseTotal[0]) + Number(totalIGVCostos[0]);

    var totalVenta =
      Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);

    var naviera;
    switch (tipoCon) {
      case "Maritimo - Completo":
        {
          naviera = flete[0].detalles_calculos.naviera;
        }
        break;
      case "Maritimo - Compartido":
        {
          naviera = flete[0].detalles_calculos.agentes_lcl_nombre;
        }
        break;
      case "Aereo":
        {
          naviera = flete[0].detalles_calculos.agentes_aereo_nombre;
        }
        break;

      default:
    }



    if (localStorage.empresa !== undefined && localStorage.empresa.length > 0) {
      cliente = localStorage.empresa.toUpperCase();
    } else {
      cliente = data[1].toString();
    }

    correo = data[2].toString();

    if (
      localStorage.telefono !== undefined &&
      localStorage.telefono.length > 0
    ) {
      tlf = localStorage.telefono;
    }

    impuestos = JSON.parse(localStorage.taxesResumen);
    subtotal = JSON.parse(localStorage.subtotal);
    total = JSON.parse(localStorage.total);
    valido = JSON.parse(localStorage.fechas_vigencia);
    usd = impuestos[0] == "No cotizado" ? "" : "USD";

    if (localStorage.ruc !== undefined) {
      ruc = localStorage.ruc;
    } else {
      ruc = "No requerido";
    }

    const multiDataSet = [
      {
        ySteps: 1, //will put space of 5 rows,
        columns: [
          {
            title: "PIC",
            width: { wpx: 150 },

            style: {
              alignment: { vertical: "center", horizontal: "center" },
              font: {
                sz: "16",
                bold: true,
                vertAlign: true,
                color: { rgb: "b33641" },
              },
            },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          },
          {
            title: "INSTRUCTIVO COTIZACIÓN #: ID " + this.props.token,
            width: { wpx: 150 },
            style: {
              alignment: { vertical: "center", horizontal: "center" },
              font: { sz: "13", bold: true, vertAlign: true },
            },
          },
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //char width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
        ],
        data: [
          [
            { value: "" },
            { value: "" },
            { value: "" },
            { value: "" },
            { value: "" },
            { value: "" },
            { value: "" },
            { value: "" },
          ],
        ],
      },
      {
        columns: [
          {
            title: "",
            width: { wpx: 150 },
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
            },
          }, //pixels width
          {
            title: prueba,
            width: { wpx: 150 },
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
            },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
            },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: {
              fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
            },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: {
              font: { sz: "12", bold: true },
              fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
            },
          }, //char width
          {
            title: "PROFIT:",
            width: { wpx: 150 },
            style: {
              font: { sz: "14", bold: true },
              fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
            },
          }, //char width

          {
            title: "305,07",
            width: { wpx: 150 },
            style: {
              font: { sz: "14", bold: true },
              fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
            },
          }, //char width

          {
            title: "",
            width: { wpx: 150 },
            style: {
              font: { sz: "12", bold: true },
              fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
            },
          }, //char width
        ],
        data: [
          [
            { value: "CLIENTE:", style: { font: { sz: "10", bold: true } } },
            { value: cliente },
            { value: "" },
            { value: "" },
            {
              value: "",
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "INFORMACIÓN DE LA CARGA",
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
          ],
          [
            { value: "CONTACTO:", style: { font: { sz: "10", bold: true } } },
            { value: "" },
            { value: "" },
            { value: "" },
            {
              value: "Transporte: ",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: localStorage.transporte + " - " + localStorage.tipocon,
              style: { font: { sz: "10" } },
            },

            {
              value: "Valor Mercancia: ",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value:
                localStorage.value6 == "true"
                  ? formatter.format(localStorage.monto) + " " + "USD"
                  : "NO",
              style: { font: { sz: "10" } },
            },
          ],
          [
            { value: "TELF:", style: { font: { sz: "10", bold: true } } },
            { value: tlf },
            { value: "" },
            { value: "" },
            {
              value: "Bultos / Peso / M3:",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: localStorage.contenedoresDes,
              style: { font: { sz: "10" } },
            },

            {
              value: "Servicios / Impuestos de Aduana: ",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: localStorage.value6 == "true" ? "SI" : "NO",
              style: { font: { sz: "10" } },
            },
          ],

          [
            { value: "RUC:", style: { font: { sz: "10", bold: true } } },
            { value: ruc },
            { value: "" },
            { value: "" },
            {
              value: "Gastos portuarios:",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: localStorage.value5 == "true" ? "SI" : "NO",
              style: { font: { sz: "10" } },
            },

            {
              value: "Transporte a Domicilio: ",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value:
                localStorage.value7 == "true"
                  ? localStorage.ProvinciaN + " - " + localStorage.distritoN
                  : "NO",
              style: { font: { sz: "10" } },
            },
          ],
          [
            { value: "CORREO:", style: { font: { sz: "10", bold: true } } },
            { value: correo },
            { value: "" },
            { value: "" },
            {
              value: "Tipo de Productos:",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: "Alcohol Metalico",
              style: { font: { sz: "10" } },
            },

            {
              value: "Seguro de Mercancia: ",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: localStorage.value8 == "true" ? "SI" : "NO",
              style: { font: { sz: "10" } },
            },
          ],
          [
            {
              value: "Ejecutivo PIC CARGO:",
              style: { font: { sz: "10", bold: true } },
            },
            { value: "" },
            { value: "" },
            { value: "" },
            {
              value: "Permiso Gubernamental:",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value:
                localStorage.value6 == "true"
                  ? localStorage.regulador
                  : "No requerido",
              style: { font: { sz: "10" } },
            },

            {
              value: "",
              style: { font: { sz: "10", bold: true } },
            },
            {
              value: "",
              style: { font: { sz: "10" } },
            },
          ],
        ],
      },
      {
        columns: [
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
        ],
        data: [
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "INGRESOS",
              style: {
                font: {
                  sz: "15",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
          ],
          [
            {
              value: "TRAMO",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "ITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "PROVEEDOR",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "SUB ITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "SUB TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "IGV",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "SUB TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
          ],
          [
            {
              value: "FLETE",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "FLETE",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "PIC CARGO",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "12454",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "FLETE",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "173,62",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "NO APLICA",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
              },
            },
            {
              value: "173,62",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "TOTALES",
              style: {
                font: { sz: "13", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "1019,00",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "154,4",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "120",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "COSTOS",
              style: {
                font: {
                  sz: "15",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
          ],
          [
            {
              value: "TRAMO",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "ITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "PROVEEDOR",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "SUB ITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "SUB TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "IGV",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "SUB TOTAL",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
          ],
          [
            {
              value: "FLETE",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "FLETE MARITIMO LCL",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "LA NAVIERA CRAFT O MSL",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "120",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "FLETE",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "120",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
            {
              value: "NO APLICA",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
              },
            },
            {
              value: "173,62",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "10", vertAlign: true },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "",
              style: {
                font: { sz: "13", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "1019,00",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "154,4",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },

            {
              value: "120",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "GANANCIA",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "",
            },
            {
              value: "",
            },
            {
              value: "305,07",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  vertAlign: true,
                  bold: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
            },

            {
              value: "361,12",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  vertAlign: true,
                  bold: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "IGV POR PAGAR A SUNAT",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "",
            },
            {
              value: "",
            },
            {
              value: "35,07",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  vertAlign: true,
                  bold: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
            },

            {
              value: "31,12",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "13",
                  vertAlign: true,
                  bold: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },

            {
              value: "IMPUESTO DE ADUANA",
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "15",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
              },
            },
          ],
          [
            {
              value: "CONCEPTO",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "BAJO",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "MEDIOS",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "ALTO",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },
          ],
          [
            {
              value: "AD VALOREM",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "0",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },

            {
              value: "",
              style: {
                font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "233,784",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "428,6",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },
          ],
          [
            {
              value: "PERCEPCION",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "459,7752",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },

            {
              value: "",
              style: {
                font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "487,36",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "510,350472",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },
          ],
          [
            {
              value: "IGV",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "623,424",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },

            {
              value: "",
              style: {
                font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "660,82",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "692,0064",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },
          ],
          [
            {
              value: "IPM",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "77,928",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },

            {
              value: "",
              style: {
                font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
              },
            },
            {
              value: "82,60368",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
              },
            },
            {
              value: "86,5008",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
              },
            },
          ],
          [
            {
              value: "",
            },
            {
              value: "",
            },
            {
              value: "1.161",
            },

            {
              value: "",
            },
            {
              value: "1.465",
            },
            {
              value: "",
            },
            {
              value: "1.717",
            },

            {
              value: "",
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },

            {
              value: "INSTRUCCIONES ESPECIALES",
              style: {
                font: { sz: "15", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
          ],
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },

            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            },
          ],
        ],
      },
    ];
    this.setState({ isFetching: true });
    try {
      const response = await multiDataSet;
      this.setState({
        multiDataSet: response,
        isFetching: false,
        token: token,
      });
    } catch (error) {
      console.log(error);
      this.setState({ isFetching: false });
    }
  };

  render() {
    const { isFetching, multiDataSet } = this.state;
    return (
      <ExcelFile
        fileExtension="csv"
        name="excel2"
        filename="excelDatos"
        element={
          <button
            className="btn-style-blue ml-2"
            disabled={isFetching || isFetching === null}
          >
            Exportar a Excel
          </button>
        }
      >
        <ExcelSheet dataSet={multiDataSet} name="Datos_Excel" />
      </ExcelFile>
    );
  }
}

export default Excel;

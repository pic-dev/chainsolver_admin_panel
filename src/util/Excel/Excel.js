import React from "react";
import ReactExport from "react-data-export";
import { Button } from "reactstrap";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
//import multiDataSet from "./data";

var prueba = "DATOS DEL CLIENTE";
var prueba2 = "Denis osuna";

class Excel extends React.Component {
  state = {
    multiDataSet: [],
    isFetching: null,
    data: [],
    token: [],
  };

  componentDidMount() {
    this.getDatosExcel();
  }

  getDatosExcel = async () => {
    const formatter = new Intl.NumberFormat("de-DE");


    var tk = this.props.token;

    var res;
    var tipotk;
    var token;

    res = tk.split("-");
    tipotk = res[1].trim();
    token = res[0].trim();

 
    var transportes;
    var tipoCon;

    switch (tipotk) {
      case "FCL":
        transportes = "FLETE MARÍTIMO";
        tipoCon = "Maritimo - Completo";

        break;
      case "LCL":
        transportes = "FLETE MARÍTIMO";

        tipoCon = "Maritimo - Compartido";

        break;
      case "AER":
        transportes = "FLETE AÉREO";
        tipoCon = "Aereo";

        break;

      default:
    }

    let resumen2 = this.props.resumen;

    let impuestosBajosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    let impuestosMediosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    let impuestosAltosTotal = resumen2
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });
    var Serviciologistico;
    let preServiciologistico = resumen2
      .filter(function (item) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
        ) {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      });

    if (preServiciologistico.length > 1) {
      Serviciologistico = preServiciologistico
        .reduce(function (accumulator, current) {
          return [+accumulator + +current];
        })
        .map(function (duration) {
          return duration.toFixed(2);
        });
    } else {
      Serviciologistico = preServiciologistico;
    }

    let venta = resumen2.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS"
      ) {
        return item;
      }
    });

    let response = this.props.data;

    let flete = response
      .filter(function (item) {
        if (item.tipo == transportes) {
          return item;
        }
      })
      .filter(function (item) {
        if (item.valor > 0) {
          return item;
        }
      });

    let impuestosBajos = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let impuestosMedios = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let impuestosAltos = response
      .filter(function (item) {
        if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
          return item;
        } else {
          return false;
        }
      })
      .map(function (duration) {
        if (duration !== false) {
          return duration.valor.toFixed(2);
        } else {
          return duration;
        }
      });

    let transporte = response.filter(function (item) {
      if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
        return item;
      } else {
        return false;
      }
    });

    let honorarios = response.filter(function (item) {
      if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
        return item;
      } else {
        return false;
      }
    });

    let seguro = response.filter(function (item) {
      if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
        return item;
      } else {
        return false;
      }
    });

    let servicioLogistico = response.filter(function (item) {
      if (
        item.tipo == "SERVICIO LOGÍSTICO" ||
        item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
        item.detalles == "Ganancia"
      ) {
        return item;
      } else {
        return false;
      }
    });

    let desgloseValuesCostos = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== transportes &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.detalles !== "Ganancia" &&
        item.tipo !== "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA"
      ) {
        if (item.valor > 0) {
          return item;
        }
      }
    });

    let desgloseValues = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== transportes &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.detalles !== "Ganancia"
      ) {
        if (item.valor !== 0) {
          return item;
        }
      }
    });

    let PretotalIGVCostos = response.filter(function (item) {
      if (
        item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
        item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
        item.tipo !== "SERVICIO LOGÍSTICO" &&
        item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
        item.tipo !== transportes &&
        item.tipo !== "FLETE" &&
        item.detalles !== "Ganancia" &&
        item.tipo !== "THCD" &&
        item.tipo !== "TCHD DOCUMENTACION" &&
        item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO"
      ) {
        if (item.valor > 0) {
          return item;
        }
      }
    });
    let totalIGVCostos = [0];

    if (PretotalIGVCostos.length > 1) {
      totalIGVCostos = PretotalIGVCostos.map(function (duration) {
        return (duration.valor * 0.18).toFixed(2);
      })
        .reduce(function (accumulator, current) {
          return [+accumulator + +current];
        })
        .map(function (duration) {
          return duration.toFixed(2);
        });
    }

    let desgloseTotal = response
      .filter(function (item) {
        if (
          item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
          item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
          item.tipo !== "SERVICIO LOGÍSTICO" &&
          item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
          item.detalles !== "Ganancia"
        ) {
          if (item.valor > 0) {
            return item;
          }
        }
      })
      .map(function (duration) {
        return duration.valor.toFixed(2);
      })
      .reduce(function (accumulator, current) {
        return [+accumulator + +current];
      });

    let preservicioLogisticoTotal = response
      .filter(function (item) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
          item.detalles == "Ganancia"
        ) {
          return item;
        }
      })
      .map(function (duration) {
        return duration.valor;
      });
    var servicioLogisticoTotal;
    if (preservicioLogisticoTotal.length > 1) {
      servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (
        accumulator,
        current
      ) {
        return [+accumulator + +current];
      });
    } else {
      servicioLogisticoTotal = preservicioLogisticoTotal;
    }

    let totalIGVFleteCostos =
      Number(desgloseTotal[0]) + Number(totalIGVCostos[0]);

    var totalVenta =
      Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);

    var naviera;
    switch (tipoCon) {
      case "Maritimo - Completo":
        {
          naviera = flete[0].detalles_calculos.naviera;
        }
        break;
      case "Maritimo - Compartido":
        {
          naviera = flete[0].detalles_calculos.agentes_lcl_nombre;
        }
        break;
      case "Aereo":
        {
          naviera = flete[0].detalles_calculos.agentes_aereo_nombre;
        }
        break;

      default:
    }



 
    const multiDataSet = [
   
      {
        columns: [
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
        ],
        data: [
          [
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },

            {
              value: "GANANCIA",
              style: {
                font: {
                  sz: "15",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "ffffff" },
                },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
            {
              value: "",
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
              },
            },
          ],
          [
            {
              value: "ITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "SUBITEM",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "DETALLES",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "PROFIT",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "IGV",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
            {
              value: "PROFIT+IGV",
              style: {
                font: { sz: "10", bold: true, vertAlign: true },
                alignment: { vertical: "center", horizontal: "center" },
                fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
              },
            },
          ],

        ],
      },
    ];


    const multiDataSet3 = [
      {
        ySteps: 1, //will put space of 5 rows,
        columns: [
          {
            title: "PIC",
            width: { wpx: 150 },

            style: {
              alignment: { vertical: "center", horizontal: "center" },
              font: {
                sz: "16",
                bold: true,
                vertAlign: true,
                color: { rgb: "b33641" },
              },
            },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          },
          {
            title: "INSTRUCTIVO COTIZACIÓN #: ID " + this.props.token,
            width: { wpx: 150 },
            style: {
              alignment: { vertical: "center", horizontal: "center" },
              font: { sz: "13", bold: true, vertAlign: true },
            },
          },
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width

          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //char width
          {
            title: "",
            width: { wpx: 150 },
            style: { font: { sz: "12", bold: true } },
          }, //pixels width
        ],
        data: [
          [
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "33" },
            { value: "3" },
            { value: "3" },
          ],   [
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "3" },
            { value: "33" },
            { value: "3" },
            { value: "3" },
          ],
        ],
      },

    ];

    this.setState({ isFetching: true });
    try {
      const response = await multiDataSet2;
      this.setState({
        multiDataSet: response,
        isFetching: false,
        token: token,
      });
    } catch (error) {
      console.log(error);
      this.setState({ isFetching: false });
    }
  };

  render() {
    const { isFetching, multiDataSet } = this.state;
    return (
      <ExcelFile

        name={this.props.token}
        filename={this.props.token}
        element={
          <button
            className="btn-style-blue ml-2"
            disabled={isFetching || isFetching === null}
          >
            Exportar a Excel
          </button>
        }
      >
        <ExcelSheet dataSet={multiDataSet} name="Datos_Excel" />
      </ExcelFile>
    );
  }
}

export default Excel;

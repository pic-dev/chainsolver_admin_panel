import React from 'react'

export function multiDataSet(customers) {

  var prueba = "DATOS DEL CLIENTE";
  var prueba2 = "Denis osuna";

   const multiDataSet = [
        {
          ySteps: 1, //will put space of 5 rows,
          columns: [
            {
              title: "PIC",
              width: { wpx: 150 },
      
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: {
                  sz: "16",
                  bold: true,
                  vertAlign: true,
                  color: { rgb: "b33641" },
                },
              },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
      
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            },
            {
              title: "INSTRUCTIVO CORIZACIÓN #: ID " + customers ,
              width: { wpx: 150 },
              style: {
                alignment: { vertical: "center", horizontal: "center" },
                font: { sz: "12", bold: true, vertAlign: true },
              },
            },
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
      
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //char width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
          ],
          data: [
            [
              { value: "" },
              { value: "" },
              { value: "" },
              { value: "" },
              { value: "" },
              { value: "" },
              { value: "" },
              { value: "" },
            ],
          ],
        },
        {
          columns: [
            {
              title: "",
              width: { wpx: 150 },
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            }, //pixels width
            {
              title:prueba,
              width: { wpx: 150 },
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: {
                fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
              },
            }, //pixels width
      
            {
              title: "",
              width: { wpx: 150 },
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
              },
            }, //char width
            {
              title: "PROFIT:",
              width: { wpx: 150 },
              style: {
                font: { sz: "14", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
              },
            }, //char width
      
            {
              title: "305,07",
              width: { wpx: 150 },
              style: {
                font: { sz: "14", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
              },
            }, //char width
      
            {
              title: "",
              width: { wpx: 150 },
              style: {
                font: { sz: "12", bold: true },
                fill: { patternType: "solid", fgColor: { rgb: "f1f1f1" } },
              },
            }, //char width
          ],
          data: [
            [
              { value: "CLIENTE:", style: { font: { sz: "10", bold: true } } },
              { value: prueba2 },
              { value: "" },
              { value: "" },
              {
                value: "",
                style: {
                  font: { sz: "12", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "INFORMACIÓN DE LA CARGA",
                style: {
                  font: { sz: "12", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  font: { sz: "12", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  font: { sz: "12", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
            ],
            [
              { value: "CONTACTO:", style: { font: { sz: "10", bold: true } } },
              { value: "" },
              { value: "" },
              { value: "" },
              {
                value: "Transporte: ",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: " Maritimo - Compartido",
                style: { font: { sz: "10" } },
              },
      
              {
                value: "Valor Mercancia: ",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "3500",
                style: { font: { sz: "10" } },
              },
            ],
            [
              { value: "TELF:", style: { font: { sz: "10", bold: true } } },
              { value: "" },
              { value: "" },
              { value: "" },
              {
                value: "Bultos / Peso / M3:",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "10 butlos /125 kgs / 2m3",
                style: { font: { sz: "10" } },
              },
      
              {
                value: "Servicios / Impuestos de Aduana: ",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "SI",
                style: { font: { sz: "10" } },
              },
            ],
      
            [
              { value: "RUC:", style: { font: { sz: "10", bold: true } } },
              { value: "" },
              { value: "" },
              { value: "" },
              {
                value: "Gastos portuarios:",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "SI",
                style: { font: { sz: "10" } },
              },
      
              {
                value: "Transporte a Domicilio: ",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "SI",
                style: { font: { sz: "10" } },
              },
            ],
            [
              { value: "CORREO:", style: { font: { sz: "10", bold: true } } },
              { value: "" },
              { value: "" },
              { value: "" },
              {
                value: "Tipo de Productos:",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "Alcohol Metalico",
                style: { font: { sz: "10" } },
              },
      
              {
                value: "Seguro de Mercancia: ",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "SI",
                style: { font: { sz: "10" } },
              },
            ],
            [
              {
                value: "Ejecutivo PIC CARGO:",
                style: { font: { sz: "10", bold: true } },
              },
              { value: "" },
              { value: "" },
              { value: "" },
              {
                value: "Permiso Gubernamental:",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "No requerido",
                style: { font: { sz: "10" } },
              },
      
              {
                value: "",
                style: { font: { sz: "10", bold: true } },
              },
              {
                value: "",
                style: { font: { sz: "10" } },
              },
            ],
          ],
        },
        {
          columns: [
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
      
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
      
            {
              title: "",
              width: { wpx: 150 },
              style: { font: { sz: "12", bold: true } },
            }, //pixels width
          ],
          data: [
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
      
              {
                value: "INGRESOS",
                style: {
                  font: {
                    sz: "15",
                    bold: true,
                    vertAlign: true,
                    color: { rgb: "ffffff" },
                  },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
            ],
            [
              {
                value: "TRAMO",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "ITEM",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "PROVEEDOR",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "SUB ITEM",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "SUB TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "IGV",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "SUB TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
            ],
            [
              {
                value: "FLETE",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "FLETE",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "PIC CARGO",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "1173,62",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "FLETE",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "173,62",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "NO APLICA",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                },
              },
              {
                value: "173,62",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "TOTALES",
                style: {
                  font: { sz: "13", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "1019,00",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "154,4",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "120",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
      
              {
                value: "COSTOS",
                style: {
                  font: {
                    sz: "15",
                    bold: true,
                    vertAlign: true,
                    color: { rgb: "ffffff" },
                  },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
            ],
            [
              {
                value: "TRAMO",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "ITEM",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "PROVEEDOR",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "SUB ITEM",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "SUB TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "IGV",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "SUB TOTAL",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
            ],
            [
              {
                value: "FLETE",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "FLETE MARITIMO LCL",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "LA NAVIERA CRAFT O MSL",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "120",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "FLETE",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "120",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
              {
                value: "NO APLICA",
                style: {
                  font: { sz: "10", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                },
              },
              {
                value: "173,62",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: { sz: "10", vertAlign: true },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "",
                style: {
                  font: { sz: "13", bold: true },
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "1019,00",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
              {
                value: "154,4",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
      
              {
                value: "120",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "cfe7fe" } },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "GANANCIA",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    bold: true,
                    vertAlign: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
      
              {
                value: "",
              },
              {
                value: "",
              },
              {
                value: "305,07",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    vertAlign: true,
                    bold: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
              },
      
              {
                value: "361,12",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    vertAlign: true,
                    bold: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "IGV POR PAGAR A SUNAT",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    bold: true,
                    vertAlign: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
      
              {
                value: "",
              },
              {
                value: "",
              },
              {
                value: "35,07",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    vertAlign: true,
                    bold: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
              {
                value: "",
              },
      
              {
                value: "31,12",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "13",
                    vertAlign: true,
                    bold: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "0d5084" } },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
      
              {
                value: "IMPUESTO DE ADUANA",
                style: {
                  alignment: { vertical: "center", horizontal: "center" },
                  font: {
                    sz: "15",
                    bold: true,
                    vertAlign: true,
                    color: { rgb: "ffffff" },
                  },
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "b33641" } },
                },
              },
            ],
            [
              {
                value: "CONCEPTO",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "BAJO",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
      
              {
                value: "",
                style: {
                 fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "MEDIOS",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "ALTO",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
            ],
            [
              {
                value: "AD VALOREM",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "0",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
      
              {
                value: "",
                style: {
                  font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "233,784",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "428,6",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
            ],
            [
              {
                value: "PERCEPCION",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "459,7752",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
      
              {
                value: "",
                style: {
                  font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "487,36",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "510,350472",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
            ],
            [
              {
                value: "IGV",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "623,424",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
      
              {
                value: "",
                style: {
                  font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "660,82",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "692,0064",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
            ],
            [
              {
                value: "IPM",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "77,928",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
      
              {
                value: "",
                style: {
                  font: { sz: "15", bold: true, color: { rgb: "ffffff" } },
                  fill: { patternType: "solid", fgColor: { rgb: "d0e4f7" } },
                },
              },
              {
                value: "82,60368",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d0f7d1" } },
                },
              },
              {
                value: "86,5008",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "eac3c3" } },
                },
              },
            ],
            [
              {
                value: "",
              },
              {
                value: "",
              },
              {
                value: "1.161",
              },
      
              {
                value: "",
              },
              {
                value: "1.465",
              },
              {
                value: "",
              },
              {
                value: "1.717",
              },
      
              {
                value: "",
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
      
              {
                value: "INSTRUCCIONES ESPECIALES",
                style: {
                  font: { sz: "15", bold: true, vertAlign: true },
                  alignment: { vertical: "center", horizontal: "center" },
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
            ],
            [
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
      
              {
                value: "",
                style: {
                  fill: { patternType: "solid", fgColor: { rgb: "d7d7d7" } },
                },
              },
            ],
          ],
        },
      ];
  return multiDataSet;
    }
import React, { Component, Fragment } from "react";
import Item from "./components/Item";
import _ from "lodash";
import "./App.css";
import {
  Col,
  Row,
  Form,
  Label,
  Badge,
  Input,
  Button,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  ListGroup,
  ListGroupItem,
  UncontrolledTooltip,
  FormText,
} from "reactstrap";

import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";
import scrollIntoView from "scroll-into-view-if-needed";
import { Event } from "./tracking";
let valid;
let selected1;
let selected2;
let selected3;
let selected4;
let selected5;
let selected6;
let selected7;

let timer;
class App extends Component {
  constructor(props) {
    super(props);
    this.handleFormState = this.handleFormState.bind(this);
    this.calcular = this.calcular.bind(this);
    this.add = this.add.bind(this);
    this.showMessage = this.showMessage.bind(this);
    this.scrollToTop = this.scrollToTop.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);

    this.state = {
      showMessage: false,
      showMessage2: false,

      data: [],
      form: [
        { Label: "LARGO", value: "" },
        { Label: "ANCHO", value: "" },
        { Label: "ALTO", value: "" },
        { Label: "PESO", value: "" },
        { Label: "BULTOS", value: "" },
        { Label: "UNIDADMEDIDA", value: "" },
        { Label: "UNIDADPESO", value: "" },
        { Label: "TOTAL", value: "" },
        { Label: "pesoAmetros", value: "" },
      ],
    };
  }

  componentDidMount() {
    if (localStorage.data !== undefined) {
      if (localStorage.data.length > 0) {
        this.setState({ data: JSON.parse(localStorage.getItem("data")) });
      }
    }

    /*if (localStorage.transporte == "MARITÍMO") {
      Event(
        "1.2 Ingreso Calculadora LCL",
        "1.2 Ingreso Calculadora LCL",
        "RUTA"
      );
    } else {
      Event(
        "3.2 Ingreso Calculadora Aereo",
        "3.2 Ingreso Calculadora Aereo",
        "RUTA"
      );
    }*/
  }
  scrollToTop() {
    var element = document.getElementById("up");

    scrollIntoView(element, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
  }

  scrollToBottom() {
    let element2;
    element2 = document.getElementById("down");

    scrollIntoView(element2, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
  }

  showMessage(e) {
    if (e == 1) {
      this.setState((ps) => ({ showMessage: true }));
      timer = setTimeout(() => {
        this.setState((ps) => ({ showMessage: false }));
      }, 15000);
    } else {
      this.setState((ps) => ({ showMessage2: true }));
      timer = setTimeout(() => {
        this.setState((ps) => ({ showMessage2: false }));
      }, 15000);
    }
  }

  handleFormState(input) {
    let newState = this.state.form;
    if (input.value < 0) {
      newState[input.id].value = "";

      this.setState({
        form: newState,
      });
    } else {
      newState[input.id].value = input.value;

      this.setState({
        form: newState,
      });
    }
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  _getPesoToKg() {
    /* form: [
        { Label: "LARGO", value: "" },
        { Label: "ANCHO", value: "" },
        { Label: "ALTO", value: "" },
        { Label: "PESO", value: "" },
        { Label: "BULTOS", value: "" },
        { Label: "UNIDADMEDIDA", value: "" },
        { Label: "UNIDADPESO", value: "" },
        { Label: "TOTAL", value: "" },
        { Label: "pesoAmetros", value: "" },
      ]*/

    let unidad;
    //discriminamos por unidad de peso
    switch (this.state.form[6].value) {
      case "kg":
        return (unidad = this.state.form[3].value);

        break;
      case "lb":
        let cantidad = this.state.form[3].value;
        let dividir = 1 / 2.2;
        return (unidad = parseFloat(cantidad * dividir).toFixed(2));

        break;
      case "Tn":
        return (unidad = this.state.form[3].value * 1000).toFixed(2);

        break;

      default:
      // code block
    }
  }

  calcular() {
    /* form: [
        { Label: "LARGO", value: "" },
        { Label: "ANCHO", value: "" },
        { Label: "ALTO", value: "" },
        { Label: "PESO", value: "" },
        { Label: "BULTOS", value: "" },
        { Label: "UNIDADMEDIDA", value: "" },
        { Label: "UNIDADPESO", value: "" },
        { Label: "TOTAL", value: "" },
        { Label: "pesoAmetros", value: "" },
      ]*/
      
    let resultado = 0;
    let x = this.state.form[5].value.toString();
    //se hace el calculo dependiento de la unidad de medida seleccionada, multiplicado LARGO, ANCHO, ALTO * BULTOS
    switch (x) {
      case "cm":
        resultado =
          (this.state.form[0].value / 100) *
          (this.state.form[1].value / 100) *
          (this.state.form[2].value / 100) *
          this.state.form[4].value;
        break;
      case "in":
        resultado =
          (this.state.form[0].value / 39.37) *
          (this.state.form[1].value / 39.37) *
          (this.state.form[2].value / 39.37) *
          this.state.form[4].value;
        break;
      case "ft":
        resultado =
          (this.state.form[0].value / 0.3048) *
          (this.state.form[1].value / 0.3048) *
          (this.state.form[2].value / 0.3048) *
          this.state.form[4].value;
        break;
      case "mm":
        resultado =
          (this.state.form[0].value / 1000) *
          (this.state.form[1].value / 1000) *
          (this.state.form[2].value / 1000) *
          this.state.form[4].value;
        break;
      case "m":
        resultado =
          this.state.form[0].value *
          this.state.form[1].value *
          this.state.form[2].value *
          this.state.form[4].value;
        break;
      default:
      // code block
    }

    let newState = this.state.form;
    //Se redondea total de m3
    newState[7].value = Math.round(resultado * 100) / 100;
    //Se calcula total de peso * bultos
    newState[8].value =
      (this._getPesoToKg() * this.state.form[4].value * 1000) / 1000;
    this.setState({
      form: newState,
    });
    this.add();
  }

  _remove(position) {
    let { data } = this.state;

    let newData = [...data.slice(0, position), ...data.slice(position + 1)];

    this.setState({ data: newData });
    localStorage.setItem("data", JSON.stringify(newData));
  }

  add() {
    if (
      this.state.form[7].value > 0.000017 &&
      this.state.form[3].value > 0 &&
      this.state.form[5].value.length > 0 &&
      this.state.form[6].value.length > 0
    ) {
      this.scrollToBottom();
      valid = "3";
      let { data } = this.state;

      let newData = [
        ...data,
        {
          Bultos: parseInt(this.state.form[4].value, 10),
          UNIDADMEDIDA: this.state.form[5].value,
          UNIDADPESO: this.state.form[6].value,
          peso: this.state.form[8].value,
          volumen: this.state.form[7].value,
          pesoVolumen: this.state.form[7].value * 166.7,
        },
      ];
      this.setState({ data: newData });

      let eraseForm = [
        { Label: "LARGO", value: "" },
        { Label: "ANCHO", value: "" },
        { Label: "ALTO", value: "" },
        { Label: "PESO", value: "" },
        { Label: "BULTOS", value: "" },
        { Label: "UNIDADMEDIDA", value: this.state.form[5].value },
        { Label: "UNIDADPESO", value: this.state.form[6].value },
        { Label: "TOTAL", value: "" },
        { Label: "pesoAmetros", value: "" },
      ];

      localStorage.setItem("data", JSON.stringify(newData));
      this.setState({
        form: eraseForm,
        showMessage: false,
        showMessage2: false,
      });
    } else if (
      this.state.form[7].value == 0 &&
      this.state.form[0].value.length > 0 &&
      this.state.form[1].value.length > 0 &&
      this.state.form[2].value.length > 0 &&
      this.state.form[3].value.length > 0 &&
      this.state.form[4].value.length > 0 &&
      this.state.form[5].value.length > 0 &&
      this.state.form[6].value.length > 0
    ) {
      this.scrollToTop();
      valid = "2";
      this.showMessage(2);
    } else {
      this.scrollToTop();
      valid = "1";
      this.showMessage(1);
    }
  }
  _getBultosTotal() {
    if (this.state.data.length > 0) {
      return _.sumBy(this.state.data, function (o) {
        return o.Bultos;
      });
    } else if (this.state.form[7].value > 0 || this.state.data.length == 0) {
      return this.state.form[4].value;
    } else {
    }
  }

  _getPesoTotal() {
    if (this.state.data.length > 0) {
      return _.sumBy(this.state.data, function (o) {
        return o.peso;
      });
    } else if (this.state.form[7].value > 0) {
      return this._getPesoToKg() * this.state.form[4].value;
    } else if (this.state.data.length == 0) {
      return this.state.form[4].value;
    } else {
    }
  }

  _getVolumenTotal() {
    if (this.state.data.length > 0) {
      return _.sumBy(this.state.data, function (o) {
        return Math.round(o.volumen * 100) / 100;
      });
    } else if (this.state.form[7].value > 0 || this.state.data.length == 0) {
      return this.state.form[7].value;
    } else {
    }
  }

  render() {
    localStorage.setItem("bultosTotal", JSON.stringify(this._getBultosTotal()));
    localStorage.setItem("pesoTotal", JSON.stringify(this._getPesoTotal()));
    localStorage.setItem(
      "volumenTotal",
      JSON.stringify(this._getVolumenTotal())
    );
    localStorage.setItem(
      "PesovolumenTotal",
      JSON.stringify(this._getVolumenTotal())
    );

    if (valid == "2") {
      selected1 = true;
      selected2 = true;
      selected3 = true;
      timer = setTimeout(() => {
        selected1 = false;
        selected2 = false;
        selected3 = false;
        valid = "1";
      }, 3000);
    } else {
    }

    if (valid === "1") {
      if (this.state.form[0].value > 0) {
        selected1 = false;
      } else {
        selected1 = true;
      }
    } else {
    }

    if (valid === "1") {
      if (this.state.form[1].value > 0) {
        selected2 = false;
      } else {
        selected2 = true;
      }
    } else {
    }

    if (valid === "1") {
      if (this.state.form[2].value > 0) {
        selected3 = false;
      } else {
        selected3 = true;
      }
    } else {
    }

    if (valid === "1") {
      if (this.state.form[3].value > 0) {
        selected4 = false;
      } else {
        selected4 = true;
      }
    } else {
      selected4 = false;
    }

    if (valid === "1") {
      if (this.state.form[4].value > 0) {
        selected5 = false;
      } else {
        selected5 = true;
      }
    } else {
      selected5 = false;
    }

    if (valid === "1") {
      if (this.state.form[5].value.length > 0) {
        selected6 = false;
      } else {
        selected6 = true;
      }
    } else {
      selected6 = false;
    }

    if (valid === "1") {
      if (this.state.form[6].value.length > 0) {
        selected7 = false;
      } else {
        selected7 = true;
      }
    } else {
      selected7 = false;
    }

    return (
      <div>
        <Col
          className="text-center mb-2 recomendationCalculatorTitle"
          xs={12}
          md={12}
        >
          <div id="up">     <i
            onClick={this.toggleCalculator}
            className="plusLump  simple-icon-question "
            id="recomendationTooltip"
            style={{
              position: "absolute",
              right: "0.5em",
              top: "0.3em",
              cursor: "pointer",
            }}
          /></div>
    
          <UncontrolledTooltip
            className="color-blue"
            placement="right"
            target="recomendationTooltip"
          >
            <div className=" color-blue text-justify">
              Agrupar por bultos que tengan la misma unidad de peso y volumen.
            </div>
            <div className="color-blue mt-2 text-justify">
              Ejemplo: Bultos en centímetros NO MEZCLAR con bultos que estén en
              metros.
            </div>
            <div className="color-blue mt-2 text-justify">
              No mezclar bultos que tengan el peso en kilos con otros que esten
              en libras.
            </div>
            <div className="color-blue mt-2 text-justify">
              Si tenemos alguno de los ejemplos antes mencionados agregar un
              bulto adicional.
            </div>
          </UncontrolledTooltip>
        </Col>
        <Form className="text-center" style={{ padding: "0em 2em 0em 2em" }}>
          {this.state.showMessage ? (
            <div
              style={{ position: "sticky !important" }}
              className="alert alert-danger text-center arial  "
              role="alert"
            >
              Debes completar los campos marcados en rojo.
            </div>
          ) : (
            ``
          )}
          {this.state.showMessage2 ? (
            <div
              style={{ position: "sticky !important" }}
              className="alert alert-danger text-center arial  mx-auto"
              role="alert"
            >
              Los valores de longitud (largo, ancho y alto) introducidos son muy
              bajos pàra realizar el calculo.
            </div>
          ) : (
            ``
          )}

          <Row form>
            <Row>
              <Col className="NoPadding" xs={6} md={4}>
                <FormGroup>
                  <Label for="UNIDAD"> UNIDAD DE LONGITUD</Label>
                  <Input
                    type="select"
                    invalid={selected6}
                    name="select"
                    onChange={(e) => {
                      this.handleFormState({
                        id: 5,
                        value: e.target.value || "",
                      });
                    }}
                  >
                    <option value="">Elige una opción</option>
                    <option key="0" value="cm">
                      Centímetros
                    </option>
                    <option key="1" value="in">
                      Pulgadas
                    </option>
                    <option key="2" value="ft">
                      Pies
                    </option>
                    <option key="3" value="mm">
                      Milímetro
                    </option>
                    <option key="4" value="m">
                      Metros
                    </option>
                  </Input>
                  {selected6 ? (
                    <FormText className="validation-text">
                      Campo requerido
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col xs={6} md={4}>
                <FormGroup>
                  <Label for="UNIDADPESO">UNIDAD DE PESO</Label>
                  <Input
                    type="select"
                    name="select"
                    invalid={selected7}
                    onChange={(e) => {
                      this.handleFormState({
                        id: 6,
                        value: e.target.value || "",
                      });
                    }}
                  >
                    <option value="">Elige una opción</option>
                    <option key="0" value="kg">
                      Kilogramos
                    </option>
                    <option key="1" value="lb">
                      Libras
                    </option>
                    <option key="2" value="Tn">
                      Toneladas
                    </option>
                  </Input>
                  {selected7 ? (
                    <FormText className="validation-text">
                      Campo requerido
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col
                className="display-sm pb-1 pt-1 mb-1 arial color-blue"
                style={{
                  backgroundColor: "#e1e2e2",
                  fontWeight: "bold",
                }}
                xs={12}
                md={12}
              >
                <span>NRO DE BULTOS</span>
              </Col>
              <Col xs={12} md={4}>
                <FormGroup>
                  <Label className="display-large" for="BULTOS">
                    NRO DE BULTOS
                  </Label>
                  <Input
                    type="number"
                    invalid={selected5}
                    placeholder="BULTOS"
                    bsSize="sm"
                    value={this.state.form[4].value}
                    onChange={(e) => {
                      this.handleFormState({
                        id: 4,
                        value: e.target.value || "",
                      });
                    }}
                  />
                  {selected5 ? (
                    <FormText className="validation-text">
                      Campo requerido
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col
                className="pt-1 pb-1 mb-1 arial color-blue"
                style={{
                  backgroundColor: "#e1e2e2",
                  fontWeight: "bold",
                }}
                xs={12}
                md={12}
              >
                <span>MEDIDAS POR CADA BULTO</span>
              </Col>
            </Row>
            <Row>
              <Col xs={6} md={3}>
                <FormGroup>
                  <Label for="LARGO">LARGO</Label>
                  <InputGroup>
                    <Input
                      invalid={selected1}
                      type="number"
                      placeholder="LARGO"
                      bsSize="sm"
                      value={this.state.form[0].value}
                      onChange={(e) => {
                        this.handleFormState({
                          id: 0,
                          value: e.target.value || "",
                        });
                      }}
                    />

                    <InputGroupAddon addonType="append">
                      <InputGroupText>
                        {this.state.form[5].value}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {selected1 ? (
                    <FormText className="validation-text">
                      Ingrese un valor valido.
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col xs={6} md={3}>
                <FormGroup>
                  <Label for="ANCHO">ANCHO</Label>
                  <InputGroup>
                    <Input
                      type="number"
                      invalid={selected2}
                      placeholder="ANCHO"
                      bsSize="sm"
                      value={this.state.form[1].value}
                      onChange={(e) => {
                        this.handleFormState({
                          id: 1,
                          value: e.target.value || "",
                        });
                      }}
                    />

                    <InputGroupAddon addonType="append">
                      <InputGroupText>
                        {this.state.form[5].value}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {selected2 ? (
                    <FormText className="validation-text">
                      Ingrese un valor valido.
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col xs={6} md={3}>
                <FormGroup>
                  <Label for="ALTO">ALTO</Label>
                  <InputGroup>
                    <Input
                      type="number"
                      invalid={selected3}
                      placeholder="ALTO"
                      bsSize="sm"
                      value={this.state.form[2].value}
                      onChange={(e) => {
                        this.handleFormState({
                          id: 2,
                          value: e.target.value || "",
                        });
                      }}
                    />

                    <InputGroupAddon addonType="append">
                      <InputGroupText>
                        {this.state.form[5].value}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {selected3 ? (
                    <FormText className="validation-text">
                      Ingrese un valor valido.
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
              <Col xs={6} md={3}>
                <FormGroup>
                  <Label for="PESO">PESO</Label>
                  <InputGroup>
                    <Input
                      type="number"
                      invalid={selected4}
                      placeholder="PESO"
                      bsSize="sm"
                      value={this.state.form[3].value}
                      onChange={(e) => {
                        this.handleFormState({
                          id: 3,
                          value: e.target.value || "",
                        });
                      }}
                    />

                    <InputGroupAddon addonType="append">
                      <InputGroupText>
                        {this.state.form[6].value}
                      </InputGroupText>
                    </InputGroupAddon>
                  </InputGroup>
                  {selected4 ? (
                    <FormText className="validation-text">
                      Campo requerido
                    </FormText>
                  ) : (
                    ``
                  )}
                </FormGroup>
              </Col>
            </Row>
          </Row>
        </Form>

        <Col xs={12} md={12} className="pt-2 text-center">
          <FormGroup>
            <Button
              size="sm"
              className="mt-3  btn-style"
              color="success"
              onClick={this.calcular}
            >
              Calcular
            </Button>
          </FormGroup>
        </Col>

        <Colxx
          className="color-white"
          style={{ padding: "1em 0em" }}
          xxs="12"
          md="12"
          sm="12"
        >
          <ListGroup>
            <ListGroupItem
              className="mb-2"
              style={{
                fontWeight: "bold",
                padding: "0px",
                color: "#000000",
                backgroundColor: "#e1e2e2",
                fontSize: "1.2em",
                display: this.state.data.length > 0 ? "" : "none",
              }}
            >
              <Row>
                <Colxx
                  xxs="1"
                  md="1"
                  className="NoPadding mx-auto my-auto "
                ></Colxx>
                <Colxx xxs="2" md="2" className="text-center mx-auto my-auto ">
                  Item
                </Colxx>
                <Colxx xxs="3" md="3" className="mx-auto  text-center my-auto ">
                  Bultos
                </Colxx>
                <Colxx xxs="3" md="3" className="mx-auto  text-center my-auto ">
                  Peso
                </Colxx>
                <Colxx xxs="3" md="3" className="mx-auto text-center my-auto ">
                  Volumen
                </Colxx>
              </Row>
            </ListGroupItem>
            {this.state.data.map((item, index) => (
              <Item
                data={item}
                tablePosition={index}
                key={index}
                onRemove={() => this._remove(index)}
              />
            ))}
          </ListGroup>
          <Colxx className="mb-2 mt-2 text-right " xxs="12" md="12" sm="12">
            <div
              onClick={this.scrollToTop}
              style={{
                cursor: "pointer",
                color: "rgb(44, 123, 157)",
                fontSize: "1.4em",
                display: this.state.data.length > 0 ? "" : "none",
              }}
            >
              <i className="plusLump mr-2 simple-icon-plus " /> Agregar otro
              bulto{" "}
            </div>
          </Colxx>
          <Colxx
            className="fontNavbar color-orange mb-2 mt-2 text-right "
            xxs="12"
            md="12"
            sm="12"
          >
            <div
              style={{
                fontSize: "1rem",
                textAlign: "justify",
                fontWeight: "bold",
              }}
            >
              Nota: Los valores de Peso y Volumen serán convertidos
              automáticamente a Kilogramos y Metros respectivamente.
            </div>
          </Colxx>
        </Colxx>

        <Colxx
          id="down"
          style={{
            backgroundColor: "#0d5084",
            padding: "0.2em 1em",
            color: "white",
            fontWeight: "bold",
          }}
          xxs="12"
          md="12"
          sm="12"
        >
          <Form>
            {localStorage.transporte == "AÉREO" ? (
              <Row form>
                <Col
                  style={{
                    fontSize: "1.2em",
                    textDecoration: "underline",
                  }}
                  className="text-center"
                  xs={12}
                  md={12}
                >
                  <div>TOTAL</div>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Bultos">BULTOS</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Bultos"
                        id="Bultos"
                        value={this._getBultosTotal()}
                        placeholder="Bultos"
                        disabled
                      />
                    </InputGroup>
                  </FormGroup>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Peso">PESO</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Peso"
                        id="Peso"
                        value={this._getPesoTotal()}
                        placeholder="Peso"
                        disabled
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>kg</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Volumen">VOLUMEN</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Volumen"
                        id="Volumen"
                        value={this._getVolumenTotal()}
                        placeholder="Volumen"
                        disabled
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>M³</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                </Col>
              </Row>
            ) : (
              <Row form>
                <Col
                  style={{
                    fontSize: "1.2em",
                    textDecoration: "underline",
                  }}
                  className="text-center"
                  xs={12}
                  md={12}
                >
                  <div>TOTAL</div>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Bultos">BULTOS</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Bultos"
                        id="Bultos"
                        value={this._getBultosTotal()}
                        placeholder="Bultos"
                        disabled
                      />
                    </InputGroup>
                  </FormGroup>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Peso">PESO</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Peso"
                        id="Peso"
                        value={this._getPesoTotal()}
                        placeholder="Peso"
                        disabled
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>kg</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                </Col>
                <Col xs={4} md={4}>
                  <FormGroup>
                    <Label for="Volumen">VOLUMEN</Label>

                    <InputGroup>
                      <Input
                        type="number"
                        name="Volumen"
                        id="Volumen"
                        value={this._getVolumenTotal()}
                        placeholder="Volumen"
                        disabled
                      />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>M³</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                </Col>
              </Row>
            )}
          </Form>
        </Colxx>
      </div>
    );
  }
}

export default App;

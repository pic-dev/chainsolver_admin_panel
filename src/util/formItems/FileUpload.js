import React, { Component } from "react";
import { CustomInput,FormGroup, Label } from "reactstrap";

class FileUpload extends Component {
  constructor() {
    super();
    this.state = {
      uploadValue: 0,
    };
  }

  render() {
    return (
      <div>
        <progress value={this.props.uploadValue} max="100">
          {this.props.uploadValue} %
        </progress>
        <br />
      <FormGroup>
        <Label for="exampleCustomFileBrowser">Elige archivos multimedia </Label>
        <CustomInput type="file" id="exampleCustomFileBrowser" name="customFile" label="Elige archivos" onChange={this.props.onUpload} />
      </FormGroup>
      </div>
    );
  }
}

export default FileUpload;

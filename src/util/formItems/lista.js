import React, { Component } from "react";
 

  function TodoItems(props) {
      var todoEntries = props.entries;

  
 
    return (
      <ul className="theList">
           {todoEntries
            .map((item) => (
              <li key={item.key}>{item.text}</li>
            ))}
      </ul>
    );
 
}
 
export default TodoItems;
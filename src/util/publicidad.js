/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState } from "react";
import {  Modal,  ModalBody, ModalFooter } from "reactstrap";

const ModalExample = (props) => {
  const [modal, setModal] = useState(true);

  const toggle = () => setModal(!modal);



  const externalCloseBtn = (
    <button
      className="close"
      style={{ position: "absolute", top: "15px", right: "15px" }}
      onClick={toggle}
    >
      &times;
    </button>
  );
  return (
    <div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        className="ModalPublicidad"

      >
        <ModalBody style={{ borderRadius: "0", padding: "0" }}>
        <button
      className="close"
      style={{ position: "absolute", top: "5px", right: "5px", 
      fontSize: "2rem",
      backgroundColor: "#0c2d47",
      borderRadius: "50%",
      padding: ".1rem",
      width: "2rem",
      fontWeight: "bold",
      opacity: "1.5" }}
      onClick={toggle}
    >
      &times;
    </button>
          <div
               className="img"
     
          />
        </ModalBody>
        <ModalFooter
          style={{
            alignSelf: "center",
            width: "100%",
            justifyContent: "center",
          }}
          className="text-center btn-style-blue p-3"
        >
          <a
            href="https://fb.me/e/j7gdeIlDn"
            target="_blank"
            rel="noopener noreferrer"
            className="fontPublicidad"
            style={{ color: "#ffffff", fontWeight: "500" }}
          >
            Únete GRATIS Transmisión en Vivo, Facebook 30 Enero <div className="botonPalpitante arial">Click Aquí</div>
          </a>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalExample;

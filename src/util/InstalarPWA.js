import React, { useEffect, useState } from "react";
import PWAInstallerPrompt from "react-pwa-installer-prompt";
import { firebaseConf } from "./Firebase";
import { getCurrentTime, getDateWithFormat } from "./Utils";
import { Event } from "./tracking";

const Install = (props) => {
  const [isInstalled, setIsInstalled] = useState(true);
  const [PWAStatus, setPWAStatus] = useState([]);

  function onChange() {
    setIsInstalled(false);
    sessionStorage.pwaNotInstalled = true;
    Event("App No instalada", "App No instalada", "INICIO");
    const record = {
      estatus: false,
      IP: sessionStorage.IP,
      pais: sessionStorage.country,
      Fecha: getDateWithFormat() + " " + getCurrentTime(),
    };
    const dbRef = firebaseConf.database().ref("instalacionPWA");
    const newInstall = dbRef.push();
    newInstall.set(record);
  }

  function EventMostro(data) {
    if (data.isInstallAllowed && sessionStorage.pwaShow == undefined) {
      Event("Mostro Mensaje App", "Mostro Mensaje App", "INICIO");
      sessionStorage.pwaShow = true;
    }
  }

  useEffect(() => {
    if (PWAStatus.isInstallSuccess) {
      Event("App instalada", "App instalada", "INICIO");
      const record = {
        estatus: true,
        IP: sessionStorage.IP,
        pais: sessionStorage.country,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("instalacionPWA");
      const newInstall = dbRef.push();
      newInstall.set(record);
    }
  }, [PWAStatus]);

  const renderInstallButton = () => {
    if (isInstalled)
      return (
        <PWAInstallerPrompt
          render={({ onClick }) => (
            <div className="install_page benefits-modal-wrapper">
              <div className="pwa-modal">
                <div className="pwa-modal-content">
                  <div className="i-header_div">
                    <b className="i_header">
                      Instalar Calculadora de Fletes y Aduana
                    </b>
                  </div>
                  <ul className="i-benefits-div">
                    <li className="i_benefit">
                      <i className="simple-icon-check mr-1" /> Cotiza Tu Aduana
                      y Flete en 2 minutos.
                    </li>
                    <li
                      style={{ fontWeight: "bold", color: "#8fceff" }}
                      className="i_benefit"
                    >
                      <i className="simple-icon-check mr-1" /> No requiere
                      registro.
                    </li>
                  </ul>
                  <div className="i-btn-div">
                    <button
                      onClick={() => onChange()}
                      type="button"
                      className="mr-3"
                      style={{
                        color: "black",
                        background:
                          "linear-gradient(90deg, #d7d7d7 0%, #d7d7d7 35%, #6c757d 100%)",
                      }}
                    >
                      <span>
                        <span>No, Gracias</span>
                      </span>
                    </button>
                    <button
                      onClick={onClick}
                      type="button"
                      style={{
                        color: "#FFFFFF",
                        background:
                          "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)",
                      }}
                    >
                      <span>
                        <span>Aceptar</span>
                      </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
          callback={(data) => {
            setPWAStatus(data);
            props.pwaInstalingToggle(data);
            EventMostro(data);
          }}
        />
      );
    return null;
  };

  return renderInstallButton();
};
export default Install;

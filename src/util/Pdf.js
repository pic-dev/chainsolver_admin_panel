import React, { Component, Fragment } from "react";
import {
  Page,
  Text,
  Image,
  View,
  Document,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";
import { getDateWithFormat, taxes, footer } from "./Utils";

Font.register({
  family: "Arial Black",
  src: "../assets/fonts/arialBlack/arialBlack.ttf",
});
const styles = StyleSheet.create({
  body: {
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 30,
    paddingBottom: 0,
  },
  sectionDate: {
    lineHeight: 1.3,
    fontSize: 9,
    marginTop: 5,
    textAlign: "right",
  },
  sectionClient: {
    lineHeight: 1.3,
    fontSize: 9,
    marginTop: 5,
    textAlign: "left",
  },
  importante: {
    marginLeft: 10,
    marginTop: "3",
    width: "90%",
    flexDirection: "row",
  },
  section2: {
    flexDirection: "row",
    color: "white",
    backgroundColor: "#a43542",
    width: 400,
    fontSize: 12,
    textAlign: "center",
    borderRadius: "5",
  },

  section3: {
    textAlign: "center",
    marginLeft: 10,
    width: 110,
    flexDirection: "row",
    color: "black",
    backgroundColor: "#e4e4e4",
    fontSize: 12,
    borderRadius: "5",
  },
  bulletPoint: {
    flexDirection: "row",
    width: 20,
    fontSize: 12,
    color: "#3b83bd",
  },

  bulletPoint2: {
    flexDirection: "row",
    fontSize: 7,
  },
  serviciosResumen: {
    lineHeight: 1,
    textAlign: "justify",
    flexDirection: "row",
    paddingBottom: 3,
    fontSize: 7.5,
  },
  serviciosLogisticos: {
    lineHeight: 1.2,
    width: "90%",
    left: "10",
    flexDirection: "row",

    fontSize: 8,
  },
  title: {
    fontSize: 12,
    textAlign: "center",
  },
  validtitle: {
    fontSize: 12,
    textAlign: "center",
    color: "grey",
  },
  title2: {
    fontSize: 12,
    textAlign: "center",
    color: "white",
    backgroundColor: "#a43542",
    borderRadius: "5",
  },
  image: {
    width: 80,
    height: 40,
    marginVertical: 0,
    marginHorizontal: 0,
  },
  image3: {
    width: 35,
    height: 20,
    marginHorizontal: 0,
    width: "10%",
    flexDirection: "col",
  },
  image2: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 100,
    marginVertical: 3,
  },
});

class Pdf extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const formatter = new Intl.NumberFormat("de-DE");
    var impuestos;
    var subtotal;
    var total;
    var valido;
    var totalIGVCostos;
    var usd;
    var ruc;
    var contacto;
    var data = JSON.parse(sessionStorage.getItem("user"));
    var cliente;

    if (localStorage.empresa !== undefined && localStorage.empresa.length > 0) {
      cliente = localStorage.empresa.toUpperCase();
    } else {
      cliente = data[1].toString();
    }
    if (
      localStorage.telefono !== undefined &&
      localStorage.telefono.length > 0
    ) {
      contacto = data[2].toString() + " / " + localStorage.telefono;
    } else {
      contacto = data[2].toString();
    }

    impuestos = this.props.taxesResumen;
    subtotal = this.props.subtotalResumen;
    total = this.props.TotalResumen;
    totalIGVCostos = this.props.totalIGVCostos;
    valido = JSON.parse(localStorage.fechas_vigencia);
    usd = impuestos[0] == "No cotizado" ? "" : "USD";

    if (localStorage.ruc !== undefined) {
      ruc = localStorage.ruc;
    } else {
      ruc = "No requerido";
    }

    return (
      <Document>
        {total.length > 0 ? (
          <Page size="A4">
            <View style={styles.body}>
              <Image
                style={styles.image}
                src="/assets/img/login/icons/LOGO.png"
              />
              <View style={styles.sectionDate}>
                <Text>
                  <Text>Lima,</Text>
                  <Text>{getDateWithFormat()}</Text>
                </Text>
                <Text>
                  <Text>ID: </Text>
                  <Text> {JSON.parse(localStorage.token)}</Text>
                </Text>
              </View>
              <View style={styles.sectionClient}>
                <Text>
                  <Text>Cliente: </Text>
                  <Text> {cliente}</Text>
                </Text>
                <Text>
                  <Text>Contácto: </Text>
                  <Text> {contacto}</Text>
                </Text>
                <Text>
                  <Text>RUC: </Text>
                  <Text> {ruc}</Text>
                </Text>
              </View>
              <View style={{ marginTop: "8" }}>
                <Text style={styles.title}>PRESUPUESTO FORMAL</Text>
              </View>
              <View style={{ marginTop: "10" }}>
                <Text style={styles.title2}>DETALLES DEL SERVICIO</Text>
              </View>
              <View style={{ marginTop: "10" }}>
                <Text style={styles.validtitle}>
                  Tarifa válida Desde el {valido[0]} - Hasta el {valido[1]}
                </Text>
              </View>
              <View
                style={{
                  marginTop: "10",
                  borderTop: "1 solid grey",
                  borderBottom: "1 solid grey",
                  padding: "3",
                }}
              >
                <Text style={styles.validtitle}>
                  TRANSPORTE {localStorage.transporte} - {localStorage.puerto}
                  <Text style={styles.bulletPoint}>{"=>"}</Text>
                  {localStorage.puertoD}
                </Text>
              </View>

              <View style={[styles.serviciosResumen, { paddingTop: 10 }]}>
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          left: "35",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        <Text style={styles.bulletPoint}>• </Text> Tipo
                      </Text>
                      <Text
                        style={{
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.tipocon}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ width: "50%", flexDirection: "col" }}>
                        <Text style={styles.bulletPoint}>• </Text> Transporte a
                        domicilio
                      </Text>
                      <Text
                        style={{
                          right: "35",
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value7 == "true"
                          ? localStorage.ProvinciaN +
                            " - " +
                            localStorage.distritoN
                          : "NO"}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.serviciosResumen}>
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          left: "35",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        <Text style={styles.bulletPoint}>• </Text> Cantidad
                      </Text>
                      <Text
                        style={{
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.contenedoresDes}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ width: "50%", flexDirection: "col" }}>
                        <Text style={styles.bulletPoint}>• </Text> Seguro de
                        Mercancía
                      </Text>
                      <Text
                        style={{
                          right: "35",
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value8 == "true" ? "SI" : "NO"}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.serviciosResumen}>
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          left: "35",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        <Text style={styles.bulletPoint}>• </Text> Gastos
                        portuarios
                      </Text>
                      <Text
                        style={{
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value5 == "true" ? "SI" : "NO"}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ width: "50%", flexDirection: "col" }}>
                        <Text style={styles.bulletPoint}>• </Text> Impuestos de
                        Aduana
                      </Text>
                      <Text
                        style={{
                          right: "35",
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value6 == "true" ? "SI" : "NO"}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.serviciosResumen}>
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          left: "35",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        <Text style={styles.bulletPoint}>• </Text> Valor de
                        mercancía
                      </Text>
                      <Text
                        style={{
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value6 == "true"
                          ? formatter.format(localStorage.monto) + " " + "USD"
                          : "NO"}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      paddingRight: 10,
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ width: "50%", flexDirection: "col" }}>
                        <Text style={styles.bulletPoint}>• </Text> Permisos de
                        Importación
                      </Text>
                      <Text
                        style={{
                          color: "#0d5084",
                          right: "35",
                          textAlign: "right",
                          width: "50%",
                          flexDirection: "col",
                        }}
                      >
                        {localStorage.value6 == "true"
                          ? localStorage.regulador
                          : "No requiere"}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>

              <View style={{ textAlign: "center", flexDirection: "row" }}>
                <View>
                  <Text style={styles.section2}>SERVICIO LOGISTICO</Text>
                </View>
                <View>
                  <Text style={styles.section3}>MONTO</Text>
                </View>
              </View>

              <View
                style={[
                  styles.serviciosResumen,
                  {
                    marginTop: 5,
                    marginBottom: 2,
                    width: "100%",
                    height: "150",
                    flexDirection: "row",
                  },
                ]}
              >
                {localStorage.tipocon == "Contenedor completo" ? (
                  <View style={{ width: "85%", flexDirection: "col" }}>
                    <Text
                      style={
                        ([styles.serviciosLogisticos],
                        { marginBottom: 3, fontSize: 10 })
                      }
                    >
                      <Text
                        style={{
                          color: "#0d5084",
                          fontWeight: "bold",
                          flexDirection: "col",
                        }}
                      >
                        Incluye
                      </Text>
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      FLETE MARITIMO
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      THCD (Manejo de naviera en Destino)
                    </Text>
                    {JSON.parse(localStorage.check) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check4) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PERU" ? (
                      <Text style={styles.serviciosLogisticos}>
                        {JSON.parse(localStorage.check) == true ? (
                          <Text>
                            <Text style={styles.bulletPoint}>• </Text>
                            VISTOS BUENOS
                          </Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {localStorage.paisDestino == "PERU" ? (
                      <Text style={styles.serviciosLogisticos}>
                        {JSON.parse(localStorage.check) == true ? (
                          <Text>
                            <Text style={styles.bulletPoint}>• </Text>
                            GATE IN (Recepción de contenedor vacio en el puerto)
                          </Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check) == false ||
                    JSON.parse(localStorage.check2) == false ||
                    JSON.parse(localStorage.check1) == false ||
                    !JSON.parse(localStorage.check4) == false ? (
                      <Text
                        style={
                          ([styles.serviciosLogisticos],
                          { marginTop: 1, marginBottom: 3, fontSize: 10 })
                        }
                      >
                        <Text
                          style={{
                            color: "#0d5084",
                            fontWeight: "bold",
                            flexDirection: "col",
                          }}
                        >
                          No incluye
                        </Text>
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check4) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PERU" ? (
                      <Text style={styles.serviciosLogisticos}>
                        {JSON.parse(localStorage.check) == false ? (
                          <Text>
                            <Text style={styles.bulletPoint}>• </Text>
                            VISTOS BUENOS
                          </Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {localStorage.paisDestino == "PERU" ? (
                      <Text style={styles.serviciosLogisticos}>
                        {JSON.parse(localStorage.check) == false ? (
                          <Text>
                            <Text style={styles.bulletPoint}>• </Text>
                            GATE IN (Recepción de contenedor vacio en el puerto)
                          </Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                  </View>
                ) : (
                  <Text style={{ margin: 0 }}></Text>
                )}
                {localStorage.tipocon == "Contenedor compartido" ? (
                  <View style={{ width: "85%", flexDirection: "col" }}>
                    <Text
                      style={
                        ([styles.serviciosLogisticos],
                        { marginBottom: 3, fontSize: 10 })
                      }
                    >
                      <Text
                        style={{
                          color: "#0d5084",
                          fontWeight: "bold",
                          flexDirection: "col",
                        }}
                      >
                        Incluye
                      </Text>
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      FLETE MARITIMO
                    </Text>
                    {localStorage.paisDestino == "PERU" ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HANDLING Y MANEJO DESTINO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PANAMA" ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        DESCARGA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PANAMA" ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        DMC DE SALIDA GASTOS PORTUARIOS
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PANAMA" ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        DESCONSOLIDACIÓN
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {localStorage.paisDestino == "PANAMA" ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        MANEJOS
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check4) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check) == false ||
                    JSON.parse(localStorage.check2) == false ||
                    JSON.parse(localStorage.check1) == false ||
                    !JSON.parse(localStorage.check4) == false ? (
                      <Text
                        style={
                          ([styles.serviciosLogisticos],
                          { marginTop: 1, marginBottom: 3, fontSize: 10 })
                        }
                      >
                        <Text
                          style={{
                            color: "#0d5084",
                            fontWeight: "bold",
                            flexDirection: "col",
                          }}
                        >
                          No incluye
                        </Text>
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check4) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                  </View>
                ) : (
                  <Text style={{ margin: 0 }}></Text>
                )}

                {localStorage.transporte == "AÉREO" ? (
                  <View style={{ width: "85%", flexDirection: "col" }}>
                    <Text
                      style={
                        ([styles.serviciosLogisticos],
                        { marginBottom: 3, fontSize: 10 })
                      }
                    >
                      <Text
                        style={{
                          color: "#0d5084",
                          fontWeight: "bold",
                          flexDirection: "col",
                        }}
                      >
                        Incluye
                      </Text>
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      FLETE AEREO
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      GUIA AEREO
                    </Text>
                    <Text style={styles.serviciosLogisticos}>
                      <Text style={styles.bulletPoint}>• </Text>
                      GASTOS EN DESTINO DE LA AEROLINEA
                    </Text>
                    {JSON.parse(localStorage.check) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check4) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check) == false ||
                    JSON.parse(localStorage.check2) == false ||
                    JSON.parse(localStorage.check1) == false ||
                    !JSON.parse(localStorage.check4) == false ? (
                      <Text
                        style={
                          ([styles.serviciosLogisticos],
                          { marginTop: 1, marginBottom: 3, fontSize: 10 })
                        }
                      >
                        <Text
                          style={{
                            color: "#0d5084",
                            fontWeight: "bold",
                            flexDirection: "col",
                          }}
                        >
                          No incluye
                        </Text>
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        ALMACEN ADUANERO
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}

                    {JSON.parse(localStorage.check2) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        HONORARIOS DE AGENCIA DE ADUANA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check1) == false ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        TRANSPORTE A FABRICA IMPORTADOR
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                    {JSON.parse(localStorage.check4) == true ? (
                      <Text style={styles.serviciosLogisticos}>
                        <Text style={styles.bulletPoint}>• </Text>
                        SEGURO DE MERCANCIA
                      </Text>
                    ) : (
                      <Text style={{ margin: 0 }}></Text>
                    )}
                  </View>
                ) : (
                  <Text style={{ margin: 0 }}></Text>
                )}

                <View
                  style={{
                    textAlign: "center",
                    width: "20%",
                    flexDirection: "col",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Arial Black",
                      right: 15,
                      width: "100%",
                      top: 50,
                      fontSize: 15,
                    }}
                  >
                    {subtotal[0]} USD
                  </Text>
                  {localStorage.paisDestino == "PERU" && totalIGVCostos > 0 ? (
                    <Text
                      style={{
                        color: "grey",
                        right: 15,
                        width: "100%",
                        top: 55,
                        fontSize: 10,
                      }}
                    >
                      + IGV 18% {totalIGVCostos} USD
                    </Text>
                  ) : (
                    <Text style={{ margin: 0 }}></Text>
                  )}
                </View>
              </View>

              <View
                style={{
                  marginTop: 10,
                  padding: 10,
                  textAlign: "center",
                  color: "black",
                  backgroundColor: "#e4e4e4",
                }}
              >
                <View style={{ width: "100%", flexDirection: "row" }}>
                  <Image
                    style={styles.image3}
                    src={taxes(localStorage.paisDestino)}
                  />
                  <View
                    style={{
                      fontSize: 12,
                      color: "black",
                      width: "40%",
                      flexDirection: "col",
                      marginHorizontal: 0,
                    }}
                  >
                    <Text style={{ flexDirection: "col" }}>
                      IMPUESTOS DE ADUANA
                    </Text>
                  </View>
                  <View
                    style={{
                      textAlign: "center",
                      fontSize: 10,
                      color: "black",
                      width: "50%",
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        Impuestos Bajos
                      </Text>
                    </View>
                    <View
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        Impuestos Medio{" "}
                      </Text>
                    </View>
                    <View
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        Impuestos Altos
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    paddingTop: 10,
                    fontSize: 10,
                    width: "100%",
                    flexDirection: "row",
                  }}
                >
                  <View
                    style={{
                      fontSize: 10,
                      color: "black",
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <Text style={{ flexDirection: "col" }}>
                      Valor VARIABLE según descripción del producto
                    </Text>
                  </View>
                  <View
                    style={{
                      textAlign: "center",
                      color: "black",
                      width: "50%",
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        {impuestos[0]} {usd}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        {impuestos[2]} {usd}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text style={{ flexDirection: "col" }}>
                        {impuestos[1]} {usd}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  fontWeight: "bold",
                  padding: "5",
                  textAlign: "center",
                  fontSize: 10,
                  color: "white",
                  backgroundColor: "#a43542",
                  width: "100%",
                  flexDirection: "row",
                }}
              >
                <View
                  style={{
                    color: "white",
                    backgroundColor: "#a43542",
                    width: "50%",
                    flexDirection: "col",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Arial Black",
                      fontWeight: "bold",
                      flexDirection: "col",
                    }}
                  >
                    {impuestos[0] === "No cotizado" ? (
                      <Text>
                        TOTAL
                        {localStorage.paisDestino == "PERU" &&
                        totalIGVCostos > 0 ? (
                          <Text> (incluye IGV)</Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    ) : (
                      <Text>
                        TOTALES
                        {localStorage.paisDestino == "PERU" &&
                        totalIGVCostos > 0 ? (
                          <Text> (incluye IGV)</Text>
                        ) : (
                          <Text style={{ margin: 0 }}></Text>
                        )}
                      </Text>
                    )}
                  </Text>
                </View>
                {impuestos[0] === "No cotizado" ? (
                  <View
                    style={{
                      color: "white",
                      backgroundColor: "#a43542",
                      width: "50%",
                      flexDirection: "col",
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Arial Black",
                        fontWeight: "bold",
                        flexDirection: "col",
                      }}
                    >
                      {" "}
                      {total[0]} USD
                    </Text>
                  </View>
                ) : (
                  <View
                    style={{
                      fontFamily: "Arial Black",
                      textAlign: "center",
                      color: "white",
                      backgroundColor: "#a43542",
                      width: "50%",
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        fontWeight: "bold",
                        color: "white",
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text
                        style={{ fontWeight: "bold", flexDirection: "col" }}
                      >
                        {total[0]} USD
                      </Text>
                    </View>
                    <View
                      style={{
                        fontWeight: "bold",
                        color: "white",
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text
                        style={{ fontWeight: "bold", flexDirection: "col" }}
                      >
                        {total[2]} USD
                      </Text>
                    </View>
                    <View
                      style={{
                        fontWeight: "bold",
                        color: "white",
                        width: "33%",
                        flexDirection: "col",
                      }}
                    >
                      <Text
                        style={{ fontWeight: "bold", flexDirection: "col" }}
                      >
                        {total[1]} USD
                      </Text>
                    </View>
                  </View>
                )}
              </View>

              <View
                style={{
                  marginTop: "3",
                  borderBottom: "1 solid grey",
                  padding: "3",
                }}
              ></View>
              <View
                style={[
                  styles.serviciosResumen,
                  { width: "100%", flexDirection: "row", paddingTop: 7 },
                ]}
              >
                <View
                  style={{
                    padding: 10,
                    left: 40,
                    backgroundColor: "#f5f5f5",
                    width: "85%",
                    flexDirection: "col",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      left: 150,
                      width: "100%",
                      flexDirection: "col",
                    }}
                  >
                    <Image
                      style={{
                        width: 10,
                        height: 10,
                      }}
                      src="/assets/img/login/icons/infoPDF.png"
                    />
                    {"  "}IMPORTANTE
                  </Text>

                  <Text style={[styles.importante, { marginTop: "7" }]}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Tarifas no aplican para mercancía peligrosa.
                  </Text>
                  <Text style={styles.importante}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Salidas Semanales.
                  </Text>
                  <Text style={styles.importante}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Costos adicionales sugeridos por inspecciones aleatorias de
                    aduana, con controladas por PIC Cargo S.A.C.
                  </Text>
                  <Text style={styles.importante}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Tarifas no aplican para mercancía con sobrepeso.
                  </Text>
                  <Text style={styles.importante}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Los documentos del embarque son responsabilidad del
                    proveedor y consignatario.
                  </Text>
                  <Text style={styles.importante}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    El embalaje de la carga debe ser acto para transporte.
                  </Text>

                  <Text style={[styles.importante, { color: "red" }]}>
                    <Text style={styles.bulletPoint2}>• </Text>
                    Tarifa basada en peso y volumen suministrado.
                  </Text>

                  {localStorage.paisDestino == "PERU" ? (
                    <Text style={[styles.importante, { color: "red" }]}>
                      <Text style={styles.bulletPoint2}>• </Text>
                      Si el canal asignado por aduana es ROJO el costo aumentará
                      USD 150 - 200.
                    </Text>
                  ) : (
                    <Text style={{ margin: 0 }}></Text>
                  )}

                  {localStorage.paisDestino == "PERU" ? (
                    <Text style={[styles.importante, { color: "red" }]}>
                      <Text style={styles.bulletPoint2}>• </Text>
                      Todos los servicios que se realicen dentro del territorio
                      peruano generarán IGV de 18%.
                    </Text>
                  ) : (
                    <Text style={{ margin: 0 }}></Text>
                  )}

                  {localStorage.paisDestino == "PERU" ? (
                    <Text style={[styles.importante, { color: "red" }]}>
                      <Text style={styles.bulletPoint2}>• </Text>
                      Para su información el FLETE no genera IGV.
                    </Text>
                  ) : (
                    <Text style={{ margin: 0 }}></Text>
                  )}
                </View>
              </View>
              <View
                style={{
                  marginTop: "3",
                  borderBottom: "1 solid grey",
                  padding: "3",
                }}
              ></View>
            </View>
            <Image
              style={styles.image2}
              src={footer(localStorage.paisDestino)}
            />
          </Page>
        ) : (
          ""
        )}
      </Document>
    );
  }
}
export default Pdf;

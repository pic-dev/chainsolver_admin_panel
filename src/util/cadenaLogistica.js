import React, { Component} from "react";
import { Row } from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";

class Cadena extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className="mb-2 NoPadding mx-auto mt-2 display-sm"
          style={{
            textAlign: "center",
          }}>
          <Row>
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="my-auto mx-auto NoPadding"
              style={{
                textAlign: "center",
              }}>
              <span className="timeLineTitle">CADENA LOGISTICA</span>
              <div className="mt-2 mx-auto timeLine">
                <i className="icon-fabricaDo mr-2" />
                <i className="icon-transporte mr-2" />
                <i className="icon-aduana mr-2" />
                <i className="icon-puertoYalma mr-2" />

                <i
                  style={{
                    display:
                      localStorage.transporte === "MARITÍMO" ? "" : "none",
                  }}
                  className="icon-transMaritimo mr-2"
                />
                <i
                  style={{
                    display: localStorage.transporte === "AÉREO" ? "" : "none",
                  }}
                  className="icon-transAereo mr-2"
                />
                <i
                  className="icon-puertoYalma2 mr-2"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-puertoYalma3 mr-2"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-aduana2 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-aduana3 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === true
                        ? "none"
                        : "initial",
                  }}
                />

                <i
                  className="icon-transporte2 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-transporte3 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-almacen2 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-almacen3 mr-2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="simple-icon-lock mr-2"
                  style={{
                    fontSize: "22px",
                    fontWeight: "bold",
                    color:
                      JSON.parse(localStorage.check4) === true
                        ? "#b4b4b4"
                        : "#275fc7",
                  }}
                />
              </div>
            </Colxx>
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="my-auto mx-auto NoPadding"
              style={{
                textAlign: "center",
              }}>
              <div className="my-auto mx-auto timeLine">
                <i className="icon-Oline ml-2" />
                <i className="icon-Oline" />
                <i className="icon-Oline" />
                <i className="icon-Oline" />
                <i className="icon-Oline2" />
                <i
                  className="icon-Oline2"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline3"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline3"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline3"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline2"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline3"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline3"
                  style={{
                    display:
                      JSON.parse(localStorage.check4) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Oline2"
                  style={{
                    display:
                      JSON.parse(localStorage.check4) === true
                        ? "none"
                        : "initial",
                  }}
                />
              </div>
            </Colxx>

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="my-auto mx-auto pt-3 NoPadding"
              style={{
                textAlign: "center",
              }}>
              <span className="timeLineTitle mr-3">
                <i className="icon-Oline mr-1" />
                Pagado por tú proveedor
              </span>
              <span className="timeLineTitle mr-3">
                <i className="icon-Oline2 mr-1" />
                Cotizado
              </span>
              <span className="timeLineTitle">
                <i className="icon-Oline3 mr-1" />
                No cotizado
              </span>
            </Colxx>
          </Row>
        </Colxx>
        <Colxx
          xxs="12"
          md="12"
          sm="12"
          className="pt-1 pb-1 display-large"
          style={{
            textAlign: "center",
            borderBottom: "1px solid rgba(0, 0, 0, 0.18)",
          }}>
          <Row>
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="mx-auto"
              style={{
                textAlign: "center",
              }}>
              <span className="timeLineTitle">CADENA LOGISTICA</span>
              <div className="mt-1  mx-auto">
                <i
                  className="icon-fabricaDo mx-timeline"
                  style={{ marginLeft: "1em" }}
                />
                <i className="icon-transporte mx-timeline" />
                <i className="icon-aduana mx-timeline" />
                <i className="icon-puertoYalma mx-timeline" />

                <i
                  style={{
                    display:
                      localStorage.transporte === "MARITÍMO" ? "" : "none",
                  }}
                  className="icon-transMaritimo mx-timeline"
                />
                <i
                  style={{
                    display: localStorage.transporte === "AÉREO" ? "" : "none",
                  }}
                  className="icon-transAereo mx-timeline"
                />
                <i
                  className="icon-puertoYalma2 mx-timeline"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-puertoYalma3 mx-timeline"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-aduana2 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-aduana3 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check2) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-transporte2 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-transporte3 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-almacen2 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-almacen3 mx-timeline"
                  style={{
                    display:
                      !JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="simple-icon-lock"
                  style={{
                    fontSize: "30px",
                    fontWeight: "bold",
                    color:
                      JSON.parse(localStorage.check4) === true
                        ? "#b4b4b4"
                        : "#275fc7",
                  }}
                />
              </div>
            </Colxx>
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="my-auto mx-auto"
              style={{
                textAlign: "center",
              }}>
              <div>
                <i className="icon-Oline " style={{ marginLeft: "0%" }} />
                <i className="icon-Oline" />
                <i className="icon-Oline" />
                <i className="icon-Oline" />
                <i className="icon-Gline" />
                <i
                  className="icon-Gline"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Greyline"
                  style={{
                    display:
                      JSON.parse(localStorage.check) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Gline"
                  style={{
                    display:
                      JSON.parse(localStorage.check2) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Greyline"
                  style={{
                    display:
                      JSON.parse(localStorage.check2) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Gline"
                  style={{
                    display:
                      JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Greyline"
                  style={{
                    display:
                      JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Gline"
                  style={{
                    display:
                      JSON.parse(localStorage.check1) === false
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Greyline"
                  style={{
                    display:
                      JSON.parse(localStorage.check1) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Gline"
                  style={{
                    display:
                      JSON.parse(localStorage.check4) === true
                        ? "none"
                        : "initial",
                  }}
                />
                <i
                  className="icon-Greyline"
                  style={{
                    display:
                      JSON.parse(localStorage.check4) === false
                        ? "none"
                        : "initial",
                  }}
                />
              </div>
            </Colxx>
            <div className="my-auto mx-auto">
              <div className="timeLineText ml-3 mr-1">FABRICA PROVEEDOR</div>
              <div className="timeLineText mr-1">TRANSPORTE EN ORIGEN</div>
              <div className="timeLineText mr-1">ADUANA EN ORIGEN</div>
              <div className="timeLineText mr-1">PUERTO EN ORIGEN</div>
              <div
                className="timeLineText mr-1"
                style={{display:
                  localStorage.transporte === "MARITÍMO" ? "" : "none",
                  color: "#275fc7",
                }}>
                FLETE MARITÍMO
              </div>
              <div
                className="timeLineText mr-1"
                style={{
                  display: localStorage.transporte === "AÉREO" ? "" : "none",
                  color: "#275fc7",
                }}>
                FLETE AÉREO
              </div>

             

              <div
                className="timeLineText mr-1"
                style={{
                  color:
                    JSON.parse(localStorage.check) === true
                      ? "#275fc7"
                      : "#b4b4b4 ",
                }}>
                PUERTO Y ALMACEN DESTINO
              </div>
              <div
                className="timeLineText mr-1"
                style={{
                  color:
                    JSON.parse(localStorage.check2) === true
                      ? "#275fc7"
                      : "#b4b4b4 ",
                }}>
                ADUANA DESTIDO
              </div>
              <div
                className="timeLineText mr-1"
                style={{
                  color:
                    JSON.parse(localStorage.check1) === true
                      ? "#275fc7"
                      : "#b4b4b4 ",
                }}>
                TRANSPORTE DESTINO
              </div>
              <div
                className="timeLineText mr-1"
                style={{
                  color:
                    JSON.parse(localStorage.check1) === true
                      ? "#275fc7"
                      : "#b4b4b4 ",
                }}>
                ALMACEN IMPORTADOR
              </div>
              <div
                className="timeLineText"
                style={{
                  color:
                    JSON.parse(localStorage.check4) === false
                      ? "#275fc7"
                      : "#b4b4b4 ",
                }}>
                SEGURO DE MERCANCIA
              </div>
            </div>

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="mt-2 mx-auto"
              style={{
                textAlign: "center",
              }}>
              <span className="timeLineTitle mr-5">
                {" "}
                <i className="icon-timeLineText2 " />
                Pagado por tú proveedor
              </span>
              <span className="timeLineTitle mr-5">
                {" "}
                <i className="icon-timeLineText2-G" />
                Cotizado
              </span>
              <span className="timeLineTitle">
                {" "}
                <i className="icon-timeLineText2-Grey" />
                No cotizado
              </span>
            </Colxx>
          </Row>
        </Colxx>{" "}
      </div>
    );
  }
}

export default Cadena;

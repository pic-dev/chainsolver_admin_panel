import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "reactstrap";

export default function botonSenioLogin() {
  return (
    <NavLink to="/seniorLogin">
      <Button className="SeniorLoginBtn btn-style-login p-1 btn btn-success btn-sm float-right">
        <i className="mr-2 simple-icon-user" />
        ¿Senior o Junior?,Iniciar Sesión
      </Button>
    </NavLink>
  );
}

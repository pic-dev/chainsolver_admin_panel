
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from "react-helmet";




export function Title(props) {
  return (
  
    <Helmet>
      {props.children}
      </Helmet>
  
  );
}

Title.propTypes = {
  children: PropTypes.node,
};
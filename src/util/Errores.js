import React, { Component } from 'react'



class Errores extends Component {

    render() {
  
      return (
        <div  className="ant-alert ant-alert-error ant-alert-with-description ant-alert-no-icon">
        <span className="ant-alert-message">Advertencia</span>
        <span className="ant-alert-description">{this.props.mensaje}</span>
        </div>
  
  );
    }
  }
  
  export default Errores;
  
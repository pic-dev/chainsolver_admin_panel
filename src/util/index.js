function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var PropTypes = _interopDefault(require('prop-types'));

var PWAInstallerPrompt = function PWAInstallerPrompt(_ref) {
  var InstallButton = _ref.render,
      callback = _ref.callback;

  var createStatus = function createStatus(object) {
    return {
      isInstallAllowed: Object.prototype.hasOwnProperty.call(object, 'isInstallAllowed') ? object.isInstallAllowed : false,
      isInstallWatingConfirm: Object.prototype.hasOwnProperty.call(object, 'isInstallWatingConfirm') ? object.isInstallWatingConfirm : false,
      isInstalling: Object.prototype.hasOwnProperty.call(object, 'isInstalling') ? object.isInstalling : false,
      isInstallCancelled: Object.prototype.hasOwnProperty.call(object, 'isInstallCancelled') ? object.isInstallCancelled : false,
      isInstallSuccess: Object.prototype.hasOwnProperty.call(object, 'isInstallSuccess') ? object.isInstallSuccess : false,
      isInstallFailed: Object.prototype.hasOwnProperty.call(object, 'isInstallFailed') ? object.isInstallFailed : false
    };
  };

  var _useState = React.useState(createStatus({})),
      installStatus = _useState[0],
      setInstallStatus = _useState[1];

  var _useState2 = React.useState(null),
      installEvent = _useState2[0],
      setInstallEvent = _useState2[1];

  React.useEffect(function () {
    if (callback) {
      callback(installStatus);
    }
  }, [installStatus]);

  var beforeAppInstallpromptHandler = function beforeAppInstallpromptHandler(e) {
    e.preventDefault();

    if (!installStatus.isInstalling) {
      if (!installStatus.isInstallSuccess) {
        setInstallEvent(e);

        if (!installStatus.isInstallAllowed) {
          setInstallStatus(createStatus({
            isInstallAllowed: true,
            isInstallCancelled: installStatus.isInstallCancelled
          }));
        }
      }
    }
  };

  var appInstalledHandler = function appInstalledHandler(e) {
    if (!installStatus.isInstallSuccess) {
      window.removeEventListener('beforeinstallprompt', beforeAppInstallpromptHandler);
      e.preventDefault();
      setInstallStatus(createStatus({
        isInstallSuccess: true
      }));
    }
  };

  React.useEffect(function () {
    window.addEventListener('beforeinstallprompt', beforeAppInstallpromptHandler);
    window.addEventListener('appinstalled', appInstalledHandler);
    return function () {
      window.removeEventListener('beforeinstallprompt', beforeAppInstallpromptHandler);
      window.removeEventListener('appinstalled', appInstalledHandler);
    };
  }, []);

  var handleOnInstall = function handleOnInstall() {
    setInstallStatus(createStatus({
      isInstallWatingConfirm: true
    }));
    installEvent.prompt();
    installEvent.userChoice.then(function (choiceResult) {
      if (choiceResult.outcome === 'accepted') {
        setInstallStatus(createStatus({
          isInstalling: true,
          isInstallAllowed: false
        }));
      } else {
        setInstallStatus(createStatus({
          isInstallCancelled: true,
          isInstallAllowed: true
        }));
      }
    })["catch"](function () {
      setInstallStatus(createStatus({
        isInstallFailed: true,
        isInstallAllowed: true
      }));
    });
    setInstallEvent(null);
  };

  if (!installStatus.isInstallAllowed) {
    return false;
  }

  return /*#__PURE__*/React__default.createElement(InstallButton, {
    onClick: handleOnInstall
  });
};

PWAInstallerPrompt.propTypes = {
  callback: PropTypes.func
};
PWAInstallerPrompt.defaultProps = {
  callback: undefined
};

module.exports = PWAInstallerPrompt;
//# sourceMappingURL=index.js.map

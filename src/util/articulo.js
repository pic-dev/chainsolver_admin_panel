import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import AddOutlined from "@material-ui/icons/AddOutlined";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "auto !important",
  },
  media: {
    height: 300,
    [theme.breakpoints.down("sm")]: {
      height: "8rem",
    },
  },
}));

export default function ImgMediaCard({
  ArticuloSelect,
  status,
  JuniorData,
  articulo,
  modalDetalleArticuloOpen,
}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          alt={articulo.name}
          className={classes.media}
          image={articulo.image}
          title={articulo.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h6">
            {articulo.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {status
              ? JuniorData.length > 1
                ? articulo.precio_venta + " USD"
                : articulo.precio_junior + " USD"
              : ""}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {status && (
          <IconButton
            aria-label={`info about ${articulo.name}`}
            className={classes.icon}
            onClick={() => ArticuloSelect(articulo)}
          >
            <AddOutlined />
          </IconButton>
        )}
        <IconButton
          aria-label={`info about ${articulo.name}`}
          className={classes.icon}
          onClick={() => modalDetalleArticuloOpen(articulo)}
        >
          <InfoIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}

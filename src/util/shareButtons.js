import React, { Component, Fragment } from "react";

import { Col } from "reactstrap";

import {
  FacebookShareButton,
  WhatsappShareButton,
  TwitterShareButton,
  TelegramShareButton,
  LinkedinShareButton,
  FacebookIcon,
  TwitterIcon,
  TelegramIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";

class shareButtons extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const shareUrl = "https://www.pic-cargo.com/es/calculadoras/";
    const title = "PIC - Calculadora de fletes";
    const description =
      "Me gustaría compartir esta HERRAMIENTA PODEROSA, Es una CALCULADORA DE FLETE e IMPUESTOS DE ADUANA, úsala en el el siguiente link:  ";

    return (
      <div>
        <Col
          xs="12"
          md="12"
          sm="12"
          className="mt-2 mb-2 display-sm  mx-auto"
          style={{
            textAlign: "center",
          }}
        >
          <div>
            <div className="arial color-blue mb-1">Compartir por:</div>
            <div className="some-network">
              <FacebookShareButton
                url={shareUrl}
                quote={description}
                className="some-network__share-button"
              >
                <FacebookIcon size={32} />
              </FacebookShareButton>
            </div>

            <div className="some-network">
              <TwitterShareButton
                url={shareUrl}
                title={description}
                className="some-network__share-button"
              >
                <TwitterIcon size={32} />
              </TwitterShareButton>
            </div>

            <div className="some-network">
              <TelegramShareButton
                url={shareUrl}
                title={description}
                className="some-network__share-button"
              >
                <TelegramIcon size={32} />
              </TelegramShareButton>
            </div>

            <div className="some-network">
              <WhatsappShareButton
                url={shareUrl}
                title={description}
                separator=" "
                className="some-network__share-button"
              >
                <WhatsappIcon size={32} />
              </WhatsappShareButton>
            </div>

            <div className="some-network">
              <LinkedinShareButton
                url={shareUrl}
                title={title}
                description={description}
                className="some-network__share-button"
              >
                <LinkedinIcon size={32} />
              </LinkedinShareButton>
            </div>
          </div>
        </Col>
        <Col
          xs="12"
          md="12"
          sm="12"
          className="theme-colors  display-large  mx-auto"
          style={{
            textAlign: "center",
          }}
        >
          <div className="theme-button">
            {" "}
            <div className="some-network">
              <FacebookShareButton
                url={shareUrl}
                quote={description}
                className="some-network__share-button"
              >
                <FacebookIcon size={45} />
              </FacebookShareButton>
            </div>
            <div className="some-network">
              <TwitterShareButton
                url={shareUrl}
                title={description}
                className="some-network__share-button"
              >
                <TwitterIcon size={45} />
              </TwitterShareButton>
            </div>
            <div className="some-network">
              <TelegramShareButton
                url={shareUrl}
                title={description}
                className="some-network__share-button"
              >
                <TelegramIcon size={45} />
              </TelegramShareButton>
            </div>
            <div className="some-network">
              <WhatsappShareButton
                url={shareUrl}
                title={description}
                separator=" "
                className="some-network__share-button"
              >
                <WhatsappIcon size={45} />
              </WhatsappShareButton>
            </div>
            <div className="some-network">
              <LinkedinShareButton
                url={shareUrl}
                title={title}
                description={description}
                className="some-network__share-button"
              >
                <LinkedinIcon size={45} />
              </LinkedinShareButton>
            </div>
          </div>
        </Col>
      </div>
    );
  }
}

export default shareButtons;

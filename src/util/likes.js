import React, { Component } from "react";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { firebaseConf } from "./Firebase";

class Likes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: 0,
      active: false,
    };
    this.toggleRates = this.toggleRates.bind(this);
  }

  componentDidMount() {
    var starCountRef = firebaseConf
      .database()
      .ref("product/" + this.props.item + "/rates");

    starCountRef.once("value").then((snapshot) => {
      this.setState({
        likes: snapshot.val(),
      });
    });

    starCountRef.on("child_changed", (snapshot) => {
      this.setState({
        likes: snapshot.val(),
      });
    });
  }

  toggleRates(key) {
    if (!this.state.active) {
      this.setState({
        active: true,
      });
      var starCountRef = firebaseConf
        .database()
        .ref("product/" + key + "/rates");
      starCountRef.transaction(
        (data) => {
          // If users/ada/rank has never been set, currentRank will be `null`.

          if (data >= 0) {
            return data + 1;
          } else {
            return; // Abort the transaction.
          }
        },
        (error, committed, snapshot) => {
          if (error) {
            console.log("Transaction failed abnormally!", error);
          } else if (!committed) {
            console.log(
              "We aborted the transaction (because ada already exists)."
            );
          } else {
            this.setState({
              likes: snapshot.val(),
            });
          }
        }
      );
    }
  }
  render() {
    return (
      <div
        style={{
          cursor: "pointer",
          width: "100%",
          textAlign: "left",
          padding: "0.3rem 0rem 0rem 1rem",
        }}
      >
        {" "}
        <FontAwesomeIcon
          key={this.props.item + "Like"}
          icon={faHeart}
          style={{ width: "1rem", height: "1rem" }}
          className={`ml-3 mr-2  ${
            this.state.active ? "color-red" : "color-grey"
          }`}
          onClick={() => {
            this.toggleRates(this.props.item);
          }}
        />
        {this.state.likes}
      </div>
    );
  }
}

export default Likes;

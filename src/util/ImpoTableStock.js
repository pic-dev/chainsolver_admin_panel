import React, { Component } from "react";
import { Grid, GridColumn, GridToolbar } from "@progress/kendo-react-grid";
import { ExcelExport } from "@progress/kendo-react-excel-export";
import "@progress/kendo-theme-default/dist/all.css";
import { process } from "@progress/kendo-data-query";

const EditCommandCell = (props) => {
  return (
    <td>
      <button
        className="btn-style-Red m-1 btn btn-danger btn-sm"
        onClick={() => props.enterEdit(props.dataItem)}
      >
        Desactivar
      </button>
    </td>
  );
};

const EraseCommandCell = (props) => {
  return (
    <td>
      <button
        className="btn-style-Red m-1 btn btn-danger btn-sm"
        onClick={() => props.enterErase(props.dataItem)}
      >
        x
      </button>
    </td>
  );
};
const dataState = {
  sort: [{ field: "code", dir: "asc" }],
  take: 10,
  skip: 0,
};

class tabla extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formClientes: [],
      dataState: dataState,
    };
  }

  _export;
  export = () => {
    this._export.save();
  };
  componentDidUpdate(nextProps) {
    if (nextProps.clients !== this.props.clients) {

      this.setState({ formClientes: nextProps.clients });
      this.renderTable();
    }
  }
  componentDidMount() {
    this.setState({
      formClientes: this.props.clients,
    });
  }
  pageChange = (event) => {
    this.setState({
      skip: event.page.skip,
      take: event.page.take,
    });
  };
  Eliminar = (props) => (
    <EraseCommandCell {...props} enterErase={this.enterErase} />
  );

  MyEditCommandCell = (props) => (
    <EditCommandCell {...props} enterEdit={this.enterEdit} />
  );

  enterEdit = (item) => {
    this.props.handleUpdate(item.key);
    this.renderTable();
  };

  enterErase = (item) => {
    this.props.handleRemove(item.key);
    this.renderTable();
  };
  renderTable() {

    if (this.props.clients.length > 0) {
      return (
        <ExcelExport
          data={this.props.clients.slice(
            this.state.skip,
            this.state.take + this.state.skip
          )}
          ref={(exporter) => (this._export = exporter)}
        >
          <Grid
            pageable
            sortable
            filterable
            data={process(this.props.clients, this.state.dataState)}
            {...this.state.dataState}
            onDataStateChange={(e) => {
              this.setState({ dataState: e.dataState });
            }}
            style={{ height: "420px" }}
          >
            <GridToolbar>
              <button
                title="Export Excel"
                className="btn-style-Green m-1 btn btn-success btn-sm"
                onClick={this.export}
              >
                Exportar a Excel
              </button>
            </GridToolbar>
            <GridColumn field="NombreProducto" title="Producto" width="200px" />
            <GridColumn field="date" title="Fecha de llegada" width="200px" />
            <GridColumn field="TipoMerca" title="Tipo de Merca" width="200px" />
            <GridColumn field="activo" title="Status" width="200px" />
            <GridColumn
              cell={this.MyEditCommandCell}
              filterable={false}
              width="200px"
            />
            <GridColumn cell={this.Eliminar} filterable={false} width="200px" />
          </Grid>
        </ExcelExport>
      );
    }
  }

  render() {
    return <div>{this.renderTable()}</div>;
  }
}

export default tabla;

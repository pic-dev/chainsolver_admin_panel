import React from 'react';
import { withRouter } from 'react-router-dom';

const GoBack = ({ history }) => <i className="BackIcon simple-icon-arrow-left-circle float-left" onClick={() => history.goBack()} /> ;

export default withRouter(GoBack);
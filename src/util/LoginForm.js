import React, { Component } from "react";
import { firebaseConf, googleAuthProvider, persistencia } from "./Firebase";
import Errores from "./Errores";
import { Event } from "./tracking";
import { Button, Form, FormGroup, Input } from "reactstrap";
import axios from "axios";
import { withRouter } from "react-router-dom";
let timer;
class LoginFrom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Signup: false,
      signIn: false,
      error: "",
    };
  }
  seterror = (e) => {
    this.setState(() => ({
      error: e,
    }));
  };
  setsignup = (e) => {
    this.setState(() => ({
      signup: e,
    }));
  };
  setsignIn = (e) => {
    this.setState(() => ({
      signIn: e,
    }));
  };

  handleSignUp = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { usuario, clave, Confirmacion } = e.target.elements;

    if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value === Confirmacion.value
    ) {
      await firebaseConf
        .auth()
        .createUserWithEmailAndPassword(usuario.value, clave.value)
        .then(() => {
          //Event("EmailRegistro", "Usuario se registro con correo", "SERVICIOS");

          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));
        })
        .catch((error) => {
          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));

          window.location.reload();
        });
    } else if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value !== Confirmacion.value
    ) {
      this.seterror("La contraseña ingresada no coincide con la confimación");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    } else {
      this.seterror("Debe llenar todos los campos requeridos");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    }
  };

  correoClave = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { usuario, clave } = e.target.elements;

    axios
      .post("https://point.qreport.site/login/general", {
        usuario: usuario.value,
        contraseña: clave.value,
      })
      .then((res) => {
        if (res.data.length > 0) {
          sessionStorage.seniorLogin = JSON.stringify(res.data);
          this.props.setUserSeniorData(res.data);
          this.props.toggle();
          this.props.handleLogin();
          this.props.history.push("/PedidosJunior");
          
              } else {
          firebaseConf
            .auth()
            .signInWithEmailAndPassword(usuario.value, clave.value)
            .then((result) => {
              //Event("EmailLoginHeader", "Usuario ingresó con correo", "home");

              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
            })
            .catch((error) => {
              //errores("Headercorreo", error);

              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));

              window.location.reload();
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  socialLogin = (provider, e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    firebaseConf
      .auth()
      .setPersistence(persistencia)
      .then(() => {
        (async () => {
          await firebaseConf
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
              /*Event(
                "GoogleLoginHeader",
                "Usuario ingresó con botton de google",
                "SERVICIOS"
              );*/
              let user = [
                [result.additionalUserInfo.profile.id],
                [result.additionalUserInfo.profile.name],
                [result.additionalUserInfo.profile.email],
                [result.additionalUserInfo.profile.picture],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));

              sessionStorage.usuario = "2";
            })
            .catch((error) => {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                ["Invitado"],
                ["Invitado"],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              window.location.reload();
            });
        })();
      })
      .catch((error) => {});
  };
  render() {
    return (
      <div>
        {!this.state.signup ? (
          <Form className=" login-form">
            {this.state.error ? (
              <FormGroup>
                <Errores mensaje={this.state.error} />
              </FormGroup>
            ) : null}
            <div
              className="paddingLogin text-center"
              style={{ textAling: "center" }}
            >
              <FormGroup className="mb-2 arial">
                <span>Iniciar sesión</span>
              </FormGroup>

              <FormGroup className="mb-2">
                <Button
                  style={{
                    borderRadius: "13px",
                    fontSize: "14px",
                  }}
                  className="GLogin"
                  onClick={(e) => this.socialLogin(googleAuthProvider, e)}
                >
                  <i className="icon-gmail" />
                  <span>Gmail,&nbsp;</span>{" "}
                  <span
                    className="color-orange"
                    style={{
                      fontWeight: "bold",
                      cursor: "pointer",
                      textDecoration: "underline",
                    }}
                  >
                    solo con un click
                  </span>
                </Button>
              </FormGroup>
              <FormGroup className="mb-2 arial">
                <span>-o-</span>
              </FormGroup>
              <Form className=" login-form" onSubmit={this.correoClave}>
                <FormGroup>
                  <Input type="text" name="usuario" placeholder="Usuario" />
                </FormGroup>
                <FormGroup>
                  <Input name="clave" type="password" placeholder="Clave" />
                </FormGroup>
                <FormGroup className="login-form-button">
                  <Button
                    type="submit"
                    className="btn-style-blue btn btn-success btn-sm"
                  >
                    Iniciar sesión
                  </Button>
                  &nbsp; O &nbsp;
                  <span
                    className="arial"
                    onClick={() => {
                      this.setsignup(true);
                    }}
                    style={{
                      fontWeight: "bold",
                      cursor: "pointer",
                      color: "rgb(54, 94, 153)",
                      textDecoration: "underline",
                    }}
                  >
                    Registrate
                  </span>
                </FormGroup>
              </Form>
            </div>
          </Form>
        ) : (
          <Form className="login-form text-center">
            <FormGroup className="mb-2 arial">
              <span>Registro</span>
            </FormGroup>
            {this.state.error ? (
              <FormGroup>
                <Errores mensaje={this.state.error} />
              </FormGroup>
            ) : null}
            <FormGroup className="mb-2">
              <Button
                style={{
                  borderRadius: "13px",
                  fontSize: "14px",
                }}
                className="GLogin"
                onClick={(e) => this.socialLogin(googleAuthProvider, e)}
              >
                <i className="icon-gmail" />
                <span>Gmail,&nbsp;</span>{" "}
                <span
                  className="color-orange"
                  style={{
                    fontWeight: "bold",
                    cursor: "pointer",
                    textDecoration: "underline",
                  }}
                >
                  solo con un click
                </span>
              </Button>
            </FormGroup>
            <FormGroup className="mb-2 arial">
              <span>-o-</span>
            </FormGroup>
            <Form
              className="login-form text-center"
              onSubmit={this.handleSignUp}
            >
              <FormGroup>
                <Input
                  type="email"
                  name="usuario"
                  placeholder="Correo electrónico"
                />
              </FormGroup>
              <FormGroup>
                <Input name="clave" type="password" placeholder="Clave" />
              </FormGroup>

              <FormGroup>
                <Input
                  name="Confirmacion"
                  type="password"
                  placeholder="Confirma tu Clave"
                />
              </FormGroup>
              <FormGroup>
                <Button
                  type="submit"
                  className="btn-style-blue btn btn-success btn-sm"
                >
                  Registrate
                </Button>
                &nbsp; O &nbsp;
                <span
                  onClick={() => {
                    this.setsignup(false);
                    this.setsignIn(true);
                  }}
                  style={{
                    fontWeight: "bold",
                    cursor: "pointer",
                    color: "rgb(54, 94, 153)",
                    textDecoration: "underline",
                  }}
                >
                  INICIAR SESIÓN
                </span>
              </FormGroup>
            </Form>
          </Form>
        )}{" "}
      </div>
    );
  }
  // setInterval
  // clearInterval

  componentWillUnmount() {
    clearInterval(timer);
  }
}

export default withRouter(LoginFrom);

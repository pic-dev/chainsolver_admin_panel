import React, { useEffect, useRef, useCallback, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AddOutlined from "@material-ui/icons/AddOutlined";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";
import { useArticles } from "Hooks/useAbonos";
import useNearScreen from "Hooks/useNearScreen";
import debounce from "just-debounce-it";
import LoadingSpiner from "./loading";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import InfoIcon from "@material-ui/icons/Info";
import "./masonry.css";

const ModalDetalleArticulo = React.lazy(() =>
  import("Util/Modals/modalDetalleArticulo")
);

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  GridListTile: {
    textAlign: "center",
    "& img": {
      width: "auto",
      height: "inherit",
      [theme.breakpoints.down("sm")]: {
        height: "60% !important",
      },
    },
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  gridList: {
    width: "inherit",
    minHeight: "100vh",
    maxHeight: "100vh",
    overflowX: "hidden",
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
    padding: "5px",
  },
  iconContainer: {
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  avatar: {
    backgroundColor: red[500],
  },
  loadingContainer: {
    textAlign: "center",
    fontWeight: "bold",
    height: "3rem !important",
  },
  visor: {
    textAlign: "center",

    height: "3rem !important",
  },
}));

export default function Album({
  ArticuloSelect,
  catalago,
  status,
  JuniorData,
  toggleModal,
}) {
  const classes = useStyles();

  const [articuloSelected, setArticuloSelected] = useState([]);
  const [modalDetalleOpen, setModalDetalleOpen] = useState(false);

  const { loading, loadingNextPage, articulo, setPage, limite } = useArticles({
    catalogo: catalago.id,
  });

  const externalRef = useRef();
  const { isNearScreen } = useNearScreen({
    externalRef: loading ? null : externalRef,
    once: false,
  });

  const debounceNextPage = useCallback(
    debounce(() => setPage((prevPage) => prevPage + 1), 2000),
    []
  );
  useEffect(function () {
    if (isNearScreen) debounceNextPage();
  });

  const modalDetalleArticuloOpen = (e) => {
    setArticuloSelected(e);
    setModalDetalleOpen(true);
  };

  return (
    <div className={classes.root}>
      {loading ? (
        <LoadingSpiner />
      ) : (
        <>
          <div className="ListOfGifs">
            {articulo.map((card) => (
              <div className="Gif" key={card.id}>
                <img
                  loading="lazy"
                  onClick={() => modalDetalleArticuloOpen(card)}
                  src={card.image}
                  alt={card.name}
                />
                <GridListTileBar
                  title={card.name}
                  subtitle={
                    status
                      ? JuniorData.length > 1
                        ? card.precio_venta
                        : card.precio_junior
                      : ""
                  }
                  actionIcon={
                    <div className={classes.iconContainer}>
                      {status && (
                        <IconButton
                          aria-label={`info about ${card.name}`}
                          className={classes.icon}
                          onClick={() => ArticuloSelect(card)}
                        >
                          <AddOutlined />
                        </IconButton>
                      )}
                      <IconButton
                        aria-label={`info about ${card.name}`}
                        className={classes.icon}
                        onClick={() => modalDetalleArticuloOpen(card)}
                      >
                        <InfoIcon />
                      </IconButton>
                    </div>
                  }
                />
              </div>
            ))}

            <div
              className={classes.visor}
              data-testid="visor"
              ref={externalRef}
            />
          </div>
          {isNearScreen && limite / articulo.length !== 1 && (
            <div className={classes.loadingContainer}>
              Cargando... <span className="loadingBlock"></span>
            </div>
          )}

          {limite / articulo.length == 1 && (
            <div className={classes.loadingContainer}>
              <span className="arial">No hay más productos disponibles</span>
            </div>
          )}
        </>
      )}
      {modalDetalleOpen && (
        <ModalDetalleArticulo
          status={status}
          open={modalDetalleOpen}
          toggle={setModalDetalleOpen}
          addArticulo={ArticuloSelect}
          articulo={articuloSelected}
          toggleModal={toggleModal}
        />
      )}
    </div>
  );
}

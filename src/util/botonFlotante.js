import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';

const useStyles = makeStyles((theme) => ({
  root: {
    position: "fixed",
    bottom: "1rem",
    right:" 1rem",
    transform: 'translateZ(0px)',
    flexGrow: 1,
    zIndex: "9999999",
  },
  speedDial: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
 
  },
  MuiFabPrimary: {
    color: "#fff",
    backgroundColor: "#1976d2",
}
}));



const actions = [
  { icon:<img style={{width:"2.5rem"}} src="/assets/img/flags/panamaRound.svg"/>, name: 'Panama', url:"https://wa.me/584141417456" },
  { icon: <img style={{width:"2.5rem"}} src='/assets/img/flags/peruRound.svg'/>, name: 'Peru',url: "https://wa.me/51920301745"},
  { icon: <img style={{width:"2.5rem"}} src='/assets/img/flags/venezuelaRound.svg'/>, name: 'Venezuela',url: "https://wa.me/584141417456" }
];

export default function SpeedDialTooltipOpen() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [hidden, setHidden] = React.useState(false);

  const handleVisibility = () => {
    setHidden((prevHidden) => !prevHidden);
  };
//handler function
function handleClick (e,url){
  e.preventDefault();
  setOpen(false);
  window.open(url,"_blank");

};
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
       <Backdrop open={open} />
      <SpeedDial
        ariaLabel="SpeedDial tooltip example"
        className={classes.speedDial}
        hidden={hidden}
        icon={ <WhatsAppIcon style={{width:"2.5em", height:"2.5rem" }}  />}
        onClose={handleClose}
        onOpen={handleOpen}
        open={open}
      >
        {actions.map((action) => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            tooltipOpen
            className={classes.MuiFabPrimary}
            onClick={(e) => {handleClick(e,action.url)}}
          />
        ))}
      </SpeedDial>
    </div>
  );
}

import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import PWAInstallerPrompt from 'react-pwa-installer-prompt';
import 'react-toastify/dist/ReactToastify.css';
// minified version is also included
// import 'react-toastify/dist/ReactToastify.min.css';

const install = (props) => {     
  
  const notify = () => toast.info(    <button className="btn-style-bluelight" onClick={notify}>Instalar App</button>, {
    position: "bottom-center",
    autoClose: false,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    });

  return (
    <div>
          <PWAInstallerPrompt 
      callback={(data) => console.log(data)} 
    />
      <button className="btn-style-bluelight" onClick={notify}>Instalar App</button>
      <ToastContainer
      position="bottom-center"
      autoClose={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable />
    </div>
  );
}

export default install;
import React, { Component } from "react";
import Carousel, { consts } from "react-elastic-carousel";
import ReactPlayer from "react-player/lazy";
const item = [
  { id: "https://www.youtube.com/watch?v=CaZ9CBR2cMk", status: "completas" },
  { id: "https://www.youtube.com/watch?v=8VW10WSP1EA", status: "completas" },
];

class transmisiones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
    };
    this.breakPoints = [
      { width: 1, itemsToShow: 1 },
      { width: 550, itemsToShow: 2, itemsToScroll: 1 },
      { width: 850, itemsToShow: 2, pagination: false, showArrows: false },
      {
        width: 1150,
        itemsToShow: 2,
        itemsToScroll: 1,
        pagination: false,
        showArrows: false,
      },
      { width: 1450, itemsToShow: 2, pagination: false, showArrows: false },
      { width: 1750, itemsToShow: 2, pagination: false, showArrows: false },
    ];
  }

  render() {


    return (
      <Carousel breakPoints={this.breakPoints}>
        {item.map((item, index) => (
          <div    key={index + "transmisiones3"}>
            <ReactPlayer
              light={true}
              url={item.id}
              className="react-player video-youtube-Tras"
              key={index + "transmisiones"}
              width="100%"
              height="100%"
              config={{
                youtube: {
                  playerVars: { 
                    'controls': 0,
                    'origin': 'https://www.calculadoradefletes.com' 
                },
                },
              }}
            />
            {item.status === "proximas" && (
              <div key={index + "transmisiones1"} className="p-1">
                <a
                key={index + "transmisiones2"}
                  className="btn-style-quest"
                  href="https://fb.me/e/6wGtSZoNJ"
                  rel="noopener noreferrer"
                  target="_blank"
                ><span>Evento online Gratis, click aquí</span>
                </a>
              </div>
            )}
          </div>
        ))}
      </Carousel>
    );
  }
}

export default transmisiones;

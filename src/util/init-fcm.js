import * as firebase from "firebase/app";
import "firebase/messaging";

const initializedFirebaseApp = firebase.initializeApp({
  messagingSenderId: "1034730132272"
});

const messaging = initializedFirebaseApp.messaging();

messaging.usePublicVapidKey(
  "BEkCcughSP9wENEzUP8hWoQJ35aSV5ZDqyxBRloa617Lr-Dg6BHa90SFJdxKl59YSXCvJb--4vB5645V3Fy-1_I"
);

export { messaging };

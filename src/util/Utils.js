import { firebaseConf } from "./Firebase";
const axios = require("axios");

export const mapOrder = (array, order, key) => {
  array.sort(function (a, b) {
    var A = a[key],
      B = b[key];
    if (order.indexOf(A + "") > order.indexOf(B + "")) {
      return 1;
    } else {
      return -1;
    }
  });
  return array;
};
export const getDateWithFormat = () => {
  const today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!

  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  return dd + "-" + mm + "-" + yyyy;
};
export const getCurrentTime = () => {
  const now = new Date();
  return now.getHours() + ":" + now.getMinutes();
};
export const addCommas = (nStr) => {
  nStr += "";
  var x = nStr.split(".");
  var x1 = x[0];
  var x2 = x.length > 1 ? "." + x[1] : "";
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, "$1" + "," + "$2");
  }
  return x1 + x2;
};
export const fechaFormateada = (nStr) => {
  var x = nStr.split("-");
  var date = new Date(
    Date.UTC(Number(x[0]), Number(x[1]) - 1, Number(x[2]) + 1, 0, 0, 0)
  );
  const options = { month: "short", day: "numeric" };
  var fecha = date.toLocaleDateString("es-ES", options);
  return fecha;
};
export const validate = (data) => {
  var ruc = data.replace(/[-.,[\]()\s]+/g, "");

  if ((ruc = Number(ruc)) && ruc % 1 === 0 && rucValido(Number(ruc))) {
    return true;
  } else {
    return false;
  }
};

export const isMovil = () => {
  if (
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  } else {
    return false;
  }
};

export function conver_conten(valor) {
  var contenedores = " ";
  var flete = valor[0].input_contenedores;
  for (var i = 0; i < flete.length; i++) {
    switch (flete[i][0]) {
      case 1:
        contenedores += flete[i][1] + "*20' STD, ";
        break;
      case 2:
        contenedores += flete[i][1] + "*40' STD, ";
        break;
      case 3:
        contenedores += flete[i][1] + "*40' HC, ";
        break;
      case 4:
        contenedores += flete[i][1] + "*40' NOR";
        break;

      default:
    }
  }
  return contenedores;
}
function rucValido(ruc) {
  //11 dígitos y empieza en 10,15,16,17 o 20
  if (
    !(
      (ruc >= 1e10 && ruc < 11e9) ||
      (ruc >= 15e9 && ruc < 18e9) ||
      (ruc >= 2e10 && ruc < 21e9)
    )
  ) {
    return false;
  } else {
    return true;
  }
}
export const gastosUsa = () => {
  let peso = Number(localStorage.weight) / 1000;
  let costoUsa;
  let mayor;
  if (Number(localStorage.volumen) > peso) {
    mayor = Number(localStorage.volumen);
  } else if (Number(localStorage.volumen) == peso) {
    mayor = volumen;
  } else {
    mayor = peso;
  }

  if (mayor * 18 < 20) {
    costoUsa = 90;
  } else {
    costoUsa = mayor * 18 + 70;
  }
  return costoUsa;
};
export function ErrorEspa(value) {
  switch (value.code) {
    case "auth/email-already-in-use":
      return "Este correo ya está siendo usado por otro usuario";
    case "auth/email-already-exists":
      return "Este correo ya está siendo usado por otro usuario";
    case "auth/user-disabled":
      return "Este usuario ha sido deshabilitado";
    case "auth/operation-not-allowed":
      return "Operación no permitida";
    case "auth/invalid-email":
      return "Correo electrónico no valido";
    case "auth/wrong-password":
      return "Contraseña incorrecta";
    case "auth/user-not-found":
      return "No se encontró cuenta del usuario con el correo especificado";
    case "auth/network-error":
      return "Problema al intentar conectar al servidor";
    case "auth/network-request-failed":
      return "Problema al intentar conectar al servidor";
    case "auth/cancelled-popup-request":
      return "La ventana emergente ha sido  bloqueada, recomendamos desactivar el bloqueo de ventanas emergentes o crear una cuenta";
    case "auth/popup-closed-by-user":
      return "La ventana emergente ha sido  cerrada antes de completar la solicitud";
    case "auth/popup-blocked":
      return "La ventana emergente ha sido  bloqueada, recomendamos desactivar el bloqueo de ventanas emergentes o crear una cuenta";
    case "auth/weak-password":
      return "Contraseña debe ser de al menos 6 caracteres";
    case "auth/missing-email":
      return "Hace falta registrar un correo electrónico";
    case "auth/internal-error":
      return "Error interno";
    case "auth/invalid-custom-token":
      return "Token personalizado invalido";
    case "auth/tooMany-requests":
      return "Ya se han enviado muchas solicitudes al servidor";
    case "auth/web-storage-unsupported":
      return "Este navegador no es compatible o se encuentran deshabilitadas las cookies y los datos de terceros.";
    default:
      return value.message;
  }
}
export function usersRegister() {
  var gastos;
  var Impuestos;
  var Transporte;
  var Seguro;

  if (!JSON.parse(localStorage.check) == false) {
    gastos = "SI";
  } else {
    gastos = "NO";
  }

  if (!JSON.parse(localStorage.check1) == false) {
    Transporte = "SI";
  } else {
    Transporte = "NO";
  }

  if (!JSON.parse(localStorage.check2) == false) {
    Impuestos = "SI";
  } else {
    Impuestos = "NO";
  }

  if (!JSON.parse(localStorage.check) == false) {
    Seguro = "SI";
  } else {
    Seguro = "NO";
  }

  let data = JSON.parse(sessionStorage.getItem("user"));
  let token = JSON.parse(localStorage.getItem("token"));
  const params = {
    fecha: getDateWithFormat() + " " + getCurrentTime(),
    ID: token.toString(),
    nombre: data[1].toString(),
    email: data[2].toString(),
    pais: sessionStorage.country,
    TipoFlete: localStorage.transporte,
    TipoConte: localStorage.tipocon,
    FleteOrigen: localStorage.puerto,
    FleteDestino: localStorage.puertoD,
    ContenidoFlete: localStorage.contenedoresDes,
    gastos: gastos,
    Impuestos: Impuestos,
    Transporte: Transporte,
    Seguro: Seguro,
  };
  if (data[2] !== "denisosuna@gmail.com") {
    firebaseConf
      .database()
      .ref("Registrados")
      .push(params)
      .then(() => {})
      .catch((err) => {
        console.log("FAILED...", err);
      });
  }
  return true;
}
export function usersIn(ip, pais) {
  const params = {
    fecha: getDateWithFormat() + " " + getCurrentTime(),
    pais: pais,
    ubicacion: window.location.pathname,
    ip: ip,
  };
  if (ip !== "190.73.138.60") {
    firebaseConf
      .database()
      .ref("IngresoHome")
      .push(params)
      .then(() => {})
      .catch((err) => {
        console.log("FAILED...", err);
      });
  }
  return true;
}
export function telefonoValid(telefono) {
  var regex = /[0-9]{6,}/;
  var telefonoValue = regex.test(telefono);

  if (telefonoValue) {
    return true;
  } else {
    return false;
  }
}
export function empresaValid(empresa) {
  var regex = /[0-9a-zA-Z]{3,}/;
  var empresaValue = regex.test(empresa);

  if (empresaValue) {
    return true;
  } else {
    return false;
  }
}
export const footer = (paisDestino) => {
  var imagenPath;
  if (paisDestino == "PERU") {
    imagenPath = "/assets/img/login/icons/direccionPer.png";
    return imagenPath;
  } else if (paisDestino == "PANAMA") {
    imagenPath = "/assets/img/login/icons/direccionPan.png";
    return imagenPath;
  } else {
    imagenPath = "/assets/img/login/icons/direccionVen.png";
    return imagenPath;
  }
};


export const numero = (paisDestino) => {
  var imagenPath;
  if (paisDestino == "PERU") {
    imagenPath = "51920301745";
    return imagenPath;
  } else if (paisDestino == "PANAMA") {
    imagenPath = "50762155389";
    return imagenPath;
  } else {
    imagenPath = "584241153525";
    return imagenPath;
  }
};


export const paisImport = (paisDestino) => {
  var imagenPath;
  if (paisDestino !== undefined) {
    if (paisDestino == "Perú" || paisDestino == "PERÚ") {
      imagenPath = "flag-peru";
      return imagenPath;
    } else if (paisDestino == "Panamá" || paisDestino == "PANAMÁ") {
      imagenPath = "flag-panama";
      return imagenPath;
    } else {
      imagenPath = "flag-vnzla";
      return imagenPath;
    }
  } else {
    imagenPath = "flag-peru";
    return imagenPath;
  }
};

export const imgProducto = (paisDestino) => {
  var imagenPath;

  if (paisDestino == "Perú" || paisDestino == "PERÚ") {
    imagenPath = "/assets/img/flags/peru.svg";
    return imagenPath;
  } else if (paisDestino == "Panamá" || paisDestino == "PANAMÁ") {
    imagenPath = "/assets/img/flags/panama.svg";
    return imagenPath;
  } else if (paisDestino == "Venezuela" || paisDestino == "VENEZUELA") {
    imagenPath = "/assets/img/flags/venezuela.svg";
    return imagenPath;
  }else if (paisDestino == "China" || paisDestino == "CHINA") {
    imagenPath = "/assets/img/flags/china.svg";
    return imagenPath;
  } else {
    imagenPath = "/assets/img/profile-pic-l.png";
    return imagenPath;
  }
};




export const taxes = (paisDestino) => {
  var taxesImagen;
  if (paisDestino == "PERU") {
    taxesImagen = "/assets/img/sunat2.png";
    return taxesImagen;
  } else if (paisDestino == "PANAMA") {
    taxesImagen = "/assets/img/ADUANA-PANAMA.png";
    return taxesImagen;
  } else {
    taxesImagen = "/assets/img/LogoSENIAT.png";
    return taxesImagen;
  }
};
export const taxesAlert = (paisDestino) => {
  var taxesImagen;
  if (paisDestino == "PERU") {
    taxesImagen = "icon-SunatAlert";
    return taxesImagen;
  } else if (paisDestino == "PANAMA") {
    taxesImagen = "icon-aduanaPanaAlert";
    return taxesImagen;
  } else {
    taxesImagen = "icon-aduanaVeneAlert";
    return taxesImagen;
  }
};
export const taxesAlertSM = (paisDestino) => {
  var taxesImagen;
  if (paisDestino == "PERU") {
    taxesImagen = "icon-SunatSm";
    return taxesImagen;
  } else if (paisDestino == "PANAMA") {
    taxesImagen = "icon-aduanaPanaSm";
    return taxesImagen;
  } else {
    taxesImagen = "icon-aduanaVeneSm";
    return taxesImagen;
  }
};
export function errores(tipo, error) {
  let info = {
    previousSites: window.history.length,
    browserName: window.navigator.appName,
    browserEngine: window.navigator.product,
    browserVersion1a: window.navigator.appVersion,
    browserVersion1b: navigator.userAgent,
    browserLanguage: navigator.language,
    browserOnline: navigator.onLine,
    browserPlatform: navigator.platform,
    javaEnabled: navigator.javaEnabled(),
    dataCookiesEnabled: navigator.cookieEnabled,
    sizeScreenW: window.screen.width,
    sizeScreenH: window.screen.height,
  };
  const params = {
    fecha: getDateWithFormat() + " " + getCurrentTime(),
    ubicacion: window.location.pathname,
    ip: sessionStorage.IP,
    tipo: tipo,
    error: error,
    info: info,
  };
  firebaseConf
    .database()
    .ref("Errores")
    .push(params)
    .then(() => {})
    .catch((err) => {
      console.log("FAILED...", err);
    });
  return true;
}

export function igv(item, pais, totalIGVCostos) {
  var valor;
  switch (pais) {
    case "PERU":
      if (
        item.tipo != "THCD" &&
        item.tipo != "TCHD DOCUMENTACION" &&
        item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
        item.tipo != "FLETE" &&
        item.tipo != "FLETE MARÍTIMO" &&
        item.tipo != "FLETE AÉREO" &&
        totalIGVCostos > 0
      ) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
        ) {
          valor = ((item.valor / 3) * 2 * 0.18).toFixed(2);
        } else {
          valor = (item.valor * 0.18).toFixed(2);
        }
      } else {
        valor = "n/a";
      }

      break;
    case "PANAMA":
      if (
        item.tipo == "PORT CHARGES" ||
        (item.tipo == "MANEJOS" && totalIGVCostos > 0)
      ) {
        valor = (item.valor * 0.07).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "VENEZUELA":
      valor = "n/a";
      break;

    default:
  }
  return valor;
}
export function igvMasTotal(item, pais, totalIGVCostos) {
  var valor;
  switch (pais) {
    case "PERU":
      if (
        item.tipo != "THCD" &&
        item.tipo != "TCHD DOCUMENTACION" &&
        item.tipo != "SEGURO DE MERCANCÍA SUGERIDO" &&
        item.tipo != "FLETE" &&
        item.tipo != "FLETE MARÍTIMO" &&
        item.tipo != "FLETE AÉREO" &&
        totalIGVCostos > 0
      ) {
        if (
          item.tipo == "SERVICIO LOGÍSTICO" ||
          item.tipo == "GANANCIA - SERVICIO LOGÍSTICO"
        ) {
          valor = ((item.valor / 3) * 2 * 0.18 + item.valor).toFixed(2);
        } else {
          valor = (item.valor * 1.18).toFixed(2);
        }
      } else {
        valor = item.valor;
      }

      break;
    case "PANAMA":
      if (
        item.tipo == "PORT CHARGES" ||
        (item.tipo == "MANEJOS" && totalIGVCostos > 0)
      ) {
        valor = (item.valor * 0.07 + item.valor).toFixed(2);
      } else {
        valor = item.valor;
      }

      break;
    case "VENEZUELA":
      valor = item.valor;
      break;

    default:
  }
  return valor;
}

export function totalGeneralIgv(item, pais, totalIGVCostos) {
  var valor;
  switch (pais) {
    case "PERU":
      if (totalIGVCostos > 0) {
        valor = ((item / 3) * 2 * 0.18).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "PANAMA":
      if (totalIGVCostos > 0) {
        valor = ((item / 3) * 2 * 0.07).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "VENEZUELA":
      valor = "n/a";
      break;

    default:
  }
  return valor;
}

export function totalGeneral(item, pais, totalIGVCostos) {
  var valor;
  switch (pais) {
    case "PERU":
      if (totalIGVCostos > 0) {
        valor = ((item / 3) * 2 * 0.18).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "PANAMA":
      if (totalIGVCostos > 0) {
        valor = ((item / 3) * 2 * 0.07).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "VENEZUELA":
      valor = "n/a";
      break;

    default:
  }
  return valor;
}
export function totalGeneralMasIGVGanancias(item, item2, pais, totalIGVCostos) {

  var valor;
  switch (pais) {
    case "PERU":
      if (totalIGVCostos > 0) {
        valor = ((Number(item) / 3) * 2 * 0.18 + Number(item2)).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "PANAMA":
      if (totalIGVCostos > 0) {
        valor = Number(item2).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "VENEZUELA":
      valor = "n/a";
      break;

    default:
  }
  return valor;
}

export function totalGeneralMasIgvVEnta(
  servicioLogisticoTotal,
  totalIGVFleteCostos,
  pais,
  totalIGVCostos,
  totalVenta
) {
  var valor;
  switch (pais) {
    case "PERU":
      if (totalIGVCostos > 0) {
        valor = (
          Number(totalIGVFleteCostos) +
          Number(
            (servicioLogisticoTotal / 3) * 2 * 0.18 + servicioLogisticoTotal
          )
        ).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "PANAMA":
      if (totalIGVCostos > 0) {
        valor = (Number(totalVenta) + Number(totalIGVCostos)).toFixed(2);
      } else {
        valor = "n/a";
      }

      break;
    case "VENEZUELA":
      valor = "n/a";
      break;

    default:
  }
  return valor;
}

export function totalGeneralCostos(item, pais) {
  var valor;
  switch (pais) {
    case "PERU":
      valor = item;
      break;
    case "PANAMA":
      valor = item;
      break;
    case "VENEZUELA":
      valor = "";
      break;

    default:
  }
  return valor;
}

export function getIpLocation() {
  (async () => {
    await axios
      .get("https://ipv4.icanhazip.com/")
      .then((resp) => {
        sessionStorage.IP = resp.data;
        let ip = resp.data;

        axios
          .get("https://get.geojs.io/v1/ip/country/" + ip + ".json")
          .then((resp) => {
            sessionStorage.country = resp.data.country;
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  })();
}

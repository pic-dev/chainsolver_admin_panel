import React from "react";
import Zoom from "@material-ui/core/Zoom";
import Grid from "@material-ui/core/Grid";



export default function ConcentradoresDiv() {

  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  return (


      <Grid container spacing={1}>

        <Zoom in={checked}>
          <Grid style={{display:checked ? "unset" : "none"}} item xs={12} sm={12} md={6}>
                   <iframe
                  className="react-player imgDetails2"
                  width="40%"
                  height="75%"
                  src="https://www.youtube-nocookie.com/embed/_baTNgLo8So"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
          
          </Grid>
        </Zoom>
        <Zoom in={checked}>
          <Grid style={{display:checked ? "unset" : "none"}} item xs={12} sm={12} md={6}>
                <iframe
                  className="react-player imgDetails2"
                  width="40%"
                  height="75%"
                  src="https://www.youtube-nocookie.com/embed/_baTNgLo8So"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
        
          </Grid>
        </Zoom>
     
        <Zoom in={!checked}>
          <Grid style={{display:!checked ? "unset" : "none"}} onClick={handleChange} item xs={12} sm={12} md={12}>
            <div
              className="img  ConcentradoresStyle"
         
              
            />
          </Grid>
        </Zoom> </Grid>

  );
}

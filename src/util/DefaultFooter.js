/*eslint-disable*/
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBriefcase,
  faPhone,
  faNewspaper
} from "@fortawesome/free-solid-svg-icons";
// reactstrap components
import { Container } from "reactstrap";

// core components
class DefaultFooter extends Component {
  render() {
    return (
      <footer className="footer footer-default">
        <Container>
          <nav>
            <ul>
              <li>
              <a
                  rel="noopener noreferrer"
                  style={{ color: "#ffffff" }}
                  href="https://www.pic-cargo.com/"
                  target="_blank"
                >
                  <FontAwesomeIcon icon={faNewspaper} className="mr-2" />{" "}
                  Servicios PIC-Cargo
                </a>
              </li>
              <li>
                <a
                          rel="noopener noreferrer"
                          style={{ color: "#ffffff" }}
                  href="https://www.pic-cargo.com/es/quienes-somos/"
                  target="_blank"
                > <FontAwesomeIcon icon={faBriefcase} className="mr-2" /> Quienes Somos
                </a>
              </li>
              <li>
              <a
                  rel="noopener noreferrer"
                  style={{ color: "#ffffff" }}
                  href="https://www.pic-cargo.com/es/contact/"
                  target="_blank"
                >
                  <FontAwesomeIcon icon={faPhone} className="mr-2" /> Contacto
                </a>
              </li>
            </ul>
          </nav>
          <div  className="copyright" id="copyright">
          Copyright © {new Date().getFullYear()}        
            <a
               rel="noopener noreferrer"
               style={{ color: "#ffffff" }}
              href="https://www.pic-cargo.com/es/"
              target="_blank"
            > PIC Cargo.
            </a>
          
     
              
         
                
            <a
               rel="noopener noreferrer"
               style={{ color: "#ffffff" }}
              href="https://www.facebook.com/PicCargoPeru/"
              target="_blank"
            > <i className="simple-icon-social-facebook  ml-3 mr-2" />
            </a>
            <a
               rel="noopener noreferrer"
               style={{ color: "#ffffff" }}
              href="https://twitter.com/pic_cargo"
              target="_blank"
            > <i className="simple-icon-social-twitter  mr-2" />
            </a>
            <a
               rel="noopener noreferrer"
               style={{ color: "#ffffff" }}
              href="https://www.instagram.com/pic_cargo/"
              target="_blank"
            > <i className="simple-icon-social-instagram" />
            </a>
          </div>
        </Container>
      </footer>
    );
  }
}
export default DefaultFooter;

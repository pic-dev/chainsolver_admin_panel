import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutline";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  table: {
    minWidth: "auto",
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {


  var totalAges = items.reduce((sum, value) => (typeof value.precio == "number" ? sum + value.precio : sum), 0);


  return totalAges;
}

function subcantidad(items) {

  var totalAges = items.reduce((sum, value) => (typeof value.cantidad == "number" ? sum + value.cantidad : sum), 0);


  return totalAges;
}


export default function SpanningTable(props) {
  const classes = useStyles();


  const invoiceTotal = subtotal(props.productos);
  const invoiceTotalCantidad = subcantidad(props.productos);



  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell align="center" colSpan={6}>
              Detalle de Pre-Orden
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Pagina</TableCell>
            <TableCell>Producto</TableCell>
            <TableCell>Codigo</TableCell>
            <TableCell>Qty.</TableCell>
            <TableCell>Precio Unitario</TableCell>
            <TableCell>Sub-Total</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.productos.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.pagina}</TableCell>
              <TableCell>{row.producto}</TableCell>
              <TableCell>
                {row.codigo_producto.length > 0 ? row.codigo_producto : "-"}
              </TableCell>
              <TableCell>{row.cantidad}</TableCell>
              <TableCell className={`${row.precio > 0 ? "" : "color-red arial"}`} >
                {row.precio > 0 ? (
                  row.precio
                ) : ("P/C")}
              </TableCell>
              <TableCell>
                {row.precio > 0
                  ? ccyFormat(priceRow(row.precio, row.cantidad))
                  : "-"}
              </TableCell>
            </TableRow>
          ))}
          {/*invoiceTotal > 0 && (
            <TableRow>
              <TableCell className="text-center" colSpan={5}>
                Total
              </TableCell>
              <TableCell>{ccyFormat(priceRow(invoiceTotalCantidad, invoiceTotal))}</TableCell>
            </TableRow>
          )*/}

               <TableRow>
              <TableCell className="text-center color-red" colSpan={7}>
                *p/c - Precio por Confirmar
              </TableCell>
            
            </TableRow>
      
        </TableBody>
      </Table>
    </TableContainer>
  );
}

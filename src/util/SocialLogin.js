import React, { Component } from "react";
import { GoogleLogout, GoogleLogin } from "react-google-login";
import { Colxx } from "Components/CustomBootstrap";
import { Row } from "reactstrap";
const clientId =
  "424806465438-hsc6nlof3m2ock89ff57kdt3dv8tqtdf.apps.googleusercontent.com";

export default class socialLogin extends Component {
  state = {
    isLoggedIng: false,
    isLoggedInf: false,
    userID: "",
    name: "",
    email: "",
    picture: "",
  };

  responseFacebook = (response) => {
    if (response.userID) {
      this.setState({
        isLoggedInf: true,
        userID: response.userID,
        name: response.name,
        email: response.email,
        picture: response.picture.data.url,
      });
      let user = [
        [response.userID],
        [response.name],
        [response.email],
        [response.picture.data.url],
      ];
      sessionStorage.setItem("user", JSON.stringify(user));
      sessionStorage.usuario = "2";
      if (
        (localStorage.page == "3" || localStorage.page == "2") &&
        localStorage.validLogin == "1"
      ) {
        this.props.history.push("/app/Presupuesto");
        window.location.reload();
      } else {
        window.location.reload();
      }
    } else {
    }
  };

  responseGoogle = (response) => {
    if (response.profileObj.googleId) {
      this.setState({
        isLoggedIng: true,
        userID: response.profileObj.googleId,
        name: response.profileObj.name,
        email: response.profileObj.email,
        picture: response.profileObj.imageUrl,
      });
      let user = [
        [response.profileObj.googleId],
        [response.profileObj.name],
        [response.profileObj.email],
        [response.profileObj.imageUrl],
      ];
      sessionStorage.setItem("user", JSON.stringify(user));
      sessionStorage.usuario = "2";
      if (
        (localStorage.page == "3" || localStorage.page == "2") &&
        localStorage.validLogin == "1"
      ) {
        this.props.history.push("/app/Presupuesto");

        window.location.reload();
      } else {
        window.location.reload();
      }
    } else {
    }
  };

  render() {
    let GgContent;
    const responseGoogle = (response) => {};

    let fbContent;
    if (this.state.isLoggedInf) {
    } else {
      fbContent = (
        <FacebookLogin
          appId="679686622784959"
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
          render={(renderProps) => (
            <button
              style={{
                borderRadius: "13px",
                fontSize: "14px",
              }}
              className="fbLogin"
              onClick={renderProps.onClick}
            >
              {" "}
              <i className="icon-facebook" />
              <span>Facebook,&nbsp;</span>{" "}
              <span
                style={{
                  fontWeight: "bold",
                  cursor: "pointer",
                  textDecoration: "underline",
                }}
              >
                solo con un click
              </span>
            </button>
          )}
        />
      );
    }

    if (this.state.isLoggedIng) {
    } else {
      GgContent = (
        <GoogleLogin
          clientId={clientId}
          buttonText="Login with Google"
          onSuccess={this.responseGoogle}
          render={(renderProps) => (
            <button
              className="GLogin"
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
            >
              <i className="icon-google" />
              Inicia sesión con Google
            </button>
          )}
          onFailure={responseGoogle}
        />
      );
    }

    return (
      <div>
        <Colxx
          xxs="12"
          md="10"
          sm="12"
          className="mx-auto mt-2 "
          style={{ textAlign: "center" }}
        >
          {fbContent}
        </Colxx>
        <Colxx
          xxs="12"
          md="10"
          sm="12"
          className="mx-auto mt-5 pb-2"
          style={{
            textAlign: "center",
          }}
        >
          {GgContent}
        </Colxx>
      </div>
    );
  }
}

import React, { Component } from "react";
import { Colxx } from "Components/CustomBootstrap";
import { ListGroup, ListGroupItem, Badge, Row } from "reactstrap";
class Item extends Component {
  _remove() {
    if (this.props.onRemove) this.props.onRemove();
  }
  render() {
    return (
      <ListGroupItem
        className="mb-2"
        style={{
          color: "#000000",
          backgroundColor: "#e1e2e2",
          fontSize: "1.2em",
          padding: "0.2em",
        }}
      >
        <Row>
          <Colxx xxs="1" md="1" className="NoPadding mx-auto  my-auto ">
            <i
              onClick={this._remove.bind(this)}
              className="simple-icon-minus color-white"
              style={{
                cursor: "pointer",
                fontWeight: "bold",
                borderRadius: "50px",
                fontSize: "1.2em",
              }}
            />
          </Colxx>
          <Colxx xxs="2" md="2" className="mr-2 mx-auto text-center my-auto ">
            <Badge color="light">
              {" "}
              &nbsp; {this.props.tablePosition + 1}&nbsp;
            </Badge>
          </Colxx>
          <Colxx xxs="3" md="3" className="mr-2 mx-auto text-center my-auto ">
            <Badge color="light"> {this.props.data.Bultos}</Badge>
          </Colxx>
          <Colxx xxs="3" md="3" className="mx-auto text-center my-auto ">
            <Badge color="light"> {this.props.data.peso}&nbsp;Kg&nbsp;</Badge>
          </Colxx>
          <Colxx xxs="3" md="3" className="mx-auto text-center my-auto ">
            <Badge color="light">
              {" "}
              {this.props.data.volumen}&nbsp;M³&nbsp;
            </Badge>
          </Colxx>
        </Row>
      </ListGroupItem>
    );
  }
}

export default Item;

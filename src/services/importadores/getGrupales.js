import { API_URL } from "./settings";

export async function getAbonos(campaña,id) {
  let apiURL = `${API_URL}/abonos/cam/${campaña}/${id}`;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const amount = data;
        return amount;
      }
    });
}

export async function getAbonosTotal(id) {
  let apiURL = `${API_URL}/rep/campana/senior_with/s/${id} `;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const amount = data;
        return amount;
      }
    });
}

export async function getJuniorByCampaingSenior(id_campaña, id_senior) {
  let apiURL = `${API_URL}/rep/campana/junior_with/${id_campaña}/${id_senior} `;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const Junior = data;
        return Junior;
      }
    });
}

export async function getJuniorBySenior(id_senior) {
  let apiURL = `${API_URL}/rep/junior_with/s/${id_senior} `;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const Junior = data;
        return Junior;
      }
    });
}

export async function getJuniorOrdersBySenior(id_senior) {
  let apiURL = `${API_URL}/rep/junior/${id_senior} `;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const Junior = data;
        return Junior;
      }
    });
}

export async function getCampaigns() {
  return await fetch(`${API_URL}/campana`)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const campañas = data;
        return campañas;
      }
    });
}

export async function getCampaignsBySenior(idSenior) {
  return await fetch(`${API_URL}/campana/senior/${idSenior}`)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const campañas = data;
        return campañas;
      }
    });
}

export async function getCampaignsByJunior(idSenior) {
  return await fetch(`${API_URL}/campana/junior/${idSenior}`)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const campañas = data;
        return campañas;
      }
    });
}

export async function getOrdenByCampaigns(id_campana) {
  return await fetch(`${API_URL}/rep/orden/campana/${id_campana}`)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const campañas = data;
        return campañas;
      }
    });
}

export async function getArticulosPage({
  catalogo,
  limit = 10,
  page = 0,
} = {}) {
  let apiURL = `${API_URL}/articulos/pag/${catalogo}/${limit}/${limit * page}`;
  return await fetch(apiURL)
    .then((res) => res.json())
    .then((response) => {
      const data = ([] = response);

      if (Array.isArray(data)) {
        return data;
      }
    });
}

export async function getTopArticulos(catalogo) {
  return await fetch(`${API_URL}/articulos/top/${catalogo}`)
    .then((res) => res.json())
    .then((response) => {
      const data = response;
      if (Array.isArray(data)) {
        const campañas = data;
        return campañas;
      }
    });
}

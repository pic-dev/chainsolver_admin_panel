import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import ReactDOM from "react-dom";
import ErrorBoundary from "Routes/pages/ErrorBoundary.js";
import TopNav from "Routes/pages/TopNav";
import OneSignal from "react-onesignal";
import { initGA } from "Util/tracking";
import Install from "Util/InstalarPWA";
import Spinner from "Components/spinner";
import { isMovil, getIpLocation } from "Util/Utils";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { blue, green } from "@material-ui/core/colors";
import { useAuthStateChanged } from "Hooks/useAuthStateChanged";
import "./assets/css/vendor/bootstrap.min.css";
import "react-perfect-scrollbar/dist/css/styles.css";
import "react-bootstrap-typeahead/css/Typeahead.css";

const Error = React.lazy(() => import("Routes/pages/error.js"));
const Inicio = React.lazy(() =>
  import("Routes/pages/calculadoraFletes/Inicio.js")
);
const Ruta = React.lazy(() => import("Routes/pages/calculadoraFletes/Ruta.js"));
const Servicios = React.lazy(() =>
  import("Routes/pages/calculadoraFletes/Servicios.js")
);
const Presupuesto = React.lazy(() =>
  import("Routes/pages/calculadoraFletes/Presupuesto.js")
);
const ImporAdmin = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImpoAdmin.js")
);
const ImpoClients = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImpoClients.js")
);

const ImpoStock = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImpoStock.js")
);

const Politicas = React.lazy(() =>
  import("Routes/pages/calculadoraFletes/Politicas.js")
);
const Condiciones = React.lazy(() =>
  import("Routes/pages/calculadoraFletes/Condiciones.js")
);
const Importadores = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImpoHome.js")
);
const Ahorrar = React.lazy(() =>
  import("Routes/pages//calculadoraFletes/Ahorrar.js")
);
const FAQ = React.lazy(() => import("Routes/pages/FAQ.js"));
const JuniorDashboard = React.lazy(() =>
  import("Routes/pages/importadoresDashboard/Dashboard/agregarJunior/Junior.js")
);
const PedidosJunior = React.lazy(() =>
  import(
    "Routes/pages/importadoresDashboard/Dashboard/VerPedidos/PedidosJunior.js"
  )
);
const SingInSenior = React.lazy(() =>
  import("Routes//pages/importadoresDashboard/SingIn.js")
);
const Dashboard = React.lazy(() =>
  import("Routes/pages/importadoresDashboard/Dashboard/Index.js")
);
const ProtectedRoute = React.lazy(() =>
  import("Routes/pages/importadoresDashboard/protectedRoute.js")
);
const ImportadoresVip = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImportadoresVip.js")
);
const BusquedaPedido = React.lazy(() =>
  import("Routes//pages/importadoresDashboard/Junior/busquedaPedido.js")
);
const BusquedaPedidoDashboard = React.lazy(() =>
  import(
    "Routes/pages/importadoresDashboard/Dashboard/BuscarPedido/busquedaPedidoDashboard"
  )
);
const SeccionImportadores = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ImpoHome.js")
);
const ProductoDetalle = React.lazy(() =>
  import("Routes/pages/seccionImportadores/ProductoDetalle.js")
);
const Abonos = React.lazy(() =>
  import("Routes/pages/importadoresDashboard/Dashboard/verAbonos/abonos.js")
);
const renderLoader = () => <Spinner />;

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blue[700],
    },
    secondary: {
      main: blue[600],
    },
    success: {
      main: green[600],
    },
  },
});

if (process.env.NODE_ENV == "production") {
  initGA("UA-145052369-9");
}
function MainApp(props) {
  const [userSenior, setUserSenior] = useState(false);
  const [userSeniorData, setUserSeniorData] = useState([]);
  const [transport, setTransport] = useState();
  const [cantidad, setcantidad] = useState();
  const api = "https://point.pic-retail.com/api_chainsolver/v1/";
  const [pwaInstaling, pwaInstalingToggle] = useState([]);
  const [isInstallSuccess, SetisInstallSuccess] = useState(false);
  const [ClosePWAMsg, SetClosePWAMsg] = useState(false);

  const { usuario, estadoAuth } = useAuthStateChanged();
  if (sessionStorage.country !== undefined) {
    getIpLocation();
  }

  useEffect(() => {
    var options = {
      safari_web_id: "web.onesignal.auto.01ea4289-b460-45e4-8d90-838752554827",
      autoRegister: true,
      persistNotification: true,
      autoResubscribe: true,
    };

    OneSignal.initialize("22578d3b-cdde-4ec2-9535-40f64b1a3213", options);
  }, []);

  useEffect(() => {
    if (pwaInstaling.isInstallSuccess) {
      SetisInstallSuccess(true);
    }
  }, [pwaInstaling]);

  useEffect(() => {
    if (sessionStorage.seniorLogin !== undefined) {
      setUserSeniorData(JSON.parse(sessionStorage.getItem("seniorLogin")));
      handleLogin();
    }
  }, []);

  const handleLogin = () => {
    setUserSenior(true);
  };

  const handleLogout = () => {
    sessionStorage.removeItem("seniorLogin");
    setUserSenior(false);
  };

  return (
    <Router>
      <div id="app-container">
        <TopNav
          toggle={sessionStorage.usuario}
          usuario={usuario}
          estadoAuth={estadoAuth}
          page={localStorage.page}
          api={api}
          user={userSenior}
          setUserSeniorData={setUserSeniorData}
          handleLogin={handleLogin}
        />
        <main>
          <div className="container-fluid">
            <ErrorBoundary>
              <React.Suspense fallback={renderLoader()}>
                <ThemeProvider theme={theme}>
                  <Switch>
                    <Route
                      exact
                      path={`/`}
                      component={(props) => (
                        <Inicio
                          data={transport}
                          name={setTransport}
                          {...props}
                        />
                      )}
                    />
                    <Route
                      path={`/Ruta`}
                      component={(props) => (
                        <Ruta
                          setcantidad={setcantidad}
                          api={api}
                          data={transport}
                          {...props}
                        />
                      )}
                    />
                    <Route
                      path={`/ImpoAdmin`}
                      component={(props) => <ImporAdmin {...props} />}
                    />
                    <Route
                      path={`/Clientes`}
                      component={(props) => <ImpoClients {...props} />}
                    />
                    <Route
                      path={`/Productos`}
                      component={(props) => <ImpoStock {...props} />}
                    />
                    <Route
                      path={`/Ahorrar`}
                      component={(props) => <Ahorrar {...props} />}
                    />
                    <Route
                      path={`/Importadores`}
                      component={(props) => <Importadores {...props} />}
                    />

                    <Route
                      path={`/Servicios`}
                      component={(props) => (
                        <Servicios
                          api={api}
                          cantidad={cantidad}
                          data={transport}
                          {...props}
                        />
                      )}
                    />
                    <Route
                      path={`/Presupuesto`}
                      component={(props) => (
                        <Presupuesto api={api} data={transport} {...props} />
                      )}
                    />
                    <Route
                      path={`/Politicas`}
                      component={(props) => <Politicas {...props} />}
                    />
                    <Route
                      path={`/Condiciones`}
                      component={(props) => <Condiciones {...props} />}
                    />
                    <Route
                      path={`/FAQ`}
                      component={(props) => <FAQ {...props} />}
                    />
                    <Route
                      path={`/ImportadoresVip`}
                      component={(props) => <ImportadoresVip {...props} />}
                    />

                    <Route
                      exact
                      path={`/SeccionImportadores`}
                      component={(props) => <SeccionImportadores {...props} />}
                    />
                    <Route
                      path={`/SeccionImportadores/:id?`}
                      component={(props) => <ProductoDetalle {...props} />}
                    />
                    <Route
                      path={`/error`}
                      component={(props) => <Error {...props} />}
                    />

                    <Route
                      path={`/busquedaPedido`}
                      component={(props) => <BusquedaPedido {...props} />}
                    />
                    <Route
                      path={`/SeniorLogin`}
                      component={(props) => (
                        <SingInSenior
                          user={userSenior}
                          setUserSeniorData={setUserSeniorData}
                          handleLogin={handleLogin}
                          {...props}
                        />
                      )}
                    />
                    <ProtectedRoute
                      exact
                      path="/Dashboard"
                      userSeniorData={userSeniorData}
                      user={userSenior}
                      handleLogout={handleLogout}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path="/BuscarPedido"
                      userSeniorData={userSeniorData}
                      user={userSenior}
                      handleLogout={handleLogout}
                      component={BusquedaPedidoDashboard}
                    />
                    <ProtectedRoute
                      exact
                      path="/Abonos"
                      userSeniorData={userSeniorData}
                      user={userSenior}
                      handleLogout={handleLogout}
                      component={Abonos}
                    />

                    <ProtectedRoute
                      exact
                      path="/JuniorDashboard"
                      userSeniorData={userSeniorData}
                      user={userSenior}
                      handleLogout={handleLogout}
                      component={JuniorDashboard}
                    />
                    <ProtectedRoute
                      exact
                      path="/PedidosJunior"
                      userSeniorData={userSeniorData}
                      user={userSenior}
                      handleLogout={handleLogout}
                      component={PedidosJunior}
                    />
                    <Redirect to="/error" />
                  </Switch>
                </ThemeProvider>
              </React.Suspense>
            </ErrorBoundary>
          </div>
        </main>

        {isMovil() && sessionStorage.pwaNotInstalled == undefined && (
          <Install pwaInstalingToggle={pwaInstalingToggle} />
        )}
        {pwaInstaling.isInstalling && !isInstallSuccess && (
          <div className="Pwa-Install-Msg">
            <div className="Contenedor theme-color-white">
              <img
                src="/assets/img/profile-pic-l.png"
                class="logo palpitar mr-3"
                alt="logo"
              />
              <div className="arial">Instalando...</div>
            </div>
          </div>
        )}
        {isInstallSuccess && ClosePWAMsg && (
          <div className="Pwa-Install-Msg ">
            <div className="Contenedor theme-color-white">
              <div className="pwa-close" onClick={() => SetClosePWAMsg(false)}>
                &times;
              </div>
              <img
                src="/assets/img/profile-pic-l.png"
                class="logo mr-2"
                alt="logo"
              />
              <a href="www.calculadoradefletes.com" className="arial">
                App Instalada, Clic Para abrir
                <i className="color-green simple-icon-check ml-1" />
              </a>
            </div>
          </div>
        )}
      </div>
    </Router>
  );
}
export default ReactDOM.render(<MainApp />, document.getElementById("root"));

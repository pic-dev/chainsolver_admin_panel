import React from "react";
import ReactDOM from "react-dom";
function MainApp(props) {
  return (
    <div className="App">
      <header className="header-App">
        <img src={"/assets/img/login/icons/LOGO.png"} className="App-logo" alt="logo" />
        <p>
        Nos encontramos actualizando tarifas (27-01-2020), Volveremos pronto...
        </p>
     
      </header>
    </div>
  );
}

export default ReactDOM.render(<MainApp />, document.getElementById("root"));


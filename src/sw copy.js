// the cache version gets updated every time there is a new deployment
const CACHE_VERSION = 1.9;
const CURRENT_CACHE = `main-${CACHE_VERSION}`;

// these are the routes we are going to cache for offline support
const cacheFiles = [
  '/assets/fonts/arialBlack/arialBlack.ttf',
  '/assets/fonts/arialBlack/arialBlack.woff2',
  '/assets/fonts/arialBlack/arialBlack.ttf.br',
  '/assets/fonts/simple-line-icons/css/simple-line-icons.css',
  '/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.eot',
  '/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.svg',
  '/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.ttf',
  '/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff',
  '/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff2',
  '/assets/img/login/backgorund.png',
  '/assets/img/login/MARITIMO.gif',
  '/assets/img/login/AEREO.gif',
  '/assets/img/flags/china.svg',
  '/assets/img/flags/china.Round.svg',
  '/assets/img/flags/panama.svg',
  '/assets/img/flags/panamaRound.svg',
  '/assets/img/flags/peru.svg',
  '/assets/img/flags/peruRound.svg',
  '/assets/img/flags/usa.svg',
  '/assets/img/flags/usaRound.svg',
  '/assets/img/flags/venezuela.svg',
  '/assets/img/flags/venezuelaRound.svg',
  '/assets/img/BOT-PIC.gif',
  '/assets/img/help.svg',
  '/assets/img/importar.png',
  '/assets/img/money.svg',
  '/assets/img/seo.svg',
  '/assets/img/01.jpg',
  '/assets/img/contenedores/40nor.png',
  '/assets/img/contenedores/40hq.png',
  '/assets/img/contenedores/40std.png',
  '/assets/img/contenedores/20std.png',
  '/manifest/favicon.ico'

];

// on activation we clean up the previously registered service workers
self.addEventListener('activate', evt =>
  evt.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          if (cacheName !== CURRENT_CACHE) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  )
);

// on install we download the routes we want to cache for offline
self.addEventListener('install', evt =>
  evt.waitUntil(
    caches.open(CURRENT_CACHE).then(cache => {
      return cache.addAll(cacheFiles);
    })
  )
);

// fetch the resource from the network
const fromNetwork = (request, timeout) =>
  new Promise((fulfill, reject) => {
    const timeoutId = setTimeout(reject, timeout);
    fetch(request).then(response => {
      clearTimeout(timeoutId);
      fulfill(response);
      //update(request);
    }, reject);
  });

// fetch the resource from the browser cache
const fromCache = request =>
  caches
    .open(CURRENT_CACHE)
    .then(cache =>
      cache
        .match(request)
        .then(matching => matching || cache.match('/offline/'))
    );

/* cache the current page to make it available for offline
const update = request =>
  caches
    .open(CURRENT_CACHE)
    .then(cache =>
      fetch(request).then(response => cache.put(request, response))
    );*/

// general strategy when making a request (eg if online try to fetch it
// from the network with a timeout, if something fails serve from cache)
self.addEventListener('fetch', evt => {
  evt.respondWith(
    fromNetwork(evt.request, 10000).catch(() => fromCache(evt.request))
  );
  //evt.waitUntil(update(evt.request));
});
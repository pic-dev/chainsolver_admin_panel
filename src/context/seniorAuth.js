import React, { useState } from "react";
import PropTypes from "prop-types";

export const AuthContext = React.createContext();

export function AuthContextProvider(props) {
  const [userSeniorData, setUserSeniorData] = useState([]);
  const [userSenior, setUserSenior] = useState(false);

  const handleLogin = () => {
    setUserSenior(true);
  };


  const handleLogout = () => {
    sessionStorage.removeItem("seniorLogin");
    setUserSenior(false);
  };


  return (
    <AuthContext.Provider value={{ userSeniorData, setUserSeniorData, handleLogout, userSenior,handleLogin }}>
      {props.children}
    </AuthContext.Provider>
  );
}

AuthContextProvider.propTypes = {
  children: PropTypes.node,
};

export const AuthContextConsumer = AuthContext.Consumer;

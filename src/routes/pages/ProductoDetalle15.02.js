import React, { Component } from "react";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  ListGroup,
  Table,
  TabContent,
  TabPane,
  ListGroupItem,
  Collapse,
  Badge,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  CustomInput,
  FormGroup,
  Label,
  Form,
} from "reactstrap";
import { PDFViewer } from "react-view-pdf";
import { firebaseConf } from "../../util/Firebase";
import { imgProducto } from "../../util/Utils";
import classnames from "classnames";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import "react-inner-image-zoom/lib/InnerImageZoom/styles.css";
import InnerImageZoom from "react-inner-image-zoom";
import Likes from "../../util/likes";
import Pedido from "../../util/pedido";
import { PageView } from "../../util/tracking";
import { Link, withRouter } from "react-router-dom";
import { isMovil } from "../../util/Utils";
import RouteBack from "../../util/RouteBack";
import axios from "axios";
class FAQ extends Component {
  constructor(props) {
    super(props);
    this.tab = this.tab.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggleCollage = this.toggleCollage.bind(this);
    this.toggleCatalogos = this.toggleCatalogos.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      ClienteStock: [],
      typeProduct: [],

      catalogosDisponibles:[],
      toggleProductDetailsdata: [],
      product: [],
      activeTab: "2",
      toggleProducto: false,
      togglePDF: true,
      catalogoSelected: "",
      isOpen: false,
      isShowCatalogos: false,
      selectImportador: false,
      modal: false,
      tipoImportador: [],
      tipoImportadorSelected: "",
    };
  }

  componentDidMount() {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }

    axios
      .get("https://point.qreport.site/importador")
      .then((res) => {
        
        this.setState({
          tipoImportador: res.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });

      axios
    .get("https://point.qreport.site/catalogos")
    .then((res) => {
      this.setState({
        catalogosDisponibles: res.data,
      });
    })
    .catch((error) => {
      console.log(error);
    });

    var stock = firebaseConf.database().ref("/CatalagosStock/");
    stock
      .orderByChild("KeyProducto")
      .equalTo(this.props.match.params.id)
      .on("child_added", (snapshot) => {
        var product = snapshot.val();

        var databasestock = firebaseConf
          .database()
          .ref("/Catalogos/" + this.props.match.params.id);

        databasestock.on("value", (snapshot) => {
          var key = [{ key: this.props.match.params.id }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());

          var data = Object.assign(arreglo[1], arreglo[0]);

          this.setState({
            product: this.state.product.concat(Object.assign(product, data)),
          });
          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = this.props.match.params.id;
            let data = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });
            if (data.length > 0) {
              this.setState({
                toggleProductDetailsdata: [data[0]],
                ClienteStock: [data[0].clientes],
                typeProduct: data[0].TipoMerca,
              });
            }

            setTimeout(() => {
              this.setState({
                toggleProducto: true,
              });
            }, 1000);
          }
        });
      });

    var stock = firebaseConf.database().ref("/stock/");
    stock
      .orderByChild("KeyProducto")
      .equalTo(this.props.match.params.id)
      .on("child_added", (snapshot) => {
        var product = snapshot.val();

        var databasestock = firebaseConf
          .database()
          .ref("/product/" + this.props.match.params.id);

        databasestock.on("value", (snapshot) => {
          var key = [{ key: this.props.match.params.id }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());

          var data = Object.assign(arreglo[1], arreglo[0]);

          this.setState({
            product: this.state.product.concat(Object.assign(product, data)),
          });
          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = this.props.match.params.id;
            let data = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });
            if (data.length > 0) {
              this.setState({
                toggleProductDetailsdata: [data[0]],
                ClienteStock: [data[0].clientes],
                typeProduct: data[0].TipoMerca,
              });
            }

            setTimeout(() => {
              this.setState({
                toggleProducto: true,
              });
            }, 1000);
          }
        });
      });
  }

  tab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleCollage() {
    this.setState((prev) => ({
      isOpen: !prev.isOpen,
    }));
  }

  toggleCatalogos() {
    this.setState((prev) => ({
      isShowCatalogos: !prev.isShowCatalogos,
    }));
  }

  toggleModal() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  toggle(e) {
    this.setState((prev) => ({
      catalogoSelected: e,
      togglePDF: !prev.togglePDF,
    }));
  }

  render() {
    var key = this.props.match.params.id;
    console.log(this.state);

    return (
      <Container className="mt-5 pt-5">
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
            <div className="font text-center pb-2">Importaciones Grupales</div>
          </Col>
        </Row>
        {this.state.toggleProducto ? (
          <div>
            <Col
              xs="12"
              md="10"
              sm="12"
              style={{ display: "flex", justifyContent: "space-between" }}
              className="mx-auto pt-2 pb-2"
            >
              <RouteBack />
              <div className="font2 color-blue-2 text-center pb-2">
                {this.state.toggleProductDetailsdata[0].NombreProducto.toUpperCase()}
              </div>
              <img
                key="img"
                src={imgProducto(
                  this.state.toggleProductDetailsdata[0].PaisDesti
                )}
                style={{ height: isMovil() ? "3rem" : "4rem" }}
                alt="logo"
              />
            </Col>
            {this.state.togglePDF ? (
              <Row>
                <Col
                  xs="12"
                  md="10"
                  sm="12"
                  className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
                >
                  <Row className="mx-auto">
                    <Col xs="12" md="6">
                      <Card
                        className="p-1"
                        style={{
                          alignItems: "center",
                        }}
                      >
                        <Carousel
                          infiniteLoop={true}
                          autoPlay={false}
                          showThumbs={false}
                          showIndicators={false}
                          stopOnHover={true}
                          showStatus={false}
                          className="carrouselWidth"
                        >
                          {this.state.toggleProductDetailsdata[0]
                            .Multimedia[1] ? (
                            this.state.toggleProductDetailsdata[0].Multimedia[1].video.map(
                              (picture, index) => (
                                <div>
                                  <video
                                    className="react-player imgDetails"
                                    key={index + "video"}
                                    id={"my-video" + index}
                                    controls
                                    width="100%"
                                    height="100%"
                                    data-setup="{}"
                                    autoPlay
                                    muted
                                  >
                                    <source
                                      src={picture.img}
                                      type={picture.fileType}
                                    />
                                  </video>
                                </div>
                              )
                            )
                          ) : (
                            <InnerImageZoom
                              moveType="drag"
                              src={
                                this.state.toggleProductDetailsdata[0]
                                  .Multimedia[0].imagen[0].img
                              }
                              className="imgDetails p-2"
                              alt="logo"
                            />
                          )}

                          {this.state.toggleProductDetailsdata[0].Multimedia[0].imagen.map(
                            (picture, index) => (
                              <div>
                                <InnerImageZoom
                                  moveType="drag"
                                  src={picture.img}
                                  className="imgDetails p-2"
                                  alt="logo"
                                />
                              </div>
                            )
                          )}
                        </Carousel>
                        <Likes item={key} />
                        <CardBody
                          style={{
                            padding: "1rem 0px 0px",
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          <CardTitle
                            className="m-0 p-0"
                            style={{ fontWeight: "bold" }}
                          >
                            {" "}
                            <a
                              style={{ padding: "0px" }}
                              href="https://wa.link/xbr57j"
                              rel="noopener noreferrer"
                              target="_blank"
                            >
                              {" "}
                              <i className="whatsapp-contacto" />{" "}
                            </a>
                          </CardTitle>
                        </CardBody>
                      </Card>
                    </Col>

                    {this.state.typeProduct == "Proximas" ? (
                      <Col xs="12" md="6">
                        {this.state.toggleProductDetailsdata[0].catalogos
                          .length > 0 ? (
                          <div
                            style={{
                              padding: "1rem 0 1rem 0",

                              textAlign: "center",
                            }}
                          >
                            {" "}
                            <div
                              style={{
                                borderRadius: "0.4rem",
                              }}
                              className="bg-blue-2"
                            >
                              <div
                                className="catalogoTextList text-white arial mb-2 p-3 bg-blue-2"
                                onClick={this.toggleCollage}
                              >
                                <span>Ver Catalogos Disponibles</span>
                                <i
                                  style={{
                                    backgroundColor: "white",
                                    borderRadius: "50%",
                                    fontSize: "1.2rem",
                                    padding: ".2rem",
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                  }}
                                  className={`mr-2 ml-2 float-right color-orange ${
                                    this.state.isOpen
                                      ? "simple-icon-arrow-up"
                                      : "simple-icon-arrow-down"
                                  }`}
                                />
                              </div>

                              <Collapse
                                className="p-2"
                                isOpen={this.state.isOpen}
                              >
                                <ListGroup>
                                  {this.state.toggleProductDetailsdata[0].catalogos.map(
                                    (item, index) => (
                                      <ListGroupItem
                                        style={{
                                          textTransform: "uppercase",
                                          borderBottom: "1px solid #8f8f8f",
                                          fontSize: "1rem",
                                        }}
                                        className="arial"
                                      >
                                        {" "}
                                        {item.titulo}
                                        <i
                                          onClick={() => {
                                            this.toggle(item.img);
                                          }}
                                          style={{
                                            fontSize: "1.2rem",
                                            fontWeight: "bold",
                                            backgroundColor: "black",
                                            color: "white",
                                            borderRadius: "50%",
                                            padding: ".1rem .4rem",
                                            cursor: "pointer",
                                          }}
                                          className="ml-4 simple-icon-eye"
                                        />
                                      </ListGroupItem>
                                    )
                                  )}
                                </ListGroup>
                              </Collapse>
                            </div>
                            <Link to="/ImportadoresVip">
                              <div className="catalogoTextList text-white arial mb-2 p-3 bg-blue-2">
                                <span>Quiero Importar</span>
                                <i
                                  style={{
                                    backgroundColor: "white",
                                    borderRadius: "50%",
                                    fontSize: "1.2rem",
                                    padding: ".2rem",
                                    fontWeight: "bold",
                                  }}
                                  className="mr-2 ml-2 float-right color-orange simple-icon-info"
                                />
                              </div>{" "}
                            </Link>
                            <Link to="/FAQ">
                              <div className="catalogoTextList text-white arial  p-3 bg-blue-2">
                                <span>Preguntas Frecuentes</span>
                                <i
                                  style={{
                                    backgroundColor: "white",
                                    borderRadius: "50%",
                                    fontSize: "1.2rem",
                                    padding: ".2rem",
                                    fontWeight: "bold",
                                  }}
                                  className="mr-2 ml-2 float-right color-orange simple-icon-question"
                                />
                              </div>
                            </Link>
                          </div>
                        ) : (
                          <Row>
                            <Col
                              xs="12"
                              md="12"
                              className={`text-center arial tab1 ${classnames({
                                active2: this.state.activeTab === "2",
                              })}`}
                              onClick={() => {
                                this.tab("2");
                              }}
                            >
                              Especificaciones
                            </Col>
                            <TabContent
                              className="mx-auto"
                              activeTab={this.state.activeTab}
                            >
                              <TabPane tabId="2">
                                <Row className="mx-auto pt-2">
                                  <Col className="p-0" xs="12" md="12">
                                    <div
                                      className="p-1"
                                      style={{
                                        alignItems: "center",
                                      }}
                                    >
                                      <div
                                        style={{
                                          padding: "1rem 0 1rem 0",
                                          width: "100%",
                                          textAlign: "center",
                                        }}
                                      >
                                        <Table>
                                          <tbody>
                                            {this.state.toggleProductDetailsdata[0].Caracteristicas.map(
                                              (item) => (
                                                <tr
                                                  className="p-0 text-center"
                                                  key={
                                                    item.key +
                                                    "Especificaciones2"
                                                  }
                                                >
                                                  <th
                                                    scope="row"
                                                    style={{
                                                      fontWeight: "bold",
                                                      backgroundColor:
                                                        "#80808038",
                                                      width: "7rem",
                                                    }}
                                                  >
                                                    {item.nombre}
                                                  </th>
                                                  <td>{item.descripción}</td>
                                                </tr>
                                              )
                                            )}
                                          </tbody>
                                        </Table>
                                      </div>
                                    </div>
                                  </Col>
                                </Row>{" "}
                              </TabPane>
                            </TabContent>
                          </Row>
                        )}
                      </Col>
                    ) : (
                      <Col xs="12" md="6">
                        <Row>
                          <Col
                            xs="6"
                            md="6"
                            className={`text-center arial tab1 ${classnames({
                              active2: this.state.activeTab === "2",
                            })}`}
                            onClick={() => {
                              this.tab("2");
                            }}
                          >
                            Importadores asociados
                          </Col>
                          <Col
                            xs="6"
                            md="6"
                            className={`text-center arial tab1 ${classnames({
                              active2: this.state.activeTab === "1",
                            })}`}
                            onClick={() => {
                              this.tab("1");
                            }}
                          >
                            Especificaciones
                          </Col>
                        </Row>
                        <TabContent
                          className="mx-auto "
                          activeTab={this.state.activeTab}
                        >
                          <TabPane tabId="1">
                            <Row className="mx-auto pt-2">
                              <Col className="p-0" xs="12" md="12">
                                <div
                                  className="p-1"
                                  style={{
                                    alignItems: "center",
                                  }}
                                >
                                  {" "}
                                  <div
                                    style={{
                                      padding: "1rem 0 1rem 0",
                                      width: "100%",
                                      textAlign: "center",
                                    }}
                                  >
                                    <div>
                                      <Table>
                                        <tbody>
                                          {this.state.toggleProductDetailsdata[0].Caracteristicas.map(
                                            (item) => (
                                              <tr
                                                className="p-0 text-center"
                                                key={
                                                  item.key + "Especificaciones1"
                                                }
                                              >
                                                <th
                                                  scope="row"
                                                  style={{
                                                    fontWeight: "bold",
                                                    backgroundColor:
                                                      "#80808038",
                                                  }}
                                                >
                                                  {item.nombre}
                                                </th>
                                                <td>{item.descripción}</td>
                                              </tr>
                                            )
                                          )}
                                        </tbody>
                                      </Table>
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            </Row>{" "}
                          </TabPane>
                          <TabPane tabId="2">
                            <Row className="mx-auto pt-2">
                              <Col
                                className="p-0"
                                xs="12"
                                md="12"
                                className="NoPadding text-center"
                              >
                                <Table>
                                  <thead>
                                    <tr>
                                      <th>Contacto</th>
                                      <th>País</th>
                                      <th>Provincia</th>
                                      <th>Telefono</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.state.ClienteStock[0].map((item) => (
                                      <tr
                                        className="p-0 text-center"
                                        key={item.key + "clientes"}
                                      >
                                        <th
                                          scope="row"
                                          style={{
                                            fontWeight: "bold",
                                            backgroundColor: "#80808038",
                                          }}
                                        >
                                          {item.contacto}
                                        </th>
                                        <td>{item.Pais}</td>
                                        <td>{item.Provincia}</td>
                                        <td>{item.telefono}</td>
                                      </tr>
                                    ))}
                                  </tbody>
                                </Table>
                              </Col>
                            </Row>
                          </TabPane>
                        </TabContent>
                      </Col>
                    )}
                    <Col
                      xs="12"
                      md="12"
                      sm="12"
                      className="p-3"
                      style={{ textAlign: "center" }}
                    >
                      <Link to="/Importadores">
                        <Button color="success" className="btn-style" size="sm">
                          Volver a Sección de Importadores
                        </Button>
                      </Link>
                    </Col>
                  </Row>
                </Col>
              </Row>
            ) : (
              <Row>
                {this.state.selectImportador ? (
                  <Col
                    xs="12"
                    md="12"
                    sm="12"
                    className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
                  >
                    <Row className="mx-auto">
                      {this.state.isShowCatalogos ? (
                        <Col xs="12" md="6">
                          <div
                            style={{
                              height: "2rem",
                              borderRadius: "0.4rem",
                            }}
                            className="text-white arial mb-2 p-2 bg-blue-2"
                          >
                            <span>Catalogos Disponibles</span>
                          </div>

                          <div
                            className="p-1"
                            style={{
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <div
                              style={{
                                padding: "1rem 0 1rem 0",
                                width: "100%",
                                textAlign: "center",
                              }}
                            >
                              <ListGroup>
                                {this.state.toggleProductDetailsdata[0].catalogos.map(
                                  (item, index) => (
                                    <ListGroupItem
                                      style={{
                                        textTransform: "uppercase",
                                        borderBottom: "1px solid #8f8f8f",
                                        fontSize: "1rem",
                                      }}
                                      className="arial"
                                    >
                                      {" "}
                                      {item.titulo}
                                      <i
                                        onClick={() => {
                                          this.setState({
                                            catalogoSelected: item.img,
                                            isShowCatalogos: false,
                                          });
                                        }}
                                        style={{
                                          fontSize: "1.2rem",
                                          fontWeight: "bold",
                                          backgroundColor: "black",
                                          color: "white",
                                          borderRadius: "50%",
                                          padding: ".1rem .4rem",
                                          cursor: "pointer",
                                        }}
                                        className="ml-4 simple-icon-eye"
                                      />
                                    </ListGroupItem>
                                  )
                                )}
                              </ListGroup>
                            </div>
                          </div>
                        </Col>
                      ) : (
                        <Col xs="12" md="6">
                          <div className="mb-2 pb-2"></div>
                          <div className="mb-2 pb-2">
                            <Button
                              color="success"
                              className="btn-style-blue mr-2"
                              size="sm"
                              onClick={() => {
                                this.toggleCatalogos("");
                              }}
                            >
                              {" "}
                              VER OTRO CATALOGO{" "}
                              <i className="simple-icon-list ml-2" />
                            </Button>
                          </div>

                          {this.state.catalogoSelected && (
                            <iframe
                              style={{
                                height: isMovil() ? "30rem" : "50rem",
                                width: "100%",
                              }}
                              src={this.state.catalogoSelected}
                            />
                          )}
                        </Col>
                      )}
                      <Col className="NoPadding" xs="12" md="6">
                        <Pedido catalogo={this.state.catalogoSelected} />
                      </Col>
                    </Row>
                  </Col>
                ) : (
                  <Col
                    xs="12"
                    md="10"
                    sm="12"
                    className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
                  >
                    <Row className="mx-auto">
                      {this.state.isShowCatalogos ? (
                        <Col
                          xs="12"
                          md={this.state.selectImportador ? "6" : "12"}
                        >
                          <div
                            style={{
                              height: "2rem",
                              borderRadius: "0.4rem",
                            }}
                            className="text-white arial mb-2 p-2 bg-blue-2"
                          >
                            <span>Catalogos Disponibles</span>
                          </div>

                          <div
                            className="p-1"
                            style={{
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <div
                              style={{
                                padding: "1rem 0 1rem 0",
                                width: "100%",
                                textAlign: "center",
                              }}
                            >
                              <ListGroup>
                                {this.state.toggleProductDetailsdata[0].catalogos.map(
                                  (item, index) => (
                                    <ListGroupItem
                                      style={{
                                        textTransform: "uppercase",
                                        borderBottom: "1px solid #8f8f8f",
                                        fontSize: "1rem",
                                      }}
                                      className="arial"
                                    >
                                      {" "}
                                      {item.titulo}
                                      <i
                                        onClick={() => {
                                          this.setState({
                                            catalogoSelected: item.img,
                                            isShowCatalogos: false,
                                          });
                                        }}
                                        style={{
                                          fontSize: "1.2rem",
                                          fontWeight: "bold",
                                          backgroundColor: "black",
                                          color: "white",
                                          borderRadius: "50%",
                                          padding: ".1rem .4rem",
                                          cursor: "pointer",
                                        }}
                                        className="ml-4 simple-icon-eye"
                                      />
                                    </ListGroupItem>
                                  )
                                )}
                              </ListGroup>
                            </div>
                          </div>
                        </Col>
                      ) : (
                        <Col
                          xs="12"
                          md={this.state.selectImportador ? "6" : "12"}
                        >
                          {this.state.catalogoSelected && (
                            <iframe
                              style={{
                                height: isMovil() ? "30rem" : "50rem",
                                width: "100%",
                              }}
                              src={this.state.catalogoSelected}
                            />
                          )}
                          {!this.state.selectImportador && (
                            <div
                              style={{
                                marginLeft: "15%",
                                marginRight: "15%",
                                display: "inline-flex",
                                height: "3rem",
                                fontSize: "1.3rem",
                                cursor: "pointer",
                              }}
                              className="arial text-center fixed-bottom mb-2 "
                            >
                              <div
                                className="p-2"
                                style={{
                                  width: "100%",
                                  backgroundColor: "#FFCB06",
                                  color: "white",
                                  fontWeight: "bold",
                                }}
                                onClick={() => {
                                  this.toggleCatalogos();
                                }}
                              >
                                <span>
                                  {" "}
                                  <i className=" simple-icon-arrow-left-circle  mr-2" />
                                  VOLVER A LOS CATALOGOS
                                </span>
                              </div>
                              <div
                                className="p-2"
                                style={{
                                  width: "100%",
                                  backgroundColor: "#6561AB",
                                  color: "white",
                                  fontWeight: "bold",
                                }}
                                onClick={() => {
                                  this.toggleModal();
                                }}
                              >
                                <span>
                                  <i className=" simple-icon-list  mr-2" />{" "}
                                  REALIZAR PRE ORDEN EN LINEA
                                </span>
                              </div>
                            </div>
                          )}{" "}
                        </Col>
                      )}
                      {this.state.selectImportador && (
                        <Col className="NoPadding" xs="12" md="6">
                          <Pedido catalogo={this.state.catalogoSelected} />
                        </Col>
                      )}
                    </Row>
                  </Col>
                )}{" "}
              </Row>
            )}
          </div>
        ) : (
          <Row>
            <Col xs="12" md="12" sm="12" className=" mx-auto text-center m-3">
              Cargando... <span className="loadingBlock"></span>{" "}
            </Col>
          </Row>
        )}
        <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
          <ModalHeader
            style={{ alignSelf: "center" }}
            className="font"
            toggle={this.toggleModal}
          >
            Tipo De Importador
          </ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col xs="8" md="8" sm="8" className="mx-auto pb-1">
                  <FormGroup>
                    <Label
                      className="arial mx-auto pb-1 text-center"
                      for="tipoImportador"
                    >
                      Seleccione que tipo de importador es:
                    </Label>
                    <div>
                      {this.state.tipoImportador.map((item, index) => (
                        <CustomInput
                          type="radio"
                          id={"tipoImportador" + item.id}
                          name="tipoImportador"
                          label={item.name}
                          value={item.id}
                          onChange={() => {
                            this.setState({
                              selectImportador: true,
                              modal: false,
                            });
                          }}
                          inline
                        />
                      ))}
                    </div>
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </ModalBody>
          <ModalFooter style={{ alignSelf: "center" }}>
            <Button
              className="btn-style"
              color="secondary"
              onClick={this.toggleModal}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </Container>
    );
  }
}

export default withRouter(FAQ);

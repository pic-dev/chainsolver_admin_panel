import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
} from "@fortawesome/free-solid-svg-icons";
import { NavbarToggler, NavItem, NavLink, Nav } from "reactstrap";
import classNames from "classnames";
import { Link } from "react-router-dom";
import "./App.css";
import SubMenu from "./SubMenu";

const SideBar = ({ isOpen, toggle }) => (
  <div className={classNames("sidebar", { "is-open": isOpen })}>
    <div className="sidebar-header">
      <NavbarToggler
        className="float-right display-lg"
        style={{ backgroundColor: "#1370b9", border: "solid white 1px" }}
        onClick={toggle}
      />
      <span color="info" onClick={toggle} style={{ color: "#fff" }}>
        &times;
      </span>
      <h3>IMPORTADORES GRUPALES</h3>
    </div>
    <section className="side-menu">
      <Nav vertical className="list-unstyled pb-3">
        <p>Panel de control</p>
        <NavItem>
          <NavLink tag={Link} to={"/ImpoAdmin"}>
            <FontAwesomeIcon icon={faImage} className="mr-2" />
            Agregar Productos
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={"/Clientes"}>
            <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
            Agregar Cliente
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={"/Productos"}>
            <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
            Gestionar stock
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={"/Importadores"}>
            <FontAwesomeIcon icon={faQuestion} className="mr-2" />
            Listado de importadores
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={"/"}>
            <FontAwesomeIcon icon={faPaperPlane} className="mr-2" />
            Volver a calculadora
          </NavLink>
        </NavItem>
      </Nav>
    </section>
  </div>
);

const submenus = [
  [
    {
      title: "Nuevos Productos",
      target: "/ImpoAdmin",
    },
    {
      title: "Agregar Productos",
      target: "/ImpoAdmin",
    },
    {
      itle: "Home 3",
      target: "Home-3",
    },
  ],
  [
    {
      title: "Page 1",
      target: "Page-1",
    },
    {
      title: "Page 2",
      target: "Page-2",
    },
  ],
];

export default SideBar;

import React, { Component, Fragment } from "react";
import classnames from "classnames";
import {
  Alert,
  Col,
  Row,
  Form,
  Label,
  Input,
  Button,
  FormGroup,
  InputGroup,
  CustomInput,
  Nav,
  NavLink,
  NavItem,
  TabContent,
  TabPane,
  FormText,
  Modal,
  ModalBody,
  ModalFooter,
  Dropdown,
  DropdownToggle,
  UncontrolledTooltip,
  Card,
  CardHeader,
  CardImg,
  CardText,
  CardBody,
  CardFooter,
  Table,
} from "reactstrap";
import { Link, Redirect, withRouter } from "react-router-dom";
import { Colxx } from "Components/CustomBootstrap";
import {
  Typeahead,
  AsyncTypeahead,
} from "react-bootstrap-typeahead";
import axios from "axios";
import TimeLine from "Util/timeLine";
import { errores } from "Util/Utils";
import scrollIntoView from "scroll-into-view-if-needed";
import { PageView, Event } from "Util/tracking";
import { Beforeunload } from "react-beforeunload";
import MiniSearch from "minisearch";
import { Title } from "Util/helmet";
import BotChat from "Util/chat";

const Calculadora = React.lazy(() => import("Util/calculadoras"));
const ModalSignInRuta = React.lazy(() =>
  import("Util/Modals/modalSignInRuta")
);
const renderLoader = () => <div className="loading"></div>;

let timer;
var validated;
var valid;

class ruta extends Component {
  constructor(props) {
    super(props);
    this.toggleModalPreview = this.toggleModalPreview.bind(this);
    this.toggleCalculator = this.toggleCalculator.bind(this);
    this.toggle2 = this.toggle2.bind(this);
    this.toggle = this.toggle.bind(this);
    this.minusCount = this.minusCount.bind(this);
    this.plusCount = this.plusCount.bind(this);
    this.tabs = this.tabs.bind(this);
    this.handleFormState = this.handleFormState.bind(this);
    this.portOrigin = this.portOrigin.bind(this);
    this.portDestiny = this.portDestiny.bind(this);
    this.get_ctz_fcl = this.get_ctz_fcl.bind(this);
    this.get_ctz_lcl = this.get_ctz_lcl.bind(this);
    this.get_ctz_aereo = this.get_ctz_aereo.bind(this);
    this.get_ctz = this.get_ctz.bind(this);
    this.showResult = this.showResult.bind(this);
    this.changePlaceholder = this.changePlaceholder.bind(this);
    this.array_js_to_pg = this.array_js_to_pg.bind(this);
    this.contenedores = this.contenedores.bind(this);
    this.validation = this.validation.bind(this);
    this.setValues = this.setValues.bind(this);
    this.setvolume = this.setvolume.bind(this);
    this.scrollTo = this.scrollTo.bind(this);
    this.handleBoxToggle = this.handleBoxToggle.bind(this);
    this.Destiny = this.Destiny.bind(this);
    this.checkOrigen = this.checkOrigen.bind(this);
    this.EventOrigen = this.EventOrigen.bind(this);
    this.EventDestino = this.EventDestino.bind(this);
    this.redirect = this.redirect.bind(this);
    this.ResetCont = this.ResetCont.bind(this);
    this.toggleAlertOpcion = this.toggleAlertOpcion.bind(this);
    this.fuzzy = this.fuzzy.bind(this);

    this.state = {
      fuzzyOptions: [],
      msjActivo: false,
      isOpenAlertOpcion: false,
      OpcionValue: "",
      banderatipoLCL: false,
      count1: JSON.parse(localStorage.getItem("count1")) + 0,
      count2: JSON.parse(localStorage.getItem("count2")) + 0,
      count3: JSON.parse(localStorage.getItem("count3")) + 0,
      count4: JSON.parse(localStorage.getItem("count4")) + 0,
      setValuesMsj: false,
      placeholder: "Elija una opcion",
      placeholder2: "Cargue Contenido a Enviar",
      Calculator: false,
      dropdownOpen: false,
      dropdownOpen2: false,
      showLoading: false,
      toggleAlert: false,
      resultContainer: false,
      hide: false,
      hide2: false,
      activeTab: "false",
      portOriginSelected: [],
      portDestinySelected: [],
      portOrigin: [],
      portDestiny: [],
      productPerm: [],
      result: [],
      showResult: [],
      validated1: false,
      validated2: false,
      validated3: false,
      showBox1: false,
      showBox2: false,
      showBox3: false,
      showBox4: false,
      showBoxmb1: false,
      showBoxmb2: false,
      showBoxmb3: false,
      showBoxmb4: false,
      showCalmsj: false,
      IsVisiblePreview: false,
      isLoading: false,
      form: [
        { Label: "lumps", value: "1" },
        { Label: "weight", value: "1" },
        { Label: "volume", value: "" },
        { Label: "weightUnit", value: "2" },
        { Label: "volumeUnit", value: "1" },
        { Label: "weightVolume", value: "" },
      ],
    };
  }
  componentDidMount() {
    localStorage.removeItem("token");
    if (process.env.NODE_ENV == "production") {
      PageView();
    }

    valid = "2";
    localStorage.page = "2";

    if (localStorage.transporte == "AÉREO") {
      let url1 = this.props.api + "aereo_v2/get_find_port_aereo_begin";
      let datos1 = {
        word: "",
      };
      localStorage.tipocon = "Aereo";
      /*Event("3.1 Tipo Envio Aereo", "3.1 Tipo Envio Aereo", "RUTA"); */
      this.apiPost(url1, datos1, this.portOrigin);
    }
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  redirect() {
    this.props.history.push("/Presupuesto");
  }
  handleBoxToggle(e, value) {
    switch (value) {
      case 0:
        this.setState({ showBox1: e });
        break;
      case 1:
        this.setState({ showBox2: e });
        break;
      case 2:
        this.setState({ showBox3: e });
        break;
      case 3:
        this.setState({ showBox4: e });
        break;
      default:
      // code block
    }
  }
  setvolume(value) {
    switch (value) {
      case 0:
        if (
          this.state.form[0].value < 1 ||
          this.state.form[1].value < 1 ||
          this.state.form[2].value < 0.3
        ) {
          this.setState({
            setValuesMsj: true,
          });
        } else {
          this.setState({
            placeholder2: "Contenido a Enviar",
            hide: false,
            hide2: false,
            setValuesMsj: false,
          });
        }
        break;
      case 1:
        this.setState({
          setValuesMsj: false,
        });
        break;
      case 2:
        let newState = this.state.form;

        if (this.state.form[0].value == 0) {
          newState[0].value = 1;
        }

        if (this.state.form[1].value == 0) {
          newState[1].value = 1;
        }

        if (
          this.state.form[2].value == 0 &&
          localStorage.transporte == "MARITÍMO"
        ) {
          newState[2].value = 1;
        } else if (
          this.state.form[2].value == 0 &&
          localStorage.transporte == "AÉREO"
        ) {
          newState[2].value = 0.6;
          newState[5].value = 1;
        } else {
        }

        this.setState({
          placeholder2: "Contenido a Enviar",
          form: newState,
          hide: false,
          hide2: false,
          setValuesMsj: false,
        });
        break;
      default:
      // code block
    }
  }
  apiPost(url, datos, fn) {
    axios
      .post(url, datos)
      .then((res) => {
        const response = res.data.data.r_dat;
        //this.setState({ response });

        fn(response);
      })
      .catch((error) => {
        errores("ApipostServicios", error);
      });
  }
  fuzzy(query) {
    this.setState({ isLoading: true });
    let miniSearch = new MiniSearch({
      fields: ["puerto"], // fields to index for full-text search
      storeFields: ["puerto"], // fields to return with search results
      searchOptions: {
        fuzzy: 0.2,
        prefix: true,
      },
    });
    miniSearch.addAll(this.state.portOrigin);
    let results = miniSearch.search(query);

    this.setState({
      fuzzyOptions: results,
      isLoading: false,
      tokenize: (puerto) => puerto.split("-"), // indexing tokenizer
      searchOptions: {
        tokenize: (puerto) => puerto.split(/[\s-]+/), // search query tokenizer
      },
    });
  }
  setValues() {
    if (localStorage.volumenTotal > 0) {
      let PesovolumenTotal = JSON.parse(
        localStorage.getItem("PesovolumenTotal")
      );
      if (PesovolumenTotal > 0 && PesovolumenTotal < 1) {
        PesovolumenTotal.toFixed(2);
      }
      let Form = [
        {
          Label: "lumps",
          value: JSON.parse(localStorage.getItem("bultosTotal")),
        },
        {
          Label: "weight",
          value: JSON.parse(localStorage.getItem("pesoTotal")),
        },
        {
          Label: "volume",
          value: JSON.parse(localStorage.getItem("volumenTotal")),
        },
        { Label: "weightUnit", value: "2" },

        { Label: "volumeUnit", value: "1" },

        {
          Label: "weightVolume",
          value: PesovolumenTotal,
        },
      ];
      this.setState({ form: Form });
      this.setState((ps) => ({ Calculator: !ps.Calculator }));
    } else {
      this.setState({ toggleAlert: true });

      timer = setTimeout(() => {
        this.setState({ toggleAlert: false });
      }, 5000);
    }
  }
  handleFormState(input) {
    switch (input.id) {
      case 0:
        if (input.value < 0) {
        } else {
          localStorage.setItem(
            "lumps",
            JSON.stringify(this.state.form[0].value)
          );
        }
        break;
      case 1:
        if (input.value < 0) {
        } else {
          localStorage.setItem(
            "weight",
            JSON.stringify(this.state.form[1].value)
          );
        }
        break;
      case 2:
        if (input.value < 0) {
        } else {
          localStorage.setItem(
            "volume",
            JSON.stringify(this.state.form[2].value)
          );
          if (input.value > 15) {
            this.setState({
              msjActivo: true,
            });
          }
        }
        break;
      case 3:
        localStorage.setItem(
          "weightUnit",
          JSON.stringify(this.state.form[3].value)
        );
        break;
      default:
      // code block
    }
    if (input.value < 0) {
      let newState = this.state.form;
      newState[input.id].value = 0;

      this.setState({
        form: newState,
      });
    } else {
      Number(input.value);
      let newState = this.state.form;
      newState[input.id].value = input.value;

      this.setState({
        form: newState,
      });
    }
  }
  validation() {
    if (valid === "1") {
      if (
        ((this.state.activeTab =
          "1" &&
          this.state.count1 === 0 &&
          this.state.count2 === 0 &&
          this.state.count3 === 0 &&
          this.state.count4 === 0) &&
          this.state.portOriginSelected.length === 0 &&
          this.state.portDestinySelected.length === 0) ||
        ((this.state.activeTab =
          "2" &&
          this.state.form[0].length === 0 &&
          this.state.form[1].length === 0 &&
          this.state.form[2].length === 0) &&
          this.state.portOriginSelected.length === 0 &&
          this.state.portDestinySelected.length === 0)
      ) {
        this.setState({
          validated1: true,
          validated2: true,
          validated3: true,
        });
      } else if (
        (this.state.activeTab =
          "1" &&
          this.state.count1 === 0 &&
          this.state.count2 === 0 &&
          this.state.count3 === 0 &&
          this.state.count4 === 0) ||
        (this.state.activeTab =
          "2" &&
          this.state.form[0].length === 0 &&
          this.state.form[1].length === 0 &&
          this.state.form[2].length === 0)
      ) {
        this.setState({
          validated1: true,
          validated2: false,
          validated3: false,
        });
      } else if (this.state.portOriginSelected.length === 0) {
        this.setState({
          validated2: true,
          validated1: false,
          validated3: false,
        });
      } else if (this.state.portDestinySelected.length === 0) {
        this.setState({
          validated3: true,
          validated2: false,
          validated1: false,
        });
      } else {
        this.setState({
          validated1: false,
          validated2: false,
          validated3: false,
        });
      }
    } else {
    }
  }
  portOrigin(fn) {
  
   this.setState({ portOrigin: fn });
  
  }


  changePlaceholder(e) {
    switch (e) {
      case 1:
        this.setState({ placeholder: "Contenedor completo" });
        localStorage.tipocon = "Contenedor completo";
        if (localStorage.transporte == "MARITÍMO") {
          let url = this.props.api + "maritimo_v2/get_find_port_mar_begin";

          let datos = {
            word: "",
            port: "MARITIMO FCL",
          };
          this.apiPost(url, datos, this.portOrigin);
          /*Event("2.1 Tipo Envio FCL", "2.1 Tipo Envio FCL", "RUTA");*/
        } else {
          localStorage.tipocon = "Aereo";
        }

        break;
      case 2:
        this.setState({ placeholder: "Contenedor compartido" });

        if (localStorage.transporte == "MARITÍMO") {
          localStorage.tipocon = "Contenedor compartido";
          let url1 = this.props.api + "maritimo_v2/get_find_port_mar_begin";

          let datos1 = {
            word: "",
            port: "MARITIMO LCL",
          };
          this.apiPost(url1, datos1, this.portOrigin);

          if (!this.state.banderatipoLCL) {
            /*Event("1.1 Tipo Envio LCL", "1.1 Tipo Envio LCL", "RUTA");*/
            this.setState({ banderatipoLCL: true });
          }
        } else {
          localStorage.tipocon = "Aereo";
        }
        break;

      default:
      // code block
    }
  }
  portDestiny(fn) {
    this.setState({
      portDestiny: fn,
    });
  }
  ResetCont(e) {
    var x = e.map(function (item) {
      return item.detalles;
    });

    var count1;
    var count2;
    var count3;
    var count4;

    if (x.includes("20' Standard")) {
      this.setState({ count1: this.state.count1 * 0 });
      count1 = this.state.count1 * 0;
    } else {
      count1 = this.state.count1;
    }
    if (x.includes("40' Standard")) {
      this.setState({ count2: this.state.count2 * 0 });
      count2 = this.state.count2 * 0;
    } else {
      count2 = this.state.count2;
    }
    if (x.includes("40' High Cube")) {
      this.setState({ count3: this.state.count3 * 0 });
      count3 = this.state.count3 * 0;
    } else {
      count3 = this.state.count3;
    }
    if (x.includes("40' NOR")) {
      this.setState({ count4: this.state.count4 * 0 });
      count4 = this.state.count4;
    } else {
      count4 = this.state.count4;
    }
    let retorno = " ";

    if (count1 > 0) {
      retorno += count1;
      retorno += "*20' Std, ";
    }
    if (count2 > 0) {
      retorno += count2;
      retorno += "*40' Std, ";
    }
    if (count3 > 0) {
      retorno += count3;
      retorno += "*40' HIGH CUBE, ";
    }
    if (count4 > 0) {
      retorno += count4;
      retorno += "*40' NOR";
    }
    var conte = [
      [1, count1],
      [2, count2],
      [3, count3],
      [4, count4],
    ];
    var value8 = conte.filter(function (valor) {
      if (!valor.includes(0)) {
        return valor;
      }
    });
    localStorage.contenedores = this.array_js_to_pg(value8);
    localStorage.contenedoresDes = retorno;
  }
  showResult(e) {
    const formatter = new Intl.NumberFormat("de-DE");

    this.setState({
      showResult: [],
    });

    let resultado;
    let resultvalue;
    let resultNULL;
    let resulValuetNULL;
    var result;
    var filtro;

    if (
      this.state.placeholder === "Contenedor completo" &&
      localStorage.transporte === "MARITÍMO"
    ) {
      filtro = e.filter(function (item) {
        return (
          item.tipo == "FLETE MARÍTIMO" &&
          item.cantidad > 0 &&
          item.valor !== null
        );
      });

      resultNULL = e.filter(function (item) {
        if (item.valor == null) {
          return item;
        }
      });

      if (resultNULL.length > 0) {
        resulValuetNULL = true;
      } else {
        resulValuetNULL = false;
      }

      if (filtro.length > 0) {
        resultvalue = true;
      } else {
        resultvalue = false;
      }
      if (resultvalue && !resulValuetNULL) {
      } else if (resulValuetNULL) {
        this.ResetCont(resultNULL);
        resultado = (
          <div className="alert alert-danger">
            {" "}
            Actualmente NO HAY TARIFAS para{" "}
            {resultNULL.map((item) => (
              <span>
                {item.detalles}
                {" , "}
              </span>
            ))}{" "}
            Con el destino seleccionado
          </div>
        );
      } else {
        resultado = (
          <div className="alert alert-danger">
            {" "}
            Actualmente NO HAY TARIFAS para el destino seleccionado.
          </div>
        );
      }
    } else {
      resultvalue = e.map(function (item) {
        if (item > 0 || item !== null) {
          return true;
        } else {
          return false;
        }
      });

      if (resultvalue[0]) {
      } else if (
        this.state.placeholder === "Contenedor compartido" &&
        localStorage.transporte == "MARITÍMO"
      ) {
        if (this.state.form[2].value > 16) {
          resultado = (
            <div className="alert alert-danger">
              <span className="color-blue">El</span> VOLUMEN
              <span className="color-blue">registrado no debe</span> EXCEDER DE
              16M3, <span className="color-blue">seleccione</span>
              Contenedor Completo o Contacte a un ASESOR +51-972530303.
            </div>
          );
        } else if (this.state.form[1].value > 15000) {
          resultado = (
            <div className="alert alert-danger">
              <span className="color-blue">El</span> PESO{" "}
              <span className="color-blue">registrado no debe</span> EXCEDER DE
              20.000,00 kgs,{" "}
              <span className="color-blue">modifique o seleccione</span>{" "}
              Contenedor Completo.
            </div>
          );
        } else {
          resultado = (
            <div className="alert alert-danger">
              <span className="color-blue">
                No hay tarifa con estos datos, INTENTE CON OTRA RUTA.
              </span>{" "}
            </div>
          );
        }
      } else if (localStorage.transporte === "AÉREO") {
        if (
          this.state.form[5].value > this.state.form[1].value &&
          this.state.form[5].value < 0.6
        ) {
          resultado = (
            <div className="alert alert-danger">
              El{" "}
              <span className="color-blue">
                PESO VOLUMEN minimo a registrar es 0.6 M3
              </span>
              , favor colocar un Valor Superior.
            </div>
          );
        } else if (
          this.state.form[1].value > this.state.form[5].value &&
          this.state.form[1].value < 60
        ) {
          resultado = (
            <div className="alert alert-danger">
              El{" "}
              <span className="color-blue">
                PESO MINIMO a registrar debe ser MAYOR A 60 KG 0 0.6m3
              </span>
              , favor colocar un Valor Superior.
            </div>
          );
        } else if (
          this.state.form[1].value === this.state.form[5].value &&
          (this.state.form[1].value < 60 || this.state.form[5].value < 0.6)
        ) {
          resultado = (
            <div className="alert alert-danger">
              El valor de
              <span className="color-blue"> VOLUMEN O PESO KILOS</span>, debe
              ser
              <span className="color-blue"> SUPERIOR A 60 KG 0 0.6m3 </span>
              favor registrar un Valor Superior.
            </div>
          );
        } else {
          resultado = (
            <div className="alert alert-danger">
              {" "}
              No hay tarifa con estos datos, INTENTE CON OTRA RUTA.
            </div>
          );
        }
      } else {
        resultado = (
          <div className="alert alert-danger">
            {" "}
            Actualmente NO HAY TARIFAS Con el destino seleccionado
          </div>
        );
      }
    }

    this.setState({
      showResult: resultado,
    });
  }
  scrollTo(e) {
    var element;
    switch (e) {
      case 1:
        element = document.getElementById("link5");

        scrollIntoView(element, {
          behavior: "smooth",
          block: "center",
          inline: "center",
        });
        break;
      case 2:
        element = document.getElementById("alert");

        scrollIntoView(element, {
          behavior: "smooth",
          block: "center",
          inline: "center",
        });
        break;
      case 3:
        break;

      default:
      // code block
    }
  }
  get_ctz() {
    valid = "1";

    switch (validated) {
      case "2":
        localStorage.bottomChange = "2";
        this.setState({
          resultContainer: false,
        });

        break;
      case "3":
        this.setState({
          showLoading: true,
        });
        this.get_ctz_fcl();

        break;
      case "4":
        this.setState({
          showLoading: true,
        });
        this.get_ctz_lcl();

        break;
      case "5":
        this.setState({
          showLoading: true,
        });
        this.get_ctz_aereo();

        break;
      default:
      // code block
    }
  }
  get_ctz_fcl() {
    valid = "2";
    var puerto;
    const value = this.state.portOriginSelected.map(function (item) {
      return item.id;
    });

    localStorage.puerto = this.state.portOriginSelected.map(function (item) {
      return item.puerto;
    });

    localStorage.puertoD = puerto = this.state.portDestinySelected.map(
      function (item) {
        return item.puerto;
      }
    );

    const portDestiny = this.state.portDestinySelected.map(function (item) {
      return item.id;
    });
    const value2 = 0;
    const value3 = 0;
    const value4 = false;
    const value5 = false;
    const value6 = false;
    const value7 = false;

    var conte = [
      [1, this.state.count1],
      [2, this.state.count2],
      [3, this.state.count3],
      [4, this.state.count4],
    ];

    var value8 = conte.filter(function (valor) {
      if (!valor.includes(0)) {
        return valor;
      }
    });

    if (puerto[0].includes("PERÚ")) {
      localStorage.paisDestino = "PERU";
    } else if (puerto[0].includes("PANAMÁ")) {
      localStorage.paisDestino = "PANAMA";
    } else {
      localStorage.paisDestino = "VENEZUELA";
    }

    let url = this.props.api + "maritimo_v2/get_ctz_fcl_flete";

    let datos = {
      datos: [
        value.toString(),
        portDestiny.toString(),
        this.array_js_to_pg(value8),
        value2.toString(),
        value3.toString(),
        value4,
        value5,
        value6,
        value7,
      ],
    };

    axios
      .post(url, datos)
      .then((res) => {
        const response = res.data.data.r_dat;
        var filtro = response.filter(function (item) {
          return (
            item.tipo == "FLETE MARÍTIMO" &&
            item.cantidad > 0 &&
            item.valor !== null
          );
        });
        var result;
        if (filtro.length > 0) {
          result = filtro
            .map(function (duration) {
              return duration.valor;
            })
            .reduce(function (accumulator, current) {
              return [+accumulator + +current];
            });
          /*Event(
            "2.4 Agregar Servicios FCL",
            "2.4 Agregar Servicios FCL",
            "RUTA"
          );*/
        } else {
          result = filtro;
        }
        localStorage.bottomChange = "1";
        this.setState({
          result: result,
          showLoading: false,
          resultContainer: true,
          hide: false,
        });
        this.scrollTo(1);
        this.showResult(response);
      })
      .catch((error) => {
        errores("CTZ_FCL_Ruta", error);
      });

    localStorage.origen = value;
    localStorage.destino = portDestiny;
    localStorage.contenedores = this.array_js_to_pg(value8);
    localStorage.contenedoresDes = this.contenedores();
  }
  get_ctz_lcl() {
    valid = "2";
    var puerto;
    var destino;
    const portOrigin = this.state.portOriginSelected.map(function (item) {
      return item.id;
    });

    localStorage.puerto = this.state.portOriginSelected.map(function (item) {
      return item.puerto;
    });

    localStorage.puertoD = puerto = this.state.portDestinySelected.map(
      function (item) {
        return item.puerto;
      }
    );

    const portDestiny = this.state.portDestinySelected.map(function (item) {
      return item.id;
    });

    let volumenValue;
    if (this.state.form[4].value != "1") {
      volumenValue = this.state.form[2].value / 35.315;
    } else {
      volumenValue = this.state.form[2].value;
    }

    if (puerto[0].includes("PERÚ")) {
      localStorage.paisDestino = "PERU";
      destino= "PERU";
    } else if (puerto[0].includes("PANAMÁ")) {
      localStorage.paisDestino = "PANAMA";
      destino = "PANAMA";
    } else {
      localStorage.paisDestino = "VENEZUELA";
      destino = "VENEZUELA";
    }

    const weight = this.state.form[1].value;
    const weightUnit = this.state.form[3].value;
    const volumen = volumenValue;
    const volumentUnit = 5;
    const lumps = this.state.form[0].value;
    const value2 = 0;
    const value3 = 0;
    const value4 = false;
    const value5 = false;
    const value6 = false;
    const value7 = false;

    localStorage.origen = portOrigin;
    localStorage.destino = portDestiny;
    localStorage.contenedores = weightUnit;
    localStorage.weight = weight;
    localStorage.contenedoresDes = this.contenedores();
    localStorage.volumen = volumen;
    localStorage.volumentUnit = volumentUnit;
    localStorage.lumps = lumps;

    let url = this.props.api + "maritimo_v2/get_ctz_lcl_flete";

    let datos = {
      datos: [
        portOrigin.toString(),
        portDestiny.toString(),
        weight.toString(),
        weightUnit.toString(),
        volumen.toString(),
        volumentUnit.toString(),
        lumps.toString(),
        value2.toString(),
        value3.toString(),
        value4,
        value5,
        value6,
        value7,
      ],
    };

    axios
      .post(url, datos)
      .then((res) => {
        const response = res.data.data.r_dat;
        const result = response
          .filter(function (item) {
            return item.tipo == "FLETE MARÍTIMO";
          })
          .map(function (duration) {
            return duration.valor;
          });
        result.map(function (item) {
          if (item > 0) {
            /*Event(
              "1.5 Agregar Servicios LCL",
              "1.5 Agregar Servicios LCL",
              "RUTA"
            );*/
            return item;
          }
        });
        localStorage.bottomChange = "1";
 
        if(result[0] > 0 && destino == "VENEZUELA"){
          this.toggleModalPreview("2");
        }else{

          this.setState({
            result: result,
            showLoading: false,
            resultContainer: true,
            hide: false,
          });
          this.scrollTo(1);
          this.showResult(result);
        }

  
      })
      .catch((error) => {
        errores("CTZ_LCL_Ruta", error);
      });




  }
  get_ctz_aereo() {
    valid = "2";
    var puerto;
    const portOrigin = this.state.portOriginSelected.map(function (item) {
      return item.id;
    });

    localStorage.puerto = this.state.portOriginSelected.map(function (item) {
      return item.puerto;
    });

    localStorage.puertoD = puerto = this.state.portDestinySelected.map(
      function (item) {
        return item.puerto;
      }
    );

    const portDestiny = this.state.portDestinySelected.map(function (item) {
      return item.id;
    });

    if (puerto[0].includes("PERÚ")) {
      localStorage.paisDestino = "PERU";
    } else if (puerto[0].includes("PANAMÁ")) {
      localStorage.paisDestino = "PANAMA";
    } else {
      localStorage.paisDestino = "VENEZUELA";
    }

    let volumenValue;
    if (this.state.form[5].value) {
      volumenValue = this.state.form[5].value;
    } else {
      volumenValue = this.state.form[2].value;
    }

    const weight = this.state.form[1].value.toString();
    const weightUnit = this.state.form[3].value.toString();
    const volumen = volumenValue.toString();
    const volumentUnit = 5;
    const lumps = this.state.form[0].value.toString();
    const value2 = 0;
    const value3 = 0;
    const value4 = false;
    const value5 = false;
    const value6 = false;
    const value7 = false;

    let url = this.props.api + "aereo_v2/get_ctz_aereo_flete";

    let datos = {
      datos: [
        portOrigin.toString(),
        portDestiny.toString(),
        weight.toString(),
        weightUnit.toString(),
        volumen.toString(),
        volumentUnit.toString(),
        lumps.toString(),
        value2.toString(),
        value3.toString(),
        value4,
        value5,
        value6,
        value7,
      ],
    };

    axios
      .post(url, datos)
      .then((res) => {
        const response = res.data.data.r_dat;
        const result = response
          .filter(function (item) {
            return item.tipo == "FLETE AÉREO";
          })
          .map(function (duration) {
            return duration.valor;
          });
        result.map(function (item) {
          if (item > 0) {
            /*Event(
              "3.5 Agregar Servicios Aereo",
              "3.5 Agregar Servicios Aereo",
              "RUTA"
            );*/
            return item;
          }
        });
        localStorage.bottomChange = "1";
        this.setState({
          result: result,
          showLoading: false,
          resultContainer: true,
          hide: false,
        });
        this.scrollTo(1);
        this.showResult(result);
      })
      .catch((error) => {
        errores("CTZ_AEREO_Ruta", error);
      });
    localStorage.origen = portOrigin;
    localStorage.destino = portDestiny;
    localStorage.contenedores = weightUnit;
    localStorage.weight = weight;
    localStorage.contenedoresDes = this.contenedores();
    localStorage.volumen = volumen;
    localStorage.volumentUnit = volumentUnit;
    localStorage.lumps = lumps;
  }
  array_js_to_pg(array) {
    let retorno = "{";
    let x = 1;
    array.forEach((element) => {
      retorno += "{";
      for (let i = 0; i < element.length; i++) {
        retorno += element[i];
        if (i + 1 < element.length) retorno += ",";
      }
      retorno += "}";
      if (x < array.length) retorno += ",";
      x++;
    });
    retorno += "}";
    return retorno;
  }
  tabs(tab) {
    if (this.state.activeTab !== tab) {
      if (tab === "2") {
        this.setState({
          showCalmsj: false,
        });
        if (screen.width < 769) {
          timer = setTimeout(() => {
            this.setState({
              showCalmsj: false,
            });
          }, 6000);
        }
        this.setState({
          activeTab: tab,
        });
      } else {
        this.setState({
          activeTab: tab,
        });
      }
      this.scrollTo(3);
    }
  }
  minusCount(e) {
    switch (e) {
      case 1:
        if (this.state.count1 >= 1) {
          this.setState({ count1: this.state.count1 - 1 });
          localStorage.setItem("count1", JSON.stringify(this.state.count1 - 1));
        }

        break;
      case 2:
        if (this.state.count2 >= 1) {
          this.setState({ count2: this.state.count2 - 1 });
          localStorage.setItem("count2", JSON.stringify(this.state.count2 - 1));
        }
        break;
      case 3:
        if (this.state.count3 >= 1) {
          this.setState({ count3: this.state.count3 - 1 });
          localStorage.setItem("count3", JSON.stringify(this.state.count3 - 1));
        }
        break;
      case 4:
        if (this.state.count4 >= 1) {
          this.setState({ count4: this.state.count4 - 1 });
          localStorage.setItem("count4", JSON.stringify(this.state.count4 - 1));
        }
        break;
      default:
      // code block
    }
  }
  plusCount(e) {
    switch (e) {
      case 1:
        this.setState({ count1: this.state.count1 + 1 });
        localStorage.count1 = this.state.count1 + 1;
        break;
      case 2:
        this.setState({ count2: this.state.count2 + 1 });
        localStorage.count2 = this.state.count2 + 1;
        break;
      case 3:
        this.setState({ count3: this.state.count3 + 1 });
        localStorage.count3 = this.state.count3 + 1;
        break;
      case 4:
        this.setState({ count4: this.state.count4 + 1 });
        localStorage.count4 = this.state.count4 + 1;
        break;
      default:
      // code block
    }
  }
  contenedores() {
    if (this.state.placeholder === "Contenedor completo") {
      let retorno = " ";

      if (this.state.count1 > 0) {
        retorno += this.state.count1;
        retorno += "*20' Std, ";
      }
      if (this.state.count2 > 0) {
        retorno += this.state.count2;
        retorno += "*40' Std, ";
      }
      if (this.state.count3 > 0) {
        retorno += this.state.count3;
        retorno += "*40' HIGH CUBE, ";
      }
      if (this.state.count4 > 0) {
        retorno += this.state.count4;
        retorno += "*40' NOR";
      }
      return retorno;
    } else {
      let retorno = " ";
      retorno += this.state.form[0].value;
      if (this.state.form[0].value == 1) {
        retorno += " Bulto de ";
      } else {
        retorno += " Bultos de ";
      }
      retorno += this.state.form[1].value;
      retorno += " ";
      retorno += localStorage.peso;
      retorno += " y ";
      retorno += this.state.form[2].value;
      retorno += " M³";

      return retorno;
    }
  }
  toggleCalculator() {
    this.setState((ps) => ({ Calculator: !ps.Calculator }));
  }
  toggle2() {
    this.setState((ps) => ({ hide2: !ps.hide2, resultContainer: false }));
    if (this.state.hide2 == true) {
      this.scrollTo(3);
    } else {
      this.scrollTo(4);
    }
  }
  toggle() {
    this.setState((ps) => ({
      hide: !ps.hide,
      resultContainer: false,
      showResult: [],
    }));
    if (this.state.hide == true) {
      this.scrollTo(3);
    } else {
      this.scrollTo(4);
    }
  }
  EventDestino = (tipo, seleccionado) => {
    this.setState({
      resultContainer: false,
      showResult: [],
    });
    let value = seleccionado.filter(function (item) {
      if (item.puerto == "PERÚ - OTRAS PROVINCIAS") {
        return item.id;
      }
    });

    switch (tipo) {
      case 1:
        //Event("3.4 Puerto Destino Aereo", "3.4 Puerto Destino Aereo", "RUTA");
        break;
      case 2:
        if (localStorage.tipocon == "Contenedor compartido") {
          //Event("1.4 Puerto Destino LCL", "1.4 Puerto Destino LCL", "RUTA");
        } else {
          //Event("2.3 Puerto Destino FCL", "2.3 Puerto Destino FCL", "RUTA");
        }
        break;
      default:
      // code block
    }
  };
  EventOrigen = (e) => {
    this.setState({
      resultContainer: false,
      showResult: [],
    });
    switch (e) {
      case 1:
        //Event("3.3 Puerto Origen Aereo", "3.3 Puerto Origen Aereo", "RUTA");
        break;
      case 2:
        if (localStorage.tipocon == "Contenedor compartido") {
          //Event("1.3 Puerto Origen LCL", "1.3 Puerto Origen LCL", "RUTA");
        } else {
          //Event("2.2 Puerto Origen FCL", "2.2 Puerto Origen FCL", "RUTA");
        }
        break;
      default:
      // code block
    }
  };
  EventGuardar = (e) => {
    switch (e) {
      case 1:
        /*Event(
          "3.3.1 guardar elección de mercancia aereo",
          "3.3.1 guardar elección de mercancia aereo",
          "RUTA"
        );*/
        break;
      case 2:
        /*Event(
          "3.3.1 guardar elección de mercancia LCL",
          "3.3.1 guardar elección de mercancia LCL",
          "RUTA"
        );*/
        break;
      default:
      // code block
    }
  };
  eventOpcion1 = () => {
    switch (localStorage.tipocon) {
      case "Contenedor completo":
        /*Event(
          "2.7.2 Calcular Opcion 1 FCL",
          "2.7.2 Calcular Opcion 1 FCL",
          "RUTA"
        );*/
        break;
      case "Contenedor compartido":
        /*Event(
          "1.8.2 Calcular Opcion 1 LCL",
          "1.8.2 Calcular Opcion 1 LCL",
          "RUTA"
        );*/
        break;
      case "Aereo":
        /*Event(
          "3.8.2 Calcular Opcion 1 AEREO",
          "3.8.2 Calcular Opcion 1 AEREO",
          "RUTA"
        );*/
        break;

      default:
    }
  };
  eventAgregarToque = () => {
    switch (localStorage.tipocon) {
      case "Contenedor completo":
        /*Event(
          "2.4.1 Agregar Servicios FCL toque",
          "2.4.1 Agregar Servicios FCL toque",
          "RUTA"
        );*/
        break;
      case "Contenedor compartido":
        /*Event(
          "1.5.1 Agregar Servicios LCL toque",
          "1.5.1 Agregar Servicios LCL toque",
          "RUTA"
        );*/
        break;
      case "Aereo":
        /*Event(
          "3.5.1 Agregar Servicios Aereo toque",
          "3.5.1 Agregar Servicios Aereo toque",
          "RUTA"
        );*/
        break;

      default:
    }
  };
  Destiny = (origen) => {
    const origen1 = origen.map(function (item) {
      return item.id;
    });
    let puertos = [
      257,
      376,
      347,
      372,
      389,
      350,
      344,
      300,
      390,
      301,
      302,
      355,
      343,
      391,
      350,
      536,
      537,
      538,
      539,
      540,
    ];

    if (origen.length > 0) {
      const value = puertos.includes(origen1[0]);
      localStorage.UsaDestiny = value;
      let datos;
      let word = origen1[0].toString();
      let url;
      if (localStorage.tipocon == "Contenedor compartido") {
        datos = {
          word: word,
          port: "MARITIMO LCL",
        };
        url = this.props.api + "maritimo_v2/find_port_to_mar";
      } else if (localStorage.tipocon == "Contenedor completo") {
        datos = {
          word: word,
          port: "MARITIMO FCL",
        };
        url = this.props.api + "maritimo_v2/find_port_to_mar";
      } else {
        datos = {
          word: word,
        };
        url = this.props.api + "aereo_v2/find_port_to_aereo";
      }

      (async () => {
        axios
          .post(url, await datos)
          .then((res) => {
            let response;
            let data = res.data.data.r_dat;

            let value = data.filter(function (item) {
              if (
                item.id == 100 ||
                item.id == 251 ||
                item.id == 598 ||
                item.id == 646 ||
                item.id == 663
              ) {
                return item.id;
              }
            });

            let valueLCL = data.filter(function (item) {
              if (item.id == 525) {
                return item.id;
              }
            });

            if (
              value.length > 0 &&
              localStorage.tipocon == "Contenedor completo"
            ) {
              response = [
                ...data,
                { id: 100, puerto: "PERÚ - OTRAS PROVINCIAS" },
              ];
            } else if (localStorage.tipocon == "Contenedor compartido") {
              if (value.length > 0 && valueLCL.length > 0) {
                response = [
                  { id: 525, puerto: "PANAMÁ - PANAMÁ CITY" },
                  { id: 525, puerto: "PANAMÁ - OTRAS PROVINCIAS" },
                  ...data,
                  { id: 100, puerto: "PERÚ - OTRAS PROVINCIAS" },
                ];
              } else if (value.length > 0) {
                response = [
                  ...data,
                  { id: 100, puerto: "PERÚ - OTRAS PROVINCIAS" },
                ];
              } else if (valueLCL.length > 0) {
                response = [
                  ...data,
                  { id: 525, puerto: "PANAMÁ - PANAMÁ CITY" },
                  { id: 525, puerto: "PANAMÁ - OTRAS PROVINCIAS" },
                ];
              } else {
                response = data;
              }
            } else if (value.length > 0 && localStorage.transporte == "AÉREO") {
              response = [
                ...data,
                { id: 251, puerto: "PERÚ - OTRAS PROVINCIAS" },
              ];
            } else {
              response = data;
            }

            this.setState({
              portDestiny: response,
            });
          })
          .catch((error) => {});
      })();
    } else {
      this.setState({
        portDestiny: [],
        portDestinySelected: [],
      });
    }
  };
  checkOrigen = () => {
    if (this.state.portOriginSelected.length > 0) {
    } else {
      const instance = this._typeahead.getInstance();
      instance.clear();
      instance.focus();
      alert("Debe completar el puerto origen con una opción válida.");
    }
  };
  toggleModalPreview(OpcionValue) {
    switch (OpcionValue) {
      case "1":
        this.eventOpcion1();
        this.props.history.push("/Servicios");
        break;
      case "2":
        let data = JSON.parse(sessionStorage.getItem("user"));

        if (
          sessionStorage.user !== undefined &&
          sessionStorage.usuario == "2" &&
          data[1] !== "Invitado"
        ) {
          var empty = [];
          var vacio = empty.map(function (item) {
            return item;
          });
          localStorage.distrito = vacio;
          localStorage.regulador = vacio;
          localStorage.tipoMercancia = vacio;
          localStorage.distritoN = vacio;
          localStorage.ProvinciaN = vacio;
          localStorage.monto = vacio;
          localStorage.value4 = false;
          localStorage.value5 = false;
          localStorage.value6 = false;
          localStorage.value7 = false;
          localStorage.value8 = false;
          localStorage.seguro = true;

          localStorage.check3 = false;
          localStorage.check = false;
          localStorage.check2 = false;
          localStorage.check1 = false;
          localStorage.check4 = true;

          /*switch (localStorage.tipocon) {
            case "Contenedor completo":
              Event(
                "2.7.1 Calcular Cotizacion FCL Directo",
                "2.7.1 Calcular Cotizacion FCL Directo",
                "SERVICIOS"
              );
              break;
            case "Contenedor compartido":
              Event(
                "1.8.1 Calcular Cotizacion LCL Directo",
                "1.8.1 Calcular Cotizacion LCL Directo",
                "SERVICIOS"
              );
              break;
            case "Aereo":
              Event(
                "3.8.1 Calcular Cotizacion Aereo Directo",
                "3.8.1 Calcular Cotizacion Aereo Directo",
                "SERVICIOS"
              );
              break;

            default:
          }*/
          this.redirect();
        } else {
          let user = [
            ["PIC"],
            ["Invitado"],
            ["Invitado"],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));

          sessionStorage.usuario = "2";
          this.redirect();
          //this.setState((ps) => ({ IsVisiblePreview: !ps.IsVisiblePreview }));
        }
        break;
      default:
        break;
    }
  }
  toggleAlertOpcion() {
    this.setState((prevState) => ({
      isOpenAlertOpcion: !prevState.isOpenAlertOpcion,
    }));
  }
  render() {
    if (localStorage.transporte == undefined) {
      return <Redirect to="/" />;
    } else {
    }
    let selected;
    let selected2;
    let selected3;
    let portOrigin = this.state.portOrigin;
    const formatter = new Intl.NumberFormat("de-DE");

    if (valid === "1") {
      if (this.state.portDestinySelected.length > 0) {
        selected2 = false;
      } else {
        selected2 = true;
      }
    } else {
      selected2 = false;
    }
    let PortEmptyLabel;
    let PortEmptyLabel2;
    if (
      (this.state.activeTab == "1" && this.state.count1 > 0) ||
      this.state.count2 > 0 ||
      this.state.count3 > 0 ||
      this.state.count4 > 0 ||
      (this.state.activeTab == "2" &&
        this.state.form[0].value > 0 &&
        this.state.form[1].value > 0 &&
        (this.state.form[2].value > 0 || this.state.form[5].value > 0))
    ) {
      PortEmptyLabel = "Origen no disponible";
      PortEmptyLabel2 = "Cargando destinos";
    } else {
      PortEmptyLabel = "Debe selecionar tipo de envio";
      PortEmptyLabel2 = "Debe selecionar tipo de envio";
    }

    if (valid === "1") {
      if (
        (this.state.activeTab == "1" && this.state.count1 > 0) ||
        this.state.count2 > 0 ||
        this.state.count3 > 0 ||
        this.state.count4 > 0 ||
        (this.state.activeTab == "2" &&
          this.state.form[0].value > 0 &&
          this.state.form[1].value > 0 &&
          (this.state.form[2].value > 0 || this.state.form[5].value > 0))
      ) {
        selected3 = false;
      } else {
        selected3 = true;
      }
    } else {
      if (
        (this.state.activeTab == "1" && this.state.count1 > 0) ||
        this.state.count2 > 0 ||
        this.state.count3 > 0 ||
        this.state.count4 > 0 ||
        (this.state.activeTab == "2" &&
          this.state.form[0].value > 0 &&
          this.state.form[1].value > 0 &&
          (this.state.form[2].value > 0 || this.state.form[5].value > 0))
      ) {
        selected3 = false;
      } else {
        selected3 = true;
      }
    }

    if (valid === "1") {
      if (this.state.portOriginSelected.length > 0) {
        selected = false;
      } else {
        selected = true;
      }
    } else {
      if (this.state.portOriginSelected.length > 0) {
        selected = false;
      } else if (
        this.state.portOriginSelected.length < 1 &&
        ((this.state.activeTab == "1" && this.state.count1 > 0) ||
          this.state.count2 > 0 ||
          this.state.count3 > 0 ||
          this.state.count4 > 0 ||
          (this.state.activeTab == "2" &&
            this.state.form[0].value > 0 &&
            this.state.form[1].value > 0 &&
            (this.state.form[2].value > 0 || this.state.form[5].value > 0)))
      ) {
        selected = true;
      } else {
        selected = false;
      }
    }

    if (
      selected === false &&
      selected2 === false &&
      selected3 === false &&
      valid === "1"
    ) {
      if (localStorage.transporte == "MARITÍMO") {
        if (localStorage.tipocon == "Contenedor completo") {
          validated = "3";
        } else {
          validated = "4";
        }
      } else {
        validated = "5";
      }
    } else {
      validated = "2";
    }

    localStorage.page = "2";

    let unidad;
    switch (this.state.form[3].value) {
      case "2":
        unidad = "Kg";
        localStorage.peso = "Kg";
        break;
      case "1":
        unidad = "Lb";
        localStorage.peso = "Lb";
        break;
      case "4":
        unidad = "T";
        localStorage.peso = "T";
        break;

      default:
      // code block
    }

    return (
      <Fragment>
        {/*paso 2 */}
     
        <Title>
          <title>{((window.location.pathname).replace('/', '') )+ " | " + "PIC  - Calculadora de Fletes"}</title>
        </Title>
        <div className="container2 ">
          <Row className="h-100">
            <Colxx xxs="12" md="12" className="mx-auto mb-3 ">
              <Link to="/">
                {" "}
                <Button
                  style={{ fontSize: "0.8rem", padding: "1px" }}
                  className="btn-style"
                  size="sm"
                >
                  <i className="simple-icon-arrow-left-circle" /> VOLVER
                </Button>
              </Link>
            </Colxx>
            <Colxx xl="10" lg="11" className="mx-auto my-auto">
              {localStorage.transporte == "MARITÍMO" ? (
                <Colxx xxs="12" md="12" className="mx-auto mb-3 ">
                  <span className="step-style">
                    PASO 2. Indica Ruta y Tipo de Contenedor
                  </span>{" "}
                </Colxx>
              ) : (
                <Colxx xxs="12" md="12" className="mx-auto mb-3">
                  <div className="mb-2 step-style">
                    PASO 2. Indica Ruta y Volumen de mercancia
                  </div>{" "}
                  <div style={{ fontWeight: "bold" }}>
                    <span>SOLO TRANSPORTAMOS CARGAS </span>{" "}
                    <span className="color-orange">
                      MAYORES A 60 KG 0 0.6 m3
                    </span>
                  </div>{" "}
                </Colxx>
              )}
              <Colxx xxs="12" md="12" className="mx-auto my-auto choseService">
                <Row form id="link2">
                  {localStorage.transporte == "MARITÍMO" ? (
                    <Col lg={3} md={12}>
                      <FormGroup>
                        <Label
                          className="fontNavbar label-input"
                          style={{ width: "max-content", marginLeft: "-2em" }}
                          htmlFor="TypeOfContainers"
                        >
                          Tipo de envío
                        </Label>
                        <Dropdown
                          className="mr-2"
                          isOpen={this.state.dropdownOpen}
                          toggle={this.toggle}
                        >
                          <DropdownToggle
                            color="light"
                            caret
                            className="form-container-type"
                          >
                            {this.state.placeholder}
                          </DropdownToggle>
                        </Dropdown>

                        <FormText className="container-info">
                          <span
                            style={{
                              display:
                                this.state.placeholder ===
                                  "Contenedor completo" &&
                                (this.state.count1 > 0 ||
                                  this.state.count2 > 0 ||
                                  this.state.count3 > 0 ||
                                  this.state.count4 > 0)
                                  ? ""
                                  : "none",
                            }}
                          >
                            <span
                              style={{
                                display:
                                  this.state.count1 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count1}*20' Std,{" "}
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count2 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count2}*40' Std,
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count3 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count3}*40' HC,
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count4 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count4}*40' NOR
                            </span>
                          </span>
                          <span
                            style={{
                              display:
                                this.state.placeholder ===
                                  "Contenedor compartido" &&
                                this.state.form[0].value > 0 &&
                                this.state.form[1].value > 0 &&
                                (this.state.form[2].value > 0 ||
                                  this.state.form[5].value > 0)
                                  ? ""
                                  : "none",
                            }}
                          >
                            Bultos = {this.state.form[0].value}, Peso ={" "}
                            {this.state.form[1].value} {unidad},{" "}
                            <div
                              style={{
                                display:
                                  this.state.form[2].value > 0 &&
                                  localStorage.transporte == "MARITÍMO"
                                    ? ""
                                    : "none",
                              }}
                            >
                              Volumen = {this.state.form[2].value}{" "}
                              {this.state.form[4].value == "1" ? "M³" : "Ft³"}
                            </div>
                          </span>
                        </FormText>
                        <div
                          className="validation-text"
                          style={{
                            display: selected3 === false ? "none" : "",
                          }}
                        >
                          Debe escoger tipo de envío
                        </div>
                      </FormGroup>
                    </Col>
                  ) : (
                    <Col lg={3} md={12}>
                      <FormGroup>
                        <Label
                          className="fontNavbar label-input"
                          style={{ width: "max-content" }}
                          htmlFor="TypeOfContainers"
                        >
                          Contenido a Enviar
                        </Label>
                        <Dropdown
                          className="mr-2"
                          onClick={() => {
                            this.tabs("2");
                          }}
                          isOpen={this.state.dropdownOpen2}
                          toggle={this.toggle2}
                        >
                          <DropdownToggle
                            color="light"
                            caret
                            className="form-container-type"
                          >
                            {this.state.placeholder2}
                          </DropdownToggle>
                        </Dropdown>

                        <FormText className="container-info">
                          <span
                            style={{
                              display:
                                this.state.placeholder ===
                                  "Contenedor completo" &&
                                (this.state.count1 > 0 ||
                                  this.state.count2 > 0 ||
                                  this.state.count3 > 0 ||
                                  this.state.count4 > 0)
                                  ? ""
                                  : "none",
                            }}
                          >
                            <span
                              style={{
                                display:
                                  this.state.count1 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count1}*20' Std,{" "}
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count2 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count2}*40' Std,
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count3 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count3}*40' HC,
                            </span>
                            <span
                              style={{
                                display:
                                  this.state.count4 === 0
                                    ? "none"
                                    : "inline-block",
                              }}
                            >
                              {this.state.count4}*40' NOR
                            </span>
                          </span>
                          <span
                            style={{
                              display:
                                this.state.placeholder ===
                                  "Contenedor compartido" &&
                                this.state.form[0].value > 0 &&
                                this.state.form[1].value > 0 &&
                                (this.state.form[2].value > 0 ||
                                  this.state.form[5].value > 0)
                                  ? ""
                                  : "none",
                            }}
                          >
                            Bultos = {this.state.form[0].value}, Peso ={" "}
                            {this.state.form[1].value} {unidad},{" "}
                            <div
                              style={{
                                display: this.state.form[2].value > 0,
                              }}
                            >
                              Volumen = {this.state.form[2].value}{" "}
                              {this.state.form[4].value == "1" ? "M³" : "Ft³"}
                            </div>
                          </span>
                        </FormText>
                        <div
                          style={{
                            fontWeight: "bold",
                            fontSize: "12px",
                            display: selected3 === false ? "none" : "",
                          }}
                        >
                          <span className="color-orange">* </span> INGRESE LOS
                          VALORES DE LA CARGA
                        </div>
                        <div
                          className="color-orange"
                          style={{
                            fontWeight: "bold",
                            fontSize: "10px",
                            display: selected3 === false ? "none" : "",
                          }}
                        >
                          SOLO CARGAS MAYORES A 60 KG 0 0.6m3
                        </div>
                      </FormGroup>
                    </Col>
                  )}
                  <Col
                    className="typeOfContainer display-sm"
                    style={{
                      display: this.state.hide === false ? "none" : "contents",
                    }}
                  >
                    <div
                      className="fontNavbar number-of-containers"
                      style={{
                        width: "100%",
                      }}
                    >
                      <Nav tabs>
                        <NavItem
                          className={`${
                            this.state.activeTab === "false" ? "colorTab" : ""
                          }`}
                        >
                          <NavLink
                            style={{
                              padding: "1em 0em 0.5em 0.5em",
                              borderBottom: "0.2em #0d5084",
                              borderBottomStyle: "ridge",
                            }}
                            className={classnames({
                              active: this.state.activeTab === "1",
                            })}
                            onClick={() => {
                              this.tabs("1");
                              this.changePlaceholder(1);
                            }}
                          >
                            {" "}
                            <i className="icon-FCL mr-2" /> CONTENEDOR
                            COMPLETO(FCL)
                            <br /> <span>Un contenedor - Un cliente</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            style={{ padding: "1em 0em 0.5em 0.5em" }}
                            className={classnames({
                              active: this.state.activeTab === "2",
                            })}
                            onClick={() => {
                              this.tabs("2");
                              this.changePlaceholder(2);
                            }}
                          >
                            <i className="icon-LCL ml-2 mr-2" />{" "}
                            <div>
                              <div>
                                CONTENEDOR COMPARTIDO(LCL)
                                <div>ó CARGA CONSOLIDADA</div>
                              </div>
                              <div className="pl-5 ml-3">
                                Un contenedor - Varios cliente
                              </div>
                            </div>
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                          <div className="cost-top">
                            <dl className="persons_quantity">
                              <div className="quantity_wrap">
                                <dt>
                                  <span>
                                    20' STANDARD{" "}
                                    <i
                                      onClick={() =>
                                        this.setState(() => ({
                                          showBoxmb1: true,
                                        }))
                                      }
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </span>
                                </dt>
                                <dd>
                                  <ul className="number">
                                    <li>
                                      {" "}
                                      <span
                                        data-uil="minus"
                                        className="minus fal fa-minus"
                                        value={this.state.count1}
                                        onClick={() => this.minusCount(1)}
                                      >
                                        <i className="simple-icon-minus " />
                                      </span>
                                    </li>
                                    <li>
                                      <Input
                                        name="adt"
                                        type="text"
                                        className="value adults"
                                        value={this.state.count1}
                                        readOnly="readonly"
                                      />
                                    </li>
                                    <li>
                                      <span
                                        key={1}
                                        data-uil="plus"
                                        className="plus fal fa-plus"
                                        value={this.state.count1}
                                        onClick={() => this.plusCount(1)}
                                      >
                                        <i className="simple-icon-plus" />
                                      </span>
                                    </li>
                                  </ul>
                                </dd>
                              </div>

                              <div className="quantity_wrap">
                                <dt>
                                  <span>
                                    40' STANDARD{" "}
                                    <i
                                      onClick={() =>
                                        this.setState(() => ({
                                          showBoxmb2: true,
                                        }))
                                      }
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </span>
                                </dt>
                                <dd>
                                  <ul className="number">
                                    <li>
                                      {" "}
                                      <span
                                        data-uil="minus"
                                        className="minus fal fa-minus"
                                        onClick={() => this.minusCount(2)}
                                      >
                                        <i className="simple-icon-minus " />
                                      </span>
                                    </li>
                                    <li>
                                      <Input
                                        name="adt"
                                        type="text"
                                        className="value adults"
                                        value={this.state.count2}
                                        readOnly="readonly"
                                      />
                                    </li>
                                    <li>
                                      <span
                                        key={2}
                                        data-uil="plus"
                                        className="plus fal fa-plus"
                                        value={this.state.count2}
                                        onClick={() => this.plusCount(2)}
                                      >
                                        <i className="simple-icon-plus" />
                                      </span>
                                    </li>
                                  </ul>
                                </dd>
                              </div>

                              <div className="quantity_wrap">
                                <dt>
                                  <span>
                                    40' HC{" "}
                                    <i
                                      onClick={() =>
                                        this.setState(() => ({
                                          showBoxmb3: true,
                                        }))
                                      }
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </span>
                                </dt>
                                <dd>
                                  <ul className="number">
                                    <li>
                                      {" "}
                                      <span
                                        data-uil="minus"
                                        className="minus fal fa-minus"
                                        onClick={() => this.minusCount(3)}
                                      >
                                        <i className="simple-icon-minus " />
                                      </span>
                                    </li>
                                    <li>
                                      <Input
                                        name="adt"
                                        type="text"
                                        className="value adults"
                                        value={this.state.count3}
                                        readOnly="readonly"
                                      />
                                    </li>
                                    <li>
                                      <span
                                        key={3}
                                        data-uil="plus"
                                        className="plus fal fa-plus"
                                        value={this.state.count3}
                                        onClick={() => this.plusCount(3)}
                                      >
                                        <i className="simple-icon-plus" />
                                      </span>
                                    </li>
                                  </ul>
                                </dd>
                              </div>

                              <div className="quantity_wrap">
                                <dt>
                                  <span>
                                    40' NOR{" "}
                                    <i
                                      onClick={() =>
                                        this.setState(() => ({
                                          showBoxmb4: true,
                                        }))
                                      }
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </span>
                                </dt>
                                <dd>
                                  <ul className="number">
                                    <li>
                                      {" "}
                                      <span
                                        data-uil="minus"
                                        className="minus fal fa-minus"
                                        onClick={() => this.minusCount(4)}
                                      >
                                        <i className="simple-icon-minus " />
                                      </span>
                                    </li>
                                    <li>
                                      <Input
                                        name="adt"
                                        type="text"
                                        className="value adults"
                                        value={this.state.count4}
                                        readOnly="readonly"
                                      />
                                    </li>
                                    <li>
                                      <span
                                        key={4}
                                        data-uil="plus"
                                        className="plus fal fa-plus"
                                        value={this.state.count4}
                                        onClick={() => this.plusCount(4)}
                                      >
                                        <i className="simple-icon-plus" />
                                      </span>
                                    </li>
                                  </ul>
                                </dd>
                              </div>
                              <div className="quantity_wrap">
                                <dt>
                                  <Button
                                    color="success"
                                    onClick={() =>
                                      this.setState((prevState) => ({
                                        hide: !prevState.hide,
                                      }))
                                    }
                                    className="btn-style mr-2"
                                    size="sm"
                                  >
                                    VOLVER{" "}
                                    <i className="simple-icon-arrow-left-circle" />
                                  </Button>
                                </dt>
                                <dd>
                                  <ul className="number">
                                    <li>
                                      <Button
                                        color="success"
                                        onClick={() =>
                                          this.setState((prevState) => ({
                                            hide: !prevState.hide,
                                          }))
                                        }
                                        className="btn-style-blue"
                                        size="sm"
                                      >
                                        GUARDAR
                                      </Button>
                                    </li>
                                  </ul>
                                </dd>
                              </div>
                            </dl>
                          </div>
                        </TabPane>
                        <TabPane tabId="2">
                          <Form id="typeOfContainer">
                            <Row form className="p-1">
                              <Col md={12}>
                                <FormGroup className="form-row">
                                  <Label
                                    className="col-3 col-md-3 pt-1 fontNavbar"
                                    for="exampleEmail"
                                  >
                                    BULTOS
                                  </Label>
                                  <InputGroup className="col-9 col-md-9">
                                    <Input
                                      type="number"
                                      placeholder="Ingrese Nro de bultos"
                                      bsSize="sm"
                                      value={this.state.form[0].value}
                                      onChange={(e) => {
                                        this.handleFormState({
                                          id: 0,
                                          value: e.target.value,
                                        });
                                      }}
                                    />
                                  </InputGroup>
                                </FormGroup>
                              </Col>
                              <Col xs={12} md={12}>
                                <FormGroup className="form-row">
                                  <Label
                                    className="col-3 pt-1 col-md-3 fontNavbar"
                                    for="examplePassword"
                                  >
                                    PESO
                                  </Label>
                                  <InputGroup className="col-9 col-md-9">
                                    <Input
                                      type="number"
                                      placeholder="Ingrese Peso"
                                      bsSize="sm"
                                      value={this.state.form[1].value}
                                      onChange={(e) => {
                                        this.handleFormState({
                                          id: 1,
                                          value: e.target.value,
                                        });
                                      }}
                                    />
                                    <Input
                                      type="select"
                                      name="select"
                                      onChange={(e) => {
                                        this.handleFormState({
                                          id: 3,
                                          value: e.target.value || "",
                                        });
                                      }}
                                    >
                                      <option defaultValue key="2" value="2">
                                        Kilogramos
                                      </option>
                                      <option key="1" value="1">
                                        Libras
                                      </option>
                                      <option key="4" value="4">
                                        Toneladas
                                      </option>
                                    </Input>
                                  </InputGroup>
                                </FormGroup>
                              </Col>

                              <Col md={12}>
                                <FormGroup className="form-row">
                                  <Label
                                    className="col-12 pt-1 col-md-3 fontNavbar"
                                    for="examplePassword"
                                  >
                                    VOLUMEN{" "}
                                    <i
                                      id="volumen1"
                                      className="simple-icon-question question-style2"
                                    />
                                  </Label>
                                  <UncontrolledTooltip
                                    placement="right"
                                    target="volumen1"
                                  >
                                    <div className="color-blue text-justify">
                                      <span style={{ fontWeight: "bold" }}>
                                        Nota:
                                      </span>{" "}
                                      Es el resultado de multplicar largo x
                                      ancho x el alto del producto
                                    </div>
                                    <div className="color-blue text-justify">
                                      <span style={{ fontWeight: "bold" }}>
                                        Ejemplo:
                                      </span>{" "}
                                      Una mesa
                                      <ul>
                                        <li>Largo 1 metro</li>
                                        <li>Ancho 1 metro</li>
                                        <li>Alto 1 metro</li>
                                        <li>Seria 1 x 1 x 1 = 1m3</li>
                                      </ul>
                                    </div>
                                  </UncontrolledTooltip>
                                  <InputGroup className="col-12 col-md-9">
                                    <Input
                                      type="number"
                                      placeholder="Ingrese Volumen"
                                      bsSize="sm"
                                      value={this.state.form[2].value}
                                      onChange={(e) => {
                                        this.handleFormState({
                                          id: 2,
                                          value: e.target.value,
                                        });
                                      }}
                                    />
                                    <Input
                                      type="select"
                                      name="select"
                                      onChange={(e) => {
                                        this.handleFormState({
                                          id: 4,
                                          value: e.target.value || "",
                                        });
                                      }}
                                    >
                                      <option defaultValue key="1" value="1">
                                        Metros cúbicos
                                      </option>
                                      <option key="2" value="2">
                                        Pies cúbicos
                                      </option>
                                    </Input>
                                  </InputGroup>
                                  <FormText
                                    className="regulation-text mr-1 ml-1"
                                    style={{
                                      backgroundColor: "aliceblue",
                                      display:
                                        this.state.msjActivo === false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    {" "}
                                    El presupuesto final que arroja la
                                    calculadora para cargas mayores a 15m3 esta
                                    sujeto a aprobación.
                                  </FormText>
                                </FormGroup>
                              </Col>
                              <Col
                                md={12}
                                className="calculatorOpenContainer arial mx-auto  p-1 mb-2"
                              >
                                <span onClick={this.toggleCalculator}>
                                  <i
                                    style={{
                                      fontWeight: "bold",
                                      verticalAlign: "middle",
                                    }}
                                    className="mr-1 icon-doubt  "
                                  />
                                  &nbsp; AYUDA - ¡CALCULA VOLUMEN (m3) AQUI!
                                </span>
                              </Col>
                              <Col md={12}>
                                <Button
                                  color="success"
                                  onClick={() =>
                                    this.setState((prevState) => ({
                                      hide: !prevState.hide,
                                    }))
                                  }
                                  className="btn-style mr-2"
                                  size="sm"
                                >
                                  VOLVER{" "}
                                  <i className="simple-icon-arrow-left-circle" />
                                </Button>
                                <Button
                                  color="success"
                                  onClick={() => {
                                    this.setvolume(0);
                                    this.tabs("2");
                                    this.changePlaceholder(2);
                                  }}
                                  className="btn-style-blue"
                                  size="sm"
                                >
                                  GUARDAR
                                </Button>
                                <span id="bottom"></span>
                              </Col>
                            </Row>
                          </Form>
                        </TabPane>
                      </TabContent>
                    </div>
                  </Col>
                  <Col
                    className="typeOfContainer display-sm"
                    style={{
                      display: this.state.hide2 === false ? "none" : "contents",
                    }}
                  >
                    <div
                      className="fontNavbar number-of-containers"
                      style={{
                        width: "100%",
                      }}
                    >
                      <Col md={12} className="navAereo">
                        CONTENIDO DE ENVÍO
                      </Col>
                      <Form id="typeOfContainer4">
                        <Row form className="p-1">
                          <Col md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-3 col-md-3 pt-1 fontNavbar"
                                for="exampleEmail"
                              >
                                BULTOS
                              </Label>
                              <InputGroup className="col-9 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Nro de bultos"
                                  bsSize="sm"
                                  value={this.state.form[0].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 0,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col xs={12} md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-3 pt-1 col-md-3 fontNavbar"
                                for="examplePassword"
                              >
                                PESO
                              </Label>
                              <InputGroup className="col-9 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Peso"
                                  bsSize="sm"
                                  value={this.state.form[1].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 1,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                                <Input
                                  type="select"
                                  name="select"
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 3,
                                      value: e.target.value || "",
                                    });
                                  }}
                                >
                                  <option defaultValue key="2" value="2">
                                    Kilogramos
                                  </option>
                                  <option key="1" value="1">
                                    Libras
                                  </option>
                                  <option key="4" value="4">
                                    Toneladas
                                  </option>
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          </Col>

                          <Col md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-12 pt-1 col-md-3 fontNavbar"
                                for="examplePassword"
                              >
                                VOLUMEN{" "}
                                <i
                                  id="volumen3"
                                  className="simple-icon-question question-style2"
                                />
                              </Label>
                              <UncontrolledTooltip
                                placement="right"
                                target="volumen3"
                              >
                                <div className="color-blue text-justify">
                                  <span style={{ fontWeight: "bold" }}>
                                    Nota:
                                  </span>{" "}
                                  Es el resultado de multplicar largo x ancho x
                                  el alto del producto
                                </div>
                                <div className="color-blue text-justify">
                                  <span style={{ fontWeight: "bold" }}>
                                    Ejemplo:
                                  </span>{" "}
                                  Una mesa
                                  <ul>
                                    <li>Largo 1 metro</li>
                                    <li>Ancho 1 metro</li>
                                    <li>Alto 1 metro</li>
                                    <li>Seria 1 x 1 x 1 = 1m3</li>
                                  </ul>
                                </div>
                              </UncontrolledTooltip>
                              <InputGroup className="col-12 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Volumen"
                                  bsSize="sm"
                                  value={this.state.form[2].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 2,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                                <Input
                                  type="select"
                                  name="select"
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 4,
                                      value: e.target.value || "",
                                    });
                                  }}
                                >
                                  <option defaultValue key="1" value="1">
                                    Metros cúbicos
                                  </option>
                                  <option key="2" value="2">
                                    Pies cúbicos
                                  </option>
                                </Input>
                              </InputGroup>
                              <FormText
                                className="mx-auto"
                                style={{
                                  fontWeight: "bold",
                                  fontSize: "10px",
                                }}
                              >
                                <span>
                                  <span className="color-orange">* </span>{" "}
                                  INGRESE UN VALOR{" "}
                                </span>
                                <span className="color-orange">
                                  {" "}
                                  MAYOR A 60 KG 0 0.6m3
                                </span>
                              </FormText>
                            </FormGroup>
                          </Col>
                          <Col
                            md={12}
                            className="calculatorOpenContainer arial mx-auto mb-2 p-1"
                          >
                            <span onClick={this.toggleCalculator}>
                              <i
                                style={{
                                  fontWeight: "bold",
                                  verticalAlign: "middle",
                                }}
                                className="mr-1 icon-doubt  "
                              />
                              &nbsp;AYUDA -¡CALCULA VOLUMEN AQUI!
                            </span>
                          </Col>

                          <Col md={12}>
                            <Button
                              color="success"
                              onClick={() =>
                                this.setState((prevState) => ({
                                  hide2: !prevState.hide2,
                                }))
                              }
                              className="btn-style mr-2"
                              size="sm"
                            >
                              VOLVER{" "}
                              <i className="simple-icon-arrow-left-circle" />
                            </Button>
                            <Button
                              color="success"
                              onClick={() => {
                                this.setvolume(0);
                                this.tabs("2");
                                this.changePlaceholder(2);
                              }}
                              className="btn-style-blue"
                              size="sm"
                            >
                              GUARDAR
                            </Button>
                            <span id="bottom"></span>
                          </Col>
                        </Row>
                      </Form>
                    </div>
                  </Col>
                  <Col lg={3} md={12}>
                    <FormGroup>
                      <Label
                        className="fontNavbar label-input"
                        htmlFor="origin"
                      >
                        Origen
                      </Label>

                      <AsyncTypeahead
                        id="originInput"
                        isValid={selected}
                        emptyLabel={
                          localStorage.transporte == "MARITÍMO"
                            ? PortEmptyLabel
                            : "Aeropuerto no disponible"
                        }
                        flip={true}
                        delay={10}
                        placeholder={
                          localStorage.transporte == "MARITÍMO"
                            ? "ESCRIBA Pais,Ciudad o Puerto"
                            : "Pais o Ciudad"
                        }
                        isLoading={this.state.isLoading}
                        labelKey={(option) => `${option.puerto}`}
                        filterBy={() => true}
                        renderMenuItemChildren={(option) => (
                          <div>{option.puerto}</div>
                        )}
                        minLength={1}
                        onSearch={(query) => {
                          this.fuzzy(query);
                        }}
                        onChange={(portOriginSelected) => {
                          this.setState({ portOriginSelected });
                          this.Destiny(portOriginSelected);
                          this.get_ctz();
                          this.EventOrigen(
                            localStorage.transporte == "MARITÍMO" ? 2 : 1
                          );
                        }}
                        ref={(ref) => (this._typeahead = ref)}
                        onFocus={() =>
                          this.setState(() => ({
                            hide: false,
                            hide2: false,
                          }))
                        }
                        selected={this.state.portOriginSelected}
                        options={this.state.fuzzyOptions}
                      />

                      <FormText
                        className="validation-text"
                        style={{
                          display: selected === false ? "none" : "",
                        }}
                      >
                        <span className="arial">ESCRIBA</span> Pais o Puerto de
                        Origen
                      </FormText>
                    </FormGroup>
                  </Col>
                  <Col lg={3} md={12}>
                    <FormGroup>
                      <Label
                        className="fontNavbar label-input"
                        htmlFor="destiny"
                      >
                        Destino
                      </Label>

                      <Typeahead
                        isValid={selected2}
                        flip={true}
                        id="1"
                        emptyLabel={
                          localStorage.transporte == "MARITÍMO"
                            ? PortEmptyLabel2
                            : "Aeropuerto no disponible"
                        }
                        labelKey={(option) => `${option.puerto}`}
                        renderMenuItemChildren={(option) => (
                          <div>{option.puerto}</div>
                        )}
                        autocomplete={false}
                        multiple={false}
                        options={this.state.portDestiny}
                        placeholder={
                          localStorage.transporte == "MARITÍMO"
                            ? "Pais,Ciudad o Puerto"
                            : "Pais o Ciudad"
                        }
                        onChange={(portDestinySelected) => {
                          this.setState({ portDestinySelected });
                          this.EventDestino(
                            localStorage.transporte == "MARITÍMO" ? 2 : 1,
                            portDestinySelected
                          );
                        }}
                        onInputChange={this.checkOrigen}
                        onFocus={this.checkOrigen}
                        selected={this.state.portDestinySelected}
                      />

                      <FormText
                        className="validation-text"
                        style={{
                          display: selected2 === false ? "none" : "",
                        }}
                      >
                        Seleccione un puerto de destino valido
                      </FormText>
                    </FormGroup>
                  </Col>
                  <Col lg={3} md={12}>
                    <FormGroup
                      style={{
                        display: this.state.resultContainer ? "none" : "",
                        marginTop: "25px",
                      }}
                    >
                      <Button
                        type="submit"
                        className="btn-style-blue "
                        size="sm"
                        onClick={() => {
                          this.get_ctz();
                          this.eventAgregarToque();
                        }}
                        color="success"
                      >
                        SIGUIENTE
                      </Button>
                    </FormGroup>
                  </Col>
                </Row>
              </Colxx>
              <Col
                className="typeOfContainer display-large"
                style={{
                  display: this.state.hide === false ? "none" : "block",
                }}
              >
                <div className="fontNavbar number-of-containers">
                  <Nav tabs>
                    <NavItem
                      className={`${
                        this.state.activeTab === "false" ? "colorTab" : ""
                      }`}
                    >
                      <NavLink
                        style={{
                          borderBottom: "0.2em #0d5084",
                          borderBottomStyle: "ridge",
                        }}
                        className={classnames({
                          active: this.state.activeTab === "1",
                        })}
                        onClick={() => {
                          this.tabs("1");
                          this.changePlaceholder(1);
                        }}
                      >
                        {" "}
                        <i className="icon-FCL mr-2" /> CONTENEDOR COMPLETO(FCL)
                        <br /> <span>Un contenedor - Un cliente</span>
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        style={{ padding: "1em 0em 0.5em 0.5em" }}
                        className={classnames({
                          active: this.state.activeTab === "2",
                        })}
                        onClick={() => {
                          this.tabs("2");
                          this.changePlaceholder(2);
                        }}
                      >
                        <i className="icon-LCL ml-2 mr-2" />{" "}
                        <div>
                          <div>
                            CONTENEDOR COMPARTIDO(LCL)
                            <div>ó CARGA CONSOLIDADA</div>
                          </div>
                          <div className="pl-5 ml-3">
                            Un contenedor - Varios cliente
                          </div>
                        </div>
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <div className="cost-top">
                        <dl className="persons_quantity">
                          <div className="quantity_wrap">
                            <dt>
                              <span>
                                20' STANDARD{" "}
                                <i
                                  onMouseLeave={() =>
                                    this.handleBoxToggle(false, 0)
                                  }
                                  onMouseEnter={() =>
                                    this.handleBoxToggle(true, 0)
                                  }
                                  style={{ cursor: "pointer" }}
                                  className="simple-icon-question question-style2"
                                />
                              </span>
                              <div
                                onMouseLeave={() =>
                                  this.handleBoxToggle(false, 0)
                                }
                                className={`tooltip-inner-cont ${
                                  this.state.showBox1
                                    ? "show-toolkit-cont"
                                    : "hide-toolkit"
                                }`}
                              >
                                <Card
                                  style={{
                                    borderRadius: "2rem",
                                  }}
                                >
                                  <CardHeader
                                    className="theme-color-blueHeader arial"
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    20' STANDARD
                                  </CardHeader>

                                  <CardBody
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    <CardImg
                                      top
                                      width="1em"
                                      src="/assets/img/contenedores/20std.png"
                                      alt="1em"
                                    />
                                    <CardText>
                                      <Table
                                        style={{
                                          textAlign: "center",
                                          fontSize: "0.7rem",
                                        }}
                                        striped
                                      >
                                        <tbody style={{}}>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              PESO MAX. PERMITIDO
                                            </th>
                                            <td>26 TONELADAS</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              VOL. MAX. CARGABLE
                                            </th>
                                            <td>33 M3</td>
                                          </tr>
                                          <tr>
                                            <th
                                              className="theme-color-blue"
                                              scope="row"
                                              colSpan="4"
                                            >
                                              MEDIDAS INTERNAS CONTENEDOR
                                            </th>
                                          </tr>
                                          <tr>
                                            <td scope="row" colSpan="2">
                                              LARGO
                                            </td>
                                            <td>ANCHO</td>
                                            <td>ALTO</td>
                                          </tr>
                                          <tr>
                                            <td scope="row" colSpan="2">
                                              5.89 MTS
                                            </td>
                                            <td>2.35 MTS</td>
                                            <td>2.38 MTS</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                    </CardText>
                                  </CardBody>
                                  <CardFooter className="theme-color-redFooter">
                                    {" "}
                                    <div>
                                      <span style={{ fontWeight: "bold" }}>
                                        Nota:
                                      </span>{" "}
                                      El peso máximo permitido variara de
                                      acuerdo a las normativas establecidas por
                                      cada naviera.{" "}
                                    </div>
                                  </CardFooter>
                                </Card>
                              </div>
                            </dt>
                            <dd>
                              <ul className="number">
                                <li>
                                  {" "}
                                  <span
                                    data-uil="minus"
                                    className="minus fal fa-minus"
                                    value={this.state.count1}
                                    onClick={() => this.minusCount(1)}
                                  >
                                    <i className="simple-icon-minus " />
                                  </span>
                                </li>
                                <li>
                                  <Input
                                    name="adt"
                                    type="text"
                                    className="value adults"
                                    value={this.state.count1}
                                    readOnly="readonly"
                                  />
                                </li>
                                <li>
                                  <span
                                    key={1}
                                    data-uil="plus"
                                    className="plus fal fa-plus"
                                    value={this.state.count1}
                                    onClick={() => this.plusCount(1)}
                                  >
                                    <i className="simple-icon-plus" />
                                  </span>
                                </li>
                              </ul>
                            </dd>
                          </div>

                          <div className="quantity_wrap">
                            <dt>
                              <span>
                                40' STANDARD{" "}
                                <i
                                  onMouseLeave={() =>
                                    this.handleBoxToggle(false, 1)
                                  }
                                  onMouseEnter={() =>
                                    this.handleBoxToggle(true, 1)
                                  }
                                  style={{ cursor: "pointer" }}
                                  className="simple-icon-question question-style2"
                                />
                              </span>
                              <div
                                onMouseLeave={() =>
                                  this.handleBoxToggle(false, 1)
                                }
                                className={`tooltip-inner-cont ${
                                  this.state.showBox2
                                    ? "show-toolkit-cont"
                                    : "hide-toolkit"
                                }`}
                              >
                                <Card
                                  style={{
                                    borderRadius: "2rem",
                                  }}
                                >
                                  <CardHeader
                                    className="theme-color-blueHeader arial"
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    {" "}
                                    40' STANDARD
                                  </CardHeader>

                                  <CardBody
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    <CardImg
                                      top
                                      width="1em"
                                      src="/assets/img/contenedores/40std.png"
                                      alt="1em"
                                    />
                                    <CardText>
                                      <Table
                                        style={{
                                          textAlign: "center",
                                          fontSize: "0.7rem",
                                        }}
                                        striped
                                      >
                                        <tbody style={{}}>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              PESO MAX. PERMITIDO
                                            </th>
                                            <td>26 TONELADAS</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              VOL. MAX. CARGABLE
                                            </th>
                                            <td>67 M3</td>
                                          </tr>
                                          <tr>
                                            <th
                                              className="theme-color-red"
                                              scope="row"
                                              colSpan="4"
                                            >
                                              MEDIDAS INTERNAS CONTENEDOR
                                            </th>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="2">
                                              LARGO
                                            </th>
                                            <td>ANCHO</td>
                                            <td>ALTO</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="2">
                                              12.03 MTS
                                            </th>
                                            <td>2.35 MTS</td>
                                            <td>2.38 MTS</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                    </CardText>
                                  </CardBody>
                                  <CardFooter className="theme-color-redFooter">
                                    {" "}
                                    <div>
                                      <span style={{ fontWeight: "bold" }}>
                                        Nota:
                                      </span>{" "}
                                      El peso máximo permitido variara de
                                      acuerdo a las normativas establecidas por
                                      cada naviera.{" "}
                                    </div>
                                  </CardFooter>
                                </Card>
                              </div>
                            </dt>
                            <dd>
                              <ul className="number">
                                <li>
                                  {" "}
                                  <span
                                    data-uil="minus"
                                    className="minus fal fa-minus"
                                    onClick={() => this.minusCount(2)}
                                  >
                                    <i className="simple-icon-minus " />
                                  </span>
                                </li>
                                <li>
                                  <Input
                                    name="adt"
                                    type="text"
                                    className="value adults"
                                    value={this.state.count2}
                                    readOnly="readonly"
                                  />
                                </li>
                                <li>
                                  <span
                                    key={2}
                                    data-uil="plus"
                                    className="plus fal fa-plus"
                                    value={this.state.count2}
                                    onClick={() => this.plusCount(2)}
                                  >
                                    <i className="simple-icon-plus" />
                                  </span>
                                </li>
                              </ul>
                            </dd>
                          </div>

                          <div className="quantity_wrap">
                            <dt>
                              <span>
                                40' HC{" "}
                                <i
                                  onMouseLeave={() =>
                                    this.handleBoxToggle(false, 2)
                                  }
                                  onMouseEnter={() =>
                                    this.handleBoxToggle(true, 2)
                                  }
                                  style={{ cursor: "pointer" }}
                                  className="simple-icon-question question-style2"
                                />
                              </span>
                              <div
                                onMouseLeave={() =>
                                  this.handleBoxToggle(false, 2)
                                }
                                className={`tooltip-inner-cont ${
                                  this.state.showBox3
                                    ? "show-toolkit-cont"
                                    : "hide-toolkit"
                                }`}
                              >
                                <Card
                                  style={{
                                    borderRadius: "2rem",
                                  }}
                                >
                                  <CardHeader
                                    className="theme-color-blueHeader arial"
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    {" "}
                                    40' HC
                                  </CardHeader>
                                  <CardBody
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    <CardImg
                                      top
                                      width="1em"
                                      src="/assets/img/contenedores/40hq.png"
                                      alt="1em"
                                    />
                                    <CardText>
                                      <Table
                                        style={{
                                          textAlign: "center",
                                          fontSize: "0.7rem",
                                        }}
                                        striped
                                      >
                                        <tbody style={{}}>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              PESO MAX. PERMITIDO
                                            </th>
                                            <td>26 TONELADAS</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              VOL. MAX. CARGABLE
                                            </th>
                                            <td>76 M3</td>
                                          </tr>
                                          <tr>
                                            <th
                                              className="theme-color-green"
                                              scope="row"
                                              colSpan="4"
                                            >
                                              MEDIDAS INTERNAS CONTENEDOR
                                            </th>
                                          </tr>
                                          <tr>
                                            <td scope="row" colSpan="2">
                                              LARGO
                                            </td>
                                            <td>ANCHO</td>
                                            <td>ALTO</td>
                                          </tr>
                                          <tr>
                                            <td scope="row" colSpan="2">
                                              12.03 MTS
                                            </td>
                                            <td>2.35 MTS</td>
                                            <td>2.69 MTS</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                    </CardText>
                                  </CardBody>
                                  <CardFooter className="theme-color-redFooter">
                                    {" "}
                                    <div>
                                      <span style={{ fontWeight: "bold" }}>
                                        Nota:
                                      </span>{" "}
                                      El peso máximo permitido variara de
                                      acuerdo a las normativas establecidas por
                                      cada naviera.{" "}
                                    </div>
                                  </CardFooter>
                                </Card>
                              </div>
                            </dt>
                            <dd>
                              <ul className="number">
                                <li>
                                  {" "}
                                  <span
                                    data-uil="minus"
                                    className="minus fal fa-minus"
                                    onClick={() => this.minusCount(3)}
                                  >
                                    <i className="simple-icon-minus " />
                                  </span>
                                </li>
                                <li>
                                  <Input
                                    name="adt"
                                    type="text"
                                    className="value adults"
                                    value={this.state.count3}
                                    readOnly="readonly"
                                  />
                                </li>
                                <li>
                                  <span
                                    key={3}
                                    data-uil="plus"
                                    className="plus fal fa-plus"
                                    value={this.state.count3}
                                    onClick={() => this.plusCount(3)}
                                  >
                                    <i className="simple-icon-plus" />
                                  </span>
                                </li>
                              </ul>
                            </dd>
                          </div>

                          <div className="quantity_wrap">
                            <dt>
                              <span>
                                40' NOR{" "}
                                <i
                                  onMouseLeave={() =>
                                    this.handleBoxToggle(false, 3)
                                  }
                                  onMouseEnter={() =>
                                    this.handleBoxToggle(true, 3)
                                  }
                                  style={{ cursor: "pointer" }}
                                  className="simple-icon-question question-style2"
                                />
                              </span>
                              <div
                                onMouseLeave={() =>
                                  this.handleBoxToggle(false, 3)
                                }
                                className={`tooltip-inner-cont ${
                                  this.state.showBox4
                                    ? "show-toolkit-cont"
                                    : "hide-toolkit"
                                }`}
                              >
                                <Card
                                  style={{
                                    borderRadius: "2rem",
                                  }}
                                >
                                  <CardHeader
                                    className="theme-color-blueHeader arial"
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    {" "}
                                    40' NOR
                                  </CardHeader>
                                  <CardBody
                                    style={{
                                      padding: "0px",
                                    }}
                                  >
                                    <CardImg
                                      top
                                      width="1em"
                                      src="/assets/img/contenedores/40nor.png"
                                      alt="1em"
                                    />
                                    <CardText>
                                      <Table
                                        style={{
                                          textAlign: "center",
                                          fontSize: "0.7rem",
                                        }}
                                        striped
                                      >
                                        <tbody style={{}}>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              PESO MAX. PERMITIDO
                                            </th>
                                            <td>26 TONELADAS</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="3">
                                              VOL. MAX. CARGABLE
                                            </th>
                                            <td>48 M3</td>
                                          </tr>
                                          <tr>
                                            <th
                                              className="theme-color-blue"
                                              scope="row"
                                              colSpan="4"
                                            >
                                              MEDIDAS EXTERNAS CONTENEDOR
                                            </th>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="2">
                                              LARGO
                                            </th>
                                            <td>ANCHO</td>
                                            <td>ALTO</td>
                                          </tr>
                                          <tr>
                                            <th scope="row" colSpan="2">
                                              12.03 MTS
                                            </th>
                                            <td>2.35 MTS</td>
                                            <td>2.38 MTS</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                    </CardText>
                                  </CardBody>
                                  <CardFooter className="theme-color-redFooter">
                                    {" "}
                                    <div>
                                      <span style={{ fontWeight: "bold" }}>
                                        Nota:
                                      </span>{" "}
                                      Es un contenedor con un equipo de
                                      refrigeración interno que esta apagado, el
                                      cual se utiliza para para cargas de menor
                                      volumen ya que tienes 10 M3 menos que el
                                      40 ST debido al equipo que tiene interno.{" "}
                                    </div>
                                  </CardFooter>
                                </Card>
                              </div>
                            </dt>
                            <dd>
                              <ul className="number">
                                <li>
                                  {" "}
                                  <span
                                    data-uil="minus"
                                    className="minus fal fa-minus"
                                    onClick={() => this.minusCount(4)}
                                  >
                                    <i className="simple-icon-minus " />
                                  </span>
                                </li>
                                <li>
                                  <Input
                                    name="adt"
                                    type="text"
                                    className="value adults"
                                    value={this.state.count4}
                                    readOnly="readonly"
                                  />
                                </li>
                                <li>
                                  <span
                                    key={4}
                                    data-uil="plus"
                                    className="plus fal fa-plus"
                                    value={this.state.count4}
                                    onClick={() => this.plusCount(4)}
                                  >
                                    <i className="simple-icon-plus" />
                                  </span>
                                </li>
                              </ul>
                            </dd>
                          </div>
                          <div className="quantity_wrap">
                            <dt>
                              <Button
                                color="success"
                                onClick={() =>
                                  this.setState((prevState) => ({
                                    hide: !prevState.hide,
                                  }))
                                }
                                className="btn-style mr-2"
                                size="sm"
                              >
                                VOLVER{" "}
                                <i className="simple-icon-arrow-left-circle" />
                              </Button>
                            </dt>
                            <dd>
                              <ul className="number">
                                <li>
                                  <Button
                                    color="success"
                                    onClick={() =>
                                      this.setState((prevState) => ({
                                        hide: !prevState.hide,
                                      }))
                                    }
                                    className="btn-style-blue"
                                    size="sm"
                                  >
                                    GUARDAR
                                  </Button>
                                </li>
                              </ul>
                            </dd>
                          </div>
                        </dl>
                      </div>
                    </TabPane>
                    <TabPane tabId="2">
                      <Form id="typeOfContainer2">
                        <Row form className="p-1 mt-2 ">
                          <Col md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-3 col-md-3 pt-1 fontNavbar"
                                for="exampleEmail"
                              >
                                BULTOS
                              </Label>
                              <InputGroup className="col-9 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Nro de bultos"
                                  bsSize="sm"
                                  value={this.state.form[0].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 0,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col xs={12} md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-3 pt-1 col-md-3 fontNavbar"
                                for="examplePassword"
                              >
                                PESO
                              </Label>
                              <InputGroup className="col-9 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Peso"
                                  bsSize="sm"
                                  value={this.state.form[1].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 1,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                                <Input
                                  type="select"
                                  name="select"
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 3,
                                      value: e.target.value || "",
                                    });
                                  }}
                                >
                                  <option defaultValue key="2" value="2">
                                    Kilogramos
                                  </option>
                                  <option key="1" value="1">
                                    Libras
                                  </option>
                                  <option key="4" value="4">
                                    Toneladas
                                  </option>
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={12}>
                            <FormGroup className="form-row">
                              <Label
                                className="col-12 pt-1 col-md-3 fontNavbar"
                                for="examplePassword"
                              >
                                VOLUMEN{" "}
                                <i
                                  id="volumen2"
                                  className="simple-icon-question question-style2"
                                />
                                <UncontrolledTooltip
                                  placement="right"
                                  target="volumen2"
                                >
                                  <div className="color-blue text-justify">
                                    <span style={{ fontWeight: "bold" }}>
                                      Nota:
                                    </span>{" "}
                                    Es el resultado de multplicar largo x ancho
                                    x el alto del producto
                                  </div>
                                  <div className="color-blue text-justify">
                                    <span style={{ fontWeight: "bold" }}>
                                      Ejemplo:
                                    </span>{" "}
                                    Una mesa
                                    <ul>
                                      <li>Largo 1 metro</li>
                                      <li>Ancho 1 metro</li>
                                      <li>Alto 1 metro</li>
                                      <li>Seria 1 x 1 x 1 = 1m3</li>
                                    </ul>
                                  </div>
                                </UncontrolledTooltip>
                              </Label>

                              <InputGroup className="col-12 col-md-9">
                                <Input
                                  type="number"
                                  placeholder="Ingrese Volumen"
                                  bsSize="sm"
                                  value={this.state.form[2].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 2,
                                      value: e.target.value,
                                    });
                                  }}
                                />
                                <Input
                                  type="select"
                                  name="select"
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 4,
                                      value: e.target.value || "",
                                    });
                                  }}
                                >
                                  <option defaultValue key="1" value="1">
                                    Metros cúbicos
                                  </option>
                                  <option key="2" value="2">
                                    Pies cúbicos
                                  </option>
                                </Input>
                              </InputGroup>
                              <FormText
                                className="regulation-text mr-1 ml-1"
                                style={{
                                  backgroundColor: "aliceblue",
                                  display:
                                    this.state.msjActivo === false
                                      ? "none"
                                      : "",
                                }}
                              >
                                {" "}
                                El presupuesto final que arroja la calculadora
                                para cargas mayores a 15m3 esta sujeto a
                                aprobación.
                              </FormText>
                            </FormGroup>
                          </Col>
                          <Col
                            md={12}
                            className="calculatorOpenContainer arial mx-auto  p-1 mb-2"
                          >
                            <span onClick={this.toggleCalculator}>
                              <i
                                style={{
                                  fontWeight: "bold",
                                  verticalAlign: "middle",
                                }}
                                className="mr-1 icon-doubt  "
                              />
                              &nbsp;AYUDA - ¡CALCULA VOLUMEN (m3) AQUI!
                            </span>
                            <div
                              className="color-white"
                              style={{
                                border: "1px solid grey",
                                top: "-22em",
                                height: "28em",
                                zIndex: "1150 !important",
                              }}
                              className={`up-izquierda  tooltip-inner-cont ${
                                this.state.showCalmsj === false
                                  ? "hide-toolkit"
                                  : "show-toolkit-cont"
                              }`}
                            >
                              <div
                                style={{
                                  borderRadius: "2rem",
                                }}
                              >
                                {" "}
                                <i
                                  onClick={() =>
                                    this.setState((prevState) => ({
                                      showCalmsj: !prevState.showCalmsj,
                                    }))
                                  }
                                  className="plusLump  simple-icon-close "
                                  style={{
                                    fontSize: "2em",
                                    position: "absolute",
                                    right: "0.5em",
                                    top: "0.2em",
                                    cursor: "pointer",
                                  }}
                                />
                                <Row
                                  style={{
                                    height: "14em",
                                  }}
                                  className="theme-color-blueHeader arial"
                                >
                                  <Colxx xxs="6" md="12">
                                    {" "}
                                    <div style={{ fontSize: "1.3rem" }}>
                                      ¿Dudas con el VOLUMEN (Metros Cúbicos)?
                                    </div>
                                  </Colxx>
                                  <Colxx xxs="6" md="12">
                                    {" "}
                                    <div>
                                      <i
                                        onClick={this.toggleCalculator}
                                        className="icon-pop-up"
                                      />
                                    </div>
                                  </Colxx>
                                </Row>
                                <Row
                                  style={{ top: "18em", position: "absolute" }}
                                >
                                  <Colxx
                                    onClick={this.toggleCalculator}
                                    xxs="12"
                                    md="12"
                                  >
                                    {" "}
                                    <div
                                      style={{ fontSize: "1.3rem" }}
                                      className="color-blue"
                                    >
                                      ¡Calcula tu Volumen (Metros Cúbicos){" "}
                                      <span className="color-green">
                                        {" "}
                                        AQUI!
                                      </span>
                                    </div>
                                  </Colxx>
                                  <Colxx
                                    xxs="12"
                                    md="12"
                                    sm="12"
                                    className="mt-2 mb-2"
                                  >
                                    <CustomInput
                                      className="color-grey"
                                      style={{ fontSize: "0.7rem" }}
                                      id="SelectedOnce3"
                                      type="checkbox"
                                      onChange={this.SelectedOnce}
                                      label="No volver a mostrar este mensaje"
                                    />
                                  </Colxx>
                                </Row>
                              </div>
                            </div>
                          </Col>
                          <Col md={12}>
                            <Button
                              color="success"
                              onClick={() =>
                                this.setState((prevState) => ({
                                  hide: !prevState.hide,
                                }))
                              }
                              className="btn-style mr-2"
                              size="sm"
                            >
                              VOLVER{" "}
                              <i className="simple-icon-arrow-left-circle" />
                            </Button>
                            <Button
                              color="success"
                              onClick={() => {
                                this.setvolume(0);
                                this.tabs("2");
                                this.changePlaceholder(2);
                              }}
                              className="btn-style-blue"
                              size="sm"
                            >
                              GUARDAR
                            </Button>
                            <span id="bottom"></span>
                          </Col>
                        </Row>
                      </Form>
                    </TabPane>
                  </TabContent>
                </div>
              </Col>
              <Col
                className="typeOfContainer display-large"
                style={{
                  display: this.state.hide2 === false ? "none" : "block",
                }}
              >
                <div className="fontNavbar number-of-containers">
                  <Col md={12} className="navAereo">
                    CONTENIDO DE ENVÍO
                  </Col>
                  <Form id="typeOfContainer3">
                    <Row form className="p-1 mt-2 ">
                      <Col md={12}>
                        <FormGroup className="form-row">
                          <Label
                            className="col-3 col-md-3 pt-1 fontNavbar"
                            for="exampleEmail"
                          >
                            BULTOS
                          </Label>
                          <InputGroup className="col-9 col-md-9">
                            <Input
                              type="number"
                              placeholder="Ingrese Nro de bultos"
                              bsSize="sm"
                              value={this.state.form[0].value}
                              onChange={(e) => {
                                this.handleFormState({
                                  id: 0,
                                  value: e.target.value,
                                });
                              }}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col xs={12} md={12}>
                        <FormGroup className="form-row">
                          <Label
                            className="col-3 pt-1 col-md-3 fontNavbar"
                            for="examplePassword"
                          >
                            PESO
                          </Label>
                          <InputGroup className="col-9 col-md-9">
                            <Input
                              type="number"
                              placeholder="Ingrese Peso"
                              bsSize="sm"
                              value={this.state.form[1].value}
                              onChange={(e) => {
                                this.handleFormState({
                                  id: 1,
                                  value: e.target.value,
                                });
                              }}
                            />
                            <Input
                              type="select"
                              name="select"
                              onChange={(e) => {
                                this.handleFormState({
                                  id: 3,
                                  value: e.target.value || "",
                                });
                              }}
                            >
                              <option defaultValue key="2" value="2">
                                Kilogramos
                              </option>
                              <option key="1" value="1">
                                Libras
                              </option>
                              <option key="4" value="4">
                                Toneladas
                              </option>
                            </Input>
                          </InputGroup>
                        </FormGroup>
                      </Col>

                      <Col md={12}>
                        <FormGroup className="form-row">
                          <Label
                            className="col-12 pt-1 col-md-3 fontNavbar"
                            for="examplePassword"
                          >
                            VOLUMEN{" "}
                            <i
                              id="volumen4"
                              className="simple-icon-question question-style2"
                            />
                          </Label>

                          <UncontrolledTooltip
                            placement="right"
                            target="volumen4"
                          >
                            <div className="color-blue text-justify">
                              <span style={{ fontWeight: "bold" }}>Nota:</span>{" "}
                              Es el resultado de multplicar largo x ancho x el
                              alto del producto
                            </div>
                            <div className="color-blue text-justify">
                              <span style={{ fontWeight: "bold" }}>
                                Ejemplo:
                              </span>{" "}
                              Una mesa
                              <ul>
                                <li>Largo 1 metro</li>
                                <li>Ancho 1 metro</li>
                                <li>Alto 1 metro</li>
                                <li>Seria 1 x 1 x 1 = 1m3</li>
                              </ul>
                            </div>
                          </UncontrolledTooltip>
                          <InputGroup className="col-12 col-md-9">
                            <Input
                              type="number"
                              placeholder="Ingrese Volumen"
                              bsSize="sm"
                              value={this.state.form[2].value}
                              onChange={(e) => {
                                this.handleFormState({
                                  id: 2,
                                  value: e.target.value,
                                });
                              }}
                            />
                            <Input
                              type="select"
                              name="select"
                              onChange={(e) => {
                                this.handleFormState({
                                  id: 4,
                                  value: e.target.value || "",
                                });
                              }}
                            >
                              <option defaultValue key="1" value="1">
                                Metros cúbicos
                              </option>
                              <option key="2" value="2">
                                Pies cúbicos
                              </option>
                            </Input>
                          </InputGroup>
                          <FormText
                            className="mx-auto"
                            style={{
                              fontWeight: "bold",
                              fontSize: "10px",
                            }}
                          >
                            <span>
                              <span className="color-orange">* </span> INGRESE
                              UN VALOR{" "}
                            </span>
                            <span className="color-orange">
                              {" "}
                              MAYOR A 60 KG 0 0.6m3
                            </span>
                          </FormText>
                        </FormGroup>
                      </Col>
                      <Col
                        md={12}
                        className="calculatorOpenContainer arial mx-auto  p-1 mb-2"
                      >
                        <span onClick={this.toggleCalculator}>
                          <i
                            style={{
                              fontWeight: "bold",
                              verticalAlign: "middle",
                            }}
                            className="mr-1 icon-doubt  "
                          />
                          &nbsp;AYUDA - ¡CALCULA VOLUMEN AQUI!
                        </span>
                        <div
                          className="bg-white"
                          style={{
                            border: "1px solid grey",
                            top: "-22em",
                            height: "28em",
                            zIndex: "1150 !important",
                          }}
                          className={`up-izquierda  tooltip-inner-cont ${
                            this.state.showCalmsj === false
                              ? "hide-toolkit"
                              : "show-toolkit-cont"
                          }`}
                        >
                          <div
                            style={{
                              borderRadius: "2rem",
                            }}
                          >
                            {" "}
                            <i
                              onClick={() =>
                                this.setState((prevState) => ({
                                  showCalmsj: !prevState.showCalmsj,
                                }))
                              }
                              className="plusLump  simple-icon-close "
                              style={{
                                fontSize: "2em",
                                position: "absolute",
                                right: "0.5em",
                                top: "0.2em",
                                cursor: "pointer",
                              }}
                            />
                            <Row
                              style={{
                                height: "14em",
                              }}
                              className="theme-color-blueHeader arial"
                            >
                              <Colxx className="mt-1" xxs="6" md="12">
                                {" "}
                                <div style={{ fontSize: "1.3rem" }}>
                                  ¿Dudas con el VOLUMEN ?
                                </div>
                              </Colxx>
                              <Colxx xxs="6" md="12">
                                {" "}
                                <div>
                                  <i
                                    onClick={this.toggleCalculator}
                                    className="icon-pop-up"
                                  />
                                </div>
                              </Colxx>
                            </Row>
                            <Row style={{ top: "18em", position: "absolute" }}>
                              <Colxx
                                onClick={this.toggleCalculator}
                                xxs="12"
                                md="12"
                              >
                                {" "}
                                <div
                                  style={{ fontSize: "1.3rem" }}
                                  className="color-blue"
                                >
                                  ¡Calcula tu Volumen{" "}
                                  <span className="color-green"> AQUI!</span>
                                </div>
                              </Colxx>
                              <Colxx
                                xxs="12"
                                md="12"
                                sm="12"
                                className="mt-2 mb-2"
                              >
                                <CustomInput
                                  className="color-grey"
                                  style={{ fontSize: "0.7rem" }}
                                  id="SelectedOnce4"
                                  type="checkbox"
                                  onChange={this.SelectedOnce}
                                  label="No volver a mostrar este mensaje"
                                />
                              </Colxx>
                            </Row>
                          </div>
                        </div>
                      </Col>

                      <Col md={12}>
                        <Button
                          color="success"
                          onClick={() =>
                            this.setState((prevState) => ({
                              hide2: !prevState.hide2,
                            }))
                          }
                          className="btn-style mr-2"
                          size="sm"
                        >
                          VOLVER <i className="simple-icon-arrow-left-circle" />
                        </Button>
                        <Button
                          color="success"
                          onClick={() => {
                            this.setvolume(0);
                            this.tabs("2");
                            this.changePlaceholder(2);
                          }}
                          className="btn-style-blue"
                          size="sm"
                        >
                          GUARDAR
                        </Button>
                        <span id="bottom"></span>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </Col>
              {this.state.showLoading ? (
                <Colxx
                  xxs="12"
                  md="7"
                  sm="12"
                  className="NoPadding mx-auto mt-2 "
                  style={{ display: "block" }}
                >
                  <div
                    className=" mx-auto "
                    xxs="12"
                    md="7"
                    sm="12"
                    style={{ fontWeight: "bold" }}
                  >
                    CALCULANDO
                  </div>
                  <div
                    className="   mx-auto loading2"
                    xxs="12"
                    md="7"
                    sm="12"
                  ></div>
                </Colxx>
              ) : (
                ``
              )}{" "}
              {this.state.showResult == 0 ? (
                ""
              ) : (
                <Colxx
                  xxs="12"
                  md="8"
                  sm="12"
                  className="arial color-orange NoPadding mx-auto"
                >
                  {this.state.showResult}
                </Colxx>
              )}
              {this.state.resultContainer &&
              this.state.result > 0 &&
              this.state.portOriginSelected.length > 0 &&
              this.state.portDestinySelected.length > 0 ? (
                <Colxx
                  xxs="12"
                  md="12"
                  sm="12"
                  xl="8"
                  lg="12"
                  className="NoPadding mx-auto"
                >
                  <Row>
                    <Colxx xxs="12" md="12" className="mx-auto mb-3 ">
                      <span className="step-style">2.1 Elige una opcion</span>{" "}
                    </Colxx>
                    <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                      <Alert
                        color="danger"
                        isOpen={this.state.isOpenAlertOpcion}
                        toggle={this.toggleAlertOpcion}
                      >
                        Debe selecionar una opción.
                      </Alert>
                    </Col>
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="NoPadding mx-auto"
                    >
                      {" "}
                      <FormGroup
                        onClick={() => {
                          this.toggleModalPreview("1");
                        }}
                      >
                        <Label check>
                          <div
                            style={{ fontWeight: "bold" }}
                            className="divAvisoRuta color-white text-center m-2 mx-auto theme-color-blue"
                          >
                            <div>OPCION 1</div>
                            <div style={{ display: "flex" }}>
                              <CustomInput
                              id="opcion1"
                                type="radio"
                                value="1"
                                name="radio1"
                                onChange={(e) => {
                                  this.setState({
                                    OpcionValue: e.target.value || "",
                                  });
                                }}
                              />
                              AGREGAR SERVICIOS DE ADUANA EN DESTINO
                            </div>
                          </div>
                        </Label>
                      </FormGroup>
                    </Colxx>
                    <Colxx
                      xxs="12"
                      md="6"
                      sm="12"
                      className="NoPadding mx-auto"
                    >
                      {" "}
                      <FormGroup
                        onClick={() => {
                          this.toggleModalPreview("2");
                        }}
                      >
                        <Label check>
                          <div
                            style={{ fontWeight: "bold" }}
                            className="divAvisoRuta m-2 mx-auto text-center theme-color-white"
                          >
                            <div>OPCION 2</div>
                            <div style={{ display: "flex" }}>
                              <CustomInput
                                    id="opcion2"
                                type="radio"
                                value="2"
                                name="radio1"
                                onChange={(e) => {
                                  this.setState({
                                    OpcionValue: e.target.value || "",
                                  });
                                }}
                              />
                              NO AGREGAR SERVICIOS **SOLO DESEO FLETE**
                            </div>
                          </div>
                        </Label>
                      </FormGroup>
                    </Colxx>
                  </Row>
                </Colxx>
              ) : (
                ""
              )}
              <span id="link5"></span>
              <TimeLine data={this.state.resultContainer} />
            </Colxx>
          </Row>
        </div>

        <Modal
          isOpen={this.state.Calculator}
          toggle={this.toggleCalculator}
          backdrop={true}
          size="lg"
          style={{ borderRadius: "1.1rem" }}
        >
          <ModalBody style={{ padding: "1em 0px 1em 0px" }}>
            <i
              onClick={this.toggleCalculator}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
              }}
            />

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              id="alert"
              className="font mt-2 mb-2"
              style={{ textAlign: "center" }}
            >
              CALCULADORA VOLUMETRICA
            </Colxx>
            {this.state.toggleAlert ? (
              <div
                style={{ position: "sticky !important" }}
                className="alert alert-danger text-center arial  mx-auto"
                role="alert"
              >
                Debe Agregar Medidas O Volver.
              </div>
            ) : (
              ``
            )}

            <React.Suspense fallback={renderLoader()}>
              <Calculadora />
            </React.Suspense>
            <Col xs={12} md={12}>
              <Button
                size="sm"
                className="btn-style mt-2 float-left"
                color="success"
                onClick={this.toggleCalculator}
              >
                VOLVER <i className="simple-icon-arrow-left-circle" />
              </Button>
            </Col>
            <Col xs={12} md={12}>
              <Button
                size="sm"
                className="btn-style-blue mt-2 float-right"
                color="success"
                onClick={this.setValues}
              >
                AGREGAR CALCULO
              </Button>
            </Col>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.setValuesMsj}
          size="md"
          className="VolumenModal "
          style={{ borderRadius: "1.1rem", marginTop: "65px !important" }}
        >
          <ModalBody style={{ padding: "0rem" }}>
            <i
              onClick={() => this.setvolume(1)}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="text-center pt-2 pb-2 arial color-white"
              style={{
                backgroundColor: "#0d5084",
                fontSize: "2em",
              }}
            >
              ATENCIÓN
            </Colxx>
            <Row>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="mx-auto pb-2 pt-2 arial"
                style={{ textAlign: "justify" }}
              >
                <span
                  style={{
                    fontSize: "1rem",
                    color: "#0d5084",
                  }}
                >
                  Los siguientes campos fueron dejados con su valor por defecto:
                  <br />
                  <ul className="color-orange">
                    {this.state.form[0].value == 0 ? (
                      <div>
                        <li>Bultos.</li>{" "}
                      </div>
                    ) : (
                      ``
                    )}

                    {this.state.form[1].value == 0 ? (
                      <div>
                        <li>Peso. </li>{" "}
                      </div>
                    ) : (
                      ``
                    )}

                    {this.state.form[2].value == 0 ? (
                      <div>
                        {" "}
                        <li>Volumen.</li>{" "}
                      </div>
                    ) : (
                      ``
                    )}
                  </ul>
                </span>
              </Colxx>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="p-1 arial color-white"
                style={{
                  fontSize: "1rem",
                  textAlign: "center",
                  backgroundColor: "#0d5084",
                }}
              >
                <span>
                  Los campos vacíos se COMPLETARÁN AUTOMÁTICAMENTE con un VALOR
                  de 1.
                </span>
              </Colxx>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="mx-auto "
              style={{ textAlign: "center" }}
            >
              <Button
                color="success"
                onClick={() => this.setvolume(1)}
                className="btn-style mr-2"
                size="sm"
              >
                VOLVER <i className="simple-icon-arrow-left-circle" />
              </Button>
              <Button
                color="success"
                onClick={() => this.setvolume(2)}
                className="btn-style-blue"
                size="sm"
              >
                CONTINUAR <i className="simple-icon-arrow-right-circle" />
              </Button>
            </Colxx>
          </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.showBoxmb1}
          backdrop={true}
          style={{ borderRadius: "1.8rem" }}
          className="NoPadding atencion"
          style={{ padding: "0rem" }}
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={() =>
                this.setState(() => ({
                  showBoxmb1: false,
                }))
              }
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.2em",
                cursor: "pointer",
              }}
            />
            <Row>
              <div>
                <Card
                  style={{
                    borderRadius: "2rem",
                  }}
                >
                  <CardHeader
                    className="theme-color-blueHeader arial"
                    style={{
                      padding: "0px",
                    }}
                  >
                    20' STANDARD
                  </CardHeader>

                  <CardBody
                    style={{
                      padding: "0px",
                    }}
                  >
                    <CardImg
                      top
                      width="1em"
                      src="/assets/img/contenedores/20std.png"
                      alt="1em"
                    />
                    <CardText>
                      <Table
                        style={{
                          textAlign: "center",
                          fontSize: "0.7rem",
                        }}
                        striped
                      >
                        <tbody style={{}}>
                          <tr>
                            <th scope="row" colSpan="3">
                              PESO MAX. PERMITIDO
                            </th>
                            <td>26 TONELADAS</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="3">
                              VOL. MAX. CARGABLE
                            </th>
                            <td>33 M3</td>
                          </tr>
                          <tr>
                            <th
                              className="theme-color-blue"
                              scope="row"
                              colSpan="4"
                            >
                              MEDIDAS INTERNAS CONTENEDOR
                            </th>
                          </tr>
                          <tr>
                            <td scope="row" colSpan="2">
                              LARGO
                            </td>
                            <td>ANCHO</td>
                            <td>ALTO</td>
                          </tr>
                          <tr>
                            <td scope="row" colSpan="2">
                              5.89 MTS
                            </td>
                            <td>2.35 MTS</td>
                            <td>2.38 MTS</td>
                          </tr>
                        </tbody>
                      </Table>
                    </CardText>
                  </CardBody>
                  <CardFooter className="theme-color-redFooter">
                    {" "}
                    <div>
                      <span style={{ fontWeight: "bold" }}>Nota:</span> El peso
                      máximo permitido variara de acuerdo a las normativas
                      establecidas por cada naviera.{" "}
                    </div>
                  </CardFooter>
                </Card>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.showBoxmb2}
          backdrop={true}
          style={{ borderRadius: "1.8rem" }}
          className="NoPadding atencion"
          style={{ padding: "0rem" }}
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={() =>
                this.setState(() => ({
                  showBoxmb2: false,
                }))
              }
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.2em",
                cursor: "pointer",
              }}
            />

            <Row>
              <div>
                <Card
                  style={{
                    borderRadius: "2rem",
                  }}
                >
                  <CardHeader
                    className="theme-color-blueHeader arial"
                    style={{
                      padding: "0px",
                    }}
                  >
                    {" "}
                    40' STANDARD
                  </CardHeader>

                  <CardBody
                    style={{
                      padding: "0px",
                    }}
                  >
                    <CardImg
                      top
                      width="1em"
                      src="/assets/img/contenedores/40std.png"
                      alt="1em"
                    />
                    <CardText>
                      <Table
                        style={{
                          textAlign: "center",
                          fontSize: "0.7rem",
                        }}
                        striped
                      >
                        <tbody style={{}}>
                          <tr>
                            <th scope="row" colSpan="3">
                              PESO MAX. PERMITIDO
                            </th>
                            <td>26 TONELADAS</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="3">
                              VOL. MAX. CARGABLE
                            </th>
                            <td>67 M3</td>
                          </tr>
                          <tr>
                            <th
                              className="theme-color-red"
                              scope="row"
                              colSpan="4"
                            >
                              MEDIDAS INTERNAS CONTENEDOR
                            </th>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="2">
                              LARGO
                            </th>
                            <td>ANCHO</td>
                            <td>ALTO</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="2">
                              12.03 MTS
                            </th>
                            <td>2.35 MTS</td>
                            <td>2.38 MTS</td>
                          </tr>
                        </tbody>
                      </Table>
                    </CardText>
                  </CardBody>
                  <CardFooter className="theme-color-redFooter">
                    {" "}
                    <div>
                      <span style={{ fontWeight: "bold" }}>Nota:</span> El peso
                      máximo permitido variara de acuerdo a las normativas
                      establecidas por cada naviera.{" "}
                    </div>
                  </CardFooter>
                </Card>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.showBoxmb3}
          backdrop={true}
          style={{ borderRadius: "1.8rem" }}
          className="NoPadding atencion"
          style={{ padding: "0rem" }}
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={() =>
                this.setState(() => ({
                  showBoxmb3: false,
                }))
              }
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.2em",
                cursor: "pointer",
              }}
            />
            <Row>
              <div>
                <Card
                  style={{
                    borderRadius: "2rem",
                  }}
                >
                  <CardHeader
                    className="theme-color-blueHeader arial"
                    style={{
                      padding: "0px",
                    }}
                  >
                    {" "}
                    40' HC
                  </CardHeader>
                  <CardBody
                    style={{
                      padding: "0px",
                    }}
                  >
                    <CardImg
                      top
                      width="1em"
                      src="/assets/img/contenedores/40hq.png"
                      alt="1em"
                    />
                    <CardText>
                      <Table
                        style={{
                          textAlign: "center",
                          fontSize: "0.7rem",
                        }}
                        striped
                      >
                        <tbody style={{}}>
                          <tr>
                            <th scope="row" colSpan="3">
                              PESO MAX. PERMITIDO
                            </th>
                            <td>26 TONELADAS</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="3">
                              VOL. MAX. CARGABLE
                            </th>
                            <td>76 M3</td>
                          </tr>
                          <tr>
                            <th
                              className="theme-color-green"
                              scope="row"
                              colSpan="4"
                            >
                              MEDIDAS INTERNAS CONTENEDOR
                            </th>
                          </tr>
                          <tr>
                            <td scope="row" colSpan="2">
                              LARGO
                            </td>
                            <td>ANCHO</td>
                            <td>ALTO</td>
                          </tr>
                          <tr>
                            <td scope="row" colSpan="2">
                              12.03 MTS
                            </td>
                            <td>2.35 MTS</td>
                            <td>2.69 MTS</td>
                          </tr>
                        </tbody>
                      </Table>
                    </CardText>
                  </CardBody>
                  <CardFooter className="theme-color-redFooter">
                    {" "}
                    <div>
                      <span style={{ fontWeight: "bold" }}>Nota:</span> El peso
                      máximo permitido variara de acuerdo a las normativas
                      establecidas por cada naviera.{" "}
                    </div>
                  </CardFooter>
                </Card>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.showBoxmb4}
          backdrop={true}
          style={{ borderRadius: "1.8rem" }}
          className="NoPadding atencion"
          style={{ padding: "0rem" }}
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={() =>
                this.setState(() => ({
                  showBoxmb4: false,
                }))
              }
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.2em",
                cursor: "pointer",
              }}
            />
            <Row>
              <div>
                <Card
                  style={{
                    borderRadius: "2rem",
                  }}
                >
                  <CardHeader
                    className="theme-color-blueHeader arial"
                    style={{
                      padding: "0px",
                    }}
                  >
                    {" "}
                    40' NOR
                  </CardHeader>
                  <CardBody
                    style={{
                      padding: "0px",
                    }}
                  >
                    <CardImg
                      top
                      width="1em"
                      src="/assets/img/contenedores/40nor.png"
                      alt="1em"
                    />
                    <CardText>
                      <Table
                        style={{
                          textAlign: "center",
                          fontSize: "0.7rem",
                        }}
                        striped
                      >
                        <tbody style={{}}>
                          <tr>
                            <th scope="row" colSpan="3">
                              PESO MAX. PERMITIDO
                            </th>
                            <td>26 TONELADAS</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="3">
                              VOL. MAX. CARGABLE
                            </th>
                            <td>48 M3</td>
                          </tr>
                          <tr>
                            <th
                              className="theme-color-blue"
                              scope="row"
                              colSpan="4"
                            >
                              MEDIDAS EXTERNAS CONTENEDOR
                            </th>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="2">
                              LARGO
                            </th>
                            <td>ANCHO</td>
                            <td>ALTO</td>
                          </tr>
                          <tr>
                            <th scope="row" colSpan="2">
                              12.03 MTS
                            </th>
                            <td>2.35 MTS</td>
                            <td>2.38 MTS</td>
                          </tr>
                        </tbody>
                      </Table>
                    </CardText>
                  </CardBody>
                  <CardFooter className="theme-color-redFooter">
                    {" "}
                    <div>
                      <span style={{ fontWeight: "bold" }}>Nota:</span> Es un
                      contenedor con un equipo de refrigeración interno que esta
                      apagado, el cual se utiliza para para cargas de menor
                      volumen ya que tienes 10 M3 menos que el 40 ST debido al
                      equipo que tiene interno.{" "}
                    </div>
                  </CardFooter>
                </Card>
              </div>
            </Row>
          </ModalBody>
        </Modal>

        <React.Suspense fallback={renderLoader()}>
          <ModalSignInRuta
            active={this.state.IsVisiblePreview}
            toggle={this.toggleModalPreview}
            redirect={this.redirect}
            bandera={this.state.resultContainer}
          />
        </React.Suspense>
        <Beforeunload onBeforeunload={() => "Si sales perderas los datosa no guardados"} />
        <BotChat />
      </Fragment>
    );
  }
}

export default withRouter(ruta);

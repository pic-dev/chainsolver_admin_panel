import React, { Component, Fragment } from "react";
import {
  Form,
  Col,
  Row,
  Input,
  Modal,
  ModalBody,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Table,
  UncontrolledTooltip,
} from "reactstrap";
import { pdf } from "@react-pdf/renderer";
import Button from "@material-ui/core/Button";
import Spinner from "Components/spinner";
import { Link, Redirect, withRouter } from "react-router-dom";
import "react-bootstrap-typeahead/css/Typeahead.css";
import axios from "axios";
import scrollIntoView from "scroll-into-view-if-needed";
import { saveAs } from "file-saver";
import { Colxx } from "Components/CustomBootstrap";
import Whatsapp from "Util/Whatsapp";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import GetAppIcon from "@material-ui/icons/GetApp";
import RestoreIcon from "@material-ui/icons/Restore";
import {
  getCurrentTime,
  getDateWithFormat,
  errores,
  validate,
  telefonoValid,
  empresaValid,
  fechaFormateada,
  taxesAlertSM,
  numero,
} from "Util/Utils";
import ShareButtons from "Util/shareButtons";
import { PageView } from "Util/tracking";
import { firebaseConf, googleAuthProvider, persistencia } from "Util/Firebase";
import { Title } from "Util/helmet";
import BotChat from "Util/chat";
const ModalQuoteResumen = React.lazy(() =>
  import("Util/Modals/modalQuoteResumen")
);
const ModalComment = React.lazy(() => import("Util/Modals/modalComment"));

const ModalInfoPaso4 = React.lazy(() => import("Util/Modals/modalInfoPaso4"));

if (localStorage.transporte == undefined) {
  localStorage.check = true;
  localStorage.check1 = true;
  localStorage.check2 = true;
  localStorage.check4 = true;
}

var gastos;
var Impuestos;
var Transporte;
var Seguro;

if (!JSON.parse(localStorage.check) == false) {
  gastos = "SI";
} else {
  if (
    localStorage.tipocon == "Contenedor compartido" &&
    localStorage.paisDestino == "VENEZUELA"
  ) {
    gastos = "SI";
  } else {
    gastos = "NO";
  }
}
if (!JSON.parse(localStorage.check1) == false) {
  Transporte = "SI";
} else {
  if (
    localStorage.tipocon == "Contenedor compartido" &&
    localStorage.paisDestino == "VENEZUELA"
  ) {
    Transporte = "SI";
  } else {
    Transporte = "NO";
  }
}
if (!JSON.parse(localStorage.check2) == false) {
  Impuestos = "SI";
} else {
  if (
    localStorage.tipocon == "Contenedor compartido" &&
    localStorage.paisDestino == "VENEZUELA"
  ) {
    Impuestos = "SI";
  } else {
    Impuestos = "NO";
  }
}
if (!JSON.parse(localStorage.check) == false) {
  Seguro = "SI";
} else {
  Seguro = "NO";
}
// code block
let timer;
class presupuesto extends Component {
  constructor(props) {
    super(props);

    this.signIn = this.signIn.bind(this);
    this.desglose = this.desglose.bind(this);
    this.toggleinfoPaso4 = this.toggleinfoPaso4.bind(this);
    this.taxesInfo = this.taxesInfo.bind(this);
    this.checkbox4 = this.checkbox4.bind(this);
    this.clearStorage = this.clearStorage.bind(this);
    this.token = this.token.bind(this);
    this.commentary = this.commentary.bind(this);
    this.sendCommentary = this.sendCommentary.bind(this);
    this.SelectedOnceImportant = this.SelectedOnceImportant.bind(this);
    this.SelectedOnceComment = this.SelectedOnceComment.bind(this);
    this.closeComment = this.closeComment.bind(this);
    this.closedoubt = this.closedoubt.bind(this);
    this.handleBoxToggle = this.handleBoxToggle.bind(this);
    this.handleFormState = this.handleFormState.bind(this);
    this.cotizacion = this.cotizacion.bind(this);
    this.ruc = this.ruc.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.budgetstart = this.budgetstart.bind(this);
    this.getTotal = this.getTotal.bind(this);
    this.getToken = this.getToken.bind(this);

    this.state = {
      Signup: false,
      signIn: false,
      error: "",
      cotizacion: false,
      signInChoose: true,
      ruc: [],
      fechas_vigencia: [],
      showBox: false,
      showLoading: false,
      desgloseKey: [],
      desglose: false,
      desgloseValues: [],
      resumen: [],
      totalIGVCostos: [],
      IsVisiblesignIn: false,
      isOpen: false,
      infoPaso4: false,
      taxesInfo: false,
      comment: false,
      showMessage: false,
      commentEmpty: false,
      check:
        localStorage.tipocon == "Contenedor compartido" &&
        localStorage.paisDestino == "VENEZUELA"
          ? JSON.parse(localStorage.check)
          : !JSON.parse(localStorage.check),
      check1:
        localStorage.tipocon == "Contenedor compartido" &&
        localStorage.paisDestino == "VENEZUELA"
          ? JSON.parse(localStorage.check1)
          : !JSON.parse(localStorage.check1),
      check2:
        localStorage.tipocon == "Contenedor compartido" &&
        localStorage.paisDestino == "VENEZUELA"
          ? JSON.parse(localStorage.check2)
          : !JSON.parse(localStorage.check2),
      check3:
        localStorage.tipocon == "Contenedor compartido" &&
        localStorage.paisDestino == "VENEZUELA"
          ? JSON.parse(localStorage.check3)
          : !JSON.parse(localStorage.check3),
      check4: JSON.parse(localStorage.check4),
      portOriginSelected: [],
      portDestinySelected: [],
      productPermSelected: [],
      provinceSelected: [],
      districtSelected: [],
      province: [],
      district: [],
      portOrigin: [],
      token: [],
      portDestiny: [],
      productPerm: [],
      result: [],
      subtotalResumen: [],
      taxesResumen: [],
      TotalResumen: [],
      expand: true,
      animated: false,
      animated2: false,
      showArrowmsj: false,
      arrowDownload: false,
      Commentform: [
        { Label: "paso1", value: "" },
        { Label: "paso2", value: "" },
        { Label: "paso3", value: "" },
        { Label: "paso4", value: "" },
        { Label: "recomendarias", value: "" },
        { Label: "inversion", value: "" },
        { Label: "adicional", value: "sin comentarios" },
        { Label: "costo", value: "" },
      ],
      form: [
        { Label: "rate", value: "" },
        { Label: "origin", value: "" },
        { Label: "destination", value: "" },
        { Label: "freightType", value: "" },
        { Label: "container2", value: "" },
        { Label: "container2", value: "" },
      ],
    };
  }
  componentDidMount() {
    localStorage.page = "4";
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
    if (localStorage.transporte == undefined || sessionStorage.usuario == "1") {
    } else {
      if (localStorage.transporte == undefined) {
        localStorage.check = true;
        localStorage.check1 = true;
        localStorage.check2 = true;
        localStorage.check4 = true;
      } else {
        this.getToken();
      }
      this.setState({
        showLoading: true,
      });
    }
  }

  seterror = (e) => {
    this.setState(() => ({
      error: e,
    }));
  };
  socialLogin = () => {
    this.setState({
      error: "",
    });
    firebaseConf
      .auth()
      .setPersistence(persistencia)
      .then(() => {
        (async () => {
          await firebaseConf
            .auth()
            .signInWithPopup(googleAuthProvider)
            .then((result) => {
              /* Event(
                "GoogleLoginHeader",
                "Usuario ingresó con botton de google",
                "SERVICIOS"
              );*/
              let user = [
                [result.additionalUserInfo.profile.id],
                [result.additionalUserInfo.profile.name],
                [result.additionalUserInfo.profile.email],
                [result.additionalUserInfo.profile.picture],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              sessionStorage.usuario = "2";
              timer = setTimeout(() => {
                this.submitForm();
              }, 2000);
            })
            .catch((error) => {
              let user = [
                ["PIC"],
                ["Invitado"],
                ["Invitado"],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              sessionStorage.usuario = "2";
              timer = setTimeout(() => {
                this.submitForm();
              }, 2000);
            });
        })();
      })
      .catch((error) => {});
  };

  submitForm() {
    // prevent page reload
    var blob;
    if (screen.width > 1024) {
      this.setState({
        arrowDownload: true,
      });
    }

    const formatter = new Intl.NumberFormat("de-DE");
    var taxesResumen;
    var TotalResumen;
    var subtotalResumen;
    var totalIGVCostos;
    if (this.state.totalIGVCostos.length !== 0) {
      totalIGVCostos =
        localStorage.paisDestino == "PERU" ? this.state.totalIGVCostos : 0;
    }
    if (
      this.state.TotalResumen.length > 0 &&
      this.state.subtotalResumen.length > 0
    ) {
      //con seguro
      subtotalResumen = this.state.subtotalResumen.map(function (item) {
        return formatter.format(item);
      });

      //impuestos
      if (this.state.taxesResumen.length == 0) {
        taxesResumen = ["No cotizado", "No cotizado", "No cotizado"];
      } else {
        taxesResumen = this.state.taxesResumen.map(function (item) {
          return formatter.format(item);
        });
      }
      let TotalResumenprevio;
      //monto total, se valida si hay impuestos o no
      if (this.state.TotalResumen.length !== 0) {
        if (this.state.TotalResumen !== this.state.subtotalResumen) {
          TotalResumen = this.state.TotalResumen.map(function (item) {
            return formatter.format(Number(item) + Number(totalIGVCostos));
          });
        } else {
          TotalResumenprevio = [
            [this.state.subtotalResumen],
            [this.state.subtotalResumen],
            [this.state.subtotalResumen],
          ];
          TotalResumen = TotalResumenprevio.map(function (item) {
            return formatter.format(Number(item) + Number(totalIGVCostos));
          });
        }
      } else {
        TotalResumenprevio = [
          [this.state.subtotalResumen],
          [this.state.subtotalResumen],
          [this.state.subtotalResumen],
        ];
        TotalResumen = TotalResumenprevio.map(function (item) {
          return formatter.format(Number(item) + Number(totalIGVCostos));
        });
      }

      import("Util/Pdf.js").then(({ default: Pdf }) => {
        (async () => {
          blob = await pdf(
            <Pdf
              taxesResumen={taxesResumen}
              TotalResumen={TotalResumen}
              subtotalResumen={subtotalResumen}
              totalIGVCostos={totalIGVCostos}
            />
          ).toBlob();
          if (blob) {
            this.setState({
              cotizacion: false,
            });
            saveAs(blob, "Presupuesto-" + this.state.token + ".pdf");
            timer = setTimeout(() => {
              this.setState({
                arrowDownload: false,
              });
            }, 2000);
          }
        })();
      });
    }
  }

  componentWillUnmount() {
    clearTimeout(timer);
  }
  handleBoxToggle(e) {
    this.setState({ showBox: e });
  }
  signIn() {
    this.setState((ps) => ({
      IsVisiblesignIn: !ps.IsVisiblesignIn,
    }));
  }
  desglose() {
    this.setState((ps) => ({
      desglose: !ps.desglose,
    }));
  }
  cotizacion() {
    this.setState((ps) => ({
      cotizacion: !ps.cotizacion,
    }));
  }
  ruc = (e) => {
    e.preventDefault();
    const { ruc, telefono, empresa, usuario } = e.target.elements;
    let regex;
    let regex2;
    let name = ruc.value;
    var cedula;
    var rucValid;
    var alerta;
    switch (localStorage.paisDestino) {
      case "PERU":
        regex = /^\d{8}(?:[-\s]\d{4})?$/;
        cedula = regex.test(name);
        rucValid = validate(name);
        alerta =
          "Ingrese un formato valido EJEMPLO:  DNI 41135049 o RUC 10602647880";

        break;
      case "PANAMA":
        regex = /^(PE|E|N|[23456789](?:AV|PI)?|1[0123]?(?:AV|PI)?)-(\d{1,4})-(\d{1,6})$/i;
        regex2 = /^(\S{1,11})-(\d{1,4})-(\S{1,11})$/i;
        rucValid = regex2.test(name);
        cedula = regex.test(name);
        alerta =
          "Ingrese un formato valido EJEMPLO:  Cedula 1-234-567  o  RUC 2256095-1-782426DV61";

        break;
      case "VENEZUELA":
        regex = /^([VEJPG]{1})-([0-9]{8})-([0-9]{1})$/;
        rucValid = regex.test(name);
        alerta = "Ingrese un formato valido EJEMPLO: RIF J-45699654-1";

        break;

      default:
      // code block
    }
    var telefonoValue = true;
    var empresaValue = true;
    if (telefonoValid(telefono.value) && empresaValid(empresa.value)) {
      telefonoValue = true;
      empresaValue = true;
    } else if (empresa.value.length > 0) {
      if (empresaValid(empresa.value)) {
        empresaValue = empresaValid(empresa.value);
      } else {
        empresaValue = false;
        alerta = "Verifique nombre de empresa";
      }
    } else if (telefono.value.length > 0) {
      if (telefonoValid(telefono.value)) {
        telefonoValue = telefonoValid(telefono.value);
      } else {
        telefonoValue = false;

        alerta = "Verifique numero de telefono";
      }
    } else {
      telefonoValue = true;
      empresaValue = true;
    }
    let data = JSON.parse(sessionStorage.getItem("user"));
    if (sessionStorage.usuario == "1" || data[1] == "Invitado") {
      if (cedula || rucValid) {
        if (telefonoValue && empresaValue) {
          if (!this.state.signInChoose) {
            if (usuario.value) {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              localStorage.empresa = empresa.value;
              localStorage.telefono = telefono.value;
              localStorage.ruc = ruc.value;
              this.setState({
                ruc: ruc.value,
              });
              sessionStorage.setItem("user", JSON.stringify(user));
              this.submitForm();
              timer = setTimeout(() => {
                window.location.reload();
              }, 4000);
            } else {
              alert("Debe completar el correo electronico");
            }
          } else {
            localStorage.empresa = empresa.value;
            localStorage.telefono = telefono.value;
            localStorage.ruc = ruc.value;
            this.setState({
              ruc: ruc.value,
            });
            this.socialLogin();
          }
        } else {
          alert(alerta);
        }
      } else {
        alert(alerta);
      }
    } else {
      if (cedula || rucValid) {
        if (telefonoValue && empresaValue) {
          localStorage.empresa = empresa.value;
          localStorage.telefono = telefono.value;
          localStorage.ruc = ruc.value;
          this.setState({
            ruc: ruc.value,
          });
          this.submitForm();
        } else {
          alert(alerta);
        }
      } else {
        alert(alerta);
      }
    }
  };
  SelectedOnceImportant() {
    sessionStorage.ImportantNone = "1";
  }
  SelectedOnceComment() {
    sessionStorage.CommentNone = "1";
  }
  token(fn) {
    this.setState({
      token: fn,
    });
  }
  clearStorage() {
    localStorage.clear();
  }
  checkbox4() {
    this.setState((prevState) => ({
      check4: !prevState.check4,
    }));
    localStorage.check4 = this.state.check4;
  }

  budgetstart = () => {
    this.setState({
      showLoading: false,
      IsPdf: true,
    });
    if (screen.width < 768) {
      this.setState({
        showArrowmsj: true,
      });
      timer = setTimeout(() => {
        this.setState({
          showArrowmsj: false,
        });
      }, 6000);
    }

    if (sessionStorage.CommentNone !== "1") {
      timer = setTimeout(() => {
        this.setState((ps) => ({ comment: true }));
      }, 200000);
    }
  };
  arrowScroll = () => {
    let scroll = document.getElementById("bottom");
    scrollIntoView(scroll, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
  };

  getToken() {
    let value;
    let value1;
    let value2;
    let value3;
    let value4;
    let value5;
    let value6;
    let value7;
    let value8;
    let value9;
    let weightUnit;
    let weight;
    let volumen;
    let volumentUnit;
    let lumps;
    let urlToken;
    let datos;
    var tipoToken;
    let url2;
    let ur;
    let datos1;
    var token1;

    if (localStorage.token == undefined) {
      switch (localStorage.tipocon) {
        case "Contenedor completo":
          value = localStorage.origen;
          value1 = localStorage.destino;
          value2 = localStorage.monto;
          value3 = localStorage.distrito;
          value4 = localStorage.value4;
          value5 = localStorage.value5;
          value6 = localStorage.value6;
          value7 = localStorage.value7;
          value8 = localStorage.contenedores;
          value9 = localStorage.value8;
          urlToken = this.props.api + "maritimo_v2/get_ctz_fcl_token";

          datos = {
            datos: [
              value.toString(),
              value1.toString(),
              value8.toString(),
              value2.toString(),
              value3.toString(),
              value4,
              value5,
              value6,
              value7,
              value9,
            ],
          };
          break;
        case "Contenedor compartido":
          value = localStorage.origen;
          value1 = localStorage.destino;
          weightUnit = localStorage.contenedores;
          weight = localStorage.weight;
          volumen = localStorage.volumen;
          volumentUnit = localStorage.volumentUnit;
          lumps = localStorage.lumps;
          value2 = localStorage.monto;
          value3 = localStorage.distrito;
          value4 = localStorage.value4;
          value5 = localStorage.value5;
          value6 = localStorage.value6;
          value7 = localStorage.value7;
          value8 = localStorage.value8;
          urlToken = this.props.api + "maritimo_v2/get_ctz_lcl_token";

          datos = {
            datos: [
              value.toString(),
              value1.toString(),
              weight.toString(),
              weightUnit.toString(),
              volumen.toString(),
              volumentUnit.toString(),
              lumps.toString(),
              value2.toString(),
              value3.toString(),
              value4,
              value5,
              value6,
              value7,
              value8,
            ],
          };
          break;
        case "Aereo":
          value = localStorage.origen;
          value1 = localStorage.destino;
          weightUnit = localStorage.contenedores;
          weight = localStorage.weight;
          volumen = localStorage.volumen;
          volumentUnit = localStorage.volumentUnit;
          lumps = localStorage.lumps;
          value2 = localStorage.monto;
          value3 = localStorage.distrito;
          value4 = localStorage.value4;
          value5 = localStorage.value5;
          value6 = localStorage.value6;
          value7 = localStorage.value7;
          value8 = localStorage.value8;
          urlToken = this.props.api + "aereo_v2/get_ctz_aereo_token";
          datos = {
            datos: [
              value.toString(),
              value1.toString(),
              weight.toString(),
              weightUnit.toString(),
              volumen.toString(),
              volumentUnit.toString(),
              lumps.toString(),
              value2.toString(),
              value3.toString(),
              value4,
              value5,
              value6,
              value7,
              value8,
            ],
          };
          break;

        default:
      }
      axios
        .post(urlToken, datos)
        .then((res) => {
          const response = res.data.data.r_dat;
          let token = Object.values(response[0]);

          token1 = token.toString();

          switch (localStorage.tipocon) {
            case "Contenedor completo":
              tipoToken = "-FCL";
              url2 = this.props.api + "maritimo_v2/get_ctz_fcl_resumen";
              ur = this.props.api + "maritimo_v2/get_busca_cot_fcl";
              break;
            case "Contenedor compartido":
              tipoToken = "-LCL";
              url2 = this.props.api + "maritimo_v2/get_ctz_lcl_resumen";
              ur = this.props.api + "maritimo_v2/get_busca_cot_lcl";
              break;
            case "Aereo":
              tipoToken = "-AER";
              url2 = this.props.api + "aereo_v2/get_ctz_aereo_resumen";
              ur = this.props.api + "aereo_v2/get_busca_cot_aereo";
              break;
            default:
          }
          localStorage.setItem("token", JSON.stringify(token + tipoToken));
          this.token(token + tipoToken);
          datos1 = {
            token: token1,
          };
          this.getTotal(datos1, url2, ur);
        })
        .catch((error) => {
          console.log(error);
          errores("CotizaciónServicios", error);
        });
    } else {
      var tokenPrev = localStorage.token;

      var res = tokenPrev.split("-");

      var tipotk = res[1].replace('"', " ").trim().toString();
      token1 = res[0].replace('"', " ").trim().toString();

      var tipoCon;

      switch (tipotk) {
        case "FCL":
          tipoToken = "-FCL";
          url2 = this.props.api + "maritimo_v2/get_ctz_fcl_resumen";

          ur = this.props.api + "maritimo_v2/get_busca_cot_fcl";
          break;
        case "LCL":
          tipoToken = "-LCL";

          url2 = this.props.api + "maritimo_v2/get_ctz_lcl_resumen";

          ur = this.props.api + "maritimo_v2/get_busca_cot_lcl";

          break;
        case "AER":
          tipoToken = "-AER";
          tipoCon = "Aereo";
          url2 = this.props.api + "aereo_v2/get_ctz_aereo_resumen";

          ur = this.props.api + "aereo_v2/get_busca_cot_aereo";
          break;

        default:
      }
      this.token(token1 + "-" + tipotk);
      datos1 = {
        token: token1,
      };
      this.getTotal(datos1, url2, ur);
    }
  }

  getTotal(datos1, url2, ur) {
    (async () => {
      axios
        .post(url2, await datos1)
        .then((res) => {
          let resumen2 = res.data.data.r_dat;

          let result2 = resumen2.filter(function (item) {
            if (item.valor > 0) {
              return item;
            }
          });
          this.setState({
            resumen: result2,
          });
        })
        .catch((error) => {
          console.log(error);
          errores("CotizaciónServicios", error);
        });
    })();
    (async () => {
      axios
        .post(ur, await datos1)
        .then((res) => {
          let response = res.data.data.r_dat;
          let result;
          // subtotal
          result = response
            .filter(function (item) {
              if (
                item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
                item.tipo !== "IMPUESTOS DE ADUANA MEDIOS"
              ) {
                return item.valor;
              }
            })
            .map(function (duration) {
              return duration.valor.toFixed();
            })
            .reduce(function (accumulator, current) {
              return [+accumulator + +current];
            });

          //---------------------------------------------------------------------
          //---------------------------------------------------------------------
          //---------------------------------------------------------------------
          let result7;

          if (!JSON.parse(localStorage.check2) == false) {
            // IMPUESTOS DE ADUANA BAJOS
            let result4 = response
              .filter((item) => item.tipo == "IMPUESTOS DE ADUANA BAJOS")
              .map(function (duration) {
                return duration.valor.toFixed();
              })
              .reduce(function (accumulator, current) {
                return [+accumulator + +current];
              });

            // IMPUESTOS DE ADUANA MEDIOS
            let result5 = response
              .filter((item) => item.tipo == "IMPUESTOS DE ADUANA MEDIOS")
              .map(function (duration) {
                return duration.valor.toFixed();
              })
              .reduce(function (accumulator, current) {
                return [+accumulator + +current];
              });

            // IMPUESTOS DE ADUANA ALTOS
            let result6 = response
              .filter((item) => item.tipo == "IMPUESTOS DE ADUANA ALTOS")
              .map(function (duration) {
                return duration.valor.toFixed();
              })
              .reduce(function (accumulator, current) {
                return [+accumulator + +current];
              });

            result7 = [[result4], [result6], [result5]];
          } else {
            result7 = [];
          }

          //---------------------------------------------------------------------

          //----------------------------------

          let result3 = 0;

          if (result7.length > 0) {
            // montos totales con impuestos
            result3 = result7
              .map(Number)
              .map((item) => item + Number(result))
              .map((item) => item.toString());
          } else {
            // montos totales sin impuestos
            result3 = result;
          }

          let desgloseValues = response;
          var transporte;
          if (localStorage.transporte == "MARITÍMO") {
            transporte = "FLETE MARÍTIMO";
          } else {
            transporte = "FLETE AÉREO";
          }
          let fecha = response
            .filter(function (item) {
              if (item.tipo == transporte) {
                return item;
              }
            })
            .filter(function (item) {
              if (item.valor > 0) {
                return item;
              }
            });
          let preservicioLogisticoTotal = response
            .filter(function (item) {
              if (
                item.tipo == "SERVICIO LOGÍSTICO" ||
                item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" ||
                item.detalles == "Ganancia"
              ) {
                return item;
              }
            })
            .map(function (duration) {
              return duration.valor;
            });
          var servicioLogisticoTotal;
          if (preservicioLogisticoTotal.length > 1) {
            servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (
              accumulator,
              current
            ) {
              return [+accumulator + +current];
            });
          } else {
            servicioLogisticoTotal = preservicioLogisticoTotal;
          }

          let PretotalIGVCostos = response.filter(function (item) {
            if (
              item.tipo !== "IMPUESTOS DE ADUANA BAJOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA ALTOS" &&
              item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" &&
              item.tipo !== "SERVICIO LOGÍSTICO" &&
              item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" &&
              item.tipo !== "FLETE" &&
              item.tipo !== transporte &&
              item.detalles !== "Ganancia" &&
              item.tipo !== "THCD" &&
              item.tipo !== "TCHD DOCUMENTACION" &&
              item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO"
            ) {
              if (item.valor > 0) {
                return item;
              }
            }
          });
          var totalIGVCostos = [0];
          var totalIGV;
          if (PretotalIGVCostos.length > 1) {
            totalIGVCostos = PretotalIGVCostos.map(function (duration) {
              return (duration.valor * 0.18).toFixed(0);
            })
              .reduce(function (accumulator, current) {
                return [+accumulator + +current];
              })
              .map(function (duration) {
                return duration.toFixed(2);
              });

            totalIGV = (
              Number(totalIGVCostos[0]) +
              Number((servicioLogisticoTotal[0] / 3) * 2 * 0.18)
            ).toFixed(0);
          } else {
            totalIGV = [0];
          }

          var vigencia_desde = fecha[0].detalles_calculos.vigencia_desde;
          var vigencia_hasta = fecha[0].detalles_calculos.vigencia_hasta;

          var fechas = [
            fechaFormateada(vigencia_desde),
            fechaFormateada(vigencia_hasta),
          ];

          localStorage.setItem("fechas_vigencia", JSON.stringify(fechas));
          this.budgetstart();
          this.setState({
            desgloseValues: desgloseValues,
            fechas_vigencia: fechas,
            TotalResumen: result3,
            taxesResumen: result7,
            subtotalResumen: result,
            totalIGVCostos: totalIGV,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    })();
  }
  toggleinfoPaso4() {
    this.setState((ps) => ({ infoPaso4: !ps.infoPaso4 }));
  }
  taxesInfo() {
    this.setState((ps) => ({ taxesInfo: !ps.taxesInfo }));
  }
  commentary() {
    this.setState((ps) => ({ comment: !ps.comment }));
  }
  closeComment() {
    this.setState({ comment: false, animated: true });
    timer = setTimeout(() => {
      this.setState({ animated: false });
    }, 3000);
    let scroll = document.getElementById("link4");
    scrollIntoView(scroll, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
  }
  closedoubt() {
    this.setState({ infoPaso4: false, animated2: true });
    timer = setTimeout(() => {
      this.setState({ animated2: false });
    }, 3000);
    let scroll = document.getElementById("link4");
    scrollIntoView(scroll, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
  }
  sendCommentary() {
    if (
      (this.state.Commentform[0].value.length > 0 ||
        this.state.Commentform[1].value.length > 0 ||
        this.state.Commentform[2].value.length > 0 ||
        this.state.Commentform[3].value.length > 0) &&
      this.state.Commentform[4].value.length > 0 &&
      this.state.Commentform[5].value.length > 0
    ) {
      let data = JSON.parse(sessionStorage.getItem("user"));
      const params = {
        nombre: data[1].toString(),
        email: data[2].toString(),
        fecha: getDateWithFormat() + " " + getCurrentTime(),
        pais: sessionStorage.country,
        paso1: this.state.Commentform[0].value,
        paso2: this.state.Commentform[1].value,
        paso3: this.state.Commentform[2].value,
        paso4: this.state.Commentform[3].value,
        recomendarias: this.state.Commentform[4].value,
        inversion: this.state.Commentform[5].value,
        adicional: this.state.Commentform[6].value,
        costo: this.state.Commentform[7].value,
      };

      this.setState({ comment: false });

      let scroll = document.getElementById("link4");
      scrollIntoView(scroll, {
        behavior: "smooth",
        block: "center",
        inline: "center",
      });
      firebaseConf
        .database()
        .ref("estadisticas")
        .push(params)
        .then(() => {
          this.setState({ showMessage: true });
          timer = setTimeout(() => {
            this.setState({ showMessage: false });
          }, 4000);
          let element = document.getElementById("alert");
          scrollIntoView(element, {
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
        })
        .catch((error) => {
          errores("EnvioComentario", error);
          console.log("FAILED...", error);
        });
    } else {
      this.setState({ commentEmpty: true });

      timer = setTimeout(() => {
        this.setState({ commentEmpty: false });
      }, 4000);
    }
  }
  handleFormState(input) {
    let newState = this.state.Commentform;
    newState[input.id].value = input.value;
    this.setState({
      Commentform: newState,
    });
  }
  render() {
    var token;
    if (localStorage.token !== undefined) {
      token = JSON.parse(localStorage.getItem("token"));
    }
    localStorage.page = "4";

    if (localStorage.transporte == undefined || sessionStorage.usuario == "1") {
      return <Redirect to="/" />;
    }

    var ruc;
    if (localStorage.ruc == undefined) {
      ruc = (
        <Button
          size="large"
          style={{
            background:
              "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)",
            color: "white",
            borderRadius: "1em",
            width: "22rem",
          }}
          onClick={this.cotizacion}
          className="arial"
          startIcon={<GetAppIcon />}
        >
          DESCARGA TU PRESUPUESTO
        </Button>
      );
    } else {
      ruc = (
        <Button
          size="large"
          style={{
            background:
              "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)",
            color: "white",
            borderRadius: "1em",
            width: "22rem",
          }}
          onClick={this.submitForm}
          className="arial"
          startIcon={<GetAppIcon />}
        >
          DESCARGA TU PRESUPUESTO
        </Button>
      );
    }

    localStorage.check4 = this.state.check4;
    const formatter = new Intl.NumberFormat("de-DE");
    const origen = localStorage.puerto.toUpperCase();
    const destino = localStorage.puertoD.toUpperCase();
    const regulador = localStorage.regulador;
    const tipoMercancia = localStorage.tipoMercancia.toUpperCase();
    const Provincia = localStorage.ProvinciaN.toUpperCase();
    const distrito = localStorage.distritoN.toUpperCase();
    const monto = formatter.format(localStorage.monto);
    let taxesResumen = 0;
    let subtotalResumen = 0;
    let TotalResumen = 0;
    let totalIGVCostos =
      localStorage.paisDestino == "PERU" ? this.state.totalIGVCostos : 0;

    //con seguro
    subtotalResumen = this.state.subtotalResumen.map(function (item) {
      return formatter.format(item);
    });

    //impuestos
    if (this.state.taxesResumen.length == 0) {
      taxesResumen = ["No cotizado", "No cotizado", "No cotizado"];
    } else {
      taxesResumen = this.state.taxesResumen.map(function (item) {
        return formatter.format(item);
      });
    }
    let TotalResumenprevio;
    //monto total, se valida si hay impuestos o no

    if (this.state.TotalResumen !== this.state.subtotalResumen) {
      TotalResumen = this.state.TotalResumen.map(function (item) {
        return formatter.format(Number(item) + Number(totalIGVCostos));
      });
    } else {
      TotalResumenprevio = [
        [this.state.subtotalResumen],
        [this.state.subtotalResumen],
        [this.state.subtotalResumen],
      ];
      TotalResumen = TotalResumenprevio.map(function (item) {
        return formatter.format(Number(item) + Number(totalIGVCostos));
      });
    }

    let atencion =
      localStorage.paisDestino == "PERU" ? "atencion" : "atencionPE";
    let data = JSON.parse(sessionStorage.getItem("user"));

    localStorage.setItem("taxesResumen", JSON.stringify(taxesResumen));
    localStorage.setItem("total", JSON.stringify(TotalResumen));
    localStorage.setItem("subtotal", JSON.stringify(subtotalResumen));

    var fechas_vigencia = [1, 1];
    if (this.state.fechas_vigencia.length > 0) {
      fechas_vigencia = this.state.fechas_vigencia;
    }

    return (
      <Fragment>
        {/*paso 4 */}
        <Title>
          <title>{"Presupuesto | PIC  - Calculadora de Fletes"}</title>
        </Title>
        <i className="CommentIcon2" />
        <i className={` ${this.state.animated ? "CommentIcon2" : ""}`} />
        <i className="CommentIcon3" />
        <i className={` ${this.state.animated2 ? "CommentIcon3" : ""}`} />
        {this.state.showLoading ? (
          <Col
            xs="12"
            md="7"
            sm="12"
            className="NoPadding mx-auto mt-2 "
            style={{ display: "block" }}
          >
            <div
              className=" mx-auto "
              xs="12"
              md="7"
              sm="12"
              style={{ fontWeight: "bold" }}
            >
              CALCULANDO
            </div>
            <div className=" mx-auto " xs="12" md="7" sm="12">
              <Spinner />
            </div>
          </Col>
        ) : (
          <div className="pb-5 container2">
            <Row className="h-100 ">
              {this.state.showMessage && (
                <div
                  className="alertSuccess  arial alert alert-success col-12 col-md-5 mx-auto"
                  role="alert"
                >
                  ¡Comentario enviado satisfactoriamente!
                </div>
              )}
              <span id="link4"></span>
              <Row className="budget">
                <Col xs="12" md="10" className="mx-auto mb-2">
                  <span className="mt-2 step-style">
                    PASO 4. PRESUPUESTO&nbsp;
                  </span>
                </Col>
                <i className="icon-exactitud" />
                {data[1] == "Invitado" ? (
                  <Col
                    sm="12"
                    md="12"
                    className="mb-2 NoPadding mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    <Button
                      size="large"
                      style={{
                        background:
                          "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)",
                        color: "white",
                        borderRadius: "1em",
                        width: "17rem",
                        fontSize: "0.75rem",
                        padding: 5,
                      }}
                      onClick={this.signIn}
                      className="arial"
                      startIcon={<GetAppIcon />}
                    >
                      DESCARGA TU PRESUPUESTO
                    </Button>
                  </Col>
                ) : (
                  <Col
                    sm="12"
                    md="12"
                    className="mb-2 NoPadding mx-auto"
                    style={{
                      textAlign: "center",
                    }}
                  >
                    {ruc}
                  </Col>
                )}
                <Col
                  sm="12"
                  xs="12"
                  xl="10"
                  lg="10"
                  className="NoPaddingCost NoPadding mx-auto my-auto"
                  style={{
                    backgroundColor: "#ffffff78",
                    borderRadius: "10px",
                  }}
                >
                  <Row>
                    {/*resumen de carga*/}
                    <Col
                      xs="12"
                      md="11"
                      sm="12"
                      className="pb-2 NoPadding mx-auto"
                    >
                      <Row>
                        <Col
                          xs="12"
                          md="12"
                          sm="12"
                          className="NoPadding text-center pb-1"
                        >
                          <span style={{ fontSize: "1.5em" }}>
                            Resumen de Carga
                          </span>
                        </Col>
                        <Col xs="12" md="12" sm="12" className="pb-1">
                          <span
                            className="float-right"
                            style={{
                              fontWeight: "bold",
                              fontSize: "0.7rem",
                            }}
                          >
                            ID: {token}
                          </span>
                        </Col>
                        <Col xs="12" md="12" sm="12" className="NoPadding pb-1">
                          <span className="plSM float-left">
                            {" "}
                            <i
                              style={{
                                display:
                                  localStorage.transporte === "MARITÍMO"
                                    ? ""
                                    : "none",
                              }}
                              className="icon-ruta icon-transMaritimo "
                            />
                            <i
                              style={{
                                display:
                                  localStorage.transporte === "AÉREO"
                                    ? ""
                                    : "none",
                              }}
                              className="icon-transAereo icon-ruta"
                            />
                            {origen} → {destino}
                          </span>
                        </Col>

                        <Col
                          xs="12"
                          md="6"
                          sm="12"
                          className="display-large NoPadding NoPaddingCost"
                          style={{ fontSize: "0.9em" }}
                        >
                          <ul style={{ margin: "0", textAlign: "justify" }}>
                            <li>
                              {" "}
                              <span className="float-left">
                                {" "}
                                Transporte
                              </span>{" "}
                              <span className="float-right">
                                {localStorage.transporte}
                              </span>
                            </li>

                            <li
                              style={{
                                display:
                                  localStorage.transporte == "AÉREO"
                                    ? "none"
                                    : "",
                              }}
                            >
                              <span className="float-left">Tipo</span>
                              <span className="float-right">
                                {localStorage.tipocon.toUpperCase()}
                              </span>
                            </li>
                            <li>
                              <span className="float-left">Cantidad</span>
                              <span className="float-right">
                                {localStorage.contenedoresDes}
                              </span>
                            </li>
                            <li
                              style={{
                                display:
                                  localStorage.transporte == "AÉREO"
                                    ? "none"
                                    : "",
                              }}
                            >
                              <span className="float-left">
                                Gasto portuario y Almacenamiento Aduanero
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check == true ? "none" : "",
                                }}
                                className="float-right"
                              >
                                Si
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check == false ? "none" : "",
                                }}
                                className="float-right"
                              >
                                NO
                              </span>
                            </li>

                            <li
                              style={{
                                display:
                                  localStorage.transporte == "AÉREO"
                                    ? ""
                                    : "none",
                              }}
                            >
                              <span className="float-left">
                                Gasto Aeroportuario y Almacenamiento Aduanero
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check == true ? "none" : "",
                                }}
                                className="float-right"
                              >
                                Si
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check == false ? "none" : "",
                                }}
                                className="float-right"
                              >
                                NO
                              </span>
                            </li>
                            <li
                              style={{
                                display:
                                  localStorage.transporte == "AÉREO"
                                    ? ""
                                    : "none",
                              }}
                            >
                              {" "}
                              <span className="float-left">
                                {" "}
                                Permiso Gubernamental Adicional:{" "}
                              </span>{" "}
                              <span
                                style={{
                                  display:
                                    this.state.check2 == true ? "none" : "",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                {regulador}
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check2 == false ? "none" : "",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                No Requiere
                              </span>
                            </li>

                            <li
                              style={{
                                display:
                                  this.state.check2 == true ? "none" : "",
                              }}
                            >
                              <span className="float-left">
                                Tipo de producto
                              </span>

                              <span className="float-right">
                                {tipoMercancia}
                              </span>
                            </li>
                            <li
                              style={{
                                display:
                                  localStorage.paisDestino == "PERU" &&
                                  this.state.check2 == false
                                    ? ""
                                    : "none",
                              }}
                            >
                              <span className="float-left">Impuestos:</span>

                              <span className="float-right">
                                {" "}
                                {this.state.check3 == true
                                  ? "Primera Importación"
                                  : "Segunda Importación"}
                              </span>
                            </li>
                          </ul>
                        </Col>

                        <Col
                          xs="12"
                          md="6"
                          sm="12"
                          className="display-large NoPadding NoPaddingCost"
                          style={{ fontSize: "0.9em" }}
                        >
                          <ul style={{ margin: "0", textAlign: "justify" }}>
                            <li
                              style={{
                                display:
                                  this.state.check2 == true ? "none" : "",
                              }}
                            >
                              <span className="float-left">
                                Valor de mercancía
                              </span>

                              <span className="float-right">{monto} USD</span>
                            </li>
                            <li>
                              {" "}
                              <span className="float-left">
                                Impuestos de Aduana
                              </span>{" "}
                              <span
                                style={{
                                  display:
                                    this.state.check2 == true ? "none" : "",
                                }}
                                className="float-right"
                              >
                                Si
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check2 == false ? "none" : "",
                                }}
                                className="float-right"
                              >
                                NO
                              </span>
                            </li>

                            <li>
                              <span className="float-left">
                                {" "}
                                Transporte a Domicilio
                              </span>{" "}
                              <span
                                className="float-right"
                                style={{
                                  display:
                                    this.state.check1 == true ? "none" : "",
                                }}
                              >
                                {Provincia} -{distrito}
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check1 == false ? "none" : "",
                                }}
                                className="float-right"
                              >
                                NO
                              </span>
                            </li>
                            <li>
                              {" "}
                              <span className="float-left">
                                {" "}
                                Seguro de Mercancia:
                              </span>{" "}
                              <span
                                style={{
                                  display:
                                    this.state.check4 == true ? "none" : "",
                                }}
                                className="float-right"
                              >
                                SI
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check4 == false ? "none" : "",
                                }}
                                className="float-right"
                              >
                                NO
                              </span>
                            </li>
                            <li
                              style={{
                                display:
                                  localStorage.transporte == "AÉREO"
                                    ? "none"
                                    : "",
                              }}
                            >
                              {" "}
                              <span className="float-left">
                                {" "}
                                Permiso Gubernamental Adicional:{" "}
                              </span>{" "}
                              <span
                                style={{
                                  display:
                                    this.state.check2 == true ? "none" : "",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                {regulador}
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check2 == false ? "none" : "",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                No Requiere
                              </span>
                            </li>
                          </ul>
                        </Col>
                        <Col
                          xs="12"
                          md="12"
                          sm="12"
                          className="display-sm pt-1 mx-auto"
                          style={{ fontSize: "0.9em" }}
                        >
                          <ul
                            style={{
                              paddingInlineStart: "20px",
                              textAlign: "justify",
                              marginBottom: "0em",
                            }}
                          >
                            <li>
                              {" "}
                              <span className="float-left">
                                {" "}
                                {localStorage.transporte}{" "}
                                <span
                                  style={{
                                    display:
                                      localStorage.transporte == "AÉREO"
                                        ? "none"
                                        : "",
                                  }}
                                >
                                  / {localStorage.tipocon}
                                </span>
                              </span>{" "}
                            </li>

                            <li>
                              <span className="float-left">Cantidad</span>
                              <span className="float-right">
                                {localStorage.contenedoresDes}
                              </span>
                            </li>
                            <li>
                              {" "}
                              <span className="float-left">
                                {" "}
                                Permiso Gubernamental Adicional:{" "}
                              </span>{" "}
                              <span
                                style={{
                                  display:
                                    this.state.check2 == true
                                      ? "none"
                                      : "contents",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                {"  "}
                                {regulador}
                              </span>
                              <span
                                style={{
                                  display:
                                    this.state.check2 == false ? "none" : "",
                                  fontWeight: "bold",
                                  color: "#0d5084",
                                }}
                                className="float-right"
                              >
                                No Requiere
                              </span>
                            </li>

                            <span
                              style={{
                                display:
                                  this.state.expand == true ? "none" : "",
                              }}
                            >
                              <li
                                style={{
                                  display:
                                    this.state.check2 == true ? "none" : "",
                                }}
                              >
                                <span className="float-left">
                                  Tipo de producto
                                </span>

                                <span className="float-right">
                                  {tipoMercancia}
                                </span>
                              </li>
                              <li
                                style={{
                                  display:
                                    this.state.check2 == true ? "none" : "",
                                }}
                              >
                                <span className="float-left">
                                  Valor de mercancía
                                </span>

                                <span className="float-right">{monto} USD</span>
                              </li>
                              <li
                                style={{
                                  display:
                                    localStorage.transporte == "AÉREO"
                                      ? "none"
                                      : "",
                                }}
                              >
                                <span className="float-left">
                                  Gasto portuario y Almacenamiento Aduanero
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check == true ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  Si
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check == false ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  NO
                                </span>
                              </li>

                              <li
                                style={{
                                  display:
                                    localStorage.transporte == "AÉREO"
                                      ? ""
                                      : "none",
                                }}
                              >
                                <span className="float-left">
                                  Gasto Aeroportuario y Almacenamiento Aduanero
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check == true ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  Si
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check == false ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  NO
                                </span>
                              </li>

                              <li>
                                {" "}
                                <span className="float-left">
                                  Impuestos de Aduana
                                </span>{" "}
                                <span
                                  style={{
                                    display:
                                      this.state.check2 == true ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  Si
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check2 == false ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  NO
                                </span>
                              </li>
                              <li
                                style={{
                                  display:
                                    this.state.check3 == true ? "none" : "",
                                }}
                              >
                                <span className="float-left">
                                  Primera Importación:
                                </span>

                                <span className="float-right">NO</span>
                              </li>
                              <li>
                                <span className="float-left">
                                  {" "}
                                  Transporte a Domicilio
                                </span>{" "}
                                <span
                                  className="float-right"
                                  style={{
                                    display:
                                      this.state.check1 == true ? "none" : "",
                                  }}
                                >
                                  {Provincia}-{distrito}
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check1 == false ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  NO
                                </span>
                              </li>
                              <li>
                                {" "}
                                <span className="float-left">
                                  {" "}
                                  Seguro de Mercancia:
                                </span>{" "}
                                <span
                                  style={{
                                    display:
                                      this.state.check4 == true ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  Si
                                </span>
                                <span
                                  style={{
                                    display:
                                      this.state.check4 == false ? "none" : "",
                                  }}
                                  className="float-right"
                                >
                                  NO
                                </span>
                              </li>
                            </span>
                          </ul>
                        </Col>

                        <Col
                          xs="12"
                          md="12"
                          sm="12"
                          className="display-sm pt-4 "
                          style={{
                            fontSize: "1.2em",
                            borderTop: "1px solid rgba(0, 0, 0, 0.18)",
                          }}
                        >
                          <span>
                            <i
                              className="arrowDown arrow-style2"
                              style={{
                                cursor: "pointer",
                                display:
                                  this.state.expand == false ? "none" : "",
                              }}
                              onClick={() =>
                                this.setState((prevState) => ({
                                  expand: !prevState.expand,
                                }))
                              }
                            />

                            <i
                              className="arrowUp arrow-style2"
                              style={{
                                cursor: "pointer",
                                display:
                                  this.state.expand == true ? "none" : "",
                              }}
                              onClick={() =>
                                this.setState((prevState) => ({
                                  expand: !prevState.expand,
                                }))
                              }
                            />
                          </span>
                        </Col>
                      </Row>
                    </Col>
                    {/*dudas*/}
                    <Col
                      xs="12"
                      md="12"
                      sm="12"
                      style={{
                        textAlign: "center",
                      }}
                      className="NoPadding mx-auto color-white"
                      style={{
                        backgroundColor: "#FF9800",
                        fontWeight: "bold",
                        cursor: "help",
                        fontSize: "1rem",
                      }}
                      onClick={this.commentary}
                    >
                      <span>
                        ¿Dudas? -
                        <span
                          style={{
                            textDecoration: "underline",
                            cursor: "pointer",
                          }}
                        >
                          {" "}
                          Click Aquí
                        </span>{" "}
                      </span>
                    </Col>
                    <Col
                      xs="12"
                      md="12"
                      sm="12"
                      style={{
                        textAlign: "center",
                      }}
                      className="mb-2 NoPadding mx-auto color-wine"
                      style={{
                        fontWeight: "bold",
                        borderBottom: "1px solid rgb(237, 237, 237)",
                      }}
                    >
                      {" "}
                      <span className="valid-time2">
                        Validez de tarifa: {fechas_vigencia[0]} {" - "}
                        {fechas_vigencia[1]}
                      </span>
                    </Col>

                    <Row>
                      {/*servicio logistico*/}
                      <Col xs="12" sm="12" md="10" className="mx-auto">
                        <Row>
                          <Col
                            className="NoPadding fontLogisTittleAling"
                            xs="7"
                            sm="7"
                            md="6"
                            style={{
                              borderRight: "1px solid rgb(237, 237, 237)",
                            }}
                          >
                            <Row>
                              <Col
                                xs="12"
                                md="12"
                                sm="12"
                                className="NoPadding mb-2 fontLogisTittleAling2 color-wine"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                <i className="icon-Logo" />{" "}
                                <span className="fontLogisTittle">
                                  SERVICIO LOGÍSTICO
                                </span>
                                <i
                                  id="question1"
                                  className="simple-icon-question question-style2"
                                />
                                <UncontrolledTooltip
                                  placement="right"
                                  target="question1"
                                >
                                  <div className="color-blue text-justify">
                                    El Servicio Logistico es el{" "}
                                    <span style={{ color: "red" }}>
                                      valor cotizado por la Agencia de Carga y
                                      Aduana
                                    </span>{" "}
                                    por realizar los servicios seleccionados.{" "}
                                    <span style={{ color: "red" }}>
                                      El costo del mismo se mantiene en todos
                                      los escenarios.
                                    </span>
                                  </div>
                                  <div className="color-blue text-justify">
                                    Lo que hace{" "}
                                    <span style={{ fontWeight: "bold" }}>
                                      {" "}
                                      diferente{" "}
                                    </span>{" "}
                                    a un Escenario de Otro son los Impuestos de
                                    Aduana.
                                  </div>
                                </UncontrolledTooltip>
                                <div
                                  style={{ fontWeight: "bold" }}
                                  className="valid-time2"
                                >
                                  Valor FIJO
                                </div>
                              </Col>
                              <Col
                                xs="12"
                                sm="12"
                                md="12"
                                style={{
                                  height: "fit-content",
                                }}
                                className="float-left NoPadding investmentStyle"
                              >
                                {/*Contenedor completo*/}

                                <ul
                                  style={{
                                    paddingInlineStart: "20px",
                                    display:
                                      localStorage.transporte == "MARITÍMO" &&
                                      localStorage.tipocon ==
                                        "Contenedor completo"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <li
                                    style={{
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    Incluye
                                  </li>
                                  <span></span>
                                  <li>FLETE MARITIMO</li>
                                  <li>THCD (Manejo de naviera en Destino)</li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == true &&
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == true
                                            ? "none"
                                            : "",
                                      }}
                                    >
                                      VISTOS BUENOS
                                    </span>
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == true &&
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == true
                                            ? "none"
                                            : "",
                                      }}
                                    >
                                      {" "}
                                      GATE IN(Recepción de contenedor vacio en
                                      el puerto
                                    </span>
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == true ? "none" : "",
                                    }}
                                  >
                                    ALMACEN ADUANERO
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == true ? "none" : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == true ? "none" : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>

                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == true ? "none" : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == true ||
                                        this.state.check1 == true ||
                                        this.state.check2 == true ||
                                        this.state.check4 == true
                                          ? ""
                                          : "none",
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    No incluye
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == true &&
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == true
                                            ? ""
                                            : "none",
                                      }}
                                    >
                                      VISTOS BUENOS
                                    </span>
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == true &&
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == true
                                            ? ""
                                            : "none",
                                      }}
                                    >
                                      {" "}
                                      GATE IN(Recepción de contenedor vacio en
                                      el puerto
                                    </span>
                                  </li>

                                  <li
                                    style={{
                                      display:
                                        this.state.check == false ? "none" : "",
                                    }}
                                  >
                                    ALMACEN ADUANERO
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                </ul>
                                {/*Contenedor compartido*/}

                                <ul
                                  style={{
                                    paddingInlineStart: "20px",
                                    display:
                                      localStorage.transporte == "MARITÍMO" &&
                                      localStorage.tipocon ==
                                        "Contenedor compartido"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <li
                                    style={{
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    Incluye
                                  </li>
                                  <span></span>
                                  <li>FLETE MARITIMO</li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    HANDLING Y MANEJO DESTINO
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    VISTOS BUENOS
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    DESCARGA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    DMC DE SALIDA GASTOS PORTUARIOS
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    DESCONSOLIDACIÓN
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    MANEJOS
                                  </li>

                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == true &&
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == true
                                            ? "none"
                                            : "",
                                      }}
                                    >
                                      ALMACEN ADUANERO
                                    </span>
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == true ? "none" : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == true ? "none" : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>

                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == true ? "none" : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == true ||
                                        this.state.check1 == true ||
                                        this.state.check2 == true ||
                                        this.state.check4 == true
                                          ? ""
                                          : "none",
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    No incluye
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        localStorage.paisDestino == "PERU"
                                          ? ""
                                          : "none",
                                      listStyle:
                                        this.state.check == false &&
                                        localStorage.paisDestino == "PERU"
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    <span
                                      style={{
                                        display:
                                          this.state.check == false
                                            ? "none"
                                            : "",
                                      }}
                                    >
                                      ALMACEN ADUANERO
                                    </span>
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                </ul>

                                {/*Flete aereo*/}
                                <ul
                                  style={{
                                    paddingInlineStart: "20px",
                                    display:
                                      localStorage.transporte == "AÉREO"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <li
                                    style={{
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    Incluye
                                  </li>
                                  <span></span>
                                  <li>FLETE AEREO</li>
                                  <li>GUIA AEREA</li>
                                  <li>GASTOS EN DESTINO DE LA AEROLINEA</li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == true ? "none" : "",
                                    }}
                                  >
                                    ALMACEN ADUANERO
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == true ? "none" : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == true ? "none" : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == true ? "none" : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == true ||
                                        this.state.check1 == true ||
                                        this.state.check2 == true ||
                                        this.state.check4 == true
                                          ? ""
                                          : "none",
                                      listStyle: "none",
                                      color: "#0d5084",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    No incluye
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check == false ? "none" : "",
                                    }}
                                  >
                                    ALMACEN ADUANERO
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check2 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    HONORARIOS DE AGENCIA DE ADUANA
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check1 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    TRANSPORTE A FABRICA IMPORTADOR
                                  </li>
                                  <li
                                    style={{
                                      display:
                                        this.state.check4 == false
                                          ? "none"
                                          : "",
                                    }}
                                  >
                                    SEGURO DE MERCANCIA
                                  </li>
                                </ul>
                              </Col>
                            </Row>
                          </Col>
                          <Col
                            xs="5"
                            sm="5"
                            md="6"
                            className="NoPadding mx-auto"
                            style={{ alignSelf: "center" }}
                          >
                            <Row>
                              <Col
                                className="NoPadding mx-auto"
                                xs="12"
                                md="12"
                                sm="12"
                              >
                                <span className="arial fontLogis color-wine">
                                  {subtotalResumen}
                                  <span className="precio">,</span> USD
                                </span>
                                <br />
                                <span
                                  style={{
                                    display:
                                      localStorage.paisDestino == "PERU" &&
                                      this.state.totalIGVCostos > 0
                                        ? ""
                                        : "none",
                                  }}
                                  className="valid-time2"
                                >
                                  + IGV 18% {this.state.totalIGVCostos} USD
                                </span>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                      {/*finservicio logistico*/}
                      {/*IGV PERU*/}
                      <Col
                        xs="12"
                        sm="12"
                        md="12"
                        style={{
                          borderTop: "1px solid rgb(237, 237, 237)",
                          alignSelf: "center",
                          display:
                            localStorage.paisDestino == "PERU" ? "" : "none",
                        }}
                        className="amountSize arial color-blue p-2"
                      >
                        <span>
                          NOTA: Los conceptos están sujetos a IGV, excepto el
                          flete internacional.
                        </span>
                      </Col>
                      {/*impuestos*/}
                      <Col
                        xs="12"
                        sm="12"
                        md="12"
                        style={{ alignSelf: "center" }}
                        className="NoPadding mx-auto p-1 theme-color-lightBlue"
                      >
                        <Row>
                          <i
                            className={`${taxesAlertSM(
                              localStorage.paisDestino
                            )}`}
                          />
                          <Col
                            xs="12"
                            md="6"
                            sm="12"
                            className="marginSM"
                            style={{
                              borderRight: "1px solid rgb(237, 237, 237)",
                            }}
                          >
                            <span className="fontLogisTittle">
                              IMPUESTOS DE ADUANA
                            </span>{" "}
                            <i
                              onClick={this.taxesInfo}
                              id="question2"
                              style={{ cursor: "pointer" }}
                              className="simple-icon-question question-style2"
                            />
                            <div
                              className={`tooltip-inner-cus ${
                                this.state.showBox
                                  ? "show-toolkit"
                                  : "hide-toolkit"
                              }`}
                            >
                              <Row>
                                <Col
                                  md="12"
                                  lg={
                                    localStorage.paisDestino == "PERU"
                                      ? "6"
                                      : "12"
                                  }
                                  sm="12"
                                  className=" NoPaddingCost mx-auto"
                                  style={{
                                    textAlign: "center",
                                  }}
                                >
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" arial mx-auto"
                                    style={{
                                      textAlign: "center",
                                    }}
                                  >
                                    <i className="icon-ICO-2 " />
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" arial mb-2 mx-auto"
                                    style={{
                                      textAlign: "center",
                                    }}
                                  >
                                    <ListGroup>
                                      <ListGroupItem className="listTittle">
                                        ¿Porque varían los impuestos de aduana?
                                      </ListGroupItem>
                                    </ListGroup>
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" my-auto mx-auto"
                                    style={{
                                      textAlign: "justify",
                                    }}
                                  >
                                    <div
                                      style={{
                                        margin: "0.5em",

                                        borderRadius: "1em",
                                        padding: "0.5em",
                                        height: "max-content",
                                      }}
                                    >
                                      <div style={{ fontWeight: "normal" }}>
                                        &nbsp;&nbsp;Los Impuestos de aduana
                                        varían de un escenario a otro y el monto
                                        exacto dependerá de los siguientes
                                        detalles de su producto:
                                      </div>
                                      <ul style={{ fontWeight: "bold" }}>
                                        <li>Descripción</li>
                                        <li>Composición</li>
                                        <li>Uso final</li>
                                      </ul>{" "}
                                    </div>
                                  </Col>
                                </Col>
                                <Col
                                  md="12"
                                  lg="6"
                                  sm="12"
                                  className="NoPaddingCost mx-auto"
                                  style={{
                                    textAlign: "center",
                                    display:
                                      localStorage.paisDestino == "PERU"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" arial mx-auto"
                                    style={{
                                      textAlign: "center",
                                    }}
                                  >
                                    <i className="icon-Sunat" />
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" arial mb-2 mx-auto"
                                    style={{
                                      textAlign: "center",
                                    }}
                                  >
                                    <ListGroup>
                                      <ListGroupItem className="listTittle">
                                        Los Impuestos fueron calculados de la
                                        siguiente mandera:
                                      </ListGroupItem>
                                    </ListGroup>
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className=" my-auto mx-auto"
                                    style={{
                                      textAlign: "justify",
                                    }}
                                  >
                                    <div
                                      style={{
                                        borderRadius: "1em",
                                      }}
                                    >
                                      <Table
                                        style={{
                                          fontSize: "0.7em",
                                          textAlign: "center",
                                        }}
                                        striped
                                      >
                                        <thead>
                                          <tr>
                                            <th>CONCEPTO</th>
                                            <th>ESCENARIO MÍNIMO</th>
                                            <th>ESCENARIO MEDIO</th>
                                            <th>ESCENARIO MÁXIMO</th>
                                          </tr>
                                        </thead>
                                        <tbody
                                          style={{
                                            fontSize: "0.8rem",
                                          }}
                                        >
                                          <tr>
                                            <th scope="row">IGV</th>
                                            <td>16%</td>
                                            <td>16%</td>
                                            <td>16%</td>
                                          </tr>
                                          <tr>
                                            <th scope="row">IPM</th>
                                            <td>2%</td>
                                            <td>2%</td>
                                            <td>2%</td>
                                          </tr>
                                          <tr>
                                            <th scope="row">PERCEPCIÓN</th>
                                            <td>
                                              {localStorage.value4 == "false"
                                                ? "10%"
                                                : "3,50%"}
                                            </td>
                                            <td>
                                              {localStorage.value4 == "false"
                                                ? "10%"
                                                : "3,50%"}
                                            </td>
                                            <td>
                                              {localStorage.value4 == "false"
                                                ? "10%"
                                                : "3,50%"}
                                            </td>
                                          </tr>
                                          <tr
                                            style={{
                                              fontSize: "0.9rem",
                                              color: "red",
                                            }}
                                          >
                                            <th scope="row">AD VALOREM</th>
                                            <td>0%</td>
                                            <td>6%</td>
                                            <td>11%</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                    </div>
                                  </Col>
                                </Col>
                              </Row>
                            </div>
                            <div
                              style={{ fontWeight: "bold" }}
                              className="valid-time2"
                            >
                              Valor{" "}
                              <span style={{ color: "red" }}>VARIABLE</span>{" "}
                              según descripción de producto
                            </div>
                          </Col>

                          <Col
                            sm="12"
                            md="6"
                            style={{ alignSelf: "center" }}
                            className="arial NoPadding mx-auto"
                          >
                            <Row>
                              <Col
                                xs="4"
                                md="4"
                                sm="4"
                                className="arial NoPadding"
                                style={{
                                  borderRight: "1px solid rgb(237, 237, 237)",
                                }}
                              >
                                {" "}
                                <Row>
                                  <Col xs="12" md="12" sm="12">
                                    <span>BAJO </span>{" "}
                                    <i
                                      onClick={this.taxesInfo}
                                      id="question2"
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className="amountSize arial p-1"
                                  >
                                    <span
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    >
                                      {taxesResumen[0]}
                                      <span
                                        style={{
                                          display:
                                            this.state.taxesResumen.length > 0
                                              ? ""
                                              : "none",
                                        }}
                                      >
                                        <span className="precio">,</span> USD
                                      </span>
                                    </span>
                                  </Col>
                                </Row>{" "}
                              </Col>{" "}
                              <Col
                                xs="4"
                                md="4"
                                sm="4"
                                style={{
                                  borderRight: "1px solid rgb(237, 237, 237)",
                                }}
                                className=" NoPadding"
                              >
                                {" "}
                                <Row>
                                  <Col xs="12" md="12" sm="12">
                                    <span>MEDIO</span>{" "}
                                    <i
                                      onClick={this.taxesInfo}
                                      id="question2"
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className="amountSize arial p-1"
                                  >
                                    <span
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    >
                                      {taxesResumen[2]}
                                      <span
                                        style={{
                                          display:
                                            this.state.taxesResumen.length > 0
                                              ? ""
                                              : "none",
                                        }}
                                      >
                                        <span className="precio">,</span> USD
                                      </span>
                                    </span>
                                  </Col>
                                </Row>{" "}
                              </Col>
                              <Col xs="4" md="4" sm="4" className=" NoPadding">
                                {" "}
                                <Row>
                                  <Col xs="12" md="12" sm="12">
                                    <span>MÁXIMO</span>{" "}
                                    <i
                                      onClick={this.taxesInfo}
                                      id="question2"
                                      style={{ cursor: "pointer" }}
                                      className="simple-icon-question question-style2"
                                    />
                                  </Col>
                                  <Col
                                    xs="12"
                                    md="12"
                                    sm="12"
                                    className="amountSize arial p-1"
                                  >
                                    <span
                                      style={{
                                        fontWeight: "bold",
                                      }}
                                    >
                                      {taxesResumen[1]}
                                      <span
                                        style={{
                                          display:
                                            this.state.taxesResumen.length > 0
                                              ? ""
                                              : "none",
                                        }}
                                      >
                                        <span className="precio">,</span> USD
                                      </span>
                                    </span>
                                  </Col>
                                </Row>{" "}
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                      {/*totales*/}
                      <Col
                        xs="12"
                        sm="12"
                        md="12"
                        style={{ alignSelf: "center" }}
                        className="arial NoPadding mb-3 theme-color-blue"
                      >
                        <Row>
                          <Col
                            xs="12"
                            md="6"
                            sm="12"
                            className="p-2 marginSM fontLogisTittle"
                            style={{
                              borderRight: "1px solid rgb(237, 237, 237)",
                              fontWeight: "bold",
                              alignSelf: "center",
                            }}
                          >
                            {taxesResumen[0] === "No cotizado" ? (
                              <span>
                                TOTAL{" "}
                                <span
                                  style={{
                                    display:
                                      localStorage.paisDestino == "PERU" &&
                                      this.state.totalIGVCostos > 0
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  (incluye IGV)
                                </span>
                              </span>
                            ) : (
                              <span>
                                TOTALES{" "}
                                <span
                                  style={{
                                    display:
                                      localStorage.paisDestino == "PERU" &&
                                      this.state.totalIGVCostos > 0
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  (incluye IGV)
                                </span>
                              </span>
                            )}
                          </Col>

                          <Col
                            xs="12"
                            md="6"
                            sm="12"
                            style={{ alignSelf: "center" }}
                            className="amountSize mx-auto"
                          >
                            {" "}
                            {taxesResumen[0] === "No cotizado" ? (
                              <Row>
                                <Col
                                  xs="12"
                                  md="12"
                                  sm="12"
                                  className="arial p-1"
                                >
                                  <span
                                    style={{
                                      fontWeight: "bold",
                                    }}
                                  >
                                    {TotalResumen[0]}
                                    <span className="precio">,</span> USD
                                  </span>
                                </Col>
                              </Row>
                            ) : (
                              <Row>
                                <Col
                                  xs="4"
                                  md="4"
                                  sm="4"
                                  style={{
                                    borderRight: "1px solid rgb(237, 237, 237)",
                                  }}
                                  className=" NoPadding"
                                >
                                  {" "}
                                  <Row>
                                    <Col
                                      xs="12"
                                      md="12"
                                      sm="12"
                                      className="arial p-1"
                                    >
                                      <span
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {TotalResumen[0]}
                                        <span className="precio">,</span> USD
                                      </span>
                                    </Col>
                                  </Row>{" "}
                                </Col>{" "}
                                <Col
                                  xs="4"
                                  md="4"
                                  sm="4"
                                  style={{
                                    borderRight: "1px solid rgb(237, 237, 237)",
                                  }}
                                  className=" NoPadding"
                                >
                                  {" "}
                                  <Row>
                                    <Col
                                      xs="12"
                                      md="12"
                                      sm="12"
                                      className="arial p-1"
                                    >
                                      <span
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {TotalResumen[2]}
                                        <span className="precio">,</span> USD
                                      </span>
                                    </Col>
                                  </Row>{" "}
                                </Col>{" "}
                                <Col
                                  xs="4"
                                  md="4"
                                  sm="4"
                                  className=" NoPadding"
                                >
                                  {" "}
                                  <Row>
                                    <Col
                                      xs="12"
                                      md="12"
                                      sm="12"
                                      className="arial p-1"
                                    >
                                      <span
                                        style={{
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {TotalResumen[1]}
                                        <span className="precio">,</span> USD
                                      </span>
                                    </Col>
                                  </Row>{" "}
                                </Col>
                              </Row>
                            )}
                          </Col>
                        </Row>
                      </Col>
                    </Row>

                    {/*<Cadena />*/}

                    <Col
                      sm="12"
                      md="6"
                      className="NoPadding mb-2 mx-auto"
                      style={{
                        textAlign: "center",
                      }}
                    >
                      <Button
                        className="btn-style-Green"
                        size="large"
                        style={{
                          backgroundColor: "#64b86a",
                          color: "white",
                          border: "1px solid #64b86a",
                          borderRadius: "1em",
                          width: "22rem",
                        }}
                        href={
                          "https://api.whatsapp.com/send?phone=" +
                          numero(localStorage.paisDestino) +
                          "&text=Saludos,%20me%20gustar%C3%ADa%20cotizar%20ID:" +
                          token +
                          ",%20%20Tipo Flete:%20" +
                          localStorage.transporte +
                          ",%20%20%20Tipo Conte:%20" +
                          localStorage.tipocon +
                          ",%20%20%20Flete Origen:%20" +
                          localStorage.puerto +
                          ",%20%20%20Flete Destino:%20" +
                          localStorage.puertoD +
                          ",%20%20%20Contenido Flete:%20" +
                          localStorage.contenedoresDes +
                          ",%20%20%20Valor Flete:%20" +
                          this.state.subtotalResumen +
                          ",%20%20%20%20%20gastos:%20" +
                          gastos +
                          ",%20%20%20%20%20Impuestos:%20" +
                          Impuestos +
                          ",%20%20%20%20%20Transporte:%20" +
                          Transporte +
                          ",%20%20%20%20%20Seguro:%20" +
                          Seguro +
                          ",%20%20%20%20%20ImpuestoBajo:%20" +
                          taxesResumen[0] +
                          ",%20%20%20%20%20ImpuestoMedio:%20" +
                          taxesResumen[2] +
                          ",%20%20%20%20%20ImpuestoAlto:%20" +
                          taxesResumen[1]
                        }
                        target="_blank"
                        className="arial"
                        startIcon={<WhatsAppIcon />}
                      >
                        CONTACTAR ASESOR DE ADUANA
                      </Button>
                    </Col>
                    <Col
                      sm="12"
                      md="6"
                      className="NoPadding mb-2 mx-auto"
                      style={{
                        textAlign: "center",
                      }}
                    >
                      <Link to="/">
                        <Button
                          size="large"
                          style={{
                            background:
                              "linear-gradient(90deg, #dd9472 0%, #ff6822 58%, #802a03 100%)",
                            color: "white",
                            borderRadius: "1em",
                            width: "22rem",
                          }}
                          onClick={this.clearStorage}
                          className="arial"
                          startIcon={<RestoreIcon />}
                        >
                          NUEVA CONSULTA
                        </Button>
                      </Link>
                    </Col>

                    <Col
                      xs="12"
                      md="11"
                      sm="12"
                      className="mb-2 NoPadding row my-auto mx-auto"
                      style={{
                        textAlign: "center",
                        borderBottom: "1px solid rgba(0, 0, 0, 0.18)",
                      }}
                    >
                      {data[1] == "Invitado" ? (
                        <Col
                          sm="12"
                          md="12"
                          className="mb-2 NoPadding mx-auto"
                          style={{
                            textAlign: "center",
                          }}
                        >
                          <Button
                            size="large"
                            style={{
                              background:
                                "linear-gradient(90deg, #3c80b4 0%, #0d5084 35%, #0c1924 100%)",
                              color: "white",
                              borderRadius: "1em",
                              width: "22rem",
                            }}
                            onClick={this.signIn}
                            className="arial"
                            startIcon={<GetAppIcon />}
                          >
                            DESCARGA TU PRESUPUESTO
                          </Button>
                        </Col>
                      ) : (
                        <Col
                          sm="12"
                          md="12"
                          className="mb-2 NoPadding mx-auto"
                          style={{
                            textAlign: "center",
                          }}
                        >
                          {ruc}
                        </Col>
                      )}
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Row>
            <i
              onClick={this.arrowScroll}
              className="arrowDownMove animated infinite bounce slow"
            />
            <i
              onClick={() =>
                this.setState(() => ({
                  arrowDownload: false,
                }))
              }
              style={{ display: this.state.arrowDownload ? "" : "none" }}
              className="arrowDownload animated infinite bounce slow"
            />
            <ShareButtons />
            <Whatsapp
              pag={"presupuesto"}
              gastos={gastos}
              Impuestos={Impuestos}
              Transporte={Transporte}
              Seguro={Seguro}
              taxesResumen={taxesResumen}
              subtotalResumen={this.state.subtotalResumen}
            />
            <span id="bottom"></span>
            {this.state.token.length > 0 && (
              <i
                onClick={this.desglose}
                style={{ fontSize: "2rem", color: "red", cursor: "pointer" }}
                className="simple-icon-notebook  float-right"
              />
            )}
          </div>
        )}

        <Modal
          isOpen={this.state.taxesInfo}
          toggle={this.taxesInfo}
          backdrop={true}
          style={{ borderRadius: "2.1rem", maxWidth: "27em !important" }}
          className="fontAtentionStyle"
        >
          <ModalBody className="NoPadding" style={{ padding: "0rem" }}>
            <i
              onClick={this.taxesInfo}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
              }}
            />
            <Row>
              <div
                className="color-white mb-3 "
                style={{
                  textAlign: "center",
                  backgroundColor: "#0d5084",
                  width: "100%",
                }}
              >
                <Col
                  md="12"
                  sm="12"
                  style={{ fontSize: "1.5rem" }}
                  className="pb-2 pt-2 text-center arial mx-auto"
                >
                  <span>
                    <i className="simple-icon-info mr-2" /> IMPORTANTE
                  </span>
                </Col>
              </div>

              <Col
                md="12"
                sm="12"
                className="mb-5 NoPaddingCost mx-auto"
                style={{
                  textAlign: "center",
                }}
              >
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" arial mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <i className="icon-ICO-2 " />
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" arial  mb-2 mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <ListGroup>
                    <ListGroupItem className="listTittle">
                      ¿Porque varían los impuestos de aduana?
                    </ListGroupItem>
                  </ListGroup>
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" my-auto mx-auto"
                  style={{
                    textAlign: "justify",
                  }}
                >
                  <div
                    style={{
                      margin: "0.5em",

                      borderRadius: "1em",
                      padding: "0.5em",
                      height: "8em",
                    }}
                  >
                    <div style={{ fontWeight: "normal" }}>
                      &nbsp;&nbsp;Los Impuestos de aduana varían de un escenario
                      a otro y el monto exacto dependerá de los siguientes
                      detalles de su producto:
                    </div>
                    <ul style={{ fontWeight: "bold" }}>
                      <li>Descripción</li>
                      <li>Composición</li>
                      <li>Uso final</li>
                    </ul>{" "}
                  </div>
                </Col>
              </Col>
              <Col
                md="12"
                md="12"
                sm="12"
                className="NoPaddingCost mx-auto"
                style={{
                  textAlign: "center",
                  display:
                    localStorage.paisDestino == "PERU" ||
                    localStorage.paisDestino == "PANAMA"
                      ? ""
                      : "none",
                }}
              >
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" arial mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <i className="icon-Sunat " />
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" arial mb-2 mx-auto"
                  style={{
                    textAlign: "center",
                  }}
                >
                  <ListGroup>
                    <ListGroupItem className="listTittle">
                      Los Impuestos fueron calculados de la siguiente mandera:
                    </ListGroupItem>
                  </ListGroup>
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" my-auto mx-auto"
                  style={{
                    textAlign: "justify",
                  }}
                >
                  <div
                    style={{
                      borderRadius: "1em",
                    }}
                  >
                    <Table
                      style={{
                        fontSize: "0.7em",
                        textAlign: "center",
                      }}
                      striped
                    >
                      <thead>
                        <tr>
                          <th>CONCEPTO</th>
                          <th>ESCENARIO MÍNIMO</th>
                          <th>ESCENARIO MEDIO</th>
                          <th>ESCENARIO MÁXIMO</th>
                        </tr>
                      </thead>
                      <tbody
                        style={{
                          fontSize: "0.8rem",
                        }}
                      >
                        <tr>
                          <th scope="row">IGV</th>
                          <td>16%</td>
                          <td>16%</td>
                          <td>16%</td>
                        </tr>
                        <tr>
                          <th scope="row">IPM</th>
                          <td>2%</td>
                          <td>2%</td>
                          <td>2%</td>
                        </tr>
                        <tr>
                          <th scope="row">PERCEPCIÓN</th>
                          <td>
                            {localStorage.value4 == "false" ? "10%" : "3,50%"}
                          </td>
                          <td>
                            {localStorage.value4 == "false" ? "10%" : "3,50%"}
                          </td>
                          <td>
                            {localStorage.value4 == "false" ? "10%" : "3,50%"}
                          </td>
                        </tr>
                        {localStorage.paisDestino == "PERU" ? (
                          <tr
                            style={{
                              fontSize: "0.9rem",
                              color: "red",
                            }}
                          >
                            <th scope="row">AD VALOREM</th>
                            <td>0%</td>
                            <td>6%</td>
                            <td>11%</td>
                          </tr>
                        ) : (
                          <tr
                            style={{
                              fontSize: "0.9rem",
                              color: "red",
                            }}
                          >
                            <th scope="row">AD VALOREM</th>
                            <td>0%</td>
                            <td>10%</td>
                            <td>15%</td>
                          </tr>
                        )}
                      </tbody>
                    </Table>
                  </div>
                </Col>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.IsVisiblesignIn}
          toggle={this.signIn}
          backdrop={false}
          size="md"
          style={{ borderRadius: "1.1rem" }}
        >
          <ModalBody style={{ padding: "0rem" }}>
            <i
              onClick={this.signIn}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />

            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className=" font NoPadding text-center pt-1 pb-1 arial color-white"
              style={{
                backgroundColor: "#0d5084",

                borderRadius: "1.1rem 1.1rem 0rem 0rem",
              }}
            >
              <div>
                <div>
                  <span
                    className="bggradien-orange color-white"
                    style={{
                      fontWeight: "bold",
                      borderRadius: "5px",
                      fontSize: "1.35rem",
                    }}
                  >
                    DESCARGA TU OFERTA
                  </span>{" "}
                </div>
                <div>
                  <span
                    className="color-white"
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    Ingresando los siguientes datos
                  </span>
                </div>
              </div>
            </Colxx>

            <Row className="p-2 text-center">
              <Colxx xs="12">
                <Form className="login-form" onSubmit={this.ruc}>
                  <div style={{ fontSize: "0.9rem" }}>
                    <hr className="ten" />
                    <FormGroup
                      style={{ fontWeight: "bold" }}
                      className="mt-1 mb-1"
                    >
                      <div className="mb-1">
                        <span
                          className="color-blue"
                          style={{
                            fontWeight: "bold",
                          }}
                        >
                          {" "}
                          {localStorage.paisDestino == "PERU"
                            ? "INGRESA tu DNI / RUC 10 / RUC 20"
                            : "INGRESA tu numero de cedula o numero de RUC"}{" "}
                        </span>{" "}
                      </div>
                      <div>
                        <Input
                          name="ruc"
                          type="text"
                          placeholder="Campo requerido"
                        />
                      </div>
                    </FormGroup>
                    <FormGroup className="mb-1">
                      <span
                        style={{
                          fontWeight: "bold",
                        }}
                      >
                        NOMBRE DE TU EMPRESA
                      </span>{" "}
                      - (Opcional)
                    </FormGroup>
                    <FormGroup className="mb-2">
                      <Input
                        type="text"
                        name="empresa"
                        placeholder="Ingrese aqui el nombre de su empresa"
                      />
                    </FormGroup>
                    <FormGroup className="mb-1">
                      <span
                        style={{
                          fontWeight: "bold",
                        }}
                      >
                        NÚMERO DE TELEFONO
                      </span>{" "}
                      - (Opcional)
                    </FormGroup>
                    <FormGroup className="mb-2">
                      <Input
                        type="number"
                        name="telefono"
                        placeholder="Ingrese aqui su número de telefono"
                      />
                    </FormGroup>
                    {this.state.signInChoose ? (
                      <FormGroup className="mt-1 mb-2">
                        <div className="mb-3">INGRESA TU EMAIL</div>
                        <div>
                          <Button
                            style={{
                              borderRadius: "13px",
                              fontSize: "14px",
                            }}
                            className="GLogin"
                            type="submit"
                          >
                            <i className="icon-gmail" />
                            <span>Gmail,&nbsp;</span>{" "}
                            <span
                              className="color-orange"
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                textDecoration: "underline",
                              }}
                            >
                              solo con un click
                            </span>
                          </Button>
                        </div>
                        <div className="text-center mt-2 mb-2">
                          <span style={{ fontSize: "0.9rem" }}>
                            {" "}
                            o con otro&nbsp;
                            <span
                              className="arial"
                              onClick={() =>
                                this.setState(() => ({
                                  signInChoose: false,
                                }))
                              }
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              correo electrónico
                            </span>
                          </span>
                        </div>
                      </FormGroup>
                    ) : (
                      <span>
                        <FormGroup className="mt-1 mb-2">
                          <div className="mb-3">INGRESA TU EMAIL</div>
                          <Input
                            type="email"
                            name="usuario"
                            placeholder="Correo electrónico"
                          />
                        </FormGroup>
                        <FormGroup className="mt-2 mb-2 login-form-button">
                          <Button
                            type="submit"
                            className="btn-style-blue btn btn-success btn-sm"
                          >
                            Descargar cotizacion
                          </Button>
                        </FormGroup>{" "}
                      </span>
                    )}
                  </div>
                </Form>
              </Colxx>
            </Row>
          </ModalBody>
        </Modal>
        <Modal
          isOpen={this.state.cotizacion}
          toggle={this.cotizacion}
          backdrop={false}
          size="lg"
          style={{ borderRadius: "1.1rem" }}
        >
          <ModalBody style={{ padding: "1rem 0px 1rem" }}>
            <i
              onClick={this.cotizacion}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <div xxs="12" md="12" sm="12" className="text-center font">
              Cotización
            </div>
            <hr className="ten" />

            <Row>
              <Form className="mx-auto" onSubmit={this.ruc}>
                <FormGroup style={{ fontWeight: "bold" }} className="mt-2 mb-1">
                  <div className="mb-3">
                    <span
                      className="color-blue"
                      style={{
                        fontWeight: "bold",
                      }}
                    >
                      {" "}
                      {localStorage.paisDestino == "PERU"
                        ? "INGRESA tu DNI / RUC 10 / RUC 20"
                        : "INGRESA tu numero de cedula o numero de RUC"}{" "}
                    </span>{" "}
                  </div>
                </FormGroup>
                <FormGroup style={{ fontWeight: "bold" }} className="mb-2">
                  <div>
                    <Input name="ruc" type="text" />
                  </div>
                </FormGroup>
                <FormGroup className="mb-1">
                  <span
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    NOMBRE DE TU EMPRESA
                  </span>{" "}
                  - (Opcional)
                </FormGroup>
                <FormGroup className="mb-2">
                  <Input
                    type="text"
                    name="empresa"
                    placeholder="Ingrese aqui el nombre de su empresa"
                  />
                </FormGroup>
                <FormGroup className="mb-1">
                  <span
                    style={{
                      fontWeight: "bold",
                    }}
                  >
                    NÚMERO DE TELEFONO
                  </span>{" "}
                  - (Opcional)
                </FormGroup>
                <FormGroup className="mb-2">
                  <Input
                    type="number"
                    name="telefono"
                    placeholder="Ingrese aqui su número de telefono"
                  />
                </FormGroup>
                <FormGroup style={{ fontWeight: "bold" }} className="mt-2 mb-1">
                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className="mx-auto  p-3 text-center"
                  >
                    <Button
                      type="submit"
                      className="btn-style-blue btn btn-success btn-sm"
                    >
                      Descargar cotizacion
                    </Button>
                  </Colxx>
                </FormGroup>
              </Form>

              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="pt-3  pb-3"
                style={{ textAlign: "center" }}
              >
                <hr className="ten" />
                <Button
                  color="success"
                  onClick={this.cotizacion}
                  className="btn-style"
                  size="sm"
                >
                  {" "}
                  VOLVER
                </Button>
              </Colxx>
            </Row>
          </ModalBody>
        </Modal>

        <ModalComment active={this.state.comment} toggle={this.commentary} />

        <ModalInfoPaso4
          active={this.state.infoPaso4}
          toggle={this.toggleinfoPaso4}
        />

        {this.state.desgloseValues.length > 0 && (
          <ModalQuoteResumen
            api={this.props.api}
            active={this.state.desglose}
            desglose={this.state.desgloseValues}
            resumen={this.state.resumen}
            token={this.state.token}
            toggle={this.desglose}
          />
        )}
        <BotChat />
      </Fragment>
    );
  }
}

export default withRouter(presupuesto);

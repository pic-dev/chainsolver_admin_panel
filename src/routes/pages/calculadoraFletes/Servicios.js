import React, { Component, Fragment, createRef } from "react";
import {
  Button,
  Col,
  Row,
  Label,
  FormGroup,
  UncontrolledTooltip,
  Modal,
  ModalBody,
  ModalFooter,
  FormText,
  Alert,
  CustomInput,
  Form,
  Input,
} from "reactstrap";
import CurrencyInput from "Util/CurrencyInput";
import { Link, Redirect,withRouter } from "react-router-dom";
import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";
import { Typeahead } from "react-bootstrap-typeahead";
import axios from "axios";
import TimeLine from "Util/timeLine";
import scrollIntoView from "scroll-into-view-if-needed";
import {
  firebaseConf,
  googleAuthProvider,
  persistencia,
} from "Util/Firebase";
import { errores, ErrorEspa, telefonoValid } from "Util/Utils";
import Errores from "Util/Errores";
import { PageView, Event } from "Util/tracking";
import Cadena from "Util/cadenaLogistica";
import { Beforeunload } from "react-beforeunload";
import { Title } from "Util/helmet";
import BotChat from "Util/chat";
const ref = createRef();
let timer;
var valid;
var validated;
class servicios extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleModalPreview = this.toggleModalPreview.bind(this);
    this.confirmation = this.confirmation.bind(this);
    this.handleFormState = this.handleFormState.bind(this);
    this.checkbox = this.checkbox.bind(this);
    this.get_ctz_resumen = this.get_ctz_resumen.bind(this);
    this.find_product_perm = this.find_product_perm.bind(this);
    this.get_prov = this.get_prov.bind(this);
    this.get_dist = this.get_dist.bind(this);
    this.array_js_to_pg = this.array_js_to_pg.bind(this);
    this.validation = this.validation.bind(this);
    this.showMessage = this.showMessage.bind(this);
    this.SelectedOnce = this.SelectedOnce.bind(this);
    this.socialLogin = this.socialLogin.bind(this);
    this.correoClave = this.correoClave.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.setsignup = this.setsignup.bind(this);
    this.seterror = this.seterror.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.invitado = this.invitado.bind(this);
    this.handleOnChangeProvi = this.handleOnChangeProvi.bind(this);

    this.state = {
      eventBandera: false,
      msjToggle: false,
      Signup: false,
      showPer: false,
      signInChoose: false,
      Choose: true,
      agotadoChoose: false,
      error: "",
      authAlert: false,
      isOpen: false,
      showMessage: false,
      IsVisiblePreview: false,
      confirmation: false,
      check: true,
      check1: true,
      check2: true,
      check3: true,
      check4: true,
      isLoggedIng: false,
      isLoggedInf: false,
      userID: "",
      name: "",
      email: "",
      picture: "",
      validated1: false,
      validated2: false,
      validated3: false,
      validated4: false,
      errorProv: true,
      productPermSelected: [],
      provinceSelected: [],
      districtSelected: [],
      province: [],
      district: [],
      productPerm: [],
      result: [],
      iconPuertoYalma: "icon-puertoYalma3",
      aduana: "icon-aduana2",
      Transporte: "icon-transporte2",
      animated: "icon-puertoYalmaAnimate",
      animated2: "icon-aduanaAnimate",
      animated3: "icon-transporteAnimate",
      animated4: "icon-seguroAnimate",
      form: [
        { Label: "value", value: "" },
        { Label: "origin", value: "" },
        { Label: "destination", value: "" },
        { Label: "freightType", value: "" },
        { Label: "container2", value: "" },
        { Label: "container2", value: "" },
      ],
    };
  }

  componentDidMount() {
    localStorage.page = "3";
     if (process.env.NODE_ENV == "production") {
      PageView();
    }
    localStorage.removeItem("token");
    if (localStorage.transporte == undefined) {
      return <Redirect to="/" />;
    } else {
      var element = document.getElementById("link3");

      scrollIntoView(element, {
        behavior: "smooth",
        block: "center",
        inline: "center",
      });
    }

    let destino = localStorage.destino;
    let tipoTransporte;
    if (localStorage.tipocon == "Contenedor completo") {
      tipoTransporte = "MARITIMO FCL";
    } else if (localStorage.tipocon == "Contenedor compartido") {
      tipoTransporte = "MARITIMO LCL";
    } else {
      tipoTransporte = "";
    }
    if (localStorage.transporte == "MARITÍMO") {
      axios
        .post(
          this.props.api + "maritimo_v2/get_prov_mar",
          { prov: [destino, tipoTransporte] }
        )
        .then((res) => {
          let response;
          let data = res.data.data.r_dat;
          let estado = res.data.data.r_suc;
          if (estado) {
            if (localStorage.puertoD == "PERÚ - OTRAS PROVINCIAS") {
              response = [
                ...data,
                { id: 15, nombre: "PERÚ - OTRAS PROVINCIAS" },
              ];
            } else {
              response = data;
            }
            this.setState({
              province: response,
            });
          } else {
            this.setState({
              errorProv: false,
            });
          }
        })
        .catch((error) => {
          console.log(error);
          errores("ApipostProvincias", error);
        });
    } else {
      axios
        .post(
          this.props.api + "aereo_v2/get_prov_aereo",
          { prov: [destino] }
        )
        .then((res) => {
          let response;
          let data = res.data.data.r_dat;
          let estado = res.data.data.r_suc;
          if (estado) {
            if (localStorage.puertoD == "PERÚ - OTRAS PROVINCIAS") {
              response = [
                ...data,
                { id: 15, nombre: "PERÚ - OTRAS PROVINCIAS" },
              ];
            } else {
              response = data;
            }

            this.setState({
              province: response,
            });
          } else {
            this.setState({
              errorProv: false,
            });
          }
        })
        .catch((error) => {
          errores("ApipostProvincias", error);
        });
    }
    // PERMISOS
    this.apiPost(
      this.props.api + "maritimo_v2/get_merca_per",
      { id_port: destino },
      this.find_product_perm
    );
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  EventProducto(x) {
    if (this.state.check2 === false) {
      switch (x) {
        case "1":
          switch (localStorage.tipocon) {
            case "Contenedor completo":
              if (this.state.eventBandera == false) {
                 /*Event(
                  "2.5 Producto FCL Solo Toque",
                  "2.5 Producto FCL Solo Toque",
                  "SERVICIO"
                );*/

                this.setState(() => ({
                  eventBandera: true,
                }));
              }
              break;
            case "Contenedor compartido":
              if (this.state.eventBandera == false) {
                 /*Event(
                  "1.6 Producto LCL Solo Toque",
                  "1.6 Producto LCL Solo Toque",
                  "SERVICIO"
                );*/
                this.setState(() => ({
                  eventBandera: true,
                }));
              }
              break;
            case "Contenedor Aereo":
              if (this.state.eventBandera == false) {
                 /*Event(
                  "3.6 Producto AEREO Solo Toque",
                  "3.6 Producto AEREO Solo Toque",
                  "SERVICIO"
                );*/
                this.setState(() => ({
                  eventBandera: true,
                }));
              }
              break;
            default:
          }
          break;
        case "2":
           /*switch (localStorage.tipocon) {
            case "Contenedor completo":
              Event(
                "2.6 Producto FCL Selección De Producto",
                "2.6 Producto FCL Selección De Producto",
                "SERVICIO"
              );
              break;
            case "Contenedor compartido":
              Event(
                "1.7 Producto LCL Selección De Producto",
                "1.7 Producto LCL Selección De Producto",
                "SERVICIO"
              );
              break;
            case "Contenedor Aereo":
              Event(
                "3.7 Producto AEREO Selección De Producto",
                "3.7 Producto AEREO Selección De Producto",
                "SERVICIO"
              );
              break;
            default:
          }*/
          break;
        default:
      }
    }
  }



  handleOnChangeProvi = (provinceSelected) => {
    ref.current.clear();
    this.setState(() => ({
      district: [],
    }));

    let tipoTransporte;
    if (localStorage.tipocon == "Contenedor completo") {
      tipoTransporte = "MARITIMO FCL";
    } else if (localStorage.tipocon == "Contenedor compartido") {
      tipoTransporte = "MARITIMO LCL";
    } else {
      tipoTransporte = "MARITIMO LCL";
    }

    if (provinceSelected.length > 0) {
      let id = provinceSelected.map(function (item) {
        return item.id.toString();
      });
      let nombre = provinceSelected.map(function (item) {
        return item.nombre.toString();
      });

      if (localStorage.transporte == "MARITÍMO") {
        axios
          .post(
            this.props.api + "maritimo_v2/get_dis_mar",
            { prov: [id[0], tipoTransporte] }
          )
          .then((res) => {
            let response;
            let data = res.data.data.r_dat;

            if (nombre == "PERÚ - OTRAS PROVINCIAS") {
              response = [
                { id: 20, nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL" },
              ];
              this.setState(() => ({
                msjToggle: true,
                districtSelected: [
                  { id: 20, nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL" },
                ],
                district: response,
              }));
            } else {
              this.setState(() => ({
                district: data,
              }));
            }
          })
          .catch((error) => {
            console.log(error);
            errores("ApipostDistritos", error);
          });
      } else {
        axios
          .post(
            this.props.api + "aereo_v2/get_dis_aereo",
            { prov: [id[0]] }
          )
          .then((res) => {
            let response;
            let data = res.data.data.r_dat;

            if (nombre == "PERÚ - OTRAS PROVINCIAS") {
              response = [
                { id: 20, nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL" },
              ];
              this.setState(() => ({
                msjToggle: true,
                districtSelected: [
                  { id: 20, nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL" },
                ],
                district: response,
              }));
            } else {
              this.setState(() => ({
                district: data,
              }));
            }
          })
          .catch((error) => {
            errores("ApipostDistritos", error);
          });

        // ***** EJEMPLO 3 llamada a url que recibe parametros, en este caso se asigna valor a la variable data
        // DISTRITOS

        // PERMISOS
      }
    }
  };
  handleOnChange = (input) => {
    if (input.value == "SI") {
      this.setState(() => ({
        check3: false,
      }));
    } else {
      this.setState(() => ({
        check3: true,
      }));
    }
  };
  SelectedOnce() {
    sessionStorage.confirmationNone = "1";
  }
  apiPost(url, datos, fn) {
    axios
      .post(url, datos)
      .then((res) => {
        const response = res.data.data.r_dat;
        //this.setState({ response });

        fn(response);
      })
      .catch((error) => {
        errores("ApipostServicios", error);
      });
  }
  get_dist(e) {
    this.setState(() => ({
      district: e,
    }));
  }
  setsignup(e) {
    this.setState(() => ({
      signup: e,
    }));
  }
  setsignIn(e) {
    this.setState(() => ({
      signIn: e,
    }));
  }
  seterror(e) {
    this.setState(() => ({
      error: e,
    }));
  }
  get_prov(fn) {
    const result = fn.filter(function (item) {
      return item.id == "15";
    });
    this.setState({
      province: result,
    });
  }
  showResult(fn) {
    this.setState({
      result: fn,
    });
  }
  find_product_perm(fn) {
    this.setState({
      productPerm: fn,
    });
  }
  get_ctz_resumen() {
    localStorage.distrito = this.state.districtSelected.map(function (item) {
      return item.id;
    });

    localStorage.regulador = this.state.productPermSelected.map(function (
      item
    ) {
      return item.ente_regulatorio;
    });
    localStorage.tipoMercancia = this.state.productPermSelected.map(function (
      item
    ) {
      return item.tipo_mercancia;
    });

    localStorage.distritoN = this.state.districtSelected.map(function (item) {
      return item.nombre;
    });
    localStorage.ProvinciaN = this.state.provinceSelected.map(function (item) {
      return item.nombre;
    });

    let regex = /(\d+)/g;
    let name = this.state.form[0].value;

    let x;
    if (this.state.form[0].value != 0) {
      localStorage.monto = name.match(regex).join("");
    } else {
      localStorage.monto = this.state.form[0].value;
    }

    localStorage.value4 = !this.state.check3;
    localStorage.value5 = !this.state.check;
    localStorage.value6 = !this.state.check2;
    localStorage.value7 = !this.state.check1;
    localStorage.value8 = !this.state.check4;
    localStorage.seguro = this.state.check4;
  }
  array_js_to_pg(array) {
    let retorno = "{";
    let x = 1;
    array.forEach((element) => {
      retorno += "{";
      for (let i = 0; i < element.length; i++) {
        retorno += element[i];
        if (i + 1 < element.length) retorno += ",";
      }
      retorno += "}";
      if (x < array.length) retorno += ",";
      x++;
    });
    retorno += "}";
    return retorno;
  }
  toggle() {
    this.setState((prevState) => ({
      isOpen: !prevState.isOpen,
    }));
  }
  checkbox(e) {
    switch (e) {
      case 0:
        if (this.state.iconPuertoYalma === "icon-puertoYalma3") {
          this.setState((prevState) => ({
            check: !prevState.check,
            iconPuertoYalma: "icon-puertoYalma4",
            animated: "icon-puertoYalmaAnimate2",
          }));
        } else {
          this.setState((prevState) => ({
            check: !prevState.check,
            iconPuertoYalma: "icon-puertoYalma3",
            animated: "icon-puertoYalmaAnimate",
          }));
        }
        if (this.state.Transporte === "icon-transporte2") {
          this.setState((prevState) => ({
            check1: !prevState.check1,
            Transporte: "icon-transporte4",
            animated3: "icon-transporteAnimate2",
          }));
        } else {
          this.setState((prevState) => ({
            check1: !prevState.check1,
            Transporte: "icon-transporte2",
            animated3: "icon-transporteAnimate",
          }));
        }
        if (this.state.aduana === "icon-aduana2") {
          this.setState((prevState) => ({
            check2: !prevState.check2,
            aduana: "icon-aduana4",
            animated2: "icon-aduanaAnimate2",
          }));
        } else {
          this.setState((prevState) => ({
            check2: !prevState.check2,
            aduana: "icon-aduana2",
            animated2: "icon-aduanaAnimate",
          }));
        }
        if (this.state.check4 === true) {
          this.setState((prevState) => ({
            animated4: "icon-seguroAnimate2",
            check4: !prevState.check4,
          }));
        } else {
          this.setState((prevState) => ({
            animated4: "icon-seguroAnimate",
            check4: !prevState.check4,
          }));
        }

        break;
      case 1:
        if (this.state.iconPuertoYalma === "icon-puertoYalma3") {
          this.setState((prevState) => ({
            check: !prevState.check,
            iconPuertoYalma: "icon-puertoYalma4",
            animated: "icon-puertoYalmaAnimate2",
          }));
        } else {
          this.setState((prevState) => ({
            check: !prevState.check,
            iconPuertoYalma: "icon-puertoYalma3",
            animated: "icon-puertoYalmaAnimate",
          }));
        }

        break;
      case 2:
        if (this.state.Transporte === "icon-transporte2") {
          this.setState((prevState) => ({
            check1: !prevState.check1,
            Transporte: "icon-transporte4",
            animated3: "icon-transporteAnimate2 ",
          }));
        } else {
          this.setState((prevState) => ({
            check1: !prevState.check1,
            Transporte: "icon-transporte2",
            animated3: "icon-transporteAnimate",
          }));
        }
        break;
      case 3:
        if (this.state.aduana === "icon-aduana2") {
          this.setState((prevState) => ({
            check2: !prevState.check2,
            aduana: "icon-aduana4",
            animated2: "icon-aduanaAnimate2 ",
            showPer: true,
          }));
          timer = setTimeout(() => {
            this.setState((ps) => ({ showPer: false }));
          }, 5000);
        } else {
          this.setState((prevState) => ({
            check2: !prevState.check2,
            showPer: false,
            aduana: "icon-aduana2",
            animated2: "icon-aduanaAnimate",
          }));
        }
        break;
      case 4:
        this.setState((prevState) => ({
          check3: !prevState.check3,
        }));
        break;
      case 5:
        if (this.state.check4 === true) {
          this.setState((prevState) => ({
            animated4: "icon-seguroAnimate2",
            check4: !prevState.check4,
          }));
        } else {
          this.setState((prevState) => ({
            animated4: "icon-seguroAnimate",
            check4: !prevState.check4,
          }));
        }

        break;
      default:
      // code block
    }
  }
  handleFormState(input) {
    let newState = this.state.form;

    newState[input.id].value = input.value;

    this.setState({
      form: newState,
    });
  }
  showMessage() {
    this.setState((ps) => ({ showMessage: true }));
    timer = setTimeout(() => {
      this.setState((ps) => ({ showMessage: false }));
    }, 10000);
  }
  toggleModalPreview() {
    //orifginal
    let data = JSON.parse(sessionStorage.getItem("user"));

    if (sessionStorage.usuario == "1" || data[1] == "Invitado") {
      if (sessionStorage.agotado == undefined) {
        sessionStorage.agotado = "1";
      } else if (sessionStorage.agotado == "1") {
        sessionStorage.agotado = "2";
      } else if (sessionStorage.agotado == "2") {
        sessionStorage.agotado = "3";
      } else {
        sessionStorage.agotado = "4";
      }
      localStorage.validLogin = "1";
      if (sessionStorage.usuario == "2" && sessionStorage.agotado !== "4") {
        this.setState({
          confirmation: false,
        });
        this.props.history.push("/Presupuesto");
      } else {
        /*this.setState({
          confirmation: false,
          IsVisiblePreview: !this.state.IsVisiblePreview,
        });*/
        this.setState({
          confirmation: false,
        });
        let user = [
          ["PIC"],
          ["Invitado"],
          ["Invitado"],
          ["/assets/img/profile-pic-l.png"],
        ];
        sessionStorage.setItem("user", JSON.stringify(user));

        sessionStorage.usuario = "2";
        this.props.history.push("/Presupuesto");
      }
    } else {
      this.setState({ confirmation: false });
      this.props.history.push("/Presupuesto");
    }
  }
  invitado = () => {
    Event("Ingresar como invitado", "Ingresar como invitado", "SERVICIOS");

    let user = [
      ["PIC"],
      ["Invitado"],
      ["Invitado"],
      ["/assets/img/profile-pic-l.png"],
    ];
    sessionStorage.setItem("user", JSON.stringify(user));

    sessionStorage.usuario = "2";
    this.setState({
      IsVisiblePreview: false,
    });

    this.props.history.push("/Presupuesto");
    window.location.reload();
  };
  validation() {
    valid = "1";
    if (this.state.signInChoose === true) {
      this.setState({
        signInChoose: false,
      });
      this.setsignup(false);
    }
    if (
      this.state.check2 === false &&
      this.state.form[0].value.length === 0 &&
      this.state.check2 === false &&
      this.state.productPermSelected.length === 0 &&
      this.state.check1 === false &&
      this.state.provinceSelected.length === 0 &&
      this.state.check1 === false &&
      this.state.districtSelected.length === 0
    ) {
      this.showMessage();
    } else if (
      this.state.check2 === false &&
      this.state.productPermSelected.length === 0
    ) {
      this.showMessage();
    } else if (
      this.state.check2 === false &&
      this.state.form[0].value.length === 0
    ) {
      this.showMessage();
    } else if (
      this.state.check4 === false &&
      this.state.form[0].value.length === 0
    ) {
      this.showMessage();
    } else if (
      this.state.check1 === false &&
      this.state.provinceSelected.length === 0
    ) {
      this.showMessage();
    } else if (
      this.state.check1 === false &&
      this.state.districtSelected.length === 0
    ) {
      this.showMessage();
    } else if (
      sessionStorage.confirmationNone !== "1" &&
      (this.state.check === true ||
        this.state.check1 === true ||
        this.state.check2 === true ||
        this.state.check4 === true)
    ) {
      this.get_ctz_resumen();
      this.confirmation();
    } else {
      this.get_ctz_resumen();
      this.toggleModalPreview();
    }
  }
  confirmation() {
    this.setState({
      confirmation: !this.state.confirmation,
    });
  }
  correoClave = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { usuario, clave } = e.target.elements;
    if (usuario.value.length > 0 && clave.value.length > 0) {
      await firebaseConf
        .auth()
        .signInWithEmailAndPassword(usuario.value, clave.value)
        .then((result) => {
          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));
          this.setState({
            IsVisiblePreview: false,
          });

          this.props.history.push("/Presupuesto");
        })
        .catch((error) => {
          sessionStorage.usuario = "2";
          let user = [
            ["PIC"],
            [usuario.value],
            [usuario.value],
            ["/assets/img/profile-pic-l.png"],
          ];
          sessionStorage.setItem("user", JSON.stringify(user));
          this.setState({
            IsVisiblePreview: false,
          });

          this.props.history.push("/Presupuesto");

          errores("Logincorreo", error);
        });
    } else {
      this.seterror("Debe llenar todos los campos requeridos");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    }
  };
  handleSignUp = async (e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    const { telefono, usuario, clave, Confirmacion } = e.target.elements;

    if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value === Confirmacion.value
    ) {
      await firebaseConf
        .auth()
        .createUserWithEmailAndPassword(usuario.value, clave.value)
        .then((result) => {
          if (telefono.value.length > 0) {
            if (telefonoValid(telefono.value)) {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              this.setState({
                IsVisiblePreview: false,
              });
              localStorage.telefono = telefono.value;

              timer = setTimeout(() => {
                this.props.history.push("/Presupuesto");
              }, 1000);
            } else {
              var alerta = "Verifique numero de telefono";
              alert(alerta);
            }
          } else {
            sessionStorage.usuario = "2";
            let user = [
              ["PIC"],
              [usuario.value],
              [usuario.value],
              ["/assets/img/profile-pic-l.png"],
            ];
            sessionStorage.setItem("user", JSON.stringify(user));
            this.setState({
              IsVisiblePreview: false,
            });
            timer = setTimeout(() => {
              this.props.history.push("/Presupuesto");
            }, 1000);
          }
        })
        .catch((error) => {
          if (telefono.value.length > 0) {
            if (telefono.value.length > 0 && telefonoValid(telefono.value)) {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                [usuario.value],
                [usuario.value],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));

              this.setState({
                IsVisiblePreview: false,
              });
              localStorage.telefono = telefono.value;

              timer = setTimeout(() => {
                this.props.history.push("/Presupuesto");
              }, 1000);
            } else {
              alerta = "Verifique numero de telefono";
              alert(alerta);
            }
          } else {
            sessionStorage.usuario = "2";
            let user = [
              ["PIC"],
              [usuario.value],
              [usuario.value],
              ["/assets/img/profile-pic-l.png"],
            ];
            sessionStorage.setItem("user", JSON.stringify(user));

            this.setState({
              IsVisiblePreview: false,
            });
            timer = setTimeout(() => {
              this.props.history.push("/Presupuesto");
            }, 1000);
          }

          errores("RegistroCorreo", error);
          this.seterror(ErrorEspa(error));
        });
    } else if (
      usuario.value.length > 0 &&
      clave.value.length > 0 &&
      Confirmacion.value.length > 0 &&
      clave.value !== Confirmacion.value
    ) {
      this.seterror("La contraseña ingresada no coincide con la confimación");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    } else {
      this.seterror("Debe llenar todos los campos requeridos");
      timer = setTimeout(() => {
        this.setState({
          error: "",
        });
      }, 4000);
    }
  };
  socialLogin = (provider, e) => {
    e.preventDefault();
    this.setState({
      error: "",
    });
    var x;
    firebaseConf
      .auth()
      .setPersistence(persistencia)
      .then(() => {
        (async () => {
          await firebaseConf
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
              /*Event(
                "GoogleLoginHeader",
                "Usuario ingresó con botton de google",
                "SERVICIOS"
              );*/
              let user = [
                [result.additionalUserInfo.profile.id],
                [result.additionalUserInfo.profile.name],
                [result.additionalUserInfo.profile.email],
                [result.additionalUserInfo.profile.picture],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
              sessionStorage.usuario = "2";

              this.props.history.push("/Presupuesto");
            })
            .catch((error) => {
              sessionStorage.usuario = "2";
              let user = [
                ["PIC"],
                ["Invitado"],
                ["Invitado"],
                ["/assets/img/profile-pic-l.png"],
              ];
              sessionStorage.setItem("user", JSON.stringify(user));
            });
        })();
      })
      .catch((error) => {});
  };
  eventVolver = () => {
    /*switch (localStorage.tipocon) {
      case "Contenedor completo":
        Event(
          "2.8 Volver a Servcios FCL",
          "2.8 Volver a Servcios FCL",
          "SERVICIOS"
        );
        break;
      case "Contenedor compartido":
        Event(
          "1.9 Volver a Servcios LCL",
          "1.9 Volver a Servcios LCL",
          "SERVICIOS"
        );
        break;
      case "Aereo":
        Event(
          "3.9 Volver a Servcios AEREO",
          "3.9 Volver a Servcios AEREO",
          "SERVICIOS"
        );
        break;

      default:
    }*/
  };
  eventIrPresupuestos = () => {
    /*switch (localStorage.tipocon) {
      case "Contenedor completo":
        Event(
          "2.9 Ir a Presupuestos FCL",
          "2.9 Ir a Presupuestos FCL",
          "SERVICIOS"
        );
        break;
      case "Contenedor compartido":
        Event(
          "1.9.1 Ir a Presupuestos LCL",
          "1.9.1 Ir a Presupuestos LCL",
          "SERVICIOS"
        );
        break;
      case "Aereo":
        Event(
          "3.9.1 Ir a Presupuestos AEREO",
          "3.9.1 Ir a Presupuestos AEREO",
          "SERVICIOS"
        );
        break;

      default:
    }*/
  };
  eventCalcularCotizacion = () => {
    /*if (localStorage.tipocon == "Contenedor compartido") {
      Event(
        "1.8 Calcular Cotizacion LCL",
        "1.8 Calcular Cotizacion LCL",
        "SERVICIOS"
      );
    } else if (localStorage.tipocon == "Contenedor completo") {
      Event(
        "2.7 Calcular Cotizacion FCL",
        "2.7 Calcular Cotizacion FCL",
        "SERVICIOS"
      );
    } else {
      Event(
        "3.8 Calcular Cotizacion Aereo",
        "3.8 Calcular Cotizacion Aereo",
        "SERVICIOS"
      );
    }*/
  };

  render() {
    localStorage.page = "3";
    if (localStorage.transporte == undefined) {
      return <Redirect to="/" />;
    } else {
    }
    //validaciones de inputs
    let selected;
    let selected2;
    let selected3;
    let selected4;

    if (valid === "1") {
      if (
        this.state.productPermSelected.length > 0 &&
        this.state.check2 === false
      ) {
        selected = false;
      } else {
        selected = true;
      }
    } else {
      selected = false;
    }

    if (valid === "1") {
      if (
        (this.state.form[0].value.length > 0 && this.state.check2 === false) ||
        (this.state.form[0].value.length > 0 && this.state.check4 === false)
      ) {
        selected2 = false;
      } else {
        selected2 = true;
      }
    } else {
      selected2 = false;
    }

    if (valid === "1") {
      if (
        this.state.provinceSelected.length > 0 &&
        this.state.check1 === false
      ) {
        selected3 = false;
      } else {
        selected3 = true;
      }
    } else {
      selected3 = false;
    }

    if (valid === "1") {
      if (
        this.state.districtSelected.length > 0 &&
        this.state.check1 === false
      ) {
        selected4 = false;
      } else {
        selected4 = true;
      }
    } else {
      selected4 = false;
    }

    if (
      selected === false &&
      selected2 == false &&
      selected3 === false &&
      selected4 === false &&
      valid === "1"
    ) {
      validated = "1";
    } else {
      validated = "2";
    }

    //validaciones de inputs fin

    //pasar valores de check a localstorage
    localStorage.check3 = !this.state.check3;
    localStorage.check = !this.state.check;
    localStorage.check2 = !this.state.check2;
    localStorage.check1 = !this.state.check1;
    localStorage.setItem("check4", JSON.stringify(this.state.check4));

    //pasar valores de check a localstorage fin

    //login
    let regulador;
    if (this.state.productPermSelected.length > 0) {
      regulador = this.state.productPermSelected.map(function (item) {
        return item.ente_regulatorio;
      });
    } else {
    }

    return (
      <Fragment>
             
             <Title>
          <title>{((window.location.pathname).replace('/', '') )+ " | " + "PIC - Calculadora de Fletes"}</title>
        </Title>
        <span id="link3"></span>
        {/*paso 3 */}
        <div className="container2 ">
          {this.state.showMessage ? (
            <div className=" alert alert-danger arial " role="alert">
              Seleccionó algunos servicios. Complete los datos requeridos o
              desmarque los mismos.
            </div>
          ) : (
            ``
          )}

          <Colxx xxs="12" md="12" className="mx-auto  mb-3 ">
            <Link to="/Ruta">
              {" "}
              <Button
                color="success"
                style={{ fontSize: "0.8rem", padding: "1px" }}
                className="btn-style"
                size="sm"
              >
                <i className="simple-icon-arrow-left-circle" /> VOLVER
              </Button>
            </Link>
          </Colxx>
          <Row className="h-100 ">
            <Colxx xl="10" lg="12" className="mx-auto NoPadding">
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="mx-auto my-auto NoPadding "
              >
                <Colxx xxs="12" md="12" className="mx-auto  ">
                  <span className="step-style">Resumen de Ruta</span>{" "}
                </Colxx>
                <Colxx xxs="12" md="12" className="mx-auto mb-1 ">
                  {" "}
                  <span className="color-blue">
                    <div className="arial font2">
                      {" "}
                      Flete{" "}
                      <span
                        style={{
                          display:
                            localStorage.transporte == "AÉREO" ? "none" : "",
                        }}
                      >
                        {" "}
                        Maritimo
                      </span>
                      <span
                        style={{
                          display:
                            localStorage.transporte == "AÉREO" ? "" : "none",
                        }}
                      >
                        {" "}
                        Aéreo
                      </span>{" "}
                    </div>{" "}
                    <div>
                      {localStorage.puerto} → {localStorage.puertoD}
                    </div>
                    <span
                      className="font2"
                      style={{
                        display:
                          localStorage.tipocon === "Contenedor completo" &&
                          localStorage.transporte !== "AÉREO"
                            ? ""
                            : "none",
                      }}
                    >
                      <div>{localStorage.contenedoresDes}</div>
                    </span>
                    <div
                      className="font2"
                      style={{
                        display:
                          localStorage.tipocon === "Contenedor compartido" &&
                          localStorage.transporte !== "AÉREO"
                            ? ""
                            : "none",
                      }}
                    >
                      {localStorage.contenedoresDes}
                    </div>
                    <div
                      className="font2"
                      style={{
                        display:
                          localStorage.transporte == "AÉREO" ? "" : "none",
                      }}
                    >
                      {localStorage.contenedoresDes}
                    </div>
                  </span>
                </Colxx>
                <Colxx xxs="12" md="12" className="mx-auto mt-1 mb-3 ">
                  <span className="step-style">
                    PASO 3. Agrega Servicios Adicionales
                  </span>{" "}
                </Colxx>

                <Colxx xxs="12" md="7" className="mx-auto mb-3 NoPadding">
                  <Alert
                    style={{ fontSize: "1.1rem" }}
                    color="primary"
                    className="NoPadding"
                  >
                    <i className="simple-icon-check" /> Selecciona 1 o
                    <span
                      onClick={() => this.checkbox(0)}
                      style={{
                        fontWeight: "bold",
                        textDecoration: "underline",
                        color: "#002752",
                        cursor: "pointer",
                      }}
                    >
                      {" "}
                      MÁS{" "}
                    </span>{" "}
                    Servicios Adicionales
                  </Alert>
                </Colxx>

                <Col className="mx-auto my-auto" xxs="12" md="7" sm="12">
                  <div className="included-services">
                    <ul className="included-list">
                      <li className="included-item-wrapper">
                        <div className="included-item">
                          <div className="included-checkbox">
                            <CustomInput
                              style={{ padding: "0px" }}
                              checked={!this.state.check}
                              onChange={() => this.checkbox(1)}
                              value={this.state.check}
                              type="checkbox"
                              id="exampleCustomCheckbox4"
                            />
                          </div>
                          <div className="included-content">
                            <Label htmlFor="filter-1-2">
                              <i className={this.state.iconPuertoYalma} />
                              <i className={this.state.animated} />
                              <b
                                style={{
                                  color:
                                    this.state.check === true ? "" : "#11a017",
                                }}
                                onClick={() => this.checkbox(1)}
                              >
                                {" "}
                                <span
                                  style={{
                                    display:
                                      localStorage.transporte == "AÉREO"
                                        ? "none"
                                        : "",
                                  }}
                                >
                                  Gastos portuarios y de Almacenamiento Aduanero
                                  &nbsp;
                                </span>
                                <span
                                  style={{
                                    display:
                                      localStorage.transporte == "AÉREO"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  Gasto Aeroportuario y Almacenamiento
                                  Aduanero&nbsp;
                                </span>
                                <i
                                  className="simple-icon-question question-style2"
                                  id="Tooltip1"
                                />
                                &nbsp;
                                <i
                                  className="simple-icon-check check-style"
                                  style={{
                                    display:
                                      this.state.check === true
                                        ? "none"
                                        : "initial",
                                  }}
                                />
                              </b>

                              <UncontrolledTooltip
                                placement="right"
                                target="Tooltip1"
                              >
                                <div className="color-blue text-justify">
                                  <span style={{ fontWeight: "bold" }}>
                                    NOTA
                                  </span>
                                  :Toda mercancia al momento de arribar a
                                  Territorio Nacional, es almacenada en un
                                  Deposito Temporal Aduanero, y este genera
                                  gastos desde el primer dia de almacenamiento.
                                  De no seleccionar esta opcion sus costos de
                                  importación no estaran completos.
                                </div>
                                <div className="color-blue text-justify">
                                  En Peru se Cobran los siguientes ítems
                                  dependiendo de la terminal portuaria y/o
                                  Almacen: Thcd, Vistos Buenos, Descarga entre
                                  otros.
                                </div>
                              </UncontrolledTooltip>
                            </Label>
                          </div>
                        </div>
                      </li>

                      <li className="included-item-wrapper">
                        <div className="included-item">
                          <div className="included-checkbox">
                            <CustomInput
                              style={{ padding: "0px" }}
                              checked={!this.state.check2}
                              onChange={() => this.checkbox(3)}
                              value={this.state.check2}
                              type="checkbox"
                              id="exampleCustomCheckbox3"
                            />
                          </div>
                          <div className="included-content">
                            <Label htmlFor="filter-1-2">
                              <i className={this.state.aduana} />
                              <i className={this.state.animated2} />
                              <b onClick={() => this.checkbox(3)}>
                                <span
                                  style={{
                                    fontWeight: "bold",
                                    color:
                                      this.state.check2 === true
                                        ? "#0d5084"
                                        : "#11a017",
                                  }}
                                >
                                  Cálculo de impuestos y permisos de
                                  aduana&nbsp;
                                  <i
                                    className="simple-icon-question question-style2"
                                    id="Tooltip3"
                                  />
                                  &nbsp;
                                </span>
                                <i
                                  className="simple-icon-check check-style"
                                  style={{
                                    display:
                                      this.state.check2 === true
                                        ? "none"
                                        : "initial",
                                  }}
                                />
                              </b>
                              <UncontrolledTooltip
                                placement="right"
                                target="Tooltip3"
                              >
                                <div className="color-blue text-justify">
                                  Impuesto que fija el Gobierno Nacional para
                                  que le producto pueda ingresar al país
                                  destino.
                                </div>
                              </UncontrolledTooltip>
                            </Label>
                          </div>
                        </div>
                      </li>

                      <li
                        style={{
                          display:
                            this.state.check2 === true ? "none" : "block",
                        }}
                        className="included-item-wrapper"
                      >
                        {" "}
                        <div className="included-item">
                          <div className="included-content2 ">
                            <Row>
                              <Colxx
                                xxs="12"
                                md="7"
                                sm="4"
                                className="mb-1"
                                style={{ marginLeft: "auto" }}
                              >
                                <Typeahead
                                  isValid={selected}
                                  flip={true}
                                  id="2"
                                  autoFocus={false}
                                  labelKey={(option) =>
                                    `${option.tipo_mercancia}`
                                  }
                                  renderMenuItemChildren={(option) => (
                                    <div
                                      style={{
                                        color:
                                          option.tipo_mercancia ===
                                          "OTROS PRODUCTOS / CARGA GENERAL"
                                            ? "red"
                                            : "unset",
                                        fontWeight:
                                          option.tipo_mercancia ===
                                          "OTROS PRODUCTOS / CARGA GENERAL"
                                            ? "bold"
                                            : "unset",
                                      }}
                                    >
                                      {option.tipo_mercancia}
                                      <div>
                                        <small>
                                          Regulador: {option.ente_regulatorio}
                                        </small>
                                      </div>
                                    </div>
                                  )}
                                  multiple={false}
                                  emptyLabel="Si Producto no aparece, elegir CARGA GENERAL"
                                  options={this.state.productPerm}
                                  placeholder="Producto"
                                  onChange={(productPermSelected) => {
                                    this.setState({ productPermSelected });
                                    this.EventProducto("2");
                                  }}
                                  onFocus={() => this.EventProducto("1")}
                                  selected={this.state.productPermSelected}
                                />
                                <div
                                  className="bg-white"
                                  style={{
                                    left: "0.5rem",
                                  }}
                                  onClick={() =>
                                    this.setState((prevState) => ({
                                      showPer: !prevState.showPer,
                                    }))
                                  }
                                  className={`up-izquierda2  ${
                                    this.state.showPer === false
                                      ? "hide-toolkit"
                                      : "show-toolkit-cont2"
                                  }`}
                                >
                                  <div
                                    style={{
                                      borderRadius: "2rem",
                                    }}
                                  >
                                    <i
                                      onClick={() =>
                                        this.setState((prevState) => ({
                                          showPer: !prevState.showPer,
                                        }))
                                      }
                                      className="plusLump  simple-icon-close "
                                      style={{
                                        fontSize: "2em",
                                        position: "absolute",
                                        right: "0.5em",
                                        top: "0.2em",
                                        cursor: "pointer",
                                      }}
                                    />
                                    <Row
                                      style={{ height: "auto" }}
                                      className="NoPadding theme-color-blueHeader arial"
                                    >
                                      <Colxx xxs="12" md="12">
                                        <div>
                                          <i
                                            style={{
                                              borderRadius: "10rem",
                                              padding: "0.5rem",
                                              backgroundColor: "white",
                                              height: "5em",
                                              left: "0em",
                                            }}
                                            className="icon-list"
                                          />
                                        </div>
                                      </Colxx>
                                      <Colxx
                                        className="text-center"
                                        xxs="12"
                                        md="12"
                                      >
                                        {" "}
                                        <div style={{ fontSize: "1rem" }}>
                                          Si tu producto no aparece en el
                                          listado elegir:
                                        </div>
                                      </Colxx>
                                    </Row>
                                    <Row
                                      style={{ height: "auto" }}
                                      className="NoPadding theme-color-orange  arial"
                                    >
                                      <Colxx
                                        className="text-center"
                                        xxs="12"
                                        md="12"
                                      >
                                        {" "}
                                        <div
                                          style={{
                                            fontSize: "1rem",
                                            textDecoration: "underline",
                                          }}
                                        >
                                          OTROS PRODUCTOS / CARGA GENERAL:
                                        </div>
                                      </Colxx>
                                    </Row>
                                  </div>
                                </div>
                                {this.state.productPermSelected.length > 0 ? (
                                  <FormText className="regulation-text">
                                    Permiso Gubernamental Adicional:
                                    {regulador}
                                  </FormText>
                                ) : (
                                  <FormText
                                    style={{
                                      display: selected === false ? "" : "none",
                                    }}
                                    className="validation-text"
                                  >
                                    Si Tu Producto NO APARECE elegir CARGA
                                    GENERAL.
                                  </FormText>
                                )}
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected === false ? "none" : "",
                                  }}
                                >
                                  Si Tu Producto NO APARECE elegir CARGA
                                  GENERAL.
                                </FormText>
                              </Colxx>
                              <Colxx
                                xxs="12"
                                md="5"
                                sm="4"
                                style={{ marginRight: "auto" }}
                              >
                                <CurrencyInput
                                  type="text"
                                  className="form-control"
                                  invalid={selected2.toString()}
                                  name="value"
                                  id="value2"
                                  placeholder="Valor de mercancia $"
                                  value={this.state.form[0].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 0,
                                      value: e.target.value || "",
                                    });
                                  }}
                                />

                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected2 == false ? "none" : "",
                                  }}
                                >
                                  Debe ingresar el valor de la mercancia
                                </FormText>
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected2 == false ? "" : "none",
                                  }}
                                >
                                  Ingresar valor exacto, SIN DECIMALES
                                </FormText>
                              </Colxx>
                            </Row>
                          </div>
                        </div>
                      </li>
                      <span
                        style={{
                          display:
                            localStorage.paisDestino == "PERU"
                              ? ""
                              : "none",
                        }}
                      >
                        <li
                          style={{
                            display: this.state.check2 === true ? "none" : "",
                          }}
                          className="included-item-wrapper"
                        >
                          <div className="included-item">
                            <div className="included-checkbox">
                              <Input
                                defaultValue="NO"
                                onChange={(e) => {
                                  this.handleOnChange({
                                    value: e.target.value || "",
                                  });
                                }}
                                type="select"
                                name="select"
                                id="exampleSelect"
                              >
                                <option key="SI" value="SI">
                                  SI
                                </option>
                                <option key="NO" value="NO">
                                  NO
                                </option>
                              </Input>
                            </div>
                            <div className="included-content">
                              <Label htmlFor="filter-1-2">
                                <b
                                  style={{
                                    color:
                                      this.state.check3 === true
                                        ? ""
                                        : "#11a017",
                                  }}
                                >
                                  ¿Ha realizado importaciones previamente ?{" "}
                                  &nbsp;
                                  <i
                                    className="simple-icon-question question-style2"
                                    id="Tooltip2"
                                  />
                                  <UncontrolledTooltip
                                    placement="right"
                                    target="Tooltip2"
                                  >
                                    <div className="color-blue text-justify">
                                      <span style={{ fontWeight: "bold" }}>
                                        NOTA
                                      </span>
                                      : Por ley de Aduana, los impuestos varian
                                      la primera vez que se importa ( No se
                                      considera importaciones envios via Dhl,
                                      Serport, Fedex o Ups), se requiere esta
                                      informacion para dar un calculo de
                                      impuestos más exacto.
                                    </div>
                                  </UncontrolledTooltip>
                                  <i
                                    className="simple-icon-check check-style"
                                    style={{
                                      display:
                                        this.state.check3 === true
                                          ? "none"
                                          : "initial",
                                    }}
                                  />
                                </b>
                              </Label>
                            </div>
                          </div>
                        </li>
                      </span>
                      <li
                        className="included-item-wrapper"
                        style={{
                          display: this.state.errorProv === true ? "" : "none",
                        }}
                      >
                        <div className="included-item">
                          <div className="included-checkbox">
                            <CustomInput
                              style={{ padding: "0px" }}
                              checked={!this.state.check1}
                              onChange={() => this.checkbox(2)}
                              value={this.state.check1}
                              type="checkbox"
                              id="exampleCustomCheckbox2"
                            />
                          </div>
                          <div className="included-content">
                            <Label htmlFor="filter-1-2">
                              <i className={this.state.Transporte} />
                              <i className={this.state.animated3} />
                              <b
                                style={{
                                  color:
                                    this.state.check1 === true ? "" : "#11a017",
                                }}
                                onClick={this.checkbox1}
                              >
                                {" "}
                                Transporte a domicilio&nbsp;
                                <i
                                  className="simple-icon-question question-style2"
                                  id="Tooltip4"
                                />
                                &nbsp;
                                <i
                                  className="simple-icon-check check-style"
                                  style={{
                                    display:
                                      this.state.check1 === true
                                        ? "none"
                                        : "initial",
                                  }}
                                />
                                <UncontrolledTooltip
                                  placement="right"
                                  target="Tooltip4"
                                >
                                  <div className="color-blue text-justify">
                                    Servicio de Transporte desde Puerto o
                                    Almacén portuario hasta las instalaciones
                                    del Importador o Exportador.
                                  </div>
                                </UncontrolledTooltip>
                              </b>
                            </Label>
                          </div>
                        </div>
                      </li>
                      <li
                        style={{
                          display:
                            this.state.check1 === true ? "none" : "block",
                        }}
                        className="included-item-wrapper"
                      >
                        <div className="included-item">
                          <div className="included-content2 ">
                            <Row>
                              <Colxx
                                xxs="12"
                                md="6"
                                sm="4"
                                className="mb-1"
                                style={{ marginLeft: "auto" }}
                              >
                                <Typeahead
                                  isValid={selected3}
                                  flip={true}
                                  id="3"
                                  labelKey={(option) => `${option.nombre}`}
                                  renderMenuItemChildren={(option) => (
                                    <div>
                                      {option.nombre}
                                      <div>
                                        <small>
                                          Pais:{localStorage.paisDestino}
                                        </small>
                                      </div>
                                    </div>
                                  )}
                                  multiple={false}
                                  options={this.state.province}
                                  placeholder="Provincia"
                                  onChange={(provinceSelected) => {
                                    this.setState({ provinceSelected });
                                    this.handleOnChangeProvi(provinceSelected);
                                  }}
                                  selected={this.state.provinceSelected}
                                />
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected3 === false ? "none" : "",
                                  }}
                                >
                                  Debe seleccionar la provincia
                                </FormText>
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected3 === false ? "none" : "",
                                  }}
                                ></FormText>
                              </Colxx>
                              <Colxx
                                xxs="12"
                                md="6"
                                sm="4"
                                style={{ marginRight: "auto" }}
                              >
                                <Typeahead
                                  isValid={selected4}
                                  flip={true}
                                  id="4"
                                  labelKey={(option) => `${option.nombre}`}
                                  renderMenuItemChildren={(option) => (
                                    <div>{option.nombre}</div>
                                  )}
                                  multiple={false}
                                  options={this.state.district}
                                  placeholder="Distrito"
                                  onChange={(districtSelected) => {
                                    this.setState({ districtSelected });
                                  }}
                                  selected={this.state.districtSelected}
                                  ref={ref}
                                />
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected4 === false ? "none" : "",
                                  }}
                                >
                                  Debe seleccionar el distrito
                                </FormText>
                              </Colxx>
                              {this.state.msjToggle ? (
                                <FormText>
                                  <Errores
                                    mensaje={
                                      "Los Centro de Recepcion de las Agencias de Envios Interprovinciales se encuentran principalmente en Lima - La Victoria,  se calculara hasta ese Distrito."
                                    }
                                  />
                                </FormText>
                              ) : null}
                            </Row>
                          </div>
                        </div>
                      </li>
                      <li className="included-item-wrapper">
                        <div className="included-item">
                          <div className="included-checkbox">
                            <CustomInput
                              style={{ padding: "0px" }}
                              checked={!this.state.check4}
                              onChange={() => this.checkbox(5)}
                              value={this.state.check4}
                              type="checkbox"
                              id="exampleCustomCheckbox"
                            />
                          </div>
                          <div className="included-content">
                            <Label htmlFor="filter-1-2">
                              <i className={this.state.animated4} />
                              <i
                                className="simple-icon-lock"
                                style={{
                                  color:
                                    this.state.check4 === true
                                      ? "#b5b5b5"
                                      : "#11a017",
                                }}
                              />

                              <b
                                style={{
                                  color:
                                    this.state.check4 === true ? "" : "#11a017",
                                }}
                                onClick={() => this.checkbox(5)}
                              >
                                {" "}
                                Seguro de mercancia&nbsp;
                                <i
                                  className="simple-icon-question question-style2"
                                  id="Tooltip5"
                                />
                                &nbsp;
                                <i
                                  className="simple-icon-check check-style"
                                  style={{
                                    display:
                                      this.state.check4 === true
                                        ? "none"
                                        : "initial",
                                  }}
                                />
                              </b>
                              <UncontrolledTooltip
                                placement="right"
                                target="Tooltip5"
                              >
                                <div className="color-blue text-justify">
                                  Las navieras y aerolíneas operan bajo
                                  condiciones que limitan su responsabilidad en
                                  casos de pérdida, daño o demora.{" "}
                                </div>
                                <div className="color-blue text-justify">
                                  Existen factores que no son controlados por el
                                  operador logísticos, asi como los llamados
                                  <span style={{ color: "red" }}>
                                    {" "}
                                    Actos de Dios
                                  </span>
                                  , los cual en caso de suceder si Ud ha
                                  adquirido Seguro contra Perdida Total,
                                  <span style={{ color: "red" }}>
                                    estará asegurando su inversión y/o producto
                                  </span>{" "}
                                  una vez pagado su deducible correspondiente.{" "}
                                </div>
                              </UncontrolledTooltip>
                            </Label>
                          </div>
                        </div>
                      </li>
                      <li
                        style={{
                          display:
                            this.state.check4 === true ? "none" : "block",
                        }}
                        className="included-item-wrapper"
                      >
                        {" "}
                        <div
                          style={{
                            display:
                              this.state.check2 === false ? "none" : "block",
                          }}
                          className="included-item"
                        >
                          <div className="included-content2 ">
                            <Row>
                              {" "}
                              <Colxx
                                xxs="12"
                                md="12"
                                sm="12"
                                style={{ marginRight: "auto" }}
                              >
                                <CurrencyInput
                                  type="text"
                                  className="form-control"
                                  invalid={selected2.toString()}
                                  name="value"
                                  id="value"
                                  placeholder="Valor de mercancia $"
                                  value={this.state.form[0].value}
                                  onChange={(e) => {
                                    this.handleFormState({
                                      id: 0,
                                      value: e.target.value || "",
                                    });
                                  }}
                                />

                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected2 == false ? "none" : "",
                                  }}
                                >
                                  Debe ingresar el valor de la mercancia
                                </FormText>
                                <FormText
                                  className="validation-text"
                                  style={{
                                    display: selected2 == false ? "" : "none",
                                  }}
                                >
                                  Ingresar valor exacto, SIN DECIMALES
                                </FormText>
                              </Colxx>
                            </Row>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Colxx>
              <Colxx xxs="12" md="12" sm="12" className="mx-auto mb-2  ">
                <Button
                  size="sm"
                  color="success"
                  className="mb-3  mx-auto btn-style-blue"
                  onClick={() => {
                    this.validation();
                    this.eventCalcularCotizacion();
                  }}
                >
                  CALCULAR COTIZACIÓN
                </Button>
              </Colxx>
              <Cadena />

              <TimeLine />
            </Colxx>
          </Row>
        </div>

        <Modal
          isOpen={this.state.IsVisiblePreview}
          toggle={this.toggleModalPreview}
          backdrop={false}
          size="md"
          className="modalConfirmatinoWidth"
          style={{ borderRadius: "1.1rem", marginTop: "65px !important" }}
        >
          {sessionStorage.agotado !== "4" ? (
            <ModalBody style={{ padding: "0rem" }}>
              {!this.state.signInChoose ? (
                <Row style={{ padding: "0rem" }}>
                  <i
                    onClick={this.toggleModalPreview}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡IMPORTANTE!</span>
                    </div>
                  </Colxx>

                  <Row className="p-2 text-center">
                    <Colxx xs="12">
                      <Form className=" login-form">
                        <div style={{ fontSize: "1.3rem" }}>
                          <FormGroup
                            style={{ fontWeight: "bold" }}
                            className="mt-2 mb-1"
                          >
                            <div>
                              <i className="icon-list" />
                              <span>
                                <span className="color-blue">Para&nbsp;</span>
                                <span className="color-green">
                                  formalizar&nbsp;
                                </span>
                                <span className="color-blue">
                                  tu oferta&nbsp;
                                </span>
                                <span className="color-orange">
                                  REGÍSTRATE&nbsp;
                                </span>
                              </span>
                            </div>
                          </FormGroup>
                          <hr className="ten" />
                          <FormGroup className="mt-1 mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Sin Formularios
                              </span>
                            </div>
                          </FormGroup>
                          <FormGroup className=" mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Sin TDC ni pagos
                              </span>
                            </div>
                          </FormGroup>
                          <FormGroup className=" mb-1">
                            <div>
                              <i className="simple-icon-check color-green" />{" "}
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Solo con tu email
                              </span>
                            </div>
                          </FormGroup>
                          <hr className="ten" />
                          <Colxx
                            xxs="12"
                            md="12"
                            sm="12"
                            className="p-2"
                            style={{ textAlign: "center" }}
                          >
                            <Button
                              color="success"
                              onClick={this.invitado}
                              className="btn-style-grey mr-1"
                              size="sm"
                            >
                              INGRESAR COMO INVITADO
                            </Button>
                            <Button
                              color="success"
                              onClick={() =>
                                this.setState(() => ({
                                  signInChoose: true,
                                }))
                              }
                              className="btn-style"
                              size="sm"
                            >
                              REGISTRARME
                            </Button>
                          </Colxx>
                          <hr className="ten" />
                          <FormGroup
                            style={{ fontSize: "0.9rem" }}
                            className="text-center mb-1"
                          >
                            Ya tengo una cuenta -&nbsp;
                            <span
                              className="arial"
                              onClick={() =>
                                this.setState(() => ({
                                  signInChoose: true,
                                }))
                              }
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </div>
                      </Form>
                    </Colxx>
                  </Row>
                </Row>
              ) : (
                <Row style={{ padding: "0rem" }}>
                  <i
                    onClick={this.toggleModalPreview}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡INGRESA&nbsp;</span>
                      <span
                        className="bggradien-orange color-white"
                        style={{
                          fontWeight: "bold",
                          borderRadius: "5px",
                        }}
                      >
                        &nbsp;GRATIS!&nbsp;
                      </span>
                    </div>
                  </Colxx>

                  <Row className="mx-auto p-2">
                    <Colxx xs="12">
                      {this.state.signup ? (
                        <Form
                          className="login-form"
                          onSubmit={this.correoClave}
                        >
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                          <div
                            className="paddingLogin text-center"
                            style={{ textAling: "center" }}
                          >
                            <FormGroup className="mb-2 font2 color-blue">
                              <span>Iniciar sesión</span>
                            </FormGroup>

                            <FormGroup className="mb-2">
                              <Button
                                className="GLogin "
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider, e)
                                }
                              >
                                <i className="icon-google" />
                                Ingresa con Google
                              </Button>
                            </FormGroup>
                            <FormGroup className="mb-2 arial">
                              <span>-o-</span>
                            </FormGroup>
                            <FormGroup>
                              <Input
                                type="email"
                                name="usuario"
                                placeholder="Correo electrónico"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Input
                                name="clave"
                                type="password"
                                placeholder="Clave"
                              />
                            </FormGroup>
                            <FormGroup className="login-form-button">
                              <Button
                                type="submit"
                                className="btn-style-blue btn btn-success btn-sm"
                              >
                                Iniciar sesión
                              </Button>
                              &nbsp; O &nbsp;
                              <span
                                className="arial"
                                onClick={() => {
                                  this.setsignup(false);
                                }}
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  color: "rgb(54, 94, 153)",
                                  textDecoration: "underline",
                                }}
                              >
                                Registrate
                              </span>
                            </FormGroup>
                          </div>
                        </Form>
                      ) : (
                        <Form
                          className="paddingLogin text-center"
                          onSubmit={this.handleSignUp}
                        >
                          {this.state.Choose ? (
                            <div>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2  "
                              >
                                <span className="color-blue">INGRESA </span>{" "}
                                <span className="color-black">TUS DATOS</span>
                              </FormGroup>
                              {this.state.error ? (
                                <FormGroup>
                                  <Errores mensaje={this.state.error} />
                                </FormGroup>
                              ) : null}
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="arial color-orange ">
                                  PASO 1 -{" "}
                                </span>
                                <span>Número de teléfono -</span>{" "}
                                <span className="color-grey">(Opcional)</span>
                              </FormGroup>
                              <FormGroup className="mb-3">
                                <Input
                                  type="text"
                                  name="telefono"
                                  placeholder="Ingrese aqui su número de telefono"
                                />
                              </FormGroup>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="arial color-orange">
                                  PASO 2 -{" "}
                                </span>
                                <span>Correo electrónico</span>
                              </FormGroup>

                              <FormGroup className="mb-2">
                                <Button
                                  style={{
                                    borderRadius: "13px",
                                    fontSize: "14px",
                                  }}
                                  className="GLogin"
                                  onClick={(e) =>
                                    this.socialLogin(googleAuthProvider, e)
                                  }
                                >
                                  <i className="icon-gmail" />
                                  <span>Gmail,&nbsp;</span>{" "}
                                  <span
                                    className="color-orange"
                                    style={{
                                      fontWeight: "bold",
                                      cursor: "pointer",
                                      textDecoration: "underline",
                                    }}
                                  >
                                    solo con un click
                                  </span>
                                </Button>
                              </FormGroup>
                              <FormGroup className="mb-2">
                                <span>o con otro </span>
                                <span
                                  onClick={() =>
                                    this.setState(() => ({
                                      Choose: false,
                                    }))
                                  }
                                  className="color-blue"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  correo electrónico
                                </span>
                              </FormGroup>
                              <hr className="ten" />
                              <FormGroup className="mt-2 mb-2">
                                &nbsp; Ya tengo una cuenta - &nbsp;
                                <span
                                  onClick={() => {
                                    this.setsignup(true);
                                  }}
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  INICIAR SESIÓN
                                </span>
                              </FormGroup>
                            </div>
                          ) : (
                            <div>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2 font2 "
                              >
                                <span className="color-blue">INGRESA </span>{" "}
                                <span className="color-black"> TUS DATOS</span>
                              </FormGroup>
                              {this.state.error ? (
                                <FormGroup>
                                  <Errores mensaje={this.state.error} />
                                </FormGroup>
                              ) : null}
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-1"
                              >
                                <span className="color-orange ">PASO 1 - </span>
                                <span>Número de teléfono -</span>{" "}
                                <span className="color-grey">(Opcional)</span>
                              </FormGroup>
                              <FormGroup className="mb-3">
                                <Input
                                  type="text"
                                  name="telefono"
                                  placeholder="Ingrese aqui su número de telefono"
                                />
                              </FormGroup>
                              <FormGroup
                                style={{
                                  fontWeight: "bold",
                                }}
                                className="mb-2"
                              >
                                <span className="color-orange">PASO 2 - </span>
                                <span>Correo electrónico</span>
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  type="email"
                                  name="usuario"
                                  placeholder="Correo electrónico"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  name="clave"
                                  type="password"
                                  placeholder="Clave"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  name="Confirmacion"
                                  type="password"
                                  placeholder="Confirma tu Clave"
                                />
                              </FormGroup>
                              <FormGroup>
                                <Button
                                  type="submit"
                                  className="btn-style-blue btn btn-success btn-sm"
                                >
                                  Registrate
                                </Button>
                              </FormGroup>
                              <hr className="ten" />
                              <FormGroup className="mt-2 mb-2">
                                &nbsp; Ya tengo una cuenta - &nbsp;
                                <span
                                  onClick={() => {
                                    this.setsignup(true);
                                  }}
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  INICIAR SESIÓN
                                </span>
                              </FormGroup>
                            </div>
                          )}
                        </Form>
                      )}
                    </Colxx>
                  </Row>
                </Row>
              )}
            </ModalBody>
          ) : (
            <ModalBody style={{ padding: "0rem" }}>
              {!this.state.agotadoChoose ? (
                <div style={{ padding: "0rem" }}>
                  <i
                    onClick={this.toggleModalPreview}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <div>
                        Has agotado el máximo de consultas como invitado
                      </div>
                    </div>
                  </Colxx>

                  <Row className="p-2 text-center">
                    <Colxx xs="12">
                      <Form className=" login-form">
                        <div style={{ fontSize: "0.9rem" }}>
                          <FormGroup className="mt-1 mb-2">
                            <div className="mb-3">
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Opción 1 -
                              </span>{" "}
                              Consultas ilimitadas Registrate{" "}
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                {" "}
                                GRATIS
                              </span>
                            </div>
                            <div className="mb-1">
                              <span
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                Nombre de empresa
                              </span>{" "}
                              - (Opcional)
                            </div>
                            <div className="mb-1">
                              <Input
                                type="text"
                                name="Empresa "
                                placeholder="Ingrese aqui el nommbre de su empresa"
                              />
                            </div>
                            <div className="mb-1">
                              <span
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                Número de teléfono
                              </span>{" "}
                              - (Opcional)
                            </div>
                            <div className="mb-2">
                              <Input
                                type="text"
                                name="telefono "
                                placeholder="Ingrese aqui su número de telefono"
                              />
                            </div>

                            <div>
                              <Button
                                style={{
                                  borderRadius: "13px",
                                  fontSize: "14px",
                                }}
                                className="GLogin"
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider, e)
                                }
                              >
                                <i className="icon-gmail" />
                                <span>Gmail,&nbsp;</span>{" "}
                                <span
                                  className="color-orange"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  solo con un click
                                </span>
                              </Button>
                            </div>

                            <div className="text-center mt-2 mb-2">
                              <span style={{ fontSize: "0.9rem" }}>
                                {" "}
                                o con otro&nbsp;
                                <span
                                  className="arial"
                                  onClick={() =>
                                    this.setState(() => ({
                                      agotadoChoose: true,
                                    }))
                                  }
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    color: "rgb(54, 94, 153)",
                                    textDecoration: "underline",
                                  }}
                                >
                                  correo electrónico
                                </span>
                              </span>
                            </div>
                          </FormGroup>

                          <FormGroup className="mb-2">
                            <div className="mb-3">
                              <span
                                className="color-blue"
                                style={{
                                  fontWeight: "bold",
                                }}
                              >
                                &nbsp; Opción 2 -&nbsp;
                              </span>
                              <span>
                                Regresa en 15 días y goza de 3 nuevas consultas
                                sin registro.
                              </span>
                            </div>
                          </FormGroup>

                          <hr className="ten" />
                          <FormGroup
                            style={{ fontSize: "0.9rem" }}
                            className="text-center m-3"
                          >
                            Ya tengo una cuenta -&nbsp;
                            <span
                              className="arial"
                              onClick={() =>
                                this.setState(() => ({
                                  agotadoChoose: true,
                                }))
                              }
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </div>
                      </Form>
                    </Colxx>
                  </Row>
                </div>
              ) : (
                <div style={{ padding: "0rem" }}>
                  <i
                    onClick={this.toggleModalPreview}
                    className="plusLump  simple-icon-close "
                    style={{
                      fontSize: "2em",
                      position: "absolute",
                      right: "0.5em",
                      top: "0.5em",
                      cursor: "pointer",
                      zIndex: "999",
                    }}
                  />

                  <Colxx
                    xxs="12"
                    md="12"
                    sm="12"
                    className=" font NoPadding text-center pt-1 pb-1 arial color-white"
                    style={{
                      backgroundColor: "#0d5084",
                      fontSize: "1.35rem",
                      borderRadius: "1.1rem 1.1rem 0rem 0rem",
                    }}
                  >
                    <div>
                      <span>¡INGRESA&nbsp;</span>
                      <span
                        className="bggradien-orange color-white"
                        style={{
                          fontWeight: "bold",
                          borderRadius: "5px",
                        }}
                      >
                        &nbsp;GRATIS!&nbsp;
                      </span>
                    </div>
                  </Colxx>

                  <Row className="p-2">
                    <Colxx xs="12">
                      <hr className="ten" />
                      {!this.state.signup ? (
                        <Form
                          className=" login-form"
                          onSubmit={this.correoClave}
                        >
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                          <div
                            className="paddingLogin text-center"
                            style={{ textAling: "center" }}
                          >
                            <FormGroup className="mb-2 arial">
                              <span>Iniciar sesión</span>
                            </FormGroup>
                            <FormGroup className="mb-2">
                              <Button
                                style={{
                                  borderRadius: "13px",
                                  fontSize: "14px",
                                }}
                                className="GLogin"
                                onClick={(e) =>
                                  this.socialLogin(googleAuthProvider, e)
                                }
                              >
                                <i className="icon-gmail" />
                                <span>Gmail,&nbsp;</span>{" "}
                                <span
                                  className="color-orange"
                                  style={{
                                    fontWeight: "bold",
                                    cursor: "pointer",
                                    textDecoration: "underline",
                                  }}
                                >
                                  solo con un click
                                </span>
                              </Button>
                            </FormGroup>
                            <FormGroup className="mb-2 arial">
                              <span>-o-</span>
                            </FormGroup>
                            <FormGroup>
                              <Input
                                type="email"
                                name="usuario"
                                placeholder="Correo electrónico"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Input
                                name="clave"
                                type="password"
                                placeholder="Clave"
                              />
                            </FormGroup>
                            <FormGroup className="login-form-button">
                              <Button
                                type="submit"
                                className="btn-style-blue btn btn-success btn-sm"
                              >
                                Iniciar sesión
                              </Button>
                              &nbsp; O &nbsp;
                              <span
                                className="arial"
                                onClick={() => {
                                  this.setsignup(true);
                                }}
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  color: "rgb(54, 94, 153)",
                                  textDecoration: "underline",
                                }}
                              >
                                Registrate
                              </span>
                            </FormGroup>
                          </div>
                        </Form>
                      ) : (
                        <Form
                          className="paddingLogin text-center"
                          onSubmit={this.handleSignUp}
                        >
                          <FormGroup className="mb-2 arial">
                            <span>Registro</span>
                          </FormGroup>
                          {this.state.error ? (
                            <FormGroup>
                              <Errores mensaje={this.state.error} />
                            </FormGroup>
                          ) : null}
                          <FormGroup className="mb-2">
                            <Button
                              style={{
                                borderRadius: "13px",
                                fontSize: "14px",
                              }}
                              className="GLogin"
                              onClick={(e) =>
                                this.socialLogin(googleAuthProvider, e)
                              }
                            >
                              <i className="icon-gmail" />
                              <span>Gmail,&nbsp;</span>{" "}
                              <span
                                className="color-orange"
                                style={{
                                  fontWeight: "bold",
                                  cursor: "pointer",
                                  textDecoration: "underline",
                                }}
                              >
                                solo con un click
                              </span>
                            </Button>
                          </FormGroup>
                          <FormGroup className="mb-2 arial">
                            <span>-o-</span>
                          </FormGroup>
                          <FormGroup>
                            <Input
                              type="email"
                              name="usuario"
                              placeholder="Correo electrónico"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Input
                              name="clave"
                              type="password"
                              placeholder="Clave"
                            />
                          </FormGroup>

                          <FormGroup>
                            <Input
                              name="Confirmacion"
                              type="password"
                              placeholder="Confirma tu Clave"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Button
                              type="submit"
                              className="btn-style-blue btn btn-success btn-sm"
                            >
                              Registrate
                            </Button>
                            &nbsp; O &nbsp;
                            <span
                              onClick={() => {
                                this.setsignup(false);
                              }}
                              style={{
                                fontWeight: "bold",
                                cursor: "pointer",
                                color: "rgb(54, 94, 153)",
                                textDecoration: "underline",
                              }}
                            >
                              INICIAR SESIÓN
                            </span>
                          </FormGroup>
                        </Form>
                      )}
                    </Colxx>

                    <Colxx
                      xxs="12"
                      md="12"
                      sm="12"
                      className="p-1"
                      style={{ textAlign: "center" }}
                    >
                      <hr className="ten" />
                      <Button
                        color="success"
                        onClick={this.toggleModalPreview}
                        className="btn-style"
                        size="sm"
                      >
                        {" "}
                        VOLVER
                      </Button>
                    </Colxx>
                  </Row>
                </div>
              )}
            </ModalBody>
          )}
        </Modal>
        <Modal
          isOpen={this.state.confirmation}
          toggle={this.confirmation}
          size="md"
          className="modalConfirmatinoWidth"
          style={{ borderRadius: "1.1rem", marginTop: "65px !important" }}
        >
          <ModalBody style={{ padding: "0rem" }}>
            <i
              onClick={this.confirmation}
              className="plusLump  simple-icon-close "
              style={{
                fontSize: "2em",
                position: "absolute",
                right: "0.5em",
                top: "0.5em",
                cursor: "pointer",
                zIndex: "999",
              }}
            />
            <Colxx
              xxs="12"
              md="12"
              sm="12"
              className="text-center pt-2 pb-2 arial color-white"
              style={{
                backgroundColor: "#0d5084",
                fontSize: "1.5em",
              }}
            >
              ATENCIÓN
            </Colxx>
            <Row>
              <Colxx
                xxs="12"
                md="12"
                sm="12"
                className="mx-auto pb-2 pt-2 arial"
                style={{ textAlign: "center" }}
              >
                <span
                  style={{
                    fontSize: "1.25em",
                    color: "#0d5084",
                  }}
                >
                  Para un{" "}
                  <span className="color-orange">PRESUPUESTO MÁS EXACTO</span>,
                  sugerimos volver y completar:
                </span>
              </Colxx>
              <Colxx
                xxs="12"
                md="10"
                sm="10"
                className="mx-auto mt-2 pt-2"
                style={{
                  textAlign: "justify",
                  fontSize: "0.9rem",
                }}
              >
                <ul>
                  <li
                    style={{
                      display: this.state.check === false ? "none" : "",
                    }}
                  >
                    <span
                      style={{
                        display:
                          localStorage.transporte == "AÉREO" ? "none" : "",
                      }}
                    >
                      Gastos portuarios y de Almacenamiento Aduanero &nbsp;
                    </span>
                    <span
                      style={{
                        display:
                          localStorage.transporte == "AÉREO" ? "" : "none",
                      }}
                    >
                      Gasto Aeroportuario y Almacenamiento Aduanero&nbsp;
                    </span>
                  </li>

                  <li
                    style={{
                      display: this.state.check2 === false ? "none" : "",
                    }}
                  >
                    Impuestos y permisos de aduana
                  </li>

                  <li
                    style={{
                      display: this.state.check1 === false ? "none" : "",
                    }}
                  >
                    Transporte a domicilio
                  </li>
                  <li
                    style={{
                      display: this.state.check4 === false ? "none" : "",
                    }}
                  >
                    Seguro de mercancía
                  </li>
                </ul>
              </Colxx>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Row>
              <Colxx xxs="12" md="12" sm="12" className="mb-2">
                <CustomInput
                  id="SelectedOnce"
                  type="checkbox"
                  onChange={this.SelectedOnce}
                  label="No volver a mostrar este mensaje"
                />
              </Colxx>
              <Colxx xxs="12" md="12" sm="12" style={{ textAlign: "center" }}>
                <Button
                  color="success"
                  onClick={() => {
                    this.confirmation();
                    this.eventVolver();
                  }}
                  className="btn-style mr-2"
                  size="sm"
                >
                  VOLVER
                </Button>
                <Button
                  color="success"
                  onClick={() => {
                    this.toggleModalPreview();
                    this.eventIrPresupuestos();
                  }}
                  className="btn-style-blue"
                  size="sm"
                >
                  IR A PRESUPUESTO
                </Button>
              </Colxx>
            </Row>
          </ModalFooter>
        </Modal>
        <Beforeunload onBeforeunload={() => "You'll lose your data!"} />
        <BotChat />
      </Fragment>
    );
  }
}

export default withRouter(servicios);

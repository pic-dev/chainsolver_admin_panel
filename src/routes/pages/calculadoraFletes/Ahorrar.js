import React, { Component } from "react";
import ReactPlayer from "react-player/lazy";
import { Col, Row, Container } from "reactstrap";
import { PageView } from "Util/tracking";
import RouteBack from "Util/RouteBack";
import { Title } from "Util/helmet";
var getYoutubeTitle = require("get-youtube-title");

const item = [
  { link: "https://www.youtube.com/watch?v=CaZ9CBR2cMk", id: "CaZ9CBR2cMk" },
  { link: "https://www.youtube.com/watch?v=9fjMYXoygb8", id: "9fjMYXoygb8" },
  { link: "https://www.youtube.com/watch?v=YAAamNCy870", id: "YAAamNCy870" },
  { link: "https://www.youtube.com/watch?v=XbtodscK5j4", id: "XbtodscK5j4" },
  { link: "https://www.youtube.com/watch?v=fRgieiClsNg", id: "fRgieiClsNg" },
  { link: "https://www.youtube.com/watch?v=WMDmQp2H3HU", id: "WMDmQp2H3HU" },
  { link: "https://www.youtube.com/watch?v=IQFsvausRh0", id: "IQFsvausRh0" },
  { link: "https://www.youtube.com/watch?v=6eca33r5pKU", id: "6eca33r5pKU" },
  { link: "https://www.youtube.com/watch?v=Tzs1pz336ng", id: "Tzs1pz336ng" },
  { link: "https://www.youtube.com/watch?v=c7ZL0rcpk-k", id: "c7ZL0rcpk-k" },
  { link: "https://www.youtube.com/watch?v=xxvydbM0bCQ", id: "xxvydbM0bCQ" },
  { link: "https://www.youtube.com/watch?v=8ee2EZ-3Ib8", id: "8ee2EZ-3Ib8" },
  { link: "https://www.youtube.com/watch?v=lzd8-jM9mao", id: "lzd8-jM9mao" },
  { link: "https://www.youtube.com/watch?v=U8kkib2Cvec", id: "U8kkib2Cvec" },
  { link: "https://www.youtube.com/watch?v=BupT29V-_To", id: "BupT29V-_To" },
  { link: "https://www.youtube.com/watch?v=a0vjSMtWHIU", id: "a0vjSMtWHIU" },
  { link: "https://www.youtube.com/watch?v=g8RucOFFTnA", id: "g8RucOFFTnA" },
  { link: "https://www.youtube.com/watch?v=aViiB6wgeLg", id: "aViiB6wgeLg" },
  { link: "https://www.youtube.com/watch?v=d337HkFxOMo", id: "d337HkFxOMo" },
  { link: "https://www.youtube.com/watch?v=EOhA2YA4ovU", id: "EOhA2YA4ovU" },
  { link: "https://www.youtube.com/watch?v=4aIG38QWDJE", id: "4aIG38QWDJE" },
  { link: "https://www.youtube.com/watch?v=oL2moVT6M2Q", id: "oL2moVT6M2Q" },
  { link: "https://www.youtube.com/watch?v=RkJyy0Attxo", id: "RkJyy0Attxo" },
  { link: "https://www.youtube.com/watch?v=04PEHoN3fm0", id: "04PEHoN3fm0" },
  { link: "https://www.youtube.com/watch?v=HAekro_6zNM", id: "HAekro_6zNM" },
  { link: "https://www.youtube.com/watch?v=KEar-WxVfj0", id: "KEar-WxVfj0" },
  { link: "https://www.youtube.com/watch?v=AwciHATnk7g", id: "AwciHATnk7g" },
  { link: "https://www.youtube.com/watch?v=amZo9xTiwPU", id: "amZo9xTiwPU" },
  { link: "https://www.youtube.com/watch?v=YaXB--XO7qI", id: "YaXB--XO7qI" },
];

class ahorrar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
    };
  }

  componentDidMount() {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }

    item.map((item) => {
      getYoutubeTitle(item.id, (err, title) => {
        this.setState({
          item: this.state.item.concat([{ link: item.link, title: title }]),
        });
      });
    });
  }

  render() {
    return (
      <Container className="mt-5 pt-5">
        <Title>
          <title>
            {window.location.pathname.replace("/", "") +
              " | " +
              "PIC  - Calculadora de Fletes"}
          </title>
        </Title>
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
            <div className="font text-center pb-2">APRENDE A AHORRAR</div>
            <div className="font2 text-center pb-2">Biblioteca multimedia</div>
          </Col>
          <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
            <RouteBack />
          </Col>
          <Row>
            <div
              className="g-ytsubscribe"
              data-channelid="UCaAprzcpP0PrafQzMDfJ2uA"
              data-layout="full"
              data-count="default"
            ></div>
            {this.state.item.map((item, index) => (
              <Col
                className="mx-auto my-auto p-2"
                xxs="12"
                lg="3"
                md="4"
                sm="12"
              >
                <ReactPlayer
                  light={true}
                  url={item.link}
                  className="react-player video-youtube-Tras"
                  key={index + "Ahorrar"}
                  width="100%"
                  height="100%"
                  config={{
                    youtube: {
                      playerVars: {
                        controls: 0,
                        'origin': `${window.location.hostname}`
                      },
                    },
                  }}
                />
                <div
                  style={{ fontWeight: "bold" }}
                  className="text-justify p-1"
                >
                  {item.title}
                </div>
              </Col>
            ))}
          </Row>
        </Row>
      </Container>
    );
  }
}

export default ahorrar;

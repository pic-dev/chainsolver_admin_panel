import React, { Component, Fragment } from "react";
import { Col, Row } from "reactstrap";
import { NavLink, withRouter } from "react-router-dom";
import { Colxx } from "Components/CustomBootstrap";
import scrollIntoView from "scroll-into-view-if-needed";
import { PageView } from "Util/tracking";
import Banderas from "Util/banderas";
import { isMovil } from "Util/Utils";
import Carrousel from "Util/carruselHome";
import {Title} from "Util/helmet";
import BotChat from "Util/chat";

class inicio extends Component {
  constructor(props) {
    super(props);
    this.transporte = this.transporte.bind(this);
    this.pwaInstalingToggle = this.pwaInstalingToggle.bind(this);

    this.state = {
      isOpen: false,
    };
  }
  componentDidMount() {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }

    localStorage.clear();
    localStorage.page = "1";

    var element = document.getElementById("link3");
    scrollIntoView(element, {
      behavior: "smooth",
      block: "center",
      inline: "center",
    });
    localStorage.page = "1";
  }

  pwaInstalingToggle(e) {
    this.setState({
      pwaInstaling: e,
    });
  }

  transporte(e) {
    switch (e) {
      case 1:
        this.props.name("MARITÍMO");
        localStorage.transporte = "MARITÍMO";
        break;
      case 2:
        this.props.name("AÉREO");
        localStorage.transporte = "AÉREO";
        break;

      default:
      // code block
    }
  }
  render() {
    if (sessionStorage.usuario === "2") {
    } else {
      sessionStorage.usuario = "1";
    }
    localStorage.page = "1";
    return (
      <Fragment>
        <Title>
          <title>{"Home | PIC  - Calculadora de Fletes"}</title>
        </Title>
        <div className="container2 topMargin">
          <Row className="h-100 " id="link3">
            <Colxx xxs="12" md="10" className="mx-auto NoPadding">
              <Colxx
                xxs="12"
                md="10"
                className="font  mx-auto my-auto first-tittle NoPadding"
              >
                {" "}
                <span>
                  ¡COTIZA TÚ FLETE
                  <br />Y ADUANA&nbsp;
                  <span
                    className="bggradien-orange color-white"
                    style={{
                      fontWeight: "bold",
                      borderRadius: "5px",
                    }}
                  >
                    &nbsp;EN LÍNEA!&nbsp;
                  </span>
                </span>
              </Colxx>
              <i className="icon-porcent" />

              <Colxx xxs="6" md="4" className="mx-auto mt-3 mb-2">
                <hr className="my-2" />
              </Colxx>
              <Colxx xxs="12" md="6" className="mx-auto  ">
                <span className="step-style">
                  PASO 1. Selecciona el Servicio
                </span>{" "}
              </Colxx>

              <Colxx xxs="12" md="8" className="mx-auto paddingInicio">
                <Row>
                  <Col
                    xs="6"
                    sm="6"
                    md="6"
                    lg="6"
                    style={{ float: "left", textAlign: "center" }}
                  >
                    <NavLink to="/Ruta" style={{ padding: "0px" }}>
                      <div
                        className="select-tipe-cargo-crl ship-img"
                        onClick={() => this.transporte(1)}
                      />{" "}
                    </NavLink>
                  </Col>
                  <Col
                    xs="6"
                    sm="6"
                    md="6"
                    lg="6"
                    style={{ float: "left", textAlign: "center" }}
                  >
                    <NavLink to="/Ruta" style={{ padding: "0px" }}>
                      <div
                        className="select-tipe-cargo-crl plane-img"
                        onClick={() => this.transporte(2)}
                      />
                    </NavLink>
                  </Col>
                </Row>
              </Colxx>
            </Colxx>

            <Colxx
              xxs="12"
              md="10"
              xl="6"
              lg="8"
              className={`NoPadding mx-auto mt-2 mb-2 ${
                isMovil() && "fixed-bottom"
              }`}
            >
              <hr className="mt-2 my-2" />
              <NavLink to="/Importadores" style={{ padding: "0px" }}>
                <Carrousel />
              </NavLink>
            </Colxx>

            <Banderas />
          </Row>

        </div>
        <BotChat />
      </Fragment>
    );
  }
}

export default withRouter(inicio);

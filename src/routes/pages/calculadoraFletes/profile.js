import React, { Component, Fragment } from "react";
import { Col, Row, Table, ListGroup, ListGroupItem, Button } from "reactstrap";
import { Link } from "react-router-dom";
import { Colxx } from "Components/CustomBootstrap";
import "react-bootstrap-typeahead/css/Typeahead.css";

class perfil extends Component {
  constructor(props) {
    super(props);

    this.renderTableHeader = this.renderTableHeader.bind(this);
    this.renderTableData = this.renderTableData.bind(this);

    this.state = {
      cotizaciones: [
        {
          Id: 1,
          Ruta: "Callao-Shanghaì",
          Fecha: "01/05/2020",
          Mercancia: "Muebles",
          Ver: 1,
        },
        {
          Id: 2,
          Ruta: "Callao-MIami",
          Fecha: "01/05/2020",
          Mercancia: "Ropa",
          Ver: 2,
        },
        {
          Id: 3,
          Ruta: "Callao-Panama",
          Fecha: "11/05/2020",
          Mercancia: "Ropa",
          Ver: 3,
        },
        {
          Id: 4,
          Ruta: "Callao-Shanghaì",
          Fecha: "23/04/2020",
          Mercancia: "Ropa",
          Ver: 4,
        },
      ],
    };
  }

  componentDidMount() {}

  renderTableHeader() {
    let header = Object.keys(this.state.cotizaciones[0]);
    return header.map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>;
    });
  }

  renderTableData() {
    return this.state.cotizaciones.map((student, index) => {
      const { Id, Ruta, Fecha, Mercancia } = student;
      return (
        <tr key={Id}>
          <td>{Id}</td>
          <td>{Ruta}</td>
          <td>{Fecha}</td>
          <td>{Mercancia}</td>
          <td>Ver</td>
        </tr>
      );
    });
  }

  render() {
    let user;
    let data = JSON.parse(sessionStorage.getItem("user"));
    if (sessionStorage.usuario == "2") {
      if (!data) {
        user = [
          ["Carlos"],
          ["Carlos"],
          ["director@pic.com"],
          ["/assets/img/profile-pic-l.png"],
        ];
      } else {
        user = data;
      }
    } else {
      user = [
        ["Carlos"],
        ["Carlos"],
        ["director@pic.com"],
        ["/assets/img/profile-pic-l.png"],
      ];
    }

    return (
      <Fragment>
        <div className="container2">
          <Row className="h-100">
            <Colxx xxs="12" md="10" className="mx-auto NoPadding">
              <Row>
                <Colxx xxs="12" md="10" className="mx-auto mb-3">
                  <span className="mx-auto mt-2 step-style">
                    Perfil de Usuario
                  </span>
                </Colxx>

                <Col
                  xxs="12"
                  md="8"
                  className="mx-auto my-auto"
                  style={{
                    backgroundColor: "#ffffff78",
                    borderRadius: "10px",
                    boxShadow: "0 0 4px white",
                  }}
                >
                  <Row>
                    <Col
                      xxs="12"
                      md="12"
                      sm="12"
                      style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.18)" }}
                      className="mx-auto"
                    >
                      <Row>
                        <Col
                          xxs="12"
                          md="8"
                          sm="12"
                          className="mx-auto NoPaddingCost"
                          style={{ fontSize: "1em" }}
                        >
                          <span
                            className="color-blue"
                            style={{
                              fontSize: "1.1em",
                              fontWeight: "bold",
                              textAlign: "center",
                            }}
                          >
                            DATOS BASICOS
                          </span>
                          <ul>
                            <li>
                              {" "}
                              <span className="float-left">
                                Nombre Completo:
                              </span>{" "}
                              <span className="float-right">{user[1]}</span>
                            </li>

                            <li>
                              <span className="float-left">Email:</span>
                              <span className="float-right">{user[2]}</span>
                            </li>

                            <div
                              style={{
                                marginTop: "3em",
                                margin: "auto",
                                paddingTop: "2em",
                              }}
                            >
                              <img src={user[3]} alt={user[1]} />
                            </div>
                          </ul>
                        </Col>
                      </Row>
                    </Col>

                    <Col xxs="12" md="12" sm="12" className="pt-2 mx-auto">
                      <Link to="/Inicio">
                        <Button size="sm" className="btn-style" color="success">
                          Ir a inicio
                        </Button>
                      </Link>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Colxx>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default perfil;

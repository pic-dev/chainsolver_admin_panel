import React, { Component, Fragment } from "react";
import { Row, Button } from "reactstrap";
import { Colxx } from "Components/CustomBootstrap";
import { NavLink } from "react-router-dom";
import { Title } from "Util/helmet";
class condiciones extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <Fragment>
        <Title>
          <title>
            {window.location.pathname.replace("/", "") +
              " | " +
              "PIC  - Calculadora de Fletes"}
          </title>
        </Title>
        <div className="container2 ">
          <Row className="h-100 " id="link3">
            <Colxx xxs="12" md="12" className="mx-auto text-justify ">
              <div className="font2 text-center pb-2">
                Términos y Condiciones de Uso
              </div>

              <div className="font2 text-center pb-2">
                INFORMACIÓN RELEVANTE
              </div>
              <div className="pb-1">
                Es requisito necesario para la adquisición de los productos que
                se ofrecen en este sitio, que lea y acepte los siguientes
                Términos y Condiciones que a continuación se redactan. El uso de
                nuestros servicios así como la compra de nuestros productos
                implicará que usted ha leído y aceptado los Términos y
                Condiciones de Uso en el presente documento. Todas los productos
                que son ofrecidos por nuestro sitio web pudieran ser creadas,
                cobradas, enviadas o presentadas por una página web tercera y en
                tal caso estarían sujetas a sus propios Términos y Condiciones.
                En algunos casos, para adquirir un producto, será necesario el
                registro por parte del usuario, con ingreso de datos personales
                fidedignos y definición de una contraseña.
              </div>
              <div className="pb-1">
                El usuario puede elegir y cambiar la clave para su acceso de
                administración de la cuenta en cualquier momento, en caso de que
                se haya registrado y que sea necesario para la compra de alguno
                de nuestros productos. https://calculadoradefletes.com/ no asume
                la responsabilidad en caso de que entregue dicha clave a
                terceros.
              </div>
              <div className="pb-1">
                Todas las compras y transacciones que se lleven a cabo por medio
                de este sitio web, están sujetas a un proceso de confirmación y
                verificación, el cual podría incluir la verificación del stock y
                disponibilidad de producto, validación de la forma de pago,
                validación de la factura (en caso de existir) y el cumplimiento
                de las condiciones requeridas por el medio de pago seleccionado.
                En algunos casos puede que se requiera una verificación por
                medio de correo electrónico.
              </div>
              <div className="font2 text-center pb-2">LICENCIA</div>
              <div className="pb-1">
                PIC a través de su sitio web concede una licencia para que los
                usuarios utilicen los productos que son vendidos en este sitio
                web de acuerdo a los Términos y Condiciones que se describen en
                este documento.
              </div>
              <div className="font2 text-center pb-2">USO NO AUTORIZADO</div>
              <div className="pb-1">
                En caso de que aplique (para venta de software, templetes, u
                otro producto de diseño y programación) usted no puede colocar
                uno de nuestros productos, modificado o sin modificar, en un CD,
                sitio web o ningún otro medio y ofrecerlos para la
                redistribución o la reventa de ningún tipo.
              </div>
              <div className="font2 text-center pb-2">PROPIEDAD</div>
              <div className="pb-1">
                Usted no puede declarar propiedad intelectual o exclusiva a
                ninguno de nuestros productos, modificado o sin modificar. Todos
                los productos son propiedad de los proveedores del contenido. En
                caso de que no se especifique lo contrario, nuestros productos
                se proporcionan sin ningún tipo de garantía, expresa o
                implícita. En ningún esta compañía será responsables de ningún
                daño incluyendo, pero no limitado a, daños directos, indirectos,
                especiales, fortuitos o consecuentes u otras pérdidas
                resultantes del uso o de la imposibilidad de utilizar nuestros
                productos.
              </div>
              <div className="font2 text-center pb-2">
                COMPROBACIÓN ANTIFRAUDE
              </div>
              <div className="pb-1">
                La compra del cliente puede ser aplazada para la comprobación
                antifraude. También puede ser suspendida por más tiempo para una
                investigación más rigurosa, para evitar transacciones
                fraudulentas.
              </div>
              <div className="font2 text-center pb-2">PRIVACIDAD</div>
              <div className="pb-1">
                Este <a href="" target="_blank"></a>{" "}
                https://calculadoradefletes.com/ garantiza que la información
                personal que usted envía cuenta con la seguridad necesaria. Los
                datos ingresados por usuario o en el caso de requerir una
                validación de los pedidos no serán entregados a terceros, salvo
                que deba ser revelada en cumplimiento a una orden judicial o
                requerimientos legales.
              </div>
              <div className="pb-1">
                PIC reserva los derechos de cambiar o de modificar estos
                términos sin previo aviso.
              </div>
            </Colxx>
            <Colxx className="text-center pb-2">
              <NavLink to="/" style={{ padding: "0px" }}>
                <Button color="success" className="btn-style mr-2" size="sm">
                  VOLVER Al Inicio
                  <i className="simple-icon-arrow-left-circle" />
                </Button>
              </NavLink>
            </Colxx>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default condiciones;

import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Parallax from "../../util/Parallax/Parallax.js";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { container } from "../../util/Parallax/material-kit-react.js";
import classNames from "classnames";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { NavLink, withRouter } from "react-router-dom";
import { PageView } from "../../util/tracking";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import RouteBack from "../../util/RouteBack"
const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  rootAccordion: {
    width: "100%",
  },

  headingAccordion: {
    display: "grid",
    textAlign: "left",
  },
  AccordionOrange: {
    backgroundColor: "#ec6729 !important",
    color: "white",
  },
  AccordionBlue: {
    backgroundColor: "#0d5084 !important",
    color: "white",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  heading2: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    flexBasis: "100%",
    fontWeight: "bold",
  },
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left",
  },
  title: {
    fontSize: "3.5rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative",
    textShadow: "1px -1px 0px black",
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px 0 0",
    textAlign: "justify",
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",

    zIndex: "3",
    position: "relative",
    background: "#FFFFFF",

    marginTop: "50px",
  },
  button: {
    backgroundColor: "#64b86a",
    margin: "1rem",
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  displayNone: {
    display: "none",
  },

  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  titulo: {
    fontSize: "1.5rem",
    fontWeight: "bold",
    color: "#0d5084",
  },
  mensaje: {
    fontSize: "1rem",
    color: "black",
  },
  heroContent: {
    padding: theme.spacing(4, 2),
  },
  cardHeader: {
    fontSize: "1.3rem",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
    padding: {
      padding: theme.spacing(3),
    },
    paddingTittle: {
      padding: theme.spacing(10),
      marginBottom: theme.spacing(10),
    },
  },
}));

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default function FAQ() {
  const classes = useStyles();

  const matches = useMediaQuery("(min-width:769px)");

  useEffect(() => {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
  }, []);

  return (
    <React.Fragment>
      <CssBaseline />
      <Parallax image={require("Assets/img/PublicidadLarge.png")}>
        <div className={classNames(classes.container, classes.mainRaised)}>

 
          <Container maxWidth="md" component="main">
          <Grid className="text-left" container spacing={5}>
              <Grid item xs={12} sm={12} md={12}>
    
                <RouteBack />
                </Grid>
            </Grid>
            <Grid container spacing={5}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography
                  variant="h6"
                  align="center"
                  color="textPrimary"
                  component="p"
                  className="color-blue text-center"
                  style={{ fontSize: "1.5rem" }}
                >
                  PREGUNTAS FRECUENTES
                </Typography>
              </Grid>
            </Grid>
            <Grid container spacing={5}>
              <Grid style={{ textAlign: "end" }} item xs={12} sm={12} md={12}>
                <NavLink to="/Importadores">
                  <Button className="btn-style-quest text-right m-1 btn btn-success btn-sm">
                    Ver Catalogos 2021 <i className="ml-2 simple-icon-eye" />
                  </Button>
                </NavLink>
              </Grid>
            </Grid>
          </Container>
          <Container maxWidth="md" component="main">
            <Grid container spacing={5}>
              <Grid
                style={{ textAlign: "justify" }}
                item
                xs={12}
                sm={12}
                md={12}
              >
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography className={classes.heading}>
                      ¿COMO SE QUE ESTO NO ES UNA ESTAFA?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        1. En el listado de productos en stock y productos en
                        tránsito están los nombres y números de teléfono de
                        todos los importadores que trabajan con nosotros,
                        sientase libre de llamar a cualquiera y pedir referencia
                        sobre nuestra empresa.{" "}
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        2. Puede dirigirse a nuestras oficinas y reunirse con{" "}
                        <a
                          rel="noopener noreferrer"
                          href="https://www.pic-cargo.com/es/contact/"
                          target="_blank"
                        >
                          {" "}
                          <span style={{ fontWeight: "bold" }}>nosotros</span>
                        </a>{" "}
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        3. Revisar los videos en youtube donde hablamos sobre
                        todos los temas logisticos y de aduana:
                        <a
                          rel="noopener noreferrer"
                          href="https://www.youtube.com/channel/UCaAprzcpP0PrafQzMDfJ2uA "
                          target="_blank"
                        >
                          <span style={{ fontWeight: "bold" }}>
                            {" "}
                            Link al canal
                          </span>
                        </a>
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      ¿PUEDO ESCOGER EL PRODUCTO O TENGO QUE COMPRAR LO QUE
                      USTEDES TENGAN EN LAS LISTAS?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Precisamente lo que nos diferencia de la competencia que
                        nuestros importadores pueden sugerir los productos que
                        se van a comprar, estos se someten a votación en el
                        grupo y si hay un consenso se importa.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3a-content"
                    id="panel3a-header"
                  >
                    <Typography className={classes.heading}>
                      ¿CUANTO ES EL MÍNIMO DE LA INVERSIÓN Y QUE INCLUYE?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        En la mayoría de los casos el mínimo es de USD 2.000,
                        aunque dependiendo del producto existen montos
                        superiores.
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        <span style={{ fontWeight: "bold" }}>
                          {" "}
                          La inversión incluye:{" "}
                        </span>{" "}
                        la compra del producto, logística internacional y aduana
                        de importación e impuestos.
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        <span style={{ fontWeight: "bold" }}> NOTA: </span>{" "}
                        algunas veces la aduana realiza lo que se llama ajuste
                        de valor y puede variar los impuestos estimados, para
                        esto se enviará un soporte respaldado por la misma
                        aduana.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel4a-content"
                    id="panel4a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿CÓMO OBTENGO LOS PRECIOS Y LAS NOVEDADES QUE VAN A
                      IMPORTAR?{" "}
                      <span className="color-red">(aplica para Perú) </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Se debe inscribir en los grupos cerrados, aquí los
                        participantes hacen propuestas de los productos,
                        análisis de mercado y factibilidad del negocio.{" "}
                      </div>
                 
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        (El precio de inscripción para Perú es de 100 soles
                        hasta el 30 de octubre y de 200 soles a partir de
                        noviembre){" "}
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel5a-content"
                    id="panel5a-header"
                  >
                    <Typography className={classes.heading}>
                      <span>
                        {" "}
                        ¿PORQUE PIDEN UNA INSCRIPCIÓN PARA DAR PRECIOS?{" "}
                        <span className="color-red">(aplica para Perú) </span>
                      </span>{" "}
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Lo hacemos con la finalidad de resguardar la información
                        de nuestros costos de la competencia.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>

                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel6a-content"
                    id="panel6a-header"
                  >
                    <Typography className={classes.heading}>
                      <span>
                        ¿QUE SUCEDE CON EL DINERO QUE DOY EN LA INSCRIPCIÓN?{" "}
                        <span className="color-red"> (aplica para Perú) </span>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      Lo hacemos con la finalidad de resguardar la información
                      de nuestros costos de la competencia.
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel7a-content"
                    id="panel7a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿CUÁNDO DEBO REALIZAR EL PAGO DE LA INVERSIÓN?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      Está dividida en 2 o 3 partes de acuerdo con las
                      condiciones del envío, fabricación y tiempos de llegada de
                      la mercancía.
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel8a-content"
                    id="panel8a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿DEBO TENER RUC/RIF O CONSTITUIDA UNA EMPRESA PARA
                      IMPORTAR?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        No es necesario porque nosotros PIC-CARGO.COM, nos
                        encargamos de apoyar al importador siendo los
                        responsables legales y poniendo nuestro nombre y una vez
                        que la mercancía arriba al país la separamos y
                        entregamos a cada uno de los participantes.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel9a-content"
                    id="panel9a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿A NOMBRE DE QUIEN VIENE LA IMPORTACIÓN Y QUIEN ES EL
                      RESPONSABLE?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        La importación viene a nombre de PIC-CARGO.COM y somos
                        los únicos responsables de todo el proceso.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel10a-content"
                    id="panel10a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿USTEDES HACEN LA ENTREGA EN MÍ PROVINCIA, REGIÓN O
                      ESTADO?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Sí, estas se hacen a través de una agencia
                        interprovincial o internas y con un previo acuerdo con
                        nosotros.
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel11a-content"
                    id="panel11a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿CUÁL ES EL TIEMPO DE ENTREGA?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Los tiempos de entrega dependerán de varios factores,
                        algunos de estos son:
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        1.- Condiciones de entrega del producto por parte del
                        fabricante. Es decir, si hay productos en stock la
                        entrega y envío es inmediato, si no hay productos en
                        stock la fábrica se demora un tiempo en producir (puede
                        ser de 7, 15 o 30 días)
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        2.- El tiempo de entrega también dependerá del tipo de
                        envío (marítimo o aéreo)
                        https://www.pic-cargo.com/es/noticias/flete-aereo-o-flete-maritimo/
                      </div>
                      <div style={{ textIndent: "40px", marginBottom: "1rem" }}>
                        3.- Otros de los factores que influyen va a ser el país
                        de cual importemos la mercancía (estamos trabajando
                        principalmente con USA, China o Panamá y estos tardan
                        entre 30 a 45)
                      </div>
                      <div
                        style={{
                          textIndent: "40px",
                          marginBottom: "0.5rem",
                          textAlign: "center",
                          fontWeight: "bold",
                        }}
                      >
                        Para los que no me conocen mi nombre es Carlos Ramírez
                        fundador y CEO de la empresa PIC CARGO y somos los
                        organizadores de la ASOCIACIÓN DE IMPORTADORES GRUPALES
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel12a-content"
                    id="panel12a-header"
                  >
                    <Typography className={classes.heading}>
                      {" "}
                      ¿QUIÉNES SOMOS?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        En PIC-CARGO.COM prestamos el servicio de agencias de
                        aduanas o corredor de aduanas, transporte y logística
                        internacional desde distintos orígenes como China,
                        Estados Unidos y países de Europa a América latina,
                        tenemos oficinas propias en Perú, Panamá y Venezuela.
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Dirección de nuestras oficinas:
                      </div>
                      <div
                        style={{ marginBottom: "0.5rem", fontWeight: "bold" }}
                      >
                        Lima:
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Av. Jose Pardo, Cuadra 1 y Calle Martir Olaya, Edif.
                        Empresarial Pardo 129, ofic. 1005, Miraflores, Lima,
                        Perú.
                      </div>
                      <div style={{ marginBottom: "0.5rem" }}>
                        <ul>
                          <li>
                            Correos electrónicos:
                            <span style={{ fontWeight: "bold" }}>
                                asesoramiento@pic-cargo.com
                            </span>{" "}
                          </li>
                          <li>Números telefónicos: +511-326-9130</li>
                          <li>Whatsapp: +51 972 530 303</li>
                        </ul>
                      </div>
                      <div
                        style={{ marginBottom: "0.5rem", fontWeight: "bold" }}
                      >
                        Panamá:
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Ave. Ricardo J. Alfaro, Edif. Century Tower, piso 8,
                        ofic. 811, Ciudad de Panamá.
                      </div>
                      <div style={{ marginBottom: "0.5rem" }}>
                        <ul>
                          <li>
                            Correos electrónicos:
                            <span style={{ fontWeight: "bold" }}>
                              asesoramientopa@pic-cargo.com
                            </span>{" "}
                          </li>
                          <li>Números telefónicos: +507-393 5412 / 5416</li>
                          <li>Whatsapp: +507-699-72126</li>
                        </ul>
                      </div>
                      <div
                        style={{ marginBottom: "0.5rem", fontWeight: "bold" }}
                      >
                        VENEZUELA:
                      </div>
                      <div
                        style={{ textIndent: "40px", marginBottom: "0.5rem" }}
                      >
                        Av. Francisco de Solano, Edif. Banvenez, piso 14, ofic.
                        CD, Ciudad de Caracas, CP: 1050, Distrito Capital,
                        Venezuela.
                      </div>
                      <div style={{ marginBottom: "0.5rem" }}>
                        <ul>
                          <li>
                            Correos electrónicos:
                            <span style={{ fontWeight: "bold" }}>
                              asesoramientove@pic-cargo.com
                            </span>{" "}
                          </li>
                          <li>
                            Números telefónicos: (+58212) 763-2240 / 762-6178
                          </li>
                          <li>Whatsapp: +58 424115-3525</li>
                        </ul>
                      </div>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
              </Grid>
            </Grid>
          </Container>

          <Container
            maxWidth="md"
            className={(classes.footer, classes.heroContent)}
          >
            <NavLink to="/SeccionImportadores">
              <Button className="btn-style-backConcentradores" size="sm">
                Volver a Sección de Importadores
              </Button>
            </NavLink>
          </Container>
          <Container
            maxWidth="md"
            className={(classes.footer, classes.heroContent)}
          >
            <Copyright />
          </Container>
        </div>
      </Parallax>
    </React.Fragment>
  );
}

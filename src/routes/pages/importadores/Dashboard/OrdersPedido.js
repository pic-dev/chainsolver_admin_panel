import React from "react";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Title from "./Title";
import Subtitle from "./Subtitle";

// Generate Order Data
function createData(
  id,
  date,
  name,
  Catalogo,
  Pagina,
  Cantidades,
  Codigo,
  amount
) {
  return { id, date, name, Catalogo, Pagina, Cantidades, Codigo, amount };
}

const rows = [
  createData(
    0,
    "16 Mar, 2019",
    "Elvis Presley",
    "Pantalones",
    "4",
    "4524",
    312.44
  ),
  createData(
    1,
    "16 Mar, 2019",
    "Elvis Presley",
    "Pantalones",
    "4",
    "4524",
    312.44
  ),
  createData(
    2,
    "16 Mar, 2019",
    "Elvis Presley",
    "Pantalones",
    "4",
    "4524",
    312.44
  ),
  createData(
    3,
    "16 Mar, 2019",
    "Elvis Presley",
    "Pantalones",
    "4",
    "4524",
    312.44
  ),
  createData(
    4,
    "16 Mar, 2019",
    "Elvis Presley",
    "Pantalones",
    "4",
    "4524",
    312.44
  ),
];

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Orders() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Ordenes Recientes con Precio</Title>
      <Subtitle>Junior</Subtitle>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Fecha</TableCell>
            <TableCell>Nombre</TableCell>
            <TableCell>Catalogo</TableCell>
            <TableCell>Pagina</TableCell>
            <TableCell>Cantidades</TableCell>
            <TableCell>Codigo</TableCell>
            <TableCell align="right">Precio</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.Catalogo}</TableCell>
              <TableCell>{row.Pagina}</TableCell>
              <TableCell>{row.Cantidades}</TableCell>

              <TableCell>{row.Codigo}</TableCell>
              <TableCell align="right">{row.amount}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          Ver Más Ordenes
        </Link>
      </div>
    </React.Fragment>
  );
}

import axios from "axios";

export const mapOrder = (array, order, key) => {
  array.sort(function (a, b) {
    var A = a[key],
      B = b[key];
    if (order.indexOf(A + "") > order.indexOf(B + "")) {
      return 1;
    } else {
      return -1;
    }
  });
  return array;
};


export const getJuniorList = async () => {
  await axios
    .get("https://point.qreport.site/senior")
    .then((res) => {
 
      return res.data;
    })
    .catch((error) => {
      return null;
    });
};

export const addCommas = (nStr) => {
  nStr += "";
  var x = nStr.split(".");
  var x1 = x[0];
  var x2 = x.length > 1 ? "." + x[1] : "";
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, "$1" + "," + "$2");
  }
  return x1 + x2;
};


function rucValido(ruc) {
  //11 dígitos y empieza en 10,15,16,17 o 20
  if (
    !(
      (ruc >= 1e10 && ruc < 11e9) ||
      (ruc >= 15e9 && ruc < 18e9) ||
      (ruc >= 2e10 && ruc < 21e9)
    )
  ) {
    return false;
  } else {
    return true;
  }
}
export const gastosUsa = () => {
  let peso = Number(localStorage.weight) / 1000;
  let costoUsa;
  let mayor;
  if (Number(localStorage.volumen) > peso) {
    mayor = Number(localStorage.volumen);
  } else if (Number(localStorage.volumen) == peso) {
    mayor = volumen;
  } else {
    mayor = peso;
  }

  if (mayor * 18 < 20) {
    costoUsa = 90;
  } else {
    costoUsa = mayor * 18 + 70;
  }
  return costoUsa;
};

import React, { Fragment, Component} from "react";
import FileUpload from "Util/formItems/FileUpload";

import {
  Input,
  Form,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardImg,
  Collapse,
  Button,
  FormGroup,
  Label,
  ListGroup,
  ListGroupItem,
  NavbarToggler,
  Alert
} from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import SideBar from "../sidebar/SideBar";
import { getCurrentTime, getDateWithFormat } from "Util/Utils";

const ModalProductDetails = React.lazy(() =>
  import("Util/Modals/modalProductDetails")
);
var timer;
class ImpoAdmin extends Component {
  constructor() {
    super();
    this.state = {
      isOpenAlert: false,
      isOpenAlertRemove: false,
      isOpenAlertUpdate: false,
      img: [],
      fileType: "",
      nombreProducto: "",
      product: [],
      items: [],
      productEdit: [],
      uploadValue: 0,
      sidebarIsOpen: true,
      isOpenItem: false,
      isOpenItemEdit: false,
      toggleProductDetails: false,
      toggleProductDetailsdata: [],
    };
    this.breakPoints = [
      { width: 1, itemsToShow: 1 },
      { width: 550, itemsToShow: 2, itemsToScroll: 2, pagination: false },
      { width: 850, itemsToShow: 3 },
      { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
      { width: 1450, itemsToShow: 5 },
      { width: 1750, itemsToShow: 6 },
    ];

    this.handleRemoveSpec = this.handleRemoveSpec.bind(this);
    this.handleRemoveMultime = this.handleRemoveMultime.bind(this);
    this.addItem = this.addItem.bind(this);
    this.handleForm = this.handleForm.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleItemUpdate = this.toggleItemUpdate.bind(this);
    this.toggleProductDetails = this.toggleProductDetails.bind(this);
    this.toggleProductDetailsOpen = this.toggleProductDetailsOpen.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
  }

  componentDidMount() {
    if (screen.width <= 769) {
      this.setState((prevState) => ({
        sidebarIsOpen: !prevState.sidebarIsOpen,
      }));
    }

    var database = firebaseConf.database().ref("product");

    database.on("child_added", (snapshot) => {
      var key = [{ key: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);

      this.setState({
        product: this.state.product.concat(data),
      });
    });

    database.on("child_removed", (snapshot) => {
      var eliminado = snapshot.key;
      let data = this.state.product.filter(function (item) {
        if (eliminado !== item.key) {
          return item;
        }
      });
      this.setState({
        product: data,
      });
      this.renderLoginButton();
    });
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  toggleSidebar() {
    this.setState((prevState) => ({
      sidebarIsOpen: !prevState.sidebarIsOpen,
    }));
  }
  toggleItem() {
    this.setState((prevState) => ({
      isOpenItem: !prevState.isOpenItem,
    }));

    this.setState({
      img: [],
      nombreProducto: "",
      fileType: "",
      items: [],
      uploadValue: 0,
    });
  }
  toggleAlert() {
    this.setState((prevState) => ({
      isOpenAlert: !prevState.isOpenAlert,
    }));
  }
  toggleProductDetails(e) {
    var key = e.target.value;
    let data = this.state.product.filter(function (item) {
      if (key == item.key) {
        return item;
      }
    });
    if (data.length > 0) {
      this.setState({
        toggleProductDetailsdata: data,
        toggleProductDetails: true,
      });
    }
  }
  toggleProductDetailsOpen() {
    this.setState((prevState) => ({
      toggleProductDetails: !prevState.toggleProductDetails,
    }));
  }
  toggleItemUpdate() {
    this.setState({
      img: [],
      fileType: "",
      nombreProducto: "",
      items: [],
      productEdit: [],
      isOpenItemEdit: false,
    });
  }
  handleRemove(e) {
    firebaseConf
      .database()
      .ref("/product/" + e.target.value)
      .remove();

    this.setState({
      isOpenAlertRemove: true,
    });

    timer = setTimeout(() => {
      this.setState({
        isOpenAlertRemove: false,
      });
    }, 3000);
  }
  handleUpdate() {
    if (
      this.state.nombreProducto.length > 0 &&
      this.state.img.length > 0 &&
      this.state.items.length > 0
    ) {
  
      let video = this.state.img.filter(function (item) {
        if (item.fileType.indexOf("video") == 0) {
          return item;
        }
      });
      let imagen = this.state.img.filter(function (item) {
        if (item.fileType.indexOf("video") !== 0 && item.fileType !== "application/pdf") {
          return item;
        }
      });

      let pdf = this.state.img.filter(function (item) {
        if (item.fileType == "application/pdf") {
          return item;
        }
      });

    
      const record = {
        Multimedia: [{ imagen }, { video }, { pdf }],
        NombreProducto: this.state.nombreProducto,
        Caracteristicas: this.state.items,
        fileType: this.state.fileType,
        activo: true,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      firebaseConf
        .database()
        .ref("/product/" + this.state.productEdit)
        .update(record)
        .then(() => {
          this.setState({
            img: [],
            nombreProducto: "",
            fileType: "",
            items: [],
            uploadValue: 0,
            isOpenItemEdit: false,
            productEdit: [],
            isOpenAlertUpdate: true,
          });

          timer = setTimeout(() => {
            this.setState({
              isOpenAlertUpdate: false,
            });
          }, 3000);
        })
        .catch((error) => {
          console.error("Error writing document: ", error);
        });
    } else {
      alert("Hay campos que requieren verificación");
    }
  }
  handleEdit(e) {
    let data = this.state.product.filter(function (item) {
      if (e == item.key) {
        return item;
      }
    });
    var datos;
    if (data.length > 0) {
     if (data[0].Multimedia[2] && data[0].Multimedia[1]) {
       var datapre;
       datapre = data[0].Multimedia[2].pdf.concat(
          data[0].Multimedia[0].imagen
        );

        datos = data[0].Multimedia[1].video.concat(datapre);
      }
      
      else if (data[0].Multimedia[2]) {
        datos = data[0].Multimedia[2].pdf.concat(
          data[0].Multimedia[0].imagen
        );
      }
      else  if (data[0].Multimedia[1]) {
        datos = data[0].Multimedia[1].video.concat(
          data[0].Multimedia[0].imagen
        );
      }
       else {
        datos = data[0].Multimedia[0].imagen;
      };

      this.setState({
        img: datos,
        nombreProducto: data[0].NombreProducto,
        fileType: data[0].fileType,
        items: data[0].Caracteristicas,
        productEdit: data[0].key,
        isOpenItemEdit: true,
      });
    }
  }
  handleRemoveSpec(e) {
    var key = e.target.value;
    let data = this.state.items.filter(function (item) {
      if (key != item.key) {
        return item;
      }
    });
    this.setState({
      items: data,
    });
  }
  handleRemoveMultime(value) {
    var data = this.state.img.filter(function (item, index) {
      if (value !== index) {
        return item;
      }
    });
    if (data.length > 0) {
      this.setState({
        img: data,
      });
    }
  }
  addItem(e) {
    e.preventDefault();

    const { Caracteristica, Descripción } = e.target.elements;
    if (Caracteristica.value.length > 0 && Descripción.value.length > 0) {
      var newItem = {
        nombre: Caracteristica.value,
        descripción: Descripción.value,
        key: Date.now(),
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem),
        };
      });

      document.getElementById("Caracteristica").value = "";
      document.getElementById("Descripción").value = "";
    }
    this.renderLoginButton();
  }
  handleUpload(e) {
    const file = e.target.files[0];
    const storageRef = firebaseConf.storage().ref(`fotos/${file.name}`);
    const task = storageRef.put(file);

    // Listener que se ocupa del estado de la carga del fichero
    task.on(
      "state_changed",
      (snapshot) => {
        // Calculamos el porcentaje de tamaño transferido y actualizamos
        // el estado del componente con el valor
        let percentage =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.setState({
          uploadValue: percentage,
        });
      },
      (error) => {
        // Ocurre un error
        console.error(error.message);
      },
      () => {
        task.snapshot.ref.getDownloadURL().then((downloadURL) => {
          var data = [
            { img: downloadURL, fileType: file.type, key: Date.now() },
          ];
          this.setState({
            img: this.state.img.concat(data),
            fileType: file.type,
          });
        });
      }
    );
  }
  handleForm() {
    if (
      this.state.nombreProducto.length > 0 &&
      this.state.img.length > 0 &&
      this.state.items.length > 0
    ) {


      let video = this.state.img.filter(function (item) {
        if (item.fileType.indexOf("video") == 0) {
          return item;
        }
      });
      let imagen = this.state.img.filter(function (item) {
        if (item.fileType.indexOf("video") !== 0) {
          return item;
        }
      });

      let pdf = this.state.img.filter(function (item) {
        if (item.fileType == "application/pdf") {
          return item;
        }
      });

    

      const record = {
        Multimedia: [{ imagen }, { video }, { pdf }],
        catalogos: pdf,
        NombreProducto: this.state.nombreProducto,
        Caracteristicas: this.state.items,
        fileType: this.state.fileType,
        activo: true,
        rates: 0,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("product");
      const newProduct = dbRef.push();
      newProduct.set(record);

      this.setState({
        img: [],
        nombreProducto: "",
        fileType: "",
        items: [],
        uploadValue: 0,
        isOpenItem: false,
        isOpenAlert: true,
      });

      timer = setTimeout(() => {
        this.setState({
          isOpenAlert: false,
        });
      }, 3000);
    } else {
      alert("Hay campos que requieren verificación");
    }
  }
  renderLoginButton() {
    if (true) {
      return (
        <div>
          <Row>
            <Button
              onClick={this.toggleItem}
              style={{
                display:
                  this.state.isOpenItem || this.state.isOpenItemEdit
                    ? "none"
                    : "",
              }}
              className="btn-style-Green btn btn-success btn-sm"
            >
              Agregar producto
            </Button>
            {!this.state.isOpenItemEdit ? (
              <Collapse
                style={{ display: this.state.isOpenItem ? "contents" : "" }}
                isOpen={this.state.isOpenItem}
              >
                <Col xs="12" md="8" sm="12" className="mx-auto pb-1">
                  <Row
                    className="mx-auto m-2 p-2"
                    style={{
                      borderRadius: "1em",
                      backgroundColor: "#d7d7d7",
                      border: "1px solid rgba(0, 0, 0, 0.18)",
                      fontWeight: "bold",
                    }}
                  >
                    {" "}
                    <Col xs="12" md="12" sm="12" className="arial mx-auto pb-1">
                      IMAGENES DE PRODUCTO
                    </Col><Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <FileUpload
                        uploadValue={this.state.uploadValue}
                        onUpload={this.handleUpload}
                        multiple
                      />
                      {this.state.img.length > 0
                        ? this.state.img.map((item, index) => (
                            <ListGroupItem
                              key={index}
                              style={{ display: "inline-grid" }}
                              className="justify-content-between p-1"
                            >
                              {item.fileType == "video/mp4" ? (
                                <video
                                  key={index + "video"}
                                  id={"my-video" + index}
                                  style={{ height: "4rem" }}
                                  controls
                                  data-setup="{}"
                                >
                                  <source src={item.img} type={item.fileType} />
                                </video>
                              ) : (
                                <img
                                  key={index + "img"}
                                  src={item.img}
                                  style={{ height: "4rem" }}
                                  alt="logo"
                                />
                              )}
                              <Button
                                className="btn-style-Red btn btn-success btn-sm"
                                value={index}
                                style={{ lineHeight: "1" }}
                                onClick={() => this.handleRemoveMultime(index)}
                              >
                                x
                              </Button>
                            </ListGroupItem>
                          ))
                        : ""}
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      {" "}
                      <Form>
                        <FormGroup>
                          <Label
                            className="arial mx-auto pb-1"
                            for="nombreProducto"
                          >
                            NOMBRE DE PRODUCTO
                          </Label>
                          <Input
                            name="nombreProducto"
                            type="text"
                            placeholder="nombre de producto"
                            value={this.state.nombreProducto}
                            onChange={(e) => {
                              this.setState({
                                nombreProducto: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                      </Form>
                    </Col>
                    <Col xs="12" md="12" sm="12" className="arial mx-auto pb-1">
                      CARACTERISTICAS DE PRODUCTO
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <Form onSubmit={this.addItem}>
                        <FormGroup>
                          <Label for="Caracteristica">
                            Tipo de Caracteristica
                          </Label>
                          <Input
                            name="Caracteristica"
                            id="Caracteristica"
                            type="text"
                            placeholder="Caracteristica"
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label for="Descripción">
                            descripción de Caracteristica
                          </Label>
                          <Input
                            name="Descripción"
                            id="Descripción"
                            type="text"
                            placeholder="Descripción"
                          />
                        </FormGroup>
                        <FormGroup className="login-form-button">
                          <Button
                            type="submit"
                            className="btn-style-blue btn btn-success btn-sm"
                          >
                            Agregar Caracteristica
                          </Button>
                        </FormGroup>
                      </Form>
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <ListGroup>
                        {this.state.items.map((item, index) => (
                          <ListGroupItem
                            key={item.key}
                            className="justify-content-between"
                          >
                            {item.nombre}: {item.descripción}{" "}
                            <Button
                              className="btn-style-Red btn btn-success btn-sm"
                              value={item.key}
                              style={{ lineHeight: "1" }}
                              onClick={this.handleRemoveSpec}
                            >
                              x
                            </Button>
                          </ListGroupItem>
                        ))}
                      </ListGroup>{" "}
                    </Col>
                  </Row>
                </Col>
                <Col xs="12" md="12" sm="12" className="mx-auto m-2 p-2">
                  <Button
                    className="btn-style-Green btn btn-success btn-sm"
                    onClick={this.handleForm}
                  >
                    Agregar producto
                  </Button>{" "}
                  <Button
                    className="btn-style-Red btn btn-success btn-sm"
                    onClick={this.toggleItem}
                  >
                    Cancelar
                  </Button>
                </Col>
              </Collapse>
            ) : (
              ""
            )}

            {this.state.img.length > 0 ? (
              <Collapse
                style={{ display: this.state.isOpenItemEdit ? "contents" : "" }}
                isOpen={this.state.isOpenItemEdit}
              >
                <Col xs="12" md="8" sm="12" className="mx-auto pb-1">
                  <Row
                    className="mx-auto m-2 p-2"
                    style={{
                      borderRadius: "1em",
                      backgroundColor: "#d7d7d7",
                      border: "1px solid rgba(0, 0, 0, 0.18)",
                      fontWeight: "bold",
                    }}
                  >
                    {" "}
                    <Col xs="12" md="12" sm="12" className="arial mx-auto pb-1">
                      IMAGENES DE PRODUCTO
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <FileUpload
                        uploadValue={this.state.uploadValue}
                        onUpload={this.handleUpload}
                        multiple
                      />
                      {this.state.img.map((item, index) => (
                        <ListGroupItem
                          key={index}
                          style={{ display: "inline-grid" }}
                          className="justify-content-between p-1"
                        >
                          {item.fileType == "video/mp4" ? (
                            <video
                              key={index + "video"}
                              id={"my-video" + index}
                              style={{ height: "4rem" }}
                              controls
                              data-setup="{}"
                            >
                              <source src={item.img} type={item.fileType} />
                            </video>
                          ) : (
                            <img
                              key={index + "img"}
                              src={item.img}
                              style={{ height: "4rem" }}
                              alt="logo"
                            />
                          )}
                          <Button
                            className="btn-style-Red btn btn-success btn-sm"
                            value={index}
                            style={{ lineHeight: "1" }}
                            onClick={() => this.handleRemoveMultime(index)}
                          >
                            x
                          </Button>
                        </ListGroupItem>
                      ))}
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      {" "}
                      <Form>
                        <FormGroup>
                          <Label
                            className="arial mx-auto pb-1"
                            for="nombreProducto"
                          >
                            NOMBRE DE PRODUCTO
                          </Label>
                          <Input
                            name="nombreProducto"
                            type="text"
                            placeholder="nombre de producto"
                            value={this.state.nombreProducto}
                            onChange={(e) => {
                              this.setState({
                                nombreProducto: e.target.value,
                              });
                            }}
                          />
                        </FormGroup>
                      </Form>
                    </Col>
                    <Col xs="12" md="12" sm="12" className="arial mx-auto pb-1">
                      CARACTERISTICAS DE PRODUCTO
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <Form onSubmit={this.addItem}>
                        <FormGroup>
                          <Label for="Caracteristica">
                            Tipo de Caracteristica
                          </Label>
                          <Input
                            name="Caracteristica"
                            id="Caracteristica"
                            type="text"
                            placeholder="Caracteristica"
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label for="Descripción">
                            descripción de Caracteristica
                          </Label>
                          <Input
                            name="Descripción"
                            id="Descripción"
                            type="text"
                            placeholder="Descripción"
                          />
                        </FormGroup>
                        <FormGroup className="login-form-button">
                          <Button
                            type="submit"
                            className="btn-style-blue btn btn-success btn-sm"
                          >
                            Agregar Caracteristica
                          </Button>
                        </FormGroup>
                      </Form>
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      {this.state.items.map((item, index) => (
                        <ListGroupItem
                          key={item.key}
                          style={{ display: "inline-grid" }}
                          className="justify-content-between p-1"
                        >
                          {item.nombre}: {item.descripción}{" "}
                          <Button
                            className="btn-style-Red btn btn-success btn-sm"
                            value={item.key}
                            style={{ lineHeight: "1" }}
                            onClick={this.handleRemoveSpec}
                          >
                            x
                          </Button>
                        </ListGroupItem>
                      ))}
                    </Col>
                  </Row>
                </Col>
                <Col xs="12" md="12" sm="12" className="mx-auto m-2 p-2">
                  <Button
                    className="btn-style-Green btn btn-success btn-sm"
                    onClick={this.handleUpdate}
                  >
                    Actualizar producto
                  </Button>{" "}
                  <Button
                    className="btn-style-Red btn btn-success btn-sm"
                    onClick={this.toggleItemUpdate}
                  >
                    Cancelar
                  </Button>
                </Col>
              </Collapse>
            ) : (
              ""
            )}
            <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
              <Row
                className="mx-auto m-2 p-2"
                style={{
                  borderTop: "1px solid rgba(0, 0, 0, 0.18)",
                }}
              >
                <Col xs="12" md="12" sm="12" className="font2 mx-auto m-2">
                  PRODUCTOS ACTUALES
                </Col>
                {this.state.product.map((picture, index) => (
                  <Col
                    key={index}
                    xs="6"
                    md="4"
                    lg="3"
                    xl="2"
                    sm="6"
                    className="mb-2"
                  >
                    <Card
                      className="p-0"
                      style={{
                        border: "1px #8f8f8f solid",
                        alignItems: "center",
                        borderRadius: "1rem",
                      }}
                    >  {this.state.product.length > 0 && this.state.product[index].Multimedia[0] && <CardImg
                        className="imagenProductos"
                        top
                        width="100%"
                        src={
                          this.state.product[index].Multimedia[0].imagen[0].img
                        }
                        alt="imagenProductos"
                      />}
                      <CardBody style={{ padding: "0", width: "100%" }}>
                        <CardTitle
                          style={{
                            backgroundColor: "#d7d7d7",
                            fontSize: "0.8rem",
                            height: "2rem",
                            margin: "0px",
                            padding: "0px",
                            bordeRadius: "1rem 1rem 0rem 0rem"
                          }}
                        >
                          {picture.NombreProducto}
                        </CardTitle>
                        <Button
                          className="btn-style-Green m-1 btn btn-success btn-sm"
                          value={picture.key}
                          onClick={(e) => this.toggleProductDetails(e)}
                        >
                          Ver
                        </Button>{" "}
                        <Button
                          className="btn-style m-1 btn btn-success btn-sm"
                          value={picture.key}
                          onClick={() => this.handleEdit(picture.key)}
                        >
                          <i className="simple-icon-pencil" />
                        </Button>
                        <Button
                          className="btn-style-Red m-1 btn btn-success btn-sm"
                          value={picture.key}
                          onClick={(e) => this.handleRemove(e)}
                        >
                          x
                        </Button>
                      </CardBody>
                    </Card>
                  </Col>
                ))}
              </Row>
            </Col>
          </Row>
        </div>
      );
    }
  }
  render() {
    return (
      <Fragment>
        <Row className="p-0">
          <NavbarToggler
            className="float-right display-sm"
            style={{
              backgroundColor: "#1370b9",
              border: "solid white 1px",
              top: "70px",
              left: "0",
              position: "fixed",
            }}
            onClick={this.toggleSidebar}
          />
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "3" : "1"}
            md={this.state.sidebarIsOpen ? "4" : "1"}
            sm="12"
            className="p-0 mx-auto"
          >
            <SideBar
              toggle={this.toggleSidebar}
              isOpen={this.state.sidebarIsOpen}
            />
          </Col>
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "9" : "11"}
            md={this.state.sidebarIsOpen ? "8" : "11"}
            sm="12"
            className="p-1 mx-auto"
          >
            <Row className="p-1">
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <h2>IMPORTADORES GRUPALES - Panel de control</h2>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlert}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Agregado Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertRemove}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Removido Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertUpdate}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Actualizado Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.renderLoginButton()}
              </Col>
            </Row>
          </Col>
        </Row>

        {this.state.toggleProductDetailsdata.length > 0 &&
  
            <ModalProductDetails
              active={this.state.toggleProductDetails}
              item={this.state.toggleProductDetailsdata}
              toggle={this.toggleProductDetailsOpen}
            />
    
       }
      </Fragment>
    );
  }
}

export default ImpoAdmin;

import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { DataGrid } from "@material-ui/data-grid";
import * as data from "Util/vipClientes.csv";
import Parallax from "Util/Parallax/Parallax.js";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import PropTypes from "prop-types";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { container } from "Util/Parallax/material-kit-react.js";
import classNames from "classnames";
import Divider from "@material-ui/core/Divider";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Hidden from "@material-ui/core/Hidden";
import { NavLink } from "react-router-dom";
import WhatsApp from "@material-ui/icons/WhatsApp";
import { PageView } from "Util/tracking";
import RouteBack from "Util/RouteBack";
import { Title } from "Util/helmet";
import BotonSenioLogin from "Util/botonSenioLogin";
// Generate Order Data

//var csv is the CSV file with headers

const csvJSON = (csv) => {
  var result = [];
  result = csv.map(function (item) {
    return {
      id: item.ID,
      Nombre: item.NOMBRE,
      Pais: item.PAIS,
      Provincia: item.PROVINCIA,
      Distrito: item.DISTRITO,
      Telefono: item.TELEFONO,
      Telefono2: item.TELEFONO2,
    };
  });

  //return result; //JavaScript object
  return result; //JSON
};

const SearchProvincias = () => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (current) {
      var exists = !hash[current.Provincia];
      hash[current.Provincia] = true;
      return exists;
    })
    .map((item) => {
      return item.Provincia;
    });

  return array; //JSON
};

const SearchDistritos = (e) => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (item) {
      if (e == item.Provincia) {
        return item;
      }
    })
    .filter(function (current) {
      var exists = !hash[current.Distrito];
      hash[current.Distrito] = true;
      return exists;
    })
    .map((item) => {
      return item.Distrito;
    });

  return array; //JSON
};

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  rootAccordion: {
    width: "100%",
  },

  headingAccordion: {
    display: "grid",
    textAlign: "left",
  },
  AccordionOrange: {
    backgroundColor: "#ec6729 !important",
    color: "white",
  },
  AccordionBlue: {
    backgroundColor: "#0d5084 !important",
    color: "white",
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  heading2: {
    fontSize: theme.typography.pxToRem(17),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    flexBasis: "100%",
    fontWeight: "bold",
  },
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left",
  },
  title: {
    fontSize: "3.5rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative",
    textShadow: "1px -1px 0px black",
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px 0 0",
    textAlign: "justify",
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",

    zIndex: "3",
    position: "relative",
    background: "#FFFFFF",

    marginTop: "50px",
  },
  button: {
    backgroundColor: "#64b86a",
    margin: "1rem",
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  displayNone: {
    display: "none",
  },

  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  titulo: {
    fontSize: "1.5rem",
    fontWeight: "bold",
    color: "#0d5084",
  },
  mensaje: {
    fontSize: "1rem",
    color: "black",
  },
  heroContent: {
    padding: theme.spacing(4, 2),
  },
  cardHeader: {
    fontSize: "1.3rem",
    color: "white",
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
    padding: {
      padding: theme.spacing(3),
    },
    paddingTittle: {
      padding: theme.spacing(10),
      marginBottom: theme.spacing(10),
    },
  },
}));

const tiers = [
  {
    title: "IMPORTADORES SENIOR",
    subheader: "Most popular",
    description: [
      "Inversión Mínima 2000 Dólares.",
      "Reciben ayuda directa para la venta de sus productos por parte de Pic Cargo.",
      "Después de 5 importaciones podrás participar como lider de Distrito.",
    ],
    buttonText: "CONTACTAR ASESOR PIC CARGO",
    buttonVariant: "contained",
  },
  {
    title: "IMPORTADORES JUNIOR",
    description: [
      "Inversión 500 Dólares.",
      "Recibirán ayuda y guía del Importador Senior.",
      "Si quiere importar algún producto en particular deberá apoyarse con su líder de Zona.",
    ],
    buttonText: "UNETE A UN SENIOR",
    buttonVariant: "outlined",
  },
];

const columns = [
  { field: "Nombre", sortable: true, headerName: "Nombre", width: 130 },
  {
    field: "Pais",
    headerName: "Pais",

    sortable: true,
    width: 160,
  },
  {
    field: "Provincia",
    headerName: "Provincia",

    sortable: true,
    width: 160,
  },
  {
    field: "Distrito",
    headerName: "Distrito",
    description: "This column has a value getter and is not sortable.",
    sortable: true,
    width: 160,
  },
  {
    field: "Telefono",
    headerName: "Telefono",
    sortable: true,
    type: "number",
    width: 250,
    renderCell: (params) => (
      <strong>
        {params.value}

        <Button
          size="small"
          style={{
            color: "#64b86a",
            border: "1px solid #64b86a",
            marginLeft: 5,
          }}
          href={"https://api.whatsapp.com/send?phone=51" + params.value}
          target="_blank"
          className="arial"
          startIcon={<WhatsApp />}
        >
          Contactar
        </Button>
      </strong>
    ),
  },
];

Vip.propTypes = {
  width: PropTypes.oneOf(["lg", "md", "sm", "xl", "xs"]).isRequired,
};

export default function Vip(props) {
  const classes = useStyles();
  const { width } = props;

  const [Province, setProvince] = useState("");
  const [District, setDistrict] = useState("");
  const [DistrictList, setDistrictList] = useState([]);
  const [filter, setFilter] = useState([]);
  const [Country, setCountry] = useState("PERU");
  const matches = useMediaQuery("(min-width:769px)");
  const [expanded, setExpanded] = React.useState(false);

  const handleChangeAccordion = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleChange = (event, id) => {
    switch (id) {
      case 1:
        setProvince(event.target.value);
        let datos = csvJSON(data).filter(function (item) {
          if (event.target.value == item.Provincia) {
            return item;
          }
        });
        setDistrictList(SearchDistritos(event.target.value));

        setFilter(datos);

        break;
      case 2:
        setCountry(event.target.value);
        break;
      case 3:
        setDistrict(event.target.value);

        let array = csvJSON(data).filter(function (item) {
          if (
            event.target.value == item.Distrito &&
            Province == item.Provincia
          ) {
            return item;
          }
        });
        setFilter(array);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
  }, []);

  return (
    <React.Fragment>
      <Title>
        <title>
          {window.location.pathname.replace("/", "") +
            " | " +
            "PIC  - Calculadora de Fletes"}
        </title>
      </Title>
      <CssBaseline />
      {/* <BotonSenioLogin />*/}
      <Parallax image={require("Assets/img/PublicidadLarge.png")}>
        <Hidden only={["sm", "xs"]}>
          <div className={classNames(classes.container, classes.mainRaised)}>
            <Container
              maxWidth="md"
              component="main"
              className={matches ? classes.heroContent : classes.displayNone}
            >
              <Grid className="text-left" container spacing={5}>
                <Grid item xs={12} sm={12} md={12}>
                  <RouteBack />
                </Grid>
              </Grid>
              <Grid container spacing={6}>
                <Grid item xs={12} sm={12} md={12}>
                  <Typography
                    variant="h4"
                    align="center"
                    color="textPrimary"
                    component="p"
                  >
                    ¿CON CUANTO PUEDO COMENZAR A IMPORTAR?
                  </Typography>
                </Grid>
                {tiers.map((tier) => (
                  <Grid
                    item
                    key={tier.title}
                    xs={12}
                    sm={tier.title === "Enterprise" ? 12 : 6}
                    md={6}
                  >
                    <Card
                      style={{
                        backgroundColor:
                          tier.title === "IMPORTADORES SENIOR"
                            ? "#ec6729"
                            : "#0d5084",
                      }}
                    >
                      <CardHeader
                        title={tier.title}
                        titleTypographyProps={{ align: "center" }}
                        subheaderTypographyProps={{ align: "center" }}
                        action={
                          tier.title === "IMPORTADORES SENIOR" ? (
                            <StarIcon />
                          ) : null
                        }
                        style={{
                          color: "white",
                          backgroundColor:
                            tier.title === "IMPORTADORES SENIOR"
                              ? "#622E1D"
                              : "#0F2534",
                        }}
                      />
                      <CardContent>
                        <ul>
                          {tier.description.map((line) => (
                            <Typography
                              component="li"
                              variant="subtitle1"
                              align="justify"
                              key={line}
                              style={{
                                color: "white",
                              }}
                            >
                              <i
                                style={{
                                  backgroundColor: "white",
                                  borderRadius: "50%",
                                  color:
                                    tier.title === "IMPORTADORES SENIOR"
                                      ? "#ec6729"
                                      : "#0d5084",
                                }}
                                className="simple-icon-check mr-2"
                              />
                              {line}
                            </Typography>
                          ))}
                        </ul>
                      </CardContent>
                      <CardActions>
                        {tier.title === "IMPORTADORES SENIOR" ? (
                          <Button
                            fullWidth
                            style={{
                              backgroundColor: "white",
                              color: "#ec6729",
                            }}
                            href={
                              "https://api.whatsapp.com/send?phone=0051920301745"
                            }
                            target="_blank"
                            className="arial"
                            startIcon={
                              <WhatsApp
                                style={{
                                  backgroundColor: "white",
                                  color: "#64B161",
                                }}
                              />
                            }
                          >
                            Contactar Asesor Pic Cargo
                          </Button>
                        ) : (
                          <Button
                            fullWidth
                            variant={tier.buttonVariant}
                            className="arial"
                            color="primary"
                            href="#seleccion"
                            style={{
                              backgroundColor: "white",
                              color: "#0d5084",
                            }}
                          >
                            {tier.buttonText}
                          </Button>
                        )}
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </Container>
            <Divider />
            <Container
              maxWidth="md"
              style={{ backgroundColor: "#4C3099", maxWidth: "100%" }}
              id="seleccion"
            >
              <Typography
                variant="h4"
                align="center"
                component="span"
                style={{ color: "white" }}
              >
                Listado de Importadores Senior
              </Typography>
            </Container>
            <Container maxWidth="md" className={classes.heroContent}>
              <img
                key="img"
                src="/assets/img/seo.svg"
                alt="logo"
                style={{ height: "4rem", position: "absolute", left: "40px" }}
              />
              <form className={classes.root} noValidate autoComplete="off">
                <div>
                  <TextField
                    id="Pais"
                    select
                    value={Country}
                    onChange={(e) => handleChange(e, 2)}
                    helperText="Seleccione País"
                  >
                    <MenuItem key={1 + "Pais"} value="PERU">
                      Perú
                    </MenuItem>
                  </TextField>
                  <TextField
                    id="Provincia"
                    select
                    placeholder="Seleccione"
                    value={Province}
                    onChange={handleChange}
                    onChange={(e) => handleChange(e, 1)}
                    helperText="Seleccione un Departamento o Provincia"
                    disabled={Country.length === 0}
                  >
                    {SearchProvincias().map((option, index) => (
                      <MenuItem key={index + 1 + "Provincia"} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </TextField>
                  {DistrictList.length > 1 && (
                    <TextField
                      id="Distrito"
                      select
                      helperText="Distrito"
                      value={District}
                      onChange={(e) => handleChange(e, 3)}
                      helperText="Seleccione Distrito"
                      disabled={DistrictList.length === 0}
                    >
                      {DistrictList.map((option, index) => (
                        <MenuItem key={index + 1 + "Distrito"} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </TextField>
                  )}
                </div>
              </form>
            </Container>
            {filter.length == 1 && (
              <Container maxWidth="md">
                <div
                  style={{ height: 400, width: "100%", textAlign: "center" }}
                >
                  <DataGrid rows={filter} columns={columns} pageSize={5} />
                </div>
              </Container>
            )}
            <Container
              maxWidth="md"
              className={(classes.footer, classes.heroContent)}
            >
                 <Grid container spacing={10}>
                <Grid item xs={12} sm={12} md={12}>
                  <NavLink to="/Importadores">
                    <Button className="btn-style-backConcentradores mt-5 mb-5 btn btn-sm">
                      Ver Importaciones en Proceso
                    </Button>
                  </NavLink>
                </Grid>
              </Grid>
              <Copyright />
            </Container>
          </div>
        </Hidden>

        <Hidden only={["lg", "md", "xl"]}>
          <div className={classNames(classes.container, classes.mainRaised)}>
            <Container maxWidth="md" className={classes.heroContent}>
              <Grid className="text-left" container spacing={5}>
                <Grid item xs={12} sm={12} md={12}>
                  <RouteBack />
                </Grid>
              </Grid>
            </Container>
            <Container
              maxWidth="md"
              component="main"
              className={matches ? classes.displayNone : classes.tabs}
            >
              <Grid container spacing={1}>
                <Grid style={{ display: "flex" }} item xs={7} sm={7} md={12}>
                  <Typography
                    variant="h6"
                    align="center"
                    color="textPrimary"
                    component="p"
                    className="color-orange "
                    style={{ fontSize: "2rem" }}
                  >
                    ¿
                  </Typography>
                  <Typography
                    variant="h6"
                    align="center"
                    color="textPrimary"
                    component="p"
                    className="color-blue text-left"
                    style={{ fontSize: "2rem" }}
                  >
                    Deseas <br /> Importar
                  </Typography>
                  <Typography
                    variant="h6"
                    align="center"
                    color="textPrimary"
                    component="p"
                    className="color-orange "
                    style={{ fontSize: "5rem" }}
                  >
                    ?
                  </Typography>
                </Grid>
                <Grid item xs={5} sm={5} md={12}>
                  <img
                    src="/assets/img/BOT-PIC.gif"
                    style={{ height: "6rem", width: "auto" }}
                    alt="logo"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={10}>
                <Grid style={{ textAlign: "end" }} item xs={12} sm={12} md={12}>
                  <NavLink to="/FAQ">
                    <Button className="btn-style-quest text-right m-1 btn btn-success btn-sm">
                      Preguntas Frecuentes
                    </Button>
                  </NavLink>
                </Grid>
              </Grid>
            </Container>
            <Container maxWidth="md" className="p-0">
              <Accordion
                expanded={expanded === "panel2"}
                onChange={handleChangeAccordion("panel2")}
              >
                <AccordionSummary
                  expandIcon={
                    <ExpandMoreIcon
                      style={{
                        backgroundColor: "white",
                        borderRadius: "50%",
                        color: "#0d5084",
                      }}
                    />
                  }
                  aria-controls="panel2bh-content"
                  id="panel2bh-header"
                  className={classes.AccordionBlue}
                >
                  <div className={classes.headingAccordion}>
                    <Typography className={classes.heading}>
                      IMPORTADOR SENIOR
                    </Typography>
                    <Typography className={classes.secondaryHeading}>
                      Inversión 2000 USD
                    </Typography>
                  </div>
                </AccordionSummary>
                <AccordionDetails className={classes.AccordionBlue}>
                  <Typography>
                    <ul>
                      {tiers[0].description.map((line) => (
                        <Typography
                          component="li"
                          variant="subtitle1"
                          align="justify"
                          key={line}
                        >
                          <i
                            style={{
                              backgroundColor: "white",
                              borderRadius: "50%",
                              color: "#0d5084",
                            }}
                            className="simple-icon-check mr-2"
                          />
                          {line}
                        </Typography>
                      ))}
                    </ul>
                  </Typography>
                </AccordionDetails>
                <Button
                  fullWidth
                  style={{ color: "#64b86a", border: "1px solid #64b86a" }}
                  href={"https://api.whatsapp.com/send?phone=51920301745"}
                  target="_blank"
                  className="arial"
                  startIcon={<WhatsApp />}
                >
                  Contactar Asesor Pic Cargo
                </Button>
              </Accordion>

              <Accordion
                expanded={expanded === "panel1"}
                onChange={handleChangeAccordion("panel1")}
              >
                <AccordionSummary
                  expandIcon={
                    <ExpandMoreIcon
                      style={{
                        backgroundColor: "white",
                        borderRadius: "50%",
                        color: "#ec6729",
                      }}
                    />
                  }
                  aria-controls="panel1bh-content"
                  id="panel1bh-header"
                  className={classes.AccordionOrange}
                >
                  <div className={classes.headingAccordion}>
                    <Typography className={classes.heading}>
                      IMPORTADOR JUNIOR
                    </Typography>
                    <Typography className={classes.secondaryHeading}>
                      Inversión 500 USD - *Debe unirse a un Grupo Senior
                    </Typography>
                  </div>
                </AccordionSummary>
                <AccordionDetails className={classes.AccordionOrange}>
                  <Typography>
                    <ul>
                      {tiers[1].description.map((line) => (
                        <Typography
                          component="li"
                          variant="subtitle1"
                          align="justify"
                          key={line}
                        >
                          <i
                            style={{
                              backgroundColor: "white",
                              borderRadius: "50%",
                              color: "#ec6729",
                            }}
                            className="simple-icon-check mr-2"
                          />
                          {line}
                        </Typography>
                      ))}
                    </ul>
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </Container>
            <Container maxWidth="md" className={classes.heroContent}>
              <div className={classes.headingAccordion}>
                <Typography className={classes.heading}>
                  LISTADO DE IMPORTADORES SENIOR
                </Typography>
                <Typography className={classes.secondaryHeading}>
                  Selecciona el más cercano a tú distrito
                </Typography>
              </div>
            </Container>

            <Grid container spacing={2} className={classes.heroContent}>
              <Grid item xs={4} sm={4} md={12}>
                <TextField
                  id="Pais"
                  select
                  value={Country}
                  onChange={(e) => handleChange(e, 2)}
                  helperText="Seleccione País"
                >
                  <MenuItem key={1 + "Pais"} value="PERU">
                    Perú
                  </MenuItem>
                </TextField>
              </Grid>
              <Grid item xs={8} sm={8} md={12}>
                <TextField
                  id="Provincia"
                  select
                  value={Province}
                  onChange={handleChange}
                  onChange={(e) => handleChange(e, 1)}
                  helperText="Seleccione Departamento o Provincia"
                  disabled={Country.length === 0}
                >
                  {SearchProvincias().map((option, index) => (
                    <MenuItem key={index + 1 + "Provincia"} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              {DistrictList.length > 1 && (
                <Grid item xs={4} sm={4} md={12}>
                  <TextField
                    id="Distrito"
                    select
                    helperText="Distrito"
                    value={District}
                    onChange={(e) => handleChange(e, 3)}
                    helperText="Seleccione Distrito"
                    disabled={DistrictList.length === 0}
                  >
                    {DistrictList.map((option, index) => (
                      <MenuItem key={index + 1 + "Distrito"} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
              )}
            </Grid>

            {filter.length == 1 && (
              <Container maxWidth="md">
                <div
                  style={{
                    height: "100%",
                    width: "100%",
                    backgroundColor: "#d3d3d394",
                    borderRadius: ".5rem",
                  }}
                  className="p-2 text-center"
                >
                  <ul>
                    <Typography
                      component="li"
                      variant="subtitle1"
                      align="left"
                      key="impo1"
                    >
                      Importador Senior: {filter[0].Nombre}
                    </Typography>
                    <Typography
                      component="li"
                      variant="subtitle1"
                      align="left"
                      key="impo2"
                    >
                      Teléfono: {filter[0].Telefono}
                    </Typography>
                  </ul>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    size="small"
                    style={{ marginLeft: 10 }}
                    href={
                      "https://api.whatsapp.com/send?phone=51" +
                      filter[0].Telefono
                    }
                    target="_blank"
                    startIcon={<WhatsApp />}
                  >
                    Contactar
                  </Button>
                </div>
              </Container>
            )}

            <Container
              maxWidth="md"
              className={(classes.footer, classes.heroContent)}
            >
              <Grid container spacing={10}>
                <Grid item xs={12} sm={12} md={12}>
                  <NavLink to="/Importadores">
                    <Button className="btn-style-backConcentradores mt-5 mb-5 btn btn-sm">
                      Ver Importaciones en Proceso
                    </Button>
                  </NavLink>
                </Grid>
              </Grid>
              <Copyright />
            </Container>
          </div>
        </Hidden>
      </Parallax>
    </React.Fragment>
  );
}

import React, { useState, useEffect } from "react";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
} from "reactstrap";
import { imgProducto } from "Util/Utils";
import { Link } from "react-router-dom";
import { isMovil } from "Util/Utils";
import { Carousel } from "react-responsive-carousel";
import RouteBack from "Util/RouteBack";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Floatwhatsapp from "Util/botonFlotante";
import ReactPlayer from "react-player/youtube";

export default function ProductosDetallesConcentradores(props) {
  const [muted, setMuted] = useState(false);
  const [play, setplay] = useState(true)
  useEffect(() => {
    if (props.keyCampaña === 23) {
      setMuted(true);
    }
  }, [props.keyCampaña]);

  const pauseVideo = () => {
    console.log("cha")
    if (muted === true) {
      setplay(false);
      setMuted(false);
    }
  };

  const multimedia = [
    {
      id: 4,
      name: "Concentradores de Oxígeno",
      src: "https://www.youtube.com/embed/Q1ZpfZZgzoI",
      type: "youtube",
      idCampaña: 16,
    },
    {
      id: 3,
      name: "Concentradores de Oxígeno",
      src: "https://www.youtube-nocookie.com/embed/_baTNgLo8So",
      type: "youtube",
      idCampaña: 16,
    },
    {
      id: 7,
      name: "bombonasdeOxygeno",
      src: "https://www.youtube.com/embed/2xvaKwMKhMg",
      type: "youtube",
      idCampaña: 21,
    },
    {
      id: 8,
      name: "Concentradores de Oxígeno",
      src: "https://www.youtube-nocookie.com/embed/_baTNgLo8So",
      type: "youtube",
      idCampaña: 22,
    },
    {
      id: 9,
      name: "saldos",
      src: "https://www.youtube.com/embed/_7TppKVmVmQ",
      type: "youtube",
      idCampaña: 23,
    },
    {
      id: 10,
      name: "Concentradores de Oxígeno",
      src: "https://www.youtube-nocookie.com/embed/_baTNgLo8So",
      type: "youtube",
      idCampaña: 22,
    },
    {
      id: 1,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/concentrador2.jpeg",
      type: "img",
      idCampaña: 16,
    },
    {
      id: 2,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/concentrador1.jpeg",
      type: "img",
      idCampaña: 16,
    },
    {
      id: 5,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/concentradorOxygen2.jpeg",
      type: "img",
      idCampaña: 22,
    },

    {
      id: 6,
      name: "bombonasdeOxygeno",
      src: "/assets/img/bombonas de Oxygeno.jpeg",
      type: "img",
      idCampaña: 21,
    },
  ];

  const Manual = [
    {
      id: 3,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/bombonas de Oxygeno.jpeg",
      type: "youtube",
      idCampaña: 21,
    },

    {
      id: 1,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/manualConcentradores.pdf",
      type: "img",
      idCampaña: 16,
    },
    {
      id: 2,
      name: "Concentradores de Oxígeno",
      src: "/assets/img/manualConcentradoresOxygen.pdf",
      type: "img",
      idCampaña: 22,
    },
  ];
  var pdf = Manual.find((item) => item.idCampaña === props.keyCampaña);

  return (
    <Container className="mt-5 pt-5">
      <Row>
        <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
          <div className="font text-center pb-2">Importaciones Grupales</div>
        </Col>
      </Row>

      <div>
        <Col
          xs="12"
          md="12"
          lg="10"
          sm="12"
          style={{ display: "flex", justifyContent: "space-between" }}
          className="mx-auto pt-2 pb-2"
        >
          {" "}
          <Link to="/Importadores">
            <RouteBack />
          </Link>
          <div className="titleDetallesConcentradores color-blue-2 text-center pb-2">
            {props.campañas.name}
          </div>
          <img
            key="img"
            src={imgProducto(props.campañas.destino)}
            style={{ height: isMovil() ? "3rem" : "3.5rem" }}
            alt="logo"
          />
        </Col>

        <Row>
          <Col
            xs="12"
            md="12"
            lg="10"
            sm="12"
            className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
          >
            <Row className="mx-auto">
              <Col xs="12" md="6">
                <Card
                  className="p-1"
                  style={{
                    alignItems: "center",
                  }}
                >
                  <CardTitle
                    className="mt-2 mb-2 p-2 titleDetallesConcentradores"
                    tag="h2"
                  >
                    TESTIMONIOS
                  </CardTitle>
                  <Carousel
                    infiniteLoop={false}
                    autoPlay={false}
                    showThumbs={false}
                    className="carrouselWidth"
                    onChange={() => pauseVideo()}
                  >
                    {props.keyCampaña === 23 ? (
                      <ReactPlayer
                        url="https://www.youtube.com/watch?v=jx5qAscZ_EM"
                        className="imgDetails p-0 m-0"
                        key={"videoDiv"}
                        width="100%"
                        height="100%"
                        playing={play}
                        muted={muted}
                        controls
                        config={{
                          youtube: {
                            playerVars: {
                              autoplay: "1",
                              origin: `${window.location.hostname}`,
                            },
                          },
                        }}
                      />
                    ) : (
                      <img
                        src={props.campañas.portada}
                        className="imgDetails p-1"
                        alt="logo"
                        key={"img2"}
                      />
                    )}

                    {multimedia
                      .filter((pic) => {
                        if (props.keyCampaña === pic.idCampaña) {
                          return pic;
                        }
                      })
                      .map((picture, index) =>
                        picture.type === "youtube" ? (
                          <iframe
                            className="imgDetails p-0 m-0"
                            key={index + "videoDiv"}
                            src={picture.src}
                            title="YouTube video player"
                            frameBorder="0"
                            width="80%"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                          ></iframe>
                        ) : (
                          <img
                            src={picture.src}
                            className="imgDetails p-1"
                            alt="logo"
                            key={index + "img2"}
                          />
                        )
                      )}
                  </Carousel>

                  <CardBody
                    style={{
                      padding: "1rem 0px 0px",
                      width: "100%",
                      textAlign: "center",
                    }}
                  >
                    <CardTitle className="m-0 p-0">
                      {pdf && (
                        <Button
                          onClick={() => {
                            window.open(
                              pdf && pdf.src,
                              isMovil() ? "_blank" : "Popup",
                              "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30"
                            );
                          }}
                          className="arial mb-2 btn-style-ConcentadoresPdf text-center  btn btn-sm"
                        >
                          Ver Manual <i className="ml-2 simple-icon-eye" />
                        </Button>
                      )}
                      {props.keyCampaña === 16 && (
                        <Button
                          onClick={() => {
                            window.open(
                              "/assets/img/fichaConcentradores.pdf",
                              isMovil() ? "_blank" : "Popup",
                              "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30"
                            );
                          }}
                          className="arial btn-style-ConcentadoresPdf text-center  btn btn-sm"
                        >
                          Ver Ficha Técnica{" "}
                          <i className="ml-2 simple-icon-eye" />
                        </Button>
                      )}
                    </CardTitle>
                  </CardBody>
                </Card>
              </Col>
              <Col xs="12" md="6">
                <h2 className="mt-2 p-2 titleDetallesConcentradores">
                  RESUMEN
                </h2>

                <p className="NoPadding fontDetallesConcentradores">
                  <ul>
                    <li>
                      <i className="text-one simple-icon-check check-style" />{" "}
                      Via de Importación:{" "}
                      {props.campañas.via
                        ? props.campañas.via == 0
                          ? "Marítima"
                          : "Aérea"
                        : "Por Definir"}
                    </li>
                    {props.keyCampaña == 16 ||
                      (props.keyCampaña == 22 && (
                        <>
                          <li>
                            <i className="text-one simple-icon-check check-style" />{" "}
                            1 Nebulizador
                          </li>
                          <li>
                            <i className="text-one simple-icon-check check-style" />{" "}
                            1 Solo Flujo
                          </li>
                        </>
                      ))}
                  </ul>
                </p>
                <p className="NoPadding fontDetallesConcentradores">
                  <ul>
                    {" "}
                    <li className="arial mb-3">Fechas de corte</li>
                    <li>
                      <i className="text-one simple-icon-calendar check-style" />{" "}
                      Fecha de Cierre{" "}
                      {props.campañas.cierre_corto
                        ? props.campañas.cierre_corto
                        : "Por definir"}
                    </li>
                    <li>
                      <i className="text-one simple-icon-calendar check-style" />{" "}
                      Fecha de llegada{" "}
                      {props.campañas.llegada_corto
                        ? props.campañas.llegada_corto + "Aprox"
                        : "Por definir"}
                    </li>
                  </ul>
                </p>
                {props.keyCampaña === 16 && (
                  <p className="NoPadding fontDetallesConcentradores">
                    <ul>
                      <li className="arial mb-3">
                        Los pagos se dividirán en 3 partes
                      </li>
                      <li>
                        <i className="text-one simple-icon-check check-style" />{" "}
                        33% Abril 15
                      </li>
                      <li>
                        <i className="text-one simple-icon-check check-style" />{" "}
                        33% mayo 05
                      </li>
                      <li>
                        <i className="text-one simple-icon-check check-style" />{" "}
                        34% a la llegada de la Mercancía
                      </li>
                    </ul>
                  </p>
                )}
                <p className="NoPadding fontDetallesConcentradores">
                  <ul>
                    <li className="mb-3">
                      <a
                        href="https://chat.whatsapp.com/GQunqHtcmPR4X5unyVQ9qc"
                        target="_blank"
                      >
                        Mas información únete a{" "}
                        <span
                          style={{
                            textDecoration: "underline",
                            fontWeight: "bold",
                          }}
                          className="color-green"
                        >
                          grupo de WhatsApp
                        </span>
                      </a>
                    </li>
                  </ul>
                </p>
              </Col>
              <Col xs="12" md="12" sm="12" className="p-3">
                <Link to="/Importadores">
                  <Button className="btn-style-backConcentradores" size="sm">
                    <i className="simple-icon-arrow-left-circle" /> Volver a
                    Sección de Importadores
                  </Button>
                </Link>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <Floatwhatsapp />
    </Container>
  );
}

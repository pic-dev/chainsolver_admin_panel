import React, { Fragment, Component } from "react";
import {
  Input,
  Form,
  Row,
  Col,
  Collapse,
  Button,
  FormGroup,
  Label,
  NavbarToggler,
  Alert,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import SideBar from "../sidebar/SideBar";
import { getCurrentTime, getDateWithFormat } from "Util/Utils";
import { Typeahead } from "react-bootstrap-typeahead";
import { DataGrid } from "@material-ui/data-grid";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
const ModalProductDetails = React.lazy(() =>
  import("Util/Modals/modalProductDetails")
);
var timer;

class ImpoAdmin extends Component {
  constructor() {
    super();
    this.state = {
      date: "9999-01-01",
      Producto: "",
      TipoMerca: "",
      PaisDesti: "",
      clients: [],
      product: [],
      stock: [],
      items: [],
      sidebarIsOpen: true,
      isOpenItem: false,
      statusTabla: true,
      isOpenAlert: false,
      isOpenAlertRemove: false,
      toggleProductDetails: false,
      toggleProductDetailsdata: [],
      clienteSelected: [],
    };
    this.handleForm = this.handleForm.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
    this.toggleAlertRemove = this.toggleAlertRemove.bind(this);
    this.toggleProductDetails = this.toggleProductDetails.bind(this);
    this.toggleProductDetailsOpen = this.toggleProductDetailsOpen.bind(this);
    this.handleRemoveSpec = this.handleRemoveSpec.bind(this);
    this.renderView = this.renderView.bind(this);
    this.renderTable = this.renderTable.bind(this);
    this.addItem =   this.addItem.bind(this);
  }

  componentDidMount() {
    if (screen.width <= 769) {
      this.setState((prevState) => ({
        sidebarIsOpen: !prevState.sidebarIsOpen,
      }));
    }
    var databaseproduct = firebaseConf.database().ref("product");
    var databasestock = firebaseConf.database().ref("stock");
    var database = firebaseConf.database().ref("clients");

    databaseproduct.on("child_added", (snapshot) => {
      var key = [{ key: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);

      this.setState({
        product: this.state.product.concat(data),
      });
    });

    database.on("child_added", (snapshot) => {
      var key = [{ key: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);
      this.setState({
        clients: this.state.clients.concat(data),
      });
    });

    databasestock.on("child_added", (snapshot) => {
      var key = [{ id: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);
      this.setState({
        stock: this.state.stock.concat(data),
      });
      this.renderView();
    });

    databasestock.on("child_changed", (snapshot) => {
      console.log(snapshot.val());
    });
  }
  handleRemove(e) {
    firebaseConf
      .database()
      .ref("/stock/" + e)
      .remove()
      .then(() => {
        let data = this.state.stock.filter(function (item) {
          if (e !== item.key) {
            return item;
          }
        });
        this.setState({
          stock: data,
          isOpenAlertRemove: true,
        });
        this.renderView();
        this.renderTable();
        timer = setTimeout(() => {
          this.setState({
            isOpenAlertRemove: false,
          });
        }, 3000);
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  }
  handleUpdate(e) {
    firebaseConf
      .database()
      .ref("/stock/" + e)
      .update({ activo: false })
      .then(function () {
        this.renderView();
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
  }
  toggleSidebar() {
    this.setState((prevState) => ({
      sidebarIsOpen: !prevState.sidebarIsOpen,
    }));
  }
  toggleItem() {
    this.setState((prevState) => ({
      isOpenItem: !prevState.isOpenItem,
    }));
  }
  toggleAlert() {
    this.setState((prevState) => ({
      isOpenAlert: !prevState.isOpenAlert,
    }));
  }
  toggleAlertRemove() {
    this.setState((prevState) => ({
      isOpenAlertRemove: !prevState.isOpenAlertRemove,
    }));
  }
  handleRemoveSpec(e) {
    var key = e.target.value;
    let data = this.state.items.filter(function (item) {
      if (key != item.key) {
        return item;
      }
    });
    this.setState({
      items: data,
    });
  }
  addItem(e) {
    e.preventDefault();

    var data = this.state.clienteSelected;

    if (data.length > 0) {
      var newItem = {
        Cliente: data[0].key,
        contacto: data[0].contacto,
        Pais: data[0].Pais,
        Provincia: data[0].Provincia,
        telefono: data[0].telefono,
        key: Date.now(),
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem),
        };
      });
      this.setState({
        clienteSelected: [],
      });

      this.renderView();
    } else {
      alert("Seleccióne un cliente");
    }
  }
  toggleProductDetails(e) {
    var key = e.target.value;
    let data = this.state.clients.filter(function (item) {
      if (key == item.key) {
        return item;
      }
    });
    if (data.length > 0) {
      this.setState({
        toggleProductDetailsdata: data,
        toggleProductDetails: true,
      });
    }
  }
  toggleProductDetailsOpen() {
    this.setState((prevState) => ({
      toggleProductDetails: !prevState.toggleProductDetails,
    }));
  }
  handleForm() {
    if (
      this.state.Producto.length > 0 &&
      this.state.date.length > 0 &&
      this.state.TipoMerca.length > 0 &&
      this.state.PaisDesti.length > 0
    ) {
      var key = this.state.Producto;
      let data = this.state.product.filter(function (item) {
        if (key == item.key) {
          return item;
        }
      });
      const record = {
        KeyProducto: this.state.Producto,
        NombreProducto: data[0].NombreProducto.toString(),
        Producto: data[0],
        date: this.state.date,
        clientes: this.state.items,
        TipoMerca: this.state.TipoMerca,
        PaisDesti: this.state.PaisDesti,
        orden: 0,
        activo: true,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("stock");
      const newProduct = dbRef.push();
      newProduct.set(record);

      this.setState({
        date: "",
        items: [],
        isOpenItem: false,
        isOpenAlert: true,
        Producto: "",
        TipoMerca: "",
        PaisDesti: "",
        clienteSelected: [],
      });

      document.getElementById("PaisDesti").value = "";
      document.getElementById("TipoMerca").value = "";
      document.getElementById("Producto").value = "";

   
      timer = setTimeout(() => {
        this.setState({
          isOpenAlert: false,
        });
      }, 3000);
    }
  }
  renderView() {
    if (true) {
      return (
        <div>
          <Row>
            {" "}
            <Button
              onClick={this.toggleItem}
              style={{ display: !this.state.isOpenItem ? "" : "none" }}
              className="btn-style-Green btn btn-success btn-sm"
            >
              Agregar producto en Stock / Transito
            </Button>
            <Collapse
              style={{ display: this.state.isOpenItem ? "contents" : "" }}
              isOpen={this.state.isOpenItem}
            >
              <Col xs="12" md="8" sm="12" className="mx-auto pb-1">
                <Row
                  className="mx-auto m-2 p-2"
                  style={{
                    borderRadius: "1em",
                    backgroundColor: "#d7d7d7",
                    border: "1px solid rgba(0, 0, 0, 0.18)",
                    fontWeight: "bold",
                  }}
                >
                  {" "}
                  <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                    {" "}
                    <FormGroup>
                      <Label className="arial mx-auto pb-1" for="Producto">
                        Seleccione Producto
                      </Label>
                      <Input
                        type="select"
                        name="Producto"
                        id="Producto"
                        value={this.state.Producto}
                        onChange={(e) => {
                          this.setState({
                            Producto: e.target.value,
                          });
                        }}
                      >
                        <option defaultValue value="">
                          Seleccione una opción
                        </option>
                        {this.state.product.map((item, index) => (
                          <option key={index} value={item.key}>
                            {item.NombreProducto}
                          </option>
                        ))}
                      </Input>
                    </FormGroup>
                    <FormGroup>
                      <Label className="arial mx-auto pb-1" for="TipoMerca">
                        Status del producto
                      </Label>
                      <Input
                        type="select"
                        name="TipoMerca"
                        id="TipoMerca"
                        value={this.state.TipoMerca}
                        onChange={(e) => {
                          this.setState({
                            TipoMerca: e.target.value,
                          });
                        }}
                      >
                        <option defaultValue value="">
                          Seleccione una opción
                        </option>
                        <option value="Stock">Stock inmediato</option>
                        <option value="transito">Mercancia en Transito</option>
                        <option value="Proximas">Proximas importaciones</option>
                      </Input>
                    </FormGroup>
                    {this.state.TipoMerca == "transito" && (
                      <FormGroup>
                        <Label className="arial mx-auto pb-1" for="date">
                          Fecha de llegada
                        </Label>
                        <Input
                          type="date"
                          name="date"
                          id="date"
                          value={this.state.date}
                          placeholder="Fecha de llegada"
                          onChange={(e) => {
                            this.setState({
                              date: e.target.value,
                            });
                          }}
                        />
                      </FormGroup>
                    )}
                    <FormGroup>
                      <Label className="arial mx-auto pb-1" for="PaisDesti">
                        Pais Destino
                      </Label>

                      <Input
                        type="select"
                        name="PaisDesti"
                        id="PaisDesti"
                        value={this.state.PaisDesti}
                        onChange={(e) => {
                          this.setState({
                            PaisDesti: e.target.value,
                          });
                        }}
                      >
                        <option defaultValue value="">
                          Seleccione una opción
                        </option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Perú">Peru</option>
                        <option value="Panamá">Panama</option>
                      </Input>
                    </FormGroup>
                    <Col
                      xs="12"
                      md="12"
                      sm="12"
                      className="arial mx-auto p-0 pb-1"
                    >
                      Clientes asociados
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <Form onSubmit={this.addItem}>
                        <FormGroup>
                          <Label for="Cliente">Nombre de cliente</Label>

                          <Typeahead
                            flip={true}
                            name="Cliente"
                            id="Cliente"
                            emptyLabel="Cargando..."
                            filterBy={["contacto"]}
                            labelKey={(option) => `${option.telefono}`}
                            renderMenuItemChildren={(option) => (
                              <div>
                                {" "}
                                {option.contacto} - {option.Pais} /{" "}
                                {option.Provincia} - {option.telefono}{" "}
                              </div>
                            )}
                            onChange={(clienteSelected) => {
                              this.setState({ clienteSelected });
                            }}
                            multiple={false}
                            options={this.state.clients}
                            placeholder="Clientes"
                            selected={this.state.clienteSelected}
                          />
                        </FormGroup>
                        <FormGroup className="login-form-button">
                          <Button
                            type="submit"
                            className="btn-style-blue btn btn-success btn-sm"
                          >
                            Agregar Cliente
                          </Button>
                        </FormGroup>
                      </Form>
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <ListGroup>
                        {this.state.items.map((item, index) => (
                          <ListGroupItem
                            key={item.key}
                            className="justify-content-between"
                          >
                            cliente: {item.contacto} - {item.Pais} /{" "}
                            {item.Provincia}{" "}
                            <Button
                              className="btn-style-Red btn btn-success btn-sm"
                              value={item.key}
                              style={{ lineHeight: "1" }}
                              onClick={this.handleRemoveSpec}
                            >
                              x
                            </Button>
                          </ListGroupItem>
                        ))}
                      </ListGroup>{" "}
                    </Col>
                    <FormGroup>
                      <Button
                        className="btn-style-Green btn btn-success btn-sm"
                        onClick={this.handleForm}
                      >
                        Agregar Stock
                      </Button>{" "}
                      <Button
                        className="btn-style-Red btn btn-success btn-sm"
                        onClick={this.toggleItem}
                      >
                        Cancelar
                      </Button>{" "}
                    </FormGroup>
                  </Col>
                </Row>
              </Col>
            </Collapse>
            <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
              <Row
                className="mx-auto m-2 p-2"
                style={{
                  borderTop: "1px solid rgba(0, 0, 0, 0.18)",
                }}
              >
                <Col xs="12" md="12" sm="12" className="font2 mx-auto m-2">
                  STOCK ACTUAL
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className="font2 mx-auto m-2 text-center"
                ></Col>{" "}
              </Row>
            </Col>
          </Row>
        </div>
      );
    }
  }
  renderTable() {
    const columns = [
      {
        field: "NombreProducto",
        headerName: "nombre",
        description: "name",
        sortable: true,
        width: 200,
      },
      { field: "activo", sortable: true, headerName: "status", width: 150 },
      {
        field: "Fecha",
        headerName: "Agregado",
        width: 150,
        sortable: true,
      },

      {
        field: "id",
        headerName: "Borrar",
        sortable: false,
        width: 150,
        renderCell: (params) => {
          return (
            <IconButton
              aria-label="open"
              size="small"
              onClick={() => this.handleRemove(params.value)}
            >
              <DeleteIcon />
            </IconButton>
          );
        },
      },
    ];
    if (this.state.stock.length > 1 ) {
      return (
        <DataGrid
          autoHeight
          rows={this.state.stock}
          columns={columns}
          pageSize={5}
        />
      );
    } else {
      return (
        <div className="alert alert-info text-center">
          No hay Stock agregados actualmente
        </div>
      );
    }
  }

  render() {

    return (
      <Fragment>
        <Row className="p-0">
          <NavbarToggler
            className="float-right display-sm"
            style={{
              backgroundColor: "#1370b9",
              border: "solid white 1px",
              top: "70px",
              left: "0",
              position: "fixed",
            }}
            onClick={this.toggleSidebar}
          />
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "3" : "1"}
            md={this.state.sidebarIsOpen ? "4" : "1"}
            sm="12"
            className="p-0 mx-auto"
          >
            <SideBar
              toggle={this.toggleSidebar}
              isOpen={this.state.sidebarIsOpen}
            />
          </Col>
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "9" : "11"}
            md={this.state.sidebarIsOpen ? "8" : "11"}
            sm="12"
            className="p-1 mx-auto"
          >
            <Row className="p-1">
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <h2>IMPORTACIONES GRUPALES - Productos</h2>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlert}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Agregado al stock Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertRemove}
                  toggle={this.toggleAlertRemove}
                >
                  ¡Producto Removido stock del Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.renderView()}
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.renderTable()}
              </Col>
            </Row>
          </Col>
        </Row>

        {this.state.toggleProductDetailsdata.length > 0 && (
          <ModalProductDetails
            active={this.state.toggleProductDetails}
            item={this.state.toggleProductDetailsdata}
            toggle={this.toggleProductDetailsOpen}
          />
        )}
      </Fragment>
    );
  }
}

export default ImpoAdmin;

import React, { useState } from "react";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  Table,
  TabContent,
  TabPane,
} from "reactstrap";
import { imgProducto } from "Util/Utils";
import { Link } from "react-router-dom";
import { isMovil } from "Util/Utils";
import { Carousel } from "react-responsive-carousel";
import classnames from "classnames";
import RouteBack from "Util/RouteBack";
import "react-responsive-carousel/lib/styles/carousel.min.css";
const Likes = React.lazy(() => import("Util/likes"));

export default function ProductosDetallesFirebase(props) {
  const [activeTab, setActiveTab] = useState("2");

  const tab = (tab) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };
console.log(props)
  return (
    <Container className="mt-5 pt-5">
      <Row>
        <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
          <div className="font text-center pb-2">Importaciones Grupales</div>
        </Col>
      </Row>
      {props.toggleProducto ? (
        <div>
          <Col
            xs="12"
            md="10"
            sm="12"
            style={{ display: "flex", justifyContent: "space-between" }}
            className="mx-auto pt-2 pb-2"
          >
            <RouteBack />
            <div className="font2 color-blue-2 text-center pb-2">
              {props.producto.NombreProducto.toUpperCase()}
            </div>
            <img
              key="img"
              src={imgProducto(props.producto.PaisDesti)}
              style={{ height: isMovil() ? "3rem" : "4rem" }}
              alt="logo"
            />
          </Col>

          <Row>
            <Col
              xs="12"
              md="10"
              sm="12"
              className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
            >
              <Row className="mx-auto">
                <Col xs="12" md="6">
                  <Card
                    className="p-1"
                    style={{
                      alignItems: "center",
                    }}
                  >
                    <Carousel
                      infiniteLoop={true}
                      autoPlay={false}
                      showThumbs={false}
                      showIndicators={false}
                      stopOnHover={true}
                      showStatus={false}
                      className="carrouselWidth"
                    >
                      {props.producto.Multimedia[1] ? (
                        props.producto.Multimedia[1].video.map(
                          (picture, index) => (
                            <div key={index + "videoDiv"}>
                              <video
                                className="react-player imgDetails"
                                key={index + "video"}
                                id={"my-video" + index}
                                controls
                                width="100%"
                                height="100%"
                                data-setup="{}"
                                autoPlay
                                muted
                              >
                                <source
                                  src={picture.img}
                                  type={picture.fileType}
                                />
                              </video>
                            </div>
                          )
                        )
                      ) : (
                        <img
                          src={props.producto.Multimedia[0].imagen[0].img}
                          className="imgDetails p-2"
                          alt="logo"
                          key="img2"
                        />
                      )}

                      {props.producto.Multimedia[0].imagen.map(
                        (picture, index) => (
                          <div key={index + "imagenPortadaDiv"}>
                            <img
                              key={index + "imagenPortada"}
                              src={picture.img}
                              className="imgDetails p-2"
                              alt="logo"
                            />
                          </div>
                        )
                      )}
                    </Carousel>
                    <Likes item={props.keyLike} />
                    <CardBody
                      style={{
                        padding: "1rem 0px 0px",
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      <CardTitle
                        className="m-0 p-0"
                        style={{ fontWeight: "bold" }}
                      >
                        {" "}
                        <a
                          style={{ padding: "0px" }}
                          href="https://wa.link/xbr57j"
                          rel="noopener noreferrer"
                          target="_blank"
                        >
                          {" "}
                          <i className="whatsapp-contacto" />{" "}
                        </a>
                      </CardTitle>
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="12" md="6">
                  <Row>
                    <Col
                      xs="6"
                      md="6"
                      className={`text-center arial tab1 ${classnames({
                        active2: activeTab === "2",
                      })}`}
                      onClick={() => {
                        tab("2");
                      }}
                    >
                      Importadores asociados
                    </Col>
                    <Col
                      xs="6"
                      md="6"
                      className={`text-center arial tab1 ${classnames({
                        active2: activeTab === "1",
                      })}`}
                      onClick={() => {
                        tab("1");
                      }}
                    >
                      Especificaciones
                    </Col>
                  </Row>
                  <TabContent className="mx-auto " activeTab={activeTab}>
                    <TabPane tabId="1">
                      <Row className="mx-auto pt-2">
                        <Col className="p-0" xs="12" md="12">
                          <div
                            className="p-1"
                            style={{
                              alignItems: "center",
                            }}
                          >
                            {" "}
                            <div
                              style={{
                                padding: "1rem 0 1rem 0",
                                width: "100%",
                                textAlign: "center",
                              }}
                            >
                              <div>
                                <Table>
                                  <tbody>
                                    {props.producto.Caracteristicas.map(
                                      (item) => (
                                        <tr
                                          className="p-0 text-center"
                                          key={item.key + "Especificaciones1"}
                                        >
                                          <th
                                            scope="row"
                                            style={{
                                              fontWeight: "bold",
                                              backgroundColor: "#80808038",
                                            }}
                                          >
                                            {item.nombre}
                                          </th>
                                          <td>{item.descripción}</td>
                                        </tr>
                                      )
                                    )}
                                  </tbody>
                                </Table>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>{" "}
                    </TabPane>
                    <TabPane tabId="2">
                      <Row className="mx-auto pt-2">
                        <Col
                          className="p-0"
                          xs="12"
                          md="12"
                          className="NoPadding text-center"
                        >
                          <Table>
                            <thead>
                              <tr>
                                <th>Contacto</th>
                                <th>País</th>
                                <th>Provincia</th>
                                <th>Telefono</th>
                              </tr>
                            </thead>
                            {props.ClienteStock !== undefined  ? (
                              <tbody>
                                {props.ClienteStock.map((item) => (
                                  <tr
                                    className="p-0 text-center"
                                    key={item.key + "clientes"}
                                  >
                                    <th
                                      scope="row"
                                      style={{
                                        fontWeight: "bold",
                                        backgroundColor: "#80808038",
                                      }}
                                    >
                                      {item.contacto}
                                    </th>
                                    <td>{item.Pais}</td>
                                    <td>{item.Provincia}</td>
                                    <td>{item.telefono}</td>
                                  </tr>
                                ))}
                              </tbody>
                            ) : (
                              <tbody>
                                <tr
                                  className="p-0 text-center"
                                  key={"clientes"}
                                >
                                  <th
                                    scope="row"
                                    style={{
                                      fontWeight: "bold",
                                      backgroundColor: "#80808038",
                                    }}
                                    colSpan="4"
                                  >
                                    Ho hay clientes asignados
                                  </th>
                                </tr>
                              </tbody>
                            )}
                          </Table>
                        </Col>
                      </Row>
                    </TabPane>
                  </TabContent>
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className="p-3"
                  style={{ textAlign: "center" }}
                >
                  <Link to="/Importadores">
                    <Button className="btn-style-backConcentradores" size="sm">
                      Volver a Sección de Importadores
                    </Button>
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      ) : (
        <Row>
          <Col xs="12" md="12" sm="12" className=" mx-auto text-center m-3">
            Cargando... <span className="loadingBlock"></span>{" "}
          </Col>
        </Row>
      )}{" "}
    </Container>
  );
}

import React, { Fragment, Component } from "react";
import {
  Row,
  Col,
  Collapse,
  Button,
  Alert,
} from "reactstrap";
import TablePedido from "./tablePedido";
import PreOrdenForm from "./PreOrdenForm";
import axios from "axios";

var timer;
export default class Pedido extends Component {
  constructor() {
    super();
    this.state = {
      isOpenAlert: false,
      isOpenAlertError: false,
      isOpenAlertRemove: false,
      Telefono: "",
      cliente: "",
      catalogo: "",
      items: [],
      itemSumary: [],
      isOpenItem: true,
      isOpenItemEdit: false,
      tipoImportador: [],
      tipoImportadorSelected: "",
      idOrden: "",
    };

    this.handleRemoveProduct = this.handleRemoveProduct.bind(this);
    this.addItem = this.addItem.bind(this);
    this.handleForm = this.handleForm.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
    this.toggleAlertError = this.toggleAlertError.bind(this);

    this.validate = this.validate.bind(this);
    this.SaveProduct = this.SaveProduct.bind(this);
  }

  componentDidMount() {
    axios
      .get("https://point.qreport.site/importador")
      .then((res) => {
        this.setState({
          tipoImportador: res.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });

      
  }

  componentWillUnmount() {
    clearTimeout(timer);
  }

  toggleItem() {
    this.setState((prevState) => ({
      isOpenItem: !prevState.isOpenItem,
    }));
    this.setState({
      itemSumary: [],
      img: [],
      nombreProducto: "",
      fileType: "",
      items: [],
    });
  }
  toggleAlert() {
    this.setState((prevState) => ({
      isOpenAlert: !prevState.isOpenAlert,
    }));
  }

  toggleAlertError() {
    this.setState((prevState) => ({
      isOpenAlertError: !prevState.isOpenAlertError,
    }));
  }

  validate() {
    if (this.state.items.length > 0) {
      this.handleForm();
    } else {
      this.setState({
        isOpenAlertRemove: true,
      });

      timer = setTimeout(() => {
        this.setState({
          isOpenAlertRemove: false,
        });
      }, 3000);
    }
  }
  handleRemoveProduct(e) {
    let data = this.state.items.filter((item) => {
      if (e !== item.key) {
        return item;
      }
    });
    this.setState({
      items: data,
    });
  }
  addItem(e) {
    this.setState((prevState) => {
      return {
        items: prevState.items.concat(e),
        itemSumary: prevState.itemSumary.concat(e),
      };
    });
  }

  SaveProduct = async () => {
    await axios
      .get("https://point.qreport.site/orden")
      .then((res) => {
        this.setState({
          idOrden: res.data[0].orden,
        });
        axios
          .post("https://point.qreport.site/orden", {
            norden: res.data[0].orden,
          })
          .then(() => {
            this.state.items.map((item) => {
              axios
                .post("https://point.qreport.site/productos", {
                  cliente:
                    this.props.JuniorData.length > 0
                      ? this.props.JuniorData[0].id
                      : 18,
                  catalogo: item.Catalogo,
                  orden: res.data[0].orden,
                  codigo_producto: item.CodigoProducto,
                  pagina: Number(item.Pagina),
                  producto: item.Producto,
                  cantidad: Number(item.cantidad),
                  precio: item.precio > 0 ? item.precio : 0,
                  precio_compra:item.precioCompra,
                  observacion: item.observacion,
                })
                .then((res) => {
                  if (res.data.boolean) {
                    this.setState({
                      Telefono: "",
                      cliente: "",
                      items: [],
                      isOpenItem: false,
                      isOpenAlert: true,
                    });
                  } else {
                    this.setState({
                      isOpenAlertError: true,
                    });
                    timer = setTimeout(() => {
                      this.setState({
                        isOpenAlertError: false,
                      });
                    }, 3000);
                  }
                })
                .catch((error) => {
                  console.log(error);
                });
            });
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleForm() {
    if (this.state.items.length > 0) {
      this.SaveProduct();
    } else {
      this.setState({
        isOpenAlertRemove: true,
      });

      timer = setTimeout(() => {
        this.setState({
          isOpenAlertRemove: false,
        });
      }, 3000);
    }
  }

  render() {
    return (
      <Fragment  >
        <Row>
          <Col className="NoPadding" xs="12" md="12">
            <div
              style={{
                width: "100%",
                height: "2.5rem",
                borderRadius: "0.4rem",
                textTransform: "uppercase",
                fontSize: "0.9rem",
              }}
              className="text-white text-center arial mb-2 p-2 bg-blue-2"
              onClick={this.toggleItem}
            >
              <span>Realizar Pre Orden en Linea</span>
              <i
                style={{
                  backgroundColor: "white",
                  borderRadius: "50%",
                  fontSize: "1rem",
                  padding: ".2rem",
                  fontWeight: "bold",
                  cursor: "pointer",
                }}
                className={`mr-2 ml-2 float-right color-orange ${
                  this.state.isOpenItem
                    ? "simple-icon-arrow-up"
                    : "simple-icon-arrow-down"
                }`}
              />
            </div>
            {this.props.catalogo.statusCp !== "Ok" &&
              this.props.JuniorData.length > 0 &&
              this.props.tipo === "catalogos" && (
                <div
                  style={{
                    width: "100%",
                    borderRadius: "0.4rem",
                    textTransform: "uppercase",
                    fontSize: "1rem",
                  }}
                  className="text-center alert alert-danger  arial mb-2 p-2 alert alert-red"
                >
                  <span>catalogo con precios - por confirmar</span>
                  <i
                    style={{
                      borderRadius: "50%",
                      fontSize: "1rem",
                      padding: ".2rem",
                      fontWeight: "bold",
                      cursor: "pointer",
                    }}
                    className="mr-2 ml-2 simple-icon-info"
                  />
                </div>
              )}
            <Collapse className="p-2" isOpen={this.state.isOpenItem}>
              <Row>
                <Col xs="12" md="12" sm="12" className="NoPadding mx-auto pb-1">
                  <Row
                    className="mx-auto m-2 p-2"
                    style={{
                      borderRadius: ".5em",
                      border: "1px solid rgba(0, 0, 0, 0.18)",
                    }}
                  >
                    <PreOrdenForm
                      tipo={this.props.tipo}
                      addItem={this.addItem}
                      JuniorData={this.props.JuniorData}
                      catalogo={this.props.catalogo}
                      articulo={this.props.articulo}
                      setArticuloSelected={this.props.setArticuloSelected}
                    />

                    {this.state.items.length > 0 && (
                      <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                        {" "}
                        <hr />
                        <TablePedido
                          JuniorData={this.props.JuniorData}
                          tipo={"proceso"}
                          remove={this.handleRemoveProduct}
                          productos={this.state.items}
                        />
                      </Col>
                    )}
                    {this.state.items.length > 0 && (
                      <Col xs="12" md="12" sm="12" className="mx-auto m-2 p-2">
                        <Button
                          className="text-center btn-style-Green btn btn-success btn-sm"
                          onClick={this.validate}
                        >
                          GENERAR PRE-ORDEN
                        </Button>{" "}
                      </Col>
                    )}
                  </Row>
                </Col>
              </Row>
            </Collapse>
          </Col>
          <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
            <Alert
              color="info"
              isOpen={this.state.isOpenAlert}
              toggle={this.toggleAlert}
            >
              ¡Pre Orden Agregada Exitosamente!, Su ID:{this.state.idOrden}
            </Alert>
            {this.props.JuniorData.length == 0 && (
              <Alert
                color="danger"
                isOpen={this.state.isOpenAlert}
                toggle={this.toggleAlert}
              >
                IMPORTANTE: Debera contactar a un asesor PIC Cargo para
                formalizar su orden.
                <a
                  href={"https://api.whatsapp.com/send?phone=51920301745"}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  +51920301745
                </a>
              </Alert>
            )}
            {this.state.isOpenAlert && this.state.itemSumary.length > 0 && (
              <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                {" "}
                <hr />
                <TablePedido
                  tipo={"completada"}
                  JuniorData={this.props.JuniorData}
                  remove={this.handleRemoveProduct}
                  productos={this.state.itemSumary}
                />
              </Col>
            )}
          </Col>
          <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
            <Alert
              color="info"
              isOpen={this.state.isOpenAlertError}
              toggle={this.toggleAlertError}
            >
              Hubo un error al cargar la Orden, intente nuevamente
            </Alert>
          </Col>
          <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
            <Alert
              color="danger"
              isOpen={this.state.isOpenAlertRemove}
              toggle={this.toggleAlert}
            >
              ¡Hay campos que requieren verificación!
            </Alert>
          </Col>
        </Row>
     </Fragment>
    );
  }
}

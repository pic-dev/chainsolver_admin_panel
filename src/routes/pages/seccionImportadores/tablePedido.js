import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutline";

import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
const useStyles = makeStyles({
  table: {
    minWidth: "auto",
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.map(({ subTotal }) => subTotal).reduce((sum, i) => sum + i, 0);
}

export default function SpanningTable(props) {
  const classes = useStyles();

  const invoiceTotal = subtotal(props.productos);
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell align="center" colSpan={6}>
              Detalle de Pre-Orden
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Producto</TableCell>

            <TableCell>Qty.</TableCell>
            {props.JuniorData.length > 0 && (
              <TableCell>Precio Unitario</TableCell>
            )}
            {props.JuniorData.length > 0 && <TableCell>Sub-Total</TableCell>}

            <TableCell>Pagina</TableCell>
            <TableCell>Codigo</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.productos.map((row) => (
            <Tooltip title={row.observacion}>
              <TableRow key={row.key}>
                <TableCell>{row.Producto}</TableCell>
                <TableCell>{row.cantidad}</TableCell>
                {props.JuniorData.length > 0 && (
                  <TableCell
                    className={`${row.precio > 0 ? "" : "color-red arial"}`}
                  >
                    {row.precio > 0 ? row.precio : "P/C"}
                  </TableCell>
                )}
                {props.JuniorData.length > 0 && (
                  <TableCell>
                    {row.precio > 0
                      ? ccyFormat(priceRow(row.precio, row.cantidad))
                      : "-"}
                  </TableCell>
                )}
                <TableCell>{row.Pagina}</TableCell>
                <TableCell>
                  {row.CodigoProducto.length > 0 ? row.CodigoProducto : "-"}
                </TableCell>
                {props.tipo == "proceso" && (
                  <TableCell align="right">
                    <IconButton
                      edge="end"
                      color="inherit"
                      onClick={() => props.remove(row.key)}
                      aria-label="delete"
                    >
                      {" "}
                      <DeleteOutlinedIcon />
                    </IconButton>
                  </TableCell>
                )}
              </TableRow>
            </Tooltip>
          ))}
          {invoiceTotal > 0 && (
            <TableRow>
              <TableCell className="text-center" colSpan={3}>
                Total
              </TableCell>
              <TableCell>{ccyFormat(invoiceTotal)}</TableCell>
            </TableRow>
          )}

          <TableRow>
            <TableCell className="text-center color-red" colSpan={7}>
              *p/c - Precio por Confirmar
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

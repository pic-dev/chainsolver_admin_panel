import React, { Fragment, Component} from "react";
import FileUpload from "Util/formItems/FileUpload";

import {
  Input,
  Row,
  Col,
  Collapse,
  Button,
  FormGroup,
  Label,
  ListGroupItem,
  NavbarToggler,
  Alert,
} from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import SideBar from "../sidebar/SideBar";

import { getCurrentTime, getDateWithFormat } from "Util/Utils";
import { Title } from "Util/helmet";

var timer;
class ImpoAdmin extends Component {
  constructor() {
    super();
    this.state = {
      isOpenAlert: false,
      isOpenAlertRemove: false,
      isOpenAlertUpdate: false,
      img: [],
      fileType: "",
      Tipo: "",
      product: [],
      uploadValue: 0,
      sidebarIsOpen: true,
      isOpenItem: false,
      isOpenItemEdit: false,
      toggleProductDetails: false,
      toggleProductDetailsdata: [],
    };

    this.handleRemoveSpec = this.handleRemoveSpec.bind(this);
    this.handleRemoveMultime = this.handleRemoveMultime.bind(this);
    this.handleForm = this.handleForm.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleProductDetails = this.toggleProductDetails.bind(this);
    this.toggleProductDetailsOpen = this.toggleProductDetailsOpen.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
  }

  componentDidMount() {
    if (screen.width <= 769) {
      this.setState((prevState) => ({
        sidebarIsOpen: !prevState.sidebarIsOpen,
      }));
    }

    var database = firebaseConf.database().ref("testimonios");

    database.on("child_added", (snapshot) => {
      var key = [{ key: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);

      this.setState({
        product: this.state.product.concat(data),
      });
    });

    database.on("child_removed", (snapshot) => {
      var eliminado = snapshot.key;
      let data = this.state.product.filter(function (item) {
        if (eliminado !== item.key) {
          return item;
        }
      });
      this.setState({
        product: data,
      });
      this.renderLoginButton();
    });
  }
  componentWillUnmount() {
    clearTimeout(timer);
  }
  toggleSidebar() {
    this.setState((prevState) => ({
      sidebarIsOpen: !prevState.sidebarIsOpen,
    }));
  }
  toggleItem() {
    this.setState((prevState) => ({
      isOpenItem: !prevState.isOpenItem,
    }));

    this.setState({
      img: [],
      Tipo: "",
      fileType: "",
      uploadValue: 0,
    });
  }
  toggleAlert() {
    this.setState((prevState) => ({
      isOpenAlert: !prevState.isOpenAlert,
    }));
  }
  toggleProductDetails(e) {
    var key = e.target.value;
    let data = this.state.product.filter(function (item) {
      if (key == item.key) {
        return item;
      }
    });
    if (data.length > 0) {
      this.setState({
        toggleProductDetailsdata: data,
        toggleProductDetails: true,
      });
    }
  }
  toggleProductDetailsOpen() {
    this.setState((prevState) => ({
      toggleProductDetails: !prevState.toggleProductDetails,
    }));
  }




  handleRemoveMultime(value) {
    var data = this.state.img.filter(function (item, index) {
      if (value !== index) {
        return item;
      }
    });

    if (data.length > 0) {
      this.setState({
        img: data,
      });
    }
  }

  handleUpload(e) {
    const file = e.target.files[0];
    const storageRef = firebaseConf.storage().ref(`fotos/${file.name}`);
    const task = storageRef.put(file);

    // Listener que se ocupa del estado de la carga del fichero
    task.on(
      "state_changed",
      (snapshot) => {
        // Calculamos el porcentaje de tamaño transferido y actualizamos
        // el estado del componente con el valor
        let percentage =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.setState({
          uploadValue: percentage,
        });
      },
      (error) => {
        // Ocurre un error
        console.error(error.message);
      },
      () => {
        task.snapshot.ref.getDownloadURL().then((downloadURL) => {
          var data = [
            { img: downloadURL, fileType: file.type, key: Date.now() },
          ];
          this.setState({
            img: this.state.img.concat(data),
            fileType: file.type,
          });
        });
      }
    );
  }
  handleForm() {
    if (
      this.state.Tipo.length > 0 &&
      this.state.img.length > 0 
    ) {
      const record = {
        Multimedia: this.state.img,
        Tipo: this.state.Tipo,
        fileType: this.state.fileType,
        activo: true,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("testimonios");
      const newProduct = dbRef.push();
      newProduct.set(record);

      this.setState({
        img: [],
        Tipo: "",
        fileType: "",
        uploadValue: 0,
        isOpenItem: false,

        isOpenAlert: true,
      });

      timer = setTimeout(() => {
        this.setState({
          isOpenAlert: false,
        });
      }, 3000);
    } else {
      alert("Hay campos que requieren verificación");
    }
  }
  renderLoginButton() {
    if (true) {
      return (
        <div>
          <Row>
            <Button
              onClick={this.toggleItem}
              style={{
                display:
                  this.state.isOpenItem || this.state.isOpenItemEdit
                    ? "none"
                    : "",
              }}
              className="btn-style-Green btn btn-success btn-sm"
            >
              Agregar producto
            </Button>
            {!this.state.isOpenItemEdit ? (
              <Collapse
                style={{ display: this.state.isOpenItem ? "contents" : "" }}
                isOpen={this.state.isOpenItem}
              >
                <Col xs="12" md="8" sm="12" className="mx-auto pb-1">
                  <Row
                    className="mx-auto m-2 p-2"
                    style={{
                      borderRadius: "1em",
                      backgroundColor: "#d7d7d7",
                      border: "1px solid rgba(0, 0, 0, 0.18)",
                      fontWeight: "bold",
                    }}
                  >
                    {" "}
                    <Col xs="12" md="12" sm="12" className="arial mx-auto pb-1">
                      video
                    </Col>
                    <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                      <FileUpload
                        uploadValue={this.state.uploadValue}
                        onUpload={this.handleUpload}
                        multiple
                      />
                      {this.state.img.length > 0
                        ? this.state.img.map((item, index) => (
                            <ListGroupItem
                              key={index}
                              style={{ display: "inline-grid" }}
                              className="justify-content-between p-1"
                            >
                              {item.fileType == "video/mp4" ? (
                                <video
                                  key={index + "video"}
                                  id={"my-video" + index}
                                  style={{ height: "4rem" }}
                                  controls
                                  data-setup="{}"
                                >
                                  <source src={item.img} type={item.fileType} />
                                </video>
                              ) : (
                                <img
                                  key={index + "img"}
                                  src={item.img}
                                  style={{ height: "4rem" }}
                                  alt="logo"
                                />
                              )}
                              <Button
                                className="btn-style-Red btn btn-success btn-sm"
                                value={index}
                                style={{ lineHeight: "1" }}
                                onClick={() => this.handleRemoveMultime(index)}
                              >
                                x
                              </Button>
                            </ListGroupItem>
                          ))
                        : ""}
                    </Col>
                    <FormGroup>
                        <Label className="arial mx-auto pb-1" for="Tipo">
                          Tipo
                        </Label>

                        <Input
                          type="select"
                          name="Tipo"
                          onChange={(e) => {
                            this.setState({
                              Tipo: e.target.value,
                            });
                          }}
                        ><option defaultValue value="">
                        Seleccione una opción
                      </option>
                          <option   value="Importador">
                            Importador
                          </option>
                          <option  value="Fabricante">
                            Fabricante
                          </option>
                        </Input>
                      </FormGroup>
                  </Row>
                </Col>
                <Col xs="12" md="12" sm="12" className="mx-auto m-2 p-2">
                  <Button
                    className="btn-style-Green btn btn-success btn-sm"
                    onClick={this.handleForm}
                  >
                    Agregar producto
                  </Button>{" "}
                  <Button
                    className="btn-style-Red btn btn-success btn-sm"
                    onClick={this.toggleItem}
                  >
                    Cancelar
                  </Button>
                </Col>
              </Collapse>
            ) : (
              ""
            )}


            <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
              <Row
                className="mx-auto m-2 p-2"
                style={{
                  borderTop: "1px solid rgba(0, 0, 0, 0.18)",
                }}
              >
                <Col xs="12" md="12" sm="12" className="font2 mx-auto m-2">
                  Testimonios ACTUALES
                </Col>
      
              </Row>
            </Col>
          </Row>
        </div>
      );
    }
  }
  render() {
    return (
      <Fragment>
           
             <Title>
          <title>{((window.location.pathname).replace('/', '') )+ " | " + "PIC  - Calculadora de Fletes"}</title>
        </Title>
        <Row className="p-0">
          <NavbarToggler
            className="float-right display-sm"
            style={{
              backgroundColor: "#1370b9",
              border: "solid white 1px",
              top: "70px",
              left: "0",
              position: "fixed",
            }}
            onClick={this.toggleSidebar}
          />
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "3" : "1"}
            md={this.state.sidebarIsOpen ? "4" : "1"}
            sm="12"
            className="p-0 mx-auto"
          >
            <SideBar
              toggle={this.toggleSidebar}
              isOpen={this.state.sidebarIsOpen}
            />
          </Col>
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "9" : "11"}
            md={this.state.sidebarIsOpen ? "8" : "11"}
            sm="12"
            className="p-1 mx-auto"
          >
            <Row className="p-1">
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <h2>IMPORTADORES GRUPALES - Panel de control</h2>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlert}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Agregado Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertRemove}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Removido Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertUpdate}
                  toggle={this.toggleAlert}
                >
                  ¡Producto Actualizado Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.renderLoginButton()}
              </Col>
            </Row>
          </Col>
        </Row>

      </Fragment>
    );
  }
}

export default ImpoAdmin;

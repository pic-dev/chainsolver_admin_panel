import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  FormGroup,
  Label,
  Form,
  Input,
  Alert,
} from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import { PageView } from "Util/tracking";
import { NavLink, withRouter } from "react-router-dom";
import axios from "axios";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Title } from "Util/helmet";

const ProductosDetallesFirebase = React.lazy(() =>
  import("./ProductosDetallesFirebase")
);
const ProductosDetallesCampaña = React.lazy(() =>
  import("./ProductosDetallesCampaña")
);
const ProductosDetallesConcentradores = React.lazy(() =>
  import("./ProductosDetallesConcentradores")
);

class ProductoDetalle extends Component {
  constructor(props) {
    super(props);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubtmitJunior = this.handleSubtmitJunior.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);

    this.state = {
      ClienteStock: [],
      typeProduct: [],
      campañas: [],
      rubros: [],
      toggleProductDetailsdata: [],
      product: [],
      toggleProducto: false,
      isOpen: false,
      loginJunior: false,
      selectImportador: false,
      modal: false,
      JuniorData: [],
      isOpenAlert: false,
    };
  }

  componentDidMount() {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
    this.getData();
    var stock = firebaseConf.database().ref("/CatalagosStock/");
    stock
      .orderByChild("KeyProducto")
      .equalTo(this.props.match.params.id)
      .on("child_added", (snapshot) => {
        var product = snapshot.val();

        var databasestock = firebaseConf
          .database()
          .ref("/Catalogos/" + this.props.match.params.id);

        databasestock.on("value", (snapshot) => {
          var key = [{ key: this.props.match.params.id }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());

          var data = Object.assign(arreglo[1], arreglo[0]);

          this.setState({
            product: this.state.product.concat(Object.assign(product, data)),
          });
          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = this.props.match.params.id;
            let data = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });
            if (data.length > 0) {
              this.setState({
                toggleProductDetailsdata: [data[0]],
                ClienteStock: [data[0].clientes],
                typeProduct: data[0].TipoMerca,
              });
            }

            setTimeout(() => {
              this.setState({
                toggleProducto: true,
              });
            }, 1000);
          }
        });
      });

    var stock = firebaseConf.database().ref("/stock/");
    stock
      .orderByChild("KeyProducto")
      .equalTo(this.props.match.params.id)
      .on("child_added", (snapshot) => {
        var product = snapshot.val();

        var databasestock = firebaseConf
          .database()
          .ref("/product/" + this.props.match.params.id);

        databasestock.on("value", (snapshot) => {
          var key = [{ key: this.props.match.params.id }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());

          var data = Object.assign(arreglo[1], arreglo[0]);

          this.setState({
            product: this.state.product.concat(Object.assign(product, data)),
          });
          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = this.props.match.params.id;
            let data = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });
            if (data.length > 0) {
              this.setState({
                toggleProductDetailsdata: [data[0]],
                ClienteStock: [data[0].clientes],
                typeProduct: data[0].TipoMerca,
              });
            }

            setTimeout(() => {
              this.setState({
                toggleProducto: true,
              });
            }, 1000);
          }
        });
      });
  }

  async getData() {
    var key = this.props.match.params.id;
    await axios
      .get("https://point.qreport.site/campana")
      .then((res) => {
        var campaña = res.data.filter((item) => {
          if (item.id == key) {
            return item;
          }
        });
        if (campaña.length > 0) {
          this.setState({
            campañas: campaña,
            toggleProducto: true,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });

    await axios
      .get(`https://point.qreport.site/campana/rubros/${key}`)
      .then((res) => {
        this.setState({
          rubros: res.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleSubtmitJunior(e) {
    e.preventDefault();
    const { dni, password } = e.target.elements;
    axios
      .post("https://point.qreport.site/login/junior", {
        usuario: dni.value,
        contraseña: password.value,
      })
      .then((res) => {
        if (res.data.length > 0) {
          sessionStorage.seniorLogin = JSON.stringify(res.data);
          this.setState({
            JuniorData: res.data,
            selectImportador: true,
            modal: false,
          });
        } else {
          this.toggleAlert();

          setTimeout(() => {
            this.toggleAlert();
          }, 2000);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  toggleAlert() {
    this.setState((prevState) => ({
      isOpenAlert: !prevState.isOpenAlert,
    }));
  }

  toggleModal() {
    if (sessionStorage.seniorLogin !== undefined && this.state.modal == false) {
      let data = JSON.parse(sessionStorage.getItem("seniorLogin"));
      this.setState({
        JuniorData: data[0],
        selectImportador: true,
        modal: false,
      });
    } else {
      this.setState({
        loginJunior: false,
      });

      this.setState((prevState) => ({
        modal: !prevState.modal,
      }));
    }
  }

  render() {

    var key = this.props.match.params.id;
    return (
      <Fragment>
        <Title>
          <title>
            {window.location.pathname.replace("/", "") +
              " | " +
              "PIC  - Calculadora de Fletes"}
          </title>
        </Title>

        {Number(this.props.match.params.id) !== 22 &&
          Number(this.props.match.params.id) !== 21 &&
          Number(this.props.match.params.id) !== 16 &&
          Number(this.props.match.params.id) !== 23 &&
          this.state.campañas.length > 0 && (
            <ProductosDetallesCampaña
              id={key}
              toggleProducto={this.state.toggleProducto}
              rubros={this.state.rubros}
              campañas={this.state.campañas[0]}
              selectImportador={this.state.selectImportador}
              toggleModal={this.toggleModal}
              JuniorData={this.state.JuniorData}
            />
          )}

        {Number(this.props.match.params.id) !== 22 &&
          Number(this.props.match.params.id) !== 21 &&
          Number(this.props.match.params.id) !== 16 &&
          Number(this.props.match.params.id) !== 23 &&
          this.state.campañas.length == 0 && (
            <ProductosDetallesFirebase
              toggleProducto={this.state.toggleProducto}
              keyCampaña={key}
              ClienteStock={this.state.ClienteStock[0]}
              producto={this.state.toggleProductDetailsdata[0]}
            />
          )}

        {(Number(this.props.match.params.id) === 22 ||
          Number(this.props.match.params.id) === 21 ||
          Number(this.props.match.params.id) === 23 ||
          Number(this.props.match.params.id) === 16) &&
            this.state.campañas.length > 0 && (
              <ProductosDetallesConcentradores
                campañas={this.state.campañas[0]}
                keyCampaña={Number(key)}
              />
            )}

        <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
          {this.state.campañas.length > 0 &&
          this.state.campañas[0].cerrado === 0 ? (
            <ModalBody>
              {this.state.loginJunior ? (
                <Row>
                  <Col
                    xs="12"
                    md="12"
                    sm="12"
                    className="text-center arial mx-auto pb-5"
                    style={{ fontSize: "1rem" }}
                  >
                    Iniciar Sesión
                  </Col>
                  <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                    <Form onSubmit={this.handleSubtmitJunior}>
                      <FormGroup row>
                        <Label className="arial" for="dni" sm={3}>
                          Usuario o DNI
                        </Label>
                        <Col sm={9}>
                          <Input
                            type="text"
                            name="dni"
                            id="dni"
                            placeholder="Ingrese Dni"
                          />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Label className="arial" for="password" sm={3}>
                          Contraseña
                        </Label>
                        <Col sm={9}>
                          <Input
                            type="password"
                            name="password"
                            id="examplePassword"
                            placeholder="Ingrese password"
                          />
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col
                          xs={12}
                          md={12}
                          sm={12}
                          className=" font2 p-1 mx-auto"
                        >
                          <Alert
                            color="danger"
                            isOpen={this.state.isOpenAlert}
                            toggle={this.toggleAlert}
                          >
                            ¡No se encuentra registro del usuario, Verifique
                            Usuario o Contraseña!
                          </Alert>
                        </Col>
                      </FormGroup>
                      <FormGroup className="p-0" check row>
                        <Col className="text-center" sm={12}>
                          <Button
                            type="submit"
                            className="btn-style-blue"
                            color="secondary"
                          >
                            Iniciar Sesión
                          </Button>
                        </Col>
                      </FormGroup>{" "}
                    </Form>
                  </Col>
                </Row>
              ) : (
                <Row>
                  <Col
                    xs="12"
                    md="12"
                    sm="12"
                    className="text-center arial mx-auto pb-5"
                    style={{ fontSize: "1rem" }}
                  >
                    Seleccione una Opción:
                  </Col>
                  <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                    <FormGroup>
                      <div
                        onClick={() => {
                          this.setState({
                            loginJunior: true,
                          });
                        }}
                        className="btn-style-ConcentadoresPdf text-white arial  mb-2 p-3 bg-blue-2"
                      >
                        <span>Ya Tengo una Cuenta</span>
                        <i
                          style={{
                            backgroundColor: "white",
                            borderRadius: "50%",
                            fontSize: "1.2rem",
                            padding: ".2rem",
                            fontWeight: "bold",
                          }}
                          className="ml-2 float-right color-orange simple-icon-question"
                        />
                      </div>
                      <div
                        onClick={() => {
                          this.setState({
                            selectImportador: true,
                            modal: false,
                          });
                        }}
                        className="btn-style-ConcentadoresPdf text-white arial  p-3 bg-blue-2"
                      >
                        <span>No Tengo Cuenta</span>
                        <i
                          style={{
                            backgroundColor: "white",
                            borderRadius: "50%",
                            fontSize: "1.2rem",
                            padding: ".2rem",
                            fontWeight: "bold",
                          }}
                          className="ml-2 float-right color-orange simple-icon-question"
                        />
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
              )}
            </ModalBody>
          ) : (
            <ModalBody>
              <Row>
                <div className="alert alert-danger text-center mx-auto mb-3">
                  <h1>
                    {" "}
                    <i className="simple-icon-info mr-2" />
                    Esta Campaña Ha sido Cerrada
                  </h1>
                </div>
                <div className="text-center mx-auto mb-2">
                  <h2> Unete a nuestras Proxímas Importaciones</h2>
                </div>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className="text-center font mx-auto pb-5"
                >
                  <NavLink to="/Importadores" style={{ padding: "0px" }}>
                    <Button className="btn-style-quest" color="secondary">
                      Ir a Proximas campañas
                    </Button>
                  </NavLink>
                </Col>
              </Row>
            </ModalBody>
          )}
          <ModalFooter style={{ alignSelf: "center" }}>
            <Button
              className="btn-style"
              color="secondary"
              onClick={this.toggleModal}
            >
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
      </Fragment>
    );
  }
}

export default withRouter(ProductoDetalle);

import React, { useEffect, useRef, useCallback, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AddOutlined from "@material-ui/icons/AddOutlined";
import IconButton from "@material-ui/core/IconButton";
import StarRateIcon from "@material-ui/icons/StarRate";
import { useArticles } from "Hooks/useAbonos";
import useNearScreen from "Hooks/useNearScreen";
import debounce from "just-debounce-it";
import LoadingSpiner from "Util/loading";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import InfoIcon from "@material-ui/icons/Info";
import ListSubheader from "@material-ui/core/ListSubheader";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Spinner from "Components/spinner";
import Typography from "@material-ui/core/Typography";
import { yellow } from "@material-ui/core/colors";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { isMovil } from "Util/Utils";
const ModalDetalleArticulo = React.lazy(() =>
  import("Util/Modals/modalDetalleArticulo")
);

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
    paddingBottom: "10px",
  },
  media: {
    height: "10rem",
    position: "relative",
    display: "inline-block",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      height: "6rem",
    },
  },
  priceBar: {
    height: "3rem",
    position: "relative",
    display: "inline-block",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      height: "3rem",
    },
  },
  GridListTile: {
    textAlign: "center",
    display: "grid",

    "& img": {
      width: "16rem",
      height: "10rem",

      [theme.breakpoints.down("sm")]: {
        width: "5rem",
        height: "5rem",
      },
    },
    borderTopLeftRadius: "1rem",
    borderTopRightRadius: "1rem",
    borderBottomLeftRadius: "1rem",
    borderBottomRightRadius: "1rem",
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  gridList: {
    width: "inherit",
    minHeight: "100vh",
    maxHeight: "100vh",
    overflowX: "hidden",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
    padding: "5px",
  },
  iconContainer: {
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  loadingContainer: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: "1rem",
    height: "3rem !important",
  },
  visor: {
    textAlign: "center",

    height: "3rem !important",
  },
  TopIconLeft: {
    textAlign: "left",
    zIndex: 1,
    fontWeight: "500",
    color: "#48494a",
    position: "absolute",
    left: 15,
    top: 10,
    fontSize: "1rem",
    [theme.breakpoints.down("sm")]: {
      left: 10,
      fontSize: ".7rem",
    },
  },
  TopIconRight: {
    textAlign: "right",
    zIndex: 1,
    color: "#48494a",
    position: "absolute",
    right: 15,
    top: 10,
    [theme.breakpoints.down("sm")]: {
        right: 10,
    },
  },
  TopIconRightTitle:{
    fontWeight: "bold",
    color: "#48494a",
    fontSize: "1.5rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: ".8rem",


    },

  },
  TopIconRightSubtitle:{
    fontWeight: "400",
    color: "#48494a",

    fontSize: ".8rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: ".6rem",
 
 
    },

  },
  TopIconPages: {
    position: "absolute",
    top: 0,
    right: 0,
  },
  titleBar: {
    backgroundColor: "#59bae8",
    color: "white",
    borderTopLeftRadius: "1rem",
    borderTopRightRadius: "1rem",
    height: "2rem",
    fontWeight: "bold",
  },
  titleBarNone: {
    color: "white",
    borderTopLeftRadius: "1rem",
    borderTopRightRadius: "1rem",
    height: "2rem",
    fontWeight: "bold",
  },
  descriptionBar: {
    position: "relative",
    display: "inline-block",
    textAlign: "center",
    background: "rgb(140 140 140)",

    color: "white",
    borderBottomLeftRadius: "1rem",
    borderBottomRightRadius: "1rem",
    height: "4rem",
  },
  descriptionBartitle: {
    fontWeight: "bold",
    fontSize: "1rem",

    [theme.breakpoints.down("sm")]: {
      fontWeight: "bold",
      fontSize: ".8rem",
    },
  },
  descriptionBarSubtitle: {
    fontWeight: "600",
    fontSize: ".8rem",

    [theme.breakpoints.down("sm")]: {
      fontWeight: "bold",
      fontSize: ".6rem",
    },
  },
  descriptionBarIcon: {
    zIndex: 1,
    position: "absolute",
    color: "#000000",
    right: "5px",
    bottom: "5px",
    backgroundColor: " #50efc2",
    borderRadius: " 50%",
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      fontSize: ".7rem",
    },
  },
}));

export default function Album({
  ArticuloSelect,
  catalago,
  status,
  JuniorData,
  toggleModal,
  fullscreen,
}) {
  const classes = useStyles();

  const [articuloSelected, setArticuloSelected] = useState([]);
  const [modalDetalleOpen, setModalDetalleOpen] = useState(false);
  const [productoPage, setProductoPage] = useState(0);

  const { loading, loadingNextPage, articulo, setPage, limite } = useArticles({
    catalogo: catalago.id,
  });

  const externalRef = useRef();
  const { isNearScreen } = useNearScreen({
    externalRef: loading ? null : externalRef,
    once: false,
  });

  const debounceNextPage = useCallback(
    debounce(() => setPage((prevPage) => prevPage + 1), 2000),
    []
  );

  const debounceNextPageModal = useCallback(
    debounce(() => setPage((prevPage) => prevPage + 1), 500),
    []
  );
  useEffect(function () {
    if (isNearScreen) debounceNextPage();
  });

  const modalDetalleArticuloOpen = (e) => {
    console.log(e);
    let index = articulo.findIndex(function (item) {
      return item.id == e.id;
    });
    setProductoPage(index);
    setArticuloSelected(e);
    setModalDetalleOpen(true);
  };

  const changeProducto = (tipo, id) => {
    let index = articulo.findIndex(function (item) {
      return item.id == id;
    });

    switch (tipo) {
      case "prev":
        if (productoPage > 0) {
          setProductoPage(index - 1);
          setArticuloSelected(articulo[index - 1]);
        }
        break;
      case "next":
        if (productoPage < limite && index < articulo.length - 1) {
          setProductoPage(index + 1);
          setArticuloSelected(articulo[index + 1]);
          if (index - articulo.length < 2) {
            debounceNextPageModal();
          }
        }
        break;

      default:
        break;
    }
  };

  return (
    <div className={classes.root}>
      {loading ? (
        <LoadingSpiner />
      ) : (
        <>
          <GridList
            cellHeight={300}
            cols={isMovil() ? 2 : status ? (fullscreen ? 3 : 2) : 3}
            spacing={10}
            cellHeight={"14rem"}
            className={classes.gridList}
          >
            <GridListTile
              key="Subheader"
              cols={4}
              style={{ height: "auto", width: "100%" }}
            >
              <ListSubheader className="arial" style={{ display: "flex" }}>
                <ShoppingCartIcon
                  className="mr-3"
                  style={{ borderRadius: "50%" }}
                />{" "}
                <Typography component="h6" variant="h6" color="secondary">
                  {`${limite} Articulos Disponibles`}
                </Typography>
              </ListSubheader>
            </GridListTile>
            {articulo.map((card) => (
              <div key={card.id}>
                <div className={classes.GridListTile}>
                  <div className={classes.titleBar}>
                    {card.top === 1 && (
                      <Typography
                        component="div"
                        variant="inherit"
                        onClick={() => modalDetalleArticuloOpen(card)}
                      >
                        <StarRateIcon
                          style={{ borderRadius: "50%", color: yellow[500] }}
                        />
                        TOP PRODUCT
                      </Typography>
                    )}
                  </div>
                  <div className={classes.priceBar}>
                    {card.escala !== null && (
                      <Typography
                        className={classes.TopIconLeft}
                        component="div"
                        variant="p"
                      >
                        TALLA: <br />
                        <span>{card.escala}</span>
                      </Typography>
                    )}
                    <div className={classes.TopIconRight}>
                      {" "}
                      {status && (
                        <Typography
                          component="div"
                          className={classes.TopIconRightTitle}
                          variant="p"
                          style={{
                            display: "grid",
                          }}
                        >
                          {JuniorData.length > 1
                            ? "$" + card.precio_venta
                            : "$" + card.precio_junior}
                        </Typography>
                      )}
                      {card.genero !== null && (
                        <Typography
                          component="div"
                          className={classes.TopIconRightSubtitle}
                          variant="p"
                          style={{
                            display: "grid",
                            paddingBottom: "5px",
                          }}
                        >
                          {card.genero}
                        </Typography>
                      )}
                    </div>
                  </div>

                  <div className={classes.media}>
                    <img
                      onClick={() => modalDetalleArticuloOpen(card)}
                      src={card.image}
                      alt={card.name}
                    />
                  </div>

                  <div className={classes.descriptionBar}>
                    <Typography
                      component="div"
                      variant="inherit"
                      className={classes.descriptionBartitle}
                      onClick={() => modalDetalleArticuloOpen(card)}
                    >
                      {card.name}{" "}
                    </Typography>

                    {status && (
                      <Typography
                        component="div"
                        variant="p"
                        className={classes.descriptionBarSubtitle}
                      >
                        {JuniorData.length > 1
                          ? "$" + card.precio_venta
                          : "$" + card.precio_junior}
                      </Typography>
                    )}
                    <Typography
                      className={classes.descriptionBarIcon}
                      component="div"
                      variant="p"
                    >
                      {status ? (
                        <IconButton
                        size='small'
                          color="inherit"
                          aria-label="addIcon"
                          onClick={() => ArticuloSelect(card)}
                        >
                          <AddShoppingCartIcon />
                        </IconButton>
                      ) : (
                        <IconButton
                        size='small'
                          color="inherit"
                          aria-label="addIcon"
                          onClick={() => modalDetalleArticuloOpen(card)}
                        >
                          <InfoIcon />
                        </IconButton>
                      )}
                    </Typography>
                  </div>
                </div>
              </div>
            ))}

            <div
              className={classes.visor}
              data-testid="visor"
              ref={externalRef}
            />
            {limite / articulo.length == 1 && (
              <div className={classes.loadingContainer}>
                <span className="arial">No hay más productos disponibles</span>
              </div>
            )}
          </GridList>
          {isNearScreen && limite / articulo.length !== 1 && (
            <div className={classes.loadingContainer}>
              Cargando... <Spinner />
            </div>
          )}
        </>
      )}
      {modalDetalleOpen && (
        <ModalDetalleArticulo
          JuniorData={JuniorData}
          status={status}
          open={modalDetalleOpen}
          toggle={setModalDetalleOpen}
          toggleModal={toggleModal}
          addArticulo={ArticuloSelect}
          articulo={articuloSelected}
          limite={limite}
          loadingNextPage={loadingNextPage}
          productoPage={productoPage}
          changeProducto={changeProducto}
        />
      )}
    </div>
  );
}

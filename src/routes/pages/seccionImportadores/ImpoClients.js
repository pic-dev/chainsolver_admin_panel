import React, { Fragment, Component } from "react";
import {
  Input,
  Form,
  Row,
  Col,
  Collapse,
  Button,
  FormGroup,
  Label,
  NavbarToggler,
  Alert
} from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import SideBar from "../sidebar/SideBar";
import { DataGrid } from "@material-ui/data-grid";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import {
  getCurrentTime,
  getDateWithFormat
} from "Util/Utils";

const ModalProductDetails = React.lazy(() =>
  import("Util/Modals/modalProductDetails")
);
var timer;
class ImpoAdmin extends Component {
  constructor() {
    super();
    this.state = {
      contacto: "",
      Pais: "",
      Provincia: "",
      telefono: "",
      clients: [],
      sidebarIsOpen: true,
      isOpenItem: false,
      isOpenAlert: false,
      isOpenAlertRemove: false,
      toggleProductDetails: false,
      toggleProductDetailsdata: [],
    };
    this.handleForm = this.handleForm.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
    this.toggleAlertRemove = this.toggleAlertRemove.bind(this);
    this.toggleProductDetails = this.toggleProductDetails.bind(this);
    this.toggleProductDetailsOpen = this.toggleProductDetailsOpen.bind(this);
  }

  componentDidMount() {
    if (screen.width <= 769) {
      this.setState((prevState) => ({
        sidebarIsOpen: !prevState.sidebarIsOpen,
      }));
    }

    var database = firebaseConf.database().ref("clients");

    database.on("child_added", (snapshot) => {
      var key = [{ id: snapshot.key }];

      var arreglo = key.concat(snapshot.val());
      var data = Object.assign(arreglo[1], arreglo[0]);
      this.setState({
        clients: this.state.clients.concat(data),
      });
      this.renderLoginButton();
    });

    database.on("child_removed", (snapshot) => {
      var eliminado = snapshot.key;

      let data = this.state.clients.filter(function (item) {
        if (eliminado !== item.key) {
          return item;
        }
      });
      this.setState({
        clients: data,
      });
      this.renderLoginButton();
    });
  }

  handleRemove(e) {
    firebaseConf
      .database()
      .ref("/clients/" + e)
      .remove();


      let data = this.state.clients.filter(function (item) {
        if (e !== item.key) {
          return item;
        }
      });

      this.setState({
        clients: data,
        toggleAlertRemove:true
      });

      timer = setTimeout(() => {
        this.setState({
         toggleAlertRemove:false
        });
      }, 3000);
  
  }

  toggleSidebar() {
    this.setState((prevState) => ({
      sidebarIsOpen: !prevState.sidebarIsOpen,
    }));
  }
  toggleItem() {
    this.setState((prevState) => ({
      isOpenItem: !prevState.isOpenItem,
      contacto: "",
      Pais: "",
      Provincia: "",
      telefono: "",
    }));


  }
  toggleAlert() {
  
      this.setState((prevState) => ({
        isOpenAlert: !prevState.isOpenAlert,
      }));
 
  }
  toggleAlertRemove() {
  
    this.setState((prevState) => ({
      isOpenAlertRemove: !prevState.isOpenAlertRemove,
    }));

}
  toggleProductDetails(e) {
    var key = e.target.value;
    let data = this.state.clients.filter(function (item) {
      if (key == item.key) {
        return item;
      }
    });
    if (data.length > 0) {
      this.setState({
        toggleProductDetailsdata: data,
        toggleProductDetails: true,
      });
    }
  }

  toggleProductDetailsOpen() {
    this.setState((prevState) => ({
      toggleProductDetails: !prevState.toggleProductDetails,
    }));
  }

  handleForm() {
    if (
      this.state.telefono.length > 0 &&
      this.state.Provincia.length > 0 &&
      this.state.Pais.length > 0 &&
      this.state.contacto.length > 0
    ) {
      const record = {
        telefono: this.state.telefono,
        Provincia: this.state.Provincia,
        Pais: this.state.Pais,
        contacto: this.state.contacto,
        activo:true,
        Fecha: getDateWithFormat() + " " + getCurrentTime(),
      };
      const dbRef = firebaseConf.database().ref("clients");
      const newProduct = dbRef.push();
      newProduct.set(record);

      this.setState({
        contacto: "",
        Pais: "",
        Provincia: "",
        telefono: "",
        isOpenItem: false,
        isOpenAlert: true,
      });

      timer = setTimeout(() => {
        this.setState({
          isOpenAlert: false,
        });
      }, 3000);
    }
  }

  renderLoginButton() {
    if (true) {
      return (
        <div>
          <Row>
            {" "}
            <Button
              onClick={this.toggleItem}
              style={{ display: !this.state.isOpenItem ? "" : "none" }}
              className="btn-style-Green btn btn-success btn-sm"
            >
              Agregar Cliente
            </Button>
            <Collapse
              style={{ display: this.state.isOpenItem ? "contents" : "" }}
              isOpen={this.state.isOpenItem}
            >
              <Col xs="12" md="8" sm="12" className="mx-auto pb-1">
                <Row
                  className="mx-auto m-2 p-2"
                  style={{
                    borderRadius: "1em",
                    backgroundColor: "#d7d7d7",
                    border: "1px solid rgba(0, 0, 0, 0.18)",
                    fontWeight: "bold",
                  }}
                >
                  {" "}
                  <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
                    {" "}
                    <Form>
                      <FormGroup>
                        <Label className="arial mx-auto pb-1" for="contacto">
                          Razon social o contacto
                        </Label>
                        <Input
                          name="contacto"
                          type="text"
                          placeholder="Razon social o contacto"
                          value={this.state.contacto}
                          onChange={(e) => {
                            this.setState({
                              contacto: e.target.value,
                            });
                          }}
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label className="arial mx-auto pb-1" for="Pais">
                          Pais
                        </Label>

                        <Input
                          type="select"
                          name="Pais"
                          onChange={(e) => {
                            this.setState({
                              Pais: e.target.value,
                            });
                          }}
                        ><option defaultValue value="">
                        Seleccione una opción
                      </option>
                          <option   value="Peru">
                            Peru
                          </option>
                          <option  value="Panama">
                            Panama
                          </option>
                          <option value="Venezuela">
                            Venezuela
                          </option>
                        </Input>
                      </FormGroup>
                      <FormGroup>
                        <Label className="arial mx-auto pb-1" for="Provincia">
                          Provincia / Estado
                        </Label>
                        <Input
                          name="Provincia"
                          type="text"
                          placeholder="Pais"
                          value={this.state.Provincia}
                          onChange={(e) => {
                            this.setState({
                              Provincia: e.target.value,
                            });
                          }}
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label className="arial mx-auto pb-1" for="telefono">
                          Telefono
                        </Label>
                        <Input
                          name="telefono"
                          type="text"
                          placeholder="telefono"
                          value={this.state.telefono}
                          onChange={(e) => {
                            this.setState({
                              telefono: e.target.value,
                            });
                          }}
                        />
                      </FormGroup>

                      <FormGroup>
                        <Button
                          className="btn-style-Green btn btn-success btn-sm"
                          onClick={this.handleForm}
                        >
                          Agregar cliente
                        </Button>{" "}
                        <Button
                          className="btn-style-Red btn btn-success btn-sm"
                          onClick={this.toggleItem}
                        >
                          Cancelar
                        </Button>{" "}
                      </FormGroup>
                    </Form>
                  </Col>
                </Row>
              </Col>
            </Collapse>
            <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
              <Row
                className="mx-auto m-2 p-2"
                style={{
                  borderTop: "1px solid rgba(0, 0, 0, 0.18)",
                }}
              >
                <Col xs="12" md="12" sm="12" className="font2 mx-auto m-2">
                  CLIENTES ACTUALES
                </Col>
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className="font2 mx-auto m-2 text-center"
                >
             
                </Col>{" "}
              </Row>
            </Col>
          </Row>
        </div>
      );
    }
  }

  render() {

    const columns = [
      
     
      {
        field: "contacto",
        headerName: "nombre",
        description: "name",
        sortable: true,
        width: 200,
     
      },
      { field: "Provincia", sortable: true, headerName: "provincia", width: 150 },
      {
        field: "Pais",
        headerName: "Pais",
        width: 150,
        sortable: true,
      },
      {
        field: "telefono",
        headerName: "telefono",
        width: 150,
        sortable: true,
      },

    {
      field: "id",
      headerName: "Borrar",
      sortable: false,
      width: 150,
      renderCell: (params) => {
        return (
          <IconButton
            aria-label="open"
            size="small"
            onClick={() => this.handleRemove(params.value)}
          >
            <DeleteIcon  />
          </IconButton>
        );
      },
    },
      
    ];
    return (
      <Fragment>
        <Row className="p-0">
          <NavbarToggler
            className="float-right display-sm"
            style={{
              backgroundColor: "#1370b9",
              border: "solid white 1px",
              top: "70px",
              left: "0",
              position: "fixed",
            }}
            onClick={this.toggleSidebar}
          />
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "3" : "1"}
            md={this.state.sidebarIsOpen ? "4" : "1"}
            sm="12"
            className="p-0 mx-auto"
          >
            <SideBar
              toggle={this.toggleSidebar}
              isOpen={this.state.sidebarIsOpen}
            />
          </Col>
          <Col
            xs="12"
            lg={this.state.sidebarIsOpen ? "9" : "11"}
            md={this.state.sidebarIsOpen ? "8" : "11"}
            sm="12"
            className="p-1 mx-auto"
          >
            <Row className="p-1">
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <h2>IMPORTACIONES GRUPALES - CLIENTES</h2>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlert}
                  toggle={this.toggleAlert}
                >
                  ¡Cliente Agregado Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className=" font2 p-1 mx-auto">
                <Alert
                  color="info"
                  isOpen={this.state.isOpenAlertRemove}
                  toggle={this.toggleAlertRemove}
                >
                  ¡Cliente Removido Exitosamente!
                </Alert>
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.renderLoginButton()}
              </Col>
              <Col xs="12" md="12" sm="12" className="p-1 mx-auto">
                {this.state.clients.length > 1 ? 
              <DataGrid autoHeight rows={this.state.clients} columns={columns} pageSize={5} />
              : <div className="alert alert-info text-center">No hay clientes agregados actualmente</div>}
              </Col>
            </Row>
          </Col>
        </Row>

        {this.state.toggleProductDetailsdata.length > 0 &&
          <React.Suspense fallback={<div className="loading"></div>}>
            <ModalProductDetails
              active={this.state.toggleProductDetails}
              item={this.state.toggleProductDetailsdata}
              toggle={this.toggleProductDetailsOpen}
            />
          </React.Suspense>
       }
      </Fragment>
    );
  }
}

export default ImpoAdmin;

import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import AddOutlined from "@material-ui/icons/AddOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#1470b9",
    color: "white",
  },
}));

export default function Junior(props) {
  const classes = useStyles();

  const handleSubmit = (e) => {
    e.preventDefault();
    const {
      Pagina,
      Producto,
      CodigoProducto,
      CantidadProducto,
      precio,
      observacion,
    } = e.target.elements;
    var newItem = {
      Catalogo: props.catalogo.id,
      Pagina: Pagina ? Pagina.value : "",
      Producto: Producto.value,
      CodigoProducto:
        CodigoProducto && CodigoProducto.value ? CodigoProducto.value : "",
      cantidad: Number(CantidadProducto.value),
      precio: precio ? Number(precio.value) : 0,
      subTotal: precio
        ? Number(precio.value) * Number(CantidadProducto.value)
        : 0,
      key: Date.now(),
      precioCompra:Object.entries(props.articulo).length > 0  ? props.articulo.precio_compra: 0,
      observacion: observacion.value,
    };
    props.setArticuloSelected([]);
    props.addItem(newItem);
    if (Pagina) {
      document.getElementById("Pagina").value = "";
    }
    if (CodigoProducto) {
      document.getElementById("CodigoProducto").value = "";
    }

    document.getElementById("Producto").value = "";
    document.getElementById("CantidadProducto").value = "";
    document.getElementById("observacion").value = "";
    if (precio) {
      document.getElementById("precio").value = "";
    }
  };

  return (

    <div className={classes.root}>
      <CssBaseline />

      <Container maxWidth="lg">
        <Typography component="h6" variant="h6">
          <AddOutlined /> Agregar Productos
        </Typography>
        {props.tipo === "catalogos" ? (
          <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={2}>
                <TextField
                  autoComplete="Pagina"
                  name="Pagina"
                  required
                  fullWidth
                  id="Pagina"
                  label="Pagina"
                  autoFocus
                />
              </Grid>
              <Grid item xs={4} sm={3}>
                <TextField
                  autoComplete="Producto"
                  name="Producto"
                  required
                  fullWidth
                  id="Producto"
                  label="Producto"
                />
              </Grid>
              <Grid item xs={4} sm={3}>
                <TextField
                  fullWidth
                  id="CodigoProducto"
                  label="Codigo"
                  name="CodigoProducto"
                  autoComplete="Codigo"
                />
              </Grid>
              <Grid item xs={4} sm={2}>
                <TextField
                  required
                  fullWidth
                  id="CantidadProducto"
                  label="Qty"
                  name="CantidadProducto"
                  autoComplete="Cantidad"
                />
              </Grid>
              {props.catalogo.statusCp == "Ok" && props.JuniorData.length > 0 && (
                <Grid item xs={4} sm={2}>
                  <TextField
                    fullWidth
                    id="precio"
                    label="precio"
                    name="precio"
                    autoComplete="precio"
                  />
                </Grid>
              )}
              <Grid item xs={12} sm={12}>
                <TextField
                  id="observacion"
                  name="observacion"
                  label="Observación"
                  multiline
                  fullWidth
                  rowsMax={4}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
            >
              Agregar
            </Button>
          </form>
        ) : (
          <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={4}>
                <TextField
                  autoComplete="Producto"
                  name="Producto"
                  value={props.articulo ? props.articulo.name : ""}
                  fullWidth
                  id="Producto"
                  helperText="Producto"
                  disabled
                />
              </Grid>
              <Grid item xs={4} sm={4}>
                <TextField
                  fullWidth
                  id="CodigoProducto"
                  helperText="Codigo"
                  value={props.articulo ? props.articulo.codigo : ""}
                  fullWidth
                  name="CodigoProducto"
                  autoComplete="Codigo"
                  disabled
                />
              </Grid>
              <Grid item xs={4} sm={4}>
                <TextField
                  required
                  fullWidth
                  id="CantidadProducto"
                  name="CantidadProducto"
                  autoComplete="Cantidad"
                  helperText="Cantidad"
                  disabled={props.articulo.length == 0 ? true : false}
                />
              </Grid>

              <Grid item xs={4} sm={4}>
                <TextField
                  fullWidth
                  id="precio"
                  name="precio"
                  value={
                    props.articulo
                      ? props.JuniorData.length > 1
                        ? props.articulo.precio_venta
                        : props.articulo.precio_junior
                      : ""
                  }
                  helperText="Precio"
                  disabled
                />
              </Grid>

              <Grid item xs={12} sm={12}>
                <TextField
                  id="observacion"
                  name="observacion"
                  multiline
                  fullWidth
                  rowsMax={4}
                  helperText="Observación"
                  disabled={props.articulo.length == 0 ? true : false}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
              disabled={props.articulo.length == 0 ? true : false}
            >
              Agregar
            </Button>
          </form>
        )}
      </Container>
    </div>
  );
}

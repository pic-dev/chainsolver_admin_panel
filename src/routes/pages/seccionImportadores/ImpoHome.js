import React, { Component, Fragment } from "react";
import { Row, Col, Button, Table } from "reactstrap";
import { firebaseConf } from "Util/Firebase";
import Video from "Util/video";
import Transmision from "Util/transmisiones";
import Testimonios from "Util/testimonios";
import Carrusel from "Util/carrusel";
/*import CarruselCampaña from "Util/carruselCampañas";
import Publicidad from "../../util/publicidad";*/
import { NavLink } from "react-router-dom";
import {
  faWarehouse,
  faShip,
  faCheckCircle,
  faStore,
  faCommentAlt,
  faShoppingCart,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import Floatwhatsapp from "Util/botonFlotante";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { PageView } from "Util/tracking";
import DefaultFooter from "Util/DefaultFooter";
import Whatsapp from "Util/Whatsapp";
import RouteBack from "Util/RouteBack";
import axios from "axios";

import Spinner from "Components/spinner";
import { Title } from "Util/helmet";
import { imgProducto } from "Util/Utils";
import { isMovil, fechaFormateada } from "Util/Utils";
import TouchAppIcon from "@material-ui/icons/TouchApp";
const ModalProductDetails = React.lazy(() =>
  import("Util/Modals/modalProductDetailsList")
);
class SeccionImportadores extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      product2: [],
      product3: [],
      ClienteStock: [],
      campañasDisponibles: [],
      typeProduct: [],
      toggleProductDetails: false,
      toggleProductDetailsdata: [],
    };
    this.toggleProductDetails = this.toggleProductDetails.bind(this);
    this.toggleProductDetailsOpen = this.toggleProductDetailsOpen.bind(this);
  }

  componentDidMount() {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }

    this.getData();
  }

  async getData() {
    await axios
      .get("https://point.qreport.site/campana")
      .then((res) => {
        if (res.data) {
          this.setState({
            campañasDisponibles: res.data,
          });

          this.renderLoginButton();
        }
      })
      .catch((error) => {
        console.log(error);
      });

    var database = firebaseConf.database().ref("Catalogos");
    var databasestock = firebaseConf
      .database()
      .ref("CatalagosStock")
      .orderByChild("orden");

    database.on("child_added", (snapshot) => {
      var product = snapshot.val();
      var productkey = snapshot.key;

      databasestock.on("child_added", (snapshot) => {
        if (productkey == snapshot.val().KeyProducto) {
          var key = [{ key: productkey }];

          var arreglo = key.concat(snapshot.val());
          var data = Object.assign(arreglo[1], arreglo[0]);

          this.setState({
            product: this.state.product.concat(Object.assign(product, data)),
          });
        }
      });

      this.renderLoginButton();
    });

    var databasestock2 = firebaseConf
      .database()
      .ref("stock")
      .orderByChild("orden");

    firebaseConf
      .database()
      .ref("product")
      .on("child_added", (snapshot) => {
        var product2 = snapshot.val();
        var productkey2 = snapshot.key;

        databasestock2.on("child_added", (snapshot) => {
          if (productkey2 == snapshot.val().KeyProducto) {
            var key2 = [{ key: productkey2 }];

            var arreglo2 = key2.concat(snapshot.val());
            var data2 = Object.assign(arreglo2[1], arreglo2[0]);

            this.setState({
              product2: this.state.product2.concat(
                Object.assign(product2, data2)
              ),
            });
          }
        });

        this.renderLoginButton();
      });
  }

  renderLoginButton() {
    return (
      <div className="mx-auto ">
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
            <Row className="mx-auto pt-3 pb-3 CarrouselContainer">
              <Col
                xs="12"
                md="12"
                sm="12"
                className="fontImpoList  my-auto mx-auto m-3"
              >
                {" "}
                <h2 className="fontImpoListLive">
                  <FontAwesomeIcon
                    icon={faCommentAlt}
                    style={{ color: "#33c58c" }}
                    className="mr-2"
                  />
                  <i className="fab fa-whatsapp"></i>
                  ÚNETE AL GRUPO DE WHATSAPP
                </h2>
                <div className="labelContainer"></div>
              </Col>
              <Col
                xs="12"
                md="12"
                sm="12"
                style={{
                  alignSelf: "flex-end",
                  fontWeight: "800",
                  fontSize: "1rem",
                }}
                className="text-center  mx-auto pb-5 pt-3"
              >
                Selecciona tú país de residencia:
              </Col>
              <Col
                xs="4"
                md="4"
                sm="4"
                className="whatsIconCenter arial mx-auto text-center"
              >
                <a
                  href="https://chat.whatsapp.com/BjCygW9T1yDKlwq0MLygac"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  {" "}
                  <div className="btn-bluelight-Groups">
                    <Button className="btnPanama"></Button>{" "}
                    <br className="display-sm" />
                    Panamá
                  </div>
                </a>
              </Col>

              <Col
                xs="4"
                md="4"
                sm="4"
                className="whatsIconCenter arial mx-auto text-center"
              >
                <a
                  href="https://chat.whatsapp.com/GQunqHtcmPR4X5unyVQ9qc"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="btn-bluelight-Groups">
                    <Button className="btnPeru"></Button>{" "}
                    <br className="display-sm" /> Perú
                  </div>
                </a>
              </Col>

              <Col
                xs="4"
                md="4"
                sm="4"
                className="whatsIconCenter arial mx-auto text-center"
              >
                <a
                  href="https://chat.whatsapp.com/F0A0dOrWoYEFDCLjN06m9l"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="btn-bluelight-Groups">
                    <Button className="btnVenezuela"></Button>{" "}
                    <br className="display-sm" /> Venezuela
                  </div>
                </a>
              </Col>
              <Col
                xs="12"
                md="12"
                sm="12"
                style={{
                  alignSelf: "flex-end",
                  fontWeight: "800",
                  fontSize: "1rem",
                }}
                className="text-center  mx-auto pb-3 pt-3"
              >
                Taller gratuito de Importación:
              </Col>

              <Col
                xs="12"
                md="6"
                sm="12"
                className="whatsIconCenter arial mx-auto text-center"
              >
                <a
                  href="https://chat.whatsapp.com/KmERnFFfNn29aTDDGBN3HR"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <div className="btn-bluelight-Groups2">
                    <Button className="btnTaller mr-2"></Button>{" "}
                    <br className="display-sm ml-2" /> Unete aquí
                  </div>
                </a>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row>
          <Col xs="12" md="12" sm="12" className="backgroudCampañas mx-auto pb-1">
            <Row className="mx-auto pt-3 CarrouselContainerCampaign">
              <Col
                xs="12"
                md="12"
                sm="12"
                className="pb-2 pt-2  mx-auto 
                "
              >
                {" "}
                <img
                  key="imgLogo"
                  className="imgLogo"
                  src="/assets/img/logoNew.svg"
                  alt="logo"
                />
              </Col>

              <Col
                xs="12"
                md="12"
                sm="12"
                className="title pb-2 pt-2  mx-auto 
                "
              >
                IMPORTACIONES EN PROCESO
              </Col>
              <Col
                xs="12"
                md="12"
                sm="12"
                className="display-large datesDiv pb-2 pt-2  mx-auto 
                "
              >
                Seleccione una campaña <TouchAppIcon />
              </Col>
              <Col
                style={{ padding: 0 }}
                xs="12"
                md="12"
                sm="12"
                className="mx-auto "
              >
                {isMovil() ? (
                  this.state.campañasDisponibles.map((item, index) => (
                    <div className="shadowCard">
                      <NavLink
                        to={`/SeccionImportadores/${item.id}?Name=${item.description}`}
                      >
                        <div className="tituloProducto">
                          {/*<span className="numberStyle">{index + 1}</span>*/}
                          {item.name}
                        </div>
                
                        <div className="p-1 mb-2 btn-style-open">
                          <img
                            key={index + "img"}
                            src="/assets/img/arrowCampaña.gif"
                            className="destinoImg"
                            alt="arrowCampaña"
                          />

                          <div>Más información aqui</div>
                          <img
                            key={index + "img"}
                            src="/assets/img/arrowCampaña.gif"
                            style={{ transform: "rotate(180deg)" }}
                            className="destinoImg"
                            alt="arrowCampaña"
                          />
                        </div>{" "}
                      </NavLink>
                      {item.market == 1 && (
                        <div className="p-1 MarketMsg">
                          PREPÁRATE PARA LA TERCERA OLA
                        </div>
                      )}
                      <div style={{ backgroundColor: "#f5f3f3" }}>
                        <NavLink
                          to={`/SeccionImportadores/${item.id}?Name=${item.description}`}
                        >
                          {" "}
                          <img
                            key={index + "img"}
                            src={item.portada}
                            className="imgProducto"
                            alt="logo"
                          />{" "}
                        </NavLink>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          padding: "0 1rem",
                        }}
                      >
                        <div>
                          <img
                            key={index + "img"}
                            src={imgProducto(item.pais)}
                            className="destinoImg"
                            alt="via"
                          />
                        </div>

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            padding: "0 1rem",
                          }}
                        >
                          <img
                            key={index + "origen"}
                            src={
                              item.via !== null
                                ? item.via == 0
                                  ? "/assets/img/barco.png"
                                  : "/assets/img/avion.png"
                                : "/assets/img/profile-pic-l.png"
                            }
                            style={{ height: "2rem" }}
                            className="destinoImg"
                            alt="origen"
                          />
                          <div className="viaTitle">
                            {item.via !== null
                              ? item.via == 0
                                ? "MARÍTIMO"
                                : "AÉREO"
                              : "IMPORTA YA"}
                          </div>
                        </div>
                      </div>
                      <div className="datesDiv">
                        {" "}
                        <div>
                          {item.cierre_corto
                            ? "CIERRE: " + fechaFormateada(item.cierre_corto)
                            : "IMPORTA YA"}
                        </div>
                        <div>
                          {item.llegada_corto
                            ? "LLEGADA: " + fechaFormateada(item.llegada_corto)
                            : "IMPORTA YA"}
                        </div>
                      </div>
                    </div>
                  ))
                ) : (
                  <Table hover borderless>
                    <thead>
                      <tr className="tableHead pb-3 pt-3 text-center">
                        <th>PRODUCTO</th>
                        <th>DESDE</th>
                        <th>VÍA</th>
                        <th>CIERRE</th>
                        <th>LEGADA</th>
                      </tr>
                    </thead>

                    <tbody>
                      {this.state.campañasDisponibles.map((item, index) => {
                        return (
                          <tr
                            style={{ borderBottom: "1px solid #d7d7d7" }}
                            key={item.id}
                          >
                            <>
                              <NavLink
                                to={`/SeccionImportadores/${item.id}?Name=${item.description}`}
                              >
                                <td className="tdProducto">
                                  <>
                                    <div>
                                      <img
                                        key={index + "img"}
                                        src={item.portada}
                                        className="imgProducto"
                                        alt="logo"
                                      />{" "}
                                    </div>
                                    <div className="text-center">
                                      <div className="tituloProducto">
                                        {item.name}
                                      </div>
                                      {item.market == 1 && (
                                        <div className="p-1 MarketMsg">
                                          {" "}
                                          PREPÁRATE PARA LA TERCERA OLA
                                        </div>
                                      )}
                                    </div>
                                  </>
                                </td>{" "}
                              </NavLink>
                            </>
                            <td style={{ verticalAlign: "middle" }}>
                              <>
                                <div>
                                  <img
                                    key={index + "img"}
                                    src={imgProducto(item.pais)}
                                    className="destinoImg"
                                    alt="via"
                                  />
                                </div>
                              </>
                            </td>
                            <td
                              className="fechaStyle"
                              style={{ color: " #843646" }}
                            >
                              <>
                                <div>
                                  <img
                                    key={index + "origen"}
                                    src={
                                      item.via !== null
                                        ? item.via == 0
                                          ? "/assets/img/barco.png"
                                          : "/assets/img/avion.png"
                                        : "/assets/img/profile-pic-l.png"
                                    }
                                    className="destinoImg"
                                    alt="origen"
                                  />
                                  <div>
                                    {item.via !== null
                                      ? item.via == 0
                                        ? "MARÍTIMO"
                                        : "AÉREO"
                                      : "IMPORTA YA"}
                                  </div>
                                </div>
                              </>
                            </td>
                            <td
                              className="p-1 fechaStyle"
                              style={{
                                color: "#5a5a5a",
                              }}
                            >
                              {" "}
                              <>
                                <div>
                                  {item.cierre_corto
                                    ? fechaFormateada(item.cierre_corto)
                                    : "IMPORTA YA"}
                                </div>
                              </>
                            </td>
                            <td
                              className="p-1 fechaStyle"
                              style={{
                                color: "#5a5a5a",
                              }}
                            >
                              {" "}
                              <>
                                <div>
                                  {item.llegada_corto
                                    ? fechaFormateada(item.llegada_corto)
                                    : "IMPORTA YA"}
                                </div>
                              </>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                )}
                {/*(
                  <CarruselCampaña
                    tipo={"Proximas"}
                    toggleProductDetails={this.toggleProductDetails}
                    product={this.state.campañasDisponibles}
                  />
                )*/}
              </Col>
            </Row>
          </Col>
        </Row>
      
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
            <Row className="mx-auto pt-2 CarrouselContainer">
              <Col
                xs="12"
                md="12"
                sm="12"
                className="fontImpoList text-center mx-auto m-3"
              >
                {" "}
                <NavLink to="/ImportadoresVip">
                  <h2 className="fontImpoListLive">
                    {" "}
                    <FontAwesomeIcon
                      icon={faWarehouse}
                      style={{ color: "#fc5a2c" }}
                      className="mr-2"
                    />{" "}
                    ¿DESEAS IMPORTAR?
                  </h2>
                </NavLink>
                <div className="labelContainer"></div>
              </Col>

              <Col
                xs="12"
                md="12"
                sm="12"
                className="my-auto mx-auto text-center m-3"
              >
                <NavLink to="/ImportadoresVip">
                  <img
                    className="img importarImagen"
                    src="/assets/img/importar.png"
                    alt="importar"
                  />
                </NavLink>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto pb-1">
            <Row className="mx-auto pt-2 CarrouselContainer">
              <Col xs="12" md="12" sm="12" className="fontImpoList mx-auto m-3">
                <h2 className="fontImpoListLive">
                  {" "}
                  <FontAwesomeIcon
                    icon={faWarehouse}
                    style={{ color: "#fc5a2c" }}
                    className="mr-2"
                  />{" "}
                  MERCANCÍA EN STOCK
                </h2>
                <div className="labelContainer"></div>
              </Col>
              {this.state.product2.length > 0 ? (
                <Carrusel
                  tipo={"Stock"}
                  toggleProductDetails={this.toggleProductDetails}
                  product={this.state.product2}
                />
              ) : (
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" mx-auto text-center m-3"
                >
                  Cargando... <span className="loadingBlock"></span>{" "}
                </Col>
              )}
            </Row>
          </Col>
        </Row>

        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto m-2 pb-3">
            <Row className="mx-auto pt-2 CarrouselContainer">
              <Col xs="12" md="12" sm="12" className="fontImpoList mx-auto m-3">
                <h2 className="fontImpoListLive">
                  {" "}
                  <FontAwesomeIcon
                    icon={faShip}
                    style={{ color: "#3fa6f5" }}
                    className="mr-2"
                  />{" "}
                  MERCANCÍA EN TRANSITO
                </h2>
                <div className="labelContainer"></div>
              </Col>
              {this.state.product2.length > 0 ? (
                <Carrusel
                  tipo={"transito"}
                  toggleProductDetails={this.toggleProductDetails}
                  product={this.state.product2}
                />
              ) : (
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className=" mx-auto text-center m-3"
                >
                  <span>No hay mercancia en transito actualmente</span>{" "}
                </Col>
              )}
            </Row>
          </Col>
        </Row>
        <Row>
          <Col xs="12" md="12" sm="12" className="mx-auto m-2 pb-3">
            <Row className="mx-auto pt-2 CarrouselContainer">
              <Col
                xs="12"
                md="12"
                sm="12"
                className="fontImpoListLive text-center mx-auto m-4"
              >
                <h2 className="fontImpoListLive">
                  <i
                    style={{ color: "#f7142f", fontWeight: "bold" }}
                    className=" simple-icon-social-youtube mr-2"
                  />
                  TRANSMISIONES EN VIVO
                </h2>
              </Col>
              <Col
                xs="12"
                md="12"
                sm="12"
                className="mb-3 fontImpoList mx-auto "
              >
                <Transmision />
              </Col>
            </Row>
          </Col>
        </Row>

        <Row>
          <Col
            xs="12"
            md="12"
            sm="12"
            style={{ fontStyle: "italic" }}
            className="fontImpoListLive mx-auto mt-4"
          >
            <h2 className="fontImpoListLive">~TESTIMONIOS~</h2>
          </Col>
          <Col xs="12" md="12" sm="12" className="color-blue mx-auto mb-4">
            <FontAwesomeIcon
              icon={faCheckCircle}
              className="mr-2 color-green"
            />
            Importadores y Fabricantes
          </Col>
          <Col
            xs="12"
            md="12"
            sm="12"
            className="fontImpoList pb-4 mb-5 mx-auto NoPadding"
          >
            <Testimonios />
          </Col>
        </Row>
      </div>
    );
  }

  toggleProductDetails(e) {
    var key = e;
    let data = this.state.product.filter(function (item) {
      if (key == item.key) {
        return item.Producto;
      }
    });
    if (data.length > 0) {
      this.setState({
        toggleProductDetailsdata: [data[0]],
        ClienteStock: [data[0].clientes],
        typeProduct: data[0].TipoMerca,
        toggleProductDetails: true,
      });
    }
  }

  toggleProductDetailsOpen() {
    this.setState((prevState) => ({
      toggleProductDetails: !prevState.toggleProductDetails,
    }));
  }

  render() {
    return (
      <Fragment>
        <Title>
          <title>
            {window.location.pathname.replace("/", "") +
              " | " +
              "PIC  - Calculadora de Fletes"}
          </title>
        </Title>
        <div>
          {/* <BotonSenioLogin />*/}
          <Row className="h-100 ">
            <Col
              xs="12"
              md="12"
              xl="10"
              lg="11"
              sm="12"
              className="mx-auto pb-2 pt-5 NoPadding"
            >
              <Row className="mx-auto NoPadding p-1 mx-auto text-justify">
                <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
                  <div className="font text-center pb-2">
                    IMPORTACIONES GRUPALES
                  </div>
                </Col>
                <Col
                  xs="12"
                  md="6"
                  xl="5"
                  lg="6"
                  sm="12"
                  className="text-center mx-auto"
                >
                  <Video />
                </Col>
                <Col
                  xs="12"
                  md="6"
                  xl="7"
                  lg="6"
                  sm="12"
                  style={{
                    alignSelf: "center",
                    fontSize: "1rem",
                    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
                    fontWeight: "400",
                    lineHeight: "1.334",
                    letterSpacing: "0em",
                  }}
                  className="mx-auto"
                >
                  <div>
                    <h4 className="pb-2" style={{ fontWeight: "bold" }}>
                      IMPORTACIONES PARA PYMES Y EMPRENDEDORES
                    </h4>
                  </div>
                  <div className="pb-2" style={{ textIndent: "40px" }}>
                    Las principales desafiantes al momento de empezar una
                    IMPORTACIÓN son: La falta de capital y el desconocimiento de
                    proveedores confiables.
                  </div>
                  <div style={{ textIndent: "40px" }}>
                    Por lo antes expuesto PIC CARGO ha creado una nueva linea de
                    atención a nuestros importantes para que puedan realizar su
                    importación facil y segura.
                  </div>
                </Col>
              </Row>

              <Row className="h-100 pt-5">
                <Col
                  xs="12"
                  md="12"
                  xl="12"
                  lg="12"
                  sm="12"
                  style={{ display: "inline-block" }}
                  className="mx-auto pt-4"
                >
                  <RouteBack />

                  <NavLink to="/FAQ">
                    <Button className="btn-style-quest m-1 btn btn-success btn-sm float-right">
                      <i className="mr-2 icon-question" />
                      Preguntas Frecuentes
                    </Button>
                  </NavLink>
                </Col>

                {this.state.product.length > 0 ? (
                  this.renderLoginButton()
                ) : (
                  <Col
                    xs="12"
                    md="12"
                    sm="12"
                    style={{ height: "10rem" }}
                    className=" mx-auto text-center m-3"
                  >
                    Cargando... <Spinner />
                  </Col>
                )}

                <Whatsapp pag={"importadores"} />
              </Row>
            </Col>
          </Row>

          <div
            className={`${this.state.product.length > 0 ? "" : "fixed-bottom"}`}
          >
            <DefaultFooter />
          </div>
          {/* <Publicidad />*/}
        </div>

        {this.state.toggleProductDetailsdata.length > 0 && (
          <ModalProductDetails
            active={this.state.toggleProductDetails}
            item={this.state.toggleProductDetailsdata}
            typeProduct={this.state.typeProduct}
            ClienteStock={this.state.ClienteStock}
            toggle={this.toggleProductDetailsOpen}
          />
        )}
        <Floatwhatsapp />
      </Fragment>
    );
  }
}

export default SeccionImportadores;

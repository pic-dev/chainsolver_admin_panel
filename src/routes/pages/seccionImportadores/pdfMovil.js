import React, { useState, useEffect } from "react";
import { Document, Page, pdfjs } from "react-pdf";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import ArrowForwardIosOutlinedIcon from '@material-ui/icons/ArrowForwardIosOutlined';
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';
import ZoomOutOutlinedIcon from '@material-ui/icons/ZoomOutOutlined';
import ZoomInOutlinedIcon from '@material-ui/icons/ZoomInOutlined';
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  heroContent: {
    paddingBottom:20,
    padding: 0,
    textAlign: "-webkit-center",
  },
  page: {
    width: "100%",
  },
}));

const MOBILE_WIDTH = 768;

export default function pdf(props) {
  const classes = useStyles();
 
  pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [scale, setPageScale] = useState(1);
  /*To Prevent right click on screen*/
  document.addEventListener("contextmenu", (event) => {
    event.preventDefault();
  });


  useEffect(() => {
    window.addEventListener('resize', resize);
    resize();
    return () => {
      window.removeEventListener('resize', resize);
    }
  }, [])

  /*When document gets loaded successfully*/
  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setPageNumber(props.pag ? props.pag : 1);
  }

  function resize() {
    if (window.innerWidth <= MOBILE_WIDTH) {
      setPageScale(0.75);
    }
  }

  function changePage(offset) {
    setPageNumber((prevPageNumber) => prevPageNumber + offset);
  }

  function previousPage() {
    changePage(-1);
  }

  function nextPage() {
    changePage(1);
  }

  function zoomIn() {
    setPageScale(scale + 0.2);
  }

  function zoomOut() {
    setPageScale(scale - 0.2);
  }

  return (
    <>
      <Container maxWidth="md" component="main" className={classes.heroContent}>
      <CssBaseline />
        <Grid container spacing={5}>
          <Grid item xs={12} sm={12} md={12}>
            <ButtonGroup
              variant="text"
              color="primary"
              aria-label="text primary button group"
            >
              <Button disabled={pageNumber <= 1} onClick={previousPage}>
               <ArrowBackIosOutlinedIcon />
              </Button>
              <Button onClick={zoomIn}>
                <ZoomInOutlinedIcon />
              </Button>
              <Button>
                {" "}
                Página {pageNumber || (numPages ? 1 : "--")} /{" "}
                {numPages || "--"}
              </Button>
           
              <Button  onClick={zoomOut}>
                <ZoomOutOutlinedIcon />
              </Button>
              <Button disabled={pageNumber >= numPages} onClick={nextPage}>
                <ArrowForwardIosOutlinedIcon />
              </Button>
            </ButtonGroup>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>


        <Document
          file={props.url}
          onLoadSuccess={onDocumentLoadSuccess}

          loading={
            <div>
              {" "}
              Cargando... <span className="loadingBlock"></span>
            </div>
          }
        >  <Page  scale={scale} width={ window.screen.width - 5 }   height={700}  pageNumber={pageNumber} />
        </Document>
        </Grid>
        </Grid>
      </Container>
    </>
  );
}

import React, { useState } from "react";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  ListGroup,
  ListGroupItem,
  Collapse,
  Alert,
  NavbarToggler,
  NavItem,
  Nav,
} from "reactstrap";
import { imgProducto } from "Util/Utils";
import { Link } from "react-router-dom";
import { isMovil } from "Util/Utils";
import { Carousel } from "react-responsive-carousel";
import classnames from "classnames";
import RouteBack from "Util/RouteBack";
import Spinner from "Components/spinner";
import "../sidebarJunior/App.css";
const ArticulosList = React.lazy(() => import("./articulosList"));
const Pedido = React.lazy(() => import("./pedido"));
const PdfMovil = React.lazy(() => import("./pdfMovil"));
const ModaPedidoMovil = React.lazy(() => import("Util/Modals/modaPedidoMovil"));

export default function ProductosDetallesCampaña(props) {
  const [togglePDF, setTogglePDF] = useState(true);
  const [catalogoSelected, setCatalogoSelected] = useState("");
  const [articuloSelected, setArticuloSelected] = useState([]);
  const [cargandoCatalogos, setCargandoCatalogos] = useState(false);
  const [catalogosDisponibles, setCatalogosDisponibles] = useState([]);
  const [rubrosActivo, setRubrosActivo] = useState(true);
  const [isOpen, setIsOpen] = useState(false);
  const [openPedidoMovil, setOpenPedidoMovil] = useState(false);
  const [isShowCatalogos, setIsShowCatalogos] = useState(false);
  const [fullscreen, setIsfullscreen] = useState(false);
  const [tipo, setTipo] = useState("");

  const toggleModalPedidoMovil = () => {
    setOpenPedidoMovil(!openPedidoMovil);
  };

  const toggle = (e) => {
    setCatalogoSelected(e);
    setTogglePDF(!togglePDF);
  };

  const getCatalogos = async (e) => {
    setCargandoCatalogos(true);

    await fetch(`https://point.qreport.site/catalogos/r/c/${e}/${props.id}`)
      .then((res) => res.json())
      .then((response) => {
        if (response[0].statusCatalogo == "1") {
          setTipo("catalogos");
          setCatalogosDisponibles(response);
          setRubrosActivo(false);
          setCargandoCatalogos(false);
        } else {
          setTipo("articulos");
          setTogglePDF(false);
          setCatalogoSelected(response[0]);
          setTogglePDF(false);
          setCatalogosDisponibles(response);
          setRubrosActivo(false);
          setCargandoCatalogos(false);
          setTipo("articulos");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const ArticuloSelect = (e) => {
    setArticuloSelected(e);
    toggleModalPedidoMovil();
    if (fullscreen) {
      setIsfullscreen(false);
    }
  };

  
  return (
    <Container className="mt-5 pt-5">
      <Row>
        <Col xs="12" md="12" sm="12" className="mx-auto pt-2 pb-2">
          <div className="font text-center pb-2">Importaciones Grupales</div>
        </Col>
      </Row>
      {props.toggleProducto ? (
        <div>
          <Col
              xs="12"
              md="12"
              lg="10"
              sm="12"
            style={{ display: "flex", justifyContent: "space-between" }}
            className="NoPadding mx-auto pt-2 pb-2"
          >
            {togglePDF == true ? (
              <RouteBack />
            ) : (
              <i
                className="BackIcon simple-icon-arrow-left-circle float-left"
                onClick={() => {
                  toggle("");
                }}
              />
            )}

            <div className="font2 color-blue-2 p-1 text-center pb-2">
              {props.campañas.description.toUpperCase()}
            </div>
            <img
              key="img"
              src={imgProducto(props.campañas.destino)}
              style={{ height: isMovil() ? "3rem" : "4rem"}}
              alt="logo"
            />
          </Col>
          {togglePDF ? (
            <Row>
              <Col
               xs="12"
               md="12"
               lg="10"
               sm="12"
                className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
              >
                <Row className="mx-auto">
                  <Col xs="12" md="6">
                    <Card
                      className="p-1"
                      style={{
                        alignItems: "center",
                      }}
                    >  <div   style={{ border:"5px solid #d2d2d2",backgroundColor:"white" }}
                    >
                   <img
                     src={props.campañas.portada}
                     className="imgDetails3"
                    alt="logo"
                   />
                 </div>

                      <CardBody
                        style={{
                          padding: "1rem 0px 0px",
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        <CardTitle
                          className="m-0 p-0"
                          style={{ fontWeight: "bold" }}
                        >
                          {" "}
                          <a
                            style={{ padding: "0px" }}
                            href="https://wa.link/xbr57j"
                            rel="noopener noreferrer"
                            target="_blank"
                          >
                            {" "}
                            <i className="whatsapp-contacto" />{" "}
                          </a>
                        </CardTitle>
                      </CardBody>
                    </Card>
                  </Col>

                  <Col xs="12" md="6">
                    {/*primer eleccion de catalogo*/}
                    {props.campañas.market === 1 &&
                          <div className="btn-style-ConcentadoresPdf text-center arial mb-2 pt-3 pb-3 pt-2 pb-2 alert alert-success">
                            <span>MUY PRONTO </span>
                          
                          </div>
                     } 
                    {props.rubros.length > 0 && (
                      <div
                        style={{
                          textAlign: "right",
                        }}
                      >
                        {" "}
                        <div
                          style={{
                            borderRadius: "0.4rem",
                          }}
                          className="bg-blue-2"
                        >
                          <div
                            className="btn-style-ConcentadoresPdf text-white arial mb-2 pt-3 pb-3 pt-2 pb-2 bg-blue-2"
                            onClick={() => setIsOpen(!isOpen)}
                          >
                            <span>Ver Catalogos Disponibles       <i
                              style={{
                                backgroundColor: "white",
                                borderRadius: "50%",
                                fontSize: "1rem",
                                padding: ".2rem",
                                fontWeight: "bold",
                                cursor: "pointer",
                              }}
                              className={`ml-2 float-right color-orange ${
                                isOpen
                                  ? "simple-icon-arrow-up"
                                  : "simple-icon-arrow-down"
                              }`}
                            /></span>
                      
                          </div>

                          <Collapse className="p-2" isOpen={isOpen}>
                            {rubrosActivo ? (
                              <ListGroup>
                                {
                                  <ListGroupItem>
                                    <Alert
                                      className="arial"
                                      color="info"
                                      isOpen={cargandoCatalogos}
                                      toggle={() =>
                                        setCargandoCatalogos(!cargandoCatalogos)
                                      }
                                    >
                                      Cargando Catalagos...{" "}
                                      <span className="loadingBlock"></span>
                                    </Alert>
                                  </ListGroupItem>
                                }
                                {props.rubros.map((item, index) => (
                                  <ListGroupItem
                                    onClick={() => {
                                      getCatalogos(item.id);
                                    }}
                                    style={{
                                      textTransform: "uppercase",
                                      borderBottom: "1px solid #8f8f8f",
                                      fontSize: "0.8rem",
                                      cursor: "pointer",
                                    }}
                                    className="arial"
                                    key={index + "ListGroupItem"}
                                  >
                                    <span>{item.name}</span>
                                    <i
                                      style={{
                                        fontSize: "0,8rem",
                                        fontWeight: "bold",
                                        backgroundColor: "black",
                                        color: "white",
                                        borderRadius: "50%",
                                        padding: ".1rem .4rem",
                                        cursor: "pointer",
                                      }}
                                      className="ml-2 mr-2 simple-icon-eye"
                                    />
                                  </ListGroupItem>
                                ))}
                              </ListGroup>
                            ) : (
                              <ListGroup>
                                <ListGroupItem>
                                  <div
                                    className="p-2"
                                    style={{
                                      width: "100%",
                                      backgroundColor: "#6c757d",
                                      color: "white",
                                      fontWeight: "bold",
                                      fontSize: "1rem",
                                      cursor: "pointer",
                                    }}
                                    onClick={() => {
                                      setRubrosActivo(!rubrosActivo);
                                    }}
                                  >
                                    {" "}
                                    <span>
                                      <i className=" simple-icon-arrow-left-circle  mr-2" />
                                      VOLVER A LOS RUBROS
                                    </span>
                                  </div>
                                </ListGroupItem>
                                {catalogosDisponibles.map((item, index) => (
                                  <ListGroupItem
                                    onClick={() => {
                                      toggle(item);
                                    }}
                                    style={{
                                      cursor: "pointer",
                                      textTransform: "uppercase",
                                      borderBottom: "1px solid #8f8f8f",
                                      fontSize: "0.8rem",
                                    }}
                                    className="arial"
                                    key={index + "ListGroupItem2"}
                                  >
                                    {" "}
                                    {item.name}
                                    <i
                                      style={{
                                        fontSize: "0.8rem",
                                        fontWeight: "bold",
                                        backgroundColor: "black",
                                        color: "white",
                                        borderRadius: "50%",
                                        padding: ".1rem .4rem",
                                        cursor: "pointer",
                                      }}
                                      className="ml-2 mr-2 simple-icon-eye"
                                    />
                                  </ListGroupItem>
                                ))}
                              </ListGroup>
                            )}
                          </Collapse>
                        </div>
                     </div>
                    )}{" "}
                        <div
                        style={{
                          textAlign: "right",
                        }}
                      > 
                      <Link to="/ImportadoresVip">
                          <div className="btn-style-ConcentadoresPdf text-white arial mb-2 pt-3 pb-3 pt-2 pb-2 bg-blue-2">
                            <span>Inversión Mínima</span>
                            <i
                              style={{
                                backgroundColor: "white",
                                borderRadius: "50%",
                                fontSize: "1rem",
                                padding: ".2rem",
                                fontWeight: "bold",
                              }}
                              className="mr-2 ml-2 float-right color-orange simple-icon-info"
                            />
                          </div>{" "}
                        </Link>
                        <Link to="/FAQ">
                          <div className="btn-style-ConcentadoresPdf text-white arial pt-3 pb-3 pt-2 pb-2  bg-blue-2">
                            <span>Preguntas Frecuentes</span>
                            <i
                              style={{
                                backgroundColor: "white",
                                borderRadius: "50%",
                                fontSize: "1rem",
                                padding: ".2rem",
                                fontWeight: "bold",
                              }}
                              className="mr-2 ml-2 float-right color-orange simple-icon-question"
                            />
                          </div>
                        </Link>
                      </div>
                   {/*primer eleccion de catalogo*/}
                  </Col>
                  <Col
                    xs="12"
                    md="12"
                    sm="12"
                    className="p-3"
                    style={{ textAlign: "center" }}
                  ><Link to="/Importadores">
                      <Button className="btn-style-backConcentradores" size="sm">
                        Volver a Sección de Importadores
                      </Button>
                    </Link>
                  </Col>
                </Row>
              </Col>
            </Row>
          ) : (
            <Row>
              {props.selectImportador ? (
                <Col
                  xs="12"
                  md="12"
                  sm="12"
                  className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
                >
                  <Row className="mx-auto">
                    {tipo === "catalogos" && (
                      <div
                        className={classnames("openEfecto sidebar", {
                          "is-open": isShowCatalogos,
                        })}
                        style={{
                          alignItems: "center",
                        }}
                      >
                        <div className="sidebar-header">
                          <NavbarToggler
                            className="float-right display-lg"
                            style={{
                              backgroundColor: "#0d5084 !important",
                              border: "solid white 1px",
                            }}
                            onClick={() => setIsShowCatalogos(!isShowCatalogos)}
                          />
                          <span
                            className="display-sm"
                            color="info"
                            onClick={() => setIsShowCatalogos(!isShowCatalogos)}
                            style={{ color: "#fff" }}
                          >
                            &times;
                          </span>
                        </div>
                        <section
                          className="side-menu"
                          style={{
                            padding: "1rem 0 1rem 0",
                            width: "100%",
                            textAlign: "right",
                          }}
                        >
                          {rubrosActivo ? (
                            <Nav vertical className="list-unstyled pb-3">
                              <p
                                style={{ fontSize: "1rem" }}
                                className="arial mr-5"
                              >
                                Catalogos Disponibles
                              </p>
                              {
                                <NavItem>
                                  <Alert
                                    className="arial"
                                    color="success"
                                    isOpen={cargandoCatalogos}
                                    toggle={() =>
                                      setCargandoCatalogos(!cargandoCatalogos)
                                    }
                                  >
                                    Cargando Catalagos...{" "}
                                    <span className="loadingBlock"></span>
                                  </Alert>
                                </NavItem>
                              }
                              {props.rubros.map((item, index) => (
                                <NavItem
                                  onClick={() => {
                                    getCatalogos(item.id);
                                  }}
                                  style={{
                                    borderBottom: "1px solid #8f8f8f",
                                    cursor: "pointer",
                                  }}
                                  className="arial"
                                  key={index + "ListGroupItemrubros"}
                                >
                                  {" "}
                                  {item.name}
                                  <i
                                    onClick={() => {
                                      getCatalogos(item.id);
                                    }}
                                    style={{
                                      fontSize: "1rem",
                                      fontWeight: "bold",
                                      backgroundColor: "black",
                                      color: "white",
                                      borderRadius: "50%",
                                      padding: ".1rem .4rem",
                                      cursor: "pointer",
                                    }}
                                    className="ml-2 mr-2 simple-icon-eye"
                                  />
                                </NavItem>
                              ))}
                            </Nav>
                          ) : (
                            <Nav>
                              <p
                                style={{ fontSize: "1rem" }}
                                className="arial mr-5"
                              >
                                Catalogos Disponibles
                              </p>
                              <NavItem>
                                <div
                                  className="p-2"
                                  style={{
                                    width: "100%",
                                    backgroundColor: "#6c757d",
                                    color: "white",
                                    fontWeight: "bold",
                                    fontSize: "1rem",
                                    cursor: "pointer",
                                  }}
                                  onClick={() => {
                                    setRubrosActivo(!rubrosActivo);
                                  }}
                                >
                                  {" "}
                                  <span>
                                    <i className=" simple-icon-arrow-left-circle  mr-2" />
                                    VOLVER A LOS RUBROS
                                  </span>
                                </div>
                              </NavItem>
                              {catalogosDisponibles.map((item, index) => (
                                <NavItem
                                  onClick={() => {
                                    setCatalogoSelected(item);
                                    setIsShowCatalogos(false);
                                  }}
                                  style={{
                                    cursor: "pointer",
                                    textTransform: "uppercase",
                                    borderBottom: "1px solid #8f8f8f",
                                    fontSize: "0.8rem",
                                  }}
                                  className="arial"
                                  key={index + "catalogosDisponibles"}
                                >
                                  {" "}
                                  {item.name}
                                  <i
                                    style={{
                                      fontSize: "0.8rem",
                                      fontWeight: "bold",
                                      backgroundColor: "black",
                                      color: "white",
                                      borderRadius: "50%",
                                      padding: ".1rem .4rem",
                                      cursor: "pointer",
                                    }}
                                    className="ml-2 mr-2 simple-icon-eye"
                                  />
                                </NavItem>
                              ))}
                            </Nav>
                          )}
                        </section>
                      </div>
                    )}
                    {isMovil() && props.selectImportador && (
                      <div className="stickyDiv arial text-center  mb-2 ">
                        <div
                          className="p-2"
                          style={{
                            width: "100%",
                            backgroundColor: "#0d4674",
                            color: "white",
                            fontWeight: "bold",
                          }}
                          onClick={() => {
                            toggleModalPedidoMovil();
                          }}
                        >
                          <h2>
                            <i className=" simple-icon-list  mr-2" /> REALIZAR
                            PRE ORDEN EN LINEA
                          </h2>
                        </div>
                      </div>
                    )}
                    <Col
                      className="sidePadding NoPadding"
                      xs="12"
                      md={fullscreen ? "12" : "6"}
                    >
                      <div className="mb-3 pb-4">
                        {tipo === "catalogos" && (
                          <Button
                            color="success"
                            className="float-left btn-style-blue"
                            size="sm"
                            onClick={() => setIsShowCatalogos(!isShowCatalogos)}
                          >
                            VER MAS CATALOGOS{" "}
                            <i className="simple-icon-list ml-2" />
                          </Button>
                        )}
                        <Button
                          color="success"
                          className="display-large float-right btn-style-bluelight"
                          size="sm"
                          onClick={() => {
                            setIsfullscreen(!fullscreen);
                          }}
                        >
                          {fullscreen
                            ? "VOLVER A PRE-ORDEN"
                            : "AMPLIAR CATALOGO"}
                          <i
                            className={`${
                              fullscreen
                                ? "simple-icon-size-fullscreen"
                                : "simple-icon-size-actual"
                            } ml-2`}
                          />
                        </Button>
                      </div>
                      {catalogoSelected.statusCatalogo == 1 ? (
                        <>
                          {catalogoSelected && !isMovil() && (
                            <iframe
                              style={{
                                height: isMovil() ? "30rem" : "50rem",
                                width: "100%",
                              }}
                              src={`https://point.qreport.site/files/${
                                props.JuniorData.length > 0 &&
                                catalogoSelected.statusCp == "Ok"
                                  ? catalogoSelected.ruta_cp
                                  : catalogoSelected.ruta_sp
                              }`}
                            />
                          )}
                          {catalogoSelected && isMovil() && (
                            <PdfMovil
                              style={{
                                height: "30rem",
                                width: "100%",
                              }}
                              url={`https://point.qreport.site/files/${
                                props.JuniorData.length > 0 &&
                                catalogoSelected.statusCp == "Ok"
                                  ? catalogoSelected.ruta_cp
                                  : catalogoSelected.ruta_sp
                              }`}
                            />
                          )}
                        </>
                      ) : (
                        <ArticulosList
                          ArticuloSelect={ArticuloSelect}
                          fullscreen={fullscreen}
                          status={props.selectImportador}
                          catalago={catalogoSelected}
                          JuniorData={props.JuniorData}
                          toggleModal={props.toggleModal}
                        />
                      )}
                    </Col>

                    {!isMovil() && (
                      <Col
                        style={{
                          display: fullscreen ? "none" : "unset",
                        }}
                        className="NoPadding"
                        xs="12"
                        md="6"
                      >
                        <Pedido
                          JuniorData={props.JuniorData}
                          catalogo={catalogoSelected}
                          articulo={articuloSelected}
                          setArticuloSelected={setArticuloSelected}
                          tipo={tipo}
                        />
                      </Col>
                    )}
                  </Row>
                </Col>
              ) : (
                <Col
                  xs="12"
                  md="10"
                  sm="12"
                  className="ModalContainer mx-auto  pt-3 p-1 text-justify  "
                >
                  <Row className="mx-auto">
                    {isShowCatalogos ? (
                      <Col
                        className="NoPadding"
                        xs="12"
                        md={props.selectImportador ? "6" : "12"}
                      >
                        <div
                          style={{
                            height: "2.5rem",
                            borderRadius: "0.4rem",
                            textTransform: "uppercase",
                            fontSize: "1rem",
                          }}
                          className="text-white text-center arial mb-2 p-2 bg-blue-2"
                        >
                          {" "}
                          <span>Ver Catalogos Disponibles</span>
                        </div>

                        <div
                          className="p-1"
                          style={{
                            alignItems: "center",
                          }}
                        >
                          {" "}
                          <div
                            style={{
                              padding: "1rem 0 1rem 0",
                              width: "100%",
                              textAlign: "right",
                            }}
                          >
                            {rubrosActivo ? (
                              <ListGroup>
                                {
                                  <ListGroupItem>
                                    <Alert
                                      className="arial"
                                      color="success"
                                      isOpen={cargandoCatalogos}
                                      toggle={() =>
                                        setCargandoCatalogos(!cargandoCatalogos)
                                      }
                                    >
                                      Cargando Catalagos...{" "}
                                      <span className="loadingBlock"></span>
                                    </Alert>
                                  </ListGroupItem>
                                }
                                {props.rubros.map((item, index) => (
                                  <ListGroupItem
                                    onClick={() => {
                                      getCatalogos(item.id);
                                    }}
                                    style={{
                                      textTransform: "uppercase",
                                      borderBottom: "1px solid #8f8f8f",
                                      fontSize: "0.8rem",
                                      cursor: "pointer",
                                    }}
                                    className="arial"
                                    key={index + "ListGroupItemrubros2"}
                                  >
                                    {" "}
                                    {item.name}
                                    <i
                                      onClick={() => {
                                        getCatalogos(item.id);
                                      }}
                                      style={{
                                        fontSize: "0.8rem",
                                        fontWeight: "bold",
                                        backgroundColor: "black",
                                        color: "white",
                                        borderRadius: "50%",
                                        padding: ".1rem .4rem",
                                        cursor: "pointer",
                                      }}
                                      className="ml-2 mr-2 simple-icon-eye"
                                    />
                                  </ListGroupItem>
                                ))}
                              </ListGroup>
                            ) : (
                              <ListGroup>
                                <ListGroupItem>
                                  <div
                                    className="p-2"
                                    style={{
                                      width: "100%",
                                      backgroundColor: "#6c757d",
                                      color: "white",
                                      fontWeight: "bold",
                                      fontSize: "1rem",
                                      cursor: "pointer",
                                    }}
                                    onClick={() => {
                                      setRubrosActivo(!rubrosActivo);
                                    }}
                                  >
                                    {" "}
                                    <span>
                                      <i className=" simple-icon-arrow-left-circle  mr-2" />
                                      VOLVER A LOS RUBROS
                                    </span>
                                  </div>
                                </ListGroupItem>
                                {catalogosDisponibles.map((item, index) => (
                                  <ListGroupItem
                                    onClick={() => {
                                      setCatalogoSelected(item);
                                      setIsShowCatalogos(false);
                                    }}
                                    style={{
                                      cursor: "pointer",
                                      textTransform: "uppercase",
                                      borderBottom: "1px solid #8f8f8f",
                                      fontSize: "0.8rem",
                                    }}
                                    className="arial"
                                    key={index + "catalogosDisponibles2"}
                                  >
                                    {" "}
                                    {item.name}
                                    <i
                                      onClick={() => {
                                        setCatalogoSelected(item);
                                        setIsShowCatalogos(false);
                                      }}
                                      style={{
                                        fontSize: "0.8rem",
                                        fontWeight: "bold",
                                        backgroundColor: "black",
                                        color: "white",
                                        borderRadius: "50%",
                                        padding: ".1rem .4rem",
                                        cursor: "pointer",
                                      }}
                                      className="ml-2 mr-2 simple-icon-eye"
                                    />
                                  </ListGroupItem>
                                ))}
                              </ListGroup>
                            )}{" "}
                          </div>
                        </div>
                      </Col>
                    ) : (
                      <Col
                        className="NoPadding"
                        xs="12"
                        md={props.selectImportador ? "6" : "12"}
                      >
                        {!props.selectImportador && (
                          <div className="stickyDiv arial text-center  mb-2 ">
                            {catalogoSelected.statusCatalogo == 1 && (
                              <div
                                className="p-2"
                                style={{
                                  width: "100%",
                                  backgroundColor: "#FFCB06",
                                  color: "white",
                                  fontWeight: "bold",
                                }}
                                onClick={() =>
                                  setIsShowCatalogos(!isShowCatalogos)
                                }
                              >
                                <h2>
                                  {" "}
                                  <i className=" simple-icon-arrow-left-circle  mr-2" />
                                  VOLVER A LOS CATALOGOS
                                </h2>
                              </div>
                            )}
                            <div
                              className="p-2"
                              style={{
                                width: "100%",
                                backgroundColor: "#0d4674",
                                color: "white",
                                fontWeight: "bold",
                              }}
                              onClick={() => {
                                props.toggleModal();
                              }}
                            >
                              <h2>
                                <i className=" simple-icon-list  mr-2" />{" "}
                                REALIZAR PRE ORDEN EN LINEA
                              </h2>
                            </div>
                          </div>
                        )}
                        {catalogoSelected.statusCatalogo == 1 ? (
                          <>
                            {catalogoSelected && !isMovil() && (
                              <iframe
                                style={{
                                  height: isMovil() ? "30rem" : "50rem",
                                  width: "100%",
                                }}
                                src={`https://point.qreport.site/files/${
                                  props.JuniorData.length > 0 &&
                                  catalogoSelected.statusCp == "Ok"
                                    ? catalogoSelected.ruta_cp
                                    : catalogoSelected.ruta_sp
                                }`}
                              />
                            )}
                            {catalogoSelected && isMovil() && (
                              <PdfMovil
                                style={{
                                  height: "30rem",
                                  width: "100%",
                                }}
                                url={`https://point.qreport.site/files/${
                                  props.JuniorData.length > 0 &&
                                  catalogoSelected.statusCp == "Ok"
                                    ? catalogoSelected.ruta_cp
                                    : catalogoSelected.ruta_sp
                                }`}
                              />
                            )}
                          </>
                        ) : (
                          <ArticulosList
                          fullscreen={fullscreen}
                            ArticuloSelect={ArticuloSelect}
                            status={props.selectImportador}
                            catalago={catalogoSelected}
                            JuniorData={props.JuniorData}
                            toggleModal={props.toggleModal}
                          />
                        )}
                      </Col>
                    )}
                    {props.selectImportador && (
                      <Col className="NoPadding" xs="12" md="6">
                        <Pedido
                          JuniorData={props.JuniorData}
                          catalogo={catalogoSelected}
                          articulo={articuloSelected}
                          setArticuloSelected={setArticuloSelected}
                          tipo={tipo}
                        />
                      </Col>
                    )}
                  </Row>
                </Col>
              )}{" "}
            </Row>
          )}
        </div>
      ) : (
        <Row>
          <Col xs="12" md="12" sm="12" className=" mx-auto text-center m-3">
            Cargando... <Spinner />{" "}
          </Col>
        </Row>
      )}
      {isMovil() && props.selectImportador && (
        <ModaPedidoMovil
          JuniorData={props.JuniorData}
          catalogo={catalogoSelected}
          articulo={articuloSelected}
          setArticuloSelected={setArticuloSelected}
          tipo={tipo}
          toggle={toggleModalPedidoMovil}
          open={openPedidoMovil}
        />
      )}
    </Container>
  );
}

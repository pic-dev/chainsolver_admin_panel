import React, { useEffect, useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import GridContainer from "../../util/Parallax/GridContainer.js";
import GridItem from "../../util/Parallax/GridItem.js";
import StarIcon from "@material-ui/icons/StarBorder";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import { DataGrid } from "@material-ui/data-grid";
import * as data from "../../util/vipClientes.csv";
import Parallax from "../../util/Parallax/Parallax.js";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import PropTypes from "prop-types";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { container } from "../../util/Parallax/material-kit-react.js";
import classNames from "classnames";
import Divider from "@material-ui/core/Divider";
// Generate Order Data

//var csv is the CSV file with headers

const csvJSON = (csv) => {
  var result = [];
  result = csv.map(function (item, index) {
    return {
      id: item.ID,
      Nombre: item.NOMBRE,
      Pais: item.PAIS,
      Provincia: item.PROVINCIA,
      Distrito: item.DISTRITO,
      Telefono: item.TELEFONO,
      Telefono2: item.TELEFONO2,
    };
  });

  //return result; //JavaScript object
  return result; //JSON
};

const SearchProvincias = () => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (current) {
      var exists = !hash[current.Provincia];
      hash[current.Provincia] = true;
      return exists;
    })
    .map((item) => {
      return item.Provincia;
    });

  return array; //JSON
};

const SearchDistritos = (e) => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (item) {
      if (e == item.Provincia) {
        return item;
      }
    })
    .filter(function (current) {
      var exists = !hash[current.Distrito];
      hash[current.Distrito] = true;
      return exists;
    })
    .map((item) => {
      return item.Distrito;
    });

  return array; //JSON
};

const columns = [
  { field: "Nombre", sortable: true, headerName: "Nombre", width: 130 },
  {
    field: "Pais",
    headerName: "Pais",

    sortable: true,
    width: 70,
  },
  {
    field: "Provincia",
    headerName: "Provincia",

    sortable: true,
    width: 160,
  },
  {
    field: "Distrito",
    headerName: "Distrito",
    description: "This column has a value getter and is not sortable.",
    sortable: true,
    width: 160,
  },
  {
    field: "Telefono",
    headerName: "Telefono",
    sortable: true,
    type: "number",
    width: 250,
    renderCell: (params) => (
      <strong>
        {params.value}
        <Button
          variant="contained"
          color="primary"
          size="small"
          style={{ marginLeft: 16 }}
          href={"https://api.whatsapp.com/send?phone=0051" + params.value}
          target="_blank"
        >
          Contactar
        </Button>
      </strong>
    ),
  },
];

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left",
  },
  title: {
    fontSize: "3.5rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative",
    textShadow: "1px -1px 0px black",
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px 0 0",
    textAlign: "justify",
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  displayNone: {
    display: "none",
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  titulo: {
    fontSize: "1.5rem",
    fontWeight: "bold",
    color: "#0d5084",
  },
  mensaje: {
    fontSize: "1rem",
    color: "black",
  },
  heroContent: {
    padding: theme.spacing(4, 0, 6),
    backgroundColor: "#ffffff87",
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
    padding: {
      padding: theme.spacing(3),
    },
    paddingTittle: {
      padding: theme.spacing(10),
      marginBottom: theme.spacing(10),
    },
  },
}));

const tiers = [
  {
    title: "IMPORTADORES JUNIOR",
    description: [
      ">  Inversión 500 Dólares",
      ">  Recibirán ayuda y guía del Importador Senior",
      ">  Si quiere importar algún producto en particular deberá apoyarse con su líder de Zona",
    ],
    buttonText: "Unete a un Senior",
    buttonVariant: "outlined",
  },
  {
    title: "IMPORTADORES SENIOR",
    subheader: "Most popular",
    description: [
      ">  Inversión Mínima 2000 Dólares",
      ">  Reciben ayuda directa para la venta de sus productos por parte de Pic Cargo",
      ">  Se encontrarán como Importadores Directos en nuestra Marketplace https://pic-shopping.com/",
      ">  Pueden escoger los Productos que vamos a mostrar en los catálogos o indicar cual seria la tendencia para las próximas importaciones",
      ">  La inversión es por cada catalogo, es decir podrá mezclar todos los productos del catalogo",
      ">  El Senior recibía un descuento del 10% en caso de que lleguemos a 12 personas por Importación",
    ],
    buttonText: "Unete a un Senior",
    buttonVariant: "contained",
  },
];

export default function Vip() {
  const classes = useStyles();

  const [Province, setProvince] = useState("");
  const [District, setDistrict] = useState("");
  const [DistrictList, setDistrictList] = useState([]);
  const [Country, setCountry] = useState("");
  const [filter, setFilter] = useState(csvJSON(data));
  const matches = useMediaQuery("(min-width:769px)");

  const handleChange = (event, id) => {
    switch (id) {
      case 1:
        setProvince(event.target.value);
        let datos = csvJSON(data).filter(function (item) {
          if (event.target.value == item.Provincia) {
            return item;
          }
        });
        setDistrictList(SearchDistritos(event.target.value));

        setFilter(datos);

        break;
      case 2:
        setCountry(event.target.value);
        break;
      case 3:
        setDistrict(event.target.value);

        let array = csvJSON(data).filter(function (item) {
          if (
            event.target.value == item.Distrito &&
            Province == item.Provincia
          ) {
            return item;
          }
        });
        setFilter(array);
        break;
      default:
        break;
    }
  };

  const [value, setValue] = useState(0);

  const handleTab = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Parallax image={require("Assets/img/PublicidadLarge.png")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand}>
                <h1 className={classes.title}> Importadores Senior</h1>
                <h3 className={classes.subtitle}>
                  Los importadores Senior son aquellos que invierten por cada
                  catalogo un minimo de 2000 dolares. Si deseas invertir menos
                  puedes realizar uniendote al importador mas cercano.
                </h3>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
        <Container
          maxWidth="md"
          component="main"
          className={matches ? classes.heroContent : classes.displayNone}
        >
          <Grid container spacing={5}>
            <Grid item xs={12} sm={12} md={12}>
              <Typography
                variant="h6"
                align="center"
                color="textPrimary"
                component="p"
              >
                Nuestros Importadores
              </Typography>
            </Grid>
            {tiers.map((tier) => (
              <Grid
                item
                key={tier.title}
                xs={12}
                sm={tier.title === "Enterprise" ? 12 : 6}
                md={6}
              >
                <Card>
                  <CardHeader
                    title={tier.title}
                    subheader={tier.subheader}
                    titleTypographyProps={{ align: "center" }}
                    subheaderTypographyProps={{ align: "center" }}
                    action={
                      tier.title === "IMPORTADORES SENIOR" ? <StarIcon /> : null
                    }
                    className={classes.cardHeader}
                  />
                  <CardContent>
                    <ul>
                      {tier.description.map((line) => (
                        <Typography
                          component="li"
                          variant="subtitle1"
                          align="justify"
                          key={line}
                        >
                          {line}
                        </Typography>
                      ))}
                    </ul>
                  </CardContent>
                  <CardActions>
                    <Button
                      fullWidth
                      variant={tier.buttonVariant}
                      color="primary"
                    >
                      {tier.buttonText}
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
        <Container
          maxWidth="md"
          component="main"
          className={matches ? classes.displayNone : classes.tabs}
        >
          {" "}
          <Grid container spacing={5}>
            <Grid item xs={12} sm={12} md={12}>
              <Typography
                variant="h6"
                align="center"
                color="textPrimary"
                component="p"
              >
                Nuestros Importadores
              </Typography>
            </Grid>
          </Grid>
          <div>
            <AppBar position="static">
              <Tabs
                value={value}
                onChange={handleTab}
                aria-label="simple tabs example"
              >
                <Tab label="IMPORTADORES JUNIOR" {...a11yProps(0)} />
                <Tab label="IMPORTADORES SENIOR" {...a11yProps(1)} />
              </Tabs>
            </AppBar>

            <Grid container spacing={5} alignItems="flex-end">
              {tiers.map((tier, index) => (
                <TabPanel
                  className={classes.padding}
                  value={value}
                  index={index}
                >
                  <Grid
                    item
                    key={tier.title}
                    xs={12}
                    sm={tier.title === "Enterprise" ? 12 : 6}
                    md={6}
                  >
                    <Card>
                      <CardHeader
                        title={tier.title}
                        subheader={tier.subheader}
                        titleTypographyProps={{ align: "center" }}
                        subheaderTypographyProps={{ align: "center" }}
                        action={tier.title === "Pro" ? <StarIcon /> : null}
                        className={classes.cardHeader}
                      />
                      <CardContent>
                        <ul>
                          {tier.description.map((line) => (
                            <Typography
                              component="li"
                              variant="subtitle1"
                              align="justify"
                              key={line}
                            >
                              {line}
                            </Typography>
                          ))}
                        </ul>
                      </CardContent>
                      <CardActions>
                        <Button
                          fullWidth
                          variant={tier.buttonVariant}
                          color="primary"
                        >
                          {tier.buttonText}
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                </TabPanel>
              ))}
            </Grid>
          </div>{" "}
        </Container>
        <Divider />
        <Container
          maxWidth="md"
          className={(classes.footer, classes.heroContent)}
        >
          <Typography
            variant="h6"
            align="center"
            color="textPrimary"
            component="p"
          >
            Seleccione una Provincia para ubicar al importador de su zona
          </Typography>
        </Container>
        <Container
          maxWidth="md"
          className={(classes.footer, classes.heroContent)}
        >
          <form className={classes.root} noValidate autoComplete="off">
            <div>
              <TextField
                id="Pais"
                select
                label="Select"
                value={Country}
                onChange={(e) => handleChange(e, 2)}
                helperText="Seleccione su País"
              >
                {" "}
                <MenuItem key={1 + "Pais"} value="PERU">
                  Perú
                </MenuItem>
              </TextField>
              <TextField
                id="Provincia"
                select
                label="Select"
                value={Province}
                onChange={handleChange}
                onChange={(e) => handleChange(e, 1)}
                helperText="Seleccione una Provincia"
              >
                {SearchProvincias().map((option, index) => (
                  <MenuItem key={index + 1 + "Provincia"} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                id="Distrito"
                select
                label="Select"
                value={District}
                onChange={(e) => handleChange(e, 3)}
                helperText="Seleccione una Distrito"
              >
                {DistrictList.map((option, index) => (
                  <MenuItem key={index + 1 + "Distrito"} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </div>
          </form>
        </Container>
        <Container maxWidth="md">
          <div style={{ height: 400, width: "100%" }}>
            <DataGrid
              rows={filter}
              columns={columns}
              pageSize={5}
              checkboxSelection
            />
          </div>
        </Container>
        <Container maxWidth="md">
          <Copyright />
        </Container>
      </div>
    </React.Fragment>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

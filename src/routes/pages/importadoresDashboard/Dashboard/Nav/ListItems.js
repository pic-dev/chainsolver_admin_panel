import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import SearchIcon from '@material-ui/icons/Search';
import { Link } from "react-router-dom";
export const mainListItems = (
  <div>
    {" "}
  
    <Link to="/PedidosJunior">
      <ListItem button>
        <ListItemIcon>
          <ShoppingCartIcon className="color-white" />
        </ListItemIcon>
        <ListItemText className="color-white" primary="Ver Pedidos" />
      </ListItem>
    </Link>
    <Link to="/BuscarPedido">
    <ListItem button>
      <ListItemIcon>
        <SearchIcon className="color-white" />
      </ListItemIcon>
      <ListItemText className="color-white" primary="Buscar Pedido" />
    </ListItem>
    </Link>
    <Link to="/Abonos">
    <ListItem button>
      <ListItemIcon>
        <BarChartIcon className="color-white" />
      </ListItemIcon>
      <ListItemText className="color-white" primary="Ver abonos" />
    </ListItem>
    </Link>
    <Link to="/JuniorDashboard">
      <ListItem button>
        <ListItemIcon>
          <PeopleIcon className="color-white" />
        </ListItemIcon>
        <ListItemText className="color-white" primary="Agregar Junior" />
      </ListItem>
    </Link>

  </div>
);

export const mainListItemsJunior = (
  <div>
    <Link to="/PedidosJunior">
      <ListItem button>
        <ListItemIcon>
          <ShoppingCartIcon className="color-white" />
        </ListItemIcon>
        <ListItemText className="color-white" primary="Ver Pedidos" />
      </ListItem>
    </Link>
    <Link to="/BuscarPedido">
    <ListItem button>
      <ListItemIcon>
        <SearchIcon className="color-white" />
      </ListItemIcon>
      <ListItemText className="color-white" primary="Buscar Pedido" />
    </ListItem>
    </Link>
  </div>
);


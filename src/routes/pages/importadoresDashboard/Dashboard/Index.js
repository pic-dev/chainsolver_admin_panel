import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Nav from "./Nav/NavImportadores";




const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
}));

export default function Dashboard(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const handleDrawerOpen = () => {
    setOpen(!open);
  };


  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav userSeniorData={props.userSeniorData} handleLogout={props.handleLogout} handleDrawerOpen={handleDrawerOpen} open={open} />
      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Dashboard
        </Typography>
       </main>
    </div>
  );
}

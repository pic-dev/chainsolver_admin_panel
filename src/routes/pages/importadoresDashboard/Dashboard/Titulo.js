import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";

export function Titulo(props) {
  return (
    <Typography
      className="p-2"
      component="h3"
      variant="h6"
      color="primary"
      gutterBottom
    >
      {props.children}
    </Typography>
  );
}

Titulo.propTypes = {
  children: PropTypes.node,
};

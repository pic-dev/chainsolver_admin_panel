import React, { useEffect, useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import Nav from "../Nav/NavImportadores";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import { PageView } from "Util/tracking";
import PedidosTable from "Util/tablePedidoSearch";
import Alert from "Util/dialogAlert";
import Button from "@material-ui/core/Button";
import axios from "axios";

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    top: "auto",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    zIndex: "1049",
    color: "white",
    background: "#304152",
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  submit: {
    margin: theme.spacing(2, 3, 2),
  },
}));

export default function Dashboard(props) {
  const classes = useStyles();
  const { width } = props;
  const [alert, setAlert] = useState(false);
  const [filter, setFilter] = useState([]);
  const [open, setOpen] = useState(true);
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const handleChange = (e) => {
    setAlert(false);
    e.preventDefault();
    const { Orden } = e.target.elements;
    axios
      .get(`https://point.qreport.site/productos/orden/${Orden.value}`)
      .then((res) => {
        if (res.data.length > 0) {
          setFilter(res.data);
        } else {
          setAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
  }, []);

  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav
        userSeniorData={props.userSeniorData}
        handleLogout={props.handleLogout}
        handleDrawerOpen={handleDrawerOpen}
        open={open}
      />
      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Dashboard - Busqueda de Pedido
        </Typography>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={5}>
            <Grid item xs={12} sm={12} md={12}>
              <img
                key="img"
                src="/assets/img/seo.svg"
                alt="logo"
                style={{ height: "4rem" }}
              />
              <Typography
                variant="h6"
                align="center"
                color="textPrimary"
                component="p"
              >
                Busqueda de Pedidos
              </Typography>
            </Grid>
       

         
            <Grid item xs={12} sm={12} md={12}>
              <form onSubmit={handleChange} autoComplete="off">
                <TextField
                  required
                  id="Orden"
                  type="number"
                  name="Orden"
                  variant="filled"
                  label="ID Orden"
                  helperText="Ingrese el ID de su orden"
                />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Buscar
                </Button>
              </form>
            </Grid>
            <Divider />
            {filter.length > 0 && (
              <Grid item xs={12} md={12} lg={12}>
                <div style={{ width: "100%", textAlign: "center" }}>
                  <PedidosTable productos={filter} />
                </div>
              </Grid>
            )}
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>

        <Alert open={alert}  setAlert={setAlert} type={"error"} message="Numero de orden no encontrada, verifique" />
                  
  
      </main>
    </div>
  );
}

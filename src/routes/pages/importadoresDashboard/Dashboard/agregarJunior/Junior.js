import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import Orders from "./JuniorsList";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AddOutlined from "@material-ui/icons/AddOutlined";
import axios from "axios";
import Alert from "@material-ui/lab/Alert";
import Nav from "../Nav/NavImportadores";
import ModalConfirmDelete from "../modalConfirmDelete";
import { isMovil } from "Util/Utils";
import JuniorListMovil from "./JuniorsListMovil"
function Copyright() {

  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    color: "white",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    top: "auto",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    zIndex: "1049",
    color: "white",
    background: "#304152",
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function Junior(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [SucessAdd, setSucessAdd] = useState(false);
  const [ErrorAdd, setErrorAdd] = useState(false);
  const [juniorList, setJuniorList] = useState([]);
  const [idDelete, setIdDelete] = React.useState("");
  const [openModalDelete, setOpenModalDelete] = React.useState(false);

  const handleChange = (panel) => (isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleModalDelete = (e) => {
    setIdDelete(e);
    setOpenModalDelete(!openModalDelete);
  };
  const [open, setOpen] = useState(true);
  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const { dni, firstName, lastName, telefono, email } = e.target.elements;

    axios
      .post("https://point.qreport.site/junior", {
        id_senior: props.userSeniorData[0].id,
        documento: dni.value,
        nombres: firstName.value,
        apellidos: lastName.value,
        telefono: telefono.value,
        correo: email ? email.value : "",
        usuario: dni.value,
        contraseña: dni.value,
        status: true,
      })
      .then((res) => {
        if (res.data.boolean) {
          document.getElementById("dni").value = "";
          document.getElementById("firstName").value = "";
          document.getElementById("lastName").value = "";
          document.getElementById("telefono").value = "";
          document.getElementById("email").value = "";
          setExpanded(false);
          toggleAlert(1);
          getData();
        }
      })
      .catch((error) => {
        toggleAlert(2);
      });
  };
  const toggleAlert = (e) => {
    switch (e) {
      case 1:
        setSucessAdd(true);

        setTimeout(() => {
          setSucessAdd(false);
        }, 2000);

        break;
      case 2:
        setErrorAdd(true);
        setTimeout(() => {
          setErrorAdd(false);
        }, 1000);
        break;
      default:
        break;
    }
  };

  const getData = async () => {
    await axios
      .get(`https://point.qreport.site/junior/s/${props.userSeniorData[0].id}`)
      .then((res) => {
        setJuniorList([]);
        setJuniorList(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deleteJunior = async (e) => {
    await axios
      .delete(`https://point.qreport.site/junior/${e}`)
      .then(() => {
        handleModalDelete("");
        getData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }
    getData();
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav
        userSeniorData={props.userSeniorData}
        handleLogout={props.handleLogout}
        handleDrawerOpen={handleDrawerOpen}
        open={open}
      />

      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Gestion de Junior
        </Typography>
        <Container maxWidth="lg" className={classes.container}>
          <Accordion
            expanded={expanded === "panel1"}
            onChange={handleChange("panel1")}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography component="h2" variant="h5">
                <AddOutlined /> Agregar Junior
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <form className={classes.form} onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      autoComplete="dni"
                      name="dni"
                      required
                      fullWidth
                      id="dni"
                      label="DNI"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      autoComplete="fname"
                      name="firstName"
                      required
                      fullWidth
                      id="firstName"
                      label="Nombre"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      required
                      fullWidth
                      id="lastName"
                      label="Apellido"
                      name="Apellido"
                      autoComplete="lname"
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      required
                      fullWidth
                      id="telefono"
                      label="telefono"
                      name="telefono"
                      autoComplete="lname"
                    />
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <TextField
                      fullWidth
                      id="email"
                      label="Email"
                      name="email"
                      autoComplete="email"
                    />
                  </Grid>
                </Grid>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Registrar
                </Button>
              </form>
            </AccordionDetails>
          </Accordion>
          {SucessAdd && (
            <Alert severity="success">Junior Agregado exitosamente</Alert>
          )}
          {ErrorAdd && (
            <Alert severity="error">
              Error al Cargar Junior, Intente nuevamente
            </Alert>
          )}
          <Grid container spacing={2}>
            {/* Recent Orders */}
            <Grid item xs={12}>
              <Paper className={classes.paper}>
              {isMovil() ?   <JuniorListMovil deleteJunior={handleModalDelete} data={juniorList} />
                   :   <Orders deleteJunior={handleModalDelete} data={juniorList} />}
              
              </Paper>
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
        {idDelete && (
          <ModalConfirmDelete
            id={idDelete}
            handleDelete={deleteJunior}
            open={openModalDelete}
            handleModal={handleModalDelete}
            type={"al Junior Seleccionado"}
          />
        )}
      </main>
    </div>
  );
}

import React, { useState } from "react";

import { green, red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutline";
import _ from "lodash";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
  loading: {
    width: "100%",
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.productos
    .map(({ subtotal }) => subtotal)
    .reduce((sum, i) => sum + i, 0);
}

function statusItem(items) {
  return items.productos.map(({ status }) => status);
}

function filterDelete(items, id) {
  return items.filter((item) => {
    if (item.id_orden != id) {
      return item;
    }
  });
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableBody>
        <>
          <TableRow className={classes.root}>
            <TableCell
              style={{ fontWeight: "bold" }}
              onClick={() => setOpen(!open)}
              colSpan={4}
            >
              {" "}
              <IconButton aria-label="expand row" size="small">
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
              {`${row.nombres} ${row.apellidos}`}
            </TableCell>
          </TableRow>

          <TableRow className={classes.root}>
            <TableCell onClick={() => setOpen(!open)}>
              DNI: {row.documento}
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell style={{ padding: 0 }} colSpan={6}>
              <Collapse in={open} timeout="auto" unmountOnExit>
                <Box margin={1}>
                  <Table size={"small"} aria-label="products">
                    <TableBody>
                      <TableRow>
                        <TableCell>Telefono</TableCell>
                        <TableCell style={{ fontWeight: "300" }}>
                          {row.telefono}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Correo</TableCell>
                        <TableCell style={{ fontWeight: "300" }}>
                          {row.correo}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Usuario</TableCell>
                        <TableCell style={{ fontWeight: "300" }}>
                          {row.usuario}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Contraseña</TableCell>
                        <TableCell style={{ fontWeight: "300" }}>
                          {row.contraseña}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Acción</TableCell>
                        <TableCell style={{ fontWeight: "300" }}>
                          <IconButton
                            aria-label="open"
                            size="small"
                            onClick={() => props.deleteJunior(row.id)}
                          >
                            <DeleteOutlinedIcon style={{ color: red[500] }} />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </Box>
              </Collapse>
            </TableCell>
          </TableRow>
        </>
      </TableBody>
    </React.Fragment>
  );
}

export default function JuniorListMovil(props) {
  const classes = useRowStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div>
      <Paper>
        <TableContainer component={Paper}>
          <Typography
            className="p-2"
            style={{ background: "#0d4674", color: "white" }}
            component="h2"
            variant="h6"
            gutterBottom
          >
             {props.data.length > 0 ? `Tiene ${props.data.length} Junior resgistrados` : ""}
          </Typography>
          <Table size={"small"} aria-label="collapsible table">
            <TableHead>
              <TableRow>
                <TableCell />
              </TableRow>
            </TableHead>
            {props.data.length == 0 && (
              <TableBody>
                {" "}
                <TableRow className={classes.root}>
                  <TableCell style={{ fontWeight: "bold" }} colSpan={4}>
                    No posee Juniors actualmente
                  </TableCell>
                </TableRow>
              </TableBody>
            )}

            {props.data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <Row
                    deleteJunior={props.deleteJunior}
                    key={row.id}
                    row={row}
                  />
                );
              })}
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={props.data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

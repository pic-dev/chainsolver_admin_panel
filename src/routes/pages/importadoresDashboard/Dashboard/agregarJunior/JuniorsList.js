import React from "react";
import { DataGrid } from "@material-ui/data-grid";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutline";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
  },
}));

function LinearIndeterminate() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Skeleton animation="wave" />

      <Typography component="h2" variant="h5">
        No tiene Juniors Registrados Actualmente
      </Typography>
      <Skeleton animation="wave" />
    </div>
  );
}

export default function DataTable(props) {
  const columns = [
    {
      field: "fullName",
      headerName: "Junior",
      description: "This column has a value getter and is not sortable.",
      sortable: true,
      width: 200,
      valueGetter: (params) =>
        `${params.getValue("nombres") || ""} ${
          params.getValue("apellidos") || ""
        }`,
    },
    { field: "documento", sortable: true, headerName: "DNI", width: 150 },
    {
      field: "telefono",
      headerName: "Telefono",
      width: 150,
      sortable: true,
    },
    {
      field: "correo",
      headerName: "Email",
      width: 150,
      sortable: true,
    },

    {
      field: "contraseña",
      headerName: "Contraseña",
      width: 150,
    },
    {
      field: "usuario",
      headerName: "Usuario",
      width: 150,
      sortable: true,
    },
    {
      field: "id",
      headerName: "acciones",
      sortable: false,
      width: 150,
      renderCell: (params) => {
        return (
          <DeleteOutlinedIcon
            style={{ cursor: "pointer" }}
            className="color-red"
            onClick={() => props.deleteJunior(params.value)}
          />
        );
      },
    },
  ];

  return (
    <div style={{ width: "100%" }}>
      <Typography
        className="p-2"
        style={{ background: "#0d4674", color: "white" }}
        component="h2"
        variant="h6"
        gutterBottom
      >
       
        {props.data.length > 0 ? `Tiene ${props.data.length} Junior resgistrados` : ""}
      </Typography>
      {props.data ? (
        <DataGrid autoHeight rows={props.data} columns={columns} pageSize={5} />
      ) : (
        <LinearIndeterminate />
      )}
    </div>
  );
}

import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

import { makeStyles } from "@material-ui/core/styles";
import BackspaceIcon from '@material-ui/icons/Backspace';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" in={props.open} ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
    button: {
      margin: theme.spacing(1),
    },
    TextField: {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));


export default function ModalConfirmDelete({ handleModal, open,handleDelete,id,type }) {
  const classes = useStyles();


  return (
    <div>
      {" "}
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {`Eliminar ${type}`}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            ¿Confirma que deseas eliminar {type}?  
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleModal} color="primary">
            Cancelar
          </Button>
          <Button
           
            variant="contained"
            color="primary"
            size="small"
            onClick={() => handleDelete(id)}
            className={classes.button}
            startIcon={<BackspaceIcon />}
          >
            Confirmar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import axios from "axios";
import Nav from "../Nav/NavImportadores";
import CampaingList from "./campaingList";
import Alert from "Util/dialogAlert";
import Button from "@material-ui/core/Button";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import clsx from "clsx";
import { useGetCampaigns } from "Hooks/useAbonos";
import Spinner from "Components/spinner";
const Orders = React.lazy(() => import("./tableRow"));
const JuniorsListPedidos = React.lazy(() => import("./JuniorsListPedidos"));
const ResumenAbonosListPedidos = React.lazy(() =>
  import("../verAbonos/Deposits")
);
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  title: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(0),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: "auto",
  },
  button: {
    margin: theme.spacing(1),
  },
}));

export default function PedidosJunior(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [alert, toggleAlert] = useState(false);
  const [selectJuniorToggle, setSelectJuniorToggle] = useState(true);
  const [juniorList, setJuniorList] = useState([]);
  const [selectedJunior, setSelectedJunior] = useState([]);
  const [AbonosRestante, setAbonosRestante] = useState([]);
  const [idSelected, setIdSelected] = useState("");
  const [Campaign, setCampaign] = useState("");
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const [toggleCampaignsSelected, setToggleCampaignsSelected] = useState(false);
  toggleCampaignsSelected;
  const [alertData, setAlertData] = useState({
    message: "El Junior Seleccionado, no posee pedidos actualmente.",
    type: "error",
  });

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const getJuniorOrdersUpdate = async (e) => {
    await axios
      .get(`https://point.qreport.site/rep/junior/${e}/${Campaign.id_campana}`)
      .then((res) => {
        if (res.data.length > 0) {
          setSelectedJunior(res.data);
          setSelectJuniorToggle(false);
        } else {
          setSelectJuniorToggle(false);
          toggleAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getJuniorOrders = async (e) => {
    await axios
      .get(
        `https://point.qreport.site/rep/junior/${props.userSeniorData[0].id}/${e}`
      )
      .then((res) => {
        if (res.data.length > 0) {
          setSelectedJunior(res.data);
          setToggleCampaignsSelected(true);
          setSelectJuniorToggle(false);
        } else {
          setSelectJuniorToggle(false);
          toggleAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getJuniorList = async (e) => {
    await axios
      .get(
        `https://point.qreport.site/rep/campana/junior_with/${e}/${props.userSeniorData[0].id}`
      )
      .then((res) => {
        setJuniorList(res.data);
        setToggleCampaignsSelected(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAbonosRestante = async (e) => {
    await axios
      .get(
        `https://point.qreport.site/rep/campana/senior_with/s/${props.userSeniorData[0].id}`
      )
      .then((res) => {
        if (res.data.length > 0) {
          const abonos = res.data.filter((item) => {
            if (item.id_campana == e) {
              return item;
            }
          });
          setAbonosRestante(abonos[0]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getJuniorByCampaing = async (e) => {
    if (props.userSeniorData.length === 1) {
      getJuniorOrders(e);
    } else {
      getJuniorList(e);
      getAbonosRestante(e);
    }
  };

  const selectJunior = async (e) => {
    await axios
      .get(`https://point.qreport.site/rep/junior/${e}/${Campaign.id_campana}`)
      .then((res) => {
        const data = res.data;
        if (data.length > 0) {
          setIdSelected(e);
          setSelectedJunior(data);
          setSelectJuniorToggle(false);
        } else {
          toggleAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function updateData() {
    if (props.userSeniorData.length == 1) {
      getJuniorOrders(Campaign.id_campana);
    } else {
      getJuniorOrdersUpdate(idSelected);
    }
  }

  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }
  }, []);

  const { loadingCampaigns, Campaigns } = useGetCampaigns({
    idSenior: props.userSeniorData[0].id,
    tipo: props.userSeniorData.length,
  });

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav
        userSeniorData={props.userSeniorData}
        handleLogout={props.handleLogout}
        handleDrawerOpen={handleDrawerOpen}
        open={open}
      />

      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Ver Pedidos
        </Typography>
        <Container maxWidth="xxl" className={classes.container}>
          {toggleCampaignsSelected ? (
            <Grid container spacing={3}>
              {!selectJuniorToggle && props.userSeniorData.length > 1 && (
                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<ArrowBackIosIcon />}
                    onClick={() => setSelectJuniorToggle(true)}
                  >
                    Volver
                  </Button>
                </Grid>
              )}
              {selectJuniorToggle && (
                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<ArrowBackIosIcon />}
                    onClick={() => setToggleCampaignsSelected(false)}
                  >
                    Volver
                  </Button>
                </Grid>
              )}

              {Campaigns.length > 0 && (
                <Grid align="left" item xs={12}>
                  <Typography
                    color="secondary"
                    component="p"
                    variant="subtitle2"
                  >
                    {Campaign.description}
                  </Typography>
                </Grid>
              )}

              {selectJuniorToggle && (
                <Grid
                  item
                  md={
                    props.userSeniorData.length > 1 &&
                    AbonosRestante &&
                    AbonosRestante.abono > 0
                      ? 8
                      : 12
                  }
                  xs={12}
                  sm={12}
                >
                  <Paper className={fixedHeightPaper}>
                    <JuniorsListPedidos
                      data={juniorList}
                      selectJunior={selectJunior}
                      seniorID={props.userSeniorData[0].id}
                      AbonosRestante={AbonosRestante}
                    />
                  </Paper>
                </Grid>
              )}

              {selectJuniorToggle &&
                props.userSeniorData.length > 1 &&
                AbonosRestante &&
                AbonosRestante.abono > 0 && (
                  <Grid
                    item
                    md={props.userSeniorData.length > 1 ? 4 : 12}
                    xs={12}
                    sm={12}
                  >
                    <Paper className={fixedHeightPaper}>
                      <ResumenAbonosListPedidos total={AbonosRestante} />
                    </Paper>
                  </Grid>
                )}

              {!selectJuniorToggle && (
                <Grid item xs={12}>
                  <Paper className={fixedHeightPaper}>
                    <Orders
                      setIdSelected={setIdSelected}
                      updateData={updateData}
                      setSelectedJunior={setSelectedJunior}
                      selectedJunior={selectedJunior}
                    />
                  </Paper>
                </Grid>
              )}
            </Grid>
          ) : (
            <Grid container spacing={4}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography component="h2" color="primary" variant="h6">
                  {Campaigns.length > 0
                    ? "Seleccione una campaña"
                    : "No tiene pedidos"}
                </Typography>
              </Grid>
              {Campaigns.map((card) => (
                <Grid item key={card.id_campana} xs={12} sm={6} md={4}>
                  <CampaingList
                    data={card}
                    getJuniorByCampaing={getJuniorByCampaing}
                    setCampaign={setCampaign}
                  />
                </Grid>
              ))}

              {loadingCampaigns && (
                <Grid item xs={12} sm={12} md={12}>
                  <Spinner />
                </Grid>
              )}
            </Grid>
          )}
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
        <Alert
          open={alert}
          setAlert={toggleAlert}
          type={alertData.type}
          message={alertData.message}
        />
      </main>
    </div>
  );
}

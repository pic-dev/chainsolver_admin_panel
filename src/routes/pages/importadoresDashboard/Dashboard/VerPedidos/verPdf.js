import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { isMovil } from "Util/Utils";
const PdfMovil = React.lazy(() =>
  import("Routes/pages/seccionImportadores/pdfMovil.js")
);
export default function verPdf({ img,pag, open, handleClose }) {

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="Catalogo"
        aria-describedby="Catalogo"
      >
        <DialogTitle id="Catalogo">{"Catalogo"}</DialogTitle>
        <DialogContent>
          {!isMovil() ? (
            <iframe
              style={{
                height: isMovil() ? "30rem" : "50rem",
                width: "34rem",
              }}
              src={`https://point.qreport.site/files/${img}#page=${pag}&zoom=110&navpanes=0&scrollbar=0&view=Fit`}
            />
          ) : (
            <PdfMovil
              style={{
                height: "30rem",
                width: "100%",
              }}
              pag={pag}
              url={`https://point.qreport.site/files/${img}`}
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

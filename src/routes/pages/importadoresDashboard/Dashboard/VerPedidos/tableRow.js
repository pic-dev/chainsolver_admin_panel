import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import LockIcon from "@material-ui/icons/Lock";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ModalEdit from "./modalEditOrder";
import ModalConfirmDelete from "../modalConfirmDelete";
import axios from "axios";
import Alert from "Util/dialogAlert";
import Tooltip from "@material-ui/core/Tooltip";
import Spinner from "Components/spinner";
import { isMovil } from "Util/Utils";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { green, red } from "@material-ui/core/colors";
import Pdf from "./verPdf";
import _ from "lodash";

const useRowStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > *": {
      borderBottom: "unset",
    },
  },
  loading: {
    width: "100%",
  },
  box: {
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
    padding: 0,
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    flexBasis: "100%",
    flexShrink: 0,
    textAlign: "left",
    width: "9rem",
    padding: 0,
  },
}));
function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.productos
    .map(({ subtotal }) => subtotal)
    .reduce((sum, i) => sum + i, 0);
}

function subtotalOrden(items) {
  return items.map(({ subtotal }) => subtotal).reduce((sum, i) => sum + i, 0);
}

function statusItem(items) {
  return items.productos.map(({ status }) => status);
}

function filterDelete(items, id) {
  return items.filter((item) => {
    if (item.id_orden != id) {
      return item;
    }
  });
}

function Row(props) {
  const { row, updateData, ordenesData } = props;
  const [open, setOpen] = React.useState(false);
  const [url, setUrl] = useState("");
  const [pag, setPag] = useState("");
  const [openPdf, setOpenPdf] = useState(false);
  const [idDelete, setIdDelete] = React.useState("");
  const [ordenEdit, setOrdenEdit] = React.useState([]);
  const [openModalDelete, setOpenModalDelete] = React.useState(false);
  const [openModalEdit, setOpenModalEdit] = React.useState(false);
  const [alertData, setAlertData] = useState({});
  const [alert, setAlert] = useState(false);
  const classes = useRowStyles();
  const invoiceTotal = subtotal(row);
  const status = statusItem(row);

  const handleModalEdit = (e) => {
    setOrdenEdit(e);
    setOpenModalEdit(!openModalEdit);
  };
  const handleModalDelete = (e) => {
    setIdDelete(e);
    setOpenModalDelete(!openModalDelete);
  };
  const handleEdit = (e) => {
    axios
      .delete(`https://point.qreport.site/productos/orden/${e[0].id_orden}`)
      .then((res) => {
        if (res.status == 200) {
          e.map((item, index) => {
            axios
              .post("https://point.qreport.site/productos", {
                cliente: Number(item.id_cliente),
                catalogo: Number(item.id_catalogo),
                orden: Number(item.id_orden),
                codigo_producto: item.codigo_producto,
                pagina: Number(item.pagina),
                producto: item.producto,
                cantidad: Number(item.cantidad),
                precio: item.precio > 0 ? item.precio : 0,
                observacion: item.observacion,
              })
              .then((res) => {
                if (res.data.boolean && e.length == index + 1) {
                  toggleAlert(1);
                  handleModalEdit([]);
                  updateData();
                }
              })
              .catch((error) => {
                console.log(error);
              });
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleDelete = (e) => {
    axios
      .delete(`https://point.qreport.site/productos/orden/${e}`)
      .then((res) => {
        if (res.status == 200) {
          handleModalDelete("");
          toggleAlert(2);
          props.setSelectedJunior(filterDelete(ordenesData, e));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const toggleAlert = (e) => {
    switch (e) {
      case 1:
        setAlertData({
          message: "Orden modificada exitosamente",
          type: "success",
        });
        setAlert(true);
        break;
      case 2:
        setAlertData({
          message: "Orden Eliminada exitosamente",
          type: "error",
        });
        setAlert(true);

        break;
      default:
        break;
    }
  };

  const handleClickOpen = (direccion, pagina) => {
    setUrl(direccion);
    setPag(pagina);
    setOpenPdf(true);
  };

  const handleClose = () => {
    setUrl("");
    setPag("");
    setOpenPdf(false);
  };

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <>
      {!isMovil() ? (
        <>
          <TableRow className={classes.root}>
            <TableCell
              onClick={() => setOpen(!open)}
              style={{ padding: 1 }}
              align="left"
            >
              <IconButton aria-label="expand row" size="small">
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
              <span style={{ fontWeight: "bold" }}>Orden:</span> {row.idOrden}
            </TableCell>
            <TableCell
              onClick={() => setOpen(!open)}
              style={{ padding: 1 }}
              align="left"
            >
              {" "}
              <span style={{ fontWeight: "bold" }}>Subtotal:</span>
              {" $ " + ccyFormat(invoiceTotal)}
            </TableCell>
            {status[0] == 1 ? (
              <TableCell style={{ padding: 1 }} align="right">
                {" "}
                <IconButton
                  aria-label="update row"
                  size="small"
                  onClick={() => handleModalEdit(row)}
                >
                  <EditIcon style={{ color: green[500] }} />
                </IconButton>
                <IconButton
                  aria-label="delete row"
                  size="small"
                  onClick={() => handleModalDelete(row.idOrden)}
                >
                  <DeleteIcon style={{ color: red[500] }} />
                </IconButton>
              </TableCell>
            ) : (
              <TableCell align="right">
                <Tooltip title="Pedido Cerrada">
                  <IconButton size="small" aria-label="closed">
                    <LockIcon style={{ color: red[500] }} />
                  </IconButton>
                </Tooltip>
              </TableCell>
            )}
          </TableRow>
          <TableRow>
            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
              <Collapse in={open} timeout="auto" unmountOnExit>
                <Box margin={1}>
                  <Typography variant="h6" gutterBottom component="div">
                    Productos
                  </Typography>
                  <Table size={"small"} aria-label="products">
                    <TableHead>
                      <TableRow>
                        <TableCell>Fecha</TableCell>
                        <TableCell>Catalogo</TableCell>
                        <TableCell>Producto</TableCell>
                        <TableCell>Pagina</TableCell>
                        <TableCell>Qty</TableCell>
                        <TableCell>Codigo</TableCell>
                        <TableCell>Precio Unitario</TableCell>
                        <TableCell align="right">Sub-Total</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {row.productos.map((historyRow) => (
                        <TableRow key={historyRow.id}>
                          <TableCell>
                            {historyRow.created_at.substr(0, 10)}
                          </TableCell>
                          <TableCell>{historyRow.catalogo}</TableCell>
                          <Tooltip title={historyRow.producto}>
                            <TableCell>{historyRow.producto}</TableCell>
                          </Tooltip>
                          <TableCell>
                            {historyRow.pagina}{" "}
                            {historyRow.ruta_sp && (
                              <IconButton
                                size="small"
                                aria-label="open"
                                onClick={() =>
                                  handleClickOpen(
                                    historyRow.ruta_cp.length > 0
                                      ? historyRow.ruta_cp
                                      : historyRow.ruta_sp,
                                    historyRow.pagina
                                  )
                                }
                              >
                                <VisibilityIcon />
                              </IconButton>
                            )}
                          </TableCell>
                          <TableCell>{historyRow.cantidad}</TableCell>
                          <TableCell>{historyRow.codigo_producto}</TableCell>
                          <TableCell>
                            {historyRow.precio > 0
                              ? "$ " + ccyFormat(historyRow.precio)
                              : "-"}
                          </TableCell>
                          <TableCell align="right">
                            {historyRow.subtotal > 0
                              ? "$ " + ccyFormat(historyRow.subtotal)
                              : "-"}
                          </TableCell>
                        </TableRow>
                      ))}
                      {invoiceTotal > 0 && (
                        <TableRow>
                          <TableCell className="arial text-center" colSpan={6}>
                            Total
                          </TableCell>
                          <TableCell className="arial" align="right">
                            {"$ " + ccyFormat(invoiceTotal)}
                          </TableCell>
                        </TableRow>
                      )}

                      <TableRow>
                        <TableCell
                          className="text-center color-red"
                          colSpan={7}
                        >
                          *p/c - Precio por Confirmar
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </Box>
              </Collapse>
            </TableCell>
          </TableRow>
        </>
      ) : (
        <>
          <Accordion
            expanded={expanded === `panel${row.idOrden}`}
            onChange={handleChange(`panel${row.idOrden}`)}
            className={classes.root}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls={`panel1bh${row.idOrden}`}
              id={`panel${row.idOrden}`}
            >
              {" "}
              <TableRow style={{ fontStyle: "bold" }}>
                <TableCell className={classes.heading} align="left">
                  Orden: {row.idOrden} <br /> Subtotal:
                  {" $ " + ccyFormat(invoiceTotal)}
                </TableCell>
                {status[0] == 1 ? (
                  <TableCell
                    style={{ padding: 1, textAlign: "right" }}
                    align="right"
                  >
                    {" "}
                    <IconButton
                      aria-label="update row"
                      size="small"
                      onClick={() => handleModalEdit(row)}
                    >
                      <EditIcon style={{ color: green[500] }} />
                    </IconButton>
                    <IconButton
                      aria-label="delete row"
                      size="small"
                      onClick={() => handleModalDelete(row.idOrden)}
                    >
                      <DeleteIcon style={{ color: red[500] }} />
                    </IconButton>
                  </TableCell>
                ) : (
                  <TableCell align="right">
                    <Tooltip title="Pedido Cerrada">
                      <IconButton
                        color="danger"
                        size="small"
                        aria-label="closed"
                      >
                        <LockIcon style={{ color: red[500] }} />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                )}
              </TableRow>
            </AccordionSummary>

            {row.productos.map((historyRow) => (
              <AccordionDetails className={classes.box}>
                <Table size={"small"} aria-label="products">
                  <TableBody>
                    <TableRow style={{ fontStyle: "bold" }}>
                      <TableCell>Producto</TableCell>
                      <TableCell>Pagina</TableCell>
                      <TableCell>Qty</TableCell>
                    </TableRow>
                    <TableRow key={historyRow.id}>
                      <Tooltip title={historyRow.producto}>
                        <TableCell
                          style={{
                            fontWeight: "300",
                            textTransform: "uppercase",
                          }}
                        >
                          {historyRow.producto.substr(0, 25)}
                        </TableCell>
                      </Tooltip>
                      <TableCell style={{ fontWeight: "300" }}>
                        {historyRow.pagina}{" "}
                        {historyRow.ruta_sp && (
                          <IconButton
                            size="small"
                            aria-label="open"
                            onClick={() =>
                              handleClickOpen(
                                historyRow.ruta_cp.length > 0
                                  ? historyRow.ruta_cp
                                  : historyRow.ruta_sp,
                                historyRow.pagina
                              )
                            }
                          >
                            <VisibilityIcon />
                          </IconButton>
                        )}
                      </TableCell>
                      <TableCell style={{ fontWeight: "300" }}>
                        {historyRow.cantidad}
                      </TableCell>
                    </TableRow>
                    <TableRow style={{ fontStyle: "bold" }}>
                      <TableCell>Codigo</TableCell>
                      <TableCell>P/U</TableCell>
                      <TableCell>Monto</TableCell>
                    </TableRow>
                    <TableRow key={historyRow.id}>
                      <TableCell style={{ fontWeight: "300" }}>
                        {historyRow.codigo_producto}
                      </TableCell>
                      <TableCell className="p-1" style={{ fontWeight: "300" }}>
                        {" "}
                        {historyRow.precio > 0
                          ? "$ " + historyRow.precio
                          : "p/c"}
                      </TableCell>
                      <TableCell className="p-1" style={{ fontWeight: "300" }}>
                        {historyRow.subtotal > 0
                          ? "$ " + ccyFormat(historyRow.subtotal)
                          : "-"}
                      </TableCell>
                    </TableRow>
                    {historyRow.precio == 0 && (
                      <TableRow>
                        <TableCell
                          className="text-center color-red"
                          colSpan={4}
                        >
                          *p/c - Precio por Confirmar
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </AccordionDetails>
            ))}
          </Accordion>
        </>
      )}

      <ModalConfirmDelete
        id={idDelete}
        handleDelete={handleDelete}
        open={openModalDelete}
        handleModal={handleModalDelete}
        type={"Orden"}
      />
      {openModalEdit && (
        <ModalEdit
          orden={ordenEdit}
          handleEdit={handleEdit}
          open={openModalEdit}
          handleModal={handleModalEdit}
        />
      )}
      <Pdf img={url} pag={pag} open={openPdf} handleClose={handleClose} />
      <Alert
        open={alert}
        setAlert={setAlert}
        type={alertData.type}
        message={alertData.message}
      />
    </>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    productos: PropTypes.arrayOf(
      PropTypes.shape({
        precio: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired,
        created_at: PropTypes.string.isRequired,
        catalogo: PropTypes.string.isRequired,
        pagina: PropTypes.number.isRequired,
        cantidad: PropTypes.number.isRequired,
        codigo_producto: PropTypes.string.isRequired,
      })
    ).isRequired,
  }).isRequired,
};

export default function CollapsibleTable(props) {
  const [filteredData, setFilteredData] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  React.useEffect(() => {
    var rows = _.chain(props.selectedJunior)
      // Group the elements of Array based on `color` property
      .groupBy("id_orden")
      // `key` is group's name (color), `value` is the array of objects
      .map((value, key) => ({ idOrden: key, productos: value }))
      .value();

    setFilteredData(rows);
  }, [props.selectedJunior]);

  const handleChangePage = (newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div>
      {props.selectedJunior.length > 0 && filteredData.length > 0 ? (
        <>
          <TableContainer component={Paper}>
            <Table size={"small"} aria-label="collapsible table">
              <TableHead>
                <TableRow>
                  <TableCell className="text-center p-0" colSpan={4}>
                    <Typography
                      className="p-2"
                      style={{ background: "#0d4674", color: "white" }}
                      component="h2"
                      variant="h6"
                      gutterBottom
                    >
                      {filteredData.length}{" "}
                      {filteredData.length == 1 ? " Orden de " : " Ordenes de "}
                      {filteredData[0].productos[0].junior}
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredData
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <Row
                        key={"orden" + row.idOrden}
                        updateData={props.updateData}
                        setSelectedJunior={props.setSelectedJunior}
                        row={row}
                        ordenesData={props.selectedJunior}
                      />
                    );
                  })}
              </TableBody>
              <TableHead>
                {isMovil() ? (
                  <TableRow>
                    <TableCell className=" p-0">
                      <Typography className="p-2" component="h6" variant="h6">
                        TOTAL:
                        <span style={{ color: "red" }}>
                          {" "}
                          {ccyFormat(subtotalOrden(props.selectedJunior))}{" "}
                        </span>
                      </Typography>
                    </TableCell>
                  </TableRow>
                ) : (
                  <TableRow>
                    <TableCell className=" p-0">
                      <Typography
                        className="p-2"
                        component="h2"
                        variant="h6"
                        gutterBottom
                      >
                        TOTAL
                      </Typography>
                    </TableCell>
                    <TableCell className=" p-0">
                      <Typography
                        className="p-2"
                        style={{ color: "red" }}
                        component="h2"
                        variant="h6"
                        gutterBottom
                      >
                        {ccyFormat(subtotalOrden(props.selectedJunior))}
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </TableHead>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={filteredData.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </>
      ) : (
        <Spinner />
      )}
    </div>
  );
}

import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { green, red } from "@material-ui/core/colors";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import { isMovil } from "Util/Utils";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
  },
  cardMedia: {
    paddingTop: "80%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const ListStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: "grid",
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));
export default function CampaignList({
  data,
  getJuniorByCampaing,
  setCampaign,
}) {
  const classes = useStyles();
  const classesList = ListStyles();
  return (
    <>
      {isMovil() ? (
        <List className={classesList.root}>
          <ListItem
            onClick={() => {
              getJuniorByCampaing(data.id_campana);
              setCampaign(data);
            }}
            alignItems="flex-start"
          >
            <ListItemAvatar>
              <Avatar
                variant="square"
                alt={data.name}
                src={data.portada}
                className={classesList.large}
              />
            </ListItemAvatar>
            <ListItemText
              className="pl-2"
              primary={data.name}
              secondary={
                <React.Fragment>
                  <Typography
                    component="div"
                    variant="body1"
                    className={classesList.inline}
                  >
                    {data.description}
                  </Typography>
                  <Typography
                    component="div"
                    variant="body2"
                    className={classesList.inline}
                    style={{
                      color: data.statusCarga == 0 ? green[500] : red[500],
                    }}
                  >
                    {data.statusCarga == 0 ? "En transito" : "Cerrada"}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        </List>
      ) : (
        <div className={classes.root}>
          <Card
            key={data.name + "data"}
            onClick={() => {
              getJuniorByCampaing(data.id_campana);
              setCampaign(data);
            }}
            className={classes.card}
          >
            <CardMedia
              key={data.name + "img"}
              className={classes.cardMedia}
              image={data.portada}
              title={data.name}
            />
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" component="h5">
                {data.description}
              </Typography>
              <Typography>{data.description}</Typography>
              <Typography
                style={{ color: data.statusCarga == 0 ? green[500] : red[500] }}
                gutterBottom
                variant="h6"
                component="h6"
              >
                {data.statusCarga == 0 ? "En transito" : "Cerrada"}
              </Typography>
            </CardContent>
          </Card>
        </div>
      )}
    </>
  );
}

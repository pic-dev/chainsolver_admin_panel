import React from "react";
import { DataGrid } from "@material-ui/data-grid";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
}));

function LinearIndeterminate() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Skeleton animation="wave" />
      <Typography component="h2" variant="h5">
        No posees ordenes con abonos actualmente
      </Typography>
      <Skeleton animation="wave" />
    </div>
  );
}

export default function DataTable(props) {
  const columns = [
    {
      field: "precio_compra",
      headerName: "P.Unitario",
      sortable: true,
      width: screen.width <= 769 ? 120 : 150,
    },
    {
      field: "cantidad_bulto",
      headerName: "Qty * Bulto",
      width: screen.width <= 769 ? 120 : 150,
      sortable: true,
    },

    {
      field: "total_inversion",
      headerName: "Total Invers * Bulto",
      width: screen.width <= 769 ? 120 : 150,
      sortable: true,
    },
    {
      field: "cantidad_bulto",
      headerName: "Total Uni / Junior",
      width: screen.width <= 769 ? 120 : 150,
      sortable: true,
    },
    {
      field: "cantidad_bulto",
      headerName: "Faltante * Bulto",
      width: screen.width <= 769 ? 120 : 150,
      sortable: true,
    },
  ];

  return (
    <>
      <div className="arial mb-3">
        Resumen de Articulos
      </div>
      {props.data && (
        <DataGrid
          rows={props.data}
          columns={columns}
          pageSize={5}
          autoHeight
          autoPageSize
          disableMultipleSelection={true}
        />
      )}
    </>
  );
}

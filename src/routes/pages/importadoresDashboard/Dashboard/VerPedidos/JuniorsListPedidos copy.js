import React from "react";
import { DataGrid } from "@material-ui/data-grid";
import Typography from "@material-ui/core/Typography";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import IconButton from "@material-ui/core/IconButton";
const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
  },
}));

function LinearIndeterminate() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Skeleton animation="wave" />
      <Typography component="h2" variant="h5">
        No tiene Juniors Registrados Actualmente
      </Typography>
      <Skeleton animation="wave" />
    </div>
  );
}

export default function DataTable(props) {
  const columns = [
    {
      field: "junior",
      headerName: "Nombre",
      sortable: true,
      flex: 1,
      width: screen.width <= 769 ? 120 : 200,
    },
    {
      field: "subtotal",
      headerName: "subtotal",
      flex: 0.8,
      width:
        screen.width <= 769
          ? 120
          : props.AbonosRestante && props.AbonosRestante.abono > 0
          ? 450
          : 910,
      sortable: true,
      renderCell: (params) => {
        return <span>{"$" + params.value}</span>;
      },
    },
    {
      field: "id",
      headerName: "Ver",
      flex: 0.4,
      sortable: false,
      width: screen.width <= 769 ? 80 : 120,
      renderCell: (params) => {
        return (
          <IconButton
            aria-label="open"
            size="small"
            onClick={() => props.selectJunior(params.value)}
          >
            <VisibilityIcon style={{ color: green[500] }} />
          </IconButton>
        );
      },
    },
  ];
console.log(props.AbonosRestante && props.AbonosRestante.abono > 0);
  return (
    <>

      <Typography className="p-2" style={{background:"#0d4674", color:"white"}} component="h2" variant="h6"  gutterBottom>
      Seleccione un Junior
      </Typography>
      {props.data ? (
        <DataGrid
          rows={props.data}
          columns={columns}
          pageSize={5}
          autoHeight
          autoPageSize
          disableMultipleSelection={true}
          onSelectionChange={(newSelection) => {
            props.selectJunior(newSelection.rowIds[0]);
          }}
        />
      ) : (
        <LinearIndeterminate />
      )}
    </>
  );
}

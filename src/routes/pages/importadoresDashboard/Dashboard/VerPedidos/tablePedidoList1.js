import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { green, red } from "@material-ui/core/colors";
import Skeleton from "@material-ui/lab/Skeleton";
import AddIcon from "@material-ui/icons/Add";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
  loading: {
    width: "100%",
  },
});

function LinearIndeterminate() {
  const classes = useRowStyles();

  return (
    <div className={classes.loading}>
      <Skeleton animation="wave" />

      <Typography component="h2" variant="h5">
        No tiene pedidos cargados actualmente
      </Typography>
      <Skeleton animation="wave" />
    </div>
  );
}

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.map(({ subtotal }) => subtotal).reduce((sum, i) => sum + i, 0);
}

function Row(props) {
  const { row, edit, Delete } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell
          style={{ fontWeight: "bold" }}
          onClick={() => setOpen(!open)}
        >
          {" "}
          <IconButton className="mr-2" aria-label="expand row" size="small">
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell
          style={{ fontWeight: "bold" }}
          onClick={() => setOpen(!open)}
          colSpan={2}
        >
          {row.producto}
        </TableCell>
        <TableCell style={{ padding: 1 }}>
          <IconButton
            aria-label="update row"
            size="small"
            onClick={() => edit(row)}
          >
            <EditIcon style={{ color: green[500] }} />
          </IconButton>
          <IconButton
            aria-label="delete row"
            size="small"
            onClick={() => Delete(row.id)}
          >
            <DeleteIcon style={{ color: red[500] }} />
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow className={classes.root}>
        <TableCell onClick={() => setOpen(!open)}>Qty</TableCell>
        <TableCell onClick={() => setOpen(!open)}>P/U</TableCell>
        <TableCell onClick={() => setOpen(!open)}>Total</TableCell>
      </TableRow>
      <TableRow className={classes.root}>
        <TableCell onClick={() => setOpen(!open)}>{row.cantidad}</TableCell>
        <TableCell onClick={() => setOpen(!open)}>${row.precio} </TableCell>
        <TableCell onClick={() => setOpen(!open)}>
          {row.precio > 0
            ? "$" + ccyFormat(priceRow(row.precio, row.cantidad))
            : "-"}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Table size={"small"} aria-label="products">
                <TableBody>
                  <TableRow style={{ fontStyle: "bold" }}>
                    <TableCell>Producto</TableCell>
                    <TableCell>Pagina</TableCell>
                    <TableCell>Cant</TableCell>
                  </TableRow>
                  <TableRow key={row.id}>
                    <TableCell style={{ fontWeight: "300" }}>
                      {row.producto}
                    </TableCell>
                    <TableCell style={{ fontWeight: "300" }}>
                      {row.pagina > 0 ? row.pagina : "p/c"}
                    </TableCell>
                    <TableCell style={{ fontWeight: "300" }}>
                      {row.cantidad}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ fontStyle: "bold" }}>
                    <TableCell>Codigo</TableCell>
                    <TableCell>P/U</TableCell>
                    <TableCell>Total</TableCell>
                  </TableRow>
                  <TableRow key={row.id}>
                    <TableCell style={{ fontWeight: "300" }}>
                      {row.codigo_producto}
                    </TableCell>
                    <TableCell className="p-1" style={{ fontWeight: "300" }}>
                      {" "}
                      {row.precio > 0 ? "$ " + row.precio : "p/c"}
                    </TableCell>
                    <TableCell className="p-1" style={{ fontWeight: "300" }}>
                      {row.precio > 0
                        ? "$ " + ccyFormat(priceRow(row.precio, row.cantidad))
                        : "-"}
                    </TableCell>
                  </TableRow>
                  {row.precio == 0 && (
                    <TableRow>
                      <TableCell className="text-center color-red" colSpan={4}>
                        *p/c - Precio por Confirmar
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    productos: PropTypes.arrayOf(
      PropTypes.shape({
        precio: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired,
        created_at: PropTypes.string.isRequired,
        catalogo: PropTypes.string.isRequired,
        pagina: PropTypes.number.isRequired,
        cantidad: PropTypes.number.isRequired,
        codigo_producto: PropTypes.string.isRequired,
      })
    ).isRequired,
  }).isRequired,
};

export default function CollapsibleTable(props) {
  const classes = useRowStyles();

  const [filteredData, setFilteredData] = useState([]);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  React.useEffect(() => {
    setFilteredData(props.productos);
  }, [props.productos]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const invoiceTotal = subtotal(props.productos);
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div>
      {props.productos.length > 0 && filteredData.length > 0 ? (
        <Paper>
          <TableContainer component={Paper}>
            <Table size={"small"} aria-label="collapsible table">
              <TableHead>
                <TableRow>
                  <TableCell
                    className={classes.TableHead}
                    align="center"
                    colSpan={3}
                  >
                    Pre-Orden seleccionada
                  </TableCell>
                  <TableCell
                    onClick={() => props.new()}
                    style={{
                      fontSize: "0.8rem",
                      cursor: "pointer",
                    }}
                    className={classes.TableHead}
                    align="center"
                  >
                    <AddIcon /> Agregar
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredData
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <Row
                        key={"orden" + row.id}
                        row={row}
                        edit={props.edit}
                        Delete={props.delete}
                      />
                    );
                  })}

                {invoiceTotal > 0 && (
                  <TableRow>
                    <TableCell className="arial text-center" colSpan={2}>
                      Total
                    </TableCell>
                    <TableCell
                      style={{ color: "red" }}
                      className="arial text-center"
                    >
                      {ccyFormat(invoiceTotal)}
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={filteredData.length}
            rowsPerPage={rowsPerPage}
            page={page}
            labelRowsPerPage="Filas"
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      ) : (
        <LinearIndeterminate />
      )}
    </div>
  );
}

import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import SaveIcon from "@material-ui/icons/Save";
import Grow from "@material-ui/core/Grow";
import Grid from "@material-ui/core/Grid";
import { isMovil } from "Util/Utils";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from '@material-ui/core/MenuItem';
import TextField from "@material-ui/core/TextField";
import CloseIcon from "@material-ui/icons/Close";
import ModalConfirmDelete from "../modalConfirmDelete";
import axios from "axios";
const TablePedido = React.lazy(() => import("./tablePedidoList"));
const TablePedido1 = React.lazy(() => import("./tablePedidoList1"));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" in={props.open} ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
    TextField: {
      margin: theme.spacing(1),
      width: "25ch",
    },
    appBar: {
      position: "relative",
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    buttons: {
      display: "flex",
      justifyContent: "flex-end",
    },
    button: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(1),
    },
  },
}));

export default function ModalEdit({ handleModal, handleEdit, open, orden }) {
  const classes = useStyles();

  const [openEditForm, setOpenEditForm] = React.useState(false);
  const [New, setNew] = React.useState(false);
  const [opentable, setOpenTable] = React.useState(false);
  const [selectedOrden, setSelectedOrden] = React.useState([]);
  const [catalogosDisponibles, setCatalogosDisponibles] = React.useState([]);
  const [selectedEdit, setSelectedEdit] = React.useState([]);
  const [openModalDelete, setOpenModalDelete] = React.useState(false);
  const [idDelete, setIdDelete] = React.useState("");

  useEffect(() => {
    setSelectedOrden(orden.productos);
    setOpenTable(!opentable);
    getCatalogos();
  }, []);

  const handleSelected = (event) => {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setSelectedEdit(event);
    document.getElementById("Pagina").value = event.pagina;
    document.getElementById("Producto").value = event.producto;
    document.getElementById("CodigoProducto").value = event.codigo_producto;
    document.getElementById("CantidadProducto").value = event.cantidad;
    document.getElementById("precio").value = event.precio;
  };

  const handleNew = () => {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setNew(true);
  };

  const handleDelete = (id) => {
    let data = selectedOrden.filter((item) => {
      if (item.id !== id) {
        return item;
      }
    });
    if (data.length > 0) {
      setSelectedOrden(data);
      setOpenModalDelete(!openModalDelete);
    }
  };

  const getCatalogos  = async () => {

     await axios
      .get(`https://point.qreport.site/catalogos`)
      .then((res) => {
           setCatalogosDisponibles(res.data)
          })
      .catch((error) => {
        console.log(error);
      });
  }

  const handleModalDelete = (e) => {
    setIdDelete(e);
    setOpenModalDelete(!openModalDelete);
  };

  const handleSubmit = async (e) => {


    e.preventDefault();
    const {
      Pagina,
      Producto,
      CodigoProducto,
      CantidadProducto,
      precio,
      catalogo
    } = e.target.elements;
    setNew(false);
    var newItem = {
      id_cliente: Number(selectedOrden[0].id_cliente),
      id_catalogo: catalogo ? Number(catalogo.value) : Number(selectedEdit.id_catalogo),
      pagina: Number(Pagina.value),
      producto: Producto.value,
      codigo_producto: CodigoProducto.value ? CodigoProducto.value : "",
      cantidad: Number(CantidadProducto.value),
      precio: precio ? Number(precio.value) : 0,
      observacion:selectedEdit.observacion? selectedEdit.observacion : "",
      subtotal: precio
        ? Number(precio.value) * Number(CantidadProducto.value)
        : 0,
      id: selectedEdit.id ? selectedEdit.id : Date.now(),
      id_orden: selectedOrden[0].id_orden,
    };


    var data;
    var arreglo;
    if (selectedEdit) {
      data = selectedOrden.filter((item) => {
        if (item.id !== selectedEdit.id) {
          return item;
        }
      });
      arreglo = data.concat(newItem);
    } else {
      data = selectedOrden;
      arreglo = data.concat(newItem);
    }
    if (arreglo) {
      setSelectedOrden(arreglo);
      setOpenEditForm(!openEditForm);
      setOpenTable(!opentable);
      setSelectedEdit([]);
      document.getElementById("Pagina").value = "";
      document.getElementById("Producto").value = "";
      document.getElementById("CodigoProducto").value = "";
      document.getElementById("CantidadProducto").value = "";
      document.getElementById("precio").value = "";
    }
  };

  const handleCancel = () => {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setSelectedEdit([]);
    document.getElementById("Pagina").value = "";
    document.getElementById("Producto").value = "";
    document.getElementById("CodigoProducto").value = "";
    document.getElementById("CantidadProducto").value = "";
    document.getElementById("precio").value = "";
  };

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        fullWidth={true}
        fullScreen={isMovil()}
        maxWidth={"lg"}
        keepMounted
        onClose={handleModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        {isMovil() && (
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleModal}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>

              {!openEditForm && (
                <IconButton
                  edge="end"
                  color="inherit"
                  onClick={() => handleEdit(selectedOrden)}
                  aria-label="save"
                >
                  <SaveIcon />
                </IconButton>
              )}
            </Toolbar>
          </AppBar>
        )}
        <DialogTitle id="alert-dialog-slide-title">
          {"Actualizar orden"}
        </DialogTitle>
        <DialogContent>
          {selectedOrden && opentable&& !isMovil() && (
            <TablePedido
              delete={handleModalDelete}
              edit={handleSelected}
              new={handleNew}
              productos={selectedOrden}
            />
          )}

          {selectedOrden && opentable && isMovil() && (
            <TablePedido1
              delete={handleModalDelete}
              edit={handleSelected}
              new={handleNew}
              productos={selectedOrden}
            />
          )}

          <Grow in={openEditForm}>
            <form
              className={classes.form}
              style={{ paddingTop: ".5rem" }}
              onSubmit={handleSubmit}
              
            >
              <Grid container spacing={4}>
              {New && <Grid item xs={4} sm={2}>
                  <TextField
                    autoComplete="catalogo"
                    name="catalogo"
                    required
                    select
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="catalogo"
                    label="catalogo"
                    autoFocus
                    >
                    {catalogosDisponibles.map((option) => (
                      <MenuItem key={option.id} value={option.id}>
                        {option.name}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>}
                <Grid item xs={4} sm={2}>
                  <TextField
                    autoComplete="Pagina"
                    name="Pagina"
                    required
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="Pagina"
                    label="Pagina"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={4} sm={3}>
                  <TextField
                    autoComplete="Producto"
                    name="Producto"
                    required
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="Producto"
                    label="Producto"
                  />
                </Grid>
                <Grid item xs={4} sm={3}>
                  <TextField
                    fullWidth
                    id="CodigoProducto"
                    label="Codigo"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    name="CodigoProducto"
                    autoComplete="Codigo"
                  />
                </Grid>
                <Grid item xs={4} sm={2}>
                  <TextField
                    required
                    fullWidth
                    id="CantidadProducto"
                    label="Qty"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    name="CantidadProducto"
                    autoComplete="Cantidad"
                  />
                </Grid>

                <Grid item xs={4} sm={2}>
                  <TextField
                    fullWidth
                    id="precio"
                    label="precio"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    name="precio"
                    autoComplete="precio"
                  />
                </Grid>

                <Grid className={classes.buttons} item xs={12} sm={12}>
                  <Button onClick={handleCancel} className={classes.button}>
                    Cancelar
                  </Button>

                  <Button
                    type="submit"
                    color="primary"
                    className={classes.button}
                  >
                    <SaveIcon /> Guardar
                  </Button>
                </Grid>
              </Grid>{" "}
            </form>
          </Grow>
          {!openEditForm && !isMovil() && (
            <DialogActions>
              <Button onClick={handleModal} color="primary">
                Cancelar
              </Button>
              <Button onClick={() => handleEdit(selectedOrden)} color="primary">
                <SaveIcon /> Actualizar Orden
              </Button>
            </DialogActions>
          )}
        </DialogContent>
      </Dialog>
      <ModalConfirmDelete
        id={idDelete}
        handleDelete={handleDelete}
        open={openModalDelete}
        handleModal={handleModalDelete}
        type={"Producto"}
      />
    </div>
  );
}

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import { green,red } from '@material-ui/core/colors';
const useStyles = makeStyles({
  table: {
    minWidth: "auto",
    padding: 1,
  },
  TableHead: {
    padding: 1,
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.map(({ subtotal }) => subtotal).reduce((sum, i) => sum + i, 0);
}

export default function SpanningTable(props) {
  const classes = useStyles();
  const invoiceTotal = subtotal(props.productos);

  return (
    <TableContainer component={Paper}>
      <Table
        size={"small"}
        className={classes.table}
        aria-label="spanning table"
      >
        <TableHead className={classes.TableHead}>
          <TableRow>
            <TableCell className={classes.TableHead} align="center" colSpan={6}>
              Pre-Orden seleccionada
            </TableCell>
            <TableCell
              onClick={() => props.new()}
              style={{ fontSize: "0.8rem",  textAlign: "end",cursor:"pointer" }}
              className={classes.TableHead}
              align="center" 
            >
              <AddIcon /> Agregar
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Pagina</TableCell>
            <TableCell>Producto</TableCell>
            <TableCell>Codigo</TableCell>
            <TableCell>Qty.</TableCell>
            <TableCell>Precio Unitario</TableCell>
            <TableCell>Sub-Total</TableCell>
            <TableCell align="right">Acciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.productos.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.pagina}</TableCell>
              <TableCell>{row.producto}</TableCell>
              <TableCell>{row.codigo_producto}</TableCell>
              <TableCell>{row.cantidad}</TableCell>

              <TableCell
                className={`${row.precio > 0 ? "" : "color-red arial"}`}
              >
                {row.precio > 0 ?  "$ " + row.precio  : "P/C"}
              </TableCell>

              <TableCell>
                {row.precio > 0
                  ? "$ " + ccyFormat(priceRow(row.precio, row.cantidad))
                  : "-"}
              </TableCell>

              <TableCell align="right">
                <IconButton
                  aria-label="update row"
                  size="small"
                  onClick={() => props.edit(row)}
                >
             <EditIcon style={{ color: green[500] }}/>
                </IconButton>
                <IconButton
                  aria-label="delete row"
                  size="small"
                  onClick={() => props.delete(row.id)}
                >
                      <DeleteOutlinedIcon style={{ color: red[500] }} />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
          {invoiceTotal > 0 && (
            <TableRow>
              <TableCell className="arial text-center" colSpan={5}>
                Total
              </TableCell>
              <TableCell>{ccyFormat(invoiceTotal)}</TableCell>
            </TableRow>
          )}

          <TableRow>
            <TableCell className="text-center color-red" colSpan={7}>
              *p/c - Precio por Confirmar
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Nav from "../Nav/NavImportadores";
import { useAbonos, useAbonosTotal } from "Hooks/useAbonos";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import { isMovil } from "Util/Utils";
const AbonosTableList = React.lazy(() => import("./tableRowMovil"));
const AbonosTableListWeb = React.lazy(() => import("./tableRowWeb"));
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
    padding: theme.spacing(0),
  },
  paper: {
    padding: theme.spacing(0),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

export default function Dashboard(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);

  const [catalogosDisponibles, setCatalogosDisponibles] = React.useState("");
  const handleDrawerOpen = () => {
    setOpen(!open);
  };
  const handleChange = (e) => {
    setCatalogosDisponibles(e);
  };
  const id = props.userSeniorData[0].id;
  const { loading, abonos } = useAbonos({
    campaña: catalogosDisponibles ? catalogosDisponibles.id_campana : "",
    id,
  });

  const { abonosTotal } = useAbonosTotal({ id });

  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }
  }, []);
  useEffect(() => {
    if (props.history.location.search !== undefined && abonosTotal.length > 0) {
      let id = props.history.location.search.substr(1, 4);
      let campaña = abonosTotal.find((item) => item.id_campana == id);

      setCatalogosDisponibles(campaña);
    }
  }, [abonosTotal]);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav
        userSeniorData={props.userSeniorData}
        handleLogout={props.handleLogout}
        handleDrawerOpen={handleDrawerOpen}
        open={open}
      />
      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Ver Resumen de Abonos
        </Typography>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={4} lg={4}>
              <TextField
                autoComplete="catalogo"
                name="catalogo"
                required
                select
                onChange={(e) => handleChange(e.target.value)}
                fullWidth
                InputLabelProps={{
                  shrink: true,
                }}
                id="catalogo"
                label="Seleccione una campaña"
                autoFocus
              >
                {abonosTotal.map((option) => (
                  <MenuItem key={option.id_campana} value={option}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            <Grid item xs={12} md={12} lg={12}>
              <Paper className={classes.paper}>
                {isMovil() ? (
                  <AbonosTableList
                    catalogosDisponibles={catalogosDisponibles}
                    data={abonos}
                  />
                ) : (
                  <AbonosTableListWeb
                    catalogosDisponibles={catalogosDisponibles}
                    y
                    data={abonos}
                  />
                )}
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </main>
    </div>
  );
}

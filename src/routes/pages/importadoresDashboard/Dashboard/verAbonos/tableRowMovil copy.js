import React, { useState } from "react";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Comprobante from "./verComprobante";
import { green, red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import DoneIcon from "@material-ui/icons/Done";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import Tooltip from "@material-ui/core/Tooltip";
import Chip from "@material-ui/core/Chip";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import _ from "lodash";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
    },
  },
  loading: {
    width: "100%",
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.productos
    .map(({ subtotal }) => subtotal)
    .reduce((sum, i) => sum + i, 0);
}

function statusItem(items) {
  return items.productos.map(({ status }) => status);
}

function filterDelete(items, id) {
  return items.filter((item) => {
    if (item.id_orden != id) {
      return item;
    }
  });
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  const [openimg, setOpenimg] = React.useState(false);
  const [img, setImg] = React.useState("");

  const handleClickOpen = (e) => {
    setImg(e);
    setOpenimg(true);
  };

  const handleClose = () => {
    setImg("");
    setOpenimg(false);
  };

  return (
    <React.Fragment>
      <TableBody>
        <>
          <TableRow className={classes.root}>
            <TableCell
              style={{ fontWeight: "bold" }}
              onClick={() => setOpen(!open)}
              colSpan={3}
            >
              {" "}
              <IconButton aria-label="expand row" size="small">
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
              Monto: {row.monto} $
            </TableCell>

            {row.aprobado !== "Aprobado" ? (
              <TableCell style={{ padding: 1 }} align="right">
                <IconButton size="small" aria-label="closed">
                  <DoneIcon style={{ color: red[500] }} />
                </IconButton>
              </TableCell>
            ) : (
              <TableCell align="right">
                <Tooltip title="Pedido Cerrada">
                  <IconButton size="small" aria-label="closed">
                    <DoneAllIcon style={{ color: green[500] }} />
                  </IconButton>
                </Tooltip>
              </TableCell>
            )}
          </TableRow>

          <TableRow>
            <TableCell style={{ padding: 0 }} colSpan={6}>
              <Collapse in={open} timeout="auto" unmountOnExit>
                <Box margin={1}>
                  <Table size={"small"} aria-label="products">
                    <TableBody>
                      <TableRow>
                        <TableCell className="text-center">Banco</TableCell>
                        <TableCell
                          className="text-center"
                          style={{ fontWeight: "300" }}
                        >
                          {row.banco}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className="text-center">
                          Nro_transaccion
                        </TableCell>
                        <TableCell
                          className="text-center"
                          style={{ fontWeight: "300" }}
                        >
                          {row.nro_transaccion}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className="text-center">Aprobado</TableCell>
                        <TableCell
                          className="text-center"
                          style={{
                            fontWeight: "300",
                            color:
                              row.aprobado == "Aprobado"
                                ? green[500]
                                : red[500],
                          }}
                        >
                          {row.aprobado == "Aprobado"
                            ? "Confirmado Por PIC-Cargo"
                            : "Por Aprobar"}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className="text-center">
                          Comprobante
                        </TableCell>
                        <TableCell
                          className="text-center"
                          style={{ fontWeight: "300" }}
                        >
                          <IconButton
                            aria-label="open"
                            size="small"
                            onClick={() => handleClickOpen(row.deposito)}
                          >
                            <VisibilityIcon style={{ color: green[500] }} />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </Box>
              </Collapse>
            </TableCell>
          </TableRow>
        </>
      </TableBody>
      <Comprobante img={img} open={openimg} handleClose={handleClose} />
    </React.Fragment>
  );
}

export default function CollapsibleTable(props) {
  const classes = useRowStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div>
      <Paper>
        {props.data.length > 0 ? (
          <TableContainer component={Paper}>
            <Typography
              component="h5"
              variant="h5"
              color="primary"
              gutterBottom
            >
              Total Abonos
            </Typography>
            {Object.keys(props.catalogosDisponibles).length > 0 && (
              <div style={{ display: "flex" }} className="arial mb-3">
                <Chip
                  className="m-2"
                  avatar={
                    <MonetizationOnIcon
                      style={{ borderRadius: "50%", color: "white" }}
                    />
                  }
                  label={`Subtotal: ${props.catalogosDisponibles.subtotal}`}
                  color="secondary"
                />
                <Chip
                  className="m-2"
                  avatar={
                    <MonetizationOnIcon
                      style={{ borderRadius: "50%", color: "white" }}
                    />
                  }
                  label={`Abonos: ${props.catalogosDisponibles.abono}`}
                  color="secondary"
                />
                <Chip
                  className="m-2"
                  avatar={
                    <MonetizationOnIcon
                      style={{ borderRadius: "50%", color: "white" }}
                    />
                  }
                  label={`Resta:  ${props.catalogosDisponibles.resta}`}
                  color="secondary"
                />
              </div>
            )}
            <Typography
              component="h6"
              variant="h6"
              color="secondary"
              gutterBottom
            >
              Detalles
            </Typography>
            <Table size={"small"} aria-label="collapsible table">
              <TableHead>
                <TableRow>
                  <TableCell />
                </TableRow>
              </TableHead>
              {props.data.length == 0 && (
                <TableBody>
                  {" "}
                  <TableRow className={classes.root}>
                    <TableCell style={{ fontWeight: "bold" }} colSpan={4}>
                      {Object.keys(props.catalogosDisponibles).length > 0
                        ? "No tiene abonos actualmente"
                        : "Seleccione una Campaña"}
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}

              {props.data
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return <Row key={"orden" + row.id} row={row} />;
                })}
            </Table>
          </TableContainer>
        ) : (
          <TableContainer component={Paper}>
            <Typography
              component="h5"
              variant="h5"
              color="primary"
              gutterBottom
            >
              Total Abonos
            </Typography>
            <Table size={"small"} aria-label="collapsible table">
              <TableHead>
                <TableRow>
                  <TableCell />
                </TableRow>
              </TableHead>
              {props.data.length == 0 && (
                <TableBody>
                  {" "}
                  <TableRow className={classes.root}>
                    <TableCell style={{ fontWeight: "bold" }} colSpan={4}>
                      {Object.keys(props.catalogosDisponibles).length > 0
                        ? "No tiene abonos actualmente"
                        : "Seleccione una Campaña"}
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>
        )}
      </Paper>
    </div>
  );
}

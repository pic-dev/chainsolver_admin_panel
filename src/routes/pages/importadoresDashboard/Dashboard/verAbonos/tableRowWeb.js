import React, { useEffect, useState } from "react";
import { DataGrid } from "@material-ui/data-grid";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import Typography from "@material-ui/core/Typography";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Comprobante from "./verComprobante";
import Spinner from "Components/spinner";
import { green, red } from "@material-ui/core/colors";
import IconButton from "@material-ui/core/IconButton";
import Chip from "@material-ui/core/Chip";
function DataTable({ data, catalogosDisponibles }) {
  const columns = [
    {
      field: "fecha_corta",
      headerName: "Fecha",
      sortable: true,
      width: 150,
    },
    {
      field: "monto",
      headerName: "monto",
      sortable: true,
      width: 120,
      renderCell: (params) => {
        return <span>{`$ ${params.value}`}</span>;
      },
    },
    {
      field: "banco",
      headerName: "Banco",
      width: 200,
      sortable: true,
    },
    {
      field: "nro_transaccion",
      headerName: "nro_transaccion",
      width: 350,
      sortable: true,
    },
    {
      field: "aprobado",
      headerName: "status",
      sortable: false,
      width: 200,
      renderCell: (params) => {
        return (
          <span
            style={{
              color: params.value == "Aprobado" ? green[500] : red[500],
            }}
          >
            {params.value == "Aprobado"
              ? "Confirmado Por PIC-Cargo"
              : "Por Aprobar"}
          </span>
        );
      },
    },
    {
      field: "deposito",
      headerName: "comprobante",
      sortable: false,
      width: 150,
      renderCell: (params) => {
        return (
          <IconButton
            aria-label="open"
            size="small"
            onClick={() => handleClickOpen(params.value)}
          >
            <VisibilityIcon style={{ color: green[500] }} />
          </IconButton>
        );
      },
    },
  ];
  const [open, setOpen] = useState(false);
  const [abonos, setAbonos] = useState([]);
  const [img, setImg] = useState("");

  const handleClickOpen = (e) => {
    setImg(e);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setAbonos(data);
  }, [data, catalogosDisponibles]);
  return (
    <>
      {data.length > 0 ? (
        <div>
          <Typography
            className="p-2 mb-3"
            style={{ background: "#0d4674", color: "white" }}
            component="h2"
            variant="h6"
            gutterBottom
          >
            Total Abonos
          </Typography>
          <div style={{ display: "flex" }} className="arial mb-3">
            <Chip
              className="m-2"
              style={{ fontSize: "1.5rem", padding: "1.5rem" }}
              avatar={
                <MonetizationOnIcon
                  style={{ borderRadius: "50%", color: "white" }}
                />
              }
              label={`Subtotal: ${catalogosDisponibles.subtotal}`}
              color="secondary"
            />
            <Chip
              className="m-2"
              style={{ fontSize: "1.5rem", padding: "1.5rem" }}
              avatar={
                <MonetizationOnIcon
                  style={{ borderRadius: "50%", color: "white" }}
                />
              }
              label={`Abonos: ${catalogosDisponibles.abono}`}
              color="secondary"
            />
            <Chip
              className="m-2"
              style={{ fontSize: "1.5rem", padding: "1.5rem" }}
              avatar={
                <MonetizationOnIcon
                  style={{ borderRadius: "50%", color: "white" }}
                />
              }
              label={`Resta:  ${catalogosDisponibles.resta}`}
              color="secondary"
            />
          </div>
          <div className="arial mb-3">Detalles</div>
          <DataGrid pageSize={5} autoHeight rows={abonos} columns={columns} />
        </div>
      ) : (
        <>
          <Typography
            className="p-2"
            component="h3"
            variant="h6"
            color="primary"
            gutterBottom
          >
            {Object.keys(catalogosDisponibles).length > 0
              ? "No tiene abonos actualmente"
              : "Seleccione una Campaña"}
            <Spinner />
          </Typography>
        </>
      )}

      <Comprobante img={img} open={open} handleClose={handleClose} />
    </>
  );
}

export default React.memo(DataTable, (prevProps, nextProps) => {
  return prevProps.data === nextProps.data;
});

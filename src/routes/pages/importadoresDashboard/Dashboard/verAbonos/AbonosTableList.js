import React from "react";
import { DataGrid } from "@material-ui/data-grid";

import Typography from "@material-ui/core/Typography";
import VisibilityIcon from "@material-ui/icons/Visibility";

import Spinner from "Components/spinner";

export default function DataTable({ data }) {
  const columns = [
    {
      field: "id_campana",
      headerName: "Campaña",
      sortable: true,
      width: 150,
    },
    {
      field: "subtotal",
      headerName: "subtotal",
      width: 150,
      sortable: true,
    },
    {
      field: "abono",
      headerName: "abono",
      width: 150,
      sortable: true,
    },
    {
      field: "resta",
      headerName: "resta",
      width: 150,
      sortable: true,
    },
    {
      field: "id",
      headerName: "Ver",
      sortable: false,
      width: 150,
      renderCell: (params) => {
        return (
          <VisibilityIcon
            style={{ cursor: "pointer" }}
            onClick={() => props.selectJunior(params.value)}
          />
        );
      },
    },
  ];
  return (
    <>
      {data.length > 0 ? (
        <div style={{ height: 500, width: "100%" }}>

          <Typography className="p-2 mb-3" style={{background:"#0d4674", color:"white"}} component="h2" variant="h6"  gutterBottom>
          Detalles
      </Typography>
          <DataGrid
            rows={data}
            autoHeight
            autoPageSize
            columns={columns}
            pageSize={5}
          />
        </div>
      ) : (
        <>
          <Typography
            className="p-2"
            component="h3"
            variant="h6"
            color="primary"
            gutterBottom
          >
            No tiene abonos actualmente
            <Spinner />
          </Typography>
        </>
      )}
    </>
  );
}

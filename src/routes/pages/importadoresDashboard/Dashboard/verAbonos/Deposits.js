import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
  align: {
    textAlign: "left",
  },
});

export default function Deposits({ total }) {
  const classes = useStyles();
  return (
    <>
      <Typography
        className="p-2 mb-3"
        style={{ background: "#0d4674", color: "white" }}
        component="h2"
        variant="h6"
        gutterBottom
      >Total Abonos
      </Typography>
        <div className="p-2">
        <Typography className={classes.align} component="p" variant="h6">
          Total:<span style={{fontWeight:"400"}}> $ {total.subtotal}</span>
        </Typography>
          <Typography className={classes.align} component="p" variant="h6">
          Abonos:<span style={{fontWeight:"400"}}> $ {total.abono}</span>
        </Typography>
         <Typography className={classes.align} component="p" variant="h6">
          Resta:<span style={{fontWeight:"400"}}> $ {total.resta}</span>
        </Typography>
        <Divider />
        <Link color="primary" to={`/Abonos?${total.id_campana}`}>
          <Typography color="primary">Ver Balance</Typography>
        </Link>
      </div>
    </>
  );
}

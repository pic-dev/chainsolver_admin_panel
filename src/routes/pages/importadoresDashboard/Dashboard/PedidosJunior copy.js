import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import Orders from "./tableRow";
import JuniorsListPedidos from "./JuniorsListPedidos";
import axios from "axios";
import Nav from "./NavImportadores";
import Alert from "Util/dialogAlert";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { useJuniorByCampaingSenior, useGetCampaigns } from "Hooks/useAbonos";

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    background: "white",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    top: "auto",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    zIndex: "1049",
    color: "white",
    background: "#304152",
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    marginTop: "50px",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  button: {
    margin: theme.spacing(1),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

export default function Pedidos(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [alert, toggleAlert] = useState(false);
  const [selectJuniorToggle, setSelectJuniorToggle] = useState(true);
  const [pedidosList, setPedidosList] = useState([]);
  const [juniorList, setJuniorList] = useState([]);
  const [selectedJunior, setSelectedJunior] = useState([]);
  const [idSelected, setIdSelected] = useState("");
  const [junior, setjunior] = useState("");
  const [alertData, setAlertData] = useState({
    message: "El Junior Seleccionado, no posee pedidos actualmente.",
    type: "error",
  });
  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const getJuniorList = async () => {
    await axios
      .get(
        `https://point.qreport.site/rep/junior_with/s/${props.userSeniorData[0].id}`
      )
      .then((res) => {
        setJuniorList(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getData = async () => {
    await axios
      .get(
        `https://point.qreport.site/rep/senior/${props.userSeniorData[0].id}`
      )
      .then((res) => {
        setPedidosList(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getJuniorOrders = async () => {
    await axios
      .get(
        `https://point.qreport.site/rep/junior/${props.userSeniorData[0].id}`
      )
      .then((res) => {
        if (res.data.length > 0) {
          setSelectedJunior(res.data);
          setSelectJuniorToggle(false);
        } else {
          setSelectJuniorToggle(false);
          toggleAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getJuniorOrdersUpdate = async (e) => {
    await axios
      .get(`https://point.qreport.site/rep/junior/${e}`)
      .then((res) => {
        if (res.data.length > 0) {
          setSelectedJunior(res.data);
          setSelectJuniorToggle(false);
        } else {
          setSelectJuniorToggle(false);
          toggleAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const selectJunior = (e) => {
    const data = pedidosList.filter((datos) => {
      if (e == datos.id_cliente) {
        return datos;
      }
    });
    if (data.length > 0) {
      setIdSelected(e);
      setSelectedJunior(data);
      setSelectJuniorToggle(false);
    } else {
      toggleAlert(true);
    }
  };

  function updateData() {
    if (props.userSeniorData.length == 1) {
      getJuniorOrders();
    } else {
      getJuniorOrdersUpdate(idSelected);
    }
  }

  const { loading, seniorList } = useJuniorByCampaingSenior({
    idCampaña: junior,
    idSenior: props.userSeniorData[0].id,
    tipo: props.userSeniorData.length,
  });

  const { loadingCampaigns, Campaigns } = useGetCampaigns({
    idSenior: props.userSeniorData[0].id,
  });

  useEffect(() => {
    if (screen.width <= 769) {
      setOpen(!open);
    }

    if (props.userSeniorData.length == 1) {
      //getJuniorOrders();
    } else {
      //getJuniorList();
      getData();
    }
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Nav
        userSeniorData={props.userSeniorData}
        handleLogout={props.handleLogout}
        handleDrawerOpen={handleDrawerOpen}
        open={open}
      />

      <main className={classes.content}>
        <Typography component="h2" variant="h5">
          Dashboard - Ver Pedidos
        </Typography>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {!selectJuniorToggle && props.userSeniorData.length > 1 && (
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  startIcon={<ArrowBackIosIcon />}
                  onClick={() => setSelectJuniorToggle(true)}
                >
                  Volver
                </Button>
              </Grid>
            )}
            {loading && selectedJunior.length == 0 && (
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <JuniorsListPedidos
                    data={seniorList}
                    selectJunior={selectJunior}
                    seniorID={props.userSeniorData[0].id}
                  />
                </Paper>
              </Grid>
            )}
            {selectedJunior.length > 0 && (
              <Grid item xs={12}>
                <Orders
                  setIdSelected={setIdSelected}
                  updateData={updateData}
                  setSelectedJunior={setSelectedJunior}
                  selectedJunior={selectedJunior}
                />
              </Grid>
            )}
          </Grid>

          {Campaigns && !loading && (
            <Grid container spacing={4}>
              <Grid item xs={12} sm={12} md={12}>
                <Typography component="h2" color="primary" variant="h6">
                  Seleccione una campaña
                </Typography>
              </Grid>
              {Campaigns.map((card) => (
                <Grid item key={card.id} xs={12} sm={6} md={4}>
                  <Card
                    onClick={() => setjunior(card.id)}
                    className={classes.card}
                  >
                    <CardMedia
                      className={classes.cardMedia}
                      image={card.portada}
                      title={card.name}
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {card.name}
                      </Typography>
                      <Typography>{card.description}</Typography>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          )}
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
        <Alert
          open={alert}
          setAlert={toggleAlert}
          type={alertData.type}
          message={alertData.message}
        />
      </main>
    </div>
  );
}

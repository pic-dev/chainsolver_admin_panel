import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { DataGrid } from "@material-ui/data-grid";
import * as data from "Util/vipClientes.csv";
import Parallax from "Util/Parallax/Parallax.js";
import TextField from "@material-ui/core/TextField";
import { container } from "Util/Parallax/material-kit-react.js";
import classNames from "classnames";
import Divider from "@material-ui/core/Divider";
import Alert from "@material-ui/lab/Alert";
import WhatsApp from "@material-ui/icons/WhatsApp";
import { PageView } from "Util/tracking";
import RouteBack from "Util/RouteBack";
import PedidosTable from "Util/tablePedidoSearch"
import axios from "axios";
// Generate Order Data

//var csv is the CSV file with headers

const csvJSON = (csv) => {
  var result = [];
  result = csv.map(function (item, index) {
    return {
      id: item.ID,
      Nombre: item.NOMBRE,
      Pais: item.PAIS,
      Provincia: item.PROVINCIA,
      Distrito: item.DISTRITO,
      Telefono: item.TELEFONO,
      Telefono2: item.TELEFONO2,
    };
  });

  //return result; //JavaScript object
  return result; //JSON
};

const SearchProvincias = () => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (current) {
      var exists = !hash[current.Provincia];
      hash[current.Provincia] = true;
      return exists;
    })
    .map((item) => {
      return item.Provincia;
    });

  return array; //JSON
};

const SearchDistritos = (e) => {
  var hash = {};
  var array = csvJSON(data)
    .filter(function (item) {
      if (e == item.Provincia) {
        return item;
      }
    })
    .filter(function (current) {
      var exists = !hash[current.Distrito];
      hash[current.Distrito] = true;
      return exists;
    })
    .map((item) => {
      return item.Distrito;
    });

  return array; //JSON
};

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://www.pic-cargo.com/">
        PIC
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  rootAccordion: {
    width: "100%",
  },

  headingAccordion: {
    display: "grid",
    textAlign: "left",
  },
  AccordionOrange: {
    backgroundColor: "#ec6729 !important",
    color: "white",
  },
  AccordionBlue: {
    backgroundColor: "#0d5084 !important",
    color: "white",
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  heading2: {
    fontSize: theme.typography.pxToRem(17),
    flexBasis: "100%",
    flexShrink: 0,
    fontWeight: "bold",
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    flexBasis: "100%",
    fontWeight: "bold",
  },
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left",
  },
  title: {
    fontSize: "3.5rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative",
    textShadow: "1px -1px 0px black",
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px 0 0",
    textAlign: "justify",
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",

    zIndex: "3",
    position: "relative",
    background: "#FFFFFF",

    marginTop: "50px",
  },
  button: {
    backgroundColor: "#64b86a",
    margin: "1rem",
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  displayNone: {
    display: "none",
  },

  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "50%",
    },
  },  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  toolbar: {
    flexWrap: "wrap",
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  titulo: {
    fontSize: "1.5rem",
    fontWeight: "bold",
    color: "#0d5084",
  },
  mensaje: {
    fontSize: "1rem",
    color: "black",
  },
  heroContent: {
    padding: theme.spacing(4, 2),
  },
  cardHeader: {
    fontSize: "1.3rem",
    color: "white",
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
    padding: {
      padding: theme.spacing(3),
    },
    paddingTittle: {
      padding: theme.spacing(10),
      marginBottom: theme.spacing(10),
    },
  },
}));


export default function BusquedaPedidos(props) {
  const classes = useStyles();
  const [alert, setAlert] = useState(false);
  const [filter, setFilter] = useState([]);

  const handleChange = (e) => {
    setAlert(false);
    e.preventDefault();
    const { Orden } = e.target.elements;
    axios
      .get(`https://point.qreport.site/productos/orden/${Orden.value}`)
      .then((res) => {
             if (res.data.length > 0) {
          setFilter(res.data);
        } else {
          setAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (process.env.NODE_ENV == "production") {
      PageView();
    }
  }, []);

  return (
    <React.Fragment>
      <CssBaseline />
      <Parallax image={require("Assets/img/PublicidadLarge.png")}>
        <div className={classNames(classes.container, classes.mainRaised)}>
          <Container
            maxWidth="md"
            component="main"
            className={classes.heroContent}
          >
            <Grid className="text-left" container spacing={5}>
              <Grid item xs={12} sm={12} md={12}>
                <RouteBack />
              </Grid>
            </Grid>
            <Grid container spacing={6}>
              <Grid item xs={12} sm={12} md={12}>

            <img
              key="img"
              src="/assets/img/seo.svg"
              alt="logo"
              style={{ height: "4rem"}}
            />   
                <Typography
                  variant="h6"
                  align="center"
                  color="textPrimary"
                  component="p"
                >
                  Busqueda de Pedidos
                </Typography>
              </Grid>
            </Grid>
          </Container>

          <Container maxWidth="md" className={classes.heroContent}>
          <Grid container >
          {alert && (
              <Grid item xs={12} sm={12} md={12}>
                <Alert severity="error">
                  Numero de orden no encontrada, verifique
                </Alert>
              </Grid>
            )}       
         </Grid>
            <form
              className={classes.root}
              
              onSubmit={handleChange}
              autoComplete="off"
            >
              <TextField
              required
                id="Orden"
                type="number"
                name="Orden"
                variant="filled"
                label="ID Orden"
                helperText="Ingrese el ID de su orden"
              />
              <Button
                type="submit"
                
                variant="contained"
                color="primary"
                className={classes.submit}
              >Buscar
              </Button>
            </form>
         
          </Container>
          <Divider />
          {filter.length > 0 && (
            <Container maxWidth="md">
              <div style={{ width: "100%", textAlign: "center" }}>
                <PedidosTable productos={filter} />
              </div>
            </Container>
          )}
          <Container
            maxWidth="md"
            className={(classes.footer, classes.heroContent)}
          >
            <Copyright />
          </Container>
        </div>
      </Parallax>
    </React.Fragment>
  );
}

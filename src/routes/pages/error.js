import React, { Component, Fragment } from "react";
import { Row, Card, CardTitle, Button } from "reactstrap";
import { NavLink } from "react-router-dom";

import { Colxx } from "Components/CustomBootstrap";

class Error404 extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.history.push("/");
    document.body.classList.add("background");
  }
  componentWillUnmount() {
    document.body.classList.remove("background");
  }
  render() {
    return (
      <Fragment>
        <div className="fixed-background" />
        <main>
          <div>
            <Row className="h-100">
              <Colxx xxs="12" md="10" className="mx-auto my-auto">
                <Card className="auth-card">
                  <div className="position-relative image-side ">
                    <p className="color-white h2">PIC</p>
                    <p className="color-white mb-0">
                      CALCULADORA DE FLETES E IMPUESTOS DE ADUANA
                    </p>
                  </div>
                  <div className="form-side">
                    <NavLink to={`/`} className="color-white">
                      <span className="logo-single" />
                    </NavLink>
                    <CardTitle className="mb-4"></CardTitle>
                    <p className="mb-0 text-muted text-small mb-0"></p>
                    <p className="display-1 font-weight-bold mb-5">404</p>
                    <Button
                      href="/"
                      color="primary"
                      className="btn-shadow"
                      size="lg"
                    ></Button>
                  </div>
                </Card>
              </Colxx>
            </Row>
          </div>
        </main>
      </Fragment>
    );
  }
}
export default Error404;

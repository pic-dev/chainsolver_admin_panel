import React, { Component, Suspense, lazy } from "react";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faBriefcase,
  faPhone,
  faNewspaper,
  faSignOutAlt,
  faMoneyCheck,
  faSearch,
  faSign,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { firebaseConf } from "Util/Firebase";
import { Event } from "Util/tracking";

const SignInModal = React.lazy(() => import("Util/Modals/modalSignIn"));
const ModalComment = React.lazy(() => import("Util/Modals/modalComment"));
const ModalInfoPaso4 = React.lazy(() => import("Util/Modals/modalInfoPaso4"));
const ModalQuoteSearch = React.lazy(() =>
  import("Util/Modals/modalQuoteSearch")
);
class TopNav extends Component {
  constructor(props) {
    super(props);

    this.signIn = this.signIn.bind(this);
    this.commentary = this.commentary.bind(this);
    this.toggleinfoPaso4 = this.toggleinfoPaso4.bind(this);
    this.desglose = this.desglose.bind(this);
    this.state = {
      desglose: false,
      comment: false,
      IsVisiblesignIn: false,
      infoPaso4: false,
    };
  }

  componentDidMount() {
    let usuario;
    if (sessionStorage.usuario !== undefined) {
      if (sessionStorage.usuario === "1") {
        usuario = false;
      } else {
        usuario = true;
      }
    } else {
    }

    if (sessionStorage.usuario === "2") {
    } else {
      sessionStorage.usuario = "1";
    }
  }

  signIn() {
    this.setState((ps) => ({
      IsVisiblesignIn: !ps.IsVisiblesignIn,
    }));
  }

  toggleinfoPaso4() {
    this.setState((ps) => ({ infoPaso4: !ps.infoPaso4 }));
  }

  desglose() {
    this.setState((ps) => ({
      desglose: !ps.desglose,
    }));
  }

  commentary() {
    this.setState((ps) => ({ comment: !ps.comment }));
  }

  handleLogout = () => {
    firebaseConf.auth().signOut();
    sessionStorage.usuario = "1";
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("seniorLogin");
    window.location.reload();
  };
  EventServicio = () => {
    /*Event(
      "Servicio de Agente de Aduana",
      "Servicio de Agente de Aduana",
      "Header"
    );*/
  };
  EventAhorrar = () => {
    //Event("Aprende a Ahorrar", "Aprende a Ahorrar", "Header");
  };

  render() {
    let usuario;
    if (sessionStorage.usuario !== undefined) {
      if (sessionStorage.usuario == "1") {
        usuario = false;
      } else {
        usuario = true;
      }
    } else {
    }

    let user;
    let data = this.props.usuario;
    if (sessionStorage.usuario == "2" || this.props.estadoAuth) {
      if (!data || this.props.usuario.length == 0) {
        user = [
          ["PIC"],
          ["Invitado"],
          ["Invitado"],
          ["/assets/img/profile-pic-l.png"],
        ];
      } else {
        user = data;
      }
    } else {
      user = [
        ["PIC"],
        ["Invitado"],
        ["Invitado"],
        ["/assets/img/profile-pic-l.png"],
      ];
    }

    return (
      <nav className="navbar fixed-top">
        <a className="navbar-logo" href="/">
          <span className="logo d-none d-xs-block" />
          <span className="logo-mobile d-block d-xs-none" />
          <div
            className={`titleNav arial display-large ${
              this.props.toggle === "1" ? "" : "display-smTop"
            }`}
          >
            CALCULADORA DE FLETES E IMPUESTOS DE ADUANA
          </div>
        </a>

        <div
          style={{ display: "flex", alignItems: "flex-end", fontSize: "large" }}
          className="ml-auto "
        >
          <div className="display-sm  mr-3">
            <a
              rel="noopener noreferrer"
              style={{ color: "#ffffff" }}
              href="https://www.facebook.com/PicCargoPeru/"
              target="_blank"
            >
              {" "}
              <i className="simple-icon-social-facebook  ml-3 mr-2" />
            </a>
            <a
              rel="noopener noreferrer"
              style={{ color: "#ffffff" }}
              href="https://twitter.com/pic_cargo"
              target="_blank"
            >
              {" "}
              <i className="simple-icon-social-twitter  mr-2" />
            </a>
            <a
              rel="noopener noreferrer"
              style={{ color: "#ffffff" }}
              href="https://www.instagram.com/pic_cargo/"
              target="_blank"
            >
              {" "}
              <i className="simple-icon-social-instagram" />
            </a>
          </div>

          <div
            style={{ fontStyle: "normal" }}
            className="header-icons  align-middle"
          >
            {" "}
            <div className="display-large pb-1 text-right mr-3">
              <a
                href="https://wa.me/50762197664"
                className="color-white"
                style={{fontSize:"0.8rem"}}
                target="_blank"
              >
                {" "}
                <img
                  height="15px"
                  src="/assets/img/flags/panamaRound.svg"
                  alt="Panama"
                />{" "}
                Panamá +50762197664 &nbsp;{" "}
              </a>

              <a
                href="https://wa.me/51920301745"
                className="color-white"
                style={{fontSize:"0.8rem"}}
                target="_blank"
              >
                {" "}
                <img
                  height="15px"
                  src="/assets/img/flags/peruRound.svg"
                  alt="Perú"
                />{" "}
                Perú +51920301745 &nbsp;{" "}
              </a>

              <a
                href="https://wa.me/584141417456"
                className="color-white"
                style={{fontSize:"0.8rem"}}
                target="_blank"
              >
                {" "}
                <img
                  height="15px"
                  src="/assets/img/flags/venezuelaRound.svg"
                  alt="Venezuela"
                />{" "}
                Venezuela +582127632240 &nbsp;&nbsp;{" "}
              </a>
              <a
                rel="noopener noreferrer"
                style={{ color: "#ffffff" }}
                href="https://www.facebook.com/PicCargoPeru/"
                target="_blank"
              >
                {" "}
                <i className="simple-icon-social-facebook  ml-3 mr-2" />
              </a>
              <a
                rel="noopener noreferrer"
                style={{ color: "#ffffff" }}
                href="https://twitter.com/pic_cargo"
                target="_blank"
              >
                {" "}
                <i className="simple-icon-social-twitter  mr-2" />
              </a>
              <a
                rel="noopener noreferrer"
                style={{ color: "#ffffff" }}
                href="https://www.instagram.com/pic_cargo/"
                target="_blank"
              >
                {" "}
                <i className="simple-icon-social-instagram" />
              </a>
            </div>
            <span
              className={` ${this.props.toggle === "2" ? "" : "display-large"}`}
            >
              <span
                style={{ padding: "0px", fontSize: "0.8rem" }}
                className="header-icon btn display-smTop position-relative "
              >
                &nbsp;
                <a style={{ color: "#ffffff" }} href="/">
                  <FontAwesomeIcon icon={faHome} className="mr-2" />
                  Cotiza tu Flete
                </a>
                &nbsp;
              </span>
              <span
                style={{ padding: "0px" }}
                className="display-smTop header-icon btn  position-relative "
              >
                &nbsp;|&nbsp;
              </span>

              <span
                style={{ padding: "0px", fontSize: "0.8rem" }}
                className="header-icon btn display-large  position-relative "
              >
                &nbsp;
                <a style={{ color: "#ffffff" }} href="/Importadores">
                  <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                  Importaciones grupales
                </a>
                &nbsp;
              </span>
              <span
                style={{ padding: "0px" }}
                className="display-large header-icon btn  position-relative "
              >
                &nbsp;|&nbsp;
              </span>

              <span
                style={{ padding: "0px", fontSize: "0.8rem" }}
                className="header-icon btn display-large  position-relative "
              >
                &nbsp;
                <a
                  rel="noopener noreferrer"
                  style={{ color: "#ffffff" }}
                  href="https://www.pic-cargo.com/es/agencia-de-aduana-agente-aduanal/"
                  target="_blank"
                  onClick={() => {
                    this.EventServicio();
                  }}
                >
                  <FontAwesomeIcon icon={faNewspaper} className="mr-2" />{" "}
                  Servicio de Agente de Aduana
                </a>
                &nbsp;
              </span>
              <span
                style={{ padding: "0px" }}
                className="display-large header-icon btn  position-relative "
              >
                &nbsp;|&nbsp;
              </span>
              <span
                style={{ padding: "0px", fontSize: "0.8rem" }}
                className="header-icon btn display-large  position-relative "
              >
                &nbsp;
                <a
                  rel="noopener noreferrer"
                  style={{ color: "#ffffff" }}
                  href="/Ahorrar"
                  onClick={() => {
                    this.EventAhorrar();
                  }}
                >
                  <FontAwesomeIcon icon={faMoneyCheck} className="mr-2" />
                  Aprende a Ahorrar
                </a>
                &nbsp;
              </span>
              <span
                style={{ padding: "0px" }}
                className="display-large display-large header-icon btn display-smTop position-relative "
              >
                &nbsp;|&nbsp;
              </span>
              <span
                style={{
                  padding: "0px",
                  fontSize: "0.8rem",
                  color: "#ffffff",
                }}
                className="header-icon btn display-large display-smTop position-relative "
              >
                &nbsp;
                <a
                  rel="noopener noreferrer"
                  style={{ color: "#ffffff" }}
                  href="https://www.pic-cargo.com/es/contact/"
                  target="_blank"
                >
                  <FontAwesomeIcon icon={faPhone} className="mr-2" /> Contacto
                </a>
                &nbsp;
              </span>
              <span
                style={{ padding: "0px" }}
                className="display-large header-icon btn  position-relative "
              >
                &nbsp;- &nbsp;
              </span>
            </span>
            <div className="header-icon user d-inline-block">
              {usuario || this.props.estadoAuth ? (
                <UncontrolledDropdown className="dropdown-menu-right">
                  <DropdownToggle className="pb-0 pt-0 pl-0" color="empty">
                    <span
                      style={{ color: "#ffffff" }}
                      className="arial fontNavbar name mr-1"
                    >
                      {user[1]}
                    </span>
                    <span>
                      <img alt="Profile" src={user[3]} />
                    </span>
                    <span>
                      <i
                        className="arrowDown arrow-style2"
                        style={{
                          fontSize: "1rem",
                          cursor: "pointer",
                        }}
                      />
                    </span>
                  </DropdownToggle>

                  <DropdownMenu className="mt-3 bg-b" right>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/"
                    >
                      <FontAwesomeIcon icon={faHome} className="mr-2" />
                      Cotiza tu Flete
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/Importadores"
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                      Importadores grupales
                    </DropdownItem>

                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      onClick={() => {
                        this.EventServicio();
                      }}
                      href="https://www.pic-cargo.com/es/agencia-de-aduana-agente-aduanal/"
                      target="_blank"
                    >
                      <FontAwesomeIcon icon={faNewspaper} className="mr-2" />{" "}
                      Servicio de Agente de Aduana
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="https://www.youtube.com/channel/UCaAprzcpP0PrafQzMDfJ2uA/videos"
                      onClick={() => {
                        this.EventAhorrar();
                      }}
                    >
                      <FontAwesomeIcon icon={faMoneyCheck} className="mr-2" />{" "}
                      Aprende a Ahorrar
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/Ahorrar"
                      target="_blank"
                    >
                      {" "}
                      <FontAwesomeIcon icon={faPhone} className="mr-2" />
                      Contacto
                    </DropdownItem>

                    <DropdownItem
                      onClick={this.desglose}
                      style={{ color: "white" }}
                    >
                      <FontAwesomeIcon icon={faSearch} className="mr-2" />
                      Busqueda
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      key="logout"
                      style={{ color: "white" }}
                      onClick={() => this.handleLogout()}
                    >
                      {" "}
                      <FontAwesomeIcon
                        icon={faSignOutAlt}
                        className="mr-2"
                      />{" "}
                      Cerrar sesión
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              ) : (
                <UncontrolledDropdown className="dropdown-menu-right">
                  <span
                    style={{
                      cursor: "pointer",
                      color: "white",
                      verticalAlign: "top",
                    }}
                    className="arial fontNavbar name mr-1"
                    onClick={this.signIn}
                  >
                    Iniciar sesión
                  </span>

                  <DropdownToggle className="p-0 display-sm" color="empty">
                    <span className="navbar-toggler-icon ml-3 mr-2"></span>
                  </DropdownToggle>

                  <DropdownMenu className="mt-3 bg-b" right>
                    <DropdownItem
                      onClick={this.signIn}
                      style={{ color: "white" }}
                    >
                      <FontAwesomeIcon icon={faSign} className="mr-2" /> Iniciar
                      sesión
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/"
                    >
                      <FontAwesomeIcon icon={faHome} className="mr-2" />
                      Cotiza tu Flete
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/Importadores"
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                      Importadores grupales
                    </DropdownItem>

                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="https://www.pic-cargo.com/es/agencia-de-aduana-agente-aduanal/"
                      rel="noopener noreferrer"
                      target="_blank"
                      onClick={() => {
                        this.EventServicio();
                      }}
                    >
                      <FontAwesomeIcon icon={faNewspaper} className="mr-2" />{" "}
                      Servicio de Agente de Aduana
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      href="/Ahorrar"
                      onClick={() => {
                        this.EventAhorrar();
                      }}
                    >
                      <FontAwesomeIcon icon={faMoneyCheck} className="mr-2" />{" "}
                      Aprende a Ahorrar
                    </DropdownItem>
                    <DropdownItem
                      className="display-sm"
                      style={{ color: "white" }}
                      rel="noopener noreferrer"
                      href="https://www.pic-cargo.com/es/contact/"
                      target="_blank"
                    >
                      <FontAwesomeIcon icon={faPhone} className="mr-2" />
                      Contacto
                    </DropdownItem>

                    <DropdownItem
                      onClick={this.desglose}
                      style={{ color: "white" }}
                    >
                      <FontAwesomeIcon icon={faSearch} className="mr-2" />
                      Busqueda
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              )}
            </div>
          </div>
        </div>
        <React.Suspense fallback={<div className="loading"></div>}>
          <SignInModal
            active={this.state.IsVisiblesignIn}
            toggle={this.signIn}
            setUserSeniorData={this.props.setUserSeniorData}
            handleLogin={this.props.handleLogin}
          />
        </React.Suspense>
        <React.Suspense fallback={<div className="loading"></div>}>
          <ModalComment active={this.state.comment} toggle={this.commentary} />
        </React.Suspense>
        <React.Suspense fallback={<div className="loading"></div>}>
          <ModalInfoPaso4
            active={this.state.infoPaso4}
            toggle={this.toggleinfoPaso4}
          />
        </React.Suspense>
        <React.Suspense fallback={<div className="loading"></div>}>
          <ModalQuoteSearch
            api={this.props.api}
            active={this.state.desglose}
            toggle={this.desglose}
          />
        </React.Suspense>
      </nav>
    );
  }
}

export default TopNav;

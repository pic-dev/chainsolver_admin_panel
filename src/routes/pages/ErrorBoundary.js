import React, { Component, Fragment } from "react";
import { Row, Card, CardTitle, Button } from "reactstrap";
import { NavLink } from "react-router-dom";
import { Colxx } from "Components/CustomBootstrap";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  reset = () => {
    window.location.reload();
  };
  static getDerivedStateFromError(error) {
    // Actualiza el estado para que el siguiente renderizado muestre la interfaz de repuesto
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return (
        <Fragment>
          <div className="container2 ">
            <Row className="h-100 ">
              <Colxx
                xxs="12"
                md="10"
                style={{ top: "70px" }}
                className="mx-auto my-auto"
              >
                <Card className="auth-card">
                  <div className="position-relative image-side ">
                    <p className="text-white arial h1">PIC</p>
                    <p className="white mb-0">
                      CALCULADORA DE FLETES E IMPUESTOS DE ADUANA
                    </p>
                  </div>
                  <div className="form-side">
                    <NavLink to={`/`} className="white">
                      <span className="logo-single" />
                    </NavLink>
                    <CardTitle className="mb-4"></CardTitle>
                    <p className="mb-0 text-muted text-small mb-0"></p>
                    <p className="display-1 font-weight-bold mb-5">
                      Error Interno
                    </p>

                    <Button
                      color="primary"
                      className="btn-shadow"
                      onClick={this.reset}
                      size="lg"
                    >
                      Recargar pagina
                    </Button>
                  </div>
                </Card>
              </Colxx>
            </Row>
          </div>
        </Fragment>
      );
    }

    return this.props.children;
  }
}
export default ErrorBoundary;

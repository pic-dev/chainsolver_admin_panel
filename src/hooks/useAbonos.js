import { useEffect, useState } from "react";
import {
  getAbonos,
  getAbonosTotal,
  getJuniorByCampaingSenior,
  getJuniorOrdersBySenior,
  getCampaignsBySenior,
  getCampaignsByJunior,
  getArticulosPage,
  getTopArticulos,
} from "../services/importadores";

export function useAbonos({ campaña,id }) {
  const [abonos, setAbonos] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    getAbonos(campaña,id).then((abono) => {
      setAbonos(abono);
      setLoading(false);
    });
  }, [campaña,id]);

  return { loading, abonos };
}

export function useAbonosTotal({ id }) {
  const [abonosTotal, setAbonosTotal] = useState([]);
  const [loadingTotal, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getAbonosTotal(id).then((abono) => {
      setAbonosTotal(abono);
      setLoading(false);
    });
  }, [id]);

  return { loadingTotal, abonosTotal };
}

export function useJuniorByCampaingSenior({ idCampaña, idSenior, tipo }) {
  const [seniorList, setSeniorList] = useState([]);
  const [loading, setLoading] = useState(false);

  if (tipo === 1) {
    useEffect(() => {
      getJuniorOrdersBySenior(idSenior).then((senior) => {
        setSeniorList(senior);
        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  } else {
    useEffect(() => {
      getJuniorByCampaingSenior(idCampaña, idSenior).then((senior) => {
        setSeniorList(senior);
        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  }
  return { loading, seniorList };
}

export function useGetCampaigns({ idSenior, tipo }) {
  const [Campaigns, setCampaigns] = useState([]);
  const [loadingCampaigns, setLoading] = useState(false);

  if (tipo === 1) {
    useEffect(() => {
      setLoading(true);
      getCampaignsByJunior(idSenior).then((campaña) => {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  } else {
    useEffect(() => {
      setLoading(true);
      getCampaignsBySenior(idSenior).then((campaña) => {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  }
  return { loadingCampaigns, Campaigns };
}

const INITIAL_PAGE = 0;

export function useArticles({ catalogo }) {
  const [articulo, setArticulo] = useState([]);
  const [limite, setLimite] = useState(0);
  const [loading, setLoading] = useState(false);
  const [loadingNextPage, setLoadingNextPage] = useState(false);
  const [page, setPage] = useState(INITIAL_PAGE);

  useEffect(() => {
    setLoading(true);
    //Recuperamos la keyword del localStorage
    getArticulosPage({ catalogo: catalogo }).then((articulo) => {
      setArticulo(articulo);
      setLimite(articulo[0].limite);
      setLoading(false);
    });
  }, [catalogo]);

  useEffect(() => {
      if (page !== INITIAL_PAGE && (limite / articulo.length !== 1)) {
      setLoadingNextPage(true);
      getArticulosPage({ catalogo: catalogo, page }).then((nextarticulo) => {
        setArticulo((prevarticulo) => prevarticulo.concat(nextarticulo));
        setLoadingNextPage(false);
      });
    }
  }, [catalogo, page]);

  return { loading, loadingNextPage, articulo, setPage, limite, page };
}

export function useArticlesTop(catalogo) {
  const [articulosTop, setArticulo] = useState([]);
  const [loadingTop, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    //Recuperamos la keyword del localStorage
    getTopArticulos(catalogo).then((articulosTop) => {
      setArticulo(articulosTop);
      setLoading(false);
    });
  }, [catalogo]);

  return { loadingTop, articulosTop };
}

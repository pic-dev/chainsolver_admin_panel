import { useEffect, useState } from "react";
import { firebaseConf } from "Util/Firebase";

export function useAuthStateChanged() {
  const [usuario, setUsuario] = useState(null);
  const [estadoAuth, setEstadoAuth] = useState(false);

  useEffect(() => {
    if (sessionStorage.seniorLogin !== undefined) {
      let data = JSON.parse(sessionStorage.getItem("seniorLogin"));
      let datos = [
        [data[0].usuario],
        [data[0].nombres + " " + data[0].apellidos],
        [data[0].correo ? data[0].correo : data[0].usuario],
        ["/assets/img/profile-pic-l.png"],
      ];

      sessionStorage.usuario = "2";
      setUsuario(datos);
      setEstadoAuth(true);
    } else {
      firebaseConf.auth().onAuthStateChanged((user) => {
        var name, email, photoUrl, uid;
        if (user) {
          if (user.displayName) {
            name = user.displayName;
          } else {
            name = user.email;
          }
          email = user.email;
          if (user.photoURL) {
            photoUrl = user.photoURL;
          } else {
            photoUrl = "/assets/img/profile-pic-l.png";
          }
          uid = user.uid;
          sessionStorage.usuario = "2";
          let datos = [[uid], [name], [email], [photoUrl]];
          sessionStorage.setItem("user", JSON.stringify(datos));
          setUsuario(datos);
          setEstadoAuth(true);
        } else {
          setUsuario(null);
          setEstadoAuth(false);
        }
      });
    }
  }, []);

  return { usuario, estadoAuth, setUsuario, setEstadoAuth };
}

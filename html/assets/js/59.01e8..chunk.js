(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[59],{

/***/ 1529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/NavLink.js
var NavLink = __webpack_require__(136);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/withRouter.js
var withRouter = __webpack_require__(249);

// EXTERNAL MODULE: ./src/components/CustomBootstrap/index.js
var CustomBootstrap = __webpack_require__(52);

// EXTERNAL MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js + 1 modules
var es = __webpack_require__(339);

// EXTERNAL MODULE: ./src/util/tracking.js + 12 modules
var tracking = __webpack_require__(20);

// EXTERNAL MODULE: ./node_modules/react-bootstrap-typeahead/css/Typeahead.css
var Typeahead = __webpack_require__(80);

// CONCATENATED MODULE: ./src/util/banderas.js






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var banderas_banderas = /*#__PURE__*/function (_Component) {
  inherits_default()(banderas, _Component);

  var _super = _createSuper(banderas);

  function banderas(props) {
    classCallCheck_default()(this, banderas);

    return _super.call(this, props);
  }

  createClass_default()(banderas, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement("div", {
        className: " banderas  mx-auto "
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "flag-usa banderasSize"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "flag-panama banderasSize "
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "flag-china banderasSize "
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "flag-peru banderasSize "
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "flag-vnzla banderasSize"
      }));
    }
  }]);

  return banderas;
}(react["Component"]);

/* harmony default export */ var util_banderas = (banderas_banderas);
// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// CONCATENATED MODULE: ./src/util/carruselHome.js






function carruselHome_createSuper(Derived) { var hasNativeReflectConstruct = carruselHome_isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function carruselHome_isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var carruselHome_App = /*#__PURE__*/function (_Component) {
  inherits_default()(App, _Component);

  var _super = carruselHome_createSuper(App);

  function App(props) {
    classCallCheck_default()(this, App);

    return _super.call(this, props);
  }

  createClass_default()(App, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement("div", {
        className: "theme-color-blue mx-auto FooterAlibaba"
      }, /*#__PURE__*/react_default.a.createElement("div", null, "  ", /*#__PURE__*/react_default.a.createElement("div", {
        className: "text-center titulo"
      }, "IMPORTACIONES GRUPALES")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "flex"
      }, /*#__PURE__*/react_default.a.createElement("img", {
        className: "img",
        src: "/assets/img/money.svg",
        alt: "alibaba"
      }), /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("div", {
        className: " SubTitulo"
      }, "POCO CAPITAL"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "parpadea  bottomCarruselHome"
      }, "CLICK AQU\xCD")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          height: "5rem",
          width: "5rem"
        }
      })));
    }
  }]);

  return App;
}(react["Component"]);

/* harmony default export */ var carruselHome = (carruselHome_App);
// EXTERNAL MODULE: ./src/util/helmet.js
var helmet = __webpack_require__(182);

// EXTERNAL MODULE: ./src/util/chat.js
var chat = __webpack_require__(341);

// CONCATENATED MODULE: ./src/routes/pages/calculadoraFletes/Inicio.js







function Inicio_createSuper(Derived) { var hasNativeReflectConstruct = Inicio_isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function Inicio_isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }













var Inicio_inicio = /*#__PURE__*/function (_Component) {
  inherits_default()(inicio, _Component);

  var _super = Inicio_createSuper(inicio);

  function inicio(props) {
    var _this;

    classCallCheck_default()(this, inicio);

    _this = _super.call(this, props);
    _this.transporte = _this.transporte.bind(assertThisInitialized_default()(_this));
    _this.pwaInstalingToggle = _this.pwaInstalingToggle.bind(assertThisInitialized_default()(_this));
    _this.state = {
      isOpen: false
    };
    return _this;
  }

  createClass_default()(inicio, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (true) {
        Object(tracking["b" /* PageView */])();
      }

      localStorage.clear();
      localStorage.page = "1";
      var element = document.getElementById("link3");
      Object(es["a" /* default */])(element, {
        behavior: "smooth",
        block: "center",
        inline: "center"
      });
      localStorage.page = "1";
    }
  }, {
    key: "pwaInstalingToggle",
    value: function pwaInstalingToggle(e) {
      this.setState({
        pwaInstaling: e
      });
    }
  }, {
    key: "transporte",
    value: function transporte(e) {
      switch (e) {
        case 1:
          this.props.name("MARITÍMO");
          localStorage.transporte = "MARITÍMO";
          break;

        case 2:
          this.props.name("AÉREO");
          localStorage.transporte = "AÉREO";
          break;

        default: // code block

      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      if (sessionStorage.usuario === "2") {} else {
        sessionStorage.usuario = "1";
      }

      localStorage.page = "1";
      return /*#__PURE__*/react_default.a.createElement(react["Fragment"], null, /*#__PURE__*/react_default.a.createElement(helmet["a" /* Title */], null, /*#__PURE__*/react_default.a.createElement("title", null, "Home | PIC  - Calculadora de Fletes")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "container2 topMargin"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "h-100 ",
        id: "link3"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "10",
        className: "mx-auto NoPadding"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "10",
        className: "font  mx-auto my-auto first-tittle NoPadding"
      }, " ", /*#__PURE__*/react_default.a.createElement("span", null, "\xA1COTIZA T\xDA FLETE", /*#__PURE__*/react_default.a.createElement("br", null), "Y ADUANA\xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "bggradien-orange color-white",
        style: {
          fontWeight: "bold",
          borderRadius: "5px"
        }
      }, "\xA0EN L\xCDNEA!\xA0"))), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-porcent"
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "6",
        md: "4",
        className: "mx-auto mt-3 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("hr", {
        className: "my-2"
      })), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "6",
        className: "mx-auto  "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "step-style"
      }, "PASO 1. Selecciona el Servicio"), " "), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "8",
        className: "mx-auto paddingInicio"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "6",
        sm: "6",
        md: "6",
        lg: "6",
        style: {
          float: "left",
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(NavLink["a" /* default */], {
        to: "/Ruta",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "select-tipe-cargo-crl ship-img",
        onClick: function onClick() {
          return _this2.transporte(1);
        }
      }), " ")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "6",
        sm: "6",
        md: "6",
        lg: "6",
        style: {
          float: "left",
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(NavLink["a" /* default */], {
        to: "/Ruta",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "select-tipe-cargo-crl plane-img",
        onClick: function onClick() {
          return _this2.transporte(2);
        }
      })))))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "10",
        xl: "6",
        lg: "8",
        className: "NoPadding mx-auto mt-2 mb-2 ".concat(Object(Utils["m" /* isMovil */])() && "fixed-bottom")
      }, /*#__PURE__*/react_default.a.createElement("hr", {
        className: "mt-2 my-2"
      }), /*#__PURE__*/react_default.a.createElement(NavLink["a" /* default */], {
        to: "/Importadores",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(carruselHome, null))), /*#__PURE__*/react_default.a.createElement(util_banderas, null))), /*#__PURE__*/react_default.a.createElement(chat["a" /* default */], null));
    }
  }]);

  return inicio;
}(react["Component"]);

/* harmony default export */ var Inicio = __webpack_exports__["default"] = (Object(withRouter["a" /* default */])(Inicio_inicio));

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/compute-scroll-into-view/dist/index.module.js
function t(t){return null!=t&&"object"==typeof t&&1===t.nodeType}function e(t,e){return(!e||"hidden"!==t)&&"visible"!==t&&"clip"!==t}function n(t,n){if(t.clientHeight<t.scrollHeight||t.clientWidth<t.scrollWidth){var r=getComputedStyle(t,null);return e(r.overflowY,n)||e(r.overflowX,n)||function(t){var e=function(t){if(!t.ownerDocument||!t.ownerDocument.defaultView)return null;try{return t.ownerDocument.defaultView.frameElement}catch(t){return null}}(t);return!!e&&(e.clientHeight<t.scrollHeight||e.clientWidth<t.scrollWidth)}(t)}return!1}function r(t,e,n,r,i,o,l,d){return o<t&&l>e||o>t&&l<e?0:o<=t&&d<=n||l>=e&&d>=n?o-t-r:l>e&&d<n||o<t&&d>n?l-e+i:0}/* harmony default export */ var index_module = (function(e,i){var o=window,l=i.scrollMode,d=i.block,u=i.inline,h=i.boundary,a=i.skipOverflowHiddenElements,c="function"==typeof h?h:function(t){return t!==h};if(!t(e))throw new TypeError("Invalid target");for(var f=document.scrollingElement||document.documentElement,s=[],p=e;t(p)&&c(p);){if((p=p.parentNode)===f){s.push(p);break}p===document.body&&n(p)&&!n(document.documentElement)||n(p,a)&&s.push(p)}for(var g=o.visualViewport?o.visualViewport.width:innerWidth,m=o.visualViewport?o.visualViewport.height:innerHeight,w=window.scrollX||pageXOffset,v=window.scrollY||pageYOffset,W=e.getBoundingClientRect(),b=W.height,H=W.width,y=W.top,M=W.right,E=W.bottom,V=W.left,x="start"===d||"nearest"===d?y:"end"===d?E:y+b/2,I="center"===u?V+H/2:"end"===u?M:V,C=[],T=0;T<s.length;T++){var k=s[T],B=k.getBoundingClientRect(),D=B.height,O=B.width,R=B.top,X=B.right,Y=B.bottom,L=B.left;if("if-needed"===l&&y>=0&&V>=0&&E<=m&&M<=g&&y>=R&&E<=Y&&V>=L&&M<=X)return C;var S=getComputedStyle(k),j=parseInt(S.borderLeftWidth,10),N=parseInt(S.borderTopWidth,10),q=parseInt(S.borderRightWidth,10),z=parseInt(S.borderBottomWidth,10),A=0,F=0,G="offsetWidth"in k?k.offsetWidth-k.clientWidth-j-q:0,J="offsetHeight"in k?k.offsetHeight-k.clientHeight-N-z:0;if(f===k)A="start"===d?x:"end"===d?x-m:"nearest"===d?r(v,v+m,m,N,z,v+x,v+x+b,b):x-m/2,F="start"===u?I:"center"===u?I-g/2:"end"===u?I-g:r(w,w+g,g,j,q,w+I,w+I+H,H),A=Math.max(0,A+v),F=Math.max(0,F+w);else{A="start"===d?x-R-N:"end"===d?x-Y+z+J:"nearest"===d?r(R,Y,D,N,z+J,x,x+b,b):x-(R+D/2)+J/2,F="start"===u?I-L-j:"center"===u?I-(L+O/2)+G/2:"end"===u?I-X+q+G:r(L,X,O,j,q+G,I,I+H,H);var K=k.scrollLeft,P=k.scrollTop;x+=P-(A=Math.max(0,Math.min(P+A,k.scrollHeight-D+J))),I+=K-(F=Math.max(0,Math.min(K+F,k.scrollWidth-O+G)))}C.push({el:k,top:A,left:F})}return C});
//# sourceMappingURL=index.module.js.map

// CONCATENATED MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js


function isOptionsObject(options) {
  return options === Object(options) && Object.keys(options).length !== 0;
}

function defaultBehavior(actions, behavior) {
  if (behavior === void 0) {
    behavior = 'auto';
  }

  var canSmoothScroll = ('scrollBehavior' in document.body.style);
  actions.forEach(function (_ref) {
    var el = _ref.el,
        top = _ref.top,
        left = _ref.left;

    if (el.scroll && canSmoothScroll) {
      el.scroll({
        top: top,
        left: left,
        behavior: behavior
      });
    } else {
      el.scrollTop = top;
      el.scrollLeft = left;
    }
  });
}

function getOptions(options) {
  if (options === false) {
    return {
      block: 'end',
      inline: 'nearest'
    };
  }

  if (isOptionsObject(options)) {
    return options;
  }

  return {
    block: 'start',
    inline: 'nearest'
  };
}

function scrollIntoView(target, options) {
  var targetIsDetached = !target.ownerDocument.documentElement.contains(target);

  if (isOptionsObject(options) && typeof options.behavior === 'function') {
    return options.behavior(targetIsDetached ? [] : index_module(target, options));
  }

  if (targetIsDetached) {
    return;
  }

  var computeOptions = getOptions(options);
  return defaultBehavior(index_module(target, computeOptions), computeOptions.behavior);
}

/* harmony default export */ var es = __webpack_exports__["a"] = (scrollIntoView);

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var BotChat = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(BotChat, _Component);

  var _super = _createSuper(BotChat);

  function BotChat(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, BotChat);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(BotChat, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      (function (d, m) {
        /*---------------- Kommunicate settings start ----------------*/
        var defaultSettings = {
          "defaultBotIds": ["charley-erttt"],
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "defaultAssignee": "charley-erttt",
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "skipRouting": true
        };
        var kommunicateSettings = {
          "appId": "2b0d8db8368a2f22263da303b739e204a",
          // Replace <APP_ID> with your APP_ID which you can find in install section of dashboard
          "automaticChatOpenOnNavigation": false,
          "onInit": function onInit() {
            Kommunicate.updateSettings(defaultSettings);
          }
        };
        /*----------------- Kommunicate settings end ------------------*/

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://widget.kommunicate.io/v2/kommunicate.app";
        var h = document.getElementsByTagName("head")[0];
        h.appendChild(s);
        window.kommunicate = m;
        m._globals = kommunicateSettings;
      })(document, window.kommunicate || {});
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", null);
    }
  }]);

  return BotChat;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (BotChat);

/***/ })

}]);
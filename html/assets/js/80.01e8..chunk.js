(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[80],{

/***/ 1132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_data_grid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(461);
/* harmony import */ var _material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(857);
/* harmony import */ var _material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1451);
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(652);
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _verComprobante__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(856);
/* harmony import */ var Components_spinner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(58);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(43);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(41);
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1443);
/* harmony import */ var _material_ui_core_Chip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1541);












function DataTable(_ref) {
  var data = _ref.data,
      catalogosDisponibles = _ref.catalogosDisponibles;
  var columns = [{
    field: "fecha_corta",
    headerName: "Fecha",
    sortable: true,
    width: 150
  }, {
    field: "monto",
    headerName: "monto",
    sortable: true,
    width: 120,
    renderCell: function renderCell(params) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "$ ".concat(params.value));
    }
  }, {
    field: "banco",
    headerName: "Banco",
    width: 200,
    sortable: true
  }, {
    field: "nro_transaccion",
    headerName: "nro_transaccion",
    width: 350,
    sortable: true
  }, {
    field: "aprobado",
    headerName: "status",
    sortable: false,
    width: 200,
    renderCell: function renderCell(params) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        style: {
          color: params.value == "Aprobado" ? _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"][500] : _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"][500]
        }
      }, params.value == "Aprobado" ? "Confirmado Por PIC-Cargo" : "Por Aprobar");
    }
  }, {
    field: "deposito",
    headerName: "comprobante",
    sortable: false,
    width: 150,
    renderCell: function renderCell(params) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_10__[/* default */ "a"], {
        "aria-label": "open",
        size: "small",
        onClick: function onClick() {
          return handleClickOpen(params.value);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_5___default.a, {
        style: {
          color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"][500]
        }
      }));
    }
  }];

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState4 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState3, 2),
      abonos = _useState4[0],
      setAbonos = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      _useState6 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState5, 2),
      img = _useState6[0],
      setImg = _useState6[1];

  var handleClickOpen = function handleClickOpen(e) {
    setImg(e);
    setOpen(true);
  };

  var handleClose = function handleClose() {
    setOpen(false);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setAbonos(data);
  }, [data, catalogosDisponibles]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, data.length > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    className: "p-2 mb-3",
    style: {
      background: "#0d4674",
      color: "white"
    },
    component: "h2",
    variant: "h6",
    gutterBottom: true
  }, "Total Abonos"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      display: "flex"
    },
    className: "arial mb-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Chip__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
    className: "m-2",
    style: {
      fontSize: "1.5rem",
      padding: "1.5rem"
    },
    avatar: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        borderRadius: "50%",
        color: "white"
      }
    }),
    label: "Subtotal: ".concat(catalogosDisponibles.subtotal),
    color: "secondary"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Chip__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
    className: "m-2",
    style: {
      fontSize: "1.5rem",
      padding: "1.5rem"
    },
    avatar: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        borderRadius: "50%",
        color: "white"
      }
    }),
    label: "Abonos: ".concat(catalogosDisponibles.abono),
    color: "secondary"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Chip__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
    className: "m-2",
    style: {
      fontSize: "1.5rem",
      padding: "1.5rem"
    },
    avatar: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_MonetizationOn__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        borderRadius: "50%",
        color: "white"
      }
    }),
    label: "Resta:  ".concat(catalogosDisponibles.resta),
    color: "secondary"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "arial mb-3"
  }, "Detalles"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_data_grid__WEBPACK_IMPORTED_MODULE_2__[/* DataGrid */ "a"], {
    pageSize: 5,
    autoHeight: true,
    rows: abonos,
    columns: columns
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    className: "p-2",
    component: "h3",
    variant: "h6",
    color: "primary",
    gutterBottom: true
  }, Object.keys(catalogosDisponibles).length > 0 ? "No tiene abonos actualmente" : "Seleccione una Campaña", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Components_spinner__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_verComprobante__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    img: img,
    open: open,
    handleClose: handleClose
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.memo(DataTable, function (prevProps, nextProps) {
  return prevProps.data === nextProps.data;
}));

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[61,9],{

/***/ 1499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(44);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(53);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3);
/* harmony import */ var Util_Firebase__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(11);
/* harmony import */ var Util_tracking__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(20);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(136);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(249);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(79);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(594);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var Util_helmet__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(182);









function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }









var ProductosDetallesFirebase = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(6), __webpack_require__.e(8), __webpack_require__.e(71)]).then(__webpack_require__.bind(null, 1126));
});
var ProductosDetallesCampaña = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(6), __webpack_require__.e(8), __webpack_require__.e(62)]).then(__webpack_require__.bind(null, 1127));
});
var ProductosDetallesConcentradores = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(2), __webpack_require__.e(42)]).then(__webpack_require__.bind(null, 1129));
});

var ProductoDetalle = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(ProductoDetalle, _Component);

  var _super = _createSuper(ProductoDetalle);

  function ProductoDetalle(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, ProductoDetalle);

    _this = _super.call(this, props);
    _this.toggleModal = _this.toggleModal.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.handleSubtmitJunior = _this.handleSubtmitJunior.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.toggleAlert = _this.toggleAlert.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.state = {
      ClienteStock: [],
      typeProduct: [],
      campañas: [],
      rubros: [],
      toggleProductDetailsdata: [],
      product: [],
      toggleProducto: false,
      isOpen: false,
      loginJunior: false,
      selectImportador: false,
      modal: false,
      JuniorData: [],
      isOpenAlert: false
    };
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(ProductoDetalle, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (true) {
        Object(Util_tracking__WEBPACK_IMPORTED_MODULE_11__[/* PageView */ "b"])();
      }

      this.getData();
      var stock = Util_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].database().ref("/CatalagosStock/");
      stock.orderByChild("KeyProducto").equalTo(this.props.match.params.id).on("child_added", function (snapshot) {
        var product = snapshot.val();
        var databasestock = Util_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].database().ref("/Catalogos/" + _this2.props.match.params.id);
        databasestock.on("value", function (snapshot) {
          var key = [{
            key: _this2.props.match.params.id
          }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());
          var data = Object.assign(arreglo[1], arreglo[0]);

          _this2.setState({
            product: _this2.state.product.concat(Object.assign(product, data))
          });

          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = _this2.props.match.params.id;

            var _data = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });

            if (_data.length > 0) {
              _this2.setState({
                toggleProductDetailsdata: [_data[0]],
                ClienteStock: [_data[0].clientes],
                typeProduct: _data[0].TipoMerca
              });
            }

            setTimeout(function () {
              _this2.setState({
                toggleProducto: true
              });
            }, 1000);
          }
        });
      });
      var stock = Util_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].database().ref("/stock/");
      stock.orderByChild("KeyProducto").equalTo(this.props.match.params.id).on("child_added", function (snapshot) {
        var product = snapshot.val();
        var databasestock = Util_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].database().ref("/product/" + _this2.props.match.params.id);
        databasestock.on("value", function (snapshot) {
          var key = [{
            key: _this2.props.match.params.id
          }];
          var producto = [];
          var arreglo = key.concat(snapshot.val());
          var data = Object.assign(arreglo[1], arreglo[0]);

          _this2.setState({
            product: _this2.state.product.concat(Object.assign(product, data))
          });

          producto = producto.concat(Object.assign(product, data));

          if (producto.length > 0) {
            var key = _this2.props.match.params.id;

            var _data2 = producto.filter(function (item) {
              if (key == item.key) {
                return item.Producto;
              }
            });

            if (_data2.length > 0) {
              _this2.setState({
                toggleProductDetailsdata: [_data2[0]],
                ClienteStock: [_data2[0].clientes],
                typeProduct: _data2[0].TipoMerca
              });
            }

            setTimeout(function () {
              _this2.setState({
                toggleProducto: true
              });
            }, 1000);
          }
        });
      });
    }
  }, {
    key: "getData",
    value: function () {
      var _getData = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _this3 = this;

        var key;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                key = this.props.match.params.id;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_14___default.a.get("https://point.qreport.site/campana").then(function (res) {
                  var campaña = res.data.filter(function (item) {
                    if (item.id == key) {
                      return item;
                    }
                  });

                  if (campaña.length > 0) {
                    _this3.setState({
                      campañas: campaña,
                      toggleProducto: true
                    });
                  }
                }).catch(function (error) {
                  console.log(error);
                });

              case 3:
                _context.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_14___default.a.get("https://point.qreport.site/campana/rubros/".concat(key)).then(function (res) {
                  _this3.setState({
                    rubros: res.data
                  });
                }).catch(function (error) {
                  console.log(error);
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getData() {
        return _getData.apply(this, arguments);
      }

      return getData;
    }()
  }, {
    key: "handleSubtmitJunior",
    value: function handleSubtmitJunior(e) {
      var _this4 = this;

      e.preventDefault();
      var _e$target$elements = e.target.elements,
          dni = _e$target$elements.dni,
          password = _e$target$elements.password;
      axios__WEBPACK_IMPORTED_MODULE_14___default.a.post("https://point.qreport.site/login/junior", {
        usuario: dni.value,
        contraseña: password.value
      }).then(function (res) {
        if (res.data.length > 0) {
          sessionStorage.seniorLogin = JSON.stringify(res.data);

          _this4.setState({
            JuniorData: res.data,
            selectImportador: true,
            modal: false
          });
        } else {
          _this4.toggleAlert();

          setTimeout(function () {
            _this4.toggleAlert();
          }, 2000);
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  }, {
    key: "toggleAlert",
    value: function toggleAlert() {
      this.setState(function (prevState) {
        return {
          isOpenAlert: !prevState.isOpenAlert
        };
      });
    }
  }, {
    key: "toggleModal",
    value: function toggleModal() {
      if (sessionStorage.seniorLogin !== undefined && this.state.modal == false) {
        var data = JSON.parse(sessionStorage.getItem("seniorLogin"));
        this.setState({
          JuniorData: data[0],
          selectImportador: true,
          modal: false
        });
      } else {
        this.setState({
          loginJunior: false
        });
        this.setState(function (prevState) {
          return {
            modal: !prevState.modal
          };
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var key = this.props.match.params.id;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_8__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(Util_helmet__WEBPACK_IMPORTED_MODULE_16__[/* Title */ "a"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("title", null, window.location.pathname.replace("/", "") + " | " + "PIC  - Calculadora de Fletes")), Number(this.props.match.params.id) !== 22 && Number(this.props.match.params.id) !== 21 && Number(this.props.match.params.id) !== 16 && Number(this.props.match.params.id) !== 23 && this.state.campañas.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ProductosDetallesCampaña, {
        id: key,
        toggleProducto: this.state.toggleProducto,
        rubros: this.state.rubros,
        campañas: this.state.campañas[0],
        selectImportador: this.state.selectImportador,
        toggleModal: this.toggleModal,
        JuniorData: this.state.JuniorData
      }), Number(this.props.match.params.id) !== 22 && Number(this.props.match.params.id) !== 21 && Number(this.props.match.params.id) !== 16 && Number(this.props.match.params.id) !== 23 && this.state.campañas.length == 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ProductosDetallesFirebase, {
        toggleProducto: this.state.toggleProducto,
        keyCampaña: key,
        ClienteStock: this.state.ClienteStock[0],
        producto: this.state.toggleProductDetailsdata[0]
      }), (Number(this.props.match.params.id) === 22 || Number(this.props.match.params.id) === 21 || Number(this.props.match.params.id) === 23 || Number(this.props.match.params.id) === 16) && this.state.campañas.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(ProductosDetallesConcentradores, {
        campañas: this.state.campañas[0],
        keyCampaña: Number(key)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Modal */ "D"], {
        isOpen: this.state.modal,
        toggle: this.toggleModal
      }, this.state.campañas.length > 0 && this.state.campañas[0].cerrado === 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* ModalBody */ "E"], null, this.state.loginJunior ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "text-center arial mx-auto pb-5",
        style: {
          fontSize: "1rem"
        }
      }, "Iniciar Sesi\xF3n"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Form */ "t"], {
        onSubmit: this.handleSubtmitJunior
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* FormGroup */ "u"], {
        row: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Label */ "A"], {
        className: "arial",
        for: "dni",
        sm: 3
      }, "Usuario o DNI"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        sm: 9
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Input */ "w"], {
        type: "text",
        name: "dni",
        id: "dni",
        placeholder: "Ingrese Dni"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* FormGroup */ "u"], {
        row: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Label */ "A"], {
        className: "arial",
        for: "password",
        sm: 3
      }, "Contrase\xF1a"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        sm: 9
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Input */ "w"], {
        type: "password",
        name: "password",
        id: "examplePassword",
        placeholder: "Ingrese password"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* FormGroup */ "u"], {
        row: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: 12,
        md: 12,
        sm: 12,
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Alert */ "a"], {
        color: "danger",
        isOpen: this.state.isOpenAlert,
        toggle: this.toggleAlert
      }, "\xA1No se encuentra registro del usuario, Verifique Usuario o Contrase\xF1a!"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* FormGroup */ "u"], {
        className: "p-0",
        check: true,
        row: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        className: "text-center",
        sm: 12
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue",
        color: "secondary"
      }, "Iniciar Sesi\xF3n"))), " "))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "text-center arial mx-auto pb-5",
        style: {
          fontSize: "1rem"
        }
      }, "Seleccione una Opci\xF3n:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        onClick: function onClick() {
          _this5.setState({
            loginJunior: true
          });
        },
        className: "btn-style-ConcentadoresPdf text-white arial  mb-2 p-3 bg-blue-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("span", null, "Ya Tengo una Cuenta"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("i", {
        style: {
          backgroundColor: "white",
          borderRadius: "50%",
          fontSize: "1.2rem",
          padding: ".2rem",
          fontWeight: "bold"
        },
        className: "ml-2 float-right color-orange simple-icon-question"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        onClick: function onClick() {
          _this5.setState({
            selectImportador: true,
            modal: false
          });
        },
        className: "btn-style-ConcentadoresPdf text-white arial  p-3 bg-blue-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("span", null, "No Tengo Cuenta"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("i", {
        style: {
          backgroundColor: "white",
          borderRadius: "50%",
          fontSize: "1.2rem",
          padding: ".2rem",
          fontWeight: "bold"
        },
        className: "ml-2 float-right color-orange simple-icon-question"
      })))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* ModalBody */ "E"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        className: "alert alert-danger text-center mx-auto mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("h1", null, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("i", {
        className: "simple-icon-info mr-2"
      }), "Esta Campa\xF1a Ha sido Cerrada")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("div", {
        className: "text-center mx-auto mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement("h2", null, " Unete a nuestras Prox\xEDmas Importaciones")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Col */ "l"], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "text-center font mx-auto pb-5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_12__[/* default */ "a"], {
        to: "/Importadores",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Button */ "c"], {
        className: "btn-style-quest",
        color: "secondary"
      }, "Ir a Proximas campa\xF1as"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* ModalFooter */ "F"], {
        style: {
          alignSelf: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_9__[/* Button */ "c"], {
        className: "btn-style",
        color: "secondary",
        onClick: this.toggleModal
      }, "Cerrar"))));
    }
  }]);

  return ProductoDetalle;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_13__[/* default */ "a"])(ProductoDetalle));

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(208);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(55);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }






/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ["wrappedComponentRef"]);

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Route__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
      children: function children(routeComponentProps) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, _extends({}, remainingProps, routeComponentProps, {
          ref: wrappedComponentRef
        }));
      }
    });
  };

  C.displayName = "withRouter(" + (Component.displayName || Component.name) + ")";
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
  };

  return hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default()(C, Component);
};

/* harmony default export */ __webpack_exports__["a"] = (withRouter);

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ }),

/***/ 208:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
    childContextTypes: true,
    contextTypes: true,
    defaultProps: true,
    displayName: true,
    getDefaultProps: true,
    getDerivedStateFromProps: true,
    mixins: true,
    propTypes: true,
    type: true
};

var KNOWN_STATICS = {
    name: true,
    length: true,
    prototype: true,
    caller: true,
    callee: true,
    arguments: true,
    arity: true
};

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = getPrototypeOf && getPrototypeOf(Object);

function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
    if (typeof sourceComponent !== 'string') { // don't hoist over string (html) components

        if (objectPrototype) {
            var inheritedComponent = getPrototypeOf(sourceComponent);
            if (inheritedComponent && inheritedComponent !== objectPrototype) {
                hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
            }
        }

        var keys = getOwnPropertyNames(sourceComponent);

        if (getOwnPropertySymbols) {
            keys = keys.concat(getOwnPropertySymbols(sourceComponent));
        }

        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];
            if (!REACT_STATICS[key] && !KNOWN_STATICS[key] && (!blacklist || !blacklist[key])) {
                var descriptor = getOwnPropertyDescriptor(sourceComponent, key);
                try { // Avoid failures from read-only properties
                    defineProperty(targetComponent, key, descriptor);
                } catch (e) {}
            }
        }

        return targetComponent;
    }

    return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[49],{

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var isProduction = "production" === 'production';
function warning(condition, message) {
  if (!isProduction) {
    if (condition) {
      return;
    }

    var text = "Warning: " + message;

    if (typeof console !== 'undefined') {
      console.warn(text);
    }

    try {
      throw Error(text);
    } catch (x) {}
  }
}

/* harmony default export */ __webpack_exports__["a"] = (warning);


/***/ }),

/***/ 1387:
/***/ (function(module, exports) {

module.exports = debounce;

function debounce(fn, wait, callFirst) {
  var timeout;
  return function() {
    if (!wait) {
      return fn.apply(this, arguments);
    }
    var context = this;
    var args = arguments;
    var callNow = callFirst && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(function() {
      timeout = null;
      if (!callNow) {
        return fn.apply(context, args);
      }
    }, wait);

    if (callNow) {
      return fn.apply(this, arguments);
    }
  };
}


/***/ }),

/***/ 1388:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _createSvgIcon = _interopRequireDefault(__webpack_require__(153));

var _default = (0, _createSvgIcon.default)( /*#__PURE__*/_react.default.createElement("path", {
  transform: "scale(1.33, 1.33)",
  d: "M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"
}), 'StarRate');

exports.default = _default;

/***/ }),

/***/ 1389:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

var _interopRequireWildcard = __webpack_require__(141);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var React = _interopRequireWildcard(__webpack_require__(1));

var _createSvgIcon = _interopRequireDefault(__webpack_require__(153));

var _default = (0, _createSvgIcon.default)( /*#__PURE__*/React.createElement("path", {
  d: "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z"
}), 'Info');

exports.default = _default;

/***/ }),

/***/ 1392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ Album; });

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(845);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/IconButton/IconButton.js
var IconButton = __webpack_require__(1443);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/StarRate.js
var StarRate = __webpack_require__(1388);
var StarRate_default = /*#__PURE__*/__webpack_require__.n(StarRate);

// EXTERNAL MODULE: ./src/hooks/useAbonos.js + 3 modules
var useAbonos = __webpack_require__(606);

// CONCATENATED MODULE: ./src/hooks/useNearScreen.js


function useNearScreen() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$distance = _ref.distance,
      distance = _ref$distance === void 0 ? '50px' : _ref$distance,
      externalRef = _ref.externalRef,
      _ref$once = _ref.once,
      once = _ref$once === void 0 ? true : _ref$once;

  var _useState = Object(react["useState"])(false),
      _useState2 = slicedToArray_default()(_useState, 2),
      isNearScreen = _useState2[0],
      setShow = _useState2[1];

  var fromRef = Object(react["useRef"])();
  Object(react["useEffect"])(function () {
    var observer;
    var fromElement = externalRef ? externalRef.current : fromRef.current;
    if (!fromElement) return;

    var onChange = function onChange(entries, observer) {
      var el = entries[0];

      if (el.isIntersecting) {
        setShow(true);
        once && observer.disconnect();
      } else {
        !once && setShow(false);
      }
    };

    Promise.resolve(typeof IntersectionObserver !== 'undefined' ? IntersectionObserver : __webpack_require__.e(/* import() */ 89).then(__webpack_require__.t.bind(null, 1515, 7))).then(function () {
      observer = new IntersectionObserver(onChange, {
        rootMargin: distance
      });
      observer.observe(fromElement);
    });
    return function () {
      return observer && observer.disconnect();
    };
  }, [distance, externalRef, once]);
  return {
    isNearScreen: isNearScreen,
    fromRef: fromRef
  };
}
// EXTERNAL MODULE: ./node_modules/just-debounce-it/index.js
var just_debounce_it = __webpack_require__(1387);
var just_debounce_it_default = /*#__PURE__*/__webpack_require__.n(just_debounce_it);

// EXTERNAL MODULE: ./node_modules/@material-ui/lab/esm/Skeleton/Skeleton.js
var Skeleton = __webpack_require__(1477);

// CONCATENATED MODULE: ./src/util/loading.js


function LoadingSpiner() {
  return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(Skeleton["a" /* default */], {
    variant: "text"
  }), /*#__PURE__*/react_default.a.createElement(Skeleton["a" /* default */], {
    variant: "circle",
    width: 40,
    height: 40
  }), /*#__PURE__*/react_default.a.createElement(Skeleton["a" /* default */], {
    variant: "rect",
    height: 100
  }));
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/extends.js
var esm_extends = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js
var objectWithoutProperties = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/react-is/index.js
var react_is = __webpack_require__(151);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/clsx/dist/clsx.m.js
var clsx_m = __webpack_require__(143);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/withStyles.js + 1 modules
var withStyles = __webpack_require__(145);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/GridList/GridList.js







var styles = {
  /* Styles applied to the root element. */
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    overflowY: 'auto',
    listStyle: 'none',
    padding: 0,
    WebkitOverflowScrolling: 'touch' // Add iOS momentum scrolling.

  }
};
var GridList_GridList = /*#__PURE__*/react["forwardRef"](function GridList(props, ref) {
  var _props$cellHeight = props.cellHeight,
      cellHeight = _props$cellHeight === void 0 ? 180 : _props$cellHeight,
      children = props.children,
      classes = props.classes,
      className = props.className,
      _props$cols = props.cols,
      cols = _props$cols === void 0 ? 2 : _props$cols,
      _props$component = props.component,
      Component = _props$component === void 0 ? 'ul' : _props$component,
      _props$spacing = props.spacing,
      spacing = _props$spacing === void 0 ? 4 : _props$spacing,
      style = props.style,
      other = Object(objectWithoutProperties["a" /* default */])(props, ["cellHeight", "children", "classes", "className", "cols", "component", "spacing", "style"]);

  return /*#__PURE__*/react["createElement"](Component, Object(esm_extends["a" /* default */])({
    className: Object(clsx_m["a" /* default */])(classes.root, className),
    ref: ref,
    style: Object(esm_extends["a" /* default */])({
      margin: -spacing / 2
    }, style)
  }, other), react["Children"].map(children, function (child) {
    if (! /*#__PURE__*/react["isValidElement"](child)) {
      return null;
    }

    if (false) {}

    var childCols = child.props.cols || 1;
    var childRows = child.props.rows || 1;
    return /*#__PURE__*/react["cloneElement"](child, {
      style: Object(esm_extends["a" /* default */])({
        width: "".concat(100 / cols * childCols, "%"),
        height: cellHeight === 'auto' ? 'auto' : cellHeight * childRows + spacing,
        padding: spacing / 2
      }, child.props.style)
    });
  }));
});
 false ? undefined : void 0;
/* harmony default export */ var esm_GridList_GridList = (Object(withStyles["a" /* default */])(styles, {
  name: 'MuiGridList'
})(GridList_GridList));
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/toConsumableArray.js + 2 modules
var toConsumableArray = __webpack_require__(59);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/utils/debounce.js
var debounce = __webpack_require__(488);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/utils/isMuiElement.js
var isMuiElement = __webpack_require__(578);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/GridListTile/GridListTile.js









var GridListTile_styles = {
  /* Styles applied to the root element. */
  root: {
    boxSizing: 'border-box',
    flexShrink: 0
  },

  /* Styles applied to the `div` element that wraps the children. */
  tile: {
    position: 'relative',
    display: 'block',
    // In case it's not rendered with a div.
    height: '100%',
    overflow: 'hidden'
  },

  /* Styles applied to an `img` element child, if needed to ensure it covers the tile. */
  imgFullHeight: {
    height: '100%',
    transform: 'translateX(-50%)',
    position: 'relative',
    left: '50%'
  },

  /* Styles applied to an `img` element child, if needed to ensure it covers the tile. */
  imgFullWidth: {
    width: '100%',
    position: 'relative',
    transform: 'translateY(-50%)',
    top: '50%'
  }
};

var GridListTile_fit = function fit(imgEl, classes) {
  if (!imgEl || !imgEl.complete) {
    return;
  }

  if (imgEl.width / imgEl.height > imgEl.parentElement.offsetWidth / imgEl.parentElement.offsetHeight) {
    var _imgEl$classList, _imgEl$classList2;

    (_imgEl$classList = imgEl.classList).remove.apply(_imgEl$classList, Object(toConsumableArray["a" /* default */])(classes.imgFullWidth.split(' ')));

    (_imgEl$classList2 = imgEl.classList).add.apply(_imgEl$classList2, Object(toConsumableArray["a" /* default */])(classes.imgFullHeight.split(' ')));
  } else {
    var _imgEl$classList3, _imgEl$classList4;

    (_imgEl$classList3 = imgEl.classList).remove.apply(_imgEl$classList3, Object(toConsumableArray["a" /* default */])(classes.imgFullHeight.split(' ')));

    (_imgEl$classList4 = imgEl.classList).add.apply(_imgEl$classList4, Object(toConsumableArray["a" /* default */])(classes.imgFullWidth.split(' ')));
  }
};

function ensureImageCover(imgEl, classes) {
  if (!imgEl) {
    return;
  }

  if (imgEl.complete) {
    GridListTile_fit(imgEl, classes);
  } else {
    imgEl.addEventListener('load', function () {
      GridListTile_fit(imgEl, classes);
    });
  }
}

var GridListTile_GridListTile = /*#__PURE__*/react["forwardRef"](function GridListTile(props, ref) {
  // cols rows default values are for docs only
  var children = props.children,
      classes = props.classes,
      className = props.className,
      _props$cols = props.cols,
      cols = _props$cols === void 0 ? 1 : _props$cols,
      _props$component = props.component,
      Component = _props$component === void 0 ? 'li' : _props$component,
      _props$rows = props.rows,
      rows = _props$rows === void 0 ? 1 : _props$rows,
      other = Object(objectWithoutProperties["a" /* default */])(props, ["children", "classes", "className", "cols", "component", "rows"]);

  var imgRef = react["useRef"](null);
  react["useEffect"](function () {
    ensureImageCover(imgRef.current, classes);
  });
  react["useEffect"](function () {
    var handleResize = Object(debounce["a" /* default */])(function () {
      GridListTile_fit(imgRef.current, classes);
    });
    window.addEventListener('resize', handleResize);
    return function () {
      handleResize.clear();
      window.removeEventListener('resize', handleResize);
    };
  }, [classes]);
  return /*#__PURE__*/react["createElement"](Component, Object(esm_extends["a" /* default */])({
    className: Object(clsx_m["a" /* default */])(classes.root, className),
    ref: ref
  }, other), /*#__PURE__*/react["createElement"]("div", {
    className: classes.tile
  }, react["Children"].map(children, function (child) {
    if (! /*#__PURE__*/react["isValidElement"](child)) {
      return null;
    }

    if (child.type === 'img' || Object(isMuiElement["a" /* default */])(child, ['Image'])) {
      return /*#__PURE__*/react["cloneElement"](child, {
        ref: imgRef
      });
    }

    return child;
  })));
});
 false ? undefined : void 0;
/* harmony default export */ var esm_GridListTile_GridListTile = (Object(withStyles["a" /* default */])(GridListTile_styles, {
  name: 'MuiGridListTile'
})(GridListTile_GridListTile));
// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Info.js
var Info = __webpack_require__(1389);
var Info_default = /*#__PURE__*/__webpack_require__.n(Info);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/utils/capitalize.js
var capitalize = __webpack_require__(161);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/ListSubheader/ListSubheader.js







var ListSubheader_styles = function styles(theme) {
  return {
    /* Styles applied to the root element. */
    root: {
      boxSizing: 'border-box',
      lineHeight: '48px',
      listStyle: 'none',
      color: theme.palette.text.secondary,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeightMedium,
      fontSize: theme.typography.pxToRem(14)
    },

    /* Styles applied to the root element if `color="primary"`. */
    colorPrimary: {
      color: theme.palette.primary.main
    },

    /* Styles applied to the root element if `color="inherit"`. */
    colorInherit: {
      color: 'inherit'
    },

    /* Styles applied to the inner `component` element if `disableGutters={false}`. */
    gutters: {
      paddingLeft: 16,
      paddingRight: 16
    },

    /* Styles applied to the root element if `inset={true}`. */
    inset: {
      paddingLeft: 72
    },

    /* Styles applied to the root element if `disableSticky={false}`. */
    sticky: {
      position: 'sticky',
      top: 0,
      zIndex: 1,
      backgroundColor: 'inherit'
    }
  };
};
var ListSubheader_ListSubheader = /*#__PURE__*/react["forwardRef"](function ListSubheader(props, ref) {
  var classes = props.classes,
      className = props.className,
      _props$color = props.color,
      color = _props$color === void 0 ? 'default' : _props$color,
      _props$component = props.component,
      Component = _props$component === void 0 ? 'li' : _props$component,
      _props$disableGutters = props.disableGutters,
      disableGutters = _props$disableGutters === void 0 ? false : _props$disableGutters,
      _props$disableSticky = props.disableSticky,
      disableSticky = _props$disableSticky === void 0 ? false : _props$disableSticky,
      _props$inset = props.inset,
      inset = _props$inset === void 0 ? false : _props$inset,
      other = Object(objectWithoutProperties["a" /* default */])(props, ["classes", "className", "color", "component", "disableGutters", "disableSticky", "inset"]);

  return /*#__PURE__*/react["createElement"](Component, Object(esm_extends["a" /* default */])({
    className: Object(clsx_m["a" /* default */])(classes.root, className, color !== 'default' && classes["color".concat(Object(capitalize["a" /* default */])(color))], inset && classes.inset, !disableSticky && classes.sticky, !disableGutters && classes.gutters),
    ref: ref
  }, other));
});
 false ? undefined : void 0;
/* harmony default export */ var esm_ListSubheader_ListSubheader = (Object(withStyles["a" /* default */])(ListSubheader_styles, {
  name: 'MuiListSubheader'
})(ListSubheader_ListSubheader));
// EXTERNAL MODULE: ./node_modules/@material-ui/icons/AddShoppingCart.js
var AddShoppingCart = __webpack_require__(912);
var AddShoppingCart_default = /*#__PURE__*/__webpack_require__.n(AddShoppingCart);

// EXTERNAL MODULE: ./src/components/spinner/index.js
var spinner = __webpack_require__(58);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Typography/Typography.js
var Typography = __webpack_require__(1451);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/colors/yellow.js
var yellow = {
  50: '#fffde7',
  100: '#fff9c4',
  200: '#fff59d',
  300: '#fff176',
  400: '#ffee58',
  500: '#ffeb3b',
  600: '#fdd835',
  700: '#fbc02d',
  800: '#f9a825',
  900: '#f57f17',
  A100: '#ffff8d',
  A200: '#ffff00',
  A400: '#ffea00',
  A700: '#ffd600'
};
/* harmony default export */ var colors_yellow = (yellow);
// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ShoppingCart.js
var ShoppingCart = __webpack_require__(496);
var ShoppingCart_default = /*#__PURE__*/__webpack_require__.n(ShoppingCart);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// CONCATENATED MODULE: ./src/routes/pages/seccionImportadores/articulosList.js





















var ModalDetalleArticulo = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(64)]).then(__webpack_require__.bind(null, 1514));
});
var useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      paddingBottom: "10px"
    },
    media: defineProperty_default()({
      height: "10rem",
      position: "relative",
      display: "inline-block",
      textAlign: "center"
    }, theme.breakpoints.down("sm"), {
      height: "6rem"
    }),
    priceBar: defineProperty_default()({
      height: "3rem",
      position: "relative",
      display: "inline-block",
      textAlign: "center"
    }, theme.breakpoints.down("sm"), {
      height: "3rem"
    }),
    GridListTile: {
      textAlign: "center",
      display: "grid",
      "& img": defineProperty_default()({
        width: "16rem",
        height: "10rem"
      }, theme.breakpoints.down("sm"), {
        width: "5rem",
        height: "5rem"
      }),
      borderTopLeftRadius: "1rem",
      borderTopRightRadius: "1rem",
      borderBottomLeftRadius: "1rem",
      borderBottomRightRadius: "1rem",
      boxShadow: "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)"
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },
    gridList: {
      width: "inherit",
      minHeight: "100vh",
      maxHeight: "100vh",
      overflowX: "hidden"
    },
    icon: {
      color: "rgba(255, 255, 255, 0.54)",
      padding: "5px"
    },
    iconContainer: {
      paddingTop: "10px",
      paddingBottom: "10px"
    },
    loadingContainer: {
      textAlign: "center",
      fontWeight: "bold",
      fontSize: "1rem",
      height: "3rem !important"
    },
    visor: {
      textAlign: "center",
      height: "3rem !important"
    },
    TopIconLeft: defineProperty_default()({
      textAlign: "left",
      zIndex: 1,
      fontWeight: "500",
      color: "#48494a",
      position: "absolute",
      left: 15,
      top: 10,
      fontSize: "1rem"
    }, theme.breakpoints.down("sm"), {
      left: 10,
      fontSize: ".7rem"
    }),
    TopIconRight: defineProperty_default()({
      textAlign: "right",
      zIndex: 1,
      color: "#48494a",
      position: "absolute",
      right: 15,
      top: 10
    }, theme.breakpoints.down("sm"), {
      right: 10
    }),
    TopIconRightTitle: defineProperty_default()({
      fontWeight: "bold",
      color: "#48494a",
      fontSize: "1.5rem"
    }, theme.breakpoints.down("sm"), {
      fontSize: ".8rem"
    }),
    TopIconRightSubtitle: defineProperty_default()({
      fontWeight: "400",
      color: "#48494a",
      fontSize: ".8rem"
    }, theme.breakpoints.down("sm"), {
      fontSize: ".6rem"
    }),
    TopIconPages: {
      position: "absolute",
      top: 0,
      right: 0
    },
    titleBar: {
      backgroundColor: "#59bae8",
      color: "white",
      borderTopLeftRadius: "1rem",
      borderTopRightRadius: "1rem",
      height: "2rem",
      fontWeight: "bold"
    },
    titleBarNone: {
      color: "white",
      borderTopLeftRadius: "1rem",
      borderTopRightRadius: "1rem",
      height: "2rem",
      fontWeight: "bold"
    },
    descriptionBar: {
      position: "relative",
      display: "inline-block",
      textAlign: "center",
      background: "rgb(140 140 140)",
      color: "white",
      borderBottomLeftRadius: "1rem",
      borderBottomRightRadius: "1rem",
      height: "4rem"
    },
    descriptionBartitle: defineProperty_default()({
      fontWeight: "bold",
      fontSize: "1rem"
    }, theme.breakpoints.down("sm"), {
      fontWeight: "bold",
      fontSize: ".8rem"
    }),
    descriptionBarSubtitle: defineProperty_default()({
      fontWeight: "600",
      fontSize: ".8rem"
    }, theme.breakpoints.down("sm"), {
      fontWeight: "bold",
      fontSize: ".6rem"
    }),
    descriptionBarIcon: defineProperty_default()({
      zIndex: 1,
      position: "absolute",
      color: "#000000",
      right: "5px",
      bottom: "5px",
      backgroundColor: " #50efc2",
      borderRadius: " 50%",
      fontWeight: "bold"
    }, theme.breakpoints.down("sm"), {
      fontSize: ".7rem"
    })
  };
});
function Album(_ref) {
  var _React$createElement;

  var ArticuloSelect = _ref.ArticuloSelect,
      catalago = _ref.catalago,
      status = _ref.status,
      JuniorData = _ref.JuniorData,
      toggleModal = _ref.toggleModal,
      fullscreen = _ref.fullscreen;
  var classes = useStyles();

  var _useState = Object(react["useState"])([]),
      _useState2 = slicedToArray_default()(_useState, 2),
      articuloSelected = _useState2[0],
      setArticuloSelected = _useState2[1];

  var _useState3 = Object(react["useState"])(false),
      _useState4 = slicedToArray_default()(_useState3, 2),
      modalDetalleOpen = _useState4[0],
      setModalDetalleOpen = _useState4[1];

  var _useState5 = Object(react["useState"])(0),
      _useState6 = slicedToArray_default()(_useState5, 2),
      productoPage = _useState6[0],
      setProductoPage = _useState6[1];

  var _useArticles = Object(useAbonos["c" /* useArticles */])({
    catalogo: catalago.id
  }),
      loading = _useArticles.loading,
      loadingNextPage = _useArticles.loadingNextPage,
      articulo = _useArticles.articulo,
      setPage = _useArticles.setPage,
      limite = _useArticles.limite;

  var externalRef = Object(react["useRef"])();

  var _useNearScreen = useNearScreen({
    externalRef: loading ? null : externalRef,
    once: false
  }),
      isNearScreen = _useNearScreen.isNearScreen;

  var debounceNextPage = Object(react["useCallback"])(just_debounce_it_default()(function () {
    return setPage(function (prevPage) {
      return prevPage + 1;
    });
  }, 2000), []);
  var debounceNextPageModal = Object(react["useCallback"])(just_debounce_it_default()(function () {
    return setPage(function (prevPage) {
      return prevPage + 1;
    });
  }, 500), []);
  Object(react["useEffect"])(function () {
    if (isNearScreen) debounceNextPage();
  });

  var modalDetalleArticuloOpen = function modalDetalleArticuloOpen(e) {
    console.log(e);
    var index = articulo.findIndex(function (item) {
      return item.id == e.id;
    });
    setProductoPage(index);
    setArticuloSelected(e);
    setModalDetalleOpen(true);
  };

  var changeProducto = function changeProducto(tipo, id) {
    var index = articulo.findIndex(function (item) {
      return item.id == id;
    });

    switch (tipo) {
      case "prev":
        if (productoPage > 0) {
          setProductoPage(index - 1);
          setArticuloSelected(articulo[index - 1]);
        }

        break;

      case "next":
        if (productoPage < limite && index < articulo.length - 1) {
          setProductoPage(index + 1);
          setArticuloSelected(articulo[index + 1]);

          if (index - articulo.length < 2) {
            debounceNextPageModal();
          }
        }

        break;

      default:
        break;
    }
  };

  return /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.root
  }, loading ? /*#__PURE__*/react_default.a.createElement(LoadingSpiner, null) : /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, /*#__PURE__*/react_default.a.createElement(esm_GridList_GridList, (_React$createElement = {
    cellHeight: 300,
    cols: Object(Utils["m" /* isMovil */])() ? 2 : status ? fullscreen ? 3 : 2 : 3,
    spacing: 10
  }, defineProperty_default()(_React$createElement, "cellHeight", "14rem"), defineProperty_default()(_React$createElement, "className", classes.gridList), _React$createElement), /*#__PURE__*/react_default.a.createElement(esm_GridListTile_GridListTile, {
    key: "Subheader",
    cols: 4,
    style: {
      height: "auto",
      width: "100%"
    }
  }, /*#__PURE__*/react_default.a.createElement(esm_ListSubheader_ListSubheader, {
    className: "arial",
    style: {
      display: "flex"
    }
  }, /*#__PURE__*/react_default.a.createElement(ShoppingCart_default.a, {
    className: "mr-3",
    style: {
      borderRadius: "50%"
    }
  }), " ", /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    component: "h6",
    variant: "h6",
    color: "secondary"
  }, "".concat(limite, " Articulos Disponibles")))), articulo.map(function (card) {
    return /*#__PURE__*/react_default.a.createElement("div", {
      key: card.id
    }, /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.GridListTile
    }, /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.titleBar
    }, card.top === 1 && /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      variant: "inherit",
      onClick: function onClick() {
        return modalDetalleArticuloOpen(card);
      }
    }, /*#__PURE__*/react_default.a.createElement(StarRate_default.a, {
      style: {
        borderRadius: "50%",
        color: colors_yellow[500]
      }
    }), "TOP PRODUCT")), /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.priceBar
    }, card.escala !== null && /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      className: classes.TopIconLeft,
      component: "div",
      variant: "p"
    }, "TALLA: ", /*#__PURE__*/react_default.a.createElement("br", null), /*#__PURE__*/react_default.a.createElement("span", null, card.escala)), /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.TopIconRight
    }, " ", status && /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      className: classes.TopIconRightTitle,
      variant: "p",
      style: {
        display: "grid"
      }
    }, JuniorData.length > 1 ? "$" + card.precio_venta : "$" + card.precio_junior), card.genero !== null && /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      className: classes.TopIconRightSubtitle,
      variant: "p",
      style: {
        display: "grid",
        paddingBottom: "5px"
      }
    }, card.genero))), /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.media
    }, /*#__PURE__*/react_default.a.createElement("img", {
      onClick: function onClick() {
        return modalDetalleArticuloOpen(card);
      },
      src: card.image,
      alt: card.name
    })), /*#__PURE__*/react_default.a.createElement("div", {
      className: classes.descriptionBar
    }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      variant: "inherit",
      className: classes.descriptionBartitle,
      onClick: function onClick() {
        return modalDetalleArticuloOpen(card);
      }
    }, card.name, " "), status && /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      variant: "p",
      className: classes.descriptionBarSubtitle
    }, JuniorData.length > 1 ? "$" + card.precio_venta : "$" + card.precio_junior), /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      className: classes.descriptionBarIcon,
      component: "div",
      variant: "p"
    }, status ? /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
      size: "small",
      color: "inherit",
      "aria-label": "addIcon",
      onClick: function onClick() {
        return ArticuloSelect(card);
      }
    }, /*#__PURE__*/react_default.a.createElement(AddShoppingCart_default.a, null)) : /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
      size: "small",
      color: "inherit",
      "aria-label": "addIcon",
      onClick: function onClick() {
        return modalDetalleArticuloOpen(card);
      }
    }, /*#__PURE__*/react_default.a.createElement(Info_default.a, null))))));
  }), /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.visor,
    "data-testid": "visor",
    ref: externalRef
  }), limite / articulo.length == 1 && /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.loadingContainer
  }, /*#__PURE__*/react_default.a.createElement("span", {
    className: "arial"
  }, "No hay m\xE1s productos disponibles"))), isNearScreen && limite / articulo.length !== 1 && /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.loadingContainer
  }, "Cargando... ", /*#__PURE__*/react_default.a.createElement(spinner["a" /* default */], null))), modalDetalleOpen && /*#__PURE__*/react_default.a.createElement(ModalDetalleArticulo, {
    JuniorData: JuniorData,
    status: status,
    open: modalDetalleOpen,
    toggle: setModalDetalleOpen,
    toggleModal: toggleModal,
    addArticulo: ArticuloSelect,
    articulo: articuloSelected,
    limite: limite,
    loadingNextPage: loadingNextPage,
    productoPage: productoPage,
    changeProducto: changeProducto
  }));
}

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function toVal(mix) {
	var k, y, str='';

	if (typeof mix === 'string' || typeof mix === 'number') {
		str += mix;
	} else if (typeof mix === 'object') {
		if (Array.isArray(mix)) {
			for (k=0; k < mix.length; k++) {
				if (mix[k]) {
					if (y = toVal(mix[k])) {
						str && (str += ' ');
						str += y;
					}
				}
			}
		} else {
			for (k in mix) {
				if (mix[k]) {
					str && (str += ' ');
					str += k;
				}
			}
		}
	}

	return str;
}

/* harmony default export */ __webpack_exports__["a"] = (function () {
	var i=0, tmp, x, str='';
	while (i < arguments.length) {
		if (tmp = arguments[i++]) {
			if (x = toVal(tmp)) {
				str && (str += ' ');
				str += x
			}
		}
	}
	return str;
});


/***/ }),

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (true) {
  module.exports = __webpack_require__(156);
} else {}


/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?
Symbol.for("react.suspense_list"):60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.block"):60121,w=b?Symbol.for("react.fundamental"):60117,x=b?Symbol.for("react.responder"):60118,y=b?Symbol.for("react.scope"):60119;
function z(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case t:case r:case h:return a;default:return u}}case d:return u}}}function A(a){return z(a)===m}exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;exports.Fragment=e;exports.Lazy=t;exports.Memo=r;exports.Portal=d;
exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;exports.isAsyncMode=function(a){return A(a)||z(a)===l};exports.isConcurrentMode=A;exports.isContextConsumer=function(a){return z(a)===k};exports.isContextProvider=function(a){return z(a)===h};exports.isElement=function(a){return"object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return z(a)===n};exports.isFragment=function(a){return z(a)===e};exports.isLazy=function(a){return z(a)===t};
exports.isMemo=function(a){return z(a)===r};exports.isPortal=function(a){return z(a)===d};exports.isProfiler=function(a){return z(a)===g};exports.isStrictMode=function(a){return z(a)===f};exports.isSuspense=function(a){return z(a)===p};
exports.isValidElementType=function(a){return"string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===w||a.$$typeof===x||a.$$typeof===y||a.$$typeof===v)};exports.typeOf=z;


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var reactIs = __webpack_require__(151);

/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
  childContextTypes: true,
  contextType: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDefaultProps: true,
  getDerivedStateFromError: true,
  getDerivedStateFromProps: true,
  mixins: true,
  propTypes: true,
  type: true
};
var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true
};
var FORWARD_REF_STATICS = {
  '$$typeof': true,
  render: true,
  defaultProps: true,
  displayName: true,
  propTypes: true
};
var MEMO_STATICS = {
  '$$typeof': true,
  compare: true,
  defaultProps: true,
  displayName: true,
  propTypes: true,
  type: true
};
var TYPE_STATICS = {};
TYPE_STATICS[reactIs.ForwardRef] = FORWARD_REF_STATICS;
TYPE_STATICS[reactIs.Memo] = MEMO_STATICS;

function getStatics(component) {
  // React v16.11 and below
  if (reactIs.isMemo(component)) {
    return MEMO_STATICS;
  } // React v16.12 and above


  return TYPE_STATICS[component['$$typeof']] || REACT_STATICS;
}

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = Object.prototype;
function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components
    if (objectPrototype) {
      var inheritedComponent = getPrototypeOf(sourceComponent);

      if (inheritedComponent && inheritedComponent !== objectPrototype) {
        hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
      }
    }

    var keys = getOwnPropertyNames(sourceComponent);

    if (getOwnPropertySymbols) {
      keys = keys.concat(getOwnPropertySymbols(sourceComponent));
    }

    var targetStatics = getStatics(targetComponent);
    var sourceStatics = getStatics(sourceComponent);

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i];

      if (!KNOWN_STATICS[key] && !(blacklist && blacklist[key]) && !(sourceStatics && sourceStatics[key]) && !(targetStatics && targetStatics[key])) {
        var descriptor = getOwnPropertyDescriptor(sourceComponent, key);

        try {
          // Avoid failures from read-only properties
          defineProperty(targetComponent, key, descriptor);
        } catch (e) {}
      }
    }
  }

  return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isBrowser */
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isBrowser = (typeof window === "undefined" ? "undefined" : _typeof(window)) === "object" && (typeof document === "undefined" ? "undefined" : _typeof(document)) === 'object' && document.nodeType === 9;

/* harmony default export */ __webpack_exports__["a"] = (isBrowser);


/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);



var now = Date.now();
var fnValuesNs = "fnValues" + now;
var fnRuleNs = "fnStyle" + ++now;

var functionPlugin = function functionPlugin() {
  return {
    onCreateRule: function onCreateRule(name, decl, options) {
      if (typeof decl !== 'function') return null;
      var rule = Object(jss__WEBPACK_IMPORTED_MODULE_0__[/* createRule */ "d"])(name, {}, options);
      rule[fnRuleNs] = decl;
      return rule;
    },
    onProcessStyle: function onProcessStyle(style, rule) {
      // We need to extract function values from the declaration, so that we can keep core unaware of them.
      // We need to do that only once.
      // We don't need to extract functions on each style update, since this can happen only once.
      // We don't support function values inside of function rules.
      if (fnValuesNs in rule || fnRuleNs in rule) return style;
      var fnValues = {};

      for (var prop in style) {
        var value = style[prop];
        if (typeof value !== 'function') continue;
        delete style[prop];
        fnValues[prop] = value;
      } // $FlowFixMe[prop-missing]


      rule[fnValuesNs] = fnValues;
      return style;
    },
    onUpdate: function onUpdate(data, rule, sheet, options) {
      var styleRule = rule; // $FlowFixMe[prop-missing]

      var fnRule = styleRule[fnRuleNs]; // If we have a style function, the entire rule is dynamic and style object
      // will be returned from that function.

      if (fnRule) {
        // Empty object will remove all currently defined props
        // in case function rule returns a falsy value.
        styleRule.style = fnRule(data) || {};

        if (false) { var prop; }
      } // $FlowFixMe[prop-missing]


      var fnValues = styleRule[fnValuesNs]; // If we have a fn values map, it is a rule with function values.

      if (fnValues) {
        for (var _prop in fnValues) {
          styleRule.prop(_prop, fnValues[_prop](data), options);
        }
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["a"] = (functionPlugin);


/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



var at = '@global';
var atPrefix = '@global ';

var GlobalContainerRule =
/*#__PURE__*/
function () {
  function GlobalContainerRule(key, styles, options) {
    this.type = 'global';
    this.at = at;
    this.rules = void 0;
    this.options = void 0;
    this.key = void 0;
    this.isProcessed = false;
    this.key = key;
    this.options = options;
    this.rules = new jss__WEBPACK_IMPORTED_MODULE_1__[/* RuleList */ "a"](Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));

    for (var selector in styles) {
      this.rules.add(selector, styles[selector]);
    }

    this.rules.process();
  }
  /**
   * Get a rule.
   */


  var _proto = GlobalContainerRule.prototype;

  _proto.getRule = function getRule(name) {
    return this.rules.get(name);
  }
  /**
   * Create and register rule, run plugins.
   */
  ;

  _proto.addRule = function addRule(name, style, options) {
    var rule = this.rules.add(name, style, options);
    if (rule) this.options.jss.plugins.onProcessRule(rule);
    return rule;
  }
  /**
   * Get index of a rule.
   */
  ;

  _proto.indexOf = function indexOf(rule) {
    return this.rules.indexOf(rule);
  }
  /**
   * Generates a CSS string.
   */
  ;

  _proto.toString = function toString() {
    return this.rules.toString();
  };

  return GlobalContainerRule;
}();

var GlobalPrefixedRule =
/*#__PURE__*/
function () {
  function GlobalPrefixedRule(key, style, options) {
    this.type = 'global';
    this.at = at;
    this.options = void 0;
    this.rule = void 0;
    this.isProcessed = false;
    this.key = void 0;
    this.key = key;
    this.options = options;
    var selector = key.substr(atPrefix.length);
    this.rule = options.jss.createRule(selector, style, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));
  }

  var _proto2 = GlobalPrefixedRule.prototype;

  _proto2.toString = function toString(options) {
    return this.rule ? this.rule.toString(options) : '';
  };

  return GlobalPrefixedRule;
}();

var separatorRegExp = /\s*,\s*/g;

function addScope(selector, scope) {
  var parts = selector.split(separatorRegExp);
  var scoped = '';

  for (var i = 0; i < parts.length; i++) {
    scoped += scope + " " + parts[i].trim();
    if (parts[i + 1]) scoped += ', ';
  }

  return scoped;
}

function handleNestedGlobalContainerRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;
  var rules = style ? style[at] : null;
  if (!rules) return;

  for (var name in rules) {
    sheet.addRule(name, rules[name], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: addScope(name, rule.selector)
    }));
  }

  delete style[at];
}

function handlePrefixedGlobalRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;

  for (var prop in style) {
    if (prop[0] !== '@' || prop.substr(0, at.length) !== at) continue;
    var selector = addScope(prop.substr(at.length), rule.selector);
    sheet.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: selector
    }));
    delete style[prop];
  }
}
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */


function jssGlobal() {
  function onCreateRule(name, styles, options) {
    if (!name) return null;

    if (name === at) {
      return new GlobalContainerRule(name, styles, options);
    }

    if (name[0] === '@' && name.substr(0, atPrefix.length) === atPrefix) {
      return new GlobalPrefixedRule(name, styles, options);
    }

    var parent = options.parent;

    if (parent) {
      if (parent.type === 'global' || parent.options.parent && parent.options.parent.type === 'global') {
        options.scoped = false;
      }
    }

    if (options.scoped === false) {
      options.selector = name;
    }

    return null;
  }

  function onProcessRule(rule, sheet) {
    if (rule.type !== 'style' || !sheet) return;
    handleNestedGlobalContainerRule(rule, sheet);
    handlePrefixedGlobalRule(rule, sheet);
  }

  return {
    onCreateRule: onCreateRule,
    onProcessRule: onProcessRule
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssGlobal);


/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);



var separatorRegExp = /\s*,\s*/g;
var parentRegExp = /&/g;
var refRegExp = /\$([\w-]+)/g;
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */

function jssNested() {
  // Get a function to be used for $ref replacement.
  function getReplaceRef(container, sheet) {
    return function (match, key) {
      var rule = container.getRule(key) || sheet && sheet.getRule(key);

      if (rule) {
        rule = rule;
        return rule.selector;
      }

       false ? undefined : void 0;
      return key;
    };
  }

  function replaceParentRefs(nestedProp, parentProp) {
    var parentSelectors = parentProp.split(separatorRegExp);
    var nestedSelectors = nestedProp.split(separatorRegExp);
    var result = '';

    for (var i = 0; i < parentSelectors.length; i++) {
      var parent = parentSelectors[i];

      for (var j = 0; j < nestedSelectors.length; j++) {
        var nested = nestedSelectors[j];
        if (result) result += ', '; // Replace all & by the parent or prefix & with the parent.

        result += nested.indexOf('&') !== -1 ? nested.replace(parentRegExp, parent) : parent + " " + nested;
      }
    }

    return result;
  }

  function getOptions(rule, container, prevOptions) {
    // Options has been already created, now we only increase index.
    if (prevOptions) return Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, prevOptions, {
      index: prevOptions.index + 1 // $FlowFixMe[prop-missing]

    });
    var nestingLevel = rule.options.nestingLevel;
    nestingLevel = nestingLevel === undefined ? 1 : nestingLevel + 1;

    var options = Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, rule.options, {
      nestingLevel: nestingLevel,
      index: container.indexOf(rule) + 1 // We don't need the parent name to be set options for chlid.

    });

    delete options.name;
    return options;
  }

  function onProcessStyle(style, rule, sheet) {
    if (rule.type !== 'style') return style;
    var styleRule = rule;
    var container = styleRule.options.parent;
    var options;
    var replaceRef;

    for (var prop in style) {
      var isNested = prop.indexOf('&') !== -1;
      var isNestedConditional = prop[0] === '@';
      if (!isNested && !isNestedConditional) continue;
      options = getOptions(styleRule, container, options);

      if (isNested) {
        var selector = replaceParentRefs(prop, styleRule.selector); // Lazily create the ref replacer function just once for
        // all nested rules within the sheet.

        if (!replaceRef) replaceRef = getReplaceRef(container, sheet); // Replace all $refs.

        selector = selector.replace(refRegExp, replaceRef);
        container.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
          selector: selector
        }));
      } else if (isNestedConditional) {
        // Place conditional right after the parent rule to ensure right ordering.
        container.addRule(prop, {}, options) // Flow expects more options but they aren't required
        // And flow doesn't know this will always be a StyleRule which has the addRule method
        // $FlowFixMe[incompatible-use]
        // $FlowFixMe[prop-missing]
        .addRule(styleRule.key, style[prop], {
          selector: styleRule.selector
        });
      }

      delete style[prop];
    }

    return style;
  }

  return {
    onProcessStyle: onProcessStyle
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssNested);


/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);


var px = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.px : 'px';
var ms = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.ms : 'ms';
var percent = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.percent : '%';
/**
 * Generated jss-plugin-default-unit CSS property units
 *
 * @type object
 */

var defaultUnits = {
  // Animation properties
  'animation-delay': ms,
  'animation-duration': ms,
  // Background properties
  'background-position': px,
  'background-position-x': px,
  'background-position-y': px,
  'background-size': px,
  // Border Properties
  border: px,
  'border-bottom': px,
  'border-bottom-left-radius': px,
  'border-bottom-right-radius': px,
  'border-bottom-width': px,
  'border-left': px,
  'border-left-width': px,
  'border-radius': px,
  'border-right': px,
  'border-right-width': px,
  'border-top': px,
  'border-top-left-radius': px,
  'border-top-right-radius': px,
  'border-top-width': px,
  'border-width': px,
  'border-block': px,
  'border-block-end': px,
  'border-block-end-width': px,
  'border-block-start': px,
  'border-block-start-width': px,
  'border-block-width': px,
  'border-inline': px,
  'border-inline-end': px,
  'border-inline-end-width': px,
  'border-inline-start': px,
  'border-inline-start-width': px,
  'border-inline-width': px,
  'border-start-start-radius': px,
  'border-start-end-radius': px,
  'border-end-start-radius': px,
  'border-end-end-radius': px,
  // Margin properties
  margin: px,
  'margin-bottom': px,
  'margin-left': px,
  'margin-right': px,
  'margin-top': px,
  'margin-block': px,
  'margin-block-end': px,
  'margin-block-start': px,
  'margin-inline': px,
  'margin-inline-end': px,
  'margin-inline-start': px,
  // Padding properties
  padding: px,
  'padding-bottom': px,
  'padding-left': px,
  'padding-right': px,
  'padding-top': px,
  'padding-block': px,
  'padding-block-end': px,
  'padding-block-start': px,
  'padding-inline': px,
  'padding-inline-end': px,
  'padding-inline-start': px,
  // Mask properties
  'mask-position-x': px,
  'mask-position-y': px,
  'mask-size': px,
  // Width and height properties
  height: px,
  width: px,
  'min-height': px,
  'max-height': px,
  'min-width': px,
  'max-width': px,
  // Position properties
  bottom: px,
  left: px,
  top: px,
  right: px,
  inset: px,
  'inset-block': px,
  'inset-block-end': px,
  'inset-block-start': px,
  'inset-inline': px,
  'inset-inline-end': px,
  'inset-inline-start': px,
  // Shadow properties
  'box-shadow': px,
  'text-shadow': px,
  // Column properties
  'column-gap': px,
  'column-rule': px,
  'column-rule-width': px,
  'column-width': px,
  // Font and text properties
  'font-size': px,
  'font-size-delta': px,
  'letter-spacing': px,
  'text-decoration-thickness': px,
  'text-indent': px,
  'text-stroke': px,
  'text-stroke-width': px,
  'word-spacing': px,
  // Motion properties
  motion: px,
  'motion-offset': px,
  // Outline properties
  outline: px,
  'outline-offset': px,
  'outline-width': px,
  // Perspective properties
  perspective: px,
  'perspective-origin-x': percent,
  'perspective-origin-y': percent,
  // Transform properties
  'transform-origin': percent,
  'transform-origin-x': percent,
  'transform-origin-y': percent,
  'transform-origin-z': percent,
  // Transition properties
  'transition-delay': ms,
  'transition-duration': ms,
  // Alignment properties
  'vertical-align': px,
  'flex-basis': px,
  // Some random properties
  'shape-margin': px,
  size: px,
  gap: px,
  // Grid properties
  grid: px,
  'grid-gap': px,
  'row-gap': px,
  'grid-row-gap': px,
  'grid-column-gap': px,
  'grid-template-rows': px,
  'grid-template-columns': px,
  'grid-auto-rows': px,
  'grid-auto-columns': px,
  // Not existing properties.
  // Used to avoid issues with jss-plugin-expand integration.
  'box-shadow-x': px,
  'box-shadow-y': px,
  'box-shadow-blur': px,
  'box-shadow-spread': px,
  'font-line-height': px,
  'text-shadow-x': px,
  'text-shadow-y': px,
  'text-shadow-blur': px
};

/**
 * Clones the object and adds a camel cased property version.
 */
function addCamelCasedVersion(obj) {
  var regExp = /(-[a-z])/g;

  var replace = function replace(str) {
    return str[1].toUpperCase();
  };

  var newObj = {};

  for (var _key in obj) {
    newObj[_key] = obj[_key];
    newObj[_key.replace(regExp, replace)] = obj[_key];
  }

  return newObj;
}

var units = addCamelCasedVersion(defaultUnits);
/**
 * Recursive deep style passing function
 */

function iterate(prop, value, options) {
  if (value == null) return value;

  if (Array.isArray(value)) {
    for (var i = 0; i < value.length; i++) {
      value[i] = iterate(prop, value[i], options);
    }
  } else if (typeof value === 'object') {
    if (prop === 'fallbacks') {
      for (var innerProp in value) {
        value[innerProp] = iterate(innerProp, value[innerProp], options);
      }
    } else {
      for (var _innerProp in value) {
        value[_innerProp] = iterate(prop + "-" + _innerProp, value[_innerProp], options);
      }
    }
  } else if (typeof value === 'number' && !Number.isNaN(value)) {
    var unit = options[prop] || units[prop]; // Add the unit if available, except for the special case of 0px.

    if (unit && !(value === 0 && unit === px)) {
      return typeof unit === 'function' ? unit(value).toString() : "" + value + unit;
    }

    return value.toString();
  }

  return value;
}
/**
 * Add unit to numeric values.
 */


function defaultUnit(options) {
  if (options === void 0) {
    options = {};
  }

  var camelCasedOptions = addCamelCasedVersion(options);

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;

    for (var prop in style) {
      style[prop] = iterate(prop, style[prop], camelCasedOptions);
    }

    return style;
  }

  function onChangeValue(value, prop) {
    return iterate(prop, value, camelCasedOptions);
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (defaultUnit);


/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var css_vendor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(201);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



/**
 * Add vendor prefix to a property name when needed.
 *
 * @api public
 */

function jssVendorPrefixer() {
  function onProcessRule(rule) {
    if (rule.type === 'keyframes') {
      var atRule = rule;
      atRule.at = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedKeyframes */ "a"])(atRule.at);
    }
  }

  function prefixStyle(style) {
    for (var prop in style) {
      var value = style[prop];

      if (prop === 'fallbacks' && Array.isArray(value)) {
        style[prop] = value.map(prefixStyle);
        continue;
      }

      var changeProp = false;
      var supportedProp = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedProperty */ "b"])(prop);
      if (supportedProp && supportedProp !== prop) changeProp = true;
      var changeValue = false;
      var supportedValue$1 = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(supportedProp, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value));
      if (supportedValue$1 && supportedValue$1 !== value) changeValue = true;

      if (changeProp || changeValue) {
        if (changeProp) delete style[prop];
        style[supportedProp || prop] = supportedValue$1 || value;
      }
    }

    return style;
  }

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;
    return prefixStyle(style);
  }

  function onChangeValue(value, prop) {
    return Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(prop, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value)) || value;
  }

  return {
    onProcessRule: onProcessRule,
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssVendorPrefixer);


/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Sort props by length.
 */
function jssPropsSort() {
  var sort = function sort(prop0, prop1) {
    if (prop0.length === prop1.length) {
      return prop0 > prop1 ? 1 : -1;
    }

    return prop0.length - prop1.length;
  };

  return {
    onProcessStyle: function onProcessStyle(style, rule) {
      if (rule.type !== 'style') return style;
      var newStyle = {};
      var props = Object.keys(style).sort(sort);

      for (var i = 0; i < props.length; i++) {
        newStyle[props[i]] = style[props[i]];
      }

      return newStyle;
    }
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssPropsSort);


/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/hyphenate-style-name/index.js
/* eslint-disable no-var, prefer-template */
var uppercasePattern = /[A-Z]/g
var msPattern = /^ms-/
var cache = {}

function toHyphenLower(match) {
  return '-' + match.toLowerCase()
}

function hyphenateStyleName(name) {
  if (cache.hasOwnProperty(name)) {
    return cache[name]
  }

  var hName = name.replace(uppercasePattern, toHyphenLower)
  return (cache[name] = msPattern.test(hName) ? '-' + hName : hName)
}

/* harmony default export */ var hyphenate_style_name = (hyphenateStyleName);

// CONCATENATED MODULE: ./node_modules/jss-plugin-camel-case/dist/jss-plugin-camel-case.esm.js


/**
 * Convert camel cased property names to dash separated.
 *
 * @param {Object} style
 * @return {Object}
 */

function convertCase(style) {
  var converted = {};

  for (var prop in style) {
    var key = prop.indexOf('--') === 0 ? prop : hyphenate_style_name(prop);
    converted[key] = style[prop];
  }

  if (style.fallbacks) {
    if (Array.isArray(style.fallbacks)) converted.fallbacks = style.fallbacks.map(convertCase);else converted.fallbacks = convertCase(style.fallbacks);
  }

  return converted;
}
/**
 * Allow camel cased property names by converting them back to dasherized.
 *
 * @param {Rule} rule
 */


function camelCase() {
  function onProcessStyle(style) {
    if (Array.isArray(style)) {
      // Handle rules like @font-face, which can have multiple styles in an array
      for (var index = 0; index < style.length; index++) {
        style[index] = convertCase(style[index]);
      }

      return style;
    }

    return convertCase(style);
  }

  function onChangeValue(value, prop, rule) {
    if (prop.indexOf('--') === 0) {
      return value;
    }

    var hyphenatedProp = hyphenate_style_name(prop); // There was no camel case in place

    if (prop === hyphenatedProp) return value;
    rule.prop(hyphenatedProp, value); // Core will ignore that property value we set the proper one above.

    return null;
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ var jss_plugin_camel_case_esm = __webpack_exports__["a"] = (camelCase);


/***/ }),

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ useAbonos; });
__webpack_require__.d(__webpack_exports__, "b", function() { return /* binding */ useAbonosTotal; });
__webpack_require__.d(__webpack_exports__, "d", function() { return /* binding */ useGetCampaigns; });
__webpack_require__.d(__webpack_exports__, "c", function() { return /* binding */ useArticles; });

// UNUSED EXPORTS: useJuniorByCampaingSenior, useArticlesTop

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/toArray.js
var toArray = __webpack_require__(722);
var toArray_default = /*#__PURE__*/__webpack_require__.n(toArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// CONCATENATED MODULE: ./src/services/importadores/settings.js
var API_URL = "https://point.qreport.site";

// CONCATENATED MODULE: ./src/services/importadores/getGrupales.js




function getAbonos(_x, _x2) {
  return _getAbonos.apply(this, arguments);
}

function _getAbonos() {
  _getAbonos = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee(campaña, id) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            apiURL = "".concat(API_URL, "/abonos/cam/").concat(campaña, "/").concat(id);
            _context.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var amount = data;
                return amount;
              }
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getAbonos.apply(this, arguments);
}

function getAbonosTotal(_x3) {
  return _getAbonosTotal.apply(this, arguments);
}

function _getAbonosTotal() {
  _getAbonosTotal = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee2(id) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/campana/senior_with/s/").concat(id, " ");
            _context2.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var amount = data;
                return amount;
              }
            });

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getAbonosTotal.apply(this, arguments);
}

function getJuniorByCampaingSenior(_x4, _x5) {
  return _getJuniorByCampaingSenior.apply(this, arguments);
}

function _getJuniorByCampaingSenior() {
  _getJuniorByCampaingSenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee3(id_campaña, id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/campana/junior_with/").concat(id_campaña, "/").concat(id_senior, " ");
            _context3.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _getJuniorByCampaingSenior.apply(this, arguments);
}

function getJuniorBySenior(_x6) {
  return _getJuniorBySenior.apply(this, arguments);
}

function _getJuniorBySenior() {
  _getJuniorBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee4(id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/junior_with/s/").concat(id_senior, " ");
            _context4.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _getJuniorBySenior.apply(this, arguments);
}

function getJuniorOrdersBySenior(_x7) {
  return _getJuniorOrdersBySenior.apply(this, arguments);
}

function _getJuniorOrdersBySenior() {
  _getJuniorOrdersBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee5(id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/junior/").concat(id_senior, " ");
            _context5.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context5.abrupt("return", _context5.sent);

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _getJuniorOrdersBySenior.apply(this, arguments);
}

function getCampaigns() {
  return _getCampaigns.apply(this, arguments);
}

function _getCampaigns() {
  _getCampaigns = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee6() {
    return regenerator_default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return fetch("".concat(API_URL, "/campana")).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context6.abrupt("return", _context6.sent);

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _getCampaigns.apply(this, arguments);
}

function getCampaignsBySenior(_x8) {
  return _getCampaignsBySenior.apply(this, arguments);
}

function _getCampaignsBySenior() {
  _getCampaignsBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee7(idSenior) {
    return regenerator_default.a.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return fetch("".concat(API_URL, "/campana/senior/").concat(idSenior)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context7.abrupt("return", _context7.sent);

          case 3:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _getCampaignsBySenior.apply(this, arguments);
}

function getCampaignsByJunior(_x9) {
  return _getCampaignsByJunior.apply(this, arguments);
}

function _getCampaignsByJunior() {
  _getCampaignsByJunior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee8(idSenior) {
    return regenerator_default.a.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return fetch("".concat(API_URL, "/campana/junior/").concat(idSenior)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context8.abrupt("return", _context8.sent);

          case 3:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));
  return _getCampaignsByJunior.apply(this, arguments);
}

function getOrdenByCampaigns(_x10) {
  return _getOrdenByCampaigns.apply(this, arguments);
}

function _getOrdenByCampaigns() {
  _getOrdenByCampaigns = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee9(id_campana) {
    return regenerator_default.a.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return fetch("".concat(API_URL, "/rep/orden/campana/").concat(id_campana)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context9.abrupt("return", _context9.sent);

          case 3:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));
  return _getOrdenByCampaigns.apply(this, arguments);
}

function getArticulosPage() {
  return _getArticulosPage.apply(this, arguments);
}

function _getArticulosPage() {
  _getArticulosPage = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee10() {
    var _ref,
        catalogo,
        _ref$limit,
        limit,
        _ref$page,
        page,
        apiURL,
        _args10 = arguments;

    return regenerator_default.a.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _ref = _args10.length > 0 && _args10[0] !== undefined ? _args10[0] : {}, catalogo = _ref.catalogo, _ref$limit = _ref.limit, limit = _ref$limit === void 0 ? 10 : _ref$limit, _ref$page = _ref.page, page = _ref$page === void 0 ? 0 : _ref$page;
            apiURL = "".concat(API_URL, "/articulos/pag/").concat(catalogo, "/").concat(limit, "/").concat(limit * page);
            _context10.next = 4;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var _response, _response2;

              var data = (_response = response, _response2 = toArray_default()(_response), _response);

              if (Array.isArray(data)) {
                return data;
              }
            });

          case 4:
            return _context10.abrupt("return", _context10.sent);

          case 5:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _getArticulosPage.apply(this, arguments);
}

function getTopArticulos(_x11) {
  return _getTopArticulos.apply(this, arguments);
}

function _getTopArticulos() {
  _getTopArticulos = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee11(catalogo) {
    return regenerator_default.a.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return fetch("".concat(API_URL, "/articulos/top/").concat(catalogo)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context11.abrupt("return", _context11.sent);

          case 3:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));
  return _getTopArticulos.apply(this, arguments);
}
// CONCATENATED MODULE: ./src/services/importadores/index.js

// CONCATENATED MODULE: ./src/hooks/useAbonos.js



function useAbonos(_ref) {
  var campaña = _ref.campaña,
      id = _ref.id;

  var _useState = Object(react["useState"])([]),
      _useState2 = slicedToArray_default()(_useState, 2),
      abonos = _useState2[0],
      setAbonos = _useState2[1];

  var _useState3 = Object(react["useState"])(false),
      _useState4 = slicedToArray_default()(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  Object(react["useEffect"])(function () {
    setLoading(true);
    getAbonos(campaña, id).then(function (abono) {
      setAbonos(abono);
      setLoading(false);
    });
  }, [campaña, id]);
  return {
    loading: loading,
    abonos: abonos
  };
}
function useAbonosTotal(_ref2) {
  var id = _ref2.id;

  var _useState5 = Object(react["useState"])([]),
      _useState6 = slicedToArray_default()(_useState5, 2),
      abonosTotal = _useState6[0],
      setAbonosTotal = _useState6[1];

  var _useState7 = Object(react["useState"])(false),
      _useState8 = slicedToArray_default()(_useState7, 2),
      loadingTotal = _useState8[0],
      setLoading = _useState8[1];

  Object(react["useEffect"])(function () {
    setLoading(true);
    getAbonosTotal(id).then(function (abono) {
      setAbonosTotal(abono);
      setLoading(false);
    });
  }, [id]);
  return {
    loadingTotal: loadingTotal,
    abonosTotal: abonosTotal
  };
}
function useJuniorByCampaingSenior(_ref3) {
  var idCampaña = _ref3.idCampaña,
      idSenior = _ref3.idSenior,
      tipo = _ref3.tipo;

  var _useState9 = Object(react["useState"])([]),
      _useState10 = slicedToArray_default()(_useState9, 2),
      seniorList = _useState10[0],
      setSeniorList = _useState10[1];

  var _useState11 = Object(react["useState"])(false),
      _useState12 = slicedToArray_default()(_useState11, 2),
      loading = _useState12[0],
      setLoading = _useState12[1];

  if (tipo === 1) {
    Object(react["useEffect"])(function () {
      getJuniorOrdersBySenior(idSenior).then(function (senior) {
        setSeniorList(senior);

        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  } else {
    Object(react["useEffect"])(function () {
      getJuniorByCampaingSenior(idCampaña, idSenior).then(function (senior) {
        setSeniorList(senior);

        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  }

  return {
    loading: loading,
    seniorList: seniorList
  };
}
function useGetCampaigns(_ref4) {
  var idSenior = _ref4.idSenior,
      tipo = _ref4.tipo;

  var _useState13 = Object(react["useState"])([]),
      _useState14 = slicedToArray_default()(_useState13, 2),
      Campaigns = _useState14[0],
      setCampaigns = _useState14[1];

  var _useState15 = Object(react["useState"])(false),
      _useState16 = slicedToArray_default()(_useState15, 2),
      loadingCampaigns = _useState16[0],
      setLoading = _useState16[1];

  if (tipo === 1) {
    Object(react["useEffect"])(function () {
      setLoading(true);
      getCampaignsByJunior(idSenior).then(function (campaña) {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  } else {
    Object(react["useEffect"])(function () {
      setLoading(true);
      getCampaignsBySenior(idSenior).then(function (campaña) {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  }

  return {
    loadingCampaigns: loadingCampaigns,
    Campaigns: Campaigns
  };
}
var INITIAL_PAGE = 0;
function useArticles(_ref5) {
  var catalogo = _ref5.catalogo;

  var _useState17 = Object(react["useState"])([]),
      _useState18 = slicedToArray_default()(_useState17, 2),
      articulo = _useState18[0],
      setArticulo = _useState18[1];

  var _useState19 = Object(react["useState"])(0),
      _useState20 = slicedToArray_default()(_useState19, 2),
      limite = _useState20[0],
      setLimite = _useState20[1];

  var _useState21 = Object(react["useState"])(false),
      _useState22 = slicedToArray_default()(_useState21, 2),
      loading = _useState22[0],
      setLoading = _useState22[1];

  var _useState23 = Object(react["useState"])(false),
      _useState24 = slicedToArray_default()(_useState23, 2),
      loadingNextPage = _useState24[0],
      setLoadingNextPage = _useState24[1];

  var _useState25 = Object(react["useState"])(INITIAL_PAGE),
      _useState26 = slicedToArray_default()(_useState25, 2),
      page = _useState26[0],
      setPage = _useState26[1];

  Object(react["useEffect"])(function () {
    setLoading(true); //Recuperamos la keyword del localStorage

    getArticulosPage({
      catalogo: catalogo
    }).then(function (articulo) {
      setArticulo(articulo);
      setLimite(articulo[0].limite);
      setLoading(false);
    });
  }, [catalogo]);
  Object(react["useEffect"])(function () {
    if (page !== INITIAL_PAGE && limite / articulo.length !== 1) {
      setLoadingNextPage(true);
      getArticulosPage({
        catalogo: catalogo,
        page: page
      }).then(function (nextarticulo) {
        setArticulo(function (prevarticulo) {
          return prevarticulo.concat(nextarticulo);
        });
        setLoadingNextPage(false);
      });
    }
  }, [catalogo, page]);
  return {
    loading: loading,
    loadingNextPage: loadingNextPage,
    articulo: articulo,
    setPage: setPage,
    limite: limite,
    page: page
  };
}
function useArticlesTop(catalogo) {
  var _useState27 = Object(react["useState"])([]),
      _useState28 = slicedToArray_default()(_useState27, 2),
      articulosTop = _useState28[0],
      setArticulo = _useState28[1];

  var _useState29 = Object(react["useState"])(false),
      _useState30 = slicedToArray_default()(_useState29, 2),
      loadingTop = _useState30[0],
      setLoading = _useState30[1];

  Object(react["useEffect"])(function () {
    setLoading(true); //Recuperamos la keyword del localStorage

    getTopArticulos(catalogo).then(function (articulosTop) {
      setArticulo(articulosTop);
      setLoading(false);
    });
  }, [catalogo]);
  return {
    loadingTop: loadingTop,
    articulosTop: articulosTop
  };
}

/***/ }),

/***/ 912:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

var _interopRequireWildcard = __webpack_require__(141);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var React = _interopRequireWildcard(__webpack_require__(1));

var _createSvgIcon = _interopRequireDefault(__webpack_require__(153));

var _default = (0, _createSvgIcon.default)( /*#__PURE__*/React.createElement("path", {
  d: "M11 9h2V6h3V4h-3V1h-2v3H8v2h3v3zm-4 9c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2zm-9.83-3.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.86-7.01L19.42 4h-.01l-1.1 2-2.76 5H8.53l-.13-.27L6.16 6l-.95-2-.94-2H1v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.13 0-.25-.11-.25-.25z"
}), 'AddShoppingCart');

exports.default = _default;

/***/ })

}]);
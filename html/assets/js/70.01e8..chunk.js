(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[70],{

/***/ 1461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(52);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(136);
/* harmony import */ var Util_helmet__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(182);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var condiciones = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(condiciones, _Component);

  var _super = _createSuper(condiciones);

  function condiciones(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, condiciones);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(condiciones, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Util_helmet__WEBPACK_IMPORTED_MODULE_9__[/* Title */ "a"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("title", null, window.location.pathname.replace("/", "") + " | " + "PIC  - Calculadora de Fletes")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "container2 "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__[/* Row */ "L"], {
        className: "h-100 ",
        id: "link3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        className: "mx-auto text-justify "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "T\xE9rminos y Condiciones de Uso"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "INFORMACI\xD3N RELEVANTE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Es requisito necesario para la adquisici\xF3n de los productos que se ofrecen en este sitio, que lea y acepte los siguientes T\xE9rminos y Condiciones que a continuaci\xF3n se redactan. El uso de nuestros servicios as\xED como la compra de nuestros productos implicar\xE1 que usted ha le\xEDdo y aceptado los T\xE9rminos y Condiciones de Uso en el presente documento. Todas los productos que son ofrecidos por nuestro sitio web pudieran ser creadas, cobradas, enviadas o presentadas por una p\xE1gina web tercera y en tal caso estar\xEDan sujetas a sus propios T\xE9rminos y Condiciones. En algunos casos, para adquirir un producto, ser\xE1 necesario el registro por parte del usuario, con ingreso de datos personales fidedignos y definici\xF3n de una contrase\xF1a."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "El usuario puede elegir y cambiar la clave para su acceso de administraci\xF3n de la cuenta en cualquier momento, en caso de que se haya registrado y que sea necesario para la compra de alguno de nuestros productos. https://calculadoradefletes.com/ no asume la responsabilidad en caso de que entregue dicha clave a terceros."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Todas las compras y transacciones que se lleven a cabo por medio de este sitio web, est\xE1n sujetas a un proceso de confirmaci\xF3n y verificaci\xF3n, el cual podr\xEDa incluir la verificaci\xF3n del stock y disponibilidad de producto, validaci\xF3n de la forma de pago, validaci\xF3n de la factura (en caso de existir) y el cumplimiento de las condiciones requeridas por el medio de pago seleccionado. En algunos casos puede que se requiera una verificaci\xF3n por medio de correo electr\xF3nico."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "LICENCIA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "PIC a trav\xE9s de su sitio web concede una licencia para que los usuarios utilicen los productos que son vendidos en este sitio web de acuerdo a los T\xE9rminos y Condiciones que se describen en este documento."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "USO NO AUTORIZADO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "En caso de que aplique (para venta de software, templetes, u otro producto de dise\xF1o y programaci\xF3n) usted no puede colocar uno de nuestros productos, modificado o sin modificar, en un CD, sitio web o ning\xFAn otro medio y ofrecerlos para la redistribuci\xF3n o la reventa de ning\xFAn tipo."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "PROPIEDAD"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Usted no puede declarar propiedad intelectual o exclusiva a ninguno de nuestros productos, modificado o sin modificar. Todos los productos son propiedad de los proveedores del contenido. En caso de que no se especifique lo contrario, nuestros productos se proporcionan sin ning\xFAn tipo de garant\xEDa, expresa o impl\xEDcita. En ning\xFAn esta compa\xF1\xEDa ser\xE1 responsables de ning\xFAn da\xF1o incluyendo, pero no limitado a, da\xF1os directos, indirectos, especiales, fortuitos o consecuentes u otras p\xE9rdidas resultantes del uso o de la imposibilidad de utilizar nuestros productos."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "COMPROBACI\xD3N ANTIFRAUDE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "La compra del cliente puede ser aplazada para la comprobaci\xF3n antifraude. Tambi\xE9n puede ser suspendida por m\xE1s tiempo para una investigaci\xF3n m\xE1s rigurosa, para evitar transacciones fraudulentas."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "PRIVACIDAD"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Este ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("a", {
        href: "",
        target: "_blank"
      }), " ", "https://calculadoradefletes.com/ garantiza que la informaci\xF3n personal que usted env\xEDa cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validaci\xF3n de los pedidos no ser\xE1n entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "PIC reserva los derechos de cambiar o de modificar estos t\xE9rminos sin previo aviso.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__[/* Colxx */ "a"], {
        className: "text-center pb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], {
        to: "/",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__[/* Button */ "c"], {
        color: "success",
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER Al Inicio", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })))))));
    }
  }]);

  return condiciones;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (condiciones);

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ })

}]);
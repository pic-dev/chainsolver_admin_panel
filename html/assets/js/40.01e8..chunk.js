(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40,9,19],{

/***/ 148:
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(250);

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ 1530:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/toConsumableArray.js
var toConsumableArray = __webpack_require__(670);
var toConsumableArray_default = /*#__PURE__*/__webpack_require__.n(toConsumableArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(17);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js
var objectWithoutProperties = __webpack_require__(783);
var objectWithoutProperties_default = /*#__PURE__*/__webpack_require__.n(objectWithoutProperties);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);
var prop_types_default = /*#__PURE__*/__webpack_require__.n(prop_types);

// EXTERNAL MODULE: ./node_modules/react-text-mask/dist/reactTextMask.js
var reactTextMask = __webpack_require__(939);
var reactTextMask_default = /*#__PURE__*/__webpack_require__.n(reactTextMask);

// EXTERNAL MODULE: ./node_modules/text-mask-addons/dist/createNumberMask.js
var createNumberMask = __webpack_require__(940);
var createNumberMask_default = /*#__PURE__*/__webpack_require__.n(createNumberMask);

// CONCATENATED MODULE: ./src/util/CurrencyInput.js




function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { defineProperty_default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }





var defaultMaskOptions = {
  prefix: '',
  suffix: ' USD',
  includeThousandsSeparator: true,
  thousandsSeparatorSymbol: '.',
  allowDecimal: true,
  decimalSymbol: '',
  decimalLimit: 0,
  // how many digits allowed after the decimal
  integerLimit: 7,
  // limit length of integer numbers
  allowNegative: false,
  allowLeadingZeroes: false
};

var CurrencyInput_CurrencyInput = function CurrencyInput(_ref) {
  var maskOptions = _ref.maskOptions,
      inputProps = objectWithoutProperties_default()(_ref, ["maskOptions"]);

  var currencyMask = createNumberMask_default()(_objectSpread(_objectSpread({}, defaultMaskOptions), maskOptions));
  return /*#__PURE__*/react_default.a.createElement(reactTextMask_default.a, extends_default()({
    mask: currencyMask
  }, inputProps));
};

CurrencyInput_CurrencyInput.defaultProps = {
  inputMode: 'numeric',
  maskOptions: {}
};
CurrencyInput_CurrencyInput.propTypes = {
  inputmode: prop_types_default.a.string,
  maskOptions: prop_types_default.a.shape({
    prefix: prop_types_default.a.string,
    suffix: prop_types_default.a.string,
    includeThousandsSeparator: prop_types_default.a.boolean,
    thousandsSeparatorSymbol: prop_types_default.a.string,
    allowDecimal: prop_types_default.a.boolean,
    decimalSymbol: prop_types_default.a.string,
    decimalLimit: prop_types_default.a.string,
    requireDecimal: prop_types_default.a.boolean,
    allowNegative: prop_types_default.a.boolean,
    allowLeadingZeroes: prop_types_default.a.boolean,
    integerLimit: prop_types_default.a.number
  })
};
/* harmony default export */ var util_CurrencyInput = (CurrencyInput_CurrencyInput);
// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Redirect.js + 2 modules
var Redirect = __webpack_require__(138);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Link.js
var Link = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/withRouter.js
var withRouter = __webpack_require__(249);

// EXTERNAL MODULE: ./src/components/CustomBootstrap/index.js
var CustomBootstrap = __webpack_require__(52);

// EXTERNAL MODULE: ./node_modules/react-bootstrap-typeahead/css/Typeahead.css
var Typeahead = __webpack_require__(80);

// EXTERNAL MODULE: ./node_modules/react-bootstrap-typeahead/lib/index.js
var lib = __webpack_require__(511);

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__(79);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// EXTERNAL MODULE: ./src/util/timeLine.js
var timeLine = __webpack_require__(671);

// EXTERNAL MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js + 1 modules
var es = __webpack_require__(339);

// EXTERNAL MODULE: ./src/util/Firebase.js + 4 modules
var Firebase = __webpack_require__(11);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// EXTERNAL MODULE: ./src/util/Errores.js
var Errores = __webpack_require__(615);

// EXTERNAL MODULE: ./src/util/tracking.js + 12 modules
var tracking = __webpack_require__(20);

// CONCATENATED MODULE: ./src/util/cadenaLogistica.js






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var cadenaLogistica_Cadena = /*#__PURE__*/function (_Component) {
  inherits_default()(Cadena, _Component);

  var _super = _createSuper(Cadena);

  function Cadena(props) {
    classCallCheck_default()(this, Cadena);

    return _super.call(this, props);
  }

  createClass_default()(Cadena, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mb-2 NoPadding mx-auto mt-2 display-sm",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle"
      }, "CADENA LOGISTICA"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mt-2 mx-auto timeLine"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-fabricaDo mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-transMaritimo mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-transAereo mr-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma2 mr-2",
        style: {
          display: JSON.parse(localStorage.check) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma3 mr-2",
        style: {
          display: JSON.parse(localStorage.check) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana2 mr-2",
        style: {
          display: !JSON.parse(localStorage.check2) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana3 mr-2",
        style: {
          display: !JSON.parse(localStorage.check2) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte2 mr-2",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte3 mr-2",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-almacen2 mr-2",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-almacen3 mr-2",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-lock mr-2",
        style: {
          fontSize: "22px",
          fontWeight: "bold",
          color: JSON.parse(localStorage.check4) === true ? "#b4b4b4" : "#275fc7"
        }
      }))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline ml-2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2",
        style: {
          display: JSON.parse(localStorage.check) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3",
        style: {
          display: JSON.parse(localStorage.check) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2",
        style: {
          display: !JSON.parse(localStorage.check2) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3",
        style: {
          display: !JSON.parse(localStorage.check2) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3",
        style: {
          display: JSON.parse(localStorage.check4) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2",
        style: {
          display: JSON.parse(localStorage.check4) === true ? "none" : "initial"
        }
      }))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto pt-3 NoPadding",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle mr-3"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline mr-1"
      }), "Pagado por t\xFA proveedor"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle mr-3"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline2 mr-1"
      }), "Cotizado"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline3 mr-1"
      }), "No cotizado")))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "pt-1 pb-1 display-large",
        style: {
          textAlign: "center",
          borderBottom: "1px solid rgba(0, 0, 0, 0.18)"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle"
      }, "CADENA LOGISTICA"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mt-1  mx-auto"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-fabricaDo mx-timeline",
        style: {
          marginLeft: "1em"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte mx-timeline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana mx-timeline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma mx-timeline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-transMaritimo mx-timeline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-transAereo mx-timeline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma2 mx-timeline",
        style: {
          display: JSON.parse(localStorage.check) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-puertoYalma3 mx-timeline",
        style: {
          display: JSON.parse(localStorage.check) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana2 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check2) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-aduana3 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check2) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte2 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-transporte3 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-almacen2 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-almacen3 mx-timeline",
        style: {
          display: !JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-lock",
        style: {
          fontSize: "30px",
          fontWeight: "bold",
          color: JSON.parse(localStorage.check4) === true ? "#b4b4b4" : "#275fc7"
        }
      }))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline ",
        style: {
          marginLeft: "0%"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Oline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline"
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline",
        style: {
          display: JSON.parse(localStorage.check) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Greyline",
        style: {
          display: JSON.parse(localStorage.check) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline",
        style: {
          display: JSON.parse(localStorage.check2) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Greyline",
        style: {
          display: JSON.parse(localStorage.check2) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline",
        style: {
          display: JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Greyline",
        style: {
          display: JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline",
        style: {
          display: JSON.parse(localStorage.check1) === false ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Greyline",
        style: {
          display: JSON.parse(localStorage.check1) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Gline",
        style: {
          display: JSON.parse(localStorage.check4) === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-Greyline",
        style: {
          display: JSON.parse(localStorage.check4) === false ? "none" : "initial"
        }
      }))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "my-auto mx-auto"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText ml-3 mr-1"
      }, "FABRICA PROVEEDOR"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1"
      }, "TRANSPORTE EN ORIGEN"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1"
      }, "ADUANA EN ORIGEN"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1"
      }, "PUERTO EN ORIGEN"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none",
          color: "#275fc7"
        }
      }, "FLETE MARIT\xCDMO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none",
          color: "#275fc7"
        }
      }, "FLETE A\xC9REO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          color: JSON.parse(localStorage.check) === true ? "#275fc7" : "#b4b4b4 "
        }
      }, "PUERTO Y ALMACEN DESTINO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          color: JSON.parse(localStorage.check2) === true ? "#275fc7" : "#b4b4b4 "
        }
      }, "ADUANA DESTIDO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          color: JSON.parse(localStorage.check1) === true ? "#275fc7" : "#b4b4b4 "
        }
      }, "TRANSPORTE DESTINO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText mr-1",
        style: {
          color: JSON.parse(localStorage.check1) === true ? "#275fc7" : "#b4b4b4 "
        }
      }, "ALMACEN IMPORTADOR"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "timeLineText",
        style: {
          color: JSON.parse(localStorage.check4) === false ? "#275fc7" : "#b4b4b4 "
        }
      }, "SEGURO DE MERCANCIA")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2 mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle mr-5"
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-timeLineText2 "
      }), "Pagado por t\xFA proveedor"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle mr-5"
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-timeLineText2-G"
      }), "Cotizado"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "timeLineTitle"
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-timeLineText2-Grey"
      }), "No cotizado")))), " ");
    }
  }]);

  return Cadena;
}(react["Component"]);

/* harmony default export */ var cadenaLogistica = (cadenaLogistica_Cadena);
// EXTERNAL MODULE: ./node_modules/react-beforeunload/lib/index.esm.js + 2 modules
var index_esm = __webpack_require__(773);

// EXTERNAL MODULE: ./src/util/helmet.js
var helmet = __webpack_require__(182);

// EXTERNAL MODULE: ./src/util/chat.js
var chat = __webpack_require__(341);

// CONCATENATED MODULE: ./src/routes/pages/calculadoraFletes/Servicios.js











function Servicios_createSuper(Derived) { var hasNativeReflectConstruct = Servicios_isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function Servicios_isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



















var ref = /*#__PURE__*/Object(react["createRef"])();
var timer;
var valid;
var validated;

var Servicios_servicios = /*#__PURE__*/function (_Component) {
  inherits_default()(servicios, _Component);

  var _super = Servicios_createSuper(servicios);

  function servicios(props) {
    var _this;

    classCallCheck_default()(this, servicios);

    _this = _super.call(this, props);

    defineProperty_default()(assertThisInitialized_default()(_this), "handleOnChangeProvi", function (provinceSelected) {
      ref.current.clear();

      _this.setState(function () {
        return {
          district: []
        };
      });

      var tipoTransporte;

      if (localStorage.tipocon == "Contenedor completo") {
        tipoTransporte = "MARITIMO FCL";
      } else if (localStorage.tipocon == "Contenedor compartido") {
        tipoTransporte = "MARITIMO LCL";
      } else {
        tipoTransporte = "MARITIMO LCL";
      }

      if (provinceSelected.length > 0) {
        var id = provinceSelected.map(function (item) {
          return item.id.toString();
        });
        var nombre = provinceSelected.map(function (item) {
          return item.nombre.toString();
        });

        if (localStorage.transporte == "MARITÍMO") {
          axios_default.a.post(_this.props.api + "maritimo_v2/get_dis_mar", {
            prov: [id[0], tipoTransporte]
          }).then(function (res) {
            var response;
            var data = res.data.data.r_dat;

            if (nombre == "PERÚ - OTRAS PROVINCIAS") {
              response = [{
                id: 20,
                nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL"
              }];

              _this.setState(function () {
                return {
                  msjToggle: true,
                  districtSelected: [{
                    id: 20,
                    nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL"
                  }],
                  district: response
                };
              });
            } else {
              _this.setState(function () {
                return {
                  district: data
                };
              });
            }
          }).catch(function (error) {
            console.log(error);
            Object(Utils["d" /* errores */])("ApipostDistritos", error);
          });
        } else {
          axios_default.a.post(_this.props.api + "aereo_v2/get_dis_aereo", {
            prov: [id[0]]
          }).then(function (res) {
            var response;
            var data = res.data.data.r_dat;

            if (nombre == "PERÚ - OTRAS PROVINCIAS") {
              response = [{
                id: 20,
                nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL"
              }];

              _this.setState(function () {
                return {
                  msjToggle: true,
                  districtSelected: [{
                    id: 20,
                    nombre: "AGENCIA DE ENVÍOS INTERPROVINCIAL"
                  }],
                  district: response
                };
              });
            } else {
              _this.setState(function () {
                return {
                  district: data
                };
              });
            }
          }).catch(function (error) {
            Object(Utils["d" /* errores */])("ApipostDistritos", error);
          }); // ***** EJEMPLO 3 llamada a url que recibe parametros, en este caso se asigna valor a la variable data
          // DISTRITOS
          // PERMISOS
        }
      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "handleOnChange", function (input) {
      if (input.value == "SI") {
        _this.setState(function () {
          return {
            check3: false
          };
        });
      } else {
        _this.setState(function () {
          return {
            check3: true
          };
        });
      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "invitado", function () {
      Object(tracking["a" /* Event */])("Ingresar como invitado", "Ingresar como invitado", "SERVICIOS");
      var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
      sessionStorage.setItem("user", JSON.stringify(user));
      sessionStorage.usuario = "2";

      _this.setState({
        IsVisiblePreview: false
      });

      _this.props.history.push("/Presupuesto");

      window.location.reload();
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "correoClave", /*#__PURE__*/function () {
      var _ref = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee(e) {
        var _e$target$elements, usuario, clave;

        return regenerator_default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements = e.target.elements, usuario = _e$target$elements.usuario, clave = _e$target$elements.clave;

                if (!(usuario.value.length > 0 && clave.value.length > 0)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 6;
                return Firebase["a" /* firebaseConf */].auth().signInWithEmailAndPassword(usuario.value, clave.value).then(function (result) {
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));

                  _this.setState({
                    IsVisiblePreview: false
                  });

                  _this.props.history.push("/Presupuesto");
                }).catch(function (error) {
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));

                  _this.setState({
                    IsVisiblePreview: false
                  });

                  _this.props.history.push("/Presupuesto");

                  Object(Utils["d" /* errores */])("Logincorreo", error);
                });

              case 6:
                _context.next = 10;
                break;

              case 8:
                _this.seterror("Debe llenar todos los campos requeridos");

                timer = setTimeout(function () {
                  _this.setState({
                    error: ""
                  });
                }, 4000);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    defineProperty_default()(assertThisInitialized_default()(_this), "handleSignUp", /*#__PURE__*/function () {
      var _ref2 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee2(e) {
        var _e$target$elements2, telefono, usuario, clave, Confirmacion;

        return regenerator_default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements2 = e.target.elements, telefono = _e$target$elements2.telefono, usuario = _e$target$elements2.usuario, clave = _e$target$elements2.clave, Confirmacion = _e$target$elements2.Confirmacion;

                if (!(usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value === Confirmacion.value)) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 6;
                return Firebase["a" /* firebaseConf */].auth().createUserWithEmailAndPassword(usuario.value, clave.value).then(function (result) {
                  if (telefono.value.length > 0) {
                    if (Object(Utils["r" /* telefonoValid */])(telefono.value)) {
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));

                      _this.setState({
                        IsVisiblePreview: false
                      });

                      localStorage.telefono = telefono.value;
                      timer = setTimeout(function () {
                        _this.props.history.push("/Presupuesto");
                      }, 1000);
                    } else {
                      var alerta = "Verifique numero de telefono";
                      alert(alerta);
                    }
                  } else {
                    sessionStorage.usuario = "2";
                    var _user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(_user));

                    _this.setState({
                      IsVisiblePreview: false
                    });

                    timer = setTimeout(function () {
                      _this.props.history.push("/Presupuesto");
                    }, 1000);
                  }
                }).catch(function (error) {
                  if (telefono.value.length > 0) {
                    if (telefono.value.length > 0 && Object(Utils["r" /* telefonoValid */])(telefono.value)) {
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));

                      _this.setState({
                        IsVisiblePreview: false
                      });

                      localStorage.telefono = telefono.value;
                      timer = setTimeout(function () {
                        _this.props.history.push("/Presupuesto");
                      }, 1000);
                    } else {
                      alerta = "Verifique numero de telefono";
                      alert(alerta);
                    }
                  } else {
                    sessionStorage.usuario = "2";
                    var _user2 = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(_user2));

                    _this.setState({
                      IsVisiblePreview: false
                    });

                    timer = setTimeout(function () {
                      _this.props.history.push("/Presupuesto");
                    }, 1000);
                  }

                  Object(Utils["d" /* errores */])("RegistroCorreo", error);

                  _this.seterror(Object(Utils["a" /* ErrorEspa */])(error));
                });

              case 6:
                _context2.next = 9;
                break;

              case 8:
                if (usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value !== Confirmacion.value) {
                  _this.seterror("La contraseña ingresada no coincide con la confimación");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                } else {
                  _this.seterror("Debe llenar todos los campos requeridos");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                }

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());

    defineProperty_default()(assertThisInitialized_default()(_this), "socialLogin", function (provider, e) {
      e.preventDefault();

      _this.setState({
        error: ""
      });

      var x;
      Firebase["a" /* firebaseConf */].auth().setPersistence(Firebase["c" /* persistencia */]).then(function () {
        asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee3() {
          return regenerator_default.a.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return Firebase["a" /* firebaseConf */].auth().signInWithPopup(provider).then(function (result) {
                    /*Event(
                      "GoogleLoginHeader",
                      "Usuario ingresó con botton de google",
                      "SERVICIOS"
                    );*/
                    var user = [[result.additionalUserInfo.profile.id], [result.additionalUserInfo.profile.name], [result.additionalUserInfo.profile.email], [result.additionalUserInfo.profile.picture]];
                    sessionStorage.setItem("user", JSON.stringify(user));
                    sessionStorage.usuario = "2";

                    _this.props.history.push("/Presupuesto");
                  }).catch(function (error) {
                    sessionStorage.usuario = "2";
                    var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(user));
                  });

                case 2:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }))();
      }).catch(function (error) {});
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "eventVolver", function () {
      /*switch (localStorage.tipocon) {
        case "Contenedor completo":
          Event(
            "2.8 Volver a Servcios FCL",
            "2.8 Volver a Servcios FCL",
            "SERVICIOS"
          );
          break;
        case "Contenedor compartido":
          Event(
            "1.9 Volver a Servcios LCL",
            "1.9 Volver a Servcios LCL",
            "SERVICIOS"
          );
          break;
        case "Aereo":
          Event(
            "3.9 Volver a Servcios AEREO",
            "3.9 Volver a Servcios AEREO",
            "SERVICIOS"
          );
          break;
         default:
      }*/
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "eventIrPresupuestos", function () {
      /*switch (localStorage.tipocon) {
        case "Contenedor completo":
          Event(
            "2.9 Ir a Presupuestos FCL",
            "2.9 Ir a Presupuestos FCL",
            "SERVICIOS"
          );
          break;
        case "Contenedor compartido":
          Event(
            "1.9.1 Ir a Presupuestos LCL",
            "1.9.1 Ir a Presupuestos LCL",
            "SERVICIOS"
          );
          break;
        case "Aereo":
          Event(
            "3.9.1 Ir a Presupuestos AEREO",
            "3.9.1 Ir a Presupuestos AEREO",
            "SERVICIOS"
          );
          break;
         default:
      }*/
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "eventCalcularCotizacion", function () {
      /*if (localStorage.tipocon == "Contenedor compartido") {
        Event(
          "1.8 Calcular Cotizacion LCL",
          "1.8 Calcular Cotizacion LCL",
          "SERVICIOS"
        );
      } else if (localStorage.tipocon == "Contenedor completo") {
        Event(
          "2.7 Calcular Cotizacion FCL",
          "2.7 Calcular Cotizacion FCL",
          "SERVICIOS"
        );
      } else {
        Event(
          "3.8 Calcular Cotizacion Aereo",
          "3.8 Calcular Cotizacion Aereo",
          "SERVICIOS"
        );
      }*/
    });

    _this.toggle = _this.toggle.bind(assertThisInitialized_default()(_this));
    _this.toggleModalPreview = _this.toggleModalPreview.bind(assertThisInitialized_default()(_this));
    _this.confirmation = _this.confirmation.bind(assertThisInitialized_default()(_this));
    _this.handleFormState = _this.handleFormState.bind(assertThisInitialized_default()(_this));
    _this.checkbox = _this.checkbox.bind(assertThisInitialized_default()(_this));
    _this.get_ctz_resumen = _this.get_ctz_resumen.bind(assertThisInitialized_default()(_this));
    _this.find_product_perm = _this.find_product_perm.bind(assertThisInitialized_default()(_this));
    _this.get_prov = _this.get_prov.bind(assertThisInitialized_default()(_this));
    _this.get_dist = _this.get_dist.bind(assertThisInitialized_default()(_this));
    _this.array_js_to_pg = _this.array_js_to_pg.bind(assertThisInitialized_default()(_this));
    _this.validation = _this.validation.bind(assertThisInitialized_default()(_this));
    _this.showMessage = _this.showMessage.bind(assertThisInitialized_default()(_this));
    _this.SelectedOnce = _this.SelectedOnce.bind(assertThisInitialized_default()(_this));
    _this.socialLogin = _this.socialLogin.bind(assertThisInitialized_default()(_this));
    _this.correoClave = _this.correoClave.bind(assertThisInitialized_default()(_this));
    _this.handleSignUp = _this.handleSignUp.bind(assertThisInitialized_default()(_this));
    _this.setsignup = _this.setsignup.bind(assertThisInitialized_default()(_this));
    _this.seterror = _this.seterror.bind(assertThisInitialized_default()(_this));
    _this.handleOnChange = _this.handleOnChange.bind(assertThisInitialized_default()(_this));
    _this.invitado = _this.invitado.bind(assertThisInitialized_default()(_this));
    _this.handleOnChangeProvi = _this.handleOnChangeProvi.bind(assertThisInitialized_default()(_this));
    _this.state = {
      eventBandera: false,
      msjToggle: false,
      Signup: false,
      showPer: false,
      signInChoose: false,
      Choose: true,
      agotadoChoose: false,
      error: "",
      authAlert: false,
      isOpen: false,
      showMessage: false,
      IsVisiblePreview: false,
      confirmation: false,
      check: true,
      check1: true,
      check2: true,
      check3: true,
      check4: true,
      isLoggedIng: false,
      isLoggedInf: false,
      userID: "",
      name: "",
      email: "",
      picture: "",
      validated1: false,
      validated2: false,
      validated3: false,
      validated4: false,
      errorProv: true,
      productPermSelected: [],
      provinceSelected: [],
      districtSelected: [],
      province: [],
      district: [],
      productPerm: [],
      result: [],
      iconPuertoYalma: "icon-puertoYalma3",
      aduana: "icon-aduana2",
      Transporte: "icon-transporte2",
      animated: "icon-puertoYalmaAnimate",
      animated2: "icon-aduanaAnimate",
      animated3: "icon-transporteAnimate",
      animated4: "icon-seguroAnimate",
      form: [{
        Label: "value",
        value: ""
      }, {
        Label: "origin",
        value: ""
      }, {
        Label: "destination",
        value: ""
      }, {
        Label: "freightType",
        value: ""
      }, {
        Label: "container2",
        value: ""
      }, {
        Label: "container2",
        value: ""
      }]
    };
    return _this;
  }

  createClass_default()(servicios, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      localStorage.page = "3";

      if (true) {
        Object(tracking["b" /* PageView */])();
      }

      localStorage.removeItem("token");

      if (localStorage.transporte == undefined) {
        return /*#__PURE__*/react_default.a.createElement(Redirect["a" /* default */], {
          to: "/"
        });
      } else {
        var element = document.getElementById("link3");
        Object(es["a" /* default */])(element, {
          behavior: "smooth",
          block: "center",
          inline: "center"
        });
      }

      var destino = localStorage.destino;
      var tipoTransporte;

      if (localStorage.tipocon == "Contenedor completo") {
        tipoTransporte = "MARITIMO FCL";
      } else if (localStorage.tipocon == "Contenedor compartido") {
        tipoTransporte = "MARITIMO LCL";
      } else {
        tipoTransporte = "";
      }

      if (localStorage.transporte == "MARITÍMO") {
        axios_default.a.post(this.props.api + "maritimo_v2/get_prov_mar", {
          prov: [destino, tipoTransporte]
        }).then(function (res) {
          var response;
          var data = res.data.data.r_dat;
          var estado = res.data.data.r_suc;

          if (estado) {
            if (localStorage.puertoD == "PERÚ - OTRAS PROVINCIAS") {
              response = [].concat(toConsumableArray_default()(data), [{
                id: 15,
                nombre: "PERÚ - OTRAS PROVINCIAS"
              }]);
            } else {
              response = data;
            }

            _this2.setState({
              province: response
            });
          } else {
            _this2.setState({
              errorProv: false
            });
          }
        }).catch(function (error) {
          console.log(error);
          Object(Utils["d" /* errores */])("ApipostProvincias", error);
        });
      } else {
        axios_default.a.post(this.props.api + "aereo_v2/get_prov_aereo", {
          prov: [destino]
        }).then(function (res) {
          var response;
          var data = res.data.data.r_dat;
          var estado = res.data.data.r_suc;

          if (estado) {
            if (localStorage.puertoD == "PERÚ - OTRAS PROVINCIAS") {
              response = [].concat(toConsumableArray_default()(data), [{
                id: 15,
                nombre: "PERÚ - OTRAS PROVINCIAS"
              }]);
            } else {
              response = data;
            }

            _this2.setState({
              province: response
            });
          } else {
            _this2.setState({
              errorProv: false
            });
          }
        }).catch(function (error) {
          Object(Utils["d" /* errores */])("ApipostProvincias", error);
        });
      } // PERMISOS


      this.apiPost(this.props.api + "maritimo_v2/get_merca_per", {
        id_port: destino
      }, this.find_product_perm);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "EventProducto",
    value: function EventProducto(x) {
      if (this.state.check2 === false) {
        switch (x) {
          case "1":
            switch (localStorage.tipocon) {
              case "Contenedor completo":
                if (this.state.eventBandera == false) {
                  /*Event(
                   "2.5 Producto FCL Solo Toque",
                   "2.5 Producto FCL Solo Toque",
                   "SERVICIO"
                  );*/
                  this.setState(function () {
                    return {
                      eventBandera: true
                    };
                  });
                }

                break;

              case "Contenedor compartido":
                if (this.state.eventBandera == false) {
                  /*Event(
                   "1.6 Producto LCL Solo Toque",
                   "1.6 Producto LCL Solo Toque",
                   "SERVICIO"
                  );*/
                  this.setState(function () {
                    return {
                      eventBandera: true
                    };
                  });
                }

                break;

              case "Contenedor Aereo":
                if (this.state.eventBandera == false) {
                  /*Event(
                   "3.6 Producto AEREO Solo Toque",
                   "3.6 Producto AEREO Solo Toque",
                   "SERVICIO"
                  );*/
                  this.setState(function () {
                    return {
                      eventBandera: true
                    };
                  });
                }

                break;

              default:
            }

            break;

          case "2":
            /*switch (localStorage.tipocon) {
             case "Contenedor completo":
               Event(
                 "2.6 Producto FCL Selección De Producto",
                 "2.6 Producto FCL Selección De Producto",
                 "SERVICIO"
               );
               break;
             case "Contenedor compartido":
               Event(
                 "1.7 Producto LCL Selección De Producto",
                 "1.7 Producto LCL Selección De Producto",
                 "SERVICIO"
               );
               break;
             case "Contenedor Aereo":
               Event(
                 "3.7 Producto AEREO Selección De Producto",
                 "3.7 Producto AEREO Selección De Producto",
                 "SERVICIO"
               );
               break;
             default:
            }*/
            break;

          default:
        }
      }
    }
  }, {
    key: "SelectedOnce",
    value: function SelectedOnce() {
      sessionStorage.confirmationNone = "1";
    }
  }, {
    key: "apiPost",
    value: function apiPost(url, datos, fn) {
      axios_default.a.post(url, datos).then(function (res) {
        var response = res.data.data.r_dat; //this.setState({ response });

        fn(response);
      }).catch(function (error) {
        Object(Utils["d" /* errores */])("ApipostServicios", error);
      });
    }
  }, {
    key: "get_dist",
    value: function get_dist(e) {
      this.setState(function () {
        return {
          district: e
        };
      });
    }
  }, {
    key: "setsignup",
    value: function setsignup(e) {
      this.setState(function () {
        return {
          signup: e
        };
      });
    }
  }, {
    key: "setsignIn",
    value: function setsignIn(e) {
      this.setState(function () {
        return {
          signIn: e
        };
      });
    }
  }, {
    key: "seterror",
    value: function seterror(e) {
      this.setState(function () {
        return {
          error: e
        };
      });
    }
  }, {
    key: "get_prov",
    value: function get_prov(fn) {
      var result = fn.filter(function (item) {
        return item.id == "15";
      });
      this.setState({
        province: result
      });
    }
  }, {
    key: "showResult",
    value: function showResult(fn) {
      this.setState({
        result: fn
      });
    }
  }, {
    key: "find_product_perm",
    value: function find_product_perm(fn) {
      this.setState({
        productPerm: fn
      });
    }
  }, {
    key: "get_ctz_resumen",
    value: function get_ctz_resumen() {
      localStorage.distrito = this.state.districtSelected.map(function (item) {
        return item.id;
      });
      localStorage.regulador = this.state.productPermSelected.map(function (item) {
        return item.ente_regulatorio;
      });
      localStorage.tipoMercancia = this.state.productPermSelected.map(function (item) {
        return item.tipo_mercancia;
      });
      localStorage.distritoN = this.state.districtSelected.map(function (item) {
        return item.nombre;
      });
      localStorage.ProvinciaN = this.state.provinceSelected.map(function (item) {
        return item.nombre;
      });
      var regex = /(\d+)/g;
      var name = this.state.form[0].value;
      var x;

      if (this.state.form[0].value != 0) {
        localStorage.monto = name.match(regex).join("");
      } else {
        localStorage.monto = this.state.form[0].value;
      }

      localStorage.value4 = !this.state.check3;
      localStorage.value5 = !this.state.check;
      localStorage.value6 = !this.state.check2;
      localStorage.value7 = !this.state.check1;
      localStorage.value8 = !this.state.check4;
      localStorage.seguro = this.state.check4;
    }
  }, {
    key: "array_js_to_pg",
    value: function array_js_to_pg(array) {
      var retorno = "{";
      var x = 1;
      array.forEach(function (element) {
        retorno += "{";

        for (var i = 0; i < element.length; i++) {
          retorno += element[i];
          if (i + 1 < element.length) retorno += ",";
        }

        retorno += "}";
        if (x < array.length) retorno += ",";
        x++;
      });
      retorno += "}";
      return retorno;
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState(function (prevState) {
        return {
          isOpen: !prevState.isOpen
        };
      });
    }
  }, {
    key: "checkbox",
    value: function checkbox(e) {
      var _this3 = this;

      switch (e) {
        case 0:
          if (this.state.iconPuertoYalma === "icon-puertoYalma3") {
            this.setState(function (prevState) {
              return {
                check: !prevState.check,
                iconPuertoYalma: "icon-puertoYalma4",
                animated: "icon-puertoYalmaAnimate2"
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                check: !prevState.check,
                iconPuertoYalma: "icon-puertoYalma3",
                animated: "icon-puertoYalmaAnimate"
              };
            });
          }

          if (this.state.Transporte === "icon-transporte2") {
            this.setState(function (prevState) {
              return {
                check1: !prevState.check1,
                Transporte: "icon-transporte4",
                animated3: "icon-transporteAnimate2"
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                check1: !prevState.check1,
                Transporte: "icon-transporte2",
                animated3: "icon-transporteAnimate"
              };
            });
          }

          if (this.state.aduana === "icon-aduana2") {
            this.setState(function (prevState) {
              return {
                check2: !prevState.check2,
                aduana: "icon-aduana4",
                animated2: "icon-aduanaAnimate2"
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                check2: !prevState.check2,
                aduana: "icon-aduana2",
                animated2: "icon-aduanaAnimate"
              };
            });
          }

          if (this.state.check4 === true) {
            this.setState(function (prevState) {
              return {
                animated4: "icon-seguroAnimate2",
                check4: !prevState.check4
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                animated4: "icon-seguroAnimate",
                check4: !prevState.check4
              };
            });
          }

          break;

        case 1:
          if (this.state.iconPuertoYalma === "icon-puertoYalma3") {
            this.setState(function (prevState) {
              return {
                check: !prevState.check,
                iconPuertoYalma: "icon-puertoYalma4",
                animated: "icon-puertoYalmaAnimate2"
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                check: !prevState.check,
                iconPuertoYalma: "icon-puertoYalma3",
                animated: "icon-puertoYalmaAnimate"
              };
            });
          }

          break;

        case 2:
          if (this.state.Transporte === "icon-transporte2") {
            this.setState(function (prevState) {
              return {
                check1: !prevState.check1,
                Transporte: "icon-transporte4",
                animated3: "icon-transporteAnimate2 "
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                check1: !prevState.check1,
                Transporte: "icon-transporte2",
                animated3: "icon-transporteAnimate"
              };
            });
          }

          break;

        case 3:
          if (this.state.aduana === "icon-aduana2") {
            this.setState(function (prevState) {
              return {
                check2: !prevState.check2,
                aduana: "icon-aduana4",
                animated2: "icon-aduanaAnimate2 ",
                showPer: true
              };
            });
            timer = setTimeout(function () {
              _this3.setState(function (ps) {
                return {
                  showPer: false
                };
              });
            }, 5000);
          } else {
            this.setState(function (prevState) {
              return {
                check2: !prevState.check2,
                showPer: false,
                aduana: "icon-aduana2",
                animated2: "icon-aduanaAnimate"
              };
            });
          }

          break;

        case 4:
          this.setState(function (prevState) {
            return {
              check3: !prevState.check3
            };
          });
          break;

        case 5:
          if (this.state.check4 === true) {
            this.setState(function (prevState) {
              return {
                animated4: "icon-seguroAnimate2",
                check4: !prevState.check4
              };
            });
          } else {
            this.setState(function (prevState) {
              return {
                animated4: "icon-seguroAnimate",
                check4: !prevState.check4
              };
            });
          }

          break;

        default: // code block

      }
    }
  }, {
    key: "handleFormState",
    value: function handleFormState(input) {
      var newState = this.state.form;
      newState[input.id].value = input.value;
      this.setState({
        form: newState
      });
    }
  }, {
    key: "showMessage",
    value: function showMessage() {
      var _this4 = this;

      this.setState(function (ps) {
        return {
          showMessage: true
        };
      });
      timer = setTimeout(function () {
        _this4.setState(function (ps) {
          return {
            showMessage: false
          };
        });
      }, 10000);
    }
  }, {
    key: "toggleModalPreview",
    value: function toggleModalPreview() {
      //orifginal
      var data = JSON.parse(sessionStorage.getItem("user"));

      if (sessionStorage.usuario == "1" || data[1] == "Invitado") {
        if (sessionStorage.agotado == undefined) {
          sessionStorage.agotado = "1";
        } else if (sessionStorage.agotado == "1") {
          sessionStorage.agotado = "2";
        } else if (sessionStorage.agotado == "2") {
          sessionStorage.agotado = "3";
        } else {
          sessionStorage.agotado = "4";
        }

        localStorage.validLogin = "1";

        if (sessionStorage.usuario == "2" && sessionStorage.agotado !== "4") {
          this.setState({
            confirmation: false
          });
          this.props.history.push("/Presupuesto");
        } else {
          /*this.setState({
            confirmation: false,
            IsVisiblePreview: !this.state.IsVisiblePreview,
          });*/
          this.setState({
            confirmation: false
          });
          var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
          sessionStorage.setItem("user", JSON.stringify(user));
          sessionStorage.usuario = "2";
          this.props.history.push("/Presupuesto");
        }
      } else {
        this.setState({
          confirmation: false
        });
        this.props.history.push("/Presupuesto");
      }
    }
  }, {
    key: "validation",
    value: function validation() {
      valid = "1";

      if (this.state.signInChoose === true) {
        this.setState({
          signInChoose: false
        });
        this.setsignup(false);
      }

      if (this.state.check2 === false && this.state.form[0].value.length === 0 && this.state.check2 === false && this.state.productPermSelected.length === 0 && this.state.check1 === false && this.state.provinceSelected.length === 0 && this.state.check1 === false && this.state.districtSelected.length === 0) {
        this.showMessage();
      } else if (this.state.check2 === false && this.state.productPermSelected.length === 0) {
        this.showMessage();
      } else if (this.state.check2 === false && this.state.form[0].value.length === 0) {
        this.showMessage();
      } else if (this.state.check4 === false && this.state.form[0].value.length === 0) {
        this.showMessage();
      } else if (this.state.check1 === false && this.state.provinceSelected.length === 0) {
        this.showMessage();
      } else if (this.state.check1 === false && this.state.districtSelected.length === 0) {
        this.showMessage();
      } else if (sessionStorage.confirmationNone !== "1" && (this.state.check === true || this.state.check1 === true || this.state.check2 === true || this.state.check4 === true)) {
        this.get_ctz_resumen();
        this.confirmation();
      } else {
        this.get_ctz_resumen();
        this.toggleModalPreview();
      }
    }
  }, {
    key: "confirmation",
    value: function confirmation() {
      this.setState({
        confirmation: !this.state.confirmation
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      localStorage.page = "3";

      if (localStorage.transporte == undefined) {
        return /*#__PURE__*/react_default.a.createElement(Redirect["a" /* default */], {
          to: "/"
        });
      } else {} //validaciones de inputs


      var selected;
      var selected2;
      var selected3;
      var selected4;

      if (valid === "1") {
        if (this.state.productPermSelected.length > 0 && this.state.check2 === false) {
          selected = false;
        } else {
          selected = true;
        }
      } else {
        selected = false;
      }

      if (valid === "1") {
        if (this.state.form[0].value.length > 0 && this.state.check2 === false || this.state.form[0].value.length > 0 && this.state.check4 === false) {
          selected2 = false;
        } else {
          selected2 = true;
        }
      } else {
        selected2 = false;
      }

      if (valid === "1") {
        if (this.state.provinceSelected.length > 0 && this.state.check1 === false) {
          selected3 = false;
        } else {
          selected3 = true;
        }
      } else {
        selected3 = false;
      }

      if (valid === "1") {
        if (this.state.districtSelected.length > 0 && this.state.check1 === false) {
          selected4 = false;
        } else {
          selected4 = true;
        }
      } else {
        selected4 = false;
      }

      if (selected === false && selected2 == false && selected3 === false && selected4 === false && valid === "1") {
        validated = "1";
      } else {
        validated = "2";
      } //validaciones de inputs fin
      //pasar valores de check a localstorage


      localStorage.check3 = !this.state.check3;
      localStorage.check = !this.state.check;
      localStorage.check2 = !this.state.check2;
      localStorage.check1 = !this.state.check1;
      localStorage.setItem("check4", JSON.stringify(this.state.check4)); //pasar valores de check a localstorage fin
      //login

      var regulador;

      if (this.state.productPermSelected.length > 0) {
        regulador = this.state.productPermSelected.map(function (item) {
          return item.ente_regulatorio;
        });
      } else {}

      return /*#__PURE__*/react_default.a.createElement(react["Fragment"], null, /*#__PURE__*/react_default.a.createElement(helmet["a" /* Title */], null, /*#__PURE__*/react_default.a.createElement("title", null, window.location.pathname.replace('/', '') + " | " + "PIC - Calculadora de Fletes")), /*#__PURE__*/react_default.a.createElement("span", {
        id: "link3"
      }), /*#__PURE__*/react_default.a.createElement("div", {
        className: "container2 "
      }, this.state.showMessage ? /*#__PURE__*/react_default.a.createElement("div", {
        className: " alert alert-danger arial ",
        role: "alert"
      }, "Seleccion\xF3 algunos servicios. Complete los datos requeridos o desmarque los mismos.") : "", /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto  mb-3 "
      }, /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
        to: "/Ruta"
      }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        style: {
          fontSize: "0.8rem",
          padding: "1px"
        },
        className: "btn-style",
        size: "sm"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      }), " VOLVER"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "h-100 "
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xl: "10",
        lg: "12",
        className: "mx-auto NoPadding"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto my-auto NoPadding "
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto  "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "step-style"
      }, "Resumen de Ruta"), " "), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mb-1 "
      }, " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "arial font2"
      }, " ", "Flete", " ", /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "none" : ""
        }
      }, " ", "Maritimo"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "" : "none"
        }
      }, " ", "A\xE9reo"), " "), " ", /*#__PURE__*/react_default.a.createElement("div", null, localStorage.puerto, " \u2192 ", localStorage.puertoD), /*#__PURE__*/react_default.a.createElement("span", {
        className: "font2",
        style: {
          display: localStorage.tipocon === "Contenedor completo" && localStorage.transporte !== "AÉREO" ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, localStorage.contenedoresDes)), /*#__PURE__*/react_default.a.createElement("div", {
        className: "font2",
        style: {
          display: localStorage.tipocon === "Contenedor compartido" && localStorage.transporte !== "AÉREO" ? "" : "none"
        }
      }, localStorage.contenedoresDes), /*#__PURE__*/react_default.a.createElement("div", {
        className: "font2",
        style: {
          display: localStorage.transporte == "AÉREO" ? "" : "none"
        }
      }, localStorage.contenedoresDes))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mt-1 mb-3 "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "step-style"
      }, "PASO 3. Agrega Servicios Adicionales"), " "), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "7",
        className: "mx-auto mb-3 NoPadding"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        style: {
          fontSize: "1.1rem"
        },
        color: "primary",
        className: "NoPadding"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check"
      }), " Selecciona 1 o", /*#__PURE__*/react_default.a.createElement("span", {
        onClick: function onClick() {
          return _this5.checkbox(0);
        },
        style: {
          fontWeight: "bold",
          textDecoration: "underline",
          color: "#002752",
          cursor: "pointer"
        }
      }, " ", "M\xC1S", " "), " ", "Servicios Adicionales")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "mx-auto my-auto",
        xxs: "12",
        md: "7",
        sm: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-services"
      }, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "included-list"
      }, /*#__PURE__*/react_default.a.createElement("li", {
        className: "included-item-wrapper"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-checkbox"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        style: {
          padding: "0px"
        },
        checked: !this.state.check,
        onChange: function onChange() {
          return _this5.checkbox(1);
        },
        value: this.state.check,
        type: "checkbox",
        id: "exampleCustomCheckbox4"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        htmlFor: "filter-1-2"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.iconPuertoYalma
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.animated
      }), /*#__PURE__*/react_default.a.createElement("b", {
        style: {
          color: this.state.check === true ? "" : "#11a017"
        },
        onClick: function onClick() {
          return _this5.checkbox(1);
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "none" : ""
        }
      }, "Gastos portuarios y de Almacenamiento Aduanero \xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "" : "none"
        }
      }, "Gasto Aeroportuario y Almacenamiento Aduanero\xA0"), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-question question-style2",
        id: "Tooltip1"
      }), "\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check check-style",
        style: {
          display: this.state.check === true ? "none" : "initial"
        }
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "Tooltip1"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "NOTA"), ":Toda mercancia al momento de arribar a Territorio Nacional, es almacenada en un Deposito Temporal Aduanero, y este genera gastos desde el primer dia de almacenamiento. De no seleccionar esta opcion sus costos de importaci\xF3n no estaran completos."), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "En Peru se Cobran los siguientes \xEDtems dependiendo de la terminal portuaria y/o Almacen: Thcd, Vistos Buenos, Descarga entre otros.")))))), /*#__PURE__*/react_default.a.createElement("li", {
        className: "included-item-wrapper"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-checkbox"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        style: {
          padding: "0px"
        },
        checked: !this.state.check2,
        onChange: function onChange() {
          return _this5.checkbox(3);
        },
        value: this.state.check2,
        type: "checkbox",
        id: "exampleCustomCheckbox3"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        htmlFor: "filter-1-2"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.aduana
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.animated2
      }), /*#__PURE__*/react_default.a.createElement("b", {
        onClick: function onClick() {
          return _this5.checkbox(3);
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          color: this.state.check2 === true ? "#0d5084" : "#11a017"
        }
      }, "C\xE1lculo de impuestos y permisos de aduana\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-question question-style2",
        id: "Tooltip3"
      }), "\xA0"), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check check-style",
        style: {
          display: this.state.check2 === true ? "none" : "initial"
        }
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "Tooltip3"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "Impuesto que fija el Gobierno Nacional para que le producto pueda ingresar al pa\xEDs destino.")))))), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check2 === true ? "none" : "block"
        },
        className: "included-item-wrapper"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "7",
        sm: "4",
        className: "mb-1",
        style: {
          marginLeft: "auto"
        }
      }, /*#__PURE__*/react_default.a.createElement(lib["Typeahead"], {
        isValid: selected,
        flip: true,
        id: "2",
        autoFocus: false,
        labelKey: function labelKey(option) {
          return "".concat(option.tipo_mercancia);
        },
        renderMenuItemChildren: function renderMenuItemChildren(option) {
          return /*#__PURE__*/react_default.a.createElement("div", {
            style: {
              color: option.tipo_mercancia === "OTROS PRODUCTOS / CARGA GENERAL" ? "red" : "unset",
              fontWeight: option.tipo_mercancia === "OTROS PRODUCTOS / CARGA GENERAL" ? "bold" : "unset"
            }
          }, option.tipo_mercancia, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("small", null, "Regulador: ", option.ente_regulatorio)));
        },
        multiple: false,
        emptyLabel: "Si Producto no aparece, elegir CARGA GENERAL",
        options: this.state.productPerm,
        placeholder: "Producto",
        onChange: function onChange(productPermSelected) {
          _this5.setState({
            productPermSelected: productPermSelected
          });

          _this5.EventProducto("2");
        },
        onFocus: function onFocus() {
          return _this5.EventProducto("1");
        },
        selected: this.state.productPermSelected
      }), /*#__PURE__*/react_default.a.createElement("div", defineProperty_default()({
        className: "bg-white",
        style: {
          left: "0.5rem"
        },
        onClick: function onClick() {
          return _this5.setState(function (prevState) {
            return {
              showPer: !prevState.showPer
            };
          });
        }
      }, "className", "up-izquierda2  ".concat(this.state.showPer === false ? "hide-toolkit" : "show-toolkit-cont2")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this5.setState(function (prevState) {
            return {
              showPer: !prevState.showPer
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          height: "auto"
        },
        className: "NoPadding theme-color-blueHeader arial"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          borderRadius: "10rem",
          padding: "0.5rem",
          backgroundColor: "white",
          height: "5em",
          left: "0em"
        },
        className: "icon-list"
      }))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "text-center",
        xxs: "12",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1rem"
        }
      }, "Si tu producto no aparece en el listado elegir:"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          height: "auto"
        },
        className: "NoPadding theme-color-orange  arial"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "text-center",
        xxs: "12",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1rem",
          textDecoration: "underline"
        }
      }, "OTROS PRODUCTOS / CARGA GENERAL:"))))), this.state.productPermSelected.length > 0 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "regulation-text"
      }, "Permiso Gubernamental Adicional:", regulador) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        style: {
          display: selected === false ? "" : "none"
        },
        className: "validation-text"
      }, "Si Tu Producto NO APARECE elegir CARGA GENERAL."), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected === false ? "none" : ""
        }
      }, "Si Tu Producto NO APARECE elegir CARGA GENERAL.")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "5",
        sm: "4",
        style: {
          marginRight: "auto"
        }
      }, /*#__PURE__*/react_default.a.createElement(util_CurrencyInput, {
        type: "text",
        className: "form-control",
        invalid: selected2.toString(),
        name: "value",
        id: "value2",
        placeholder: "Valor de mercancia $",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this5.handleFormState({
            id: 0,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected2 == false ? "none" : ""
        }
      }, "Debe ingresar el valor de la mercancia"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected2 == false ? "" : "none"
        }
      }, "Ingresar valor exacto, SIN DECIMALES")))))), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.paisDestino == "PERU" ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check2 === true ? "none" : ""
        },
        className: "included-item-wrapper"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-checkbox"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        defaultValue: "NO",
        onChange: function onChange(e) {
          _this5.handleOnChange({
            value: e.target.value || ""
          });
        },
        type: "select",
        name: "select",
        id: "exampleSelect"
      }, /*#__PURE__*/react_default.a.createElement("option", {
        key: "SI",
        value: "SI"
      }, "SI"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "NO",
        value: "NO"
      }, "NO"))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        htmlFor: "filter-1-2"
      }, /*#__PURE__*/react_default.a.createElement("b", {
        style: {
          color: this.state.check3 === true ? "" : "#11a017"
        }
      }, "\xBFHa realizado importaciones previamente ?", " ", "\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-question question-style2",
        id: "Tooltip2"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "Tooltip2"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "NOTA"), ": Por ley de Aduana, los impuestos varian la primera vez que se importa ( No se considera importaciones envios via Dhl, Serport, Fedex o Ups), se requiere esta informacion para dar un calculo de impuestos m\xE1s exacto.")), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check check-style",
        style: {
          display: this.state.check3 === true ? "none" : "initial"
        }
      }))))))), /*#__PURE__*/react_default.a.createElement("li", {
        className: "included-item-wrapper",
        style: {
          display: this.state.errorProv === true ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-checkbox"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        style: {
          padding: "0px"
        },
        checked: !this.state.check1,
        onChange: function onChange() {
          return _this5.checkbox(2);
        },
        value: this.state.check1,
        type: "checkbox",
        id: "exampleCustomCheckbox2"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        htmlFor: "filter-1-2"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.Transporte
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.animated3
      }), /*#__PURE__*/react_default.a.createElement("b", {
        style: {
          color: this.state.check1 === true ? "" : "#11a017"
        },
        onClick: this.checkbox1
      }, " ", "Transporte a domicilio\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-question question-style2",
        id: "Tooltip4"
      }), "\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check check-style",
        style: {
          display: this.state.check1 === true ? "none" : "initial"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "Tooltip4"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "Servicio de Transporte desde Puerto o Almac\xE9n portuario hasta las instalaciones del Importador o Exportador."))))))), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check1 === true ? "none" : "block"
        },
        className: "included-item-wrapper"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "6",
        sm: "4",
        className: "mb-1",
        style: {
          marginLeft: "auto"
        }
      }, /*#__PURE__*/react_default.a.createElement(lib["Typeahead"], {
        isValid: selected3,
        flip: true,
        id: "3",
        labelKey: function labelKey(option) {
          return "".concat(option.nombre);
        },
        renderMenuItemChildren: function renderMenuItemChildren(option) {
          return /*#__PURE__*/react_default.a.createElement("div", null, option.nombre, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("small", null, "Pais:", localStorage.paisDestino)));
        },
        multiple: false,
        options: this.state.province,
        placeholder: "Provincia",
        onChange: function onChange(provinceSelected) {
          _this5.setState({
            provinceSelected: provinceSelected
          });

          _this5.handleOnChangeProvi(provinceSelected);
        },
        selected: this.state.provinceSelected
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected3 === false ? "none" : ""
        }
      }, "Debe seleccionar la provincia"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected3 === false ? "none" : ""
        }
      })), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "6",
        sm: "4",
        style: {
          marginRight: "auto"
        }
      }, /*#__PURE__*/react_default.a.createElement(lib["Typeahead"], {
        isValid: selected4,
        flip: true,
        id: "4",
        labelKey: function labelKey(option) {
          return "".concat(option.nombre);
        },
        renderMenuItemChildren: function renderMenuItemChildren(option) {
          return /*#__PURE__*/react_default.a.createElement("div", null, option.nombre);
        },
        multiple: false,
        options: this.state.district,
        placeholder: "Distrito",
        onChange: function onChange(districtSelected) {
          _this5.setState({
            districtSelected: districtSelected
          });
        },
        selected: this.state.districtSelected,
        ref: ref
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected4 === false ? "none" : ""
        }
      }, "Debe seleccionar el distrito")), this.state.msjToggle ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: "Los Centro de Recepcion de las Agencias de Envios Interprovinciales se encuentran principalmente en Lima - La Victoria,  se calculara hasta ese Distrito."
      })) : null)))), /*#__PURE__*/react_default.a.createElement("li", {
        className: "included-item-wrapper"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-checkbox"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        style: {
          padding: "0px"
        },
        checked: !this.state.check4,
        onChange: function onChange() {
          return _this5.checkbox(5);
        },
        value: this.state.check4,
        type: "checkbox",
        id: "exampleCustomCheckbox"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        htmlFor: "filter-1-2"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: this.state.animated4
      }), /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-lock",
        style: {
          color: this.state.check4 === true ? "#b5b5b5" : "#11a017"
        }
      }), /*#__PURE__*/react_default.a.createElement("b", {
        style: {
          color: this.state.check4 === true ? "" : "#11a017"
        },
        onClick: function onClick() {
          return _this5.checkbox(5);
        }
      }, " ", "Seguro de mercancia\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-question question-style2",
        id: "Tooltip5"
      }), "\xA0", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check check-style",
        style: {
          display: this.state.check4 === true ? "none" : "initial"
        }
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "Tooltip5"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "Las navieras y aerol\xEDneas operan bajo condiciones que limitan su responsabilidad en casos de p\xE9rdida, da\xF1o o demora.", " "), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "Existen factores que no son controlados por el operador log\xEDsticos, asi como los llamados", /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          color: "red"
        }
      }, " ", "Actos de Dios"), ", los cual en caso de suceder si Ud ha adquirido Seguro contra Perdida Total,", /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          color: "red"
        }
      }, "estar\xE1 asegurando su inversi\xF3n y/o producto"), " ", "una vez pagado su deducible correspondiente.", " ")))))), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check4 === true ? "none" : "block"
        },
        className: "included-item-wrapper"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          display: this.state.check2 === false ? "none" : "block"
        },
        className: "included-item"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "included-content2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, " ", /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        style: {
          marginRight: "auto"
        }
      }, /*#__PURE__*/react_default.a.createElement(util_CurrencyInput, {
        type: "text",
        className: "form-control",
        invalid: selected2.toString(),
        name: "value",
        id: "value",
        placeholder: "Valor de mercancia $",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this5.handleFormState({
            id: 0,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected2 == false ? "none" : ""
        }
      }, "Debe ingresar el valor de la mercancia"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected2 == false ? "" : "none"
        }
      }, "Ingresar valor exacto, SIN DECIMALES")))))))))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto mb-2  "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        size: "sm",
        color: "success",
        className: "mb-3  mx-auto btn-style-blue",
        onClick: function onClick() {
          _this5.validation();

          _this5.eventCalcularCotizacion();
        }
      }, "CALCULAR COTIZACI\xD3N")), /*#__PURE__*/react_default.a.createElement(cadenaLogistica, null), /*#__PURE__*/react_default.a.createElement(timeLine["a" /* default */], null)))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], {
        isOpen: this.state.IsVisiblePreview,
        toggle: this.toggleModalPreview,
        backdrop: false,
        size: "md",
        className: "modalConfirmatinoWidth",
        style: {
          borderRadius: "1.1rem",
          marginTop: "65px !important"
        }
      }, sessionStorage.agotado !== "4" ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        style: {
          padding: "0rem"
        }
      }, !this.state.signInChoose ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleModalPreview,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", null, "\xA1IMPORTANTE!"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "p-2 text-center"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xs: "12"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: " login-form"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mt-2 mb-1"
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-list"
      }), /*#__PURE__*/react_default.a.createElement("span", null, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue"
      }, "Para\xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-green"
      }, "formalizar\xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue"
      }, "tu oferta\xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "REG\xCDSTRATE\xA0")))), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mt-1 mb-1"
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Sin Formularios"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: " mb-1"
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Sin TDC ni pagos"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: " mb-1"
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Solo con tu email"))), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-2",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: this.invitado,
        className: "btn-style-grey mr-1",
        size: "sm"
      }, "INGRESAR COMO INVITADO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this5.setState(function () {
            return {
              signInChoose: true
            };
          });
        },
        className: "btn-style",
        size: "sm"
      }, "REGISTRARME")), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontSize: "0.9rem"
        },
        className: "text-center mb-1"
      }, "Ya tengo una cuenta -\xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this5.setState(function () {
            return {
              signInChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))))))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleModalPreview,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", null, "\xA1INGRESA\xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "bggradien-orange color-white",
        style: {
          fontWeight: "bold",
          borderRadius: "5px"
        }
      }, "\xA0GRATIS!\xA0"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto p-2"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xs: "12"
      }, this.state.signup ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: "login-form",
        onSubmit: this.correoClave
      }, this.state.error ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react_default.a.createElement("div", {
        className: "paddingLogin text-center",
        style: {
          textAling: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 font2 color-blue"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "Iniciar sesi\xF3n")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        className: "GLogin ",
        onClick: function onClick(e) {
          return _this5.socialLogin(Firebase["b" /* googleAuthProvider */], e);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-google"
      }), "Ingresa con Google")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "-o-")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "login-form-button"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Iniciar sesi\xF3n"), "\xA0 O \xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          _this5.setsignup(false);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "Registrate")))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: "paddingLogin text-center",
        onSubmit: this.handleSignUp
      }, this.state.Choose ? /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2  "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue"
      }, "INGRESA "), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-black"
      }, "TUS DATOS")), this.state.error ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial color-orange "
      }, "PASO 1 -", " "), /*#__PURE__*/react_default.a.createElement("span", null, "N\xFAmero de tel\xE9fono -"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-grey"
      }, "(Opcional)")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-3"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "text",
        name: "telefono",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial color-orange"
      }, "PASO 2 -", " "), /*#__PURE__*/react_default.a.createElement("span", null, "Correo electr\xF3nico")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this5.socialLogin(Firebase["b" /* googleAuthProvider */], e);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react_default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "o con otro "), /*#__PURE__*/react_default.a.createElement("span", {
        onClick: function onClick() {
          return _this5.setState(function () {
            return {
              Choose: false
            };
          });
        },
        className: "color-blue",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "correo electr\xF3nico")), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mt-2 mb-2"
      }, "\xA0 Ya tengo una cuenta - \xA0", /*#__PURE__*/react_default.a.createElement("span", {
        onClick: function onClick() {
          _this5.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))) : /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2 font2 "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue"
      }, "INGRESA "), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-black"
      }, " TUS DATOS")), this.state.error ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange "
      }, "PASO 1 - "), /*#__PURE__*/react_default.a.createElement("span", null, "N\xFAmero de tel\xE9fono -"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-grey"
      }, "(Opcional)")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-3"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "text",
        name: "telefono",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "PASO 2 - "), /*#__PURE__*/react_default.a.createElement("span", null, "Correo electr\xF3nico")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "Confirmacion",
        type: "password",
        placeholder: "Confirma tu Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Registrate")), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mt-2 mb-2"
      }, "\xA0 Ya tengo una cuenta - \xA0", /*#__PURE__*/react_default.a.createElement("span", {
        onClick: function onClick() {
          _this5.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N")))))))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        style: {
          padding: "0rem"
        }
      }, !this.state.agotadoChoose ? /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleModalPreview,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("div", null, "Has agotado el m\xE1ximo de consultas como invitado"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "p-2 text-center"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xs: "12"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: " login-form"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "0.9rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mt-1 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-3"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Opci\xF3n 1 -"), " ", "Consultas ilimitadas Registrate", " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold"
        }
      }, " ", "GRATIS")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nombre de empresa"), " ", "- (Opcional)"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "text",
        name: "Empresa ",
        placeholder: "Ingrese aqui el nommbre de su empresa"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "N\xFAmero de tel\xE9fono"), " ", "- (Opcional)"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "text",
        name: "telefono ",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this5.socialLogin(Firebase["b" /* googleAuthProvider */], e);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react_default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "text-center mt-2 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontSize: "0.9rem"
        }
      }, " ", "o con otro\xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this5.setState(function () {
            return {
              agotadoChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "correo electr\xF3nico")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-3"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Opci\xF3n 2 -\xA0"), /*#__PURE__*/react_default.a.createElement("span", null, "Regresa en 15 d\xEDas y goza de 3 nuevas consultas sin registro."))), /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          fontSize: "0.9rem"
        },
        className: "text-center m-3"
      }, "Ya tengo una cuenta -\xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this5.setState(function () {
            return {
              agotadoChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))))))) : /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleModalPreview,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", null, "\xA1INGRESA\xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        className: "bggradien-orange color-white",
        style: {
          fontWeight: "bold",
          borderRadius: "5px"
        }
      }, "\xA0GRATIS!\xA0"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "p-2"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xs: "12"
      }, /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), !this.state.signup ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: " login-form",
        onSubmit: this.correoClave
      }, this.state.error ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react_default.a.createElement("div", {
        className: "paddingLogin text-center",
        style: {
          textAling: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "Iniciar sesi\xF3n")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this5.socialLogin(Firebase["b" /* googleAuthProvider */], e);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react_default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "-o-")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "login-form-button"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Iniciar sesi\xF3n"), "\xA0 O \xA0", /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          _this5.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "Registrate")))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: "paddingLogin text-center",
        onSubmit: this.handleSignUp
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "Registro")), this.state.error ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(Errores["a" /* default */], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this5.socialLogin(Firebase["b" /* googleAuthProvider */], e);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react_default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "-o-")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "Confirmacion",
        type: "password",
        placeholder: "Confirma tu Clave"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Registrate"), "\xA0 O \xA0", /*#__PURE__*/react_default.a.createElement("span", {
        onClick: function onClick() {
          _this5.setsignup(false);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N")))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-1",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: this.toggleModalPreview,
        className: "btn-style",
        size: "sm"
      }, " ", "VOLVER")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], {
        isOpen: this.state.confirmation,
        toggle: this.confirmation,
        size: "md",
        className: "modalConfirmatinoWidth",
        style: {
          borderRadius: "1.1rem",
          marginTop: "65px !important"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.confirmation,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "text-center pt-2 pb-2 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.5em"
        }
      }, "ATENCI\xD3N"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-2 pt-2 arial",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontSize: "1.25em",
          color: "#0d5084"
        }
      }, "Para un", " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "PRESUPUESTO M\xC1S EXACTO"), ", sugerimos volver y completar:")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "10",
        sm: "10",
        className: "mx-auto mt-2 pt-2",
        style: {
          textAlign: "justify",
          fontSize: "0.9rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("ul", null, /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check === false ? "none" : ""
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "none" : ""
        }
      }, "Gastos portuarios y de Almacenamiento Aduanero \xA0"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: localStorage.transporte == "AÉREO" ? "" : "none"
        }
      }, "Gasto Aeroportuario y Almacenamiento Aduanero\xA0")), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check2 === false ? "none" : ""
        }
      }, "Impuestos y permisos de aduana"), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check1 === false ? "none" : ""
        }
      }, "Transporte a domicilio"), /*#__PURE__*/react_default.a.createElement("li", {
        style: {
          display: this.state.check4 === false ? "none" : ""
        }
      }, "Seguro de mercanc\xEDa"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["F" /* ModalFooter */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        id: "SelectedOnce",
        type: "checkbox",
        onChange: this.SelectedOnce,
        label: "No volver a mostrar este mensaje"
      })), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this5.confirmation();

          _this5.eventVolver();
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this5.toggleModalPreview();

          _this5.eventIrPresupuestos();
        },
        className: "btn-style-blue",
        size: "sm"
      }, "IR A PRESUPUESTO"))))), /*#__PURE__*/react_default.a.createElement(index_esm["a" /* Beforeunload */], {
        onBeforeunload: function onBeforeunload() {
          return "You'll lose your data!";
        }
      }), /*#__PURE__*/react_default.a.createElement(chat["a" /* default */], null));
    }
  }]);

  return servicios;
}(react["Component"]);

/* harmony default export */ var Servicios = __webpack_exports__["default"] = (Object(withRouter["a" /* default */])(Servicios_servicios));

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = warn;
exports.resetWarned = resetWarned;

var _warning = _interopRequireDefault(__webpack_require__(7));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * This code is copied from: https://github.com/ReactTraining/react-router/blob/master/modules/routerWarning.js
 */
var warned = {};

function warn(falseToWarn, message) {
  // Only issue deprecation warnings once.
  if (!falseToWarn && message.indexOf('deprecated') !== -1) {
    if (warned[message]) {
      return;
    }

    warned[message] = true;
  }

  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  _warning["default"].apply(void 0, [falseToWarn, "[react-bootstrap-typeahead] ".concat(message)].concat(args));
}

function resetWarned() {
  warned = {};
}

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "addCustomOption", {
  enumerable: true,
  get: function get() {
    return _addCustomOption2["default"];
  }
});
Object.defineProperty(exports, "areEqual", {
  enumerable: true,
  get: function get() {
    return _areEqual2["default"];
  }
});
Object.defineProperty(exports, "defaultFilterBy", {
  enumerable: true,
  get: function get() {
    return _defaultFilterBy2["default"];
  }
});
Object.defineProperty(exports, "getAccessibilityStatus", {
  enumerable: true,
  get: function get() {
    return _getAccessibilityStatus2["default"];
  }
});
Object.defineProperty(exports, "getDisplayName", {
  enumerable: true,
  get: function get() {
    return _getDisplayName2["default"];
  }
});
Object.defineProperty(exports, "getHintText", {
  enumerable: true,
  get: function get() {
    return _getHintText2["default"];
  }
});
Object.defineProperty(exports, "getInputText", {
  enumerable: true,
  get: function get() {
    return _getInputText2["default"];
  }
});
Object.defineProperty(exports, "getIsOnlyResult", {
  enumerable: true,
  get: function get() {
    return _getIsOnlyResult2["default"];
  }
});
Object.defineProperty(exports, "getMatchBounds", {
  enumerable: true,
  get: function get() {
    return _getMatchBounds2["default"];
  }
});
Object.defineProperty(exports, "getMenuItemId", {
  enumerable: true,
  get: function get() {
    return _getMenuItemId2["default"];
  }
});
Object.defineProperty(exports, "getOptionLabel", {
  enumerable: true,
  get: function get() {
    return _getOptionLabel2["default"];
  }
});
Object.defineProperty(exports, "getStringLabelKey", {
  enumerable: true,
  get: function get() {
    return _getStringLabelKey2["default"];
  }
});
Object.defineProperty(exports, "getTruncatedOptions", {
  enumerable: true,
  get: function get() {
    return _getTruncatedOptions2["default"];
  }
});
Object.defineProperty(exports, "isSelectable", {
  enumerable: true,
  get: function get() {
    return _isSelectable2["default"];
  }
});
Object.defineProperty(exports, "isShown", {
  enumerable: true,
  get: function get() {
    return _isShown2["default"];
  }
});
Object.defineProperty(exports, "pluralize", {
  enumerable: true,
  get: function get() {
    return _pluralize2["default"];
  }
});
Object.defineProperty(exports, "preventInputBlur", {
  enumerable: true,
  get: function get() {
    return _preventInputBlur2["default"];
  }
});
Object.defineProperty(exports, "scrollIntoViewIfNeeded", {
  enumerable: true,
  get: function get() {
    return _scrollIntoViewIfNeeded2["default"];
  }
});
Object.defineProperty(exports, "shouldSelectHint", {
  enumerable: true,
  get: function get() {
    return _shouldSelectHint2["default"];
  }
});
Object.defineProperty(exports, "stripDiacritics", {
  enumerable: true,
  get: function get() {
    return _stripDiacritics2["default"];
  }
});
Object.defineProperty(exports, "warn", {
  enumerable: true,
  get: function get() {
    return _warn2["default"];
  }
});

var _addCustomOption2 = _interopRequireDefault(__webpack_require__(524));

var _areEqual2 = _interopRequireDefault(__webpack_require__(525));

var _defaultFilterBy2 = _interopRequireDefault(__webpack_require__(526));

var _getAccessibilityStatus2 = _interopRequireDefault(__webpack_require__(527));

var _getDisplayName2 = _interopRequireDefault(__webpack_require__(528));

var _getHintText2 = _interopRequireDefault(__webpack_require__(529));

var _getInputText2 = _interopRequireDefault(__webpack_require__(531));

var _getIsOnlyResult2 = _interopRequireDefault(__webpack_require__(532));

var _getMatchBounds2 = _interopRequireDefault(__webpack_require__(316));

var _getMenuItemId2 = _interopRequireDefault(__webpack_require__(533));

var _getOptionLabel2 = _interopRequireDefault(__webpack_require__(228));

var _getStringLabelKey2 = _interopRequireDefault(__webpack_require__(252));

var _getTruncatedOptions2 = _interopRequireDefault(__webpack_require__(534));

var _isSelectable2 = _interopRequireDefault(__webpack_require__(317));

var _isShown2 = _interopRequireDefault(__webpack_require__(535));

var _pluralize2 = _interopRequireDefault(__webpack_require__(536));

var _preventInputBlur2 = _interopRequireDefault(__webpack_require__(537));

var _scrollIntoViewIfNeeded2 = _interopRequireDefault(__webpack_require__(538));

var _shouldSelectHint2 = _interopRequireDefault(__webpack_require__(539));

var _stripDiacritics2 = _interopRequireDefault(__webpack_require__(265));

var _warn2 = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 157:
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    getRawTag = __webpack_require__(344),
    objectToString = __webpack_require__(345);

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ 159:
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

var baseIsNative = __webpack_require__(358),
    getValue = __webpack_require__(361);

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;


/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_LABELKEY = exports.DOWN = exports.RIGHT = exports.UP = exports.LEFT = exports.SPACE = exports.ESC = exports.RETURN = exports.TAB = exports.BACKSPACE = void 0;

/**
 * Common (non-printable) keycodes for `keydown` and `keyup` events. Note that
 * `keypress` handles things differently and may not return the same values.
 */
var BACKSPACE = 8;
exports.BACKSPACE = BACKSPACE;
var TAB = 9;
exports.TAB = TAB;
var RETURN = 13;
exports.RETURN = RETURN;
var ESC = 27;
exports.ESC = ESC;
var SPACE = 32;
exports.SPACE = SPACE;
var LEFT = 37;
exports.LEFT = LEFT;
var UP = 38;
exports.UP = UP;
var RIGHT = 39;
exports.RIGHT = RIGHT;
var DOWN = 40;
exports.DOWN = DOWN;
var DEFAULT_LABELKEY = 'label';
exports.DEFAULT_LABELKEY = DEFAULT_LABELKEY;

/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

var isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = toKey;


/***/ }),

/***/ 168:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys = __webpack_require__(269);
var hasSymbols = typeof Symbol === 'function' && typeof Symbol('foo') === 'symbol';

var toStr = Object.prototype.toString;
var concat = Array.prototype.concat;
var origDefineProperty = Object.defineProperty;

var isFunction = function (fn) {
	return typeof fn === 'function' && toStr.call(fn) === '[object Function]';
};

var arePropertyDescriptorsSupported = function () {
	var obj = {};
	try {
		origDefineProperty(obj, 'x', { enumerable: false, value: obj });
		// eslint-disable-next-line no-unused-vars, no-restricted-syntax
		for (var _ in obj) { // jscs:ignore disallowUnusedVariables
			return false;
		}
		return obj.x === obj;
	} catch (e) { /* this is IE 8. */
		return false;
	}
};
var supportsDescriptors = origDefineProperty && arePropertyDescriptorsSupported();

var defineProperty = function (object, name, value, predicate) {
	if (name in object && (!isFunction(predicate) || !predicate())) {
		return;
	}
	if (supportsDescriptors) {
		origDefineProperty(object, name, {
			configurable: true,
			enumerable: false,
			value: value,
			writable: true
		});
	} else {
		object[name] = value;
	}
};

var defineProperties = function (object, map) {
	var predicates = arguments.length > 2 ? arguments[2] : {};
	var props = keys(map);
	if (hasSymbols) {
		props = concat.call(props, Object.getOwnPropertySymbols(map));
	}
	for (var i = 0; i < props.length; i += 1) {
		defineProperty(object, props[i], map[props[i]], predicates[props[i]]);
	}
};

defineProperties.supportsDescriptors = !!supportsDescriptors;

module.exports = defineProperties;


/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(208);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(55);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }






/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ["wrappedComponentRef"]);

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Route__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
      children: function children(routeComponentProps) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, _extends({}, remainingProps, routeComponentProps, {
          ref: wrappedComponentRef
        }));
      }
    });
  };

  C.displayName = "withRouter(" + (Component.displayName || Component.name) + ")";
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
  };

  return hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default()(C, Component);
};

/* harmony default export */ __webpack_exports__["a"] = (withRouter);

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ }),

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

var listCacheClear = __webpack_require__(348),
    listCacheDelete = __webpack_require__(349),
    listCacheGet = __webpack_require__(350),
    listCacheHas = __webpack_require__(351),
    listCacheSet = __webpack_require__(352);

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

module.exports = ListCache;


/***/ }),

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

var eq = __webpack_require__(186);

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

module.exports = assocIndexOf;


/***/ }),

/***/ 186:
/***/ (function(module, exports) {

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

module.exports = eq;


/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160);

/* Built-in method references that are verified to be native. */
var nativeCreate = getNative(Object, 'create');

module.exports = nativeCreate;


/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

var isKeyable = __webpack_require__(370);

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

module.exports = getMapData;


/***/ }),

/***/ 189:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;


/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(148),
    isKey = __webpack_require__(221),
    stringToPath = __webpack_require__(409),
    toString = __webpack_require__(262);

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }
  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

module.exports = castPath;


/***/ }),

/***/ 208:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
    childContextTypes: true,
    contextTypes: true,
    defaultProps: true,
    displayName: true,
    getDefaultProps: true,
    getDerivedStateFromProps: true,
    mixins: true,
    propTypes: true,
    type: true
};

var KNOWN_STATICS = {
    name: true,
    length: true,
    prototype: true,
    caller: true,
    callee: true,
    arguments: true,
    arity: true
};

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = getPrototypeOf && getPrototypeOf(Object);

function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
    if (typeof sourceComponent !== 'string') { // don't hoist over string (html) components

        if (objectPrototype) {
            var inheritedComponent = getPrototypeOf(sourceComponent);
            if (inheritedComponent && inheritedComponent !== objectPrototype) {
                hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
            }
        }

        var keys = getOwnPropertyNames(sourceComponent);

        if (getOwnPropertySymbols) {
            keys = keys.concat(getOwnPropertySymbols(sourceComponent));
        }

        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];
            if (!REACT_STATICS[key] && !KNOWN_STATICS[key] && (!blacklist || !blacklist[key])) {
                var descriptor = getOwnPropertyDescriptor(sourceComponent, key);
                try { // Avoid failures from read-only properties
                    defineProperty(targetComponent, key, descriptor);
                } catch (e) {}
            }
        }

        return targetComponent;
    }

    return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqualDeep = __webpack_require__(347),
    isObjectLike = __webpack_require__(159);

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

module.exports = baseIsEqual;


/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map');

module.exports = Map;


/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

var mapCacheClear = __webpack_require__(362),
    mapCacheDelete = __webpack_require__(369),
    mapCacheGet = __webpack_require__(371),
    mapCacheHas = __webpack_require__(372),
    mapCacheSet = __webpack_require__(373);

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

module.exports = MapCache;


/***/ }),

/***/ 216:
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(388),
    baseKeys = __webpack_require__(395),
    isArrayLike = __webpack_require__(219);

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;


/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(390),
    isObjectLike = __webpack_require__(159);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;


/***/ }),

/***/ 218:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(229),
    isLength = __webpack_require__(218);

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(190),
    toKey = __webpack_require__(167);

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;


/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(148),
    isSymbol = __webpack_require__(183);

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

module.exports = isKey;


/***/ }),

/***/ 222:
/***/ (function(module, exports) {

/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

module.exports = noop;


/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(446);

module.exports = Function.prototype.bind || implementation;


/***/ }),

/***/ 224:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(223);
var GetIntrinsic = __webpack_require__(272);

var $apply = GetIntrinsic('%Function.prototype.apply%');
var $call = GetIntrinsic('%Function.prototype.call%');
var $reflectApply = GetIntrinsic('%Reflect.apply%', true) || bind.call($call, $apply);

var $gOPD = GetIntrinsic('%Object.getOwnPropertyDescriptor%', true);
var $defineProperty = GetIntrinsic('%Object.defineProperty%', true);
var $max = GetIntrinsic('%Math.max%');

if ($defineProperty) {
	try {
		$defineProperty({}, 'a', { value: 1 });
	} catch (e) {
		// IE 8 has a broken defineProperty
		$defineProperty = null;
	}
}

module.exports = function callBind(originalFunction) {
	var func = $reflectApply(bind, $call, arguments);
	if ($gOPD && $defineProperty) {
		var desc = $gOPD(func, 'length');
		if (desc.configurable) {
			// original length, plus the receiver, minus any additional arguments (after the receiver)
			$defineProperty(
				func,
				'length',
				{ value: 1 + $max(0, originalFunction.length - (arguments.length - 1)) }
			);
		}
	}
	return func;
};

var applyBind = function applyBind() {
	return $reflectApply(bind, $apply, arguments);
};

if ($defineProperty) {
	$defineProperty(module.exports, 'apply', { value: applyBind });
} else {
	module.exports.apply = applyBind;
}


/***/ }),

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _invariant = _interopRequireDefault(__webpack_require__(12));

var _isPlainObject = _interopRequireDefault(__webpack_require__(299));

var _getStringLabelKey = _interopRequireDefault(__webpack_require__(252));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Retrieves the display string from an option. Options can be the string
 * themselves, or an object with a defined display string. Anything else throws
 * an error.
 */
function getOptionLabel(option, labelKey) {
  if (option.paginationOption || option.customOption) {
    return option[(0, _getStringLabelKey["default"])(labelKey)];
  }

  var optionLabel;

  if (typeof option === 'string') {
    optionLabel = option;
  }

  if (typeof labelKey === 'function') {
    // This overwrites string options, but we assume the consumer wants to do
    // something custom if `labelKey` is a function.
    optionLabel = labelKey(option);
  } else if (typeof labelKey === 'string' && (0, _isPlainObject["default"])(option)) {
    optionLabel = option[labelKey];
  }

  !(typeof optionLabel === 'string') ?  false ? undefined : invariant(false) : void 0;
  return optionLabel;
}

var _default = getOptionLabel;
exports["default"] = _default;

/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObject = __webpack_require__(157);

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),

/***/ 230:
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(213);

/**
 * Performs a deep comparison between two values to determine if they are
 * equivalent.
 *
 * **Note:** This method supports comparing arrays, array buffers, booleans,
 * date objects, error objects, maps, numbers, `Object` objects, regexes,
 * sets, strings, symbols, and typed arrays. `Object` objects are compared
 * by their own, not inherited, enumerable properties. Functions and DOM
 * nodes are compared by strict equality, i.e. `===`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.isEqual(object, other);
 * // => true
 *
 * object === other;
 * // => false
 */
function isEqual(value, other) {
  return baseIsEqual(value, other);
}

module.exports = isEqual;


/***/ }),

/***/ 242:
/***/ (function(module, exports) {

/**
 * Gets the first element of `array`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @alias first
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the first element of `array`.
 * @example
 *
 * _.head([1, 2, 3]);
 * // => 1
 *
 * _.head([]);
 * // => undefined
 */
function head(array) {
  return (array && array.length) ? array[0] : undefined;
}

module.exports = head;


/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

var basePick = __webpack_require__(426),
    flatRest = __webpack_require__(431);

/**
 * Creates an object composed of the picked `object` properties.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {...(string|string[])} [paths] The property paths to pick.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'a': 1, 'b': '2', 'c': 3 };
 *
 * _.pick(object, ['a', 'c']);
 * // => { 'a': 1, 'c': 3 }
 */
var pick = flatRest(function(object, paths) {
  return object == null ? {} : basePick(object, paths);
});

module.exports = pick;


/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ }),

/***/ 250:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 251:
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),

/***/ 252:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getStringLabelKey;

var _constants = __webpack_require__(166);

function getStringLabelKey(labelKey) {
  return typeof labelKey === 'string' ? labelKey : _constants.DEFAULT_LABELKEY;
}

/***/ }),

/***/ 253:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184),
    stackClear = __webpack_require__(353),
    stackDelete = __webpack_require__(354),
    stackGet = __webpack_require__(355),
    stackHas = __webpack_require__(356),
    stackSet = __webpack_require__(357);

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  var data = this.__data__ = new ListCache(entries);
  this.size = data.size;
}

// Add methods to `Stack`.
Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;

module.exports = Stack;


/***/ }),

/***/ 254:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

module.exports = toSource;


/***/ }),

/***/ 255:
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(374),
    arraySome = __webpack_require__(256),
    cacheHas = __webpack_require__(377);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Check that cyclic values are equal.
  var arrStacked = stack.get(array);
  var othStacked = stack.get(other);
  if (arrStacked && othStacked) {
    return arrStacked == other && othStacked == array;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

module.exports = equalArrays;


/***/ }),

/***/ 256:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;


/***/ }),

/***/ 257:
/***/ (function(module, exports) {

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;


/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var root = __webpack_require__(150),
    stubFalse = __webpack_require__(391);

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(230)(module)))

/***/ }),

/***/ 259:
/***/ (function(module, exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(392),
    baseUnary = __webpack_require__(393),
    nodeUtil = __webpack_require__(394);

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;


/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157);

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;


/***/ }),

/***/ 261:
/***/ (function(module, exports) {

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

module.exports = matchesStrictComparable;


/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var baseToString = __webpack_require__(412);

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;


/***/ }),

/***/ 263:
/***/ (function(module, exports, __webpack_require__) {

var baseHasIn = __webpack_require__(414),
    hasPath = __webpack_require__(415);

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

module.exports = hasIn;


/***/ }),

/***/ 264:
/***/ (function(module, exports) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;


/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = stripDiacritics;

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Taken from: http://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript/18391901#18391901
 */

/* eslint-disable max-len */
var map = [{
  base: 'A',
  letters: "A\u24B6\uFF21\xC0\xC1\xC2\u1EA6\u1EA4\u1EAA\u1EA8\xC3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\xC4\u01DE\u1EA2\xC5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F"
}, {
  base: 'AA',
  letters: "\uA732"
}, {
  base: 'AE',
  letters: "\xC6\u01FC\u01E2"
}, {
  base: 'AO',
  letters: "\uA734"
}, {
  base: 'AU',
  letters: "\uA736"
}, {
  base: 'AV',
  letters: "\uA738\uA73A"
}, {
  base: 'AY',
  letters: "\uA73C"
}, {
  base: 'B',
  letters: "B\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181"
}, {
  base: 'C',
  letters: "C\u24B8\uFF23\u0106\u0108\u010A\u010C\xC7\u1E08\u0187\u023B\uA73E"
}, {
  base: 'D',
  letters: "D\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\xD0"
}, {
  base: 'DZ',
  letters: "\u01F1\u01C4"
}, {
  base: 'Dz',
  letters: "\u01F2\u01C5"
}, {
  base: 'E',
  letters: "E\u24BA\uFF25\xC8\xC9\xCA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\xCB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E"
}, {
  base: 'F',
  letters: "F\u24BB\uFF26\u1E1E\u0191\uA77B"
}, {
  base: 'G',
  letters: "G\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E"
}, {
  base: 'H',
  letters: "H\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D"
}, {
  base: 'I',
  letters: "I\u24BE\uFF29\xCC\xCD\xCE\u0128\u012A\u012C\u0130\xCF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197"
}, {
  base: 'J',
  letters: "J\u24BF\uFF2A\u0134\u0248"
}, {
  base: 'K',
  letters: "K\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2"
}, {
  base: 'L',
  letters: "L\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780"
}, {
  base: 'LJ',
  letters: "\u01C7"
}, {
  base: 'Lj',
  letters: "\u01C8"
}, {
  base: 'M',
  letters: "M\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C"
}, {
  base: 'N',
  letters: "N\u24C3\uFF2E\u01F8\u0143\xD1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4"
}, {
  base: 'NJ',
  letters: "\u01CA"
}, {
  base: 'Nj',
  letters: "\u01CB"
}, {
  base: 'O',
  letters: "O\u24C4\uFF2F\xD2\xD3\xD4\u1ED2\u1ED0\u1ED6\u1ED4\xD5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\xD6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\xD8\u01FE\u0186\u019F\uA74A\uA74C"
}, {
  base: 'OI',
  letters: "\u01A2"
}, {
  base: 'OO',
  letters: "\uA74E"
}, {
  base: 'OU',
  letters: "\u0222"
}, {
  base: 'OE',
  letters: "\x8C\u0152"
}, {
  base: 'oe',
  letters: "\x9C\u0153"
}, {
  base: 'P',
  letters: "P\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754"
}, {
  base: 'Q',
  letters: "Q\u24C6\uFF31\uA756\uA758\u024A"
}, {
  base: 'R',
  letters: "R\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782"
}, {
  base: 'S',
  letters: "S\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784"
}, {
  base: 'T',
  letters: "T\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786"
}, {
  base: 'TZ',
  letters: "\uA728"
}, {
  base: 'U',
  letters: "U\u24CA\uFF35\xD9\xDA\xDB\u0168\u1E78\u016A\u1E7A\u016C\xDC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244"
}, {
  base: 'V',
  letters: "V\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245"
}, {
  base: 'VY',
  letters: "\uA760"
}, {
  base: 'W',
  letters: "W\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72"
}, {
  base: 'X',
  letters: "X\u24CD\uFF38\u1E8A\u1E8C"
}, {
  base: 'Y',
  letters: "Y\u24CE\uFF39\u1EF2\xDD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE"
}, {
  base: 'Z',
  letters: "Z\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762"
}, {
  base: 'a',
  letters: "a\u24D0\uFF41\u1E9A\xE0\xE1\xE2\u1EA7\u1EA5\u1EAB\u1EA9\xE3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\xE4\u01DF\u1EA3\xE5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250"
}, {
  base: 'aa',
  letters: "\uA733"
}, {
  base: 'ae',
  letters: "\xE6\u01FD\u01E3"
}, {
  base: 'ao',
  letters: "\uA735"
}, {
  base: 'au',
  letters: "\uA737"
}, {
  base: 'av',
  letters: "\uA739\uA73B"
}, {
  base: 'ay',
  letters: "\uA73D"
}, {
  base: 'b',
  letters: "b\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253"
}, {
  base: 'c',
  letters: "c\u24D2\uFF43\u0107\u0109\u010B\u010D\xE7\u1E09\u0188\u023C\uA73F\u2184"
}, {
  base: 'd',
  letters: "d\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A"
}, {
  base: 'dz',
  letters: "\u01F3\u01C6"
}, {
  base: 'e',
  letters: "e\u24D4\uFF45\xE8\xE9\xEA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\xEB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD"
}, {
  base: 'f',
  letters: "f\u24D5\uFF46\u1E1F\u0192\uA77C"
}, {
  base: 'g',
  letters: "g\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F"
}, {
  base: 'h',
  letters: "h\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265"
}, {
  base: 'hv',
  letters: "\u0195"
}, {
  base: 'i',
  letters: "i\u24D8\uFF49\xEC\xED\xEE\u0129\u012B\u012D\xEF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131"
}, {
  base: 'j',
  letters: "j\u24D9\uFF4A\u0135\u01F0\u0249"
}, {
  base: 'k',
  letters: "k\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3"
}, {
  base: 'l',
  letters: "l\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747"
}, {
  base: 'lj',
  letters: "\u01C9"
}, {
  base: 'm',
  letters: "m\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F"
}, {
  base: 'n',
  letters: "n\u24DD\uFF4E\u01F9\u0144\xF1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5"
}, {
  base: 'nj',
  letters: "\u01CC"
}, {
  base: 'o',
  letters: "o\u24DE\uFF4F\xF2\xF3\xF4\u1ED3\u1ED1\u1ED7\u1ED5\xF5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\xF6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\xF8\u01FF\u0254\uA74B\uA74D\u0275"
}, {
  base: 'oi',
  letters: "\u01A3"
}, {
  base: 'ou',
  letters: "\u0223"
}, {
  base: 'oo',
  letters: "\uA74F"
}, {
  base: 'p',
  letters: "p\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755"
}, {
  base: 'q',
  letters: "q\u24E0\uFF51\u024B\uA757\uA759"
}, {
  base: 'r',
  letters: "r\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783"
}, {
  base: 's',
  letters: "s\u24E2\uFF53\xDF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B"
}, {
  base: 't',
  letters: "t\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787"
}, {
  base: 'tz',
  letters: "\uA729"
}, {
  base: 'u',
  letters: "u\u24E4\uFF55\xF9\xFA\xFB\u0169\u1E79\u016B\u1E7B\u016D\xFC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289"
}, {
  base: 'v',
  letters: "v\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C"
}, {
  base: 'vy',
  letters: "\uA761"
}, {
  base: 'w',
  letters: "w\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73"
}, {
  base: 'x',
  letters: "x\u24E7\uFF58\u1E8B\u1E8D"
}, {
  base: 'y',
  letters: "y\u24E8\uFF59\u1EF3\xFD\u0177\u1EF9\u0233\u1E8F\xFF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF"
}, {
  base: 'z',
  letters: "z\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763"
}];
/* eslint-enable max-len */

var diacriticsMap = {};

for (var ii = 0; ii < map.length; ii++) {
  var letters = map[ii].letters;

  for (var jj = 0; jj < letters.length; jj++) {
    diacriticsMap[letters[jj]] = map[ii].base;
  }
} // "what?" version ... http://jsperf.com/diacritics/12


function stripDiacritics(str) {
  return str.replace(/[\u0300-\u036F]/g, '') // Remove combining diacritics

  /* eslint-disable-next-line no-control-regex */
  .replace(/[^\u0000-\u007E]/g, function (a) {
    return diacriticsMap[a] || a;
  });
}

/***/ }),

/***/ 266:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160);

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

module.exports = defineProperty;


/***/ }),

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _createChainableTypeChecker = __webpack_require__(542);

var _createChainableTypeChecker2 = _interopRequireDefault(_createChainableTypeChecker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function validate(props, propName, componentName, location, propFullName) {
  var propValue = props[propName];
  var propType = typeof propValue === 'undefined' ? 'undefined' : _typeof(propValue);

  if (_react2.default.isValidElement(propValue)) {
    return new Error('Invalid ' + location + ' `' + propFullName + '` of type ReactElement ' + ('supplied to `' + componentName + '`, expected a ReactComponent or a ') + 'DOMElement. You can usually obtain a ReactComponent or DOMElement ' + 'from a ReactElement by attaching a ref to it.');
  }

  if ((propType !== 'object' || typeof propValue.render !== 'function') && propValue.nodeType !== 1) {
    return new Error('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected a ReactComponent or a ') + 'DOMElement.');
  }

  return null;
}

exports.default = (0, _createChainableTypeChecker2.default)(validate);
module.exports = exports['default'];

/***/ }),

/***/ 268:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (componentOrElement) {
  return (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(componentOrElement));
};

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ownerDocument = __webpack_require__(544);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports['default'];

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var slice = Array.prototype.slice;
var isArgs = __webpack_require__(270);

var origKeys = Object.keys;
var keysShim = origKeys ? function keys(o) { return origKeys(o); } : __webpack_require__(443);

var originalKeys = Object.keys;

keysShim.shim = function shimObjectKeys() {
	if (Object.keys) {
		var keysWorksWithArguments = (function () {
			// Safari 5.0 bug
			var args = Object.keys(arguments);
			return args && args.length === arguments.length;
		}(1, 2));
		if (!keysWorksWithArguments) {
			Object.keys = function keys(object) { // eslint-disable-line func-name-matching
				if (isArgs(object)) {
					return originalKeys(slice.call(object));
				}
				return originalKeys(object);
			};
		}
	} else {
		Object.keys = keysShim;
	}
	return Object.keys || keysShim;
};

module.exports = keysShim;


/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

module.exports = function isArguments(value) {
	var str = toStr.call(value);
	var isArgs = str === '[object Arguments]';
	if (!isArgs) {
		isArgs = str !== '[object Array]' &&
			value !== null &&
			typeof value === 'object' &&
			typeof value.length === 'number' &&
			value.length >= 0 &&
			toStr.call(value.callee) === '[object Function]';
	}
	return isArgs;
};


/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var GetIntrinsic = __webpack_require__(272);

var callBind = __webpack_require__(224);

var $indexOf = callBind(GetIntrinsic('String.prototype.indexOf'));

module.exports = function callBoundIntrinsic(name, allowMissing) {
	var intrinsic = GetIntrinsic(name, !!allowMissing);
	if (typeof intrinsic === 'function' && $indexOf(name, '.prototype.') > -1) {
		return callBind(intrinsic);
	}
	return intrinsic;
};


/***/ }),

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* globals
	AggregateError,
	SharedArrayBuffer,
*/

var undefined;

var $SyntaxError = SyntaxError;
var $Function = Function;
var $TypeError = TypeError;

// eslint-disable-next-line consistent-return
var getEvalledConstructor = function (expressionSyntax) {
	try {
		return $Function('"use strict"; return (' + expressionSyntax + ').constructor;')();
	} catch (e) {}
};

var $gOPD = Object.getOwnPropertyDescriptor;
if ($gOPD) {
	try {
		$gOPD({}, '');
	} catch (e) {
		$gOPD = null; // this is IE 8, which has a broken gOPD
	}
}

var throwTypeError = function () {
	throw new $TypeError();
};
var ThrowTypeError = $gOPD
	? (function () {
		try {
			// eslint-disable-next-line no-unused-expressions, no-caller, no-restricted-properties
			arguments.callee; // IE 8 does not throw here
			return throwTypeError;
		} catch (calleeThrows) {
			try {
				// IE 8 throws on Object.getOwnPropertyDescriptor(arguments, '')
				return $gOPD(arguments, 'callee').get;
			} catch (gOPDthrows) {
				return throwTypeError;
			}
		}
	}())
	: throwTypeError;

var hasSymbols = __webpack_require__(273)();

var getProto = Object.getPrototypeOf || function (x) { return x.__proto__; }; // eslint-disable-line no-proto

var needsEval = {};

var TypedArray = typeof Uint8Array === 'undefined' ? undefined : getProto(Uint8Array);

var INTRINSICS = {
	'%AggregateError%': typeof AggregateError === 'undefined' ? undefined : AggregateError,
	'%Array%': Array,
	'%ArrayBuffer%': typeof ArrayBuffer === 'undefined' ? undefined : ArrayBuffer,
	'%ArrayIteratorPrototype%': hasSymbols ? getProto([][Symbol.iterator]()) : undefined,
	'%AsyncFromSyncIteratorPrototype%': undefined,
	'%AsyncFunction%': needsEval,
	'%AsyncGenerator%': needsEval,
	'%AsyncGeneratorFunction%': needsEval,
	'%AsyncIteratorPrototype%': needsEval,
	'%Atomics%': typeof Atomics === 'undefined' ? undefined : Atomics,
	'%BigInt%': typeof BigInt === 'undefined' ? undefined : BigInt,
	'%Boolean%': Boolean,
	'%DataView%': typeof DataView === 'undefined' ? undefined : DataView,
	'%Date%': Date,
	'%decodeURI%': decodeURI,
	'%decodeURIComponent%': decodeURIComponent,
	'%encodeURI%': encodeURI,
	'%encodeURIComponent%': encodeURIComponent,
	'%Error%': Error,
	'%eval%': eval, // eslint-disable-line no-eval
	'%EvalError%': EvalError,
	'%Float32Array%': typeof Float32Array === 'undefined' ? undefined : Float32Array,
	'%Float64Array%': typeof Float64Array === 'undefined' ? undefined : Float64Array,
	'%FinalizationRegistry%': typeof FinalizationRegistry === 'undefined' ? undefined : FinalizationRegistry,
	'%Function%': $Function,
	'%GeneratorFunction%': needsEval,
	'%Int8Array%': typeof Int8Array === 'undefined' ? undefined : Int8Array,
	'%Int16Array%': typeof Int16Array === 'undefined' ? undefined : Int16Array,
	'%Int32Array%': typeof Int32Array === 'undefined' ? undefined : Int32Array,
	'%isFinite%': isFinite,
	'%isNaN%': isNaN,
	'%IteratorPrototype%': hasSymbols ? getProto(getProto([][Symbol.iterator]())) : undefined,
	'%JSON%': typeof JSON === 'object' ? JSON : undefined,
	'%Map%': typeof Map === 'undefined' ? undefined : Map,
	'%MapIteratorPrototype%': typeof Map === 'undefined' || !hasSymbols ? undefined : getProto(new Map()[Symbol.iterator]()),
	'%Math%': Math,
	'%Number%': Number,
	'%Object%': Object,
	'%parseFloat%': parseFloat,
	'%parseInt%': parseInt,
	'%Promise%': typeof Promise === 'undefined' ? undefined : Promise,
	'%Proxy%': typeof Proxy === 'undefined' ? undefined : Proxy,
	'%RangeError%': RangeError,
	'%ReferenceError%': ReferenceError,
	'%Reflect%': typeof Reflect === 'undefined' ? undefined : Reflect,
	'%RegExp%': RegExp,
	'%Set%': typeof Set === 'undefined' ? undefined : Set,
	'%SetIteratorPrototype%': typeof Set === 'undefined' || !hasSymbols ? undefined : getProto(new Set()[Symbol.iterator]()),
	'%SharedArrayBuffer%': typeof SharedArrayBuffer === 'undefined' ? undefined : SharedArrayBuffer,
	'%String%': String,
	'%StringIteratorPrototype%': hasSymbols ? getProto(''[Symbol.iterator]()) : undefined,
	'%Symbol%': hasSymbols ? Symbol : undefined,
	'%SyntaxError%': $SyntaxError,
	'%ThrowTypeError%': ThrowTypeError,
	'%TypedArray%': TypedArray,
	'%TypeError%': $TypeError,
	'%Uint8Array%': typeof Uint8Array === 'undefined' ? undefined : Uint8Array,
	'%Uint8ClampedArray%': typeof Uint8ClampedArray === 'undefined' ? undefined : Uint8ClampedArray,
	'%Uint16Array%': typeof Uint16Array === 'undefined' ? undefined : Uint16Array,
	'%Uint32Array%': typeof Uint32Array === 'undefined' ? undefined : Uint32Array,
	'%URIError%': URIError,
	'%WeakMap%': typeof WeakMap === 'undefined' ? undefined : WeakMap,
	'%WeakRef%': typeof WeakRef === 'undefined' ? undefined : WeakRef,
	'%WeakSet%': typeof WeakSet === 'undefined' ? undefined : WeakSet
};

var doEval = function doEval(name) {
	var value;
	if (name === '%AsyncFunction%') {
		value = getEvalledConstructor('async function () {}');
	} else if (name === '%GeneratorFunction%') {
		value = getEvalledConstructor('function* () {}');
	} else if (name === '%AsyncGeneratorFunction%') {
		value = getEvalledConstructor('async function* () {}');
	} else if (name === '%AsyncGenerator%') {
		var fn = doEval('%AsyncGeneratorFunction%');
		if (fn) {
			value = fn.prototype;
		}
	} else if (name === '%AsyncIteratorPrototype%') {
		var gen = doEval('%AsyncGenerator%');
		if (gen) {
			value = getProto(gen.prototype);
		}
	}

	INTRINSICS[name] = value;

	return value;
};

var LEGACY_ALIASES = {
	'%ArrayBufferPrototype%': ['ArrayBuffer', 'prototype'],
	'%ArrayPrototype%': ['Array', 'prototype'],
	'%ArrayProto_entries%': ['Array', 'prototype', 'entries'],
	'%ArrayProto_forEach%': ['Array', 'prototype', 'forEach'],
	'%ArrayProto_keys%': ['Array', 'prototype', 'keys'],
	'%ArrayProto_values%': ['Array', 'prototype', 'values'],
	'%AsyncFunctionPrototype%': ['AsyncFunction', 'prototype'],
	'%AsyncGenerator%': ['AsyncGeneratorFunction', 'prototype'],
	'%AsyncGeneratorPrototype%': ['AsyncGeneratorFunction', 'prototype', 'prototype'],
	'%BooleanPrototype%': ['Boolean', 'prototype'],
	'%DataViewPrototype%': ['DataView', 'prototype'],
	'%DatePrototype%': ['Date', 'prototype'],
	'%ErrorPrototype%': ['Error', 'prototype'],
	'%EvalErrorPrototype%': ['EvalError', 'prototype'],
	'%Float32ArrayPrototype%': ['Float32Array', 'prototype'],
	'%Float64ArrayPrototype%': ['Float64Array', 'prototype'],
	'%FunctionPrototype%': ['Function', 'prototype'],
	'%Generator%': ['GeneratorFunction', 'prototype'],
	'%GeneratorPrototype%': ['GeneratorFunction', 'prototype', 'prototype'],
	'%Int8ArrayPrototype%': ['Int8Array', 'prototype'],
	'%Int16ArrayPrototype%': ['Int16Array', 'prototype'],
	'%Int32ArrayPrototype%': ['Int32Array', 'prototype'],
	'%JSONParse%': ['JSON', 'parse'],
	'%JSONStringify%': ['JSON', 'stringify'],
	'%MapPrototype%': ['Map', 'prototype'],
	'%NumberPrototype%': ['Number', 'prototype'],
	'%ObjectPrototype%': ['Object', 'prototype'],
	'%ObjProto_toString%': ['Object', 'prototype', 'toString'],
	'%ObjProto_valueOf%': ['Object', 'prototype', 'valueOf'],
	'%PromisePrototype%': ['Promise', 'prototype'],
	'%PromiseProto_then%': ['Promise', 'prototype', 'then'],
	'%Promise_all%': ['Promise', 'all'],
	'%Promise_reject%': ['Promise', 'reject'],
	'%Promise_resolve%': ['Promise', 'resolve'],
	'%RangeErrorPrototype%': ['RangeError', 'prototype'],
	'%ReferenceErrorPrototype%': ['ReferenceError', 'prototype'],
	'%RegExpPrototype%': ['RegExp', 'prototype'],
	'%SetPrototype%': ['Set', 'prototype'],
	'%SharedArrayBufferPrototype%': ['SharedArrayBuffer', 'prototype'],
	'%StringPrototype%': ['String', 'prototype'],
	'%SymbolPrototype%': ['Symbol', 'prototype'],
	'%SyntaxErrorPrototype%': ['SyntaxError', 'prototype'],
	'%TypedArrayPrototype%': ['TypedArray', 'prototype'],
	'%TypeErrorPrototype%': ['TypeError', 'prototype'],
	'%Uint8ArrayPrototype%': ['Uint8Array', 'prototype'],
	'%Uint8ClampedArrayPrototype%': ['Uint8ClampedArray', 'prototype'],
	'%Uint16ArrayPrototype%': ['Uint16Array', 'prototype'],
	'%Uint32ArrayPrototype%': ['Uint32Array', 'prototype'],
	'%URIErrorPrototype%': ['URIError', 'prototype'],
	'%WeakMapPrototype%': ['WeakMap', 'prototype'],
	'%WeakSetPrototype%': ['WeakSet', 'prototype']
};

var bind = __webpack_require__(223);
var hasOwn = __webpack_require__(447);
var $concat = bind.call(Function.call, Array.prototype.concat);
var $spliceApply = bind.call(Function.apply, Array.prototype.splice);
var $replace = bind.call(Function.call, String.prototype.replace);
var $strSlice = bind.call(Function.call, String.prototype.slice);

/* adapted from https://github.com/lodash/lodash/blob/4.17.15/dist/lodash.js#L6735-L6744 */
var rePropName = /[^%.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|%$))/g;
var reEscapeChar = /\\(\\)?/g; /** Used to match backslashes in property paths. */
var stringToPath = function stringToPath(string) {
	var first = $strSlice(string, 0, 1);
	var last = $strSlice(string, -1);
	if (first === '%' && last !== '%') {
		throw new $SyntaxError('invalid intrinsic syntax, expected closing `%`');
	} else if (last === '%' && first !== '%') {
		throw new $SyntaxError('invalid intrinsic syntax, expected opening `%`');
	}
	var result = [];
	$replace(string, rePropName, function (match, number, quote, subString) {
		result[result.length] = quote ? $replace(subString, reEscapeChar, '$1') : number || match;
	});
	return result;
};
/* end adaptation */

var getBaseIntrinsic = function getBaseIntrinsic(name, allowMissing) {
	var intrinsicName = name;
	var alias;
	if (hasOwn(LEGACY_ALIASES, intrinsicName)) {
		alias = LEGACY_ALIASES[intrinsicName];
		intrinsicName = '%' + alias[0] + '%';
	}

	if (hasOwn(INTRINSICS, intrinsicName)) {
		var value = INTRINSICS[intrinsicName];
		if (value === needsEval) {
			value = doEval(intrinsicName);
		}
		if (typeof value === 'undefined' && !allowMissing) {
			throw new $TypeError('intrinsic ' + name + ' exists, but is not available. Please file an issue!');
		}

		return {
			alias: alias,
			name: intrinsicName,
			value: value
		};
	}

	throw new $SyntaxError('intrinsic ' + name + ' does not exist!');
};

module.exports = function GetIntrinsic(name, allowMissing) {
	if (typeof name !== 'string' || name.length === 0) {
		throw new $TypeError('intrinsic name must be a non-empty string');
	}
	if (arguments.length > 1 && typeof allowMissing !== 'boolean') {
		throw new $TypeError('"allowMissing" argument must be a boolean');
	}

	var parts = stringToPath(name);
	var intrinsicBaseName = parts.length > 0 ? parts[0] : '';

	var intrinsic = getBaseIntrinsic('%' + intrinsicBaseName + '%', allowMissing);
	var intrinsicRealName = intrinsic.name;
	var value = intrinsic.value;
	var skipFurtherCaching = false;

	var alias = intrinsic.alias;
	if (alias) {
		intrinsicBaseName = alias[0];
		$spliceApply(parts, $concat([0, 1], alias));
	}

	for (var i = 1, isOwn = true; i < parts.length; i += 1) {
		var part = parts[i];
		var first = $strSlice(part, 0, 1);
		var last = $strSlice(part, -1);
		if (
			(
				(first === '"' || first === "'" || first === '`')
				|| (last === '"' || last === "'" || last === '`')
			)
			&& first !== last
		) {
			throw new $SyntaxError('property names with quotes must have matching quotes');
		}
		if (part === 'constructor' || !isOwn) {
			skipFurtherCaching = true;
		}

		intrinsicBaseName += '.' + part;
		intrinsicRealName = '%' + intrinsicBaseName + '%';

		if (hasOwn(INTRINSICS, intrinsicRealName)) {
			value = INTRINSICS[intrinsicRealName];
		} else if (value != null) {
			if (!(part in value)) {
				if (!allowMissing) {
					throw new $TypeError('base intrinsic for ' + name + ' exists, but the property is not available.');
				}
				return void undefined;
			}
			if ($gOPD && (i + 1) >= parts.length) {
				var desc = $gOPD(value, part);
				isOwn = !!desc;

				// By convention, when a data property is converted to an accessor
				// property to emulate a data property that does not suffer from
				// the override mistake, that accessor's getter is marked with
				// an `originalValue` property. Here, when we detect this, we
				// uphold the illusion by pretending to see that original data
				// property, i.e., returning the value rather than the getter
				// itself.
				if (isOwn && 'get' in desc && !('originalValue' in desc.get)) {
					value = desc.get;
				} else {
					value = value[part];
				}
			} else {
				isOwn = hasOwn(value, part);
				value = value[part];
			}

			if (isOwn && !skipFurtherCaching) {
				INTRINSICS[intrinsicRealName] = value;
			}
		}
	}
	return value;
};


/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var origSymbol = global.Symbol;
var hasSymbolSham = __webpack_require__(445);

module.exports = function hasNativeSymbols() {
	if (typeof origSymbol !== 'function') { return false; }
	if (typeof Symbol !== 'function') { return false; }
	if (typeof origSymbol('foo') !== 'symbol') { return false; }
	if (typeof Symbol('bar') !== 'symbol') { return false; }

	return hasSymbolSham();
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var numberIsNaN = function (value) {
	return value !== value;
};

module.exports = function is(a, b) {
	if (a === 0 && b === 0) {
		return 1 / a === 1 / b;
	}
	if (a === b) {
		return true;
	}
	if (numberIsNaN(a) && numberIsNaN(b)) {
		return true;
	}
	return false;
};



/***/ }),

/***/ 275:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(274);

module.exports = function getPolyfill() {
	return typeof Object.is === 'function' ? Object.is : implementation;
};


/***/ }),

/***/ 276:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $Object = Object;
var $TypeError = TypeError;

module.exports = function flags() {
	if (this != null && this !== $Object(this)) {
		throw new $TypeError('RegExp.prototype.flags getter called on non-object');
	}
	var result = '';
	if (this.global) {
		result += 'g';
	}
	if (this.ignoreCase) {
		result += 'i';
	}
	if (this.multiline) {
		result += 'm';
	}
	if (this.dotAll) {
		result += 's';
	}
	if (this.unicode) {
		result += 'u';
	}
	if (this.sticky) {
		result += 'y';
	}
	return result;
};


/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(276);

var supportsDescriptors = __webpack_require__(168).supportsDescriptors;
var $gOPD = Object.getOwnPropertyDescriptor;
var $TypeError = TypeError;

module.exports = function getPolyfill() {
	if (!supportsDescriptors) {
		throw new $TypeError('RegExp.prototype.flags requires a true ES5 environment that supports property descriptors');
	}
	if ((/a/mig).flags === 'gim') {
		var descriptor = $gOPD(RegExp.prototype, 'flags');
		if (descriptor && typeof descriptor.get === 'function' && typeof (/a/).dotAll === 'boolean') {
			return descriptor.get;
		}
	}
	return implementation;
};


/***/ }),

/***/ 278:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = void 0;

var _default = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 279:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.withContext = void 0;

var _noop = _interopRequireDefault(__webpack_require__(222));

var _pick = _interopRequireDefault(__webpack_require__(243));

var _createReactContext = _interopRequireDefault(__webpack_require__(321));

var _react = _interopRequireDefault(__webpack_require__(1));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TypeaheadContext = (0, _createReactContext["default"])({
  activeIndex: -1,
  hintText: '',
  initialItem: null,
  isOnlyResult: false,
  onActiveItemChange: _noop["default"],
  onAdd: _noop["default"],
  onInitialItemChange: _noop["default"],
  onMenuItemClick: _noop["default"],
  selectHintOnEnter: false
});

var withContext = function withContext(Component, values) {
  // Note: Use a class instead of function component to support refs.

  /* eslint-disable-next-line react/prefer-stateless-function */
  return (
    /*#__PURE__*/
    function (_React$Component) {
      _inherits(_class, _React$Component);

      function _class() {
        _classCallCheck(this, _class);

        return _possibleConstructorReturn(this, _getPrototypeOf(_class).apply(this, arguments));
      }

      _createClass(_class, [{
        key: "render",
        value: function render() {
          var _this = this;

          return _react["default"].createElement(TypeaheadContext.Consumer, null, function (context) {
            return _react["default"].createElement(Component, _extends({}, _this.props, (0, _pick["default"])(context, values)));
          });
        }
      }]);

      return _class;
    }(_react["default"].Component)
  );
};

exports.withContext = withContext;
var _default = TypeaheadContext;
exports["default"] = _default;

/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.BaseMenuItem = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _noop = _interopRequireDefault(__webpack_require__(222));

var _react = _interopRequireDefault(__webpack_require__(1));

var _menuItemContainer = _interopRequireDefault(__webpack_require__(331));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BaseMenuItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BaseMenuItem, _React$Component);

  function BaseMenuItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, BaseMenuItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(BaseMenuItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_handleClick", function (e) {
      var _this$props = _this.props,
          disabled = _this$props.disabled,
          onClick = _this$props.onClick;
      e.preventDefault();
      !disabled && onClick(e);
    });

    return _this;
  }

  _createClass(BaseMenuItem, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          active = _this$props2.active,
          children = _this$props2.children,
          className = _this$props2.className,
          disabled = _this$props2.disabled,
          onClick = _this$props2.onClick,
          onMouseDown = _this$props2.onMouseDown,
          props = _objectWithoutProperties(_this$props2, ["active", "children", "className", "disabled", "onClick", "onMouseDown"]);

      var conditionalClassNames = {
        active: active,
        disabled: disabled
      };
      return (
        /* eslint-disable jsx-a11y/anchor-is-valid */
        _react["default"].createElement("li", _extends({}, props, {
          className: (0, _classnames["default"])(conditionalClassNames, className)
        }), _react["default"].createElement("a", {
          className: (0, _classnames["default"])('dropdown-item', conditionalClassNames),
          href: "#",
          onClick: this._handleClick,
          onMouseDown: onMouseDown
        }, children))
        /* eslint-enable jsx-a11y/anchor-is-valid */

      );
    }
  }]);

  return BaseMenuItem;
}(_react["default"].Component);

exports.BaseMenuItem = BaseMenuItem;
BaseMenuItem.defaultProps = {
  onClick: _noop["default"]
};

var _default = (0, _menuItemContainer["default"])(BaseMenuItem);

exports["default"] = _default;

/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    getPrototype = __webpack_require__(346),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var objectTag = '[object Object]';

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to infer the `Object` constructor. */
var objectCtorString = funcToString.call(Object);

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * @static
 * @memberOf _
 * @since 0.8.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  if (!isObjectLike(value) || baseGetTag(value) != objectTag) {
    return false;
  }
  var proto = getPrototype(value);
  if (proto === null) {
    return true;
  }
  var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
    funcToString.call(Ctor) == objectCtorString;
}

module.exports = isPlainObject;


/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _debounce = _interopRequireDefault(__webpack_require__(475));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes2 = __webpack_require__(315);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DEFAULT_DELAY_MS = 200;
/**
 * HoC that encapsulates common behavior and functionality for doing
 * asynchronous searches, including:
 *
 *  - Debouncing user input
 *  - Optional query caching
 *  - Search prompt and empty results behaviors
 */

var asyncContainer = function asyncContainer(Typeahead) {
  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_cache", {});

      _defineProperty(_assertThisInitialized(_this), "_query", _this.props.defaultInputValue || '');

      _defineProperty(_assertThisInitialized(_this), "_getEmptyLabel", function () {
        var _this$props = _this.props,
            emptyLabel = _this$props.emptyLabel,
            isLoading = _this$props.isLoading,
            promptText = _this$props.promptText,
            searchText = _this$props.searchText;

        if (!_this._query.length) {
          return promptText;
        }

        if (isLoading) {
          return searchText;
        }

        return emptyLabel;
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInputChange", function (query, e) {
        _this.props.onInputChange && _this.props.onInputChange(query, e);

        _this._handleSearchDebounced(query);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSearch", function (query) {
        _this._query = query;
        var _this$props2 = _this.props,
            minLength = _this$props2.minLength,
            onSearch = _this$props2.onSearch,
            useCache = _this$props2.useCache;

        if (!query || minLength && query.length < minLength) {
          return;
        } // Use cached results, if applicable.


        if (useCache && _this._cache[query]) {
          // Re-render the component with the cached results.
          _this.forceUpdate();

          return;
        } // Perform the search.


        onSearch(query);
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this._handleSearchDebounced = (0, _debounce["default"])(this._handleSearch, this.props.delay);
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        if (prevProps.isLoading && this.props.useCache) {
          this._cache[this._query] = this.props.options;
        }
      }
    }, {
      key: "componentWillUnmount",
      value: function componentWillUnmount() {
        this._cache = {};
        this._query = '';

        this._handleSearchDebounced.cancel();
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var _this$props3 = this.props,
            options = _this$props3.options,
            useCache = _this$props3.useCache,
            props = _objectWithoutProperties(_this$props3, ["options", "useCache"]);

        var cachedQuery = this._cache[this._query]; // Disable custom selections during a search unless `allowNew` is a
        // function.

        var allowNew = typeof props.allowNew === 'function' ? props.allowNew : props.allowNew && !props.isLoading;
        return _react["default"].createElement(Typeahead, _extends({}, props, {
          allowNew: allowNew,
          emptyLabel: this._getEmptyLabel(),
          onInputChange: this._handleInputChange,
          options: useCache && cachedQuery ? cachedQuery : options,
          ref: function ref(instance) {
            return _this2._instance = instance;
          }
        }));
      }
      /**
       * Make the component instance available.
       */

    }, {
      key: "getInstance",
      value: function getInstance() {
        return this._instance;
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  WrappedTypeahead.displayName = "AsyncContainer(".concat((0, _utils.getDisplayName)(Typeahead), ")");
  WrappedTypeahead.propTypes = {
    /**
     * Delay, in milliseconds, before performing search.
     */
    delay: _propTypes["default"].number,

    /**
     * Whether or not a request is currently pending. Necessary for the
     * container to know when new results are available.
     */
    isLoading: _propTypes["default"].bool.isRequired,

    /**
     * Number of input characters that must be entered before showing results.
     */
    minLength: _propTypes["default"].number,

    /**
     * Callback to perform when the search is executed.
     */
    onSearch: _propTypes["default"].func.isRequired,

    /**
     * Options to be passed to the typeahead. Will typically be the query
     * results, but can also be initial default options.
     */
    options: _propTypes2.optionType,

    /**
     * Message displayed in the menu when there is no user input.
     */
    promptText: _propTypes["default"].node,

    /**
     * Message displayed in the menu while the request is pending.
     */
    searchText: _propTypes["default"].node,

    /**
     * Whether or not the component should cache query results.
     */
    useCache: _propTypes["default"].bool
  };
  WrappedTypeahead.defaultProps = {
    delay: DEFAULT_DELAY_MS,
    minLength: 2,
    options: [],
    promptText: 'Type to search...',
    searchText: 'Searching...',
    useCache: true
  };
  return WrappedTypeahead;
};

var _default = asyncContainer;
exports["default"] = _default;

/***/ }),

/***/ 315:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "caseSensitiveType", {
  enumerable: true,
  get: function get() {
    return _caseSensitiveType2["default"];
  }
});
Object.defineProperty(exports, "checkPropType", {
  enumerable: true,
  get: function get() {
    return _checkPropType2["default"];
  }
});
Object.defineProperty(exports, "defaultInputValueType", {
  enumerable: true,
  get: function get() {
    return _defaultInputValueType2["default"];
  }
});
Object.defineProperty(exports, "emptyLabelType", {
  enumerable: true,
  get: function get() {
    return _emptyLabelType2["default"];
  }
});
Object.defineProperty(exports, "highlightOnlyResultType", {
  enumerable: true,
  get: function get() {
    return _highlightOnlyResultType2["default"];
  }
});
Object.defineProperty(exports, "idType", {
  enumerable: true,
  get: function get() {
    return _idType2["default"];
  }
});
Object.defineProperty(exports, "ignoreDiacriticsType", {
  enumerable: true,
  get: function get() {
    return _ignoreDiacriticsType2["default"];
  }
});
Object.defineProperty(exports, "inputPropsType", {
  enumerable: true,
  get: function get() {
    return _inputPropsType2["default"];
  }
});
Object.defineProperty(exports, "labelKeyType", {
  enumerable: true,
  get: function get() {
    return _labelKeyType2["default"];
  }
});
Object.defineProperty(exports, "optionType", {
  enumerable: true,
  get: function get() {
    return _optionType2["default"];
  }
});
Object.defineProperty(exports, "selectedType", {
  enumerable: true,
  get: function get() {
    return _selectedType2["default"];
  }
});

var _caseSensitiveType2 = _interopRequireDefault(__webpack_require__(513));

var _checkPropType2 = _interopRequireDefault(__webpack_require__(514));

var _defaultInputValueType2 = _interopRequireDefault(__webpack_require__(515));

var _emptyLabelType2 = _interopRequireDefault(__webpack_require__(516));

var _highlightOnlyResultType2 = _interopRequireDefault(__webpack_require__(517));

var _idType2 = _interopRequireDefault(__webpack_require__(518));

var _ignoreDiacriticsType2 = _interopRequireDefault(__webpack_require__(519));

var _inputPropsType2 = _interopRequireDefault(__webpack_require__(520));

var _labelKeyType2 = _interopRequireDefault(__webpack_require__(521));

var _optionType2 = _interopRequireDefault(__webpack_require__(522));

var _selectedType2 = _interopRequireDefault(__webpack_require__(523));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 316:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getMatchBounds;

var _escapeStringRegexp = _interopRequireDefault(__webpack_require__(530));

var _stripDiacritics = _interopRequireDefault(__webpack_require__(265));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var CASE_INSENSITIVE = 'i';
var COMBINING_MARKS = /[\u0300-\u036F]/;

function getMatchBounds(subject, str) {
  var search = new RegExp((0, _escapeStringRegexp["default"])((0, _stripDiacritics["default"])(str)), CASE_INSENSITIVE);
  var matches = search.exec((0, _stripDiacritics["default"])(subject));

  if (!matches) {
    return null;
  }

  var start = matches.index;
  var matchLength = matches[0].length; // Account for combining marks, which changes the indices.

  if (COMBINING_MARKS.test(subject)) {
    // Starting at the beginning of the subject string, check for the number of
    // combining marks and increment the start index whenever one is found.
    for (var ii = 0; ii <= start; ii++) {
      if (COMBINING_MARKS.test(subject[ii])) {
        start += 1;
      }
    } // Similarly, increment the length of the match string if it contains a
    // combining mark.


    for (var _ii = start; _ii <= start + matchLength; _ii++) {
      if (COMBINING_MARKS.test(subject[_ii])) {
        matchLength += 1;
      }
    }
  }

  return {
    end: start + matchLength,
    start: start
  };
}

/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = isSelectable;

/**
 * Check if an input type is selectable, based on WHATWG spec.
 *
 * See:
 *  - https://stackoverflow.com/questions/21177489/selectionstart-selectionend-on-input-type-number-no-longer-allowed-in-chrome/24175357
 *  - https://html.spec.whatwg.org/multipage/input.html#do-not-apply
 */
function isSelectable(inputNode) {
  return inputNode.selectionStart != null;
}

/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _pick = _interopRequireDefault(__webpack_require__(243));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _reactDom = __webpack_require__(15);

var _ClearButton = _interopRequireDefault(__webpack_require__(319));

var _Loader = _interopRequireDefault(__webpack_require__(540));

var _Overlay = _interopRequireDefault(__webpack_require__(541));

var _TypeaheadInputMulti = _interopRequireDefault(__webpack_require__(552));

var _TypeaheadInputSingle = _interopRequireDefault(__webpack_require__(557));

var _TypeaheadMenu = _interopRequireDefault(__webpack_require__(328));

var _typeaheadContainer = _interopRequireDefault(__webpack_require__(559));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Typeahead =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Typeahead, _React$Component);

  function Typeahead() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Typeahead);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Typeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderInput", function (inputProps) {
      var Input = inputProps.multiple ? _TypeaheadInputMulti["default"] : _TypeaheadInputSingle["default"];
      return _react["default"].createElement(Input, inputProps);
    });

    _defineProperty(_assertThisInitialized(_this), "_renderAux", function () {
      var _this$props = _this.props,
          bsSize = _this$props.bsSize,
          clearButton = _this$props.clearButton,
          disabled = _this$props.disabled,
          isLoading = _this$props.isLoading,
          onClear = _this$props.onClear,
          selected = _this$props.selected;
      var content;

      if (isLoading) {
        content = _react["default"].createElement(_Loader["default"], {
          bsSize: bsSize
        });
      } else if (clearButton && !disabled && selected.length) {
        content = _react["default"].createElement(_ClearButton["default"], {
          bsSize: bsSize,
          onClick: onClear,
          onFocus: function onFocus(e) {
            // Prevent the main input from auto-focusing again.
            e.stopPropagation();
          },
          onMouseDown: _utils.preventInputBlur
        });
      }

      return content ? _react["default"].createElement("div", {
        className: (0, _classnames["default"])('rbt-aux', {
          'rbt-aux-lg': bsSize === 'large' || bsSize === 'lg'
        })
      }, content) : null;
    });

    return _this;
  }

  _createClass(Typeahead, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          bodyContainer = _this$props2.bodyContainer,
          children = _this$props2.children,
          className = _this$props2.className,
          isMenuShown = _this$props2.isMenuShown,
          menuId = _this$props2.menuId,
          renderMenu = _this$props2.renderMenu,
          results = _this$props2.results;
      var inputProps = (0, _pick["default"])(this.props, ['activeIndex', 'activeItem', 'bsSize', 'disabled', 'inputProps', 'inputRef', 'isFocused', 'isInvalid', 'isMenuShown', 'isValid', 'labelKey', 'menuId', 'multiple', 'onBlur', 'onChange', 'onFocus', 'onKeyDown', 'onRemove', 'placeholder', 'renderToken', 'selected', 'text']);
      var overlayProps = (0, _pick["default"])(this.props, ['align', 'className', 'dropup', 'flip', 'onMenuHide', 'onMenuShow', 'onMenuToggle']);
      var menuProps = (0, _pick["default"])(this.props, ['emptyLabel', 'labelKey', 'maxHeight', 'newSelectionPrefix', 'renderMenuItemChildren', 'text']);

      var auxContent = this._renderAux();

      return _react["default"].createElement("div", {
        className: (0, _classnames["default"])('rbt', 'clearfix', 'open', {
          'has-aux': !!auxContent
        }, className),
        style: {
          position: 'relative'
        },
        tabIndex: -1
      }, this._renderInput(_objectSpread({}, inputProps, {
        // Use `findDOMNode` here since it's easier and less fragile than
        // forwarding refs down to the input's container.
        // TODO: Consider using `forwardRef` when React 16.3 usage is higher.

        /* eslint-disable-next-line react/no-find-dom-node */
        ref: function ref(node) {
          return _this2._inputContainer = (0, _reactDom.findDOMNode)(node);
        }
      })), typeof children === 'function' ? children(this.props) : children, auxContent, _react["default"].createElement(_Overlay["default"], _extends({}, overlayProps, {
        container: bodyContainer ? document.body : this,
        referenceElement: this._inputContainer,
        show: isMenuShown
      }), renderMenu(results, _objectSpread({}, menuProps, {
        id: menuId
      }))), _react["default"].createElement("div", {
        "aria-atomic": true,
        "aria-live": "polite",
        className: "sr-only rbt-sr-status",
        role: "status"
      }, (0, _utils.getAccessibilityStatus)(this.props)));
    }
  }]);

  return Typeahead;
}(_react["default"].Component);

Typeahead.propTypes = {
  renderMenu: _propTypes["default"].func
};
Typeahead.defaultProps = {
  renderMenu: function renderMenu(results, menuProps) {
    return _react["default"].createElement(_TypeaheadMenu["default"], _extends({}, menuProps, {
      options: results
    }));
  }
};

var _default = (0, _typeaheadContainer["default"])(Typeahead);

exports["default"] = _default;

/***/ }),

/***/ 319:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * ClearButton
 *
 * http://getbootstrap.com/css/#helper-classes-close
 */
var ClearButton = function ClearButton(_ref) {
  var bsSize = _ref.bsSize,
      className = _ref.className,
      label = _ref.label,
      _onClick = _ref.onClick,
      props = _objectWithoutProperties(_ref, ["bsSize", "className", "label", "onClick"]);

  return _react["default"].createElement("button", _extends({}, props, {
    "aria-label": label,
    className: (0, _classnames["default"])('close', 'rbt-close', {
      'rbt-close-lg': bsSize === 'large' || bsSize === 'lg'
    }, className),
    onClick: function onClick(e) {
      e.stopPropagation();

      _onClick(e);
    },
    type: "button"
  }), _react["default"].createElement("span", {
    "aria-hidden": "true"
  }, "\xD7"), _react["default"].createElement("span", {
    className: "sr-only"
  }, label));
};

ClearButton.propTypes = {
  bsSize: _propTypes["default"].oneOf(['large', 'lg', 'small', 'sm']),
  label: _propTypes["default"].string,
  onClick: _propTypes["default"].func.isRequired
};
ClearButton.defaultProps = {
  label: 'Clear'
};
var _default = ClearButton;
exports["default"] = _default;

/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = getContainer;

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getContainer(container, defaultContainer) {
  container = typeof container === 'function' ? container() : container;
  return _reactDom2.default.findDOMNode(container) || defaultContainer;
}
module.exports = exports['default'];

/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _implementation = __webpack_require__(549);

var _implementation2 = _interopRequireDefault(_implementation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createContext || _implementation2.default;
module.exports = exports['default'];

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SIZER_STYLE = {
  height: 0,
  left: 0,
  overflow: 'scroll',
  position: 'absolute',
  top: 0,
  visibility: 'hidden',
  whiteSpace: 'pre'
};
var INPUT_PROPS_BLACKLIST = ['inputClassName', 'inputRef', 'inputStyle'];
var MIN_WIDTH = 1;

var cleanInputProps = function cleanInputProps(inputProps) {
  var cleanProps = {};
  Object.keys(inputProps).forEach(function (key) {
    if (INPUT_PROPS_BLACKLIST.indexOf(key) === -1) {
      cleanProps[key] = inputProps[key];
    }
  });
  return cleanProps;
};

var copyStyles = function copyStyles(styles, node) {
  /* eslint-disable no-param-reassign */
  node.style.fontSize = styles.fontSize;
  node.style.fontFamily = styles.fontFamily;
  node.style.fontWeight = styles.fontWeight;
  node.style.fontStyle = styles.fontStyle;
  node.style.letterSpacing = styles.letterSpacing;
  node.style.textTransform = styles.textTransform;
  /* eslint-enable no-param-reassign */
};

var AutosizeInput =
/*#__PURE__*/
function (_React$Component) {
  _inherits(AutosizeInput, _React$Component);

  function AutosizeInput() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, AutosizeInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(AutosizeInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      inputWidth: MIN_WIDTH
    });

    _defineProperty(_assertThisInitialized(_this), "_copyInputStyles", function () {
      var inputStyles = _this._input && window.getComputedStyle && window.getComputedStyle(_this._input);

      if (!inputStyles) {
        return;
      }

      copyStyles(inputStyles, _this._sizer);

      if (_this._placeHolderSizer) {
        copyStyles(inputStyles, _this._placeHolderSizer);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "_updateInputWidth", function () {
      if (!_this._sizer || _this._sizer.scrollWidth === undefined) {
        return;
      }

      _this._copyInputStyles();

      var placeholderWidth = _this._placeHolderSizer && _this._placeHolderSizer.scrollWidth || MIN_WIDTH;
      var inputWidth = Math.max(_this._sizer.scrollWidth, placeholderWidth) + 2;

      if (inputWidth !== _this.state.inputWidth) {
        _this.setState({
          inputWidth: inputWidth
        });
      }
    });

    return _this;
  }

  _createClass(AutosizeInput, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this._updateInputWidth();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      this._updateInputWidth();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          className = _this$props.className,
          defaultValue = _this$props.defaultValue,
          placeholder = _this$props.placeholder,
          value = _this$props.value;

      var wrapperStyle = _objectSpread({}, this.props.style);

      if (!wrapperStyle.display) {
        wrapperStyle.display = 'inline-block';
      }

      var inputProps = cleanInputProps(_objectSpread({}, this.props, {
        className: this.props.inputClassName,
        style: _objectSpread({}, this.props.inputStyle, {
          boxSizing: 'content-box',
          width: "".concat(this.state.inputWidth, "px")
        })
      }));
      return _react["default"].createElement("div", {
        className: className,
        style: wrapperStyle
      }, _react["default"].createElement("input", _extends({}, inputProps, {
        ref: function ref(el) {
          _this2._input = el;

          if (typeof _this2.props.inputRef === 'function') {
            _this2.props.inputRef(el);
          }
        }
      })), _react["default"].createElement("div", {
        ref: function ref(el) {
          return _this2._sizer = el;
        },
        style: SIZER_STYLE
      }, defaultValue || value || ''), placeholder ? _react["default"].createElement("div", {
        ref: function ref(el) {
          return _this2._placeHolderSizer = el;
        },
        style: SIZER_STYLE
      }, placeholder) : null);
    }
  }]);

  return AutosizeInput;
}(_react["default"].Component);

AutosizeInput.propTypes = {
  /**
   * ClassName for the input element.
   */
  inputClassName: _propTypes["default"].string,

  /**
   * Ref callback for the input element.
   */
  inputRef: _propTypes["default"].func,

  /**
   * CSS styles for the input element.
   */

  /* eslint-disable-next-line react/forbid-prop-types */
  inputStyle: _propTypes["default"].object
};
var _default = AutosizeInput;
exports["default"] = _default;

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _ClearButton = _interopRequireDefault(__webpack_require__(319));

var _tokenContainer = _interopRequireDefault(__webpack_require__(324));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Token
 *
 * Individual token component, generally displayed within the TokenizerInput
 * component, but can also be rendered on its own.
 */
var Token =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Token, _React$Component);

  function Token() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Token);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Token)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderRemoveableToken", function () {
      var _this$props = _this.props,
          active = _this$props.active,
          children = _this$props.children,
          className = _this$props.className,
          onRemove = _this$props.onRemove,
          props = _objectWithoutProperties(_this$props, ["active", "children", "className", "onRemove"]);

      return _react["default"].createElement("div", _extends({}, props, {
        className: (0, _classnames["default"])('rbt-token', 'rbt-token-removeable', {
          'rbt-token-active': active
        }, className)
      }), children, _react["default"].createElement(_ClearButton["default"], {
        className: "rbt-token-remove-button",
        label: "Remove",
        onClick: onRemove,
        onKeyDown: _this._handleRemoveButtonKeydown,
        tabIndex: -1
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "_renderToken", function () {
      var _this$props2 = _this.props,
          children = _this$props2.children,
          className = _this$props2.className,
          disabled = _this$props2.disabled,
          href = _this$props2.href;
      var classnames = (0, _classnames["default"])('rbt-token', {
        'rbt-token-disabled': disabled
      }, className);

      if (href) {
        return _react["default"].createElement("a", {
          className: classnames,
          href: href
        }, children);
      }

      return _react["default"].createElement("div", {
        className: classnames
      }, children);
    });

    _defineProperty(_assertThisInitialized(_this), "_handleRemoveButtonKeydown", function (e) {
      switch (e.keyCode) {
        case _constants.RETURN:
          _this.props.onRemove();

          break;

        default:
          break;
      }
    });

    return _this;
  }

  _createClass(Token, [{
    key: "render",
    value: function render() {
      return this.props.onRemove && !this.props.disabled ? this._renderRemoveableToken() : this._renderToken();
    }
  }]);

  return Token;
}(_react["default"].Component);

Token.propTypes = {
  active: _propTypes["default"].bool,

  /**
   * Handler for removing/deleting the token. If not defined, the token will
   * be rendered in a read-only state.
   */
  onRemove: _propTypes["default"].func,
  tabIndex: _propTypes["default"].number
};
Token.defaultProps = {
  active: false,
  tabIndex: 0
};

var _default = (0, _tokenContainer["default"])(Token);

exports["default"] = _default;

/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _RootCloseWrapper = _interopRequireDefault(__webpack_require__(325));

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Higher-order component that encapsulates Token behaviors, allowing them to
 * be easily re-used.
 */
var tokenContainer = function tokenContainer(Component) {
  var WrappedComponent =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedComponent, _React$Component);

    function WrappedComponent() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedComponent);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "state", {
        active: false
      });

      _defineProperty(_assertThisInitialized(_this), "_handleBlur", function (e) {
        _this.setState({
          active: false
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        switch (e.keyCode) {
          case _constants.BACKSPACE:
            if (_this.state.active) {
              // Prevent backspace keypress from triggering the browser "back"
              // action.
              e.preventDefault();

              _this.props.onRemove();
            }

            break;

          default:
            break;
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActive", function (e) {
        e.stopPropagation();

        _this.setState({
          active: true
        });
      });

      return _this;
    }

    _createClass(WrappedComponent, [{
      key: "render",
      value: function render() {
        return _react["default"].createElement(_RootCloseWrapper["default"], {
          onRootClose: this._handleBlur
        }, _react["default"].createElement(Component, _extends({}, this.props, this.state, {
          onBlur: this._handleBlur,
          onClick: this._handleActive,
          onFocus: this._handleActive,
          onKeyDown: this._handleKeyDown
        })));
      }
    }]);

    return WrappedComponent;
  }(_react["default"].Component);

  WrappedComponent.displayName = "TokenContainer(".concat((0, _utils.getDisplayName)(Component), ")");
  return WrappedComponent;
};

var _default = tokenContainer;
exports["default"] = _default;

/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _contains = __webpack_require__(553);

var _contains2 = _interopRequireDefault(_contains);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _addEventListener = __webpack_require__(554);

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var escapeKeyCode = 27;

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

/**
 * The `<RootCloseWrapper/>` component registers your callback on the document
 * when rendered. Powers the `<Overlay/>` component. This is used achieve modal
 * style behavior where your callback is triggered when the user tries to
 * interact with the rest of the document or hits the `esc` key.
 */

var RootCloseWrapper = function (_React$Component) {
  _inherits(RootCloseWrapper, _React$Component);

  function RootCloseWrapper(props, context) {
    _classCallCheck(this, RootCloseWrapper);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props, context));

    _this.addEventListeners = function () {
      var event = _this.props.event;

      var doc = (0, _ownerDocument2.default)(_this);

      // Use capture for this listener so it fires before React's listener, to
      // avoid false positives in the contains() check below if the target DOM
      // element is removed in the React mouse callback.
      _this.documentMouseCaptureListener = (0, _addEventListener2.default)(doc, event, _this.handleMouseCapture, true);

      _this.documentMouseListener = (0, _addEventListener2.default)(doc, event, _this.handleMouse);

      _this.documentKeyupListener = (0, _addEventListener2.default)(doc, 'keyup', _this.handleKeyUp);
    };

    _this.removeEventListeners = function () {
      if (_this.documentMouseCaptureListener) {
        _this.documentMouseCaptureListener.remove();
      }

      if (_this.documentMouseListener) {
        _this.documentMouseListener.remove();
      }

      if (_this.documentKeyupListener) {
        _this.documentKeyupListener.remove();
      }
    };

    _this.handleMouseCapture = function (e) {
      _this.preventMouseRootClose = isModifiedEvent(e) || !isLeftClickEvent(e) || (0, _contains2.default)(_reactDom2.default.findDOMNode(_this), e.target);
    };

    _this.handleMouse = function (e) {
      if (!_this.preventMouseRootClose && _this.props.onRootClose) {
        _this.props.onRootClose(e);
      }
    };

    _this.handleKeyUp = function (e) {
      if (e.keyCode === escapeKeyCode && _this.props.onRootClose) {
        _this.props.onRootClose(e);
      }
    };

    _this.preventMouseRootClose = false;
    return _this;
  }

  RootCloseWrapper.prototype.componentDidMount = function componentDidMount() {
    if (!this.props.disabled) {
      this.addEventListeners();
    }
  };

  RootCloseWrapper.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
    if (!this.props.disabled && prevProps.disabled) {
      this.addEventListeners();
    } else if (this.props.disabled && !prevProps.disabled) {
      this.removeEventListeners();
    }
  };

  RootCloseWrapper.prototype.componentWillUnmount = function componentWillUnmount() {
    if (!this.props.disabled) {
      this.removeEventListeners();
    }
  };

  RootCloseWrapper.prototype.render = function render() {
    return this.props.children;
  };

  return RootCloseWrapper;
}(_react2.default.Component);

RootCloseWrapper.displayName = 'RootCloseWrapper';

RootCloseWrapper.propTypes = {
  /**
   * Callback fired after click or mousedown. Also triggers when user hits `esc`.
   */
  onRootClose: _propTypes2.default.func,
  /**
   * Children to render.
   */
  children: _propTypes2.default.element,
  /**
   * Disable the the RootCloseWrapper, preventing it from triggering `onRootClose`.
   */
  disabled: _propTypes2.default.bool,
  /**
   * Choose which document mouse event to bind to.
   */
  event: _propTypes2.default.oneOf(['click', 'mousedown'])
};

RootCloseWrapper.defaultProps = {
  event: 'click'
};

exports.default = RootCloseWrapper;
module.exports = exports['default'];

/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _AutosizeInput = _interopRequireDefault(__webpack_require__(322));

var _TypeaheadContext = __webpack_require__(279);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// IE doesn't seem to get the composite computed value (eg: 'padding',
// 'borderStyle', etc.), so generate these from the individual values.
function interpolateStyle(styles, attr) {
  var subattr = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  // Title-case the sub-attribute.
  if (subattr) {
    /* eslint-disable-next-line no-param-reassign */
    subattr = subattr.replace(subattr[0], subattr[0].toUpperCase());
  }

  return ['Top', 'Right', 'Bottom', 'Left'].map(function (dir) {
    return styles[attr + dir + subattr];
  }).join(' ');
}

function copyStyles(inputNode, hintNode) {
  var inputStyle = window.getComputedStyle(inputNode);
  /* eslint-disable no-param-reassign */

  hintNode.style.borderStyle = interpolateStyle(inputStyle, 'border', 'style');
  hintNode.style.borderWidth = interpolateStyle(inputStyle, 'border', 'width');
  hintNode.style.fontSize = inputStyle.fontSize;
  hintNode.style.lineHeight = inputStyle.lineHeight;
  hintNode.style.margin = interpolateStyle(inputStyle, 'margin');
  hintNode.style.padding = interpolateStyle(inputStyle, 'padding');
  /* eslint-enable no-param-reassign */
}

function hintContainer(Input) {
  var HintedInput =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(HintedInput, _React$Component);

    function HintedInput() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, HintedInput);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(HintedInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        var _this$props = _this.props,
            initialItem = _this$props.initialItem,
            onAdd = _this$props.onAdd,
            onKeyDown = _this$props.onKeyDown;

        if ((0, _utils.shouldSelectHint)(e, _this.props)) {
          e.preventDefault(); // Prevent input from blurring on TAB.

          onAdd(initialItem);
        }

        onKeyDown(e);
      });

      return _this;
    }

    _createClass(HintedInput, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        copyStyles(this._input, this._hint);
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate() {
        copyStyles(this._input, this._hint);
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var _this$props2 = this.props,
            hintText = _this$props2.hintText,
            initialItem = _this$props2.initialItem,
            _inputRef = _this$props2.inputRef,
            onAdd = _this$props2.onAdd,
            selectHintOnEnter = _this$props2.selectHintOnEnter,
            props = _objectWithoutProperties(_this$props2, ["hintText", "initialItem", "inputRef", "onAdd", "selectHintOnEnter"]);

        return _react["default"].createElement("div", {
          className: "rbt-input-hint-container",
          style: {
            position: 'relative'
          }
        }, _react["default"].createElement(Input, _extends({}, props, {
          inputRef: function inputRef(input) {
            _this2._input = input;

            _inputRef(input);
          },
          onKeyDown: this._handleKeyDown
        })), _react["default"].createElement(_AutosizeInput["default"], {
          "aria-hidden": true,
          className: "rbt-input-hint",
          inputRef: function inputRef(hint) {
            return _this2._hint = hint;
          },
          inputStyle: {
            backgroundColor: 'transparent',
            borderColor: 'transparent',
            boxShadow: 'none',
            color: 'rgba(0, 0, 0, 0.35)'
          },
          readOnly: true,
          style: {
            left: 0,
            pointerEvents: 'none',
            position: 'absolute',
            top: 0
          },
          tabIndex: -1,
          value: hintText
        }));
      }
    }]);

    return HintedInput;
  }(_react["default"].Component);

  HintedInput.displayName = "HintContainer(".concat((0, _utils.getDisplayName)(Input), ")");
  return (0, _TypeaheadContext.withContext)(HintedInput, ['hintText', 'initialItem', 'onAdd', 'selectHintOnEnter']);
}

var _default = hintContainer;
exports["default"] = _default;

/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function inputContainer(Input) {
  var WrappedInput =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedInput, _React$Component);

    function WrappedInput() {
      _classCallCheck(this, WrappedInput);

      return _possibleConstructorReturn(this, _getPrototypeOf(WrappedInput).apply(this, arguments));
    }

    _createClass(WrappedInput, [{
      key: "render",
      value: function render() {
        var _cx;

        var _this$props = this.props,
            activeIndex = _this$props.activeIndex,
            bsSize = _this$props.bsSize,
            disabled = _this$props.disabled,
            inputRef = _this$props.inputRef,
            isFocused = _this$props.isFocused,
            isInvalid = _this$props.isInvalid,
            isMenuShown = _this$props.isMenuShown,
            isValid = _this$props.isValid,
            labelKey = _this$props.labelKey,
            menuId = _this$props.menuId,
            multiple = _this$props.multiple,
            onBlur = _this$props.onBlur,
            onChange = _this$props.onChange,
            onFocus = _this$props.onFocus,
            onKeyDown = _this$props.onKeyDown,
            onRemove = _this$props.onRemove,
            placeholder = _this$props.placeholder,
            renderToken = _this$props.renderToken,
            selected = _this$props.selected;
        var _this$props$inputProp = this.props.inputProps,
            autoComplete = _this$props$inputProp.autoComplete,
            type = _this$props$inputProp.type; // Add a11y-related props.

        var inputProps = _objectSpread({}, this.props.inputProps, {
          'aria-activedescendant': activeIndex >= 0 ? (0, _utils.getMenuItemId)(activeIndex) : undefined,
          'aria-autocomplete': multiple ? 'list' : 'both',
          'aria-expanded': isMenuShown,
          'aria-haspopup': 'listbox',
          'aria-owns': isMenuShown ? menuId : undefined,
          autoComplete: autoComplete || 'nope',
          disabled: disabled,
          inputRef: inputRef,
          onBlur: onBlur,
          onChange: onChange,
          // Re-open the menu, eg: if it's closed via ESC.
          onClick: onFocus,
          onFocus: onFocus,
          onKeyDown: onKeyDown,
          placeholder: selected.length ? null : placeholder,
          // Comboboxes are single-select by definition:
          // https://www.w3.org/TR/wai-aria-practices-1.1/#combobox
          role: 'combobox',
          type: type || 'text',
          value: (0, _utils.getInputText)(this.props)
        });

        var className = inputProps.className || '';

        if (multiple) {
          inputProps = _objectSpread({}, inputProps, {
            'aria-expanded': undefined,
            inputClassName: className,
            labelKey: labelKey,
            onRemove: onRemove,
            renderToken: renderToken,
            role: undefined,
            selected: selected
          });
        }

        return _react["default"].createElement(Input, _extends({}, inputProps, {
          className: (0, _classnames["default"])('rbt-input', (_cx = {}, _defineProperty(_cx, className, !multiple), _defineProperty(_cx, "focus", isFocused), _defineProperty(_cx, 'input-lg form-control-lg', bsSize === 'large' || bsSize === 'lg'), _defineProperty(_cx, 'input-sm form-control-sm', bsSize === 'small' || bsSize === 'sm'), _defineProperty(_cx, 'is-invalid', isInvalid), _defineProperty(_cx, 'is-valid', isValid), _cx))
        }));
      }
    }]);

    return WrappedInput;
  }(_react["default"].Component);

  WrappedInput.displayName = "InputContainer(".concat((0, _utils.getDisplayName)(Input), ")");
  return WrappedInput;
}

var _default = inputContainer;
exports["default"] = _default;

/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _Highlighter = _interopRequireDefault(__webpack_require__(329));

var _Menu = _interopRequireDefault(__webpack_require__(330));

var _MenuItem = _interopRequireDefault(__webpack_require__(280));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TypeaheadMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadMenu, _React$Component);

  function TypeaheadMenu() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TypeaheadMenu);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TypeaheadMenu)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderMenuItem", function (option, idx) {
      var _this$props = _this.props,
          labelKey = _this$props.labelKey,
          newSelectionPrefix = _this$props.newSelectionPrefix,
          renderMenuItemChildren = _this$props.renderMenuItemChildren,
          text = _this$props.text;
      var label = (0, _utils.getOptionLabel)(option, labelKey);
      var menuItemProps = {
        disabled: option.disabled,
        key: idx,
        label: label,
        option: option,
        position: idx
      };

      if (option.customOption) {
        return _react["default"].createElement(_MenuItem["default"], _extends({}, menuItemProps, {
          className: "rbt-menu-custom-option",
          label: newSelectionPrefix + label
        }), newSelectionPrefix, _react["default"].createElement(_Highlighter["default"], {
          search: text
        }, label));
      }

      if (option.paginationOption) {
        return [_react["default"].createElement(_Menu["default"].Divider, {
          key: "pagination-item-divider"
        }), _react["default"].createElement(_MenuItem["default"], _extends({}, menuItemProps, {
          className: "rbt-menu-pagination-option",
          key: "pagination-item"
        }), label)];
      }

      return _react["default"].createElement(_MenuItem["default"], menuItemProps, renderMenuItemChildren(option, _this.props, idx));
    });

    return _this;
  }

  _createClass(TypeaheadMenu, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          labelKey = _this$props2.labelKey,
          newSelectionPrefix = _this$props2.newSelectionPrefix,
          options = _this$props2.options,
          renderMenuItemChildren = _this$props2.renderMenuItemChildren,
          menuProps = _objectWithoutProperties(_this$props2, ["labelKey", "newSelectionPrefix", "options", "renderMenuItemChildren"]);

      return _react["default"].createElement(_Menu["default"], menuProps, options.map(this._renderMenuItem));
    }
  }]);

  return TypeaheadMenu;
}(_react["default"].Component);

TypeaheadMenu.propTypes = {
  /**
   * Provides the ability to specify a prefix before the user-entered text to
   * indicate that the selection will be new. No-op unless `allowNew={true}`.
   */
  newSelectionPrefix: _propTypes["default"].string,

  /**
   * Provides a hook for customized rendering of menu item contents.
   */
  renderMenuItemChildren: _propTypes["default"].func
};
TypeaheadMenu.defaultProps = {
  newSelectionPrefix: 'New selection: ',
  renderMenuItemChildren: function renderMenuItemChildren(option, props, idx) {
    return _react["default"].createElement(_Highlighter["default"], {
      search: props.text
    }, (0, _utils.getOptionLabel)(option, props.labelKey));
  }
};
var _default = TypeaheadMenu;
exports["default"] = _default;

/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Stripped-down version of https://github.com/helior/react-highlighter
 *
 * Results are already filtered by the time the component is used internally so
 * we can safely ignore case and diacritical marks for the purposes of matching.
 */
var Highlighter =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Highlighter, _React$Component);

  function Highlighter() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Highlighter);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Highlighter)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_count", 0);

    return _this;
  }

  _createClass(Highlighter, [{
    key: "render",
    value: function render() {
      var children = this.props.search ? this._renderHighlightedChildren() : this.props.children;
      return _react["default"].createElement("span", null, children);
    }
  }, {
    key: "_renderHighlightedChildren",
    value: function _renderHighlightedChildren() {
      var children = [];
      var remaining = this.props.children;

      while (remaining) {
        var bounds = (0, _utils.getMatchBounds)(remaining, this.props.search);

        if (!bounds) {
          this._count += 1;
          children.push(_react["default"].createElement("span", {
            key: this._count
          }, remaining));
          return children;
        } // Capture the string that leads up to a match...


        var nonMatch = remaining.slice(0, bounds.start);

        if (nonMatch) {
          this._count += 1;
          children.push(_react["default"].createElement("span", {
            key: this._count
          }, nonMatch));
        } // Now, capture the matching string...


        var match = remaining.slice(bounds.start, bounds.end);

        if (match) {
          this._count += 1;
          children.push(_react["default"].createElement("mark", {
            className: "rbt-highlight-text",
            key: this._count
          }, match));
        } // And if there's anything left over, continue the loop.


        remaining = remaining.slice(bounds.end);
      }

      return children;
    }
  }]);

  return Highlighter;
}(_react["default"].Component);

Highlighter.propTypes = {
  children: _propTypes["default"].string.isRequired,
  search: _propTypes["default"].string.isRequired
};
var _default = Highlighter;
exports["default"] = _default;

/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _isRequiredForA11y = _interopRequireDefault(__webpack_require__(558));

var _react = _interopRequireWildcard(__webpack_require__(1));

var _MenuItem = __webpack_require__(280);

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 * Menu component that handles empty state when passed a set of results.
 */
var Menu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Menu, _React$Component);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, _getPrototypeOf(Menu).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this$props = this.props,
          inputHeight = _this$props.inputHeight,
          scheduleUpdate = _this$props.scheduleUpdate; // Update the menu position if the height of the input changes.

      if (inputHeight !== prevProps.inputHeight) {
        scheduleUpdate();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          children = _this$props2.children,
          className = _this$props2.className,
          emptyLabel = _this$props2.emptyLabel,
          id = _this$props2.id,
          innerRef = _this$props2.innerRef,
          maxHeight = _this$props2.maxHeight,
          style = _this$props2.style,
          text = _this$props2.text;
      var contents = _react.Children.count(children) === 0 ? _react["default"].createElement(_MenuItem.BaseMenuItem, {
        disabled: true
      }, emptyLabel) : children;
      return _react["default"].createElement("ul", {
        className: (0, _classnames["default"])('rbt-menu', 'dropdown-menu', 'show', className),
        id: id,
        key: // Force a re-render if the text changes to ensure that menu
        // positioning updates correctly.
        text,
        ref: innerRef,
        role: "listbox",
        style: _objectSpread({}, style, {
          display: 'block',
          maxHeight: maxHeight,
          overflow: 'auto'
        })
      }, contents);
    }
  }]);

  return Menu;
}(_react["default"].Component);

Menu.propTypes = {
  /**
   * Needed for accessibility.
   */
  id: (0, _isRequiredForA11y["default"])(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string])),

  /**
   * Maximum height of the dropdown menu.
   */
  maxHeight: _propTypes["default"].string
};
Menu.defaultProps = {
  maxHeight: '300px'
};

Menu.Divider = function (props) {
  return _react["default"].createElement("li", {
    className: "divider dropdown-divider",
    role: "separator"
  });
};

Menu.Header = function (props) {
  return _react["default"].createElement("li", _extends({}, props, {
    className: "dropdown-header"
  }));
};

var _default = Menu;
exports["default"] = _default;

/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _reactDom = __webpack_require__(15);

var _TypeaheadContext = __webpack_require__(279);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var menuItemContainer = function menuItemContainer(Component) {
  var WrappedMenuItem =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedMenuItem, _React$Component);

    function WrappedMenuItem() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedMenuItem);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedMenuItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleClick", function (e) {
        var _this$props = _this.props,
            onMenuItemClick = _this$props.onMenuItemClick,
            option = _this$props.option,
            onClick = _this$props.onClick;
        onMenuItemClick(option, e);
        onClick && onClick(e);
      });

      _defineProperty(_assertThisInitialized(_this), "_maybeUpdateItem", function () {
        var _this$props2 = _this.props,
            activeIndex = _this$props2.activeIndex,
            onActiveItemChange = _this$props2.onActiveItemChange,
            onInitialItemChange = _this$props2.onInitialItemChange,
            option = _this$props2.option,
            position = _this$props2.position;

        if (position === 0) {
          onInitialItemChange(option);
        }

        if (position === activeIndex) {
          // Ensures that if the menu items exceed the bounds of the menu, the
          // menu will scroll up or down as the user hits the arrow keys.

          /* eslint-disable-next-line react/no-find-dom-node */
          (0, _utils.scrollIntoViewIfNeeded)((0, _reactDom.findDOMNode)(_assertThisInitialized(_this)));
          onActiveItemChange(option);
        }
      });

      return _this;
    }

    _createClass(WrappedMenuItem, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this._maybeUpdateItem();
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        this._maybeUpdateItem();
      }
    }, {
      key: "render",
      value: function render() {
        var _this$props3 = this.props,
            activeIndex = _this$props3.activeIndex,
            isOnlyResult = _this$props3.isOnlyResult,
            label = _this$props3.label,
            onActiveItemChange = _this$props3.onActiveItemChange,
            onInitialItemChange = _this$props3.onInitialItemChange,
            onMenuItemClick = _this$props3.onMenuItemClick,
            option = _this$props3.option,
            position = _this$props3.position,
            props = _objectWithoutProperties(_this$props3, ["activeIndex", "isOnlyResult", "label", "onActiveItemChange", "onInitialItemChange", "onMenuItemClick", "option", "position"]);

        var active = isOnlyResult || activeIndex === position;
        return _react["default"].createElement(Component, _extends({}, props, {
          active: active,
          "aria-label": label,
          "aria-selected": active,
          id: (0, _utils.getMenuItemId)(position),
          onClick: this._handleClick,
          onMouseDown: _utils.preventInputBlur,
          role: "option"
        }));
      }
    }]);

    return WrappedMenuItem;
  }(_react["default"].Component);

  WrappedMenuItem.displayName = "MenuItemContainer(".concat((0, _utils.getDisplayName)(Component), ")");
  WrappedMenuItem.propTypes = {
    option: _propTypes["default"].oneOfType([_propTypes["default"].object, _propTypes["default"].string]).isRequired,
    position: _propTypes["default"].number
  };
  return (0, _TypeaheadContext.withContext)(WrappedMenuItem, ['activeIndex', 'isOnlyResult', 'onActiveItemChange', 'onInitialItemChange', 'onMenuItemClick']);
};

var _default = menuItemContainer;
exports["default"] = _default;

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/compute-scroll-into-view/dist/index.module.js
function t(t){return null!=t&&"object"==typeof t&&1===t.nodeType}function e(t,e){return(!e||"hidden"!==t)&&"visible"!==t&&"clip"!==t}function n(t,n){if(t.clientHeight<t.scrollHeight||t.clientWidth<t.scrollWidth){var r=getComputedStyle(t,null);return e(r.overflowY,n)||e(r.overflowX,n)||function(t){var e=function(t){if(!t.ownerDocument||!t.ownerDocument.defaultView)return null;try{return t.ownerDocument.defaultView.frameElement}catch(t){return null}}(t);return!!e&&(e.clientHeight<t.scrollHeight||e.clientWidth<t.scrollWidth)}(t)}return!1}function r(t,e,n,r,i,o,l,d){return o<t&&l>e||o>t&&l<e?0:o<=t&&d<=n||l>=e&&d>=n?o-t-r:l>e&&d<n||o<t&&d>n?l-e+i:0}/* harmony default export */ var index_module = (function(e,i){var o=window,l=i.scrollMode,d=i.block,u=i.inline,h=i.boundary,a=i.skipOverflowHiddenElements,c="function"==typeof h?h:function(t){return t!==h};if(!t(e))throw new TypeError("Invalid target");for(var f=document.scrollingElement||document.documentElement,s=[],p=e;t(p)&&c(p);){if((p=p.parentNode)===f){s.push(p);break}p===document.body&&n(p)&&!n(document.documentElement)||n(p,a)&&s.push(p)}for(var g=o.visualViewport?o.visualViewport.width:innerWidth,m=o.visualViewport?o.visualViewport.height:innerHeight,w=window.scrollX||pageXOffset,v=window.scrollY||pageYOffset,W=e.getBoundingClientRect(),b=W.height,H=W.width,y=W.top,M=W.right,E=W.bottom,V=W.left,x="start"===d||"nearest"===d?y:"end"===d?E:y+b/2,I="center"===u?V+H/2:"end"===u?M:V,C=[],T=0;T<s.length;T++){var k=s[T],B=k.getBoundingClientRect(),D=B.height,O=B.width,R=B.top,X=B.right,Y=B.bottom,L=B.left;if("if-needed"===l&&y>=0&&V>=0&&E<=m&&M<=g&&y>=R&&E<=Y&&V>=L&&M<=X)return C;var S=getComputedStyle(k),j=parseInt(S.borderLeftWidth,10),N=parseInt(S.borderTopWidth,10),q=parseInt(S.borderRightWidth,10),z=parseInt(S.borderBottomWidth,10),A=0,F=0,G="offsetWidth"in k?k.offsetWidth-k.clientWidth-j-q:0,J="offsetHeight"in k?k.offsetHeight-k.clientHeight-N-z:0;if(f===k)A="start"===d?x:"end"===d?x-m:"nearest"===d?r(v,v+m,m,N,z,v+x,v+x+b,b):x-m/2,F="start"===u?I:"center"===u?I-g/2:"end"===u?I-g:r(w,w+g,g,j,q,w+I,w+I+H,H),A=Math.max(0,A+v),F=Math.max(0,F+w);else{A="start"===d?x-R-N:"end"===d?x-Y+z+J:"nearest"===d?r(R,Y,D,N,z+J,x,x+b,b):x-(R+D/2)+J/2,F="start"===u?I-L-j:"center"===u?I-(L+O/2)+G/2:"end"===u?I-X+q+G:r(L,X,O,j,q+G,I,I+H,H);var K=k.scrollLeft,P=k.scrollTop;x+=P-(A=Math.max(0,Math.min(P+A,k.scrollHeight-D+J))),I+=K-(F=Math.max(0,Math.min(K+F,k.scrollWidth-O+G)))}C.push({el:k,top:A,left:F})}return C});
//# sourceMappingURL=index.module.js.map

// CONCATENATED MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js


function isOptionsObject(options) {
  return options === Object(options) && Object.keys(options).length !== 0;
}

function defaultBehavior(actions, behavior) {
  if (behavior === void 0) {
    behavior = 'auto';
  }

  var canSmoothScroll = ('scrollBehavior' in document.body.style);
  actions.forEach(function (_ref) {
    var el = _ref.el,
        top = _ref.top,
        left = _ref.left;

    if (el.scroll && canSmoothScroll) {
      el.scroll({
        top: top,
        left: left,
        behavior: behavior
      });
    } else {
      el.scrollTop = top;
      el.scrollLeft = left;
    }
  });
}

function getOptions(options) {
  if (options === false) {
    return {
      block: 'end',
      inline: 'nearest'
    };
  }

  if (isOptionsObject(options)) {
    return options;
  }

  return {
    block: 'start',
    inline: 'nearest'
  };
}

function scrollIntoView(target, options) {
  var targetIsDetached = !target.ownerDocument.documentElement.contains(target);

  if (isOptionsObject(options) && typeof options.behavior === 'function') {
    return options.behavior(targetIsDetached ? [] : index_module(target, options));
  }

  if (targetIsDetached) {
    return;
  }

  var computeOptions = getOptions(options);
  return defaultBehavior(index_module(target, computeOptions), computeOptions.behavior);
}

/* harmony default export */ var es = __webpack_exports__["a"] = (scrollIntoView);

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var BotChat = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(BotChat, _Component);

  var _super = _createSuper(BotChat);

  function BotChat(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, BotChat);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(BotChat, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      (function (d, m) {
        /*---------------- Kommunicate settings start ----------------*/
        var defaultSettings = {
          "defaultBotIds": ["charley-erttt"],
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "defaultAssignee": "charley-erttt",
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "skipRouting": true
        };
        var kommunicateSettings = {
          "appId": "2b0d8db8368a2f22263da303b739e204a",
          // Replace <APP_ID> with your APP_ID which you can find in install section of dashboard
          "automaticChatOpenOnNavigation": false,
          "onInit": function onInit() {
            Kommunicate.updateSettings(defaultSettings);
          }
        };
        /*----------------- Kommunicate settings end ------------------*/

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://widget.kommunicate.io/v2/kommunicate.app";
        var h = document.getElementsByTagName("head")[0];
        h.appendChild(s);
        window.kommunicate = m;
        m._globals = kommunicateSettings;
      })(document, window.kommunicate || {});
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", null);
    }
  }]);

  return BotChat;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (BotChat);

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157),
    isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ 345:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(251);

/** Built-in value references. */
var getPrototype = overArg(Object.getPrototypeOf, Object);

module.exports = getPrototype;


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(253),
    equalArrays = __webpack_require__(255),
    equalByTag = __webpack_require__(378),
    equalObjects = __webpack_require__(382),
    getTag = __webpack_require__(398),
    isArray = __webpack_require__(148),
    isBuffer = __webpack_require__(258),
    isTypedArray = __webpack_require__(259);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag : getTag(object),
      othTag = othIsArr ? arrayTag : getTag(other);

  objTag = objTag == argsTag ? objectTag : objTag;
  othTag = othTag == argsTag ? objectTag : othTag;

  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

module.exports = baseIsEqualDeep;


/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

module.exports = listCacheClear;


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/** Used for built-in method references. */
var arrayProto = Array.prototype;

/** Built-in value references. */
var splice = arrayProto.splice;

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}

module.exports = listCacheDelete;


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

module.exports = listCacheGet;


/***/ }),

/***/ 351:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

module.exports = listCacheHas;


/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

module.exports = listCacheSet;


/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184);

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new ListCache;
  this.size = 0;
}

module.exports = stackClear;


/***/ }),

/***/ 354:
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);

  this.size = data.size;
  return result;
}

module.exports = stackDelete;


/***/ }),

/***/ 355:
/***/ (function(module, exports) {

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

module.exports = stackGet;


/***/ }),

/***/ 356:
/***/ (function(module, exports) {

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

module.exports = stackHas;


/***/ }),

/***/ 357:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184),
    Map = __webpack_require__(214),
    MapCache = __webpack_require__(215);

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var data = this.__data__;
  if (data instanceof ListCache) {
    var pairs = data.__data__;
    if (!Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }
    data = this.__data__ = new MapCache(pairs);
  }
  data.set(key, value);
  this.size = data.size;
  return this;
}

module.exports = stackSet;


/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(229),
    isMasked = __webpack_require__(359),
    isObject = __webpack_require__(157),
    toSource = __webpack_require__(254);

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

var coreJsData = __webpack_require__(360);

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

module.exports = isMasked;


/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

module.exports = coreJsData;


/***/ }),

/***/ 361:
/***/ (function(module, exports) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;


/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

var Hash = __webpack_require__(363),
    ListCache = __webpack_require__(184),
    Map = __webpack_require__(214);

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

module.exports = mapCacheClear;


/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

var hashClear = __webpack_require__(364),
    hashDelete = __webpack_require__(365),
    hashGet = __webpack_require__(366),
    hashHas = __webpack_require__(367),
    hashSet = __webpack_require__(368);

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

module.exports = Hash;


/***/ }),

/***/ 364:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
  this.size = 0;
}

module.exports = hashClear;


/***/ }),

/***/ 365:
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = hashDelete;


/***/ }),

/***/ 366:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

module.exports = hashGet;


/***/ }),

/***/ 367:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? (data[key] !== undefined) : hasOwnProperty.call(data, key);
}

module.exports = hashHas;


/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

module.exports = hashSet;


/***/ }),

/***/ 369:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  var result = getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = mapCacheDelete;


/***/ }),

/***/ 370:
/***/ (function(module, exports) {

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

module.exports = isKeyable;


/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

module.exports = mapCacheGet;


/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

module.exports = mapCacheHas;


/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  var data = getMapData(this, key),
      size = data.size;

  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

module.exports = mapCacheSet;


/***/ }),

/***/ 374:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(215),
    setCacheAdd = __webpack_require__(375),
    setCacheHas = __webpack_require__(376);

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

module.exports = SetCache;


/***/ }),

/***/ 375:
/***/ (function(module, exports) {

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

module.exports = setCacheAdd;


/***/ }),

/***/ 376:
/***/ (function(module, exports) {

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

module.exports = setCacheHas;


/***/ }),

/***/ 377:
/***/ (function(module, exports) {

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

module.exports = cacheHas;


/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    Uint8Array = __webpack_require__(379),
    eq = __webpack_require__(186),
    equalArrays = __webpack_require__(255),
    mapToArray = __webpack_require__(380),
    setToArray = __webpack_require__(381);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

module.exports = equalByTag;


/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Built-in value references. */
var Uint8Array = root.Uint8Array;

module.exports = Uint8Array;


/***/ }),

/***/ 380:
/***/ (function(module, exports) {

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

module.exports = mapToArray;


/***/ }),

/***/ 381:
/***/ (function(module, exports) {

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

module.exports = setToArray;


/***/ }),

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

var getAllKeys = __webpack_require__(383);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  // Check that cyclic values are equal.
  var objStacked = stack.get(object);
  var othStacked = stack.get(other);
  if (objStacked && othStacked) {
    return objStacked == other && othStacked == object;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

module.exports = equalObjects;


/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

var baseGetAllKeys = __webpack_require__(384),
    getSymbols = __webpack_require__(385),
    keys = __webpack_require__(216);

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeys(object) {
  return baseGetAllKeys(object, keys, getSymbols);
}

module.exports = getAllKeys;


/***/ }),

/***/ 384:
/***/ (function(module, exports, __webpack_require__) {

var arrayPush = __webpack_require__(257),
    isArray = __webpack_require__(148);

/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */
function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
}

module.exports = baseGetAllKeys;


/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

var arrayFilter = __webpack_require__(386),
    stubArray = __webpack_require__(387);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
  if (object == null) {
    return [];
  }
  object = Object(object);
  return arrayFilter(nativeGetSymbols(object), function(symbol) {
    return propertyIsEnumerable.call(object, symbol);
  });
};

module.exports = getSymbols;


/***/ }),

/***/ 386:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];
    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }
  return result;
}

module.exports = arrayFilter;


/***/ }),

/***/ 387:
/***/ (function(module, exports) {

/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

module.exports = stubArray;


/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

var baseTimes = __webpack_require__(389),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148),
    isBuffer = __webpack_require__(258),
    isIndex = __webpack_require__(189),
    isTypedArray = __webpack_require__(259);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;


/***/ }),

/***/ 389:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;


/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;


/***/ }),

/***/ 391:
/***/ (function(module, exports) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;


/***/ }),

/***/ 392:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isLength = __webpack_require__(218),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;


/***/ }),

/***/ 393:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;


/***/ }),

/***/ 394:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var freeGlobal = __webpack_require__(250);

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(230)(module)))

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(396),
    nativeKeys = __webpack_require__(397);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),

/***/ 396:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(251);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

var DataView = __webpack_require__(399),
    Map = __webpack_require__(214),
    Promise = __webpack_require__(400),
    Set = __webpack_require__(401),
    WeakMap = __webpack_require__(402),
    baseGetTag = __webpack_require__(158),
    toSource = __webpack_require__(254);

/** `Object#toString` result references. */
var mapTag = '[object Map]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    setTag = '[object Set]',
    weakMapTag = '[object WeakMap]';

var dataViewTag = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
    (Map && getTag(new Map) != mapTag) ||
    (Promise && getTag(Promise.resolve()) != promiseTag) ||
    (Set && getTag(new Set) != setTag) ||
    (WeakMap && getTag(new WeakMap) != weakMapTag)) {
  getTag = function(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag;
        case mapCtorString: return mapTag;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag;
        case weakMapCtorString: return weakMapTag;
      }
    }
    return result;
  };
}

module.exports = getTag;


/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView');

module.exports = DataView;


/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Promise = getNative(root, 'Promise');

module.exports = Promise;


/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Set = getNative(root, 'Set');

module.exports = Set;


/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var WeakMap = getNative(root, 'WeakMap');

module.exports = WeakMap;


/***/ }),

/***/ 403:
/***/ (function(module, exports, __webpack_require__) {

var baseMatches = __webpack_require__(404),
    baseMatchesProperty = __webpack_require__(407),
    identity = __webpack_require__(264),
    isArray = __webpack_require__(148),
    property = __webpack_require__(416);

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

module.exports = baseIteratee;


/***/ }),

/***/ 404:
/***/ (function(module, exports, __webpack_require__) {

var baseIsMatch = __webpack_require__(405),
    getMatchData = __webpack_require__(406),
    matchesStrictComparable = __webpack_require__(261);

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

module.exports = baseMatches;


/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(253),
    baseIsEqual = __webpack_require__(213);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;


/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

var isStrictComparable = __webpack_require__(260),
    keys = __webpack_require__(216);

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

module.exports = getMatchData;


/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(213),
    get = __webpack_require__(408),
    hasIn = __webpack_require__(263),
    isKey = __webpack_require__(221),
    isStrictComparable = __webpack_require__(260),
    matchesStrictComparable = __webpack_require__(261),
    toKey = __webpack_require__(167);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
  };
}

module.exports = baseMatchesProperty;


/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220);

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

module.exports = get;


/***/ }),

/***/ 409:
/***/ (function(module, exports, __webpack_require__) {

var memoizeCapped = __webpack_require__(410);

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoizeCapped(function(string) {
  var result = [];
  if (string.charCodeAt(0) === 46 /* . */) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

module.exports = stringToPath;


/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

var memoize = __webpack_require__(411);

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

module.exports = memoizeCapped;


/***/ }),

/***/ 411:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(215);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = MapCache;

module.exports = memoize;


/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    arrayMap = __webpack_require__(413),
    isArray = __webpack_require__(148),
    isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = baseToString;


/***/ }),

/***/ 413:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;


/***/ }),

/***/ 414:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

module.exports = baseHasIn;


/***/ }),

/***/ 415:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(190),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148),
    isIndex = __webpack_require__(189),
    isLength = __webpack_require__(218),
    toKey = __webpack_require__(167);

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

module.exports = hasPath;


/***/ }),

/***/ 416:
/***/ (function(module, exports, __webpack_require__) {

var baseProperty = __webpack_require__(417),
    basePropertyDeep = __webpack_require__(418),
    isKey = __webpack_require__(221),
    toKey = __webpack_require__(167);

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = property;


/***/ }),

/***/ 417:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;


/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220);

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

module.exports = basePropertyDeep;


/***/ }),

/***/ 419:
/***/ (function(module, exports, __webpack_require__) {

var baseEach = __webpack_require__(420);

/**
 * The base implementation of `_.some` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function baseSome(collection, predicate) {
  var result;

  baseEach(collection, function(value, index, collection) {
    result = predicate(value, index, collection);
    return !result;
  });
  return !!result;
}

module.exports = baseSome;


/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {

var baseForOwn = __webpack_require__(421),
    createBaseEach = __webpack_require__(424);

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = createBaseEach(baseForOwn);

module.exports = baseEach;


/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

var baseFor = __webpack_require__(422),
    keys = __webpack_require__(216);

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;


/***/ }),

/***/ 422:
/***/ (function(module, exports, __webpack_require__) {

var createBaseFor = __webpack_require__(423);

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;


/***/ }),

/***/ 423:
/***/ (function(module, exports) {

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;


/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

var isArrayLike = __webpack_require__(219);

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

module.exports = createBaseEach;


/***/ }),

/***/ 425:
/***/ (function(module, exports, __webpack_require__) {

var eq = __webpack_require__(186),
    isArrayLike = __webpack_require__(219),
    isIndex = __webpack_require__(189),
    isObject = __webpack_require__(157);

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

module.exports = isIterateeCall;


/***/ }),

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

var basePickBy = __webpack_require__(427),
    hasIn = __webpack_require__(263);

/**
 * The base implementation of `_.pick` without support for individual
 * property identifiers.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @returns {Object} Returns the new object.
 */
function basePick(object, paths) {
  return basePickBy(object, paths, function(value, path) {
    return hasIn(object, path);
  });
}

module.exports = basePick;


/***/ }),

/***/ 427:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220),
    baseSet = __webpack_require__(428),
    castPath = __webpack_require__(190);

/**
 * The base implementation of  `_.pickBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @param {Function} predicate The function invoked per property.
 * @returns {Object} Returns the new object.
 */
function basePickBy(object, paths, predicate) {
  var index = -1,
      length = paths.length,
      result = {};

  while (++index < length) {
    var path = paths[index],
        value = baseGet(object, path);

    if (predicate(value, path)) {
      baseSet(result, castPath(path, object), value);
    }
  }
  return result;
}

module.exports = basePickBy;


/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

var assignValue = __webpack_require__(429),
    castPath = __webpack_require__(190),
    isIndex = __webpack_require__(189),
    isObject = __webpack_require__(157),
    toKey = __webpack_require__(167);

/**
 * The base implementation of `_.set`.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @param {Function} [customizer] The function to customize path creation.
 * @returns {Object} Returns `object`.
 */
function baseSet(object, path, value, customizer) {
  if (!isObject(object)) {
    return object;
  }
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = toKey(path[index]),
        newValue = value;

    if (key === '__proto__' || key === 'constructor' || key === 'prototype') {
      return object;
    }

    if (index != lastIndex) {
      var objValue = nested[key];
      newValue = customizer ? customizer(objValue, key, nested) : undefined;
      if (newValue === undefined) {
        newValue = isObject(objValue)
          ? objValue
          : (isIndex(path[index + 1]) ? [] : {});
      }
    }
    assignValue(nested, key, newValue);
    nested = nested[key];
  }
  return object;
}

module.exports = baseSet;


/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

var baseAssignValue = __webpack_require__(430),
    eq = __webpack_require__(186);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

module.exports = assignValue;


/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

var defineProperty = __webpack_require__(266);

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && defineProperty) {
    defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

module.exports = baseAssignValue;


/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

var flatten = __webpack_require__(432),
    overRest = __webpack_require__(435),
    setToString = __webpack_require__(437);

/**
 * A specialized version of `baseRest` which flattens the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @returns {Function} Returns the new function.
 */
function flatRest(func) {
  return setToString(overRest(func, undefined, flatten), func + '');
}

module.exports = flatRest;


/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

var baseFlatten = __webpack_require__(433);

/**
 * Flattens `array` a single level deep.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to flatten.
 * @returns {Array} Returns the new flattened array.
 * @example
 *
 * _.flatten([1, [2, [3, [4]], 5]]);
 * // => [1, 2, [3, [4]], 5]
 */
function flatten(array) {
  var length = array == null ? 0 : array.length;
  return length ? baseFlatten(array, 1) : [];
}

module.exports = flatten;


/***/ }),

/***/ 433:
/***/ (function(module, exports, __webpack_require__) {

var arrayPush = __webpack_require__(257),
    isFlattenable = __webpack_require__(434);

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;


/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148);

/** Built-in value references. */
var spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

module.exports = isFlattenable;


/***/ }),

/***/ 435:
/***/ (function(module, exports, __webpack_require__) {

var apply = __webpack_require__(436);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */
function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

module.exports = overRest;


/***/ }),

/***/ 436:
/***/ (function(module, exports) {

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

module.exports = apply;


/***/ }),

/***/ 437:
/***/ (function(module, exports, __webpack_require__) {

var baseSetToString = __webpack_require__(438),
    shortOut = __webpack_require__(440);

/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var setToString = shortOut(baseSetToString);

module.exports = setToString;


/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

var constant = __webpack_require__(439),
    defineProperty = __webpack_require__(266),
    identity = __webpack_require__(264);

/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var baseSetToString = !defineProperty ? identity : function(func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};

module.exports = baseSetToString;


/***/ }),

/***/ 439:
/***/ (function(module, exports) {

/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function() {
    return value;
  };
}

module.exports = constant;


/***/ }),

/***/ 440:
/***/ (function(module, exports) {

/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeNow = Date.now;

/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */
function shortOut(func) {
  var count = 0,
      lastCalled = 0;

  return function() {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);

    lastCalled = stamp;
    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }
    return func.apply(undefined, arguments);
  };
}

module.exports = shortOut;


/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

var objectKeys = __webpack_require__(269);
var isArguments = __webpack_require__(444);
var is = __webpack_require__(448);
var isRegex = __webpack_require__(450);
var flags = __webpack_require__(451);
var isDate = __webpack_require__(453);

var getTime = Date.prototype.getTime;

function deepEqual(actual, expected, options) {
  var opts = options || {};

  // 7.1. All identical values are equivalent, as determined by ===.
  if (opts.strict ? is(actual, expected) : actual === expected) {
    return true;
  }

  // 7.3. Other pairs that do not both pass typeof value == 'object', equivalence is determined by ==.
  if (!actual || !expected || (typeof actual !== 'object' && typeof expected !== 'object')) {
    return opts.strict ? is(actual, expected) : actual == expected;
  }

  /*
   * 7.4. For all other Object pairs, including Array objects, equivalence is
   * determined by having the same number of owned properties (as verified
   * with Object.prototype.hasOwnProperty.call), the same set of keys
   * (although not necessarily the same order), equivalent values for every
   * corresponding key, and an identical 'prototype' property. Note: this
   * accounts for both named and indexed properties on Arrays.
   */
  // eslint-disable-next-line no-use-before-define
  return objEquiv(actual, expected, opts);
}

function isUndefinedOrNull(value) {
  return value === null || value === undefined;
}

function isBuffer(x) {
  if (!x || typeof x !== 'object' || typeof x.length !== 'number') {
    return false;
  }
  if (typeof x.copy !== 'function' || typeof x.slice !== 'function') {
    return false;
  }
  if (x.length > 0 && typeof x[0] !== 'number') {
    return false;
  }
  return true;
}

function objEquiv(a, b, opts) {
  /* eslint max-statements: [2, 50] */
  var i, key;
  if (typeof a !== typeof b) { return false; }
  if (isUndefinedOrNull(a) || isUndefinedOrNull(b)) { return false; }

  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) { return false; }

  if (isArguments(a) !== isArguments(b)) { return false; }

  var aIsRegex = isRegex(a);
  var bIsRegex = isRegex(b);
  if (aIsRegex !== bIsRegex) { return false; }
  if (aIsRegex || bIsRegex) {
    return a.source === b.source && flags(a) === flags(b);
  }

  if (isDate(a) && isDate(b)) {
    return getTime.call(a) === getTime.call(b);
  }

  var aIsBuffer = isBuffer(a);
  var bIsBuffer = isBuffer(b);
  if (aIsBuffer !== bIsBuffer) { return false; }
  if (aIsBuffer || bIsBuffer) { // && would work too, because both are true or both false here
    if (a.length !== b.length) { return false; }
    for (i = 0; i < a.length; i++) {
      if (a[i] !== b[i]) { return false; }
    }
    return true;
  }

  if (typeof a !== typeof b) { return false; }

  try {
    var ka = objectKeys(a);
    var kb = objectKeys(b);
  } catch (e) { // happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates hasOwnProperty)
  if (ka.length !== kb.length) { return false; }

  // the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  // ~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i]) { return false; }
  }
  // equivalent values for every corresponding key, and ~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!deepEqual(a[key], b[key], opts)) { return false; }
  }

  return true;
}

module.exports = deepEqual;


/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keysShim;
if (!Object.keys) {
	// modified from https://github.com/es-shims/es5-shim
	var has = Object.prototype.hasOwnProperty;
	var toStr = Object.prototype.toString;
	var isArgs = __webpack_require__(270); // eslint-disable-line global-require
	var isEnumerable = Object.prototype.propertyIsEnumerable;
	var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
	var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
	var dontEnums = [
		'toString',
		'toLocaleString',
		'valueOf',
		'hasOwnProperty',
		'isPrototypeOf',
		'propertyIsEnumerable',
		'constructor'
	];
	var equalsConstructorPrototype = function (o) {
		var ctor = o.constructor;
		return ctor && ctor.prototype === o;
	};
	var excludedKeys = {
		$applicationCache: true,
		$console: true,
		$external: true,
		$frame: true,
		$frameElement: true,
		$frames: true,
		$innerHeight: true,
		$innerWidth: true,
		$onmozfullscreenchange: true,
		$onmozfullscreenerror: true,
		$outerHeight: true,
		$outerWidth: true,
		$pageXOffset: true,
		$pageYOffset: true,
		$parent: true,
		$scrollLeft: true,
		$scrollTop: true,
		$scrollX: true,
		$scrollY: true,
		$self: true,
		$webkitIndexedDB: true,
		$webkitStorageInfo: true,
		$window: true
	};
	var hasAutomationEqualityBug = (function () {
		/* global window */
		if (typeof window === 'undefined') { return false; }
		for (var k in window) {
			try {
				if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
					try {
						equalsConstructorPrototype(window[k]);
					} catch (e) {
						return true;
					}
				}
			} catch (e) {
				return true;
			}
		}
		return false;
	}());
	var equalsConstructorPrototypeIfNotBuggy = function (o) {
		/* global window */
		if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
			return equalsConstructorPrototype(o);
		}
		try {
			return equalsConstructorPrototype(o);
		} catch (e) {
			return false;
		}
	};

	keysShim = function keys(object) {
		var isObject = object !== null && typeof object === 'object';
		var isFunction = toStr.call(object) === '[object Function]';
		var isArguments = isArgs(object);
		var isString = isObject && toStr.call(object) === '[object String]';
		var theKeys = [];

		if (!isObject && !isFunction && !isArguments) {
			throw new TypeError('Object.keys called on a non-object');
		}

		var skipProto = hasProtoEnumBug && isFunction;
		if (isString && object.length > 0 && !has.call(object, 0)) {
			for (var i = 0; i < object.length; ++i) {
				theKeys.push(String(i));
			}
		}

		if (isArguments && object.length > 0) {
			for (var j = 0; j < object.length; ++j) {
				theKeys.push(String(j));
			}
		} else {
			for (var name in object) {
				if (!(skipProto && name === 'prototype') && has.call(object, name)) {
					theKeys.push(String(name));
				}
			}
		}

		if (hasDontEnumBug) {
			var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

			for (var k = 0; k < dontEnums.length; ++k) {
				if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
					theKeys.push(dontEnums[k]);
				}
			}
		}
		return theKeys;
	};
}
module.exports = keysShim;


/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';
var callBound = __webpack_require__(271);

var $toString = callBound('Object.prototype.toString');

var isStandardArguments = function isArguments(value) {
	if (hasToStringTag && value && typeof value === 'object' && Symbol.toStringTag in value) {
		return false;
	}
	return $toString(value) === '[object Arguments]';
};

var isLegacyArguments = function isArguments(value) {
	if (isStandardArguments(value)) {
		return true;
	}
	return value !== null &&
		typeof value === 'object' &&
		typeof value.length === 'number' &&
		value.length >= 0 &&
		$toString(value) !== '[object Array]' &&
		$toString(value.callee) === '[object Function]';
};

var supportsStandardArguments = (function () {
	return isStandardArguments(arguments);
}());

isStandardArguments.isLegacyArguments = isLegacyArguments; // for tests

module.exports = supportsStandardArguments ? isStandardArguments : isLegacyArguments;


/***/ }),

/***/ 445:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint complexity: [2, 18], max-statements: [2, 33] */
module.exports = function hasSymbols() {
	if (typeof Symbol !== 'function' || typeof Object.getOwnPropertySymbols !== 'function') { return false; }
	if (typeof Symbol.iterator === 'symbol') { return true; }

	var obj = {};
	var sym = Symbol('test');
	var symObj = Object(sym);
	if (typeof sym === 'string') { return false; }

	if (Object.prototype.toString.call(sym) !== '[object Symbol]') { return false; }
	if (Object.prototype.toString.call(symObj) !== '[object Symbol]') { return false; }

	// temp disabled per https://github.com/ljharb/object.assign/issues/17
	// if (sym instanceof Symbol) { return false; }
	// temp disabled per https://github.com/WebReflection/get-own-property-symbols/issues/4
	// if (!(symObj instanceof Symbol)) { return false; }

	// if (typeof Symbol.prototype.toString !== 'function') { return false; }
	// if (String(sym) !== Symbol.prototype.toString.call(sym)) { return false; }

	var symVal = 42;
	obj[sym] = symVal;
	for (sym in obj) { return false; } // eslint-disable-line no-restricted-syntax
	if (typeof Object.keys === 'function' && Object.keys(obj).length !== 0) { return false; }

	if (typeof Object.getOwnPropertyNames === 'function' && Object.getOwnPropertyNames(obj).length !== 0) { return false; }

	var syms = Object.getOwnPropertySymbols(obj);
	if (syms.length !== 1 || syms[0] !== sym) { return false; }

	if (!Object.prototype.propertyIsEnumerable.call(obj, sym)) { return false; }

	if (typeof Object.getOwnPropertyDescriptor === 'function') {
		var descriptor = Object.getOwnPropertyDescriptor(obj, sym);
		if (descriptor.value !== symVal || descriptor.enumerable !== true) { return false; }
	}

	return true;
};


/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice = Array.prototype.slice;
var toStr = Object.prototype.toString;
var funcType = '[object Function]';

module.exports = function bind(that) {
    var target = this;
    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
        throw new TypeError(ERROR_MESSAGE + target);
    }
    var args = slice.call(arguments, 1);

    var bound;
    var binder = function () {
        if (this instanceof bound) {
            var result = target.apply(
                this,
                args.concat(slice.call(arguments))
            );
            if (Object(result) === result) {
                return result;
            }
            return this;
        } else {
            return target.apply(
                that,
                args.concat(slice.call(arguments))
            );
        }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];
    for (var i = 0; i < boundLength; i++) {
        boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
        var Empty = function Empty() {};
        Empty.prototype = target.prototype;
        bound.prototype = new Empty();
        Empty.prototype = null;
    }

    return bound;
};


/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(223);

module.exports = bind.call(Function.call, Object.prototype.hasOwnProperty);


/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(168);
var callBind = __webpack_require__(224);

var implementation = __webpack_require__(274);
var getPolyfill = __webpack_require__(275);
var shim = __webpack_require__(449);

var polyfill = callBind(getPolyfill(), Object);

define(polyfill, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = polyfill;


/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getPolyfill = __webpack_require__(275);
var define = __webpack_require__(168);

module.exports = function shimObjectIs() {
	var polyfill = getPolyfill();
	define(Object, { is: polyfill }, {
		is: function testObjectIs() {
			return Object.is !== polyfill;
		}
	});
	return polyfill;
};


/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var callBound = __webpack_require__(271);
var hasSymbols = __webpack_require__(273)();
var hasToStringTag = hasSymbols && typeof Symbol.toStringTag === 'symbol';
var has;
var $exec;
var isRegexMarker;
var badStringifier;

if (hasToStringTag) {
	has = callBound('Object.prototype.hasOwnProperty');
	$exec = callBound('RegExp.prototype.exec');
	isRegexMarker = {};

	var throwRegexMarker = function () {
		throw isRegexMarker;
	};
	badStringifier = {
		toString: throwRegexMarker,
		valueOf: throwRegexMarker
	};

	if (typeof Symbol.toPrimitive === 'symbol') {
		badStringifier[Symbol.toPrimitive] = throwRegexMarker;
	}
}

var $toString = callBound('Object.prototype.toString');
var gOPD = Object.getOwnPropertyDescriptor;
var regexClass = '[object RegExp]';

module.exports = hasToStringTag
	// eslint-disable-next-line consistent-return
	? function isRegex(value) {
		if (!value || typeof value !== 'object') {
			return false;
		}

		var descriptor = gOPD(value, 'lastIndex');
		var hasLastIndexDataProperty = descriptor && has(descriptor, 'value');
		if (!hasLastIndexDataProperty) {
			return false;
		}

		try {
			$exec(value, badStringifier);
		} catch (e) {
			return e === isRegexMarker;
		}
	}
	: function isRegex(value) {
		// In older browsers, typeof regex incorrectly returns 'function'
		if (!value || (typeof value !== 'object' && typeof value !== 'function')) {
			return false;
		}

		return $toString(value) === regexClass;
	};


/***/ }),

/***/ 451:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(168);
var callBind = __webpack_require__(224);

var implementation = __webpack_require__(276);
var getPolyfill = __webpack_require__(277);
var shim = __webpack_require__(452);

var flagsBound = callBind(implementation);

define(flagsBound, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = flagsBound;


/***/ }),

/***/ 452:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var supportsDescriptors = __webpack_require__(168).supportsDescriptors;
var getPolyfill = __webpack_require__(277);
var gOPD = Object.getOwnPropertyDescriptor;
var defineProperty = Object.defineProperty;
var TypeErr = TypeError;
var getProto = Object.getPrototypeOf;
var regex = /a/;

module.exports = function shimFlags() {
	if (!supportsDescriptors || !getProto) {
		throw new TypeErr('RegExp.prototype.flags requires a true ES5 environment that supports property descriptors');
	}
	var polyfill = getPolyfill();
	var proto = getProto(regex);
	var descriptor = gOPD(proto, 'flags');
	if (!descriptor || descriptor.get !== polyfill) {
		defineProperty(proto, 'flags', {
			configurable: true,
			enumerable: false,
			get: polyfill
		});
	}
	return polyfill;
};


/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getDay = Date.prototype.getDay;
var tryDateObject = function tryDateGetDayCall(value) {
	try {
		getDay.call(value);
		return true;
	} catch (e) {
		return false;
	}
};

var toStr = Object.prototype.toString;
var dateClass = '[object Date]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isDateObject(value) {
	if (typeof value !== 'object' || value === null) {
		return false;
	}
	return hasToStringTag ? tryDateObject(value) : toStr.call(value) === dateClass;
};


/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157),
    now = __webpack_require__(342),
    toNumber = __webpack_require__(343);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        timeWaiting = wait - timeSinceLastCall;

    return maxing
      ? nativeMin(timeWaiting, maxWait - timeSinceLastInvoke)
      : timeWaiting;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        clearTimeout(timerId);
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;


/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isArray = __webpack_require__(148),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var stringTag = '[object String]';

/**
 * Checks if `value` is classified as a `String` primitive or object.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
 * @example
 *
 * _.isString('abc');
 * // => true
 *
 * _.isString(1);
 * // => false
 */
function isString(value) {
  return typeof value == 'string' ||
    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag);
}

module.exports = isString;


/***/ }),

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

var arraySome = __webpack_require__(256),
    baseIteratee = __webpack_require__(403),
    baseSome = __webpack_require__(419),
    isArray = __webpack_require__(148),
    isIterateeCall = __webpack_require__(425);

/**
 * Checks if `predicate` returns truthy for **any** element of `collection`.
 * Iteration is stopped once `predicate` returns truthy. The predicate is
 * invoked with three arguments: (value, index|key, collection).
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 * @example
 *
 * _.some([null, 0, 'yes', false], Boolean);
 * // => true
 *
 * var users = [
 *   { 'user': 'barney', 'active': true },
 *   { 'user': 'fred',   'active': false }
 * ];
 *
 * // The `_.matches` iteratee shorthand.
 * _.some(users, { 'user': 'barney', 'active': false });
 * // => false
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.some(users, ['active', false]);
 * // => true
 *
 * // The `_.property` iteratee shorthand.
 * _.some(users, 'active');
 * // => true
 */
function some(collection, predicate, guard) {
  var func = isArray(collection) ? arraySome : baseSome;
  if (guard && isIterateeCall(collection, predicate, guard)) {
    predicate = undefined;
  }
  return func(collection, baseIteratee(predicate, 3));
}

module.exports = some;


/***/ }),

/***/ 478:
/***/ (function(module, exports, __webpack_require__) {

var toString = __webpack_require__(262);

/** Used to generate unique IDs. */
var idCounter = 0;

/**
 * Generates a unique ID. If `prefix` is given, the ID is appended to it.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {string} [prefix=''] The value to prefix the ID with.
 * @returns {string} Returns the unique ID.
 * @example
 *
 * _.uniqueId('contact_');
 * // => 'contact_104'
 *
 * _.uniqueId();
 * // => '105'
 */
function uniqueId(prefix) {
  var id = ++idCounter;
  return toString(prefix) + id;
}

module.exports = uniqueId;


/***/ }),

/***/ 511:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AsyncTypeahead", {
  enumerable: true,
  get: function get() {
    return _AsyncTypeahead2["default"];
  }
});
Object.defineProperty(exports, "Highlighter", {
  enumerable: true,
  get: function get() {
    return _Highlighter2["default"];
  }
});
Object.defineProperty(exports, "Menu", {
  enumerable: true,
  get: function get() {
    return _Menu2["default"];
  }
});
Object.defineProperty(exports, "MenuItem", {
  enumerable: true,
  get: function get() {
    return _MenuItem2["default"];
  }
});
Object.defineProperty(exports, "Token", {
  enumerable: true,
  get: function get() {
    return _Token2["default"];
  }
});
Object.defineProperty(exports, "Typeahead", {
  enumerable: true,
  get: function get() {
    return _Typeahead2["default"];
  }
});
Object.defineProperty(exports, "TypeaheadMenu", {
  enumerable: true,
  get: function get() {
    return _TypeaheadMenu2["default"];
  }
});
Object.defineProperty(exports, "asyncContainer", {
  enumerable: true,
  get: function get() {
    return _asyncContainer2["default"];
  }
});
Object.defineProperty(exports, "menuItemContainer", {
  enumerable: true,
  get: function get() {
    return _menuItemContainer2["default"];
  }
});
Object.defineProperty(exports, "tokenContainer", {
  enumerable: true,
  get: function get() {
    return _tokenContainer2["default"];
  }
});

var _AsyncTypeahead2 = _interopRequireDefault(__webpack_require__(512));

var _Highlighter2 = _interopRequireDefault(__webpack_require__(329));

var _Menu2 = _interopRequireDefault(__webpack_require__(330));

var _MenuItem2 = _interopRequireDefault(__webpack_require__(280));

var _Token2 = _interopRequireDefault(__webpack_require__(323));

var _Typeahead2 = _interopRequireDefault(__webpack_require__(318));

var _TypeaheadMenu2 = _interopRequireDefault(__webpack_require__(328));

var _asyncContainer2 = _interopRequireDefault(__webpack_require__(314));

var _menuItemContainer2 = _interopRequireDefault(__webpack_require__(331));

var _tokenContainer2 = _interopRequireDefault(__webpack_require__(324));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 512:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _asyncContainer = _interopRequireDefault(__webpack_require__(314));

var _Typeahead = _interopRequireDefault(__webpack_require__(318));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = (0, _asyncContainer["default"])(_Typeahead["default"]);

exports["default"] = _default;

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = caseSensitiveType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function caseSensitiveType(props, propName, componentName) {
  var caseSensitive = props.caseSensitive,
      filterBy = props.filterBy;
  (0, _warn["default"])(!caseSensitive || typeof filterBy !== 'function', 'Your `filterBy` function will override the `caseSensitive` prop.');
}

/***/ }),

/***/ 514:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = checkPropType;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Allows additional warnings or messaging related to prop validation.
 */
function checkPropType(validator, callback) {
  return function (props, propName, componentName) {
    _propTypes["default"].checkPropTypes(_defineProperty({}, propName, validator), props, 'prop', componentName);

    typeof callback === 'function' && callback(props, propName, componentName);
  };
}

/***/ }),

/***/ 515:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = defaultInputValueType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function defaultInputValueType(props, propName, componentName) {
  var defaultInputValue = props.defaultInputValue,
      defaultSelected = props.defaultSelected,
      multiple = props.multiple,
      selected = props.selected;
  var name = defaultSelected.length ? 'defaultSelected' : 'selected';
  (0, _warn["default"])(!(!multiple && defaultInputValue && (defaultSelected.length || selected && selected.length)), "`defaultInputValue` will be overridden by the value from `".concat(name, "`."));
}

/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = emptyLabelType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function emptyLabelType(props, propName, componentName) {
  var emptyLabel = props.emptyLabel;
  (0, _warn["default"])(!!emptyLabel, 'Passing a falsy `emptyLabel` value to hide the menu when the result set ' + 'is empty is deprecated. Use `renderMenu` to return `null` instead.');
}

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = highlightOnlyResultType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function highlightOnlyResultType(props, propName, componentName) {
  var allowNew = props.allowNew,
      highlightOnlyResult = props.highlightOnlyResult;
  (0, _warn["default"])(!(highlightOnlyResult && allowNew), '`highlightOnlyResult` will not work with `allowNew`.');
}

/***/ }),

/***/ 518:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = idType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function idType(props, propName, componentName) {
  var id = props.id,
      menuId = props.menuId;
  (0, _warn["default"])(menuId == null, 'The `menuId` prop is deprecated. Use `id` instead.');
  (0, _warn["default"])(id != null, 'The `id` prop will be required in future versions to make the component ' + 'accessible for users of assistive technologies such as screen readers.');
}

/***/ }),

/***/ 519:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = ignoreDiacriticsType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ignoreDiacriticsType(props, propName, componentName) {
  var filterBy = props.filterBy,
      ignoreDiacritics = props.ignoreDiacritics;
  (0, _warn["default"])(ignoreDiacritics || typeof filterBy !== 'function', 'Your `filterBy` function will override the `ignoreDiacritics` prop.');
}

/***/ }),

/***/ 520:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = inputPropsType;

var _isPlainObject = _interopRequireDefault(__webpack_require__(299));

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var BLACKLIST = [{
  alt: 'onBlur',
  prop: 'onBlur'
}, {
  alt: 'onInputChange',
  prop: 'onChange'
}, {
  alt: 'onFocus',
  prop: 'onFocus'
}, {
  alt: 'onKeyDown',
  prop: 'onKeyDown'
}];

function inputPropsType(props, propName, componentName) {
  var inputProps = props.inputProps;

  if (!(inputProps && (0, _isPlainObject["default"])(inputProps))) {
    return;
  } // Blacklisted properties.


  BLACKLIST.forEach(function (_ref) {
    var alt = _ref.alt,
        prop = _ref.prop;
    var msg = alt ? " Use the top-level `".concat(alt, "` prop instead.") : null;
    (0, _warn["default"])(!inputProps[prop], "The `".concat(prop, "` property of `inputProps` will be ignored.").concat(msg));
  });
}

/***/ }),

/***/ 521:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = labelKeyType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function labelKeyType(props, propName, componentName) {
  var allowNew = props.allowNew,
      labelKey = props.labelKey;
  (0, _warn["default"])(!(typeof labelKey === 'function' && allowNew), '`labelKey` must be a string when `allowNew={true}`.');
}

/***/ }),

/***/ 522:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _propTypes["default"].oneOfType([_propTypes["default"].arrayOf(_propTypes["default"].object.isRequired), _propTypes["default"].arrayOf(_propTypes["default"].string.isRequired)]);

exports["default"] = _default;

/***/ }),

/***/ 523:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = selectedType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function selectedType(props, propName, componentName) {
  var onChange = props.onChange,
      selected = props.selected;
  (0, _warn["default"])(!selected || selected && typeof onChange === 'function', 'You provided a `selected` prop without an `onChange` handler. If you ' + 'want the typeahead to be uncontrolled, use `defaultSelected`. ' + 'Otherwise, set `onChange`.');
}

/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function addCustomOption(results, props) {
  var allowNew = props.allowNew,
      labelKey = props.labelKey,
      text = props.text;

  if (!allowNew || !text.trim()) {
    return false;
  } // If the consumer has provided a callback, use that to determine whether or
  // not to add the custom option.


  if (typeof allowNew === 'function') {
    return allowNew(results, props);
  } // By default, don't add the custom option if there is an exact text match
  // with an existing option.


  return !results.some(function (o) {
    return (0, _getOptionLabel["default"])(o, labelKey) === text;
  });
}

var _default = addCustomOption;
exports["default"] = _default;

/***/ }),

/***/ 525:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = areEqual;

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _getStringLabelKey = _interopRequireDefault(__webpack_require__(252));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Compare whether items are the same. For custom items, compare the
 * `labelKey` values since a unique id is generated each time, causing the
 * comparison to fail.
 */
function areEqual(newItem, existingItem, labelKey) {
  var stringLabelKey = (0, _getStringLabelKey["default"])(labelKey);

  if (newItem && newItem.customOption && existingItem && existingItem.customOption) {
    return newItem[stringLabelKey] === existingItem[stringLabelKey];
  }

  return (0, _isEqual["default"])(newItem, existingItem);
}

/***/ }),

/***/ 526:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = defaultFilterBy;

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _isFunction = _interopRequireDefault(__webpack_require__(229));

var _isString = _interopRequireDefault(__webpack_require__(476));

var _some = _interopRequireDefault(__webpack_require__(477));

var _stripDiacritics = _interopRequireDefault(__webpack_require__(265));

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function isMatch(input, string, props) {
  var searchStr = input;
  var str = string;

  if (!props.caseSensitive) {
    searchStr = searchStr.toLowerCase();
    str = str.toLowerCase();
  }

  if (props.ignoreDiacritics) {
    searchStr = (0, _stripDiacritics["default"])(searchStr);
    str = (0, _stripDiacritics["default"])(str);
  }

  return str.indexOf(searchStr) !== -1;
}
/**
 * Default algorithm for filtering results.
 */


function defaultFilterBy(option, props) {
  var filterBy = props.filterBy,
      labelKey = props.labelKey,
      multiple = props.multiple,
      selected = props.selected,
      text = props.text; // Don't show selected options in the menu for the multi-select case.

  if (multiple && selected.some(function (o) {
    return (0, _isEqual["default"])(o, option);
  })) {
    return false;
  }

  var fields = filterBy.slice();

  if ((0, _isFunction["default"])(labelKey) && isMatch(text, labelKey(option), props)) {
    return true;
  }

  if ((0, _isString["default"])(labelKey)) {
    // Add the `labelKey` field to the list of fields if it isn't already there.
    if (fields.indexOf(labelKey) === -1) {
      fields.unshift(labelKey);
    }
  }

  if ((0, _isString["default"])(option)) {
    (0, _warn["default"])(fields.length <= 1, 'You cannot filter by properties when `option` is a string.');
    return isMatch(text, option, props);
  }

  return (0, _some["default"])(fields, function (field) {
    var value = option[field];

    if (!(0, _isString["default"])(value)) {
      (0, _warn["default"])(false, 'Fields passed to `filterBy` should have string values. Value will ' + 'be converted to a string; results may be unexpected.'); // Coerce to string since `toString` isn't null-safe.

      value = "".concat(value);
    }

    return isMatch(text, value, props);
  });
}

/***/ }),

/***/ 527:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function getAccessibilityStatus(props) {
  var a11yNumResults = props.a11yNumResults,
      a11yNumSelected = props.a11yNumSelected,
      emptyLabel = props.emptyLabel,
      isMenuShown = props.isMenuShown,
      results = props.results,
      selected = props.selected; // If the menu is hidden, display info about the number of selections.

  if (!isMenuShown) {
    return a11yNumSelected(selected);
  } // Display info about the number of matches.


  if (results.length === 0) {
    return emptyLabel;
  }

  return a11yNumResults(results);
}

var _default = getAccessibilityStatus;
exports["default"] = _default;

/***/ }),

/***/ 528:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getDisplayName;

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

/***/ }),

/***/ 529:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getMatchBounds = _interopRequireDefault(__webpack_require__(316));

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getHintText(_ref) {
  var activeItem = _ref.activeItem,
      initialItem = _ref.initialItem,
      isFocused = _ref.isFocused,
      isMenuShown = _ref.isMenuShown,
      labelKey = _ref.labelKey,
      multiple = _ref.multiple,
      selected = _ref.selected,
      text = _ref.text;

  // Don't display a hint under the following conditions:
  if ( // No text entered.
  !text || // The input is not focused.
  !isFocused || // The menu is hidden.
  !isMenuShown || // No item in the menu.
  !initialItem || // The initial item is a custom option.
  initialItem.customOption || // One of the menu items is active.
  activeItem || // There's already a selection in single-select mode.
  !!selected.length && !multiple) {
    return '';
  }

  var initialItemStr = (0, _getOptionLabel["default"])(initialItem, labelKey);
  var bounds = (0, _getMatchBounds["default"])(initialItemStr.toLowerCase(), text.toLowerCase());

  if (!(bounds && bounds.start === 0)) {
    return '';
  } // Text matching is case- and accent-insensitive, so to display the hint
  // correctly, splice the input string with the hint string.


  return text + initialItemStr.slice(bounds.end, initialItemStr.length);
}

var _default = getHintText;
exports["default"] = _default;

/***/ }),

/***/ 530:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;

module.exports = function (str) {
	if (typeof str !== 'string') {
		throw new TypeError('Expected a string');
	}

	return str.replace(matchOperatorsRe, '\\$&');
};


/***/ }),

/***/ 531:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getInputText(_ref) {
  var activeItem = _ref.activeItem,
      labelKey = _ref.labelKey,
      multiple = _ref.multiple,
      selected = _ref.selected,
      text = _ref.text;

  if (activeItem) {
    // Display the input value if the pagination item is active.
    return activeItem.paginationOption ? text : (0, _getOptionLabel["default"])(activeItem, labelKey);
  }

  var selectedItem = !multiple && !!selected.length && (0, _head["default"])(selected);

  if (selectedItem) {
    return (0, _getOptionLabel["default"])(selectedItem, labelKey);
  }

  return text;
}

var _default = getInputText;
exports["default"] = _default;

/***/ }),

/***/ 532:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getIsOnlyResult(_ref) {
  var allowNew = _ref.allowNew,
      highlightOnlyResult = _ref.highlightOnlyResult,
      results = _ref.results;

  if (!highlightOnlyResult || allowNew) {
    return false;
  }

  return results.length === 1 && !(0, _head["default"])(results).disabled;
}

var _default = getIsOnlyResult;
exports["default"] = _default;

/***/ }),

/***/ 533:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getMenuItemId;

function getMenuItemId(position) {
  return "rbt-menu-item-".concat(position);
}

/***/ }),

/***/ 534:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Truncates the result set based on `maxResults` and returns the new set.
 */
function getTruncatedOptions(options, maxResults) {
  if (!maxResults || maxResults >= options.length) {
    return options;
  }

  return options.slice(0, maxResults);
}

var _default = getTruncatedOptions;
exports["default"] = _default;

/***/ }),

/***/ 535:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = isShown;

function isShown(results, props) {
  var emptyLabel = props.emptyLabel,
      open = props.open,
      minLength = props.minLength,
      showMenu = props.showMenu,
      text = props.text; // If menu visibility is controlled via props, that value takes precedence.

  if (open || open === false) {
    return open;
  }

  if (!showMenu) {
    return false;
  }

  if (text.length < minLength) {
    return false;
  }

  return !!(results.length || emptyLabel);
}

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = pluralize;

/**
 * Basic util for pluralizing words. By default, simply adds an 's' to the word.
 * Also allows for a custom plural version.
 */
function pluralize(text, count, plural) {
  var pluralText = plural || "".concat(text, "s");
  return "".concat(count, " ").concat(count === 1 ? text : pluralText);
}

/***/ }),

/***/ 537:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = preventInputBlur;

/**
 * Prevent the main input from blurring when a menu item or the clear button is
 * clicked. (#226 & #310)
 */
function preventInputBlur(e) {
  e.preventDefault();
}

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Partial polyfill for webkit `scrollIntoViewIfNeeded()` method. Addresses
 * vertical scrolling only.
 *
 * Inspired by https://gist.github.com/hsablonniere/2581101, but uses
 * `getBoundingClientRect`.
 */
function scrollIntoViewIfNeeded(node) {
  // Webkit browsers
  if (Element.prototype.scrollIntoViewIfNeeded) {
    node.scrollIntoViewIfNeeded();
    return;
  } // FF, IE, etc.


  var rect = node.getBoundingClientRect();
  var parent = node.parentNode;
  var parentRect = parent.getBoundingClientRect();
  var parentComputedStyle = window.getComputedStyle(parent, null);
  var parentBorderTopWidth = parseInt(parentComputedStyle.getPropertyValue('border-top-width'), 10);

  if (rect.top < parentRect.top || rect.bottom > parentRect.bottom) {
    parent.scrollTop = node.offsetTop - parent.offsetTop - parent.clientHeight / 2 - parentBorderTopWidth + node.clientHeight / 2;
  }
}

var _default = scrollIntoViewIfNeeded;
exports["default"] = _default;

/***/ }),

/***/ 539:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = shouldSelectHint;

var _isSelectable = _interopRequireDefault(__webpack_require__(317));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function shouldSelectHint(e, props) {
  var hintText = props.hintText,
      selectHintOnEnter = props.selectHintOnEnter,
      value = props.value;

  if (!hintText) {
    return false;
  }

  if (e.keyCode === _constants.RIGHT) {
    // For selectable input types ("text", "search"), only select the hint if
    // it's at the end of the input value. For non-selectable types ("email",
    // "number"), always select the hint.
    return (0, _isSelectable["default"])(e.target) ? e.target.selectionStart === value.length : true;
  }

  if (e.keyCode === _constants.TAB) {
    return true;
  }

  if (e.keyCode === _constants.RETURN && selectHintOnEnter) {
    return true;
  }

  return false;
}

/***/ }),

/***/ 540:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Loader = function Loader(_ref) {
  var bsSize = _ref.bsSize;
  return _react["default"].createElement("div", {
    className: (0, _classnames["default"])('rbt-loader', {
      'rbt-loader-lg': bsSize === 'large' || bsSize === 'lg',
      'rbt-loader-sm': bsSize === 'small' || bsSize === 'sm'
    })
  });
};

Loader.propTypes = {
  bsSize: _propTypes["default"].oneOf(['large', 'lg', 'small', 'sm'])
};
var _default = Loader;
exports["default"] = _default;

/***/ }),

/***/ 541:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _noop = _interopRequireDefault(__webpack_require__(222));

var _react = _interopRequireWildcard(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _componentOrElement = _interopRequireDefault(__webpack_require__(267));

var _Portal = _interopRequireDefault(__webpack_require__(543));

var _Popper = _interopRequireDefault(__webpack_require__(546));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BODY_CLASS = 'rbt-body-container';

function getModifiers(_ref) {
  var align = _ref.align,
      flip = _ref.flip;
  return {
    computeStyles: {
      enabled: true,
      fn: function fn(data) {
        // Use the following condition instead of `align === 'justify'` since
        // it allows the component to fall back to justifying the menu width
        // even when `align` is undefined.
        if (align !== 'right' && align !== 'left') {
          // Set the popper width to match the target width.

          /* eslint-disable-next-line no-param-reassign */
          data.styles.width = data.offsets.reference.width;
        }

        return data;
      }
    },
    flip: {
      enabled: flip
    },
    preventOverflow: {
      escapeWithReference: true
    }
  };
}

function isBody(container) {
  return container === document.body;
}
/**
 * Custom `Overlay` component, since the version in `react-overlays` doesn't
 * work for our needs. Specifically, the `Position` component doesn't provide
 * the customized placement we need.
 */


var Overlay =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Overlay, _React$Component);

  function Overlay() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Overlay);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Overlay)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_update", function () {
      var _container$classList;

      var _this$props = _this.props,
          className = _this$props.className,
          container = _this$props.container,
          show = _this$props.show;

      if (!(show && isBody(container))) {
        return;
      } // Set a classname on the body for scoping purposes.


      container.classList.add(BODY_CLASS);
      !!className && (_container$classList = container.classList).add.apply(_container$classList, _toConsumableArray(className.split(' ')));
    });

    return _this;
  }

  _createClass(Overlay, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this._update();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this$props2 = this.props,
          onMenuHide = _this$props2.onMenuHide,
          onMenuShow = _this$props2.onMenuShow,
          onMenuToggle = _this$props2.onMenuToggle,
          show = _this$props2.show;

      if (show !== prevProps.show) {
        show ? onMenuShow() : onMenuHide();
        onMenuToggle(show);
      } // Remove scoping classes if menu isn't being appended to document body.


      var className = prevProps.className,
          container = prevProps.container;

      if (isBody(container) && !isBody(this.props.container)) {
        var _container$classList2;

        container.classList.remove(BODY_CLASS);
        !!className && (_container$classList2 = container.classList).remove.apply(_container$classList2, _toConsumableArray(className.split(' ')));
      }

      this._update();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          align = _this$props3.align,
          children = _this$props3.children,
          container = _this$props3.container,
          dropup = _this$props3.dropup,
          referenceElement = _this$props3.referenceElement,
          show = _this$props3.show;

      if (!(show && _react.Children.count(children))) {
        return null;
      }

      var child = _react.Children.only(children);

      var xPlacement = align === 'right' ? 'end' : 'start';
      var yPlacement = dropup ? 'top' : 'bottom';
      return _react["default"].createElement(_Portal["default"], {
        container: container
      }, _react["default"].createElement(_Popper["default"], {
        modifiers: getModifiers(this.props),
        placement: "".concat(yPlacement, "-").concat(xPlacement),
        referenceElement: referenceElement
      }, function (_ref2) {
        var ref = _ref2.ref,
            props = _objectWithoutProperties(_ref2, ["ref"]);

        return (0, _react.cloneElement)(child, _objectSpread({}, child.props, {}, props, {
          innerRef: ref,
          inputHeight: referenceElement ? referenceElement.offsetHeight : 0
        }));
      }));
    }
  }]);

  return Overlay;
}(_react["default"].Component);

Overlay.propTypes = {
  children: _propTypes["default"].element,
  container: _componentOrElement["default"].isRequired,
  onMenuHide: _propTypes["default"].func,
  onMenuShow: _propTypes["default"].func,
  onMenuToggle: _propTypes["default"].func,
  referenceElement: _componentOrElement["default"],
  show: _propTypes["default"].bool
};
Overlay.defaultProps = {
  onMenuHide: _noop["default"],
  onMenuShow: _noop["default"],
  onMenuToggle: _noop["default"],
  show: false
};
var _default = Overlay;
exports["default"] = _default;

/***/ }),

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createChainableTypeChecker;
/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

// Mostly taken from ReactPropTypes.

function createChainableTypeChecker(validate) {
  function checkType(isRequired, props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] == null) {
      if (isRequired) {
        return new Error('Required ' + location + ' `' + propFullNameSafe + '` was not specified ' + ('in `' + componentNameSafe + '`.'));
      }

      return null;
    }

    for (var _len = arguments.length, args = Array(_len > 6 ? _len - 6 : 0), _key = 6; _key < _len; _key++) {
      args[_key - 6] = arguments[_key];
    }

    return validate.apply(undefined, [props, propName, componentNameSafe, location, propFullNameSafe].concat(args));
  }

  var chainedCheckType = checkType.bind(null, false);
  chainedCheckType.isRequired = checkType.bind(null, true);

  return chainedCheckType;
}
module.exports = exports['default'];

/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _componentOrElement = __webpack_require__(267);

var _componentOrElement2 = _interopRequireDefault(_componentOrElement);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _getContainer = __webpack_require__(320);

var _getContainer2 = _interopRequireDefault(_getContainer);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _LegacyPortal = __webpack_require__(545);

var _LegacyPortal2 = _interopRequireDefault(_LegacyPortal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * The `<Portal/>` component renders its children into a new "subtree" outside of current component hierarchy.
 * You can think of it as a declarative `appendChild()`, or jQuery's `$.fn.appendTo()`.
 * The children of `<Portal/>` component will be appended to the `container` specified.
 */
var Portal = function (_React$Component) {
  _inherits(Portal, _React$Component);

  function Portal() {
    var _temp, _this, _ret;

    _classCallCheck(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.setContainer = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _this.props;

      _this._portalContainerNode = (0, _getContainer2.default)(props.container, (0, _ownerDocument2.default)(_this).body);
    }, _this.getMountNode = function () {
      return _this._portalContainerNode;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Portal.prototype.componentDidMount = function componentDidMount() {
    this.setContainer();
    this.forceUpdate(this.props.onRendered);
  };

  Portal.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (nextProps.container !== this.props.container) {
      this.setContainer(nextProps);
    }
  };

  Portal.prototype.componentWillUnmount = function componentWillUnmount() {
    this._portalContainerNode = null;
  };

  Portal.prototype.render = function render() {
    return this.props.children && this._portalContainerNode ? _reactDom2.default.createPortal(this.props.children, this._portalContainerNode) : null;
  };

  return Portal;
}(_react2.default.Component);

Portal.displayName = 'Portal';
Portal.propTypes = {
  /**
   * A Node, Component instance, or function that returns either. The `container` will have the Portal children
   * appended to it.
   */
  container: _propTypes2.default.oneOfType([_componentOrElement2.default, _propTypes2.default.func]),

  onRendered: _propTypes2.default.func
};
exports.default = _reactDom2.default.createPortal ? Portal : _LegacyPortal2.default;
module.exports = exports['default'];

/***/ }),

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = ownerDocument;

function ownerDocument(node) {
  return node && node.ownerDocument || document;
}

module.exports = exports["default"];

/***/ }),

/***/ 545:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _componentOrElement = __webpack_require__(267);

var _componentOrElement2 = _interopRequireDefault(_componentOrElement);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _getContainer = __webpack_require__(320);

var _getContainer2 = _interopRequireDefault(_getContainer);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * The `<Portal/>` component renders its children into a new "subtree" outside of current component hierarchy.
 * You can think of it as a declarative `appendChild()`, or jQuery's `$.fn.appendTo()`.
 * The children of `<Portal/>` component will be appended to the `container` specified.
 */
var Portal = function (_React$Component) {
  _inherits(Portal, _React$Component);

  function Portal() {
    var _temp, _this, _ret;

    _classCallCheck(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this._mountOverlayTarget = function () {
      if (!_this._overlayTarget) {
        _this._overlayTarget = document.createElement('div');
        _this._portalContainerNode = (0, _getContainer2.default)(_this.props.container, (0, _ownerDocument2.default)(_this).body);
        _this._portalContainerNode.appendChild(_this._overlayTarget);
      }
    }, _this._unmountOverlayTarget = function () {
      if (_this._overlayTarget) {
        _this._portalContainerNode.removeChild(_this._overlayTarget);
        _this._overlayTarget = null;
      }
      _this._portalContainerNode = null;
    }, _this._renderOverlay = function () {
      var overlay = !_this.props.children ? null : _react2.default.Children.only(_this.props.children);

      // Save reference for future access.
      if (overlay !== null) {
        _this._mountOverlayTarget();

        var initialRender = !_this._overlayInstance;

        _this._overlayInstance = _reactDom2.default.unstable_renderSubtreeIntoContainer(_this, overlay, _this._overlayTarget, function () {
          if (initialRender && _this.props.onRendered) {
            _this.props.onRendered();
          }
        });
      } else {
        // Unrender if the component is null for transitions to null
        _this._unrenderOverlay();
        _this._unmountOverlayTarget();
      }
    }, _this._unrenderOverlay = function () {
      if (_this._overlayTarget) {
        _reactDom2.default.unmountComponentAtNode(_this._overlayTarget);
        _this._overlayInstance = null;
      }
    }, _this.getMountNode = function () {
      return _this._overlayTarget;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Portal.prototype.componentDidMount = function componentDidMount() {
    this._isMounted = true;
    this._renderOverlay();
  };

  Portal.prototype.componentDidUpdate = function componentDidUpdate() {
    this._renderOverlay();
  };

  Portal.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (this._overlayTarget && nextProps.container !== this.props.container) {
      this._portalContainerNode.removeChild(this._overlayTarget);
      this._portalContainerNode = (0, _getContainer2.default)(nextProps.container, (0, _ownerDocument2.default)(this).body);
      this._portalContainerNode.appendChild(this._overlayTarget);
    }
  };

  Portal.prototype.componentWillUnmount = function componentWillUnmount() {
    this._isMounted = false;
    this._unrenderOverlay();
    this._unmountOverlayTarget();
  };

  Portal.prototype.render = function render() {
    return null;
  };

  return Portal;
}(_react2.default.Component);

Portal.displayName = 'Portal';
Portal.propTypes = {
  /**
   * A Node, Component instance, or function that returns either. The `container` will have the Portal children
   * appended to it.
   */
  container: _propTypes2.default.oneOfType([_componentOrElement2.default, _propTypes2.default.func]),

  onRendered: _propTypes2.default.func
};
exports.default = Portal;
module.exports = exports['default'];

/***/ }),

/***/ 546:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(141);

var _interopRequireDefault = __webpack_require__(142);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Popper;
exports.placements = exports.InnerPopper = void 0;

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(__webpack_require__(547));

var _extends2 = _interopRequireDefault(__webpack_require__(17));

var _assertThisInitialized2 = _interopRequireDefault(__webpack_require__(10));

var _inheritsLoose2 = _interopRequireDefault(__webpack_require__(441));

var _defineProperty2 = _interopRequireDefault(__webpack_require__(24));

var _deepEqual = _interopRequireDefault(__webpack_require__(442));

var React = _interopRequireWildcard(__webpack_require__(1));

var _popper = _interopRequireDefault(__webpack_require__(46));

var _Manager = __webpack_require__(548);

var _utils = __webpack_require__(551);

var initialStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  opacity: 0,
  pointerEvents: 'none'
};
var initialArrowStyle = {};

var InnerPopper =
/*#__PURE__*/
function (_React$Component) {
  (0, _inheritsLoose2.default)(InnerPopper, _React$Component);

  function InnerPopper() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "state", {
      data: undefined,
      placement: undefined
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "popperInstance", void 0);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "popperNode", null);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "arrowNode", null);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setPopperNode", function (popperNode) {
      if (!popperNode || _this.popperNode === popperNode) return;
      (0, _utils.setRef)(_this.props.innerRef, popperNode);
      _this.popperNode = popperNode;

      _this.updatePopperInstance();
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setArrowNode", function (arrowNode) {
      _this.arrowNode = arrowNode;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "updateStateModifier", {
      enabled: true,
      order: 900,
      fn: function fn(data) {
        var placement = data.placement;

        _this.setState({
          data: data,
          placement: placement
        });

        return data;
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getOptions", function () {
      return {
        placement: _this.props.placement,
        eventsEnabled: _this.props.eventsEnabled,
        positionFixed: _this.props.positionFixed,
        modifiers: (0, _extends2.default)({}, _this.props.modifiers, {
          arrow: (0, _extends2.default)({}, _this.props.modifiers && _this.props.modifiers.arrow, {
            enabled: !!_this.arrowNode,
            element: _this.arrowNode
          }),
          applyStyle: {
            enabled: false
          },
          updateStateModifier: _this.updateStateModifier
        })
      };
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getPopperStyle", function () {
      return !_this.popperNode || !_this.state.data ? initialStyle : (0, _extends2.default)({
        position: _this.state.data.offsets.popper.position
      }, _this.state.data.styles);
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getPopperPlacement", function () {
      return !_this.state.data ? undefined : _this.state.placement;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getArrowStyle", function () {
      return !_this.arrowNode || !_this.state.data ? initialArrowStyle : _this.state.data.arrowStyles;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getOutOfBoundariesState", function () {
      return _this.state.data ? _this.state.data.hide : undefined;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "destroyPopperInstance", function () {
      if (!_this.popperInstance) return;

      _this.popperInstance.destroy();

      _this.popperInstance = null;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "updatePopperInstance", function () {
      _this.destroyPopperInstance();

      var _assertThisInitialize = (0, _assertThisInitialized2.default)(_this),
          popperNode = _assertThisInitialize.popperNode;

      var referenceElement = _this.props.referenceElement;
      if (!referenceElement || !popperNode) return;
      _this.popperInstance = new _popper.default(referenceElement, popperNode, _this.getOptions());
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "scheduleUpdate", function () {
      if (_this.popperInstance) {
        _this.popperInstance.scheduleUpdate();
      }
    });
    return _this;
  }

  var _proto = InnerPopper.prototype;

  _proto.componentDidUpdate = function componentDidUpdate(prevProps, prevState) {
    // If the Popper.js options have changed, update the instance (destroy + create)
    if (this.props.placement !== prevProps.placement || this.props.referenceElement !== prevProps.referenceElement || this.props.positionFixed !== prevProps.positionFixed || !(0, _deepEqual.default)(this.props.modifiers, prevProps.modifiers, {
      strict: true
    })) {
      // develop only check that modifiers isn't being updated needlessly
      if (false) {}

      this.updatePopperInstance();
    } else if (this.props.eventsEnabled !== prevProps.eventsEnabled && this.popperInstance) {
      this.props.eventsEnabled ? this.popperInstance.enableEventListeners() : this.popperInstance.disableEventListeners();
    } // A placement difference in state means popper determined a new placement
    // apart from the props value. By the time the popper element is rendered with
    // the new position Popper has already measured it, if the place change triggers
    // a size change it will result in a misaligned popper. So we schedule an update to be sure.


    if (prevState.placement !== this.state.placement) {
      this.scheduleUpdate();
    }
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    (0, _utils.setRef)(this.props.innerRef, null);
    this.destroyPopperInstance();
  };

  _proto.render = function render() {
    return (0, _utils.unwrapArray)(this.props.children)({
      ref: this.setPopperNode,
      style: this.getPopperStyle(),
      placement: this.getPopperPlacement(),
      outOfBoundaries: this.getOutOfBoundariesState(),
      scheduleUpdate: this.scheduleUpdate,
      arrowProps: {
        ref: this.setArrowNode,
        style: this.getArrowStyle()
      }
    });
  };

  return InnerPopper;
}(React.Component);

exports.InnerPopper = InnerPopper;
(0, _defineProperty2.default)(InnerPopper, "defaultProps", {
  placement: 'bottom',
  eventsEnabled: true,
  referenceElement: undefined,
  positionFixed: false
});
var placements = _popper.default.placements;
exports.placements = placements;

function Popper(_ref) {
  var referenceElement = _ref.referenceElement,
      props = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["referenceElement"]);
  return React.createElement(_Manager.ManagerReferenceNodeContext.Consumer, null, function (referenceNode) {
    return React.createElement(InnerPopper, (0, _extends2.default)({
      referenceElement: referenceElement !== undefined ? referenceElement : referenceNode
    }, props));
  });
}

/***/ }),

/***/ 548:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(141);

var _interopRequireDefault = __webpack_require__(142);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ManagerReferenceNodeSetterContext = exports.ManagerReferenceNodeContext = void 0;

var _assertThisInitialized2 = _interopRequireDefault(__webpack_require__(10));

var _inheritsLoose2 = _interopRequireDefault(__webpack_require__(441));

var _defineProperty2 = _interopRequireDefault(__webpack_require__(24));

var React = _interopRequireWildcard(__webpack_require__(1));

var _createReactContext = _interopRequireDefault(__webpack_require__(321));

var ManagerReferenceNodeContext = (0, _createReactContext.default)();
exports.ManagerReferenceNodeContext = ManagerReferenceNodeContext;
var ManagerReferenceNodeSetterContext = (0, _createReactContext.default)();
exports.ManagerReferenceNodeSetterContext = ManagerReferenceNodeSetterContext;

var Manager =
/*#__PURE__*/
function (_React$Component) {
  (0, _inheritsLoose2.default)(Manager, _React$Component);

  function Manager() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "referenceNode", void 0);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setReferenceNode", function (newReferenceNode) {
      if (newReferenceNode && _this.referenceNode !== newReferenceNode) {
        _this.referenceNode = newReferenceNode;

        _this.forceUpdate();
      }
    });
    return _this;
  }

  var _proto = Manager.prototype;

  _proto.componentWillUnmount = function componentWillUnmount() {
    this.referenceNode = null;
  };

  _proto.render = function render() {
    return React.createElement(ManagerReferenceNodeContext.Provider, {
      value: this.referenceNode
    }, React.createElement(ManagerReferenceNodeSetterContext.Provider, {
      value: this.setReferenceNode
    }, this.props.children));
  };

  return Manager;
}(React.Component);

exports.default = Manager;

/***/ }),

/***/ 549:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _gud = __webpack_require__(550);

var _gud2 = _interopRequireDefault(_gud);

var _warning = __webpack_require__(7);

var _warning2 = _interopRequireDefault(_warning);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MAX_SIGNED_31_BIT_INT = 1073741823;

// Inlined Object.is polyfill.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
function objectIs(x, y) {
  if (x === y) {
    return x !== 0 || 1 / x === 1 / y;
  } else {
    return x !== x && y !== y;
  }
}

function createEventEmitter(value) {
  var handlers = [];
  return {
    on: function on(handler) {
      handlers.push(handler);
    },
    off: function off(handler) {
      handlers = handlers.filter(function (h) {
        return h !== handler;
      });
    },
    get: function get() {
      return value;
    },
    set: function set(newValue, changedBits) {
      value = newValue;
      handlers.forEach(function (handler) {
        return handler(value, changedBits);
      });
    }
  };
}

function onlyChild(children) {
  return Array.isArray(children) ? children[0] : children;
}

function createReactContext(defaultValue, calculateChangedBits) {
  var _Provider$childContex, _Consumer$contextType;

  var contextProp = '__create-react-context-' + (0, _gud2.default)() + '__';

  var Provider = function (_Component) {
    _inherits(Provider, _Component);

    function Provider() {
      var _temp, _this, _ret;

      _classCallCheck(this, Provider);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = _possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.emitter = createEventEmitter(_this.props.value), _temp), _possibleConstructorReturn(_this, _ret);
    }

    Provider.prototype.getChildContext = function getChildContext() {
      var _ref;

      return _ref = {}, _ref[contextProp] = this.emitter, _ref;
    };

    Provider.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      if (this.props.value !== nextProps.value) {
        var oldValue = this.props.value;
        var newValue = nextProps.value;
        var changedBits = void 0;

        if (objectIs(oldValue, newValue)) {
          changedBits = 0; // No change
        } else {
          changedBits = typeof calculateChangedBits === 'function' ? calculateChangedBits(oldValue, newValue) : MAX_SIGNED_31_BIT_INT;
          if (false) {}

          changedBits |= 0;

          if (changedBits !== 0) {
            this.emitter.set(nextProps.value, changedBits);
          }
        }
      }
    };

    Provider.prototype.render = function render() {
      return this.props.children;
    };

    return Provider;
  }(_react.Component);

  Provider.childContextTypes = (_Provider$childContex = {}, _Provider$childContex[contextProp] = _propTypes2.default.object.isRequired, _Provider$childContex);

  var Consumer = function (_Component2) {
    _inherits(Consumer, _Component2);

    function Consumer() {
      var _temp2, _this2, _ret2;

      _classCallCheck(this, Consumer);

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, _Component2.call.apply(_Component2, [this].concat(args))), _this2), _this2.state = {
        value: _this2.getValue()
      }, _this2.onUpdate = function (newValue, changedBits) {
        var observedBits = _this2.observedBits | 0;
        if ((observedBits & changedBits) !== 0) {
          _this2.setState({ value: _this2.getValue() });
        }
      }, _temp2), _possibleConstructorReturn(_this2, _ret2);
    }

    Consumer.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      var observedBits = nextProps.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentDidMount = function componentDidMount() {
      if (this.context[contextProp]) {
        this.context[contextProp].on(this.onUpdate);
      }
      var observedBits = this.props.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentWillUnmount = function componentWillUnmount() {
      if (this.context[contextProp]) {
        this.context[contextProp].off(this.onUpdate);
      }
    };

    Consumer.prototype.getValue = function getValue() {
      if (this.context[contextProp]) {
        return this.context[contextProp].get();
      } else {
        return defaultValue;
      }
    };

    Consumer.prototype.render = function render() {
      return onlyChild(this.props.children)(this.state.value);
    };

    return Consumer;
  }(_react.Component);

  Consumer.contextTypes = (_Consumer$contextType = {}, _Consumer$contextType[contextProp] = _propTypes2.default.object, _Consumer$contextType);


  return {
    Provider: Provider,
    Consumer: Consumer
  };
}

exports.default = createReactContext;
module.exports = exports['default'];

/***/ }),

/***/ 550:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {// @flow


var key = '__global_unique_id__';

module.exports = function() {
  return global[key] = (global[key] || 0) + 1;
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 551:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setRef = exports.shallowEqual = exports.safeInvoke = exports.unwrapArray = void 0;

/**
 * Takes an argument and if it's an array, returns the first item in the array,
 * otherwise returns the argument. Used for Preact compatibility.
 */
var unwrapArray = function unwrapArray(arg) {
  return Array.isArray(arg) ? arg[0] : arg;
};
/**
 * Takes a maybe-undefined function and arbitrary args and invokes the function
 * only if it is defined.
 */


exports.unwrapArray = unwrapArray;

var safeInvoke = function safeInvoke(fn) {
  if (typeof fn === "function") {
    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    return fn.apply(void 0, args);
  }
};
/**
 * Does a shallow equality check of two objects by comparing the reference
 * equality of each value.
 */


exports.safeInvoke = safeInvoke;

var shallowEqual = function shallowEqual(objA, objB) {
  var aKeys = Object.keys(objA);
  var bKeys = Object.keys(objB);

  if (bKeys.length !== aKeys.length) {
    return false;
  }

  for (var i = 0; i < bKeys.length; i++) {
    var key = aKeys[i];

    if (objA[key] !== objB[key]) {
      return false;
    }
  }

  return true;
};
/**
 * Sets a ref using either a ref callback or a ref object
 */


exports.shallowEqual = shallowEqual;

var setRef = function setRef(ref, node) {
  // if its a function call it
  if (typeof ref === "function") {
    return safeInvoke(ref, node);
  } // otherwise we should treat it as a ref object
  else if (ref != null) {
      ref.current = node;
    }
};

exports.setRef = setRef;

/***/ }),

/***/ 552:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _AutosizeInput = _interopRequireDefault(__webpack_require__(322));

var _Token = _interopRequireDefault(__webpack_require__(323));

var _utils = __webpack_require__(155);

var _hintContainer = _interopRequireDefault(__webpack_require__(326));

var _inputContainer = _interopRequireDefault(__webpack_require__(327));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HintedAutosizeInput = (0, _hintContainer["default"])(_AutosizeInput["default"]);

var TypeaheadInputMulti =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadInputMulti, _React$Component);

  function TypeaheadInputMulti() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TypeaheadInputMulti);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TypeaheadInputMulti)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderToken", function (option, idx) {
      var _this$props = _this.props,
          _onRemove = _this$props.onRemove,
          renderToken = _this$props.renderToken;

      var props = _objectSpread({}, _this.props, {
        onRemove: function onRemove() {
          return _onRemove(option);
        }
      });

      return renderToken(option, props, idx);
    });

    _defineProperty(_assertThisInitialized(_this), "_handleContainerClickOrFocus", function (e) {
      // Don't focus the input if it's disabled.
      if (_this.props.disabled) {
        e.target.blur();
        return;
      } // Move cursor to the end if the user clicks outside the actual input.


      var inputNode = _this._input;

      if (e.target !== inputNode && (0, _utils.isSelectable)(inputNode)) {
        inputNode.selectionStart = inputNode.value.length;
      }

      inputNode.focus();
    });

    _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
      var _this$props2 = _this.props,
          onKeyDown = _this$props2.onKeyDown,
          selected = _this$props2.selected,
          value = _this$props2.value;

      switch (e.keyCode) {
        case _constants.BACKSPACE:
          if (e.target === _this._input && selected.length && !value) {
            // Prevent browser from going back.
            e.preventDefault(); // If the input is selected and there is no text, focus the last
            // token when the user hits backspace.

            var children = _this._wrapper.children;
            var lastToken = children[children.length - 2];
            lastToken && lastToken.focus();
          }

          break;

        default:
          break;
      }

      onKeyDown(e);
    });

    return _this;
  }

  _createClass(TypeaheadInputMulti, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          className = _this$props3.className,
          inputClassName = _this$props3.inputClassName,
          labelKey = _this$props3.labelKey,
          onRemove = _this$props3.onRemove,
          renderToken = _this$props3.renderToken,
          selected = _this$props3.selected,
          props = _objectWithoutProperties(_this$props3, ["className", "inputClassName", "labelKey", "onRemove", "renderToken", "selected"]);

      return (
        /* eslint-disable jsx-a11y/no-static-element-interactions */

        /* eslint-disable jsx-a11y/click-events-have-key-events */
        _react["default"].createElement("div", {
          className: (0, _classnames["default"])('form-control', 'rbt-input-multi', className),
          disabled: props.disabled,
          onClick: this._handleContainerClickOrFocus,
          onFocus: this._handleContainerClickOrFocus,
          tabIndex: -1
        }, _react["default"].createElement("div", {
          className: "rbt-input-wrapper",
          ref: function ref(el) {
            return _this2._wrapper = el;
          }
        }, selected.map(this._renderToken), _react["default"].createElement(HintedAutosizeInput, _extends({}, props, {
          inputClassName: (0, _classnames["default"])('rbt-input-main', inputClassName),
          inputRef: function inputRef(input) {
            _this2._input = input;

            _this2.props.inputRef(input);
          },
          inputStyle: {
            backgroundColor: 'transparent',
            border: 0,
            boxShadow: 'none',
            cursor: 'inherit',
            outline: 'none',
            padding: 0
          },
          onKeyDown: this._handleKeyDown,
          style: {
            position: 'relative',
            zIndex: 1
          }
        }))))
        /* eslint-enable jsx-a11y/no-static-element-interactions */

        /* eslint-enable jsx-a11y/click-events-have-key-events */

      );
    }
  }]);

  return TypeaheadInputMulti;
}(_react["default"].Component);

TypeaheadInputMulti.propTypes = {
  /**
   * Provides a hook for customized rendering of tokens when multiple
   * selections are enabled.
   */
  renderToken: _propTypes["default"].func
};
TypeaheadInputMulti.defaultProps = {
  renderToken: function renderToken(option, props, idx) {
    return _react["default"].createElement(_Token["default"], {
      disabled: props.disabled,
      key: idx,
      onRemove: props.onRemove,
      tabIndex: props.tabIndex
    }, (0, _utils.getOptionLabel)(option, props.labelKey));
  }
};

var _default = (0, _inputContainer["default"])(TypeaheadInputMulti);

exports["default"] = _default;

/***/ }),

/***/ 553:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var _default = function () {
  // HTML DOM and SVG DOM may have different support levels,
  // so we need to check on context instead of a document root element.
  return _inDOM.default ? function (context, node) {
    if (context.contains) {
      return context.contains(node);
    } else if (context.compareDocumentPosition) {
      return context === node || !!(context.compareDocumentPosition(node) & 16);
    } else {
      return fallback(context, node);
    }
  } : fallback;
}();

exports.default = _default;

function fallback(context, node) {
  if (node) do {
    if (node === context) return true;
  } while (node = node.parentNode);
  return false;
}

module.exports = exports["default"];

/***/ }),

/***/ 554:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (node, event, handler, capture) {
  (0, _on2.default)(node, event, handler, capture);

  return {
    remove: function remove() {
      (0, _off2.default)(node, event, handler, capture);
    }
  };
};

var _on = __webpack_require__(555);

var _on2 = _interopRequireDefault(_on);

var _off = __webpack_require__(556);

var _off2 = _interopRequireDefault(_off);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports['default'];

/***/ }),

/***/ 555:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var on = function on() {};

if (_inDOM.default) {
  on = function () {
    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.addEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.attachEvent('on' + eventName, function (e) {
        e = e || window.event;
        e.target = e.target || e.srcElement;
        e.currentTarget = node;
        handler.call(node, e);
      });
    };
  }();
}

var _default = on;
exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 556:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var off = function off() {};

if (_inDOM.default) {
  off = function () {
    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.removeEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.detachEvent('on' + eventName, handler);
    };
  }();
}

var _default = off;
exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 557:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _hintContainer = _interopRequireDefault(__webpack_require__(326));

var _inputContainer = _interopRequireDefault(__webpack_require__(327));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TypeaheadInputSingle =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadInputSingle, _React$Component);

  function TypeaheadInputSingle() {
    _classCallCheck(this, TypeaheadInputSingle);

    return _possibleConstructorReturn(this, _getPrototypeOf(TypeaheadInputSingle).apply(this, arguments));
  }

  _createClass(TypeaheadInputSingle, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          className = _this$props.className,
          inputRef = _this$props.inputRef,
          props = _objectWithoutProperties(_this$props, ["className", "inputRef"]);

      return _react["default"].createElement("input", _extends({}, props, {
        className: (0, _classnames["default"])('rbt-input-main', 'form-control', className),
        ref: inputRef
      }));
    }
  }]);

  return TypeaheadInputSingle;
}(_react["default"].Component);

var _default = (0, _inputContainer["default"])((0, _hintContainer["default"])(TypeaheadInputSingle));

exports["default"] = _default;

/***/ }),

/***/ 558:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = isRequiredForA11y;
function isRequiredForA11y(validator) {
  return function validate(props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] == null) {
      return new Error('The ' + location + ' `' + propFullNameSafe + '` is required to make ' + ('`' + componentNameSafe + '` accessible for users of assistive ') + 'technologies such as screen readers.');
    }

    for (var _len = arguments.length, args = Array(_len > 5 ? _len - 5 : 0), _key = 5; _key < _len; _key++) {
      args[_key - 5] = arguments[_key];
    }

    return validator.apply(undefined, [props, propName, componentName, location, propFullName].concat(args));
  };
}
module.exports = exports['default'];

/***/ }),

/***/ 559:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _noop = _interopRequireDefault(__webpack_require__(222));

var _uniqueId = _interopRequireDefault(__webpack_require__(478));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _deprecated = _interopRequireDefault(__webpack_require__(560));

var _react = _interopRequireDefault(__webpack_require__(1));

var _RootCloseWrapper = _interopRequireDefault(__webpack_require__(325));

var _contextContainer = _interopRequireDefault(__webpack_require__(561));

var _propTypes2 = __webpack_require__(315);

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function genId() {
  var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return prefix + Math.random().toString(36).substr(2, 12);
}

function isBodyMenuClick(e, props) {
  if (!props.bodyContainer && !props.positionFixed) {
    return false;
  }

  var target = e.target;

  while (target && target !== document.body) {
    if (target.className && typeof target.className === 'string' && target.className.indexOf('rbt-menu') > -1) {
      return true;
    }

    target = target.parentNode;
  }

  return false;
}

function getInitialState(props) {
  var defaultInputValue = props.defaultInputValue,
      defaultOpen = props.defaultOpen,
      defaultSelected = props.defaultSelected,
      maxResults = props.maxResults,
      multiple = props.multiple;
  var selected = props.selected ? props.selected.slice() : defaultSelected.slice();
  var text = defaultInputValue;

  if (!multiple && selected.length) {
    // Set the text if an initial selection is passed in.
    text = (0, _utils.getOptionLabel)((0, _head["default"])(selected), props.labelKey);

    if (selected.length > 1) {
      // Limit to 1 selection in single-select mode.
      selected = selected.slice(0, 1);
    }
  }

  return {
    activeIndex: -1,
    activeItem: null,
    initialItem: null,
    isFocused: false,
    selected: selected,
    showMenu: defaultOpen,
    shownResults: maxResults,
    text: text
  };
}

function skipDisabledOptions(results, activeIndex, keyCode) {
  var newActiveIndex = activeIndex;

  while (results[newActiveIndex] && results[newActiveIndex].disabled) {
    newActiveIndex += keyCode === _constants.UP ? -1 : 1;
  }

  return newActiveIndex;
}

function typeaheadContainer(Component) {
  var Typeahead = (0, _contextContainer["default"])(Component);

  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "state", getInitialState(_this.props));

      _defineProperty(_assertThisInitialized(_this), "_menuId", genId('rbt-menu-'));

      _defineProperty(_assertThisInitialized(_this), "blur", function () {
        _this.getInput().blur();

        _this._hideMenu();
      });

      _defineProperty(_assertThisInitialized(_this), "clear", function () {
        _this.setState(function (state, props) {
          return _objectSpread({}, getInitialState(props), {
            isFocused: state.isFocused,
            selected: [],
            text: ''
          });
        });
      });

      _defineProperty(_assertThisInitialized(_this), "focus", function () {
        _this.getInput().focus();
      });

      _defineProperty(_assertThisInitialized(_this), "getInput", function () {
        return _this._input;
      });

      _defineProperty(_assertThisInitialized(_this), "getInstance", function () {
        return _assertThisInitialized(_this);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActiveIndexChange", function (activeIndex) {
        var newState = {
          activeIndex: activeIndex
        };

        if (activeIndex === -1) {
          // Reset the active item if there is no active index.
          newState.activeItem = null;
        }

        _this.setState(newState);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActiveItemChange", function (activeItem) {
        // Don't update the active item if it hasn't changed.
        if (!(0, _utils.areEqual)(activeItem, _this.state.activeItem, _this.props.labelKey)) {
          _this.setState({
            activeItem: activeItem
          });
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleBlur", function (e) {
        e.persist();

        _this.setState({
          isFocused: false
        }, function () {
          return _this.props.onBlur(e);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleClear", function () {
        _this.clear();

        _this._updateSelected([]);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleFocus", function (e) {
        e.persist();

        _this.setState({
          isFocused: true,
          showMenu: true
        }, function () {
          return _this.props.onFocus(e);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInitialItemChange", function (initialItem) {
        // Don't update the initial item if it hasn't changed.
        if (!(0, _utils.areEqual)(initialItem, _this.state.initialItem, _this.props.labelKey)) {
          _this.setState({
            initialItem: initialItem
          });
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInputChange", function (e) {
        e.persist();
        var text = e.target.value;

        var _getInitialState = getInitialState(_this.props),
            activeIndex = _getInitialState.activeIndex,
            activeItem = _getInitialState.activeItem,
            shownResults = _getInitialState.shownResults;

        var _this$props = _this.props,
            multiple = _this$props.multiple,
            onInputChange = _this$props.onInputChange;

        _this.setState({
          activeIndex: activeIndex,
          activeItem: activeItem,
          showMenu: true,
          shownResults: shownResults,
          text: text
        }, function () {
          return onInputChange(text, e);
        }); // Clear any selections if text is entered in single-select mode.


        if (_this.state.selected.length && !multiple) {
          _this._updateSelected([]);
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e, results, isMenuShown) {
        var activeItem = _this.state.activeItem;
        var activeIndex = _this.state.activeIndex;

        switch (e.keyCode) {
          case _constants.UP:
          case _constants.DOWN:
            if (!isMenuShown) {
              _this._showMenu();

              break;
            } // Prevents input cursor from going to the beginning when pressing up.


            e.preventDefault(); // Increment or decrement index based on user keystroke.

            activeIndex += e.keyCode === _constants.UP ? -1 : 1; // Skip over any disabled options.

            activeIndex = skipDisabledOptions(results, activeIndex, e.keyCode); // If we've reached the end, go back to the beginning or vice-versa.

            if (activeIndex === results.length) {
              activeIndex = -1;
            } else if (activeIndex === -2) {
              activeIndex = results.length - 1; // Skip over any disabled options.

              activeIndex = skipDisabledOptions(results, activeIndex, e.keyCode);
            }

            _this._handleActiveIndexChange(activeIndex);

            break;

          case _constants.ESC:
            isMenuShown && _this._hideMenu();
            break;

          case _constants.RETURN:
            if (!isMenuShown) {
              break;
            } // Prevent form submission while menu is open.


            e.preventDefault();
            activeItem && _this._handleMenuItemSelect(activeItem, e);
            break;

          case _constants.RIGHT:
          case _constants.TAB:
            if (!isMenuShown) {
              break;
            }

            if (activeItem && !activeItem.paginationOption) {
              // Prevent blurring when selecting the active item.
              e.keyCode === _constants.TAB && e.preventDefault();

              _this._handleSelectionAdd(activeItem);

              break;
            }

            if (e.keyCode === _constants.TAB) {
              _this._hideMenu();
            }

            break;

          default:
            break;
        }

        _this.props.onKeyDown(e);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleMenuItemSelect", function (option, e) {
        if (option.paginationOption) {
          _this._handlePaginate(e);
        } else {
          _this._handleSelectionAdd(option);
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handlePaginate", function (e) {
        e.persist();

        _this.setState(function (_ref, _ref2) {
          var shownResults = _ref.shownResults;
          var maxResults = _ref2.maxResults;
          return {
            shownResults: shownResults + maxResults
          };
        }, function () {
          return _this.props.onPaginate(e, _this.state.shownResults);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSelectionAdd", function (selection) {
        var _this$props2 = _this.props,
            multiple = _this$props2.multiple,
            labelKey = _this$props2.labelKey;
        var selected;
        var text;

        if (multiple) {
          // If multiple selections are allowed, add the new selection to the
          // existing selections.
          selected = _this.state.selected.concat(selection);
          text = '';
        } else {
          // If only a single selection is allowed, replace the existing selection
          // with the new one.
          selected = [selection];
          text = (0, _utils.getOptionLabel)(selection, labelKey);
        }

        _this._hideMenu();

        _this.setState({
          initialItem: selection,
          text: text
        }); // Text must be updated before the selection to fix #211.
        // TODO: Find a more robust way of solving the issue.


        _this._updateSelected(selected);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSelectionRemove", function (selection) {
        var selected = _this.state.selected.filter(function (option) {
          return !(0, _isEqual["default"])(option, selection);
        }); // Make sure the input stays focused after the item is removed.


        _this.focus();

        _this._hideMenu();

        _this._updateSelected(selected);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleRootClose", function (e) {
        if (isBodyMenuClick(e, _this.props) || !_this.state.showMenu) {
          return;
        }

        _this._hideMenu();
      });

      _defineProperty(_assertThisInitialized(_this), "_hideMenu", function () {
        var _getInitialState2 = getInitialState(_this.props),
            activeIndex = _getInitialState2.activeIndex,
            activeItem = _getInitialState2.activeItem,
            shownResults = _getInitialState2.shownResults;

        _this.setState({
          activeIndex: activeIndex,
          activeItem: activeItem,
          showMenu: false,
          shownResults: shownResults
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_showMenu", function () {
        _this.setState({
          showMenu: true
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_updateSelected", function (selected) {
        _this.setState({
          selected: selected
        }, function () {
          _this.props.onChange && _this.props.onChange(selected);
        });
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this.props.autoFocus && this.focus();
      }
    }, {
      key: "componentWillReceiveProps",
      value: function componentWillReceiveProps(nextProps) {
        var labelKey = nextProps.labelKey,
            multiple = nextProps.multiple,
            selected = nextProps.selected; // If new selections are passed via props, treat as a controlled input.

        if (selected && !(0, _isEqual["default"])(selected, this.state.selected)) {
          this.setState({
            selected: selected
          });

          if (multiple) {
            return;
          }

          this.setState({
            text: selected.length ? (0, _utils.getOptionLabel)((0, _head["default"])(selected), labelKey) : ''
          });
        } // Truncate selections when in single-select mode.


        var newSelected = selected || this.state.selected;

        if (!multiple && newSelected.length > 1) {
          newSelected = newSelected.slice(0, 1);
          this.setState({
            selected: newSelected,
            text: (0, _utils.getOptionLabel)((0, _head["default"])(newSelected), labelKey)
          });
          return;
        }

        if (multiple !== this.props.multiple) {
          this.setState({
            text: ''
          });
        }
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var mergedPropsAndState = _objectSpread({}, this.props, {}, this.state);

        var filterBy = mergedPropsAndState.filterBy,
            labelKey = mergedPropsAndState.labelKey,
            minLength = mergedPropsAndState.minLength,
            options = mergedPropsAndState.options,
            paginate = mergedPropsAndState.paginate,
            paginationText = mergedPropsAndState.paginationText,
            shownResults = mergedPropsAndState.shownResults,
            text = mergedPropsAndState.text;
        var results = [];

        if (text.length >= minLength) {
          var cb = Array.isArray(filterBy) ? _utils.defaultFilterBy : filterBy;
          results = options.filter(function (option) {
            return cb(option, mergedPropsAndState);
          });
        } // This must come before results are truncated.


        var shouldPaginate = paginate && results.length > shownResults; // Truncate results if necessary.

        results = (0, _utils.getTruncatedOptions)(results, shownResults); // Add the custom option if necessary.

        if ((0, _utils.addCustomOption)(results, mergedPropsAndState)) {
          results.push(_defineProperty({
            customOption: true,
            id: (0, _uniqueId["default"])('new-id-')
          }, (0, _utils.getStringLabelKey)(labelKey), text));
        } // Add the pagination item if necessary.


        if (shouldPaginate) {
          var _results$push2;

          results.push((_results$push2 = {}, _defineProperty(_results$push2, (0, _utils.getStringLabelKey)(labelKey), paginationText), _defineProperty(_results$push2, "paginationOption", true), _results$push2));
        } // This must come after checks for the custom option and pagination.


        var isMenuShown = (0, _utils.isShown)(results, mergedPropsAndState);
        return _react["default"].createElement(_RootCloseWrapper["default"], {
          disabled: this.props.open,
          onRootClose: this._handleRootClose
        }, _react["default"].createElement(Typeahead, _extends({}, mergedPropsAndState, {
          bodyContainer: this.props.positionFixed || this.props.bodyContainer,
          inputRef: function inputRef(input) {
            return _this2._input = input;
          },
          isMenuShown: isMenuShown,
          menuId: this.props.id || this.props.menuId || this._menuId,
          onActiveItemChange: this._handleActiveItemChange,
          onAdd: this._handleSelectionAdd,
          onBlur: this._handleBlur,
          onChange: this._handleInputChange,
          onClear: this._handleClear,
          onFocus: this._handleFocus,
          onInitialItemChange: this._handleInitialItemChange,
          onKeyDown: function onKeyDown(e) {
            return _this2._handleKeyDown(e, results, isMenuShown);
          },
          onMenuItemClick: this._handleMenuItemSelect,
          onRemove: this._handleSelectionRemove,
          results: results
        })));
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  WrappedTypeahead.displayName = "TypeaheadContainer(".concat((0, _utils.getDisplayName)(Typeahead), ")");
  WrappedTypeahead.propTypes = {
    /**
     * For localized accessibility: Should return a string indicating the number
     * of results for screen readers. Receives the current results.
     */
    a11yNumResults: _propTypes["default"].func,

    /**
     * For localized accessibility: Should return a string indicating the number
     * of selections for screen readers. Receives the current selections.
     */
    a11yNumSelected: _propTypes["default"].func,

    /**
     * Specify menu alignment. The default value is `justify`, which makes the
     * menu as wide as the input and truncates long values. Specifying `left`
     * or `right` will align the menu to that side and the width will be
     * determined by the length of menu item values.
     */
    align: _propTypes["default"].oneOf(['justify', 'left', 'right']),

    /**
     * Allows the creation of new selections on the fly. Note that any new items
     * will be added to the list of selections, but not the list of original
     * options unless handled as such by `Typeahead`'s parent.
     *
     * If a function is specified, it will be used to determine whether a custom
     * option should be included. The return value should be true or false.
     */
    allowNew: _propTypes["default"].oneOfType([_propTypes["default"].bool, _propTypes["default"].func]),

    /**
     * Autofocus the input when the component initially mounts.
     */
    autoFocus: _propTypes["default"].bool,

    /**
     * Whether to render the menu inline or attach to `document.body`.
     */
    bodyContainer: (0, _deprecated["default"])(_propTypes["default"].bool, 'Use `positionFixed` instead'),

    /**
     * Whether or not filtering should be case-sensitive.
     */
    caseSensitive: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.caseSensitiveType),

    /**
     * Displays a button to clear the input when there are selections.
     */
    clearButton: _propTypes["default"].bool,

    /**
     * The initial value displayed in the text input.
     */
    defaultInputValue: (0, _propTypes2.checkPropType)(_propTypes["default"].string, _propTypes2.defaultInputValueType),

    /**
     * Whether or not the menu is displayed upon initial render.
     */
    defaultOpen: _propTypes["default"].bool,

    /**
     * Specify any pre-selected options. Use only if you want the component to
     * be uncontrolled.
     */
    defaultSelected: _propTypes2.optionType,

    /**
     * Whether to disable the component.
     */
    disabled: _propTypes["default"].bool,

    /**
     * Specify whether the menu should appear above the input.
     */
    dropup: _propTypes["default"].bool,

    /**
     * Message to display in the menu if there are no valid results.
     */
    emptyLabel: (0, _propTypes2.checkPropType)(_propTypes["default"].node, _propTypes2.emptyLabelType),

    /**
     * Either an array of fields in `option` to search, or a custom filtering
     * callback.
     */
    filterBy: _propTypes["default"].oneOfType([_propTypes["default"].arrayOf(_propTypes["default"].string.isRequired), _propTypes["default"].func]),

    /**
     * Whether or not to automatically adjust the position of the menu when it
     * reaches the viewport boundaries.
     */
    flip: _propTypes["default"].bool,

    /**
     * Highlights the menu item if there is only one result and allows selecting
     * that item by hitting enter. Does not work with `allowNew`.
     */
    highlightOnlyResult: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.highlightOnlyResultType),

    /**
     * An html id attribute, required for assistive technologies such as screen
     * readers.
     */
    id: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string]), _propTypes2.idType),

    /**
     * Whether the filter should ignore accents and other diacritical marks.
     */
    ignoreDiacritics: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.ignoreDiacriticsType),

    /**
     * Props to be applied directly to the input. `onBlur`, `onChange`,
     * `onFocus`, and `onKeyDown` are ignored.
     */
    inputProps: (0, _propTypes2.checkPropType)(_propTypes["default"].object, _propTypes2.inputPropsType),

    /**
     * Bootstrap 4 only. Adds the `is-invalid` classname to the `form-control`.
     */
    isInvalid: _propTypes["default"].bool,

    /**
     * Indicate whether an asynchronous data fetch is happening.
     */
    isLoading: _propTypes["default"].bool,

    /**
     * Bootstrap 4 only. Adds the `is-valid` classname to the `form-control`.
     */
    isValid: _propTypes["default"].bool,

    /**
     * Specify the option key to use for display or a function returning the
     * display string. By default, the selector will use the `label` key.
     */
    labelKey: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].func]), _propTypes2.labelKeyType),

    /**
     * Maximum number of results to display by default. Mostly done for
     * performance reasons so as not to render too many DOM nodes in the case of
     * large data sets.
     */
    maxResults: _propTypes["default"].number,

    /**
     * Id applied to the top-level menu element. Required for accessibility.
     */
    menuId: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string]), _propTypes2.idType),

    /**
     * Number of input characters that must be entered before showing results.
     */
    minLength: _propTypes["default"].number,

    /**
     * Whether or not multiple selections are allowed.
     */
    multiple: _propTypes["default"].bool,

    /**
     * Invoked when the input is blurred. Receives an event.
     */
    onBlur: _propTypes["default"].func,

    /**
     * Invoked whenever items are added or removed. Receives an array of the
     * selected options.
     */
    onChange: _propTypes["default"].func,

    /**
     * Invoked when the input is focused. Receives an event.
     */
    onFocus: _propTypes["default"].func,

    /**
     * Invoked when the input value changes. Receives the string value of the
     * input.
     */
    onInputChange: _propTypes["default"].func,

    /**
     * Invoked when a key is pressed. Receives an event.
     */
    onKeyDown: _propTypes["default"].func,

    /**
     * Invoked when the menu is hidden.
     */
    onMenuHide: (0, _deprecated["default"])(_propTypes["default"].func, 'Use `onMenuToggle` instead'),

    /**
     * Invoked when the menu is shown.
     */
    onMenuShow: (0, _deprecated["default"])(_propTypes["default"].func, 'Use `onMenuToggle` instead'),

    /**
     * Invoked when menu visibility changes.
     */
    onMenuToggle: _propTypes["default"].func,

    /**
     * Invoked when the pagination menu item is clicked. Receives an event.
     */
    onPaginate: _propTypes["default"].func,

    /**
     * Whether or not the menu should be displayed. `undefined` allows the
     * component to control visibility, while `true` and `false` show and hide
     * the menu, respectively.
     */
    open: _propTypes["default"].bool,

    /**
     * Full set of options, including pre-selected options. Must either be an
     * array of objects (recommended) or strings.
     */
    options: _propTypes2.optionType.isRequired,

    /**
     * Give user the ability to display additional results if the number of
     * results exceeds `maxResults`.
     */
    paginate: _propTypes["default"].bool,

    /**
     * Prompt displayed when large data sets are paginated.
     */
    paginationText: _propTypes["default"].string,

    /**
     * Placeholder text for the input.
     */
    placeholder: _propTypes["default"].string,

    /**
     * Callback for custom menu rendering.
     */
    renderMenu: _propTypes["default"].func,

    /**
     * The selected option(s) displayed in the input. Use this prop if you want
     * to control the component via its parent.
     */
    selected: (0, _propTypes2.checkPropType)(_propTypes2.optionType, _propTypes2.selectedType),

    /**
     * Allows selecting the hinted result by pressing enter.
     */
    selectHintOnEnter: _propTypes["default"].bool
  };
  WrappedTypeahead.defaultProps = {
    a11yNumResults: function a11yNumResults(results) {
      var resultString = (0, _utils.pluralize)('result', results.length);
      return "".concat(resultString, ". Use up and down arrow keys to navigate.");
    },
    a11yNumSelected: function a11yNumSelected(selected) {
      return (0, _utils.pluralize)('selection', selected.length);
    },
    align: 'justify',
    allowNew: false,
    autoFocus: false,
    caseSensitive: false,
    clearButton: false,
    defaultInputValue: '',
    defaultOpen: false,
    defaultSelected: [],
    disabled: false,
    dropup: false,
    emptyLabel: 'No matches found.',
    filterBy: [],
    flip: false,
    highlightOnlyResult: false,
    ignoreDiacritics: true,
    inputProps: {},
    isInvalid: false,
    isLoading: false,
    isValid: false,
    labelKey: _constants.DEFAULT_LABELKEY,
    maxResults: 100,
    minLength: 0,
    multiple: false,
    onBlur: _noop["default"],
    onFocus: _noop["default"],
    onInputChange: _noop["default"],
    onKeyDown: _noop["default"],
    onPaginate: _noop["default"],
    paginate: true,
    paginationText: 'Display additional results...',
    placeholder: '',
    selectHintOnEnter: false
  };
  return WrappedTypeahead;
}

var _default = typeaheadContainer;
exports["default"] = _default;

/***/ }),

/***/ 560:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = deprecated;

var _warning = __webpack_require__(7);

var _warning2 = _interopRequireDefault(_warning);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var warned = {};

function deprecated(validator, reason) {
  return function validate(props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] != null) {
      var messageKey = componentName + '.' + propName;

      (0, _warning2.default)(warned[messageKey], 'The ' + location + ' `' + propFullNameSafe + '` of ' + ('`' + componentNameSafe + '` is deprecated. ' + reason + '.'));

      warned[messageKey] = true;
    }

    for (var _len = arguments.length, args = Array(_len > 5 ? _len - 5 : 0), _key = 5; _key < _len; _key++) {
      args[_key - 5] = arguments[_key];
    }

    return validator.apply(undefined, [props, propName, componentName, location, propFullName].concat(args));
  };
}

/* eslint-disable no-underscore-dangle */
function _resetWarned() {
  warned = {};
}

deprecated._resetWarned = _resetWarned;
/* eslint-enable no-underscore-dangle */

module.exports = exports['default'];

/***/ }),

/***/ 561:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _pick = _interopRequireDefault(__webpack_require__(243));

var _react = _interopRequireDefault(__webpack_require__(1));

var _TypeaheadContext = _interopRequireDefault(__webpack_require__(279));

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function contextContainer(Typeahead) {
  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        var _this$props = _this.props,
            initialItem = _this$props.initialItem,
            onKeyDown = _this$props.onKeyDown,
            onAdd = _this$props.onAdd;

        switch (e.keyCode) {
          case _constants.RETURN:
            if ((0, _utils.getIsOnlyResult)(_this.props) && initialItem) {
              onAdd(initialItem);
            }

            break;

          default:
            break;
        }

        onKeyDown(e);
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        var _this$props2 = this.props,
            allowNew = _this$props2.allowNew,
            onInitialItemChange = _this$props2.onInitialItemChange,
            results = _this$props2.results; // Clear the initial item when there are no results.

        if (!(allowNew || results.length)) {
          onInitialItemChange(null);
        }
      }
    }, {
      key: "render",
      value: function render() {
        var contextValues = (0, _pick["default"])(this.props, ['activeIndex', 'initialItem', 'onActiveItemChange', 'onAdd', 'onInitialItemChange', 'onMenuItemClick', 'selectHintOnEnter']);
        return _react["default"].createElement(_TypeaheadContext["default"].Provider, {
          value: _objectSpread({}, contextValues, {
            hintText: (0, _utils.getHintText)(this.props),
            isOnlyResult: (0, _utils.getIsOnlyResult)(this.props)
          })
        }, _react["default"].createElement(Typeahead, _extends({}, this.props, {
          onKeyDown: this._handleKeyDown
        })));
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  return WrappedTypeahead;
}

var _default = contextContainer;
exports["default"] = _default;

/***/ }),

/***/ 615:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var Errores = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(Errores, _Component);

  var _super = _createSuper(Errores);

  function Errores() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Errores);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Errores, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "ant-alert ant-alert-error ant-alert-with-description ant-alert-no-icon"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ant-alert-message"
      }, "Advertencia"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ant-alert-description"
      }, this.props.mensaje));
    }
  }]);

  return Errores;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Errores);

/***/ }),

/***/ 671:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(52);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(80);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var timeLine = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(timeLine, _Component);

  var _super = _createSuper(timeLine);

  function timeLine(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, timeLine);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(timeLine, [{
    key: "render",
    value: function render() {
      var bottomChange = this.props.data == true ? "timelineBotton" : "fixed-bottom";
      var iconChange = this.props.data == true ? " icon-serviceGreen" : " icon-serviceGrey";
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding display-sm ",
        style: {
          display: localStorage.page === "3" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 pt-2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "".concat(iconChange)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "mr-5",
        style: {
          color: "#102a73"
        }
      }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#102a73"
        }
      }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3",
        style: {
          color: "#8a8a8a"
        }
      }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding display-sm",
        style: {
          display: localStorage.page === "2" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 pt-2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-serviceBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#102a73"
        }
      }, "3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3",
        style: {
          color: "#102a73"
        }
      }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto NoPadding display-large ".concat(bottomChange),
        style: {
          display: localStorage.page === "3" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "".concat(iconChange)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-5 mr-5",
        style: {
          color: "#102a73"
        }
      }, "RUTA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "SERVICIOS EXTRAS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        style: {
          color: "#8a8a8a"
        }
      }, "PRESUPUESTO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "pt-2 mx-auto NoPadding display-large",
        style: {
          display: localStorage.page === "2" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-serviceBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-5 mr-5",
        style: {
          color: "#102a73"
        }
      }, "RUTA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS EXTRAS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        style: {
          color: "#8a8a8a"
        }
      }, "PRESUPUESTO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })));
    }
  }]);

  return timeLine;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (timeLine);

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ Beforeunload; });

// UNUSED EXPORTS: useBeforeunload

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/use-isomorphic-layout-effect/dist/use-isomorphic-layout-effect.browser.esm.js


var index =  react["useLayoutEffect"] ;

/* harmony default export */ var use_isomorphic_layout_effect_browser_esm = (index);

// CONCATENATED MODULE: ./node_modules/use-latest/dist/use-latest.esm.js



var use_latest_esm_useLatest = function useLatest(value) {
  var ref = Object(react["useRef"])(value);
  use_isomorphic_layout_effect_browser_esm(function () {
    ref.current = value;
  });
  return ref;
};

/* harmony default export */ var use_latest_esm = (use_latest_esm_useLatest);

// CONCATENATED MODULE: ./node_modules/react-beforeunload/lib/index.esm.js




var index_esm_useBeforeunload = function useBeforeunload(handler) {
  if (false) {}

  var handlerRef = use_latest_esm(handler);
  Object(react["useEffect"])(function () {
    var handleBeforeunload = function handleBeforeunload(event) {
      var returnValue;

      if (handlerRef.current != null) {
        returnValue = handlerRef.current(event);
      } // Chrome requires `returnValue` to be set.


      if (event.defaultPrevented) {
        event.returnValue = '';
      }

      if (typeof returnValue === 'string') {
        event.returnValue = returnValue;
        return returnValue;
      }
    };

    window.addEventListener('beforeunload', handleBeforeunload);
    return function () {
      window.removeEventListener('beforeunload', handleBeforeunload);
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
};

var Beforeunload = function Beforeunload(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      onBeforeunload = _ref.onBeforeunload;
  index_esm_useBeforeunload(onBeforeunload);
  return children;
};

if (false) {}


//# sourceMappingURL=index.esm.js.map


/***/ }),

/***/ 939:
/***/ (function(module, exports, __webpack_require__) {

!function(e,t){ true?module.exports=t(__webpack_require__(1)):undefined}(this,function(e){return function(e){function t(n){if(r[n])return r[n].exports;var o=r[n]={exports:{},id:n,loaded:!1};return e[n].call(o.exports,o,o.exports,t),o.loaded=!0,o.exports}var r={};return t.m=e,t.c=r,t.p="",t(0)}([function(e,t,r){"use strict";function n(e){return e&&e.__esModule?e:{default:e}}function o(e,t){var r={};for(var n in e)t.indexOf(n)>=0||Object.prototype.hasOwnProperty.call(e,n)&&(r[n]=e[n]);return r}function i(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function a(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function u(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(t,"__esModule",{value:!0}),t.conformToMask=void 0;var s=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var r=arguments[t];for(var n in r)Object.prototype.hasOwnProperty.call(r,n)&&(e[n]=r[n])}return e},l=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),f=r(3);Object.defineProperty(t,"conformToMask",{enumerable:!0,get:function(){return n(f).default}});var c=r(11),p=n(c),d=r(9),h=n(d),v=r(5),y=n(v),m=r(2),b=function(e){function t(){var e;i(this,t);for(var r=arguments.length,n=Array(r),o=0;o<r;o++)n[o]=arguments[o];var u=a(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(n)));return u.setRef=u.setRef.bind(u),u.onBlur=u.onBlur.bind(u),u.onChange=u.onChange.bind(u),u}return u(t,e),l(t,[{key:"setRef",value:function(e){this.inputElement=e}},{key:"initTextMask",value:function(){var e=this.props,t=this.props.value;this.textMaskInputElement=(0,y.default)(s({inputElement:this.inputElement},e)),this.textMaskInputElement.update(t)}},{key:"componentDidMount",value:function(){this.initTextMask()}},{key:"componentDidUpdate",value:function(e){var t=this.props,r=t.value,n=t.pipe,o=t.mask,i=t.guide,a=t.placeholderChar,u=t.showMask,s={guide:i,placeholderChar:a,showMask:u},l="function"==typeof n&&"function"==typeof e.pipe?n.toString()!==e.pipe.toString():(0,m.isNil)(n)&&!(0,m.isNil)(e.pipe)||!(0,m.isNil)(n)&&(0,m.isNil)(e.pipe),f=o.toString()!==e.mask.toString(),c=Object.keys(s).some(function(t){return s[t]!==e[t]})||f||l,p=r!==this.inputElement.value;(p||c)&&this.initTextMask()}},{key:"render",value:function e(){var t=this.props,e=t.render,r=o(t,["render"]);return delete r.mask,delete r.guide,delete r.pipe,delete r.placeholderChar,delete r.keepCharPositions,delete r.value,delete r.onBlur,delete r.onChange,delete r.showMask,e(this.setRef,s({onBlur:this.onBlur,onChange:this.onChange,defaultValue:this.props.value},r))}},{key:"onChange",value:function(e){this.textMaskInputElement.update(),"function"==typeof this.props.onChange&&this.props.onChange(e)}},{key:"onBlur",value:function(e){"function"==typeof this.props.onBlur&&this.props.onBlur(e)}}]),t}(p.default.PureComponent);t.default=b,b.propTypes={mask:h.default.oneOfType([h.default.array,h.default.func,h.default.bool,h.default.shape({mask:h.default.oneOfType([h.default.array,h.default.func]),pipe:h.default.func})]).isRequired,guide:h.default.bool,value:h.default.oneOfType([h.default.string,h.default.number]),pipe:h.default.func,placeholderChar:h.default.string,keepCharPositions:h.default.bool,showMask:h.default.bool},b.defaultProps={render:function(e,t){return p.default.createElement("input",s({ref:e},t))}}},function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.placeholderChar="_",t.strFunction="function"},function(e,t,r){"use strict";function n(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:f,t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:l.placeholderChar;if(!o(e))throw new Error("Text-mask:convertMaskToPlaceholder; The mask property must be an array.");if(e.indexOf(t)!==-1)throw new Error("Placeholder character must not be used as part of the mask. Please specify a character that is not present in your mask as your placeholder character.\n\n"+("The placeholder character that was received is: "+JSON.stringify(t)+"\n\n")+("The mask that was received is: "+JSON.stringify(e)));return e.map(function(e){return e instanceof RegExp?t:e}).join("")}function o(e){return Array.isArray&&Array.isArray(e)||e instanceof Array}function i(e){return"string"==typeof e||e instanceof String}function a(e){return"number"==typeof e&&void 0===e.length&&!isNaN(e)}function u(e){return"undefined"==typeof e||null===e}function s(e){for(var t=[],r=void 0;r=e.indexOf(c),r!==-1;)t.push(r),e.splice(r,1);return{maskWithoutCaretTraps:e,indexes:t}}Object.defineProperty(t,"__esModule",{value:!0}),t.convertMaskToPlaceholder=n,t.isArray=o,t.isString=i,t.isNumber=a,t.isNil=u,t.processCaretTraps=s;var l=r(1),f=[],c="[]"},function(e,t,r){"use strict";function n(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:s,t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:u,r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{};if(!(0,i.isArray)(t)){if(("undefined"==typeof t?"undefined":o(t))!==a.strFunction)throw new Error("Text-mask:conformToMask; The mask property must be an array.");t=t(e,r),t=(0,i.processCaretTraps)(t).maskWithoutCaretTraps}var n=r.guide,l=void 0===n||n,f=r.previousConformedValue,c=void 0===f?s:f,p=r.placeholderChar,d=void 0===p?a.placeholderChar:p,h=r.placeholder,v=void 0===h?(0,i.convertMaskToPlaceholder)(t,d):h,y=r.currentCaretPosition,m=r.keepCharPositions,b=l===!1&&void 0!==c,g=e.length,k=c.length,C=v.length,O=t.length,T=g-k,P=T>0,x=y+(P?-T:0),w=x+Math.abs(T);if(m===!0&&!P){for(var S=s,_=x;_<w;_++)v[_]===d&&(S+=d);e=e.slice(0,x)+S+e.slice(x,g)}for(var M=e.split(s).map(function(e,t){return{char:e,isNew:t>=x&&t<w}}),j=g-1;j>=0;j--){var E=M[j].char;if(E!==d){var R=j>=x&&k===O;E===v[R?j-T:j]&&M.splice(j,1)}}var V=s,N=!1;e:for(var A=0;A<C;A++){var B=v[A];if(B===d){if(M.length>0)for(;M.length>0;){var I=M.shift(),F=I.char,q=I.isNew;if(F===d&&b!==!0){V+=d;continue e}if(t[A].test(F)){if(m===!0&&q!==!1&&c!==s&&l!==!1&&P){for(var D=M.length,L=null,W=0;W<D;W++){var J=M[W];if(J.char!==d&&J.isNew===!1)break;if(J.char===d){L=W;break}}null!==L?(V+=F,M.splice(L,1)):A--}else V+=F;continue e}N=!0}b===!1&&(V+=v.substr(A,C));break}V+=B}if(b&&P===!1){for(var U=null,H=0;H<V.length;H++)v[H]===d&&(U=H);V=null!==U?V.substr(0,U+1):s}return{conformedValue:V,meta:{someCharsRejected:N}}}Object.defineProperty(t,"__esModule",{value:!0});var o="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.default=n;var i=r(2),a=r(1),u=[],s=""},function(e,t){"use strict";function r(e){var t=e.previousConformedValue,r=void 0===t?o:t,i=e.previousPlaceholder,a=void 0===i?o:i,u=e.currentCaretPosition,s=void 0===u?0:u,l=e.conformedValue,f=e.rawValue,c=e.placeholderChar,p=e.placeholder,d=e.indexesOfPipedChars,h=void 0===d?n:d,v=e.caretTrapIndexes,y=void 0===v?n:v;if(0===s||!f.length)return 0;var m=f.length,b=r.length,g=p.length,k=l.length,C=m-b,O=C>0,T=0===b,P=C>1&&!O&&!T;if(P)return s;var x=O&&(r===l||l===p),w=0,S=void 0,_=void 0;if(x)w=s-C;else{var M=l.toLowerCase(),j=f.toLowerCase(),E=j.substr(0,s).split(o),R=E.filter(function(e){return M.indexOf(e)!==-1});_=R[R.length-1];var V=a.substr(0,R.length).split(o).filter(function(e){return e!==c}).length,N=p.substr(0,R.length).split(o).filter(function(e){return e!==c}).length,A=N!==V,B=void 0!==a[R.length-1]&&void 0!==p[R.length-2]&&a[R.length-1]!==c&&a[R.length-1]!==p[R.length-1]&&a[R.length-1]===p[R.length-2];!O&&(A||B)&&V>0&&p.indexOf(_)>-1&&void 0!==f[s]&&(S=!0,_=f[s]);for(var I=h.map(function(e){return M[e]}),F=I.filter(function(e){return e===_}).length,q=R.filter(function(e){return e===_}).length,D=p.substr(0,p.indexOf(c)).split(o).filter(function(e,t){return e===_&&f[t]!==e}).length,L=D+q+F+(S?1:0),W=0,J=0;J<k;J++){var U=M[J];if(w=J+1,U===_&&W++,W>=L)break}}if(O){for(var H=w,Y=w;Y<=g;Y++)if(p[Y]===c&&(H=Y),p[Y]===c||y.indexOf(Y)!==-1||Y===g)return H}else if(S){for(var z=w-1;z>=0;z--)if(l[z]===_||y.indexOf(z)!==-1||0===z)return z}else for(var G=w;G>=0;G--)if(p[G-1]===c||y.indexOf(G)!==-1||0===G)return G}Object.defineProperty(t,"__esModule",{value:!0}),t.default=r;var n=[],o=""},function(e,t,r){"use strict";function n(e){return e&&e.__esModule?e:{default:e}}function o(e){var t={previousConformedValue:void 0,previousPlaceholder:void 0};return{state:t,update:function(r){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:e,o=n.inputElement,l=n.mask,c=n.guide,y=n.pipe,b=n.placeholderChar,g=void 0===b?h.placeholderChar:b,k=n.keepCharPositions,C=void 0!==k&&k,O=n.showMask,T=void 0!==O&&O;if("undefined"==typeof r&&(r=o.value),r!==t.previousConformedValue){("undefined"==typeof l?"undefined":s(l))===m&&void 0!==l.pipe&&void 0!==l.mask&&(y=l.pipe,l=l.mask);var P=void 0,x=void 0;if(l instanceof Array&&(P=(0,d.convertMaskToPlaceholder)(l,g)),l!==!1){var w=a(r),S=o.selectionEnd,_=t.previousConformedValue,M=t.previousPlaceholder,j=void 0;if(("undefined"==typeof l?"undefined":s(l))===h.strFunction){if(x=l(w,{currentCaretPosition:S,previousConformedValue:_,placeholderChar:g}),x===!1)return;var E=(0,d.processCaretTraps)(x),R=E.maskWithoutCaretTraps,V=E.indexes;x=R,j=V,P=(0,d.convertMaskToPlaceholder)(x,g)}else x=l;var N={previousConformedValue:_,guide:c,placeholderChar:g,pipe:y,placeholder:P,currentCaretPosition:S,keepCharPositions:C},A=(0,p.default)(w,x,N),B=A.conformedValue,I=("undefined"==typeof y?"undefined":s(y))===h.strFunction,F={};I&&(F=y(B,u({rawValue:w},N)),F===!1?F={value:_,rejected:!0}:(0,d.isString)(F)&&(F={value:F}));var q=I?F.value:B,D=(0,f.default)({previousConformedValue:_,previousPlaceholder:M,conformedValue:q,placeholder:P,rawValue:w,currentCaretPosition:S,placeholderChar:g,indexesOfPipedChars:F.indexesOfPipedChars,caretTrapIndexes:j}),L=q===P&&0===D,W=T?P:v,J=L?W:q;t.previousConformedValue=J,t.previousPlaceholder=P,o.value!==J&&(o.value=J,i(o,D))}}}}}function i(e,t){document.activeElement===e&&(b?g(function(){return e.setSelectionRange(t,t,y)},0):e.setSelectionRange(t,t,y))}function a(e){if((0,d.isString)(e))return e;if((0,d.isNumber)(e))return String(e);if(void 0===e||null===e)return v;throw new Error("The 'value' provided to Text Mask needs to be a string or a number. The value received was:\n\n "+JSON.stringify(e))}Object.defineProperty(t,"__esModule",{value:!0});var u=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var r=arguments[t];for(var n in r)Object.prototype.hasOwnProperty.call(r,n)&&(e[n]=r[n])}return e},s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.default=o;var l=r(4),f=n(l),c=r(3),p=n(c),d=r(2),h=r(1),v="",y="none",m="object",b="undefined"!=typeof navigator&&/Android/i.test(navigator.userAgent),g="undefined"!=typeof requestAnimationFrame?requestAnimationFrame:setTimeout},function(e,t){"use strict";function r(e){return function(){return e}}var n=function(){};n.thatReturns=r,n.thatReturnsFalse=r(!1),n.thatReturnsTrue=r(!0),n.thatReturnsNull=r(null),n.thatReturnsThis=function(){return this},n.thatReturnsArgument=function(e){return e},e.exports=n},function(e,t,r){"use strict";function n(e,t,r,n,i,a,u,s){if(o(t),!e){var l;if(void 0===t)l=new Error("Minified exception occurred; use the non-minified dev environment for the full error message and additional helpful warnings.");else{var f=[r,n,i,a,u,s],c=0;l=new Error(t.replace(/%s/g,function(){return f[c++]})),l.name="Invariant Violation"}throw l.framesToPop=1,l}}var o=function(e){};e.exports=n},function(e,t,r){"use strict";var n=r(6),o=r(7),i=r(10);e.exports=function(){function e(e,t,r,n,a,u){u!==i&&o(!1,"Calling PropTypes validators directly is not supported by the `prop-types` package. Use PropTypes.checkPropTypes() to call them. Read more at http://fb.me/use-check-prop-types")}function t(){return e}e.isRequired=e;var r={array:e,bool:e,func:e,number:e,object:e,string:e,symbol:e,any:e,arrayOf:t,element:e,instanceOf:t,node:e,objectOf:t,oneOf:t,oneOfType:t,shape:t,exact:t};return r.checkPropTypes=n,r.PropTypes=r,r}},function(e,t,r){"use strict";"function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},e.exports=r(8)()},function(e,t){"use strict";var r="SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED";e.exports=r},function(t,r){t.exports=e}])});

/***/ }),

/***/ 940:
/***/ (function(module, exports, __webpack_require__) {

!function(e,t){ true?module.exports=t():undefined}(this,function(){return function(e){function t(n){if(o[n])return o[n].exports;var i=o[n]={exports:{},id:n,loaded:!1};return e[n].call(i.exports,i,i.exports,t),i.loaded=!0,i.exports}var o={};return t.m=e,t.c=o,t.p="",t(0)}([function(e,t,o){e.exports=o(2)},,function(e,t){"use strict";function o(){function e(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:l,t=e.length;if(e===l||e[0]===y[0]&&1===t)return y.split(l).concat([v]).concat(g.split(l));if(e===k&&M)return y.split(l).concat(["0",k,v]).concat(g.split(l));var o=e[0]===s&&q;o&&(e=e.toString().substr(1));var c=e.lastIndexOf(k),u=c!==-1,a=void 0,b=void 0,h=void 0;if(e.slice(T*-1)===g&&(e=e.slice(0,T*-1)),u&&(M||$)?(a=e.slice(e.slice(0,R)===y?R:0,c),b=e.slice(c+1,t),b=n(b.replace(f,l))):a=e.slice(0,R)===y?e.slice(R):e,P&&("undefined"==typeof P?"undefined":r(P))===p){var S="."===j?"[.]":""+j,w=(a.match(new RegExp(S,"g"))||[]).length;a=a.slice(0,P+w*Z)}return a=a.replace(f,l),E||(a=a.replace(/^0+(0$|[^0])/,"$1")),a=x?i(a,j):a,h=n(a),(u&&M||$===!0)&&(e[c-1]!==k&&h.push(m),h.push(k,m),b&&(("undefined"==typeof L?"undefined":r(L))===p&&(b=b.slice(0,L)),h=h.concat(b)),$===!0&&e[c-1]===k&&h.push(v)),R>0&&(h=y.split(l).concat(h)),o&&(h.length===R&&h.push(v),h=[d].concat(h)),g.length>0&&(h=h.concat(g.split(l))),h}var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},o=t.prefix,y=void 0===o?c:o,b=t.suffix,g=void 0===b?l:b,h=t.includeThousandsSeparator,x=void 0===h||h,S=t.thousandsSeparatorSymbol,j=void 0===S?u:S,w=t.allowDecimal,M=void 0!==w&&w,N=t.decimalSymbol,k=void 0===N?a:N,D=t.decimalLimit,L=void 0===D?2:D,O=t.requireDecimal,$=void 0!==O&&O,_=t.allowNegative,q=void 0!==_&&_,B=t.allowLeadingZeroes,E=void 0!==B&&B,I=t.integerLimit,P=void 0===I?null:I,R=y&&y.length||0,T=g&&g.length||0,Z=j&&j.length||0;return e.instanceOf="createNumberMask",e}function n(e){return e.split(l).map(function(e){return v.test(e)?v:e})}function i(e,t){return e.replace(/\B(?=(\d{3})+(?!\d))/g,t)}Object.defineProperty(t,"__esModule",{value:!0});var r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};t.default=o;var c="$",l="",u=",",a=".",s="-",d=/-/,f=/\D+/g,p="number",v=/\d/,m="[]"}])});

/***/ })

}]);
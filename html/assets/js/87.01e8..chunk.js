(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[87],{

/***/ 645:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(6);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5);
/* harmony import */ var _Firebase__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(11);







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var Likes = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Likes, _Component);

  var _super = _createSuper(Likes);

  function Likes(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Likes);

    _this = _super.call(this, props);
    _this.state = {
      likes: 0,
      active: false
    };
    _this.toggleRates = _this.toggleRates.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default()(_this));
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Likes, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var starCountRef = _Firebase__WEBPACK_IMPORTED_MODULE_9__[/* firebaseConf */ "a"].database().ref("product/" + this.props.item + "/rates");
      starCountRef.once("value").then(function (snapshot) {
        _this2.setState({
          likes: snapshot.val()
        });
      });
      starCountRef.on("child_changed", function (snapshot) {
        _this2.setState({
          likes: snapshot.val()
        });
      });
    }
  }, {
    key: "toggleRates",
    value: function toggleRates(key) {
      var _this3 = this;

      if (!this.state.active) {
        this.setState({
          active: true
        });
        var starCountRef = _Firebase__WEBPACK_IMPORTED_MODULE_9__[/* firebaseConf */ "a"].database().ref("product/" + key + "/rates");
        starCountRef.transaction(function (data) {
          // If users/ada/rank has never been set, currentRank will be `null`.
          if (data >= 0) {
            return data + 1;
          } else {
            return; // Abort the transaction.
          }
        }, function (error, committed, snapshot) {
          if (error) {
            console.log("Transaction failed abnormally!", error);
          } else if (!committed) {
            console.log("We aborted the transaction (because ada already exists).");
          } else {
            _this3.setState({
              likes: snapshot.val()
            });
          }
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        style: {
          cursor: "pointer",
          width: "100%",
          textAlign: "left",
          padding: "0.3rem 0rem 0rem 1rem"
        }
      }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_8__[/* FontAwesomeIcon */ "a"], {
        key: this.props.item + "Like",
        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_7__[/* faHeart */ "d"],
        style: {
          width: "1rem",
          height: "1rem"
        },
        className: "ml-3 mr-2  ".concat(this.state.active ? "color-red" : "color-grey"),
        onClick: function onClick() {
          _this4.toggleRates(_this4.props.item);
        }
      }), this.state.likes);
    }
  }]);

  return Likes;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Likes);

/***/ })

}]);
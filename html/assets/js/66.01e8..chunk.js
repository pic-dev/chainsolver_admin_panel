(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[66],{

/***/ 1460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(52);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(136);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(249);
/* harmony import */ var Util_helmet__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(182);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var politicas = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(politicas, _Component);

  var _super = _createSuper(politicas);

  function politicas(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, politicas);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(politicas, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_5__["Fragment"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Util_helmet__WEBPACK_IMPORTED_MODULE_10__[/* Title */ "a"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("title", null, window.location.pathname.replace("/", "") + " | " + "PIC  - Calculadora de Fletes")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "container2 "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__[/* Row */ "L"], {
        className: "h-100 ",
        id: "link3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        className: "mx-auto text-justify "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "POL\xCDTICA DE PRIVACIDAD"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "La presente Pol\xEDtica de Privacidad establece los t\xE9rminos en que PIC usa y protege la informaci\xF3n que es proporcionada por sus usuarios al momento de utilizar su sitio web. Esta compa\xF1\xEDa est\xE1 comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de informaci\xF3n personal con la cual usted pueda ser identificado, lo hacemos asegurando que s\xF3lo se emplear\xE1 de acuerdo con los t\xE9rminos de este documento. Sin embargo esta Pol\xEDtica de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta p\xE1gina para asegurarse que est\xE1 de acuerdo con dichos cambios."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "Informaci\xF3n que es recogida"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Nuestro sitio web podr\xE1 recoger informaci\xF3n personal por ejemplo: Nombre, informaci\xF3n de contacto como su direcci\xF3n de correo electr\xF3nica e informaci\xF3n demogr\xE1fica. As\xED mismo cuando sea necesario podr\xE1 ser requerida informaci\xF3n espec\xEDfica para procesar alg\xFAn pedido o realizar una entrega o facturaci\xF3n."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "Uso de la informaci\xF3n recogida"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Nuestro sitio web emplea la informaci\xF3n con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios. Es posible que sean enviados correos electr\xF3nicos peri\xF3dicamente a trav\xE9s de nuestro sitio con ofertas especiales, nuevos productos y otra informaci\xF3n publicitaria que consideremos relevante para usted o que pueda brindarle alg\xFAn beneficio, estos correos electr\xF3nicos ser\xE1n enviados a la direcci\xF3n que usted proporcione y podr\xE1n ser cancelados en cualquier momento."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "PIC est\xE1 altamente comprometido para cumplir con el compromiso de mantener su informaci\xF3n segura. Usamos los sistemas m\xE1s avanzados y los actualizamos constantemente para asegurarnos que no exista ning\xFAn acceso no autorizado."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "Cookies"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener informaci\xF3n respecto al tr\xE1fico web, y tambi\xE9n facilita las futuras visitas a una web recurrente. Otra funci\xF3n que tienen las cookies es que con ellas las web pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Nuestro sitio web emplea las cookies para poder identificar las p\xE1ginas que son visitadas y su frecuencia. Esta informaci\xF3n es empleada \xFAnicamente para an\xE1lisis estad\xEDstico y despu\xE9s la informaci\xF3n se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un mejor servicio de los sitios web, est\xE1s no dan acceso a informaci\xF3n de su ordenador ni de usted, a menos de que usted as\xED lo quiera y la proporcione directamente, Usted puede aceptar o negar el uso de cookies, sin embargo la mayor\xEDa de navegadores aceptan cookies autom\xE1ticamente pues sirve para tener un mejor servicio web. Tambi\xE9n usted puede cambiar la configuraci\xF3n de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "Enlaces a Terceros"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Este sitio web pudiera contener enlaces a otros sitios que pudieran ser de su inter\xE9s. Una vez que usted de clic en estos enlaces y abandone nuestra p\xE1gina, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los t\xE9rminos o privacidad ni de la protecci\xF3n de sus datos en esos otros sitios terceros. Dichos sitios est\xE1n sujetos a sus propias pol\xEDticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted est\xE1 de acuerdo con estas."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "font2 text-center pb-2"
      }, "Control de su informaci\xF3n personal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "En cualquier momento usted puede restringir la recopilaci\xF3n o el uso de la informaci\xF3n personal que es proporcionada a nuestro sitio web. Cada vez que se le solicite rellenar un formulario, como el de alta de usuario, puede marcar o desmarcar la opci\xF3n de recibir informaci\xF3n por correo electr\xF3nico. En caso de que haya marcado la opci\xF3n de recibir nuestro bolet\xEDn o publicidad usted puede cancelarla en cualquier momento."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "Esta compa\xF1\xEDa no vender\xE1, ceder\xE1 ni distribuir\xE1 la informaci\xF3n personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "pb-1"
      }, "PIC Se reserva el derecho de cambiar los t\xE9rminos de la presente Pol\xEDtica de Privacidad en cualquier momento.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_7__[/* Colxx */ "a"], {
        className: "text-center pb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], {
        to: "/",
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__[/* Button */ "c"], {
        color: "success",
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER Al Inicio", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })))))));
    }
  }]);

  return politicas;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"])(politicas));

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ })

}]);
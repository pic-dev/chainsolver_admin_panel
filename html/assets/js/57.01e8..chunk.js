(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var isProduction = "production" === 'production';
function warning(condition, message) {
  if (!isProduction) {
    if (condition) {
      return;
    }

    var text = "Warning: " + message;

    if (typeof console !== 'undefined') {
      console.warn(text);
    }

    try {
      throw Error(text);
    } catch (x) {}
  }
}

/* harmony default export */ __webpack_exports__["a"] = (warning);


/***/ }),

/***/ 1393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ ModaPedidoMovil; });

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(17);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Button/Button.js
var Button = __webpack_require__(1426);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Dialog/Dialog.js
var Dialog = __webpack_require__(1482);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogActions/DialogActions.js
var DialogActions = __webpack_require__(1486);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogContent/DialogContent.js
var DialogContent = __webpack_require__(1484);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogTitle/DialogTitle.js
var DialogTitle = __webpack_require__(1483);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Slide/Slide.js
var Slide = __webpack_require__(1480);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(845);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/AppBar/AppBar.js
var AppBar = __webpack_require__(1508);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Toolbar/Toolbar.js
var Toolbar = __webpack_require__(1456);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/IconButton/IconButton.js
var IconButton = __webpack_require__(1443);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Close.js
var Close = __webpack_require__(742);
var Close_default = /*#__PURE__*/__webpack_require__.n(Close);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Table/Table.js
var Table = __webpack_require__(1489);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableBody/TableBody.js
var TableBody = __webpack_require__(1487);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableCell/TableCell.js
var TableCell = __webpack_require__(1455);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableContainer/TableContainer.js
var TableContainer = __webpack_require__(1490);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableHead/TableHead.js
var TableHead = __webpack_require__(1491);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableRow/TableRow.js
var TableRow = __webpack_require__(1488);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Paper/Paper.js
var Paper = __webpack_require__(1440);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/DeleteOutline.js
var DeleteOutline = __webpack_require__(463);
var DeleteOutline_default = /*#__PURE__*/__webpack_require__.n(DeleteOutline);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Tooltip/Tooltip.js
var Tooltip = __webpack_require__(1445);

// CONCATENATED MODULE: ./src/routes/pages/seccionImportadores/tablePedidoMovil.js












var useStyles = Object(makeStyles["a" /* default */])({
  table: {
    minWidth: "auto",
    padding: 0
  }
});

function ccyFormat(num) {
  return "".concat(num.toFixed(2));
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.map(function (_ref) {
    var subTotal = _ref.subTotal;
    return subTotal;
  }).reduce(function (sum, i) {
    return sum + i;
  }, 0);
}

function SpanningTableMovil(props) {
  var classes = useStyles();
  var invoiceTotal = subtotal(props.productos);
  return /*#__PURE__*/react_default.a.createElement(TableContainer["a" /* default */], {
    component: Paper["a" /* default */]
  }, /*#__PURE__*/react_default.a.createElement(Table["a" /* default */], {
    className: classes.table,
    "aria-label": "spanning table"
  }, /*#__PURE__*/react_default.a.createElement(TableHead["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    align: "center",
    colSpan: 6
  }, "Detalle de Pre-Orden")), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Producto"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Qty."), Object.keys(props.JuniorData).length > 0 && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Precio Unitario"), Object.keys(props.JuniorData).length > 0 && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Sub-Total"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Codigo"), props.productos[0].Pagina == undefined && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Pagina"))), /*#__PURE__*/react_default.a.createElement(TableBody["a" /* default */], null, props.productos.map(function (row) {
    return /*#__PURE__*/react_default.a.createElement(Tooltip["a" /* default */], {
      key: row.key + "1",
      title: row.observacion
    }, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      key: row.key
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, row.Producto), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, row.cantidad), Object.keys(props.JuniorData).length > 0 && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      className: "".concat(row.precio > 0 ? "" : "color-red arial")
    }, row.precio > 0 ? row.precio : "P/C"), Object.keys(props.JuniorData).length > 0 && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, row.precio > 0 ? ccyFormat(priceRow(row.precio, row.cantidad)) : "-"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, row.CodigoProducto.length > 0 ? row.CodigoProducto : "-"), props.productos[0].Pagina == undefined && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, row.Pagina), props.tipo == "proceso" && /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      align: "right"
    }, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
      edge: "end",
      color: "inherit",
      onClick: function onClick() {
        return props.remove(row.key);
      },
      "aria-label": "delete"
    }, " ", /*#__PURE__*/react_default.a.createElement(DeleteOutline_default.a, null)))));
  }), invoiceTotal > 0 && /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "text-center",
    colSpan: 3
  }, "Total"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, ccyFormat(invoiceTotal))), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "text-center color-red",
    colSpan: 4
  }, "*p/c - Precio por Confirmar")))));
}
// EXTERNAL MODULE: ./src/routes/pages/seccionImportadores/PreOrdenForm.js
var PreOrdenForm = __webpack_require__(772);

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__(79);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// CONCATENATED MODULE: ./src/routes/pages/seccionImportadores/pedidoMovil.js










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var timer;

var pedidoMovil_Pedido = /*#__PURE__*/function (_Component) {
  inherits_default()(Pedido, _Component);

  var _super = _createSuper(Pedido);

  function Pedido(props) {
    var _this;

    classCallCheck_default()(this, Pedido);

    _this = _super.call(this, props);

    defineProperty_default()(assertThisInitialized_default()(_this), "SaveProduct", /*#__PURE__*/asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee() {
      return regenerator_default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios_default.a.get("https://point.qreport.site/orden").then(function (res) {
                _this.setState({
                  idOrden: res.data[0].orden
                });

                axios_default.a.post("https://point.qreport.site/orden", {
                  norden: res.data[0].orden
                }).then(function () {
                  _this.state.items.map(function (item) {
                    axios_default.a.post("https://point.qreport.site/productos", {
                      cliente: _this.props.JuniorData.length > 0 ? _this.props.JuniorData[0].id : 18,
                      catalogo: item.Catalogo,
                      orden: res.data[0].orden,
                      codigo_producto: item.CodigoProducto,
                      pagina: Number(item.Pagina),
                      producto: item.Producto,
                      cantidad: Number(item.cantidad),
                      precio: item.precio > 0 ? item.precio : 0,
                      precio_compra: item.precioCompra,
                      observacion: item.observacion
                    }).then(function (res) {
                      if (res.data.boolean) {
                        _this.setState({
                          Telefono: "",
                          cliente: "",
                          items: [],
                          isOpenItem: false,
                          isOpenAlert: true
                        });
                      } else {
                        _this.setState({
                          isOpenAlertError: true
                        });

                        timer = setTimeout(function () {
                          _this.setState({
                            isOpenAlertError: false
                          });
                        }, 3000);
                      }
                    }).catch(function (error) {
                      console.log(error);
                    });
                  });
                }).catch(function (error) {
                  console.log(error);
                });
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })));

    _this.state = {
      isOpenAlert: false,
      isOpenAlertError: false,
      isOpenAlertRemove: false,
      Telefono: "",
      cliente: "",
      catalogo: "",
      items: [],
      itemSumary: [],
      isOpenItem: true,
      isOpenItemEdit: false,
      tipoImportador: [],
      tipoImportadorSelected: "",
      idOrden: ""
    };
    _this.handleRemoveProduct = _this.handleRemoveProduct.bind(assertThisInitialized_default()(_this));
    _this.addItem = _this.addItem.bind(assertThisInitialized_default()(_this));
    _this.handleForm = _this.handleForm.bind(assertThisInitialized_default()(_this));
    _this.toggleItem = _this.toggleItem.bind(assertThisInitialized_default()(_this));
    _this.toggleAlert = _this.toggleAlert.bind(assertThisInitialized_default()(_this));
    _this.toggleAlertError = _this.toggleAlertError.bind(assertThisInitialized_default()(_this));
    _this.validate = _this.validate.bind(assertThisInitialized_default()(_this));
    _this.SaveProduct = _this.SaveProduct.bind(assertThisInitialized_default()(_this));
    return _this;
  }

  createClass_default()(Pedido, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      axios_default.a.get("https://point.qreport.site/importador").then(function (res) {
        _this2.setState({
          tipoImportador: res.data
        });
      }).catch(function (error) {
        console.log(error);
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "toggleItem",
    value: function toggleItem() {
      this.setState(function (prevState) {
        return {
          isOpenItem: !prevState.isOpenItem
        };
      });
      this.setState({
        itemSumary: [],
        img: [],
        nombreProducto: "",
        fileType: "",
        items: []
      });
    }
  }, {
    key: "toggleAlert",
    value: function toggleAlert() {
      this.setState(function (prevState) {
        return {
          isOpenAlert: !prevState.isOpenAlert
        };
      });
    }
  }, {
    key: "toggleAlertError",
    value: function toggleAlertError() {
      this.setState(function (prevState) {
        return {
          isOpenAlertError: !prevState.isOpenAlertError
        };
      });
    }
  }, {
    key: "validate",
    value: function validate() {
      var _this3 = this;

      if (this.state.items.length > 0) {
        this.handleForm();
      } else {
        this.setState({
          isOpenAlertRemove: true
        });
        timer = setTimeout(function () {
          _this3.setState({
            isOpenAlertRemove: false
          });
        }, 3000);
      }
    }
  }, {
    key: "handleRemoveProduct",
    value: function handleRemoveProduct(e) {
      var data = this.state.items.filter(function (item) {
        if (e !== item.key) {
          return item;
        }
      });
      this.setState({
        items: data
      });
    }
  }, {
    key: "addItem",
    value: function addItem(e) {
      this.setState(function (prevState) {
        return {
          items: prevState.items.concat(e),
          itemSumary: prevState.itemSumary.concat(e),
          isOpenItem: false
        };
      });
    }
  }, {
    key: "handleForm",
    value: function handleForm() {
      var _this4 = this;

      if (this.state.items.length > 0) {
        this.SaveProduct();
      } else {
        this.setState({
          isOpenAlertRemove: true
        });
        timer = setTimeout(function () {
          _this4.setState({
            isOpenAlertRemove: false
          });
        }, 3000);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      console.log(this.props.tipo);
      return /*#__PURE__*/react_default.a.createElement(react["Fragment"], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "NoPadding",
        xs: "12",
        md: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%",
          height: "2.5rem",
          borderRadius: "0.4rem",
          textTransform: "uppercase",
          fontSize: "0.9rem"
        },
        className: "text-white text-center arial mb-2 p-2 bg-blue-2"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "Realizar Pre Orden en Linea"), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          backgroundColor: "white",
          borderRadius: "50%",
          fontSize: "1rem",
          padding: ".2rem",
          fontWeight: "bold",
          cursor: "pointer"
        },
        className: "mr-2 ml-2 float-right color-orange ".concat(this.state.isOpenItem ? "simple-icon-arrow-up" : "simple-icon-arrow-down")
      })), this.props.catalogo.statusCp !== "Ok" && this.props.JuniorData.length > 0 && this.props.tipo === "catalogos" && /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%",
          borderRadius: "0.4rem",
          textTransform: "uppercase",
          fontSize: "1rem"
        },
        className: "text-center alert alert-danger  arial mb-2 p-2 alert alert-red"
      }, /*#__PURE__*/react_default.a.createElement("span", null, "catalogo con precios - por confirmar"), /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          borderRadius: "50%",
          fontSize: "1rem",
          padding: ".2rem",
          fontWeight: "bold",
          cursor: "pointer"
        },
        className: "mr-2 ml-2 simple-icon-info"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["m" /* Collapse */], {
        className: "p-2",
        isOpen: Object.keys(this.props.articulo).length > 0 || this.props.tipo === "catalogos"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "NoPadding mx-auto pb-1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto m-2 p-2",
        style: {
          borderRadius: ".5em",
          border: "1px solid rgba(0, 0, 0, 0.18)"
        }
      }, /*#__PURE__*/react_default.a.createElement(PreOrdenForm["a" /* default */], {
        tipo: this.props.tipo,
        addItem: this.addItem,
        JuniorData: this.props.JuniorData,
        catalogo: this.props.catalogo,
        articulo: this.props.articulo,
        setArticuloSelected: this.props.setArticuloSelected
      }))))), this.state.items.length > 0 && /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-1"
      }, " ", /*#__PURE__*/react_default.a.createElement("hr", null), /*#__PURE__*/react_default.a.createElement(SpanningTableMovil, {
        JuniorData: this.props.JuniorData,
        tipo: "proceso",
        remove: this.handleRemoveProduct,
        productos: this.state.items
      })), this.state.items.length > 0 && /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        style: {
          display: "flex",
          placeContent: "space-evenly"
        },
        className: "mx-auto m-2 p-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        className: "text-center btn-style-Green btn btn-success btn-sm",
        onClick: this.validate
      }, "GENERAR PRE-ORDEN"), " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        className: "text-center btn-style btn btn-success btn-sm",
        onClick: function onClick() {
          return _this5.props.toggle(false);
        }
      }, "VOLVER A CATALOGO"), " ")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "info",
        isOpen: this.state.isOpenAlert,
        toggle: this.toggleAlert
      }, "\xA1Pre Orden Agregada Exitosamente!, Su ID:", this.state.idOrden), this.props.JuniorData.length == 0 && /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "danger",
        isOpen: this.state.isOpenAlert,
        toggle: this.toggleAlert
      }, "IMPORTANTE: Debera contactar a un asesor PIC Cargo para formalizar su orden.", /*#__PURE__*/react_default.a.createElement("a", {
        href: "https://api.whatsapp.com/send?phone=51920301745",
        rel: "noopener noreferrer",
        target: "_blank"
      }, "+51920301745")), this.state.isOpenAlert && this.state.itemSumary.length > 0 && /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-1"
      }, " ", /*#__PURE__*/react_default.a.createElement("hr", null), /*#__PURE__*/react_default.a.createElement(SpanningTableMovil, {
        tipo: "completada",
        JuniorData: this.props.JuniorData,
        remove: this.handleRemoveProduct,
        productos: this.state.itemSumary
      }))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "info",
        isOpen: this.state.isOpenAlertError,
        toggle: this.toggleAlertError
      }, "Hubo un error al cargar la Orden, intente nuevamente")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "danger",
        isOpen: this.state.isOpenAlertRemove,
        toggle: this.toggleAlert
      }, "\xA1Hay campos que requieren verificaci\xF3n!"))));
    }
  }]);

  return Pedido;
}(react["Component"]);

/* harmony default export */ var pedidoMovil = (/*#__PURE__*/react_default.a.memo(pedidoMovil_Pedido, function (prevProps, nextProps) {
  return prevProps.articulo === nextProps.articulo;
}));
// CONCATENATED MODULE: ./src/util/Modals/modaPedidoMovil.js
















var modaPedidoMovil_Transition = /*#__PURE__*/react_default.a.forwardRef(function Transition(props, ref) {
  return /*#__PURE__*/react_default.a.createElement(Slide["a" /* default */], extends_default()({
    direction: "up",
    in: props.open,
    ref: ref
  }, props));
});
var modaPedidoMovil_useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  var _img;

  return {
    root: {
      "& > *": {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-around",
        overflow: "hidden",
        backgroundColor: theme.palette.background.paper
      },
      TextField: {
        margin: theme.spacing(1),
        width: "25ch"
      },
      appBar: {
        position: "relative"
      },
      title: {
        marginLeft: theme.spacing(2),
        flex: 1
      },
      form: {
        width: "100%",
        // Fix IE 11 issue.
        marginTop: theme.spacing(3)
      },
      buttons: {
        display: "flex",
        justifyContent: "flex-end"
      },
      button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
      },
      "& img": (_img = {
        maxWidth: "min-content",
        maxHeight: "35rem",
        width: "100%"
      }, defineProperty_default()(_img, "width", "-moz-available"), defineProperty_default()(_img, "width", "-webkit-fill-available"), defineProperty_default()(_img, "width", "fill-available"), defineProperty_default()(_img, "transition", theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })), defineProperty_default()(_img, theme.breakpoints.up("xs"), {
        maxHeight: "25rem"
      }), _img)
    },
    DialogContent: {
      padding: theme.spacing(3, 1)
    }
  };
});
function ModaPedidoMovil(_ref) {
  var toggle = _ref.toggle,
      open = _ref.open,
      JuniorData = _ref.JuniorData,
      catalogo = _ref.catalogo,
      articulo = _ref.articulo,
      setArticuloSelected = _ref.setArticuloSelected,
      tipo = _ref.tipo;
  var classes = modaPedidoMovil_useStyles();
  return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(Dialog["a" /* default */], {
    open: open,
    TransitionComponent: modaPedidoMovil_Transition,
    fullWidth: true,
    fullScreen: Object(Utils["m" /* isMovil */])(),
    maxWidth: "lg",
    keepMounted: true,
    onClose: function onClose() {
      return toggle(!open);
    },
    "aria-labelledby": "alert-dialog-slide-title",
    "aria-describedby": "alert-dialog-slide-description"
  }, Object(Utils["m" /* isMovil */])() && /*#__PURE__*/react_default.a.createElement(AppBar["a" /* default */], {
    className: classes.appBar
  }, /*#__PURE__*/react_default.a.createElement(Toolbar["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    edge: "start",
    color: "inherit",
    onClick: function onClick() {
      return toggle(!open);
    },
    "aria-label": "close"
  }, /*#__PURE__*/react_default.a.createElement(Close_default.a, null)))), /*#__PURE__*/react_default.a.createElement(DialogTitle["a" /* default */], {
    id: "alert-dialog-slide-title"
  }, "Pedido"), /*#__PURE__*/react_default.a.createElement(DialogContent["a" /* default */], {
    className: classes.DialogContent
  }, /*#__PURE__*/react_default.a.createElement(pedidoMovil, {
    JuniorData: JuniorData,
    catalogo: catalogo,
    articulo: articulo,
    setArticuloSelected: setArticuloSelected,
    tipo: tipo,
    toggle: toggle
  }), /*#__PURE__*/react_default.a.createElement(DialogActions["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    onClick: function onClick() {
      return toggle(!open);
    },
    color: "primary"
  }, "Volver")))));
}

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function toVal(mix) {
	var k, y, str='';

	if (typeof mix === 'string' || typeof mix === 'number') {
		str += mix;
	} else if (typeof mix === 'object') {
		if (Array.isArray(mix)) {
			for (k=0; k < mix.length; k++) {
				if (mix[k]) {
					if (y = toVal(mix[k])) {
						str && (str += ' ');
						str += y;
					}
				}
			}
		} else {
			for (k in mix) {
				if (mix[k]) {
					str && (str += ' ');
					str += k;
				}
			}
		}
	}

	return str;
}

/* harmony default export */ __webpack_exports__["a"] = (function () {
	var i=0, tmp, x, str='';
	while (i < arguments.length) {
		if (tmp = arguments[i++]) {
			if (x = toVal(tmp)) {
				str && (str += ' ');
				str += x
			}
		}
	}
	return str;
});


/***/ }),

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (true) {
  module.exports = __webpack_require__(156);
} else {}


/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?
Symbol.for("react.suspense_list"):60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.block"):60121,w=b?Symbol.for("react.fundamental"):60117,x=b?Symbol.for("react.responder"):60118,y=b?Symbol.for("react.scope"):60119;
function z(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case t:case r:case h:return a;default:return u}}case d:return u}}}function A(a){return z(a)===m}exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;exports.Fragment=e;exports.Lazy=t;exports.Memo=r;exports.Portal=d;
exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;exports.isAsyncMode=function(a){return A(a)||z(a)===l};exports.isConcurrentMode=A;exports.isContextConsumer=function(a){return z(a)===k};exports.isContextProvider=function(a){return z(a)===h};exports.isElement=function(a){return"object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return z(a)===n};exports.isFragment=function(a){return z(a)===e};exports.isLazy=function(a){return z(a)===t};
exports.isMemo=function(a){return z(a)===r};exports.isPortal=function(a){return z(a)===d};exports.isProfiler=function(a){return z(a)===g};exports.isStrictMode=function(a){return z(a)===f};exports.isSuspense=function(a){return z(a)===p};
exports.isValidElementType=function(a){return"string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===w||a.$$typeof===x||a.$$typeof===y||a.$$typeof===v)};exports.typeOf=z;


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var reactIs = __webpack_require__(151);

/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
  childContextTypes: true,
  contextType: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDefaultProps: true,
  getDerivedStateFromError: true,
  getDerivedStateFromProps: true,
  mixins: true,
  propTypes: true,
  type: true
};
var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true
};
var FORWARD_REF_STATICS = {
  '$$typeof': true,
  render: true,
  defaultProps: true,
  displayName: true,
  propTypes: true
};
var MEMO_STATICS = {
  '$$typeof': true,
  compare: true,
  defaultProps: true,
  displayName: true,
  propTypes: true,
  type: true
};
var TYPE_STATICS = {};
TYPE_STATICS[reactIs.ForwardRef] = FORWARD_REF_STATICS;
TYPE_STATICS[reactIs.Memo] = MEMO_STATICS;

function getStatics(component) {
  // React v16.11 and below
  if (reactIs.isMemo(component)) {
    return MEMO_STATICS;
  } // React v16.12 and above


  return TYPE_STATICS[component['$$typeof']] || REACT_STATICS;
}

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = Object.prototype;
function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components
    if (objectPrototype) {
      var inheritedComponent = getPrototypeOf(sourceComponent);

      if (inheritedComponent && inheritedComponent !== objectPrototype) {
        hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
      }
    }

    var keys = getOwnPropertyNames(sourceComponent);

    if (getOwnPropertySymbols) {
      keys = keys.concat(getOwnPropertySymbols(sourceComponent));
    }

    var targetStatics = getStatics(targetComponent);
    var sourceStatics = getStatics(sourceComponent);

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i];

      if (!KNOWN_STATICS[key] && !(blacklist && blacklist[key]) && !(sourceStatics && sourceStatics[key]) && !(targetStatics && targetStatics[key])) {
        var descriptor = getOwnPropertyDescriptor(sourceComponent, key);

        try {
          // Avoid failures from read-only properties
          defineProperty(targetComponent, key, descriptor);
        } catch (e) {}
      }
    }
  }

  return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isBrowser */
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isBrowser = (typeof window === "undefined" ? "undefined" : _typeof(window)) === "object" && (typeof document === "undefined" ? "undefined" : _typeof(document)) === 'object' && document.nodeType === 9;

/* harmony default export */ __webpack_exports__["a"] = (isBrowser);


/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);



var now = Date.now();
var fnValuesNs = "fnValues" + now;
var fnRuleNs = "fnStyle" + ++now;

var functionPlugin = function functionPlugin() {
  return {
    onCreateRule: function onCreateRule(name, decl, options) {
      if (typeof decl !== 'function') return null;
      var rule = Object(jss__WEBPACK_IMPORTED_MODULE_0__[/* createRule */ "d"])(name, {}, options);
      rule[fnRuleNs] = decl;
      return rule;
    },
    onProcessStyle: function onProcessStyle(style, rule) {
      // We need to extract function values from the declaration, so that we can keep core unaware of them.
      // We need to do that only once.
      // We don't need to extract functions on each style update, since this can happen only once.
      // We don't support function values inside of function rules.
      if (fnValuesNs in rule || fnRuleNs in rule) return style;
      var fnValues = {};

      for (var prop in style) {
        var value = style[prop];
        if (typeof value !== 'function') continue;
        delete style[prop];
        fnValues[prop] = value;
      } // $FlowFixMe[prop-missing]


      rule[fnValuesNs] = fnValues;
      return style;
    },
    onUpdate: function onUpdate(data, rule, sheet, options) {
      var styleRule = rule; // $FlowFixMe[prop-missing]

      var fnRule = styleRule[fnRuleNs]; // If we have a style function, the entire rule is dynamic and style object
      // will be returned from that function.

      if (fnRule) {
        // Empty object will remove all currently defined props
        // in case function rule returns a falsy value.
        styleRule.style = fnRule(data) || {};

        if (false) { var prop; }
      } // $FlowFixMe[prop-missing]


      var fnValues = styleRule[fnValuesNs]; // If we have a fn values map, it is a rule with function values.

      if (fnValues) {
        for (var _prop in fnValues) {
          styleRule.prop(_prop, fnValues[_prop](data), options);
        }
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["a"] = (functionPlugin);


/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



var at = '@global';
var atPrefix = '@global ';

var GlobalContainerRule =
/*#__PURE__*/
function () {
  function GlobalContainerRule(key, styles, options) {
    this.type = 'global';
    this.at = at;
    this.rules = void 0;
    this.options = void 0;
    this.key = void 0;
    this.isProcessed = false;
    this.key = key;
    this.options = options;
    this.rules = new jss__WEBPACK_IMPORTED_MODULE_1__[/* RuleList */ "a"](Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));

    for (var selector in styles) {
      this.rules.add(selector, styles[selector]);
    }

    this.rules.process();
  }
  /**
   * Get a rule.
   */


  var _proto = GlobalContainerRule.prototype;

  _proto.getRule = function getRule(name) {
    return this.rules.get(name);
  }
  /**
   * Create and register rule, run plugins.
   */
  ;

  _proto.addRule = function addRule(name, style, options) {
    var rule = this.rules.add(name, style, options);
    if (rule) this.options.jss.plugins.onProcessRule(rule);
    return rule;
  }
  /**
   * Get index of a rule.
   */
  ;

  _proto.indexOf = function indexOf(rule) {
    return this.rules.indexOf(rule);
  }
  /**
   * Generates a CSS string.
   */
  ;

  _proto.toString = function toString() {
    return this.rules.toString();
  };

  return GlobalContainerRule;
}();

var GlobalPrefixedRule =
/*#__PURE__*/
function () {
  function GlobalPrefixedRule(key, style, options) {
    this.type = 'global';
    this.at = at;
    this.options = void 0;
    this.rule = void 0;
    this.isProcessed = false;
    this.key = void 0;
    this.key = key;
    this.options = options;
    var selector = key.substr(atPrefix.length);
    this.rule = options.jss.createRule(selector, style, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));
  }

  var _proto2 = GlobalPrefixedRule.prototype;

  _proto2.toString = function toString(options) {
    return this.rule ? this.rule.toString(options) : '';
  };

  return GlobalPrefixedRule;
}();

var separatorRegExp = /\s*,\s*/g;

function addScope(selector, scope) {
  var parts = selector.split(separatorRegExp);
  var scoped = '';

  for (var i = 0; i < parts.length; i++) {
    scoped += scope + " " + parts[i].trim();
    if (parts[i + 1]) scoped += ', ';
  }

  return scoped;
}

function handleNestedGlobalContainerRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;
  var rules = style ? style[at] : null;
  if (!rules) return;

  for (var name in rules) {
    sheet.addRule(name, rules[name], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: addScope(name, rule.selector)
    }));
  }

  delete style[at];
}

function handlePrefixedGlobalRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;

  for (var prop in style) {
    if (prop[0] !== '@' || prop.substr(0, at.length) !== at) continue;
    var selector = addScope(prop.substr(at.length), rule.selector);
    sheet.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: selector
    }));
    delete style[prop];
  }
}
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */


function jssGlobal() {
  function onCreateRule(name, styles, options) {
    if (!name) return null;

    if (name === at) {
      return new GlobalContainerRule(name, styles, options);
    }

    if (name[0] === '@' && name.substr(0, atPrefix.length) === atPrefix) {
      return new GlobalPrefixedRule(name, styles, options);
    }

    var parent = options.parent;

    if (parent) {
      if (parent.type === 'global' || parent.options.parent && parent.options.parent.type === 'global') {
        options.scoped = false;
      }
    }

    if (options.scoped === false) {
      options.selector = name;
    }

    return null;
  }

  function onProcessRule(rule, sheet) {
    if (rule.type !== 'style' || !sheet) return;
    handleNestedGlobalContainerRule(rule, sheet);
    handlePrefixedGlobalRule(rule, sheet);
  }

  return {
    onCreateRule: onCreateRule,
    onProcessRule: onProcessRule
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssGlobal);


/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);



var separatorRegExp = /\s*,\s*/g;
var parentRegExp = /&/g;
var refRegExp = /\$([\w-]+)/g;
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */

function jssNested() {
  // Get a function to be used for $ref replacement.
  function getReplaceRef(container, sheet) {
    return function (match, key) {
      var rule = container.getRule(key) || sheet && sheet.getRule(key);

      if (rule) {
        rule = rule;
        return rule.selector;
      }

       false ? undefined : void 0;
      return key;
    };
  }

  function replaceParentRefs(nestedProp, parentProp) {
    var parentSelectors = parentProp.split(separatorRegExp);
    var nestedSelectors = nestedProp.split(separatorRegExp);
    var result = '';

    for (var i = 0; i < parentSelectors.length; i++) {
      var parent = parentSelectors[i];

      for (var j = 0; j < nestedSelectors.length; j++) {
        var nested = nestedSelectors[j];
        if (result) result += ', '; // Replace all & by the parent or prefix & with the parent.

        result += nested.indexOf('&') !== -1 ? nested.replace(parentRegExp, parent) : parent + " " + nested;
      }
    }

    return result;
  }

  function getOptions(rule, container, prevOptions) {
    // Options has been already created, now we only increase index.
    if (prevOptions) return Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, prevOptions, {
      index: prevOptions.index + 1 // $FlowFixMe[prop-missing]

    });
    var nestingLevel = rule.options.nestingLevel;
    nestingLevel = nestingLevel === undefined ? 1 : nestingLevel + 1;

    var options = Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, rule.options, {
      nestingLevel: nestingLevel,
      index: container.indexOf(rule) + 1 // We don't need the parent name to be set options for chlid.

    });

    delete options.name;
    return options;
  }

  function onProcessStyle(style, rule, sheet) {
    if (rule.type !== 'style') return style;
    var styleRule = rule;
    var container = styleRule.options.parent;
    var options;
    var replaceRef;

    for (var prop in style) {
      var isNested = prop.indexOf('&') !== -1;
      var isNestedConditional = prop[0] === '@';
      if (!isNested && !isNestedConditional) continue;
      options = getOptions(styleRule, container, options);

      if (isNested) {
        var selector = replaceParentRefs(prop, styleRule.selector); // Lazily create the ref replacer function just once for
        // all nested rules within the sheet.

        if (!replaceRef) replaceRef = getReplaceRef(container, sheet); // Replace all $refs.

        selector = selector.replace(refRegExp, replaceRef);
        container.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
          selector: selector
        }));
      } else if (isNestedConditional) {
        // Place conditional right after the parent rule to ensure right ordering.
        container.addRule(prop, {}, options) // Flow expects more options but they aren't required
        // And flow doesn't know this will always be a StyleRule which has the addRule method
        // $FlowFixMe[incompatible-use]
        // $FlowFixMe[prop-missing]
        .addRule(styleRule.key, style[prop], {
          selector: styleRule.selector
        });
      }

      delete style[prop];
    }

    return style;
  }

  return {
    onProcessStyle: onProcessStyle
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssNested);


/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);


var px = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.px : 'px';
var ms = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.ms : 'ms';
var percent = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.percent : '%';
/**
 * Generated jss-plugin-default-unit CSS property units
 *
 * @type object
 */

var defaultUnits = {
  // Animation properties
  'animation-delay': ms,
  'animation-duration': ms,
  // Background properties
  'background-position': px,
  'background-position-x': px,
  'background-position-y': px,
  'background-size': px,
  // Border Properties
  border: px,
  'border-bottom': px,
  'border-bottom-left-radius': px,
  'border-bottom-right-radius': px,
  'border-bottom-width': px,
  'border-left': px,
  'border-left-width': px,
  'border-radius': px,
  'border-right': px,
  'border-right-width': px,
  'border-top': px,
  'border-top-left-radius': px,
  'border-top-right-radius': px,
  'border-top-width': px,
  'border-width': px,
  'border-block': px,
  'border-block-end': px,
  'border-block-end-width': px,
  'border-block-start': px,
  'border-block-start-width': px,
  'border-block-width': px,
  'border-inline': px,
  'border-inline-end': px,
  'border-inline-end-width': px,
  'border-inline-start': px,
  'border-inline-start-width': px,
  'border-inline-width': px,
  'border-start-start-radius': px,
  'border-start-end-radius': px,
  'border-end-start-radius': px,
  'border-end-end-radius': px,
  // Margin properties
  margin: px,
  'margin-bottom': px,
  'margin-left': px,
  'margin-right': px,
  'margin-top': px,
  'margin-block': px,
  'margin-block-end': px,
  'margin-block-start': px,
  'margin-inline': px,
  'margin-inline-end': px,
  'margin-inline-start': px,
  // Padding properties
  padding: px,
  'padding-bottom': px,
  'padding-left': px,
  'padding-right': px,
  'padding-top': px,
  'padding-block': px,
  'padding-block-end': px,
  'padding-block-start': px,
  'padding-inline': px,
  'padding-inline-end': px,
  'padding-inline-start': px,
  // Mask properties
  'mask-position-x': px,
  'mask-position-y': px,
  'mask-size': px,
  // Width and height properties
  height: px,
  width: px,
  'min-height': px,
  'max-height': px,
  'min-width': px,
  'max-width': px,
  // Position properties
  bottom: px,
  left: px,
  top: px,
  right: px,
  inset: px,
  'inset-block': px,
  'inset-block-end': px,
  'inset-block-start': px,
  'inset-inline': px,
  'inset-inline-end': px,
  'inset-inline-start': px,
  // Shadow properties
  'box-shadow': px,
  'text-shadow': px,
  // Column properties
  'column-gap': px,
  'column-rule': px,
  'column-rule-width': px,
  'column-width': px,
  // Font and text properties
  'font-size': px,
  'font-size-delta': px,
  'letter-spacing': px,
  'text-decoration-thickness': px,
  'text-indent': px,
  'text-stroke': px,
  'text-stroke-width': px,
  'word-spacing': px,
  // Motion properties
  motion: px,
  'motion-offset': px,
  // Outline properties
  outline: px,
  'outline-offset': px,
  'outline-width': px,
  // Perspective properties
  perspective: px,
  'perspective-origin-x': percent,
  'perspective-origin-y': percent,
  // Transform properties
  'transform-origin': percent,
  'transform-origin-x': percent,
  'transform-origin-y': percent,
  'transform-origin-z': percent,
  // Transition properties
  'transition-delay': ms,
  'transition-duration': ms,
  // Alignment properties
  'vertical-align': px,
  'flex-basis': px,
  // Some random properties
  'shape-margin': px,
  size: px,
  gap: px,
  // Grid properties
  grid: px,
  'grid-gap': px,
  'row-gap': px,
  'grid-row-gap': px,
  'grid-column-gap': px,
  'grid-template-rows': px,
  'grid-template-columns': px,
  'grid-auto-rows': px,
  'grid-auto-columns': px,
  // Not existing properties.
  // Used to avoid issues with jss-plugin-expand integration.
  'box-shadow-x': px,
  'box-shadow-y': px,
  'box-shadow-blur': px,
  'box-shadow-spread': px,
  'font-line-height': px,
  'text-shadow-x': px,
  'text-shadow-y': px,
  'text-shadow-blur': px
};

/**
 * Clones the object and adds a camel cased property version.
 */
function addCamelCasedVersion(obj) {
  var regExp = /(-[a-z])/g;

  var replace = function replace(str) {
    return str[1].toUpperCase();
  };

  var newObj = {};

  for (var _key in obj) {
    newObj[_key] = obj[_key];
    newObj[_key.replace(regExp, replace)] = obj[_key];
  }

  return newObj;
}

var units = addCamelCasedVersion(defaultUnits);
/**
 * Recursive deep style passing function
 */

function iterate(prop, value, options) {
  if (value == null) return value;

  if (Array.isArray(value)) {
    for (var i = 0; i < value.length; i++) {
      value[i] = iterate(prop, value[i], options);
    }
  } else if (typeof value === 'object') {
    if (prop === 'fallbacks') {
      for (var innerProp in value) {
        value[innerProp] = iterate(innerProp, value[innerProp], options);
      }
    } else {
      for (var _innerProp in value) {
        value[_innerProp] = iterate(prop + "-" + _innerProp, value[_innerProp], options);
      }
    }
  } else if (typeof value === 'number' && !Number.isNaN(value)) {
    var unit = options[prop] || units[prop]; // Add the unit if available, except for the special case of 0px.

    if (unit && !(value === 0 && unit === px)) {
      return typeof unit === 'function' ? unit(value).toString() : "" + value + unit;
    }

    return value.toString();
  }

  return value;
}
/**
 * Add unit to numeric values.
 */


function defaultUnit(options) {
  if (options === void 0) {
    options = {};
  }

  var camelCasedOptions = addCamelCasedVersion(options);

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;

    for (var prop in style) {
      style[prop] = iterate(prop, style[prop], camelCasedOptions);
    }

    return style;
  }

  function onChangeValue(value, prop) {
    return iterate(prop, value, camelCasedOptions);
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (defaultUnit);


/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var css_vendor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(201);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



/**
 * Add vendor prefix to a property name when needed.
 *
 * @api public
 */

function jssVendorPrefixer() {
  function onProcessRule(rule) {
    if (rule.type === 'keyframes') {
      var atRule = rule;
      atRule.at = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedKeyframes */ "a"])(atRule.at);
    }
  }

  function prefixStyle(style) {
    for (var prop in style) {
      var value = style[prop];

      if (prop === 'fallbacks' && Array.isArray(value)) {
        style[prop] = value.map(prefixStyle);
        continue;
      }

      var changeProp = false;
      var supportedProp = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedProperty */ "b"])(prop);
      if (supportedProp && supportedProp !== prop) changeProp = true;
      var changeValue = false;
      var supportedValue$1 = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(supportedProp, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value));
      if (supportedValue$1 && supportedValue$1 !== value) changeValue = true;

      if (changeProp || changeValue) {
        if (changeProp) delete style[prop];
        style[supportedProp || prop] = supportedValue$1 || value;
      }
    }

    return style;
  }

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;
    return prefixStyle(style);
  }

  function onChangeValue(value, prop) {
    return Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(prop, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value)) || value;
  }

  return {
    onProcessRule: onProcessRule,
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssVendorPrefixer);


/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Sort props by length.
 */
function jssPropsSort() {
  var sort = function sort(prop0, prop1) {
    if (prop0.length === prop1.length) {
      return prop0 > prop1 ? 1 : -1;
    }

    return prop0.length - prop1.length;
  };

  return {
    onProcessStyle: function onProcessStyle(style, rule) {
      if (rule.type !== 'style') return style;
      var newStyle = {};
      var props = Object.keys(style).sort(sort);

      for (var i = 0; i < props.length; i++) {
        newStyle[props[i]] = style[props[i]];
      }

      return newStyle;
    }
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssPropsSort);


/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/hyphenate-style-name/index.js
/* eslint-disable no-var, prefer-template */
var uppercasePattern = /[A-Z]/g
var msPattern = /^ms-/
var cache = {}

function toHyphenLower(match) {
  return '-' + match.toLowerCase()
}

function hyphenateStyleName(name) {
  if (cache.hasOwnProperty(name)) {
    return cache[name]
  }

  var hName = name.replace(uppercasePattern, toHyphenLower)
  return (cache[name] = msPattern.test(hName) ? '-' + hName : hName)
}

/* harmony default export */ var hyphenate_style_name = (hyphenateStyleName);

// CONCATENATED MODULE: ./node_modules/jss-plugin-camel-case/dist/jss-plugin-camel-case.esm.js


/**
 * Convert camel cased property names to dash separated.
 *
 * @param {Object} style
 * @return {Object}
 */

function convertCase(style) {
  var converted = {};

  for (var prop in style) {
    var key = prop.indexOf('--') === 0 ? prop : hyphenate_style_name(prop);
    converted[key] = style[prop];
  }

  if (style.fallbacks) {
    if (Array.isArray(style.fallbacks)) converted.fallbacks = style.fallbacks.map(convertCase);else converted.fallbacks = convertCase(style.fallbacks);
  }

  return converted;
}
/**
 * Allow camel cased property names by converting them back to dasherized.
 *
 * @param {Rule} rule
 */


function camelCase() {
  function onProcessStyle(style) {
    if (Array.isArray(style)) {
      // Handle rules like @font-face, which can have multiple styles in an array
      for (var index = 0; index < style.length; index++) {
        style[index] = convertCase(style[index]);
      }

      return style;
    }

    return convertCase(style);
  }

  function onChangeValue(value, prop, rule) {
    if (prop.indexOf('--') === 0) {
      return value;
    }

    var hyphenatedProp = hyphenate_style_name(prop); // There was no camel case in place

    if (prop === hyphenatedProp) return value;
    rule.prop(hyphenatedProp, value); // Core will ignore that property value we set the proper one above.

    return null;
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ var jss_plugin_camel_case_esm = __webpack_exports__["a"] = (camelCase);


/***/ }),

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Junior; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(845);
/* harmony import */ var _material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1470);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1451);
/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1471);
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1472);
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1536);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1426);
/* harmony import */ var _material_ui_icons_AddOutlined__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(650);
/* harmony import */ var _material_ui_icons_AddOutlined__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AddOutlined__WEBPACK_IMPORTED_MODULE_9__);










var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(function (theme) {
  return {
    root: {
      display: "flex",
      background: "white"
    },
    content: {
      flexGrow: 1,
      height: "100vh",
      overflow: "auto",
      marginTop: "50px"
    },
    form: {
      width: "100%",
      // Fix IE 11 issue.
      marginTop: theme.spacing(3)
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      backgroundColor: "#1470b9",
      color: "white"
    }
  };
});
function Junior(props) {
  var _React$createElement;

  var classes = useStyles();

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    var _e$target$elements = e.target.elements,
        Pagina = _e$target$elements.Pagina,
        Producto = _e$target$elements.Producto,
        CodigoProducto = _e$target$elements.CodigoProducto,
        CantidadProducto = _e$target$elements.CantidadProducto,
        precio = _e$target$elements.precio,
        observacion = _e$target$elements.observacion;
    var newItem = {
      Catalogo: props.catalogo.id,
      Pagina: Pagina ? Pagina.value : "",
      Producto: Producto.value,
      CodigoProducto: CodigoProducto && CodigoProducto.value ? CodigoProducto.value : "",
      cantidad: Number(CantidadProducto.value),
      precio: precio ? Number(precio.value) : 0,
      subTotal: precio ? Number(precio.value) * Number(CantidadProducto.value) : 0,
      key: Date.now(),
      precioCompra: Object.entries(props.articulo).length > 0 ? props.articulo.precio_compra : 0,
      observacion: observacion.value
    };
    props.setArticuloSelected([]);
    props.addItem(newItem);

    if (Pagina) {
      document.getElementById("Pagina").value = "";
    }

    if (CodigoProducto) {
      document.getElementById("CodigoProducto").value = "";
    }

    document.getElementById("Producto").value = "";
    document.getElementById("CantidadProducto").value = "";
    document.getElementById("observacion").value = "";

    if (precio) {
      document.getElementById("precio").value = "";
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_CssBaseline__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], {
    maxWidth: "lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    component: "h6",
    variant: "h6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_AddOutlined__WEBPACK_IMPORTED_MODULE_9___default.a, null), " Agregar Productos"), props.tipo === "catalogos" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    className: classes.form,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    container: true,
    spacing: 2
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    autoComplete: "Pagina",
    name: "Pagina",
    required: true,
    fullWidth: true,
    id: "Pagina",
    label: "Pagina",
    autoFocus: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 3
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    autoComplete: "Producto",
    name: "Producto",
    required: true,
    fullWidth: true,
    id: "Producto",
    label: "Producto"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 3
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    fullWidth: true,
    id: "CodigoProducto",
    label: "Codigo",
    name: "CodigoProducto",
    autoComplete: "Codigo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    required: true,
    fullWidth: true,
    id: "CantidadProducto",
    label: "Qty",
    name: "CantidadProducto",
    autoComplete: "Cantidad"
  })), props.catalogo.statusCp == "Ok" && props.JuniorData.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    fullWidth: true,
    id: "precio",
    label: "precio",
    name: "precio",
    autoComplete: "precio"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 12,
    sm: 12
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    id: "observacion",
    name: "observacion",
    label: "Observaci\xF3n",
    multiline: true,
    fullWidth: true,
    rowsMax: 4
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], {
    type: "submit",
    fullWidth: true,
    variant: "contained",
    className: classes.submit
  }, "Agregar")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    className: classes.form,
    onSubmit: handleSubmit
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    container: true,
    spacing: 2
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 4
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    autoComplete: "Producto",
    name: "Producto",
    value: props.articulo ? props.articulo.name : "",
    fullWidth: true,
    id: "Producto",
    helperText: "Producto",
    disabled: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 4
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], (_React$createElement = {
    fullWidth: true,
    id: "CodigoProducto",
    helperText: "Codigo",
    value: props.articulo ? props.articulo.codigo : ""
  }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "fullWidth", true), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "name", "CodigoProducto"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "autoComplete", "Codigo"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "disabled", true), _React$createElement))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 4
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    required: true,
    fullWidth: true,
    id: "CantidadProducto",
    name: "CantidadProducto",
    autoComplete: "Cantidad",
    helperText: "Cantidad",
    disabled: props.articulo.length == 0 ? true : false
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 4,
    sm: 4
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    fullWidth: true,
    id: "precio",
    name: "precio",
    value: props.articulo ? props.JuniorData.length > 1 ? props.articulo.precio_venta : props.articulo.precio_junior : "",
    helperText: "Precio",
    disabled: true
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    item: true,
    xs: 12,
    sm: 12
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    id: "observacion",
    name: "observacion",
    multiline: true,
    fullWidth: true,
    rowsMax: 4,
    helperText: "Observaci\xF3n",
    disabled: props.articulo.length == 0 ? true : false
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], {
    type: "submit",
    fullWidth: true,
    variant: "contained",
    className: classes.submit,
    disabled: props.articulo.length == 0 ? true : false
  }, "Agregar"))));
}

/***/ })

}]);
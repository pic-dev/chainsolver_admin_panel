(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[83],{

/***/ 1504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(44);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(53);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(52);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(725);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(851);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(79);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _assets_img_login_icons_logoDesglose_png__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(852);
/* harmony import */ var _Utils__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(16);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_8___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_7___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }










var quoteSearch = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default()(quoteSearch, _Component);

  var _super = _createSuper(quoteSearch);

  function quoteSearch(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, quoteSearch);

    _this = _super.call(this, props);
    _this.apiPost = _this.apiPost.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this));
    _this.limpiar = _this.limpiar.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this));
    _this.printDocument = _this.printDocument.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this));
    _this.toggleModal = _this.toggleModal.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this));
    _this.state = {
      desgloseKey: [],
      desglose: false,
      desgloseValues: [],
      desgloseValuesCostos: [],
      resumen: [],
      show: false,
      flete: [],
      impuestosBajos: [],
      totalVenta: [],
      impuestosMedios: [],
      impuestosAltos: [],
      honorarios: [],
      servicioLogistico: [],
      Serviciologis: [],
      impuestosAltosTotal: [],
      impuestosBajosTotal: [],
      impuestosMediosTotal: [],
      servicioLogisticoTotal: [],
      venta: [],
      token: [],
      tipo: [],
      totalIGVFleteCostos: [],
      totalIGVCostos: [],
      activar: false,
      cliente: "",
      contacto: "",
      correo: "",
      ejecutivo: "",
      naviera: []
    };
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(quoteSearch, [{
    key: "apiPost",
    value: function apiPost(e) {
      var _this2 = this;

      e.preventDefault();
      var _e$target$elements = e.target.elements,
          tk = _e$target$elements.tk,
          tkManual = _e$target$elements.tkManual,
          tipo = _e$target$elements.tipo;
      var url2;
      var ur;
      var res;
      var tipotk;
      var token;

      if (tk.value.length > 0) {
        res = tk.value.split("-");
        tipotk = res[1].trim();
        token = res[0].trim();
      } else {
        token = tkManual.value.trim();
        tipotk = tipo.value.trim();
      }

      var tk1 = token + "-" + tipotk;
      var transportes;
      var tipoCon;

      switch (tipotk) {
        case "FCL":
          transportes = "FLETE MARÍTIMO";
          tipoCon = "Maritimo - Completo";
          url2 = this.props.api + "maritimo_v2/get_ctz_fcl_resumen";
          ur = this.props.api + "maritimo_v2/get_busca_cot_fcl";
          break;

        case "LCL":
          transportes = "FLETE MARÍTIMO";
          tipoCon = "Maritimo - Compartido";
          url2 = this.props.api + "maritimo_v2/get_ctz_lcl_resumen";
          ur = this.props.api + "maritimo_v2/get_busca_cot_lcl";
          break;

        case "AER":
          transportes = "FLETE AÉREO";
          tipoCon = "Aereo";
          url2 = this.props.api + "aereo_v2/get_ctz_aereo_resumen";
          ur = this.props.api + "aereo_v2/get_busca_cot_aereo";
          break;

        default:
      }

      var token1 = token.toString();
      var datos1 = {
        token: token1
      };

      _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.t0 = axios__WEBPACK_IMPORTED_MODULE_14___default.a;
                _context.t1 = url2;
                _context.next = 4;
                return datos1;

              case 4:
                _context.t2 = _context.sent;

                _context.t0.post.call(_context.t0, _context.t1, _context.t2).then(function (res) {
                  var resumen2 = res.data.data.r_dat;
                  var impuestosBajosTotal = resumen2.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                      return item;
                    }
                  }).map(function (duration) {
                    return duration.valor.toFixed(2);
                  });
                  var impuestosMediosTotal = resumen2.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                      return item;
                    }
                  }).map(function (duration) {
                    return duration.valor.toFixed(2);
                  });
                  var impuestosAltosTotal = resumen2.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                      return item;
                    }
                  }).map(function (duration) {
                    return duration.valor.toFixed(2);
                  });
                  var Serviciologistico;
                  var preServiciologistico = resumen2.filter(function (item) {
                    if (item.tipo == "SERVICIO LOGÍSTICO" || item.tipo == "GANANCIA - SERVICIO LOGÍSTICO") {
                      return item;
                    }
                  }).map(function (duration) {
                    return duration.valor.toFixed(2);
                  });

                  if (preServiciologistico.length > 1) {
                    Serviciologistico = preServiciologistico.reduce(function (accumulator, current) {
                      return [+accumulator + +current];
                    }).map(function (duration) {
                      return duration.toFixed(2);
                    });
                  } else {
                    Serviciologistico = preServiciologistico;
                  }

                  var venta = resumen2.filter(function (item) {
                    if (item.tipo !== "IMPUESTOS DE ADUANA BAJOS" && item.tipo !== "IMPUESTOS DE ADUANA ALTOS" && item.tipo !== "IMPUESTOS DE ADUANA MEDIOS") {
                      return item;
                    }
                  });

                  _this2.setState({
                    impuestosAltosTotal: impuestosAltosTotal,
                    impuestosMediosTotal: impuestosMediosTotal,
                    impuestosBajosTotal: impuestosBajosTotal,
                    Serviciologis: Serviciologistico,
                    venta: venta,
                    tipo: tipoCon,
                    token: tk1.toString()
                  });
                }).catch(function (error) {
                  console.log(error);
                });

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();

      _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.t0 = axios__WEBPACK_IMPORTED_MODULE_14___default.a;
                _context2.t1 = ur;
                _context2.next = 4;
                return datos1;

              case 4:
                _context2.t2 = _context2.sent;

                _context2.t0.post.call(_context2.t0, _context2.t1, _context2.t2).then(function (res) {
                  var response = res.data.data.r_dat;
                  var flete = response.filter(function (item) {
                    if (item.tipo == transportes) {
                      return item;
                    }
                  }).filter(function (item) {
                    if (item.valor > 0) {
                      return item;
                    }
                  });
                  var impuestosBajos = response.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA BAJOS") {
                      return item;
                    } else {
                      return false;
                    }
                  }).map(function (duration) {
                    if (duration !== false) {
                      return duration.valor.toFixed(2);
                    } else {
                      return duration;
                    }
                  });
                  var IPM = response.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA BAJOS" && item.nombre == "IPM") {
                      return item;
                    } else {
                      return false;
                    }
                  }).map(function (duration) {
                    if (duration !== false) {
                      return duration.valor.toFixed(2);
                    } else {
                      return duration;
                    }
                  });
                  var impuestosMedios = response.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA MEDIOS") {
                      return item;
                    } else {
                      return false;
                    }
                  }).map(function (duration) {
                    if (duration !== false) {
                      return duration.valor.toFixed(2);
                    } else {
                      return duration;
                    }
                  });
                  var impuestosAltos = response.filter(function (item) {
                    if (item.tipo == "IMPUESTOS DE ADUANA ALTOS") {
                      return item;
                    } else {
                      return false;
                    }
                  }).map(function (duration) {
                    if (duration !== false) {
                      return duration.valor.toFixed(2);
                    } else {
                      return duration;
                    }
                  });
                  var transporte = response.filter(function (item) {
                    if (item.tipo == "TRANSPORTE DE MERCANCÍA") {
                      return item;
                    } else {
                      return false;
                    }
                  });
                  var honorarios = response.filter(function (item) {
                    if (item.tipo == "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
                      return item;
                    } else {
                      return false;
                    }
                  });
                  var seguro = response.filter(function (item) {
                    if (item.tipo == "SEGURO DE MERCANCÍA SUGERIDO") {
                      return item;
                    } else {
                      return false;
                    }
                  });
                  var servicioLogistico = response.filter(function (item) {
                    if (item.tipo == "SERVICIO LOGÍSTICO" || item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" || item.detalles == "Ganancia") {
                      return item;
                    } else {
                      return false;
                    }
                  });
                  var desgloseValuesCostos = response.filter(function (item) {
                    if (item.tipo !== "IMPUESTOS DE ADUANA BAJOS" && item.tipo !== "IMPUESTOS DE ADUANA ALTOS" && item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" && item.tipo !== transportes && item.tipo !== "SERVICIO LOGÍSTICO" && item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" && item.detalles !== "Ganancia" && item.tipo !== "TRÁMITES Y HONORARIOS DE AGENCIA DE ADUANA") {
                      if (item.valor > 0) {
                        return item;
                      }
                    }
                  });
                  var desgloseValues = response.filter(function (item) {
                    if (item.tipo !== "IMPUESTOS DE ADUANA BAJOS" && item.tipo !== "IMPUESTOS DE ADUANA ALTOS" && item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" && item.tipo !== transportes && item.tipo !== "SERVICIO LOGÍSTICO" && item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" && item.detalles !== "Ganancia") {
                      if (item.valor !== 0) {
                        return item;
                      }
                    }
                  });
                  var PretotalIGVCostos, totalIGVCostos;

                  switch (flete[0].detalles_calculos.pais_destino) {
                    case "PERU":
                      PretotalIGVCostos = response.filter(function (item) {
                        if (item.tipo !== "IMPUESTOS DE ADUANA BAJOS" && item.tipo !== "IMPUESTOS DE ADUANA ALTOS" && item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" && item.tipo !== "SERVICIO LOGÍSTICO" && item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" && item.tipo !== transportes && item.tipo !== "FLETE" && item.detalles !== "Ganancia" && item.tipo !== "THCD" && item.tipo !== "TCHD DOCUMENTACION" && item.tipo !== "SEGURO DE MERCANCÍA SUGERIDO") {
                          if (item.valor > 0) {
                            return item;
                          }
                        }
                      });
                      totalIGVCostos = [0];

                      if (PretotalIGVCostos.length > 1) {
                        totalIGVCostos = PretotalIGVCostos.map(function (duration) {
                          return (duration.valor * 0.18).toFixed(2);
                        }).reduce(function (accumulator, current) {
                          return [+accumulator + +current];
                        }).map(function (duration) {
                          return duration.toFixed(2);
                        });
                      }

                      break;

                    case "PANAMA":
                      PretotalIGVCostos = response.filter(function (item) {
                        if (item.tipo == "PORT CHARGES" || item.tipo == "MANEJOS") {
                          if (item.valor > 0) {
                            return item;
                          }
                        }
                      });
                      totalIGVCostos = [0];

                      if (PretotalIGVCostos.length > 1) {
                        totalIGVCostos = PretotalIGVCostos.map(function (duration) {
                          return (duration.valor * 0.07).toFixed(2);
                        }).reduce(function (accumulator, current) {
                          return [+accumulator + +current];
                        }).map(function (duration) {
                          return duration.toFixed(2);
                        });
                      }

                      break;

                    case "VENEZUELA":
                      totalIGVCostos = [0];
                      break;

                    default:
                  }

                  var desgloseTotal = response.filter(function (item) {
                    if (item.tipo !== "IMPUESTOS DE ADUANA BAJOS" && item.tipo !== "IMPUESTOS DE ADUANA ALTOS" && item.tipo !== "IMPUESTOS DE ADUANA MEDIOS" && item.tipo !== "SERVICIO LOGÍSTICO" && item.tipo !== "GANANCIA - SERVICIO LOGÍSTICO" && item.detalles !== "Ganancia") {
                      if (item.valor > 0) {
                        return item;
                      }
                    }
                  }).map(function (duration) {
                    return duration.valor.toFixed(2);
                  }).reduce(function (accumulator, current) {
                    return [+accumulator + +current];
                  });
                  var preservicioLogisticoTotal = response.filter(function (item) {
                    if (item.tipo == "SERVICIO LOGÍSTICO" || item.tipo == "GANANCIA - SERVICIO LOGÍSTICO" || item.detalles == "Ganancia") {
                      return item;
                    }
                  }).map(function (duration) {
                    return duration.valor;
                  });
                  var servicioLogisticoTotal;

                  if (preservicioLogisticoTotal.length > 1) {
                    servicioLogisticoTotal = preservicioLogisticoTotal.reduce(function (accumulator, current) {
                      return [+accumulator + +current];
                    });
                  } else {
                    servicioLogisticoTotal = preservicioLogisticoTotal;
                  }

                  var totalIGVFleteCostos = Number(desgloseTotal[0]) + Number(totalIGVCostos[0]);
                  var totalVenta = Number(servicioLogisticoTotal[0]) + Number(desgloseTotal[0]);
                  var naviera;

                  switch (tipoCon) {
                    case "Maritimo - Completo":
                      {
                        naviera = flete[0].detalles_calculos.naviera;
                      }
                      break;

                    case "Maritimo - Compartido":
                      {
                        naviera = flete[0].detalles_calculos.agentes_lcl_nombre;
                      }
                      break;

                    case "Aereo":
                      {
                        naviera = flete[0].detalles_calculos.agentes_aereo_nombre;
                      }
                      break;

                    default:
                  }

                  _this2.setState({
                    desgloseValues: desgloseValues,
                    desgloseValuesCostos: desgloseValuesCostos,
                    show: true,
                    flete: flete,
                    impuestosAltos: impuestosAltos,
                    impuestosMedios: impuestosMedios,
                    impuestosBajos: impuestosBajos,
                    servicioLogistico: servicioLogistico,
                    transporte: transporte,
                    seguro: seguro,
                    honorarios: honorarios,
                    naviera: naviera,
                    totalIGVFleteCostos: totalIGVFleteCostos.toFixed(2),
                    totalIGVCostos: totalIGVCostos[0],
                    servicioLogisticoTotal: servicioLogisticoTotal,
                    desgloseTotal: desgloseTotal[0],
                    totalVenta: totalVenta
                  });
                }).catch(function () {
                  var _this2$setState;

                  _this2.setState((_this2$setState = {
                    servicioLogistico: [],
                    transporte: [],
                    seguro: [],
                    honorarios: [],
                    desgloseValues: [],
                    desgloseValuesCostos: [],
                    show: false,
                    flete: [],
                    impuestosAltos: [],
                    totalIGVFleteCostos: [],
                    totalIGVCostos: [],
                    impuestosMedios: [],
                    impuestosBajos: []
                  }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "servicioLogistico", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "servicioLogisticoTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "desgloseTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "Serviciologis", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "impuestosAltosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "impuestosMediosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "impuestosBajosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "venta", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "naviera", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "tipo", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this2$setState, "token", []), _this2$setState));

                  alert("token no valido");
                });

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }, {
    key: "limpiar",
    value: function limpiar() {
      var _this$setState;

      this.setState((_this$setState = {
        servicioLogistico: [],
        transporte: [],
        seguro: [],
        honorarios: [],
        desgloseValues: [],
        desgloseValuesCostos: [],
        show: false,
        flete: [],
        totalIGVFleteCostos: [],
        totalIGVCostos: [],
        impuestosAltos: [],
        impuestosMedios: [],
        impuestosBajos: []
      }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "servicioLogistico", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "servicioLogisticoTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "Serviciologis", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "desgloseTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "impuestosAltosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "impuestosMediosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "impuestosBajosTotal", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "naviera", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "tipo", []), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_this$setState, "token", []), _this$setState));
    }
  }, {
    key: "toggleModal",
    value: function toggleModal() {
      this.setState(function (ps) {
        return {
          activar: !ps.activar
        };
      });
    }
  }, {
    key: "printDocument",
    value: function printDocument() {
      var _this3 = this;

      var input = document.getElementById("divToPrint");
      html2canvas__WEBPACK_IMPORTED_MODULE_12___default()(input).then(function (canvas) {
        var imgData = canvas.toDataURL("image/png");
        var pdf = new jspdf__WEBPACK_IMPORTED_MODULE_13__[/* jsPDF */ "a"]({
          orientation: "landscape",
          unit: "mm"
        });
        pdf.addImage(imgData, "PNG", 7, 7, 280, 200);
        pdf.addImage(_assets_img_login_icons_logoDesglose_png__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], "PNG", 7, 5, 42, 12);
        var nombre = _this3.state.token;
        pdf.save(nombre + ".pdf");
      });
      this.toggleModal();
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Modal */ "D"], {
        isOpen: this.props.active,
        toggle: this.props.toggle,
        backdrop: false,
        style: {
          maxWidth: "max-content"
        }
      }, this.state.desgloseKey == "piccargo1" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalBody */ "E"], {
        style: {
          padding: "0"
        },
        className: "mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "text-center font"
      }, "Resumen"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "mx-auto"
      }, this.state.show == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: " login-form",
        onSubmit: this.apiPost
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "tk",
        type: "text",
        placeholder: "token 84524-fcl"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-3 text-center"
      }, "-----o------"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "p-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "tkManual",
        type: "text",
        placeholder: "token 43542145654215"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "p-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "select",
        name: "tipo",
        id: "exampleSelect"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("option", null, "FCL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("option", null, "LCL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("option", null, "AEREO")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Buscar")))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "mx-auto pt-3 font2 pb-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        className: "btn-style-blue",
        onClick: this.toggleModal
      }, "Descargar")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "mx-auto pt-3 font2 pb-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        className: "btn-style",
        onClick: this.limpiar
      }, "Buscar otra")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Modal */ "D"], {
        isOpen: this.state.activar,
        toggle: this.toggleModal
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalHeader */ "G"], {
        className: "mx-auto p-3 font text-center"
      }, "Descargar Presupuesto"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalBody */ "E"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto p-2 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "NOMBRE DEL CLIENTE"), "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "cliente",
        type: "text",
        placeholder: "Ingrese aqui el nombre del cliente",
        value: this.state.cliente,
        onChange: function onChange(e) {
          _this4.setState({
            cliente: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "CONTACTO"), "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "contacto",
        type: "text",
        placeholder: "Ingrese aqui el nombre del CONTACTO",
        value: this.state.contacto,
        onChange: function onChange(e) {
          _this4.setState({
            contacto: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "CORREO"), "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "correo",
        type: "text",
        placeholder: "Ingrese aqui el CORREO del CONTACTO",
        value: this.state.correo,
        onChange: function onChange(e) {
          _this4.setState({
            correo: e.target.value
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "EJECUTIVO PIC CARGO"), "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "ejecutivo",
        type: "text",
        placeholder: "Ingrese aqui el nombre del ejecutivo",
        value: this.state.ejecutivo,
        onChange: function onChange(e) {
          _this4.setState({
            ejecutivo: e.target.value
          });
        }
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalFooter */ "F"], {
        className: "mx-auto p-3 font2 pb-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        className: "btn-style-blue",
        onClick: this.printDocument
      }, "Descargar"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        id: "divToPrint",
        className: "p-2",
        style: {
          width: "330mm",
          minHeight: "210mm",
          marginLeft: "auto",
          marginRight: "auto"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        style: {
          color: "black",
          fontSize: "1rem"
        },
        className: "pb-4  font text-right"
      }, "Numero de cotizaci\xF3n #: ID ", this.state.token), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        style: {
          overflow: "auto"
        },
        className: "p-2 text-justify"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        style: {
          padding: "0"
        },
        xxs: "5",
        md: "5",
        sm: "5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "color-black",
        style: {
          textAlign: "center",
          backgroundColor: "#eae6e6"
        }
      }, "DATOS DEL CLIENTE")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        className: "font color-black",
        style: {
          textAlign: "center",
          backgroundColor: "#fdfbb7"
        },
        xxs: "7",
        md: "7",
        sm: "7"
      }, "PROFIT:", " ", this.state.servicioLogisticoTotal[0].toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "5",
        md: "5",
        sm: "5"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("ul", {
        className: "p-1",
        style: {
          fontSize: "0.8rem",
          listStyle: "none"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "CLIENTE: ", this.state.cliente), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "CONTACTO:", this.state.contacto), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "CORREO: ", this.state.correo), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Ejecutivo PIC CARGO: ", this.state.ejecutivo))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "7",
        md: "7",
        sm: "7",
        style: {
          padding: "0"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "color-black",
        style: {
          textAlign: "center",
          backgroundColor: "#eae6e6"
        }
      }, "INFORMACI\xD3N DE LA CARGA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "6",
        md: "6",
        sm: "6",
        className: "p-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("ul", {
        style: {
          fontSize: "0.8rem",
          listStyle: "none"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Transporte:", this.state.tipo), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Ruta Origen:", this.state.flete[0].detalles_calculos.pais_origen + " - " + this.state.flete[0].detalles_calculos.puerto_origen), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Ruta Destino:", this.state.flete[0].detalles_calculos.pais_destino + " - " + this.state.flete[0].detalles_calculos.puerto_destino), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Contenido de carga :", this.state.tipo === "Maritimo - Completo" ? Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* conver_conten */ "b"])(this.state.flete) : this.state.flete[0].input_cantidad_bultos + " Bultos, " + this.state.flete[0].input_peso + " Kl, " + this.state.flete[0].input_volumen + " M3 "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Gastos portuarios:", this.state.flete[0].input_calcular_gastos_y_almacen_aduanero ? " SI" : " NO"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "6",
        md: "6",
        sm: "6",
        className: "p-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("ul", {
        style: {
          fontSize: "0.8rem",
          listStyle: "none"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Valor Mercancia:", this.state.flete[0].input_calcular_inpuestos_y_permisos ? this.state.flete[0].input_valor_mercancia : " NO COTIZADO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Servicios / Impuestos de Aduana:", this.state.flete[0].input_calcular_inpuestos_y_permisos ? " SI" : " NO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Transporte a Domicilio:", this.state.flete[0].input_calcular_transporte ? " SI" : " NO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Seguro de Mercancia:", this.state.flete[0].input_calcular_seguro_mercancia ? " SI" : " NO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", null, "Naviera / Agente: ", this.state.naviera), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("li", {
        style: {
          display: this.state.flete[0].input_calcular_inpuestos_y_permisos ? "" : "none"
        }
      }, this.state.flete[0].input_ha_cotizado ? " Segunda Importación" : " Primera Importación")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        style: {
          textAlign: "center",
          backgroundColor: "#0d5084"
        },
        className: "font2 color-white",
        xxs: "12",
        md: "12",
        sm: "12"
      }, "GANANCIA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Table */ "O"], {
        style: {
          textAlign: "center",
          margin: "0rem"
        },
        responsive: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("thead", {
        style: {
          backgroundColor: "#cfe7fe",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "ITEM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "SUB ITEM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "DETALLES"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "PROFIT"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "PROFIT+IGV"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tbody", null, this.state.servicioLogistico.map(function (item, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
          key: index + "a"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
          scope: "row"
        }, item.tipo), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.nombre), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.detalles), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.valor.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null));
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        scope: "row"
      }, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "TOTAL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, this.state.servicioLogisticoTotal[0].toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, this.state.flete[0].detalles_calculos.pais_destino == "PERU" && this.state.totalIGVCostos > 0 ? (this.state.Serviciologis[0] / 3 * 2 * 0.18).toFixed(2) : "n/a"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, this.state.flete[0].detalles_calculos.pais_destino == "PERU" && this.state.totalIGVCostos > 0 ? (this.state.Serviciologis[0] / 3 * 2 * 0.18 + this.state.servicioLogisticoTotal[0]).toFixed(2) : "n/a")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        style: {
          textAlign: "center",
          backgroundColor: "#0d5084"
        },
        className: "font2 color-white",
        xxs: "12",
        md: "12",
        sm: "12"
      }, "VENTA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Table */ "O"], {
        style: {
          textAlign: "center",
          margin: "0rem"
        },
        responsive: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("thead", {
        style: {
          backgroundColor: "#cfe7fe",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "ITEM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "VALOR"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "VALOR+IGV"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tbody", null, this.state.venta.map(function (item, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
          key: index + "b"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
          scope: "row"
        }, item.tipo), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.valor.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* igv */ "j"])(item, _this4.state.flete[0].detalles_calculos.pais_destino, _this4.state.totalIGVCostos)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* igvMasTotal */ "k"])(item, _this4.state.flete[0].detalles_calculos.pais_destino, _this4.state.totalIGVCostos)));
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        scope: "row"
      }, "TOTAL:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, this.state.totalVenta.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* totalGeneralMasIGVGanancias */ "t"])(this.state.servicioLogisticoTotal[0], this.state.totalIGVCostos, this.state.flete[0].detalles_calculos.pais_destino, this.state.totalIGVCostos)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* totalGeneralMasIgvVEnta */ "u"])(this.state.servicioLogisticoTotal[0], this.state.totalIGVFleteCostos, this.state.flete[0].detalles_calculos.pais_destino, this.state.totalIGVCostos, this.state.totalVenta))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        style: {
          textAlign: "center",
          backgroundColor: "#0d5084"
        },
        className: "font2 color-white",
        xxs: "12",
        md: "12",
        sm: "12"
      }, "COSTOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Table */ "O"], {
        style: {
          textAlign: "center",
          margin: "0rem"
        },
        responsive: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("thead", {
        style: {
          backgroundColor: "#cfe7fe",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "ITEM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "SUB ITEM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "TOTAL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "TOTAL+IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", null, "DETALLES"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tbody", null, this.state.flete.map(function (item, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
          key: index + "c"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
          scope: "row"
        }, item.nombre), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.nombre), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.valor.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "n/a"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.valor.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.detalles));
      }), this.state.desgloseValues.map(function (item, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
          key: index + "d"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
          scope: "row"
        }, item.tipo), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.nombre), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.valor.toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* igv */ "j"])(item, _this4.state.flete[0].detalles_calculos.pais_destino, _this4.state.totalIGVCostos)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* igvMasTotal */ "k"])(item, _this4.state.flete[0].detalles_calculos.pais_destino, _this4.state.totalIGVCostos)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, item.detalles));
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        scope: "row"
      }, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "TOTAL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Number(this.state.desgloseTotal).toFixed(2)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* totalGeneralCostos */ "s"])(this.state.totalIGVCostos, this.state.flete[0].detalles_calculos.pais_destino)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, Object(_Utils__WEBPACK_IMPORTED_MODULE_16__[/* totalGeneralCostos */ "s"])(this.state.totalIGVFleteCostos, this.state.flete[0].detalles_calculos.pais_destino)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", null, "--")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        style: {
          textAlign: "center",
          padding: "0rem"
        },
        className: "mx-auto text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        style: {
          textAlign: "center",
          backgroundColor: "#b33641"
        },
        className: "font2 color-white",
        xxs: "12",
        md: "12",
        sm: "12"
      }, "IMPUESTOS DE ADUANA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Table */ "O"], {
        style: {
          textAlign: "center"
        },
        responsive: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, "TIPO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, "IMPUESTOS BAJOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0f7d1",
          fontWeight: "bold"
        }
      }, "IMPUESTOS MEDIOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#eac3c3",
          fontWeight: "bold"
        }
      }, "IMPUESTOS ALTOS"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "Percepciones 1era vez"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7"
        }
      }, this.state.impuestosBajos[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1"
        }
      }, this.state.impuestosMedios[0]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3"
        }
      }, this.state.impuestosAltos[0])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "ISC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7"
        }
      }, this.state.impuestosBajos[1]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1"
        }
      }, this.state.impuestosMedios[1]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3"
        }
      }, this.state.impuestosAltos[1])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "IPM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7"
        }
      }, this.state.impuestosBajos[2]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1"
        }
      }, this.state.impuestosMedios[2]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3"
        }
      }, this.state.impuestosAltos[2])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7"
        }
      }, this.state.impuestosBajos[3]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1"
        }
      }, this.state.impuestosMedios[3]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3"
        }
      }, this.state.impuestosAltos[3])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "AD valorem"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7"
        }
      }, this.state.impuestosBajos[4]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1"
        }
      }, this.state.impuestosMedios[4]), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3"
        }
      }, this.state.impuestosAltos[4])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("th", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        },
        scope: "row"
      }, "TOTAL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0e4f7",
          fontWeight: "bold"
        }
      }, this.state.impuestosBajosTotal[0] > 0 ? "$ " + this.state.impuestosBajosTotal[0] : "No cotizado"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#d0f7d1",
          fontWeight: "bold"
        }
      }, this.state.impuestosMediosTotal[0] > 0 ? "$ " + this.state.impuestosMediosTotal[0] : "No cotizado"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("td", {
        style: {
          backgroundColor: "#eac3c3",
          fontWeight: "bold"
        }
      }, this.state.impuestosAltosTotal[0] > 0 ? "$ " + this.state.impuestosAltosTotal[0] : "No cotizado")))))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalBody */ "E"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "text-center font"
      }, "Resumen"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-3 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "clave",
        type: "text",
        placeholder: "Clave",
        value: this.state.desgloseKey,
        onChange: function onChange(e) {
          _this4.setState({
            desgloseKey: e.target.value
          });
        }
      }))))));
    }
  }]);

  return quoteSearch;
}(react__WEBPACK_IMPORTED_MODULE_9__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (quoteSearch);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[72],{

/***/ 1133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(44);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(53);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _Firebase__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(11);
/* harmony import */ var _Errores__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(615);
/* harmony import */ var _tracking__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(20);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(79);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(249);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }








var timer;

var LoginFrom = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(LoginFrom, _Component);

  var _super = _createSuper(LoginFrom);

  function LoginFrom(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, LoginFrom);

    _this = _super.call(this, props);

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "seterror", function (e) {
      _this.setState(function () {
        return {
          error: e
        };
      });
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "setsignup", function (e) {
      _this.setState(function () {
        return {
          signup: e
        };
      });
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "setsignIn", function (e) {
      _this.setState(function () {
        return {
          signIn: e
        };
      });
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "handleSignUp", /*#__PURE__*/function () {
      var _ref = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
        var _e$target$elements, usuario, clave, Confirmacion;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements = e.target.elements, usuario = _e$target$elements.usuario, clave = _e$target$elements.clave, Confirmacion = _e$target$elements.Confirmacion;

                if (!(usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value === Confirmacion.value)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 6;
                return _Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].auth().createUserWithEmailAndPassword(usuario.value, clave.value).then(function () {
                  //Event("EmailRegistro", "Usuario se registro con correo", "SERVICIOS");
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));
                }).catch(function (error) {
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));
                  window.location.reload();
                });

              case 6:
                _context.next = 9;
                break;

              case 8:
                if (usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value !== Confirmacion.value) {
                  _this.seterror("La contraseña ingresada no coincide con la confimación");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                } else {
                  _this.seterror("Debe llenar todos los campos requeridos");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                }

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "correoClave", /*#__PURE__*/function () {
      var _ref2 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(e) {
        var _e$target$elements2, usuario, clave;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements2 = e.target.elements, usuario = _e$target$elements2.usuario, clave = _e$target$elements2.clave;
                axios__WEBPACK_IMPORTED_MODULE_14___default.a.post("https://point.qreport.site/login/general", {
                  usuario: usuario.value,
                  contraseña: clave.value
                }).then(function (res) {
                  if (res.data.length > 0) {
                    sessionStorage.seniorLogin = JSON.stringify(res.data);

                    _this.props.setUserSeniorData(res.data);

                    _this.props.toggle();

                    _this.props.handleLogin();

                    _this.props.history.push("/PedidosJunior");
                  } else {
                    _Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].auth().signInWithEmailAndPassword(usuario.value, clave.value).then(function (result) {
                      //Event("EmailLoginHeader", "Usuario ingresó con correo", "home");
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));
                    }).catch(function (error) {
                      //errores("Headercorreo", error);
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));
                      window.location.reload();
                    });
                  }
                }).catch(function (error) {
                  console.log(error);
                });

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "socialLogin", function (provider, e) {
      e.preventDefault();

      _this.setState({
        error: ""
      });

      _Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].auth().setPersistence(_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* persistencia */ "c"]).then(function () {
        _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return _Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].auth().signInWithPopup(provider).then(function (result) {
                    /*Event(
                      "GoogleLoginHeader",
                      "Usuario ingresó con botton de google",
                      "SERVICIOS"
                    );*/
                    var user = [[result.additionalUserInfo.profile.id], [result.additionalUserInfo.profile.name], [result.additionalUserInfo.profile.email], [result.additionalUserInfo.profile.picture]];
                    sessionStorage.setItem("user", JSON.stringify(user));
                    sessionStorage.usuario = "2";
                  }).catch(function (error) {
                    sessionStorage.usuario = "2";
                    var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(user));
                    window.location.reload();
                  });

                case 2:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }))();
      }).catch(function (error) {});
    });

    _this.state = {
      Signup: false,
      signIn: false,
      error: ""
    };
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(LoginFrom, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, !this.state.signup ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Form */ "t"], {
        className: " login-form"
      }, this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "paddingLogin text-center",
        style: {
          textAling: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Iniciar sesi\xF3n")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "-o-")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Form */ "t"], {
        className: " login-form",
        onSubmit: this.correoClave
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Input */ "w"], {
        type: "text",
        name: "usuario",
        placeholder: "Usuario"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "login-form-button"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Iniciar sesi\xF3n"), "\xA0 O \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          _this2.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "Registrate"))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Form */ "t"], {
        className: "login-form text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Registro")), this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_10__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "-o-")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Form */ "t"], {
        className: "login-form text-center",
        onSubmit: this.handleSignUp
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Input */ "w"], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Input */ "w"], {
        name: "Confirmacion",
        type: "password",
        placeholder: "Confirma tu Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_13__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Registrate"), "\xA0 O \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        onClick: function onClick() {
          _this2.setsignup(false);

          _this2.setsignIn(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N")))), " ");
    } // setInterval
    // clearInterval

  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(timer);
    }
  }]);

  return LoginFrom;
}(react__WEBPACK_IMPORTED_MODULE_9__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"])(LoginFrom));

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ 1502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(52);
/* harmony import */ var _Firebase__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(11);
/* harmony import */ var _Utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(16);








function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var timer;

var comment = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(comment, _Component);

  var _super = _createSuper(comment);

  function comment(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, comment);

    _this = _super.call(this, props);

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default()(_this), "SelectedOnceComment", function () {
      sessionStorage.CommentNone = "1";
    });

    _this.SelectedOnceComment = _this.SelectedOnceComment.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default()(_this));
    _this.sendCommentary = _this.sendCommentary.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default()(_this));
    _this.handleFormState = _this.handleFormState.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2___default()(_this));
    _this.state = {
      showMessage: false,
      commentEmpty: false,
      Commentform: [{
        Label: "paso1",
        value: "no problema"
      }, {
        Label: "paso2",
        value: "no problema"
      }, {
        Label: "paso3",
        value: "no problema"
      }, {
        Label: "paso4",
        value: "no problema"
      }, {
        Label: "recomendarias",
        value: ""
      }, {
        Label: "inversion",
        value: ""
      }, {
        Label: "adicional",
        value: "sin comentarios"
      }, {
        Label: "costo",
        value: ""
      }]
    };
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(comment, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "sendCommentary",
    value: function sendCommentary() {
      var _this2 = this;

      if ((this.state.Commentform[0].value.length > 0 || this.state.Commentform[1].value.length > 0 || this.state.Commentform[2].value.length > 0 || this.state.Commentform[3].value.length > 0) && this.state.Commentform[4].value.length > 0 && this.state.Commentform[5].value.length > 0) {
        var data = JSON.parse(sessionStorage.getItem("user"));
        var params = {
          nombre: data[1].toString(),
          email: data[2].toString(),
          fecha: Object(_Utils__WEBPACK_IMPORTED_MODULE_11__[/* getDateWithFormat */ "h"])() + " " + Object(_Utils__WEBPACK_IMPORTED_MODULE_11__[/* getCurrentTime */ "g"])(),
          pais: sessionStorage.country,
          paso1: this.state.Commentform[0].value,
          paso2: this.state.Commentform[1].value,
          paso3: this.state.Commentform[2].value,
          paso4: this.state.Commentform[3].value,
          recomendarias: this.state.Commentform[4].value,
          inversion: this.state.Commentform[5].value,
          adicional: this.state.Commentform[6].value,
          costo: this.state.Commentform[7].value
        };
        _Firebase__WEBPACK_IMPORTED_MODULE_10__[/* firebaseConf */ "a"].database().ref("estadisticas").push(params).then(function () {
          _this2.props.toggle();

          _this2.setState(function (ps) {
            return {
              showMessage: true
            };
          });

          timer = setTimeout(function () {
            _this2.setState(function (ps) {
              return {
                showMessage: false
              };
            });
          }, 4000);
        }).catch(function (err) {
          _this2.props.toggle();

          _this2.setState(function (ps) {
            return {
              showMessage: true
            };
          });

          timer = setTimeout(function () {
            _this2.setState(function (ps) {
              return {
                showMessage: false
              };
            });
          }, 4000);
          Object(_Utils__WEBPACK_IMPORTED_MODULE_11__[/* errores */ "d"])("EnviarComentario_HEADER", error);
        });
      } else {
        this.setState(function (ps) {
          return {
            commentEmpty: true
          };
        });
        timer = setTimeout(function () {
          _this2.setState(function (ps) {
            return {
              commentEmpty: false
            };
          });
        }, 4000);
      }
    }
  }, {
    key: "handleFormState",
    value: function handleFormState(input) {
      var newState = this.state.Commentform;
      newState[input.id].value = input.value;
      this.setState({
        Commentform: newState
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Modal */ "D"], {
        isOpen: this.props.active,
        toggle: this.props.toggle,
        backdrop: true,
        className: "survey"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ModalBody */ "E"], {
        style: {
          padding: "0"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Form */ "t"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "arial mx-auto",
        style: {
          textAlign: "center",
          padding: "0"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroupItem */ "C"], {
        className: "color-white",
        style: {
          textAlign: "center",
          backgroundColor: "#0d5084"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "4",
        md: "4",
        className: "mt-3  arial mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        className: "icon-regalo"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "8",
        md: "8",
        className: "mt-3 arial surveyTitle mx-auto"
      }, "\xA1AY\xDADANOS A MEJORAR"))))), this.state.commentEmpty ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        className: "alert alert-danger  arial  mx-auto",
        role: "alert"
      }, "Favor completar la encuesta, tus comentario nos seran de mucha ayuda para mejorar.") : "", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        className: "text-center mt-2 mb-2",
        xxs: "12",
        md: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, "Ingrese numero de Contacto (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Input */ "w"], {
        className: "text-center mt-1 mb-1",
        name: "number",
        type: "text",
        placeholder: "numero de contacto"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, "Ingrese nombre de Contacto (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Input */ "w"], {
        className: "text-center mt-1 mb-1",
        name: "nombre",
        type: "text",
        placeholder: "nombre"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        className: "text-center mt-2 mb-2",
        xxs: "12",
        md: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        className: "mb-2",
        style: {
          fontWeight: "bold",
          fontSize: "1.2em",
          textAlign: "left"
        }
      }, "1.-Indica el nivel de satisfacci\xF3n con las tarifas del", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        className: "color-blue"
      }, "SERVICIO LOG\xEDSTICO SIN IMPUESTOS")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        value: "alta",
        onClick: function onClick(e) {
          _this3.handleFormState({
            id: 7,
            value: "alta"
          });
        },
        className: "icon-happy mr-3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        value: "regular",
        onClick: function onClick(e) {
          _this3.handleFormState({
            id: 7,
            value: "regular"
          });
        },
        className: "icon-neutral mr-3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        value: "excelente",
        onClick: function onClick(e) {
          _this3.handleFormState({
            id: 7,
            value: "excelente"
          });
        },
        className: "icon-sad"
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2  "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Label */ "A"], {
        for: "exampleCheckbox"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          fontSize: "1.2em"
        }
      }, "2.-Indicanos \xBFen cual de los siguientes pasos present\xF3 alguna dificultad?")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "checkbox",
        id: "ruta",
        label: "Paso 1: Indica t\xFA ruta y tipo de contenedor.",
        value: "paso 1",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 0,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "checkbox",
        id: "paso2",
        label: "Paso 2: Uso de la calculadora",
        value: "Uso de la calculadora",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 1,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "checkbox",
        id: "paso3",
        label: "Paso 3: Agregar servicios adicionales",
        value: "Agregar servicios adicionales",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 2,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "checkbox",
        id: "paso4",
        label: "Paso 4: Entendimiento del presupuesto",
        value: "Entendimiento del presupuesto",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2  "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          fontSize: "1.2em"
        }
      }, "3.-\xBFRecomendarias esta aplicaci\xF3n a otro usuario?"), " ", "\xA0", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "radio",
        id: "RecomendacionRadio",
        name: "RecomendacionRadio",
        label: "Si",
        value: "Si",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        },
        inline: true
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "radio",
        id: "RecomendacionRadio2",
        name: "RecomendacionRadio",
        label: "No",
        value: "No",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        },
        inline: true
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2  "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Label */ "A"], {
        for: "exampleCheckbox"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          fontSize: "1.2em"
        }
      }, " ", "4.-\xBFCu\xE1nto estar\xEDa dispuesto a invertir por cada consulta?", " "), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          color: "gray"
        }
      }, "(D\xF3lar americado)")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* FormGroup */ "u"], {
        className: "text-center "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "radio",
        id: "invertirRadio",
        name: "invertirRadio",
        value: "1",
        label: "1 usd",
        inline: true,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 5,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "radio",
        id: "invertirRadio2",
        name: "invertirRadio",
        value: "3",
        label: "3 usd",
        inline: true,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 5,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        type: "radio",
        id: "invertirRadio3",
        name: "invertirRadio",
        value: "5",
        label: "5 usd",
        inline: true,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 5,
            value: e.target.value || ""
          });
        }
      })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          fontSize: "1.2em"
        }
      }, "Comentario adicional"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Input */ "w"], {
        style: {
          lineHeight: "13px",
          textAlign: "justify"
        },
        type: "textarea",
        placeholder: "Comentanos t\xFA experiencia con nuestro Sistema de Cotizaci\xF3n Online...",
        rows: 7,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 6,
            value: e.target.value || ""
          });
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-4  mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        id: "notShow2",
        type: "checkbox",
        onChange: this.SelectedOnceComment,
        label: "No volver a mostrar este mensaje"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        className: "mb-4 text-center",
        xxs: "12",
        md: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Button */ "c"], {
        color: "primary",
        className: "btn-style-Green",
        onClick: this.sendCommentary
      }, "ENVIAR")))))), this.state.showMessage ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        className: "alertSuccess  arial alert alert-success col-12 col-md-5 mx-auto",
        role: "alert"
      }, "\xA1Comentario enviado satisfactoriamente!") : "");
    }
  }]);

  return comment;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (comment);

/***/ })

}]);
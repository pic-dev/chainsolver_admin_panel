(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[67],{

/***/ 1125:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 1520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/classnames/index.js
var classnames = __webpack_require__(2);
var classnames_default = /*#__PURE__*/__webpack_require__.n(classnames);

// EXTERNAL MODULE: ./node_modules/react-responsive-carousel/lib/styles/carousel.min.css
var carousel_min = __webpack_require__(594);

// EXTERNAL MODULE: ./node_modules/react-responsive-carousel/lib/js/index.js
var js = __webpack_require__(497);

// EXTERNAL MODULE: ./node_modules/react-inner-image-zoom/lib/InnerImageZoom/styles.css
var styles = __webpack_require__(1125);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/InnerImageZoom/components/Image.js



var Image_Image = function Image(props) {
  var src = props.src,
      srcSet = props.srcSet,
      sizes = props.sizes,
      sources = props.sources,
      alt = props.alt,
      isZoomed = props.isZoomed,
      fadeDuration = props.fadeDuration;


  return react_default.a.createElement(
    react["Fragment"],
    null,
    sources && sources.length > 0 ? react_default.a.createElement(
      'picture',
      null,
      sources.map(function (source, i) {
        return react_default.a.createElement(
          react["Fragment"],
          { key: i },
          source.srcSet && react_default.a.createElement('source', {
            srcSet: source.srcSet,
            sizes: source.sizes,
            media: source.media,
            type: source.type
          })
        );
      }),
      react_default.a.createElement('img', {
        className: 'iiz__img ' + (isZoomed ? 'iiz__img--invisible' : ''),
        style: {
          transition: 'linear 0ms opacity ' + (isZoomed ? fadeDuration : 0) + 'ms, linear ' + fadeDuration + 'ms visibility ' + (isZoomed ? fadeDuration : 0) + 'ms'
        },
        src: src,
        srcSet: srcSet,
        sizes: sizes,
        alt: alt
      })
    ) : react_default.a.createElement('img', {
      className: 'iiz__img ' + (isZoomed ? 'iiz__img--invisible' : ''),
      style: {
        transition: 'linear 0ms opacity ' + (isZoomed ? fadeDuration : 0) + 'ms, linear ' + fadeDuration + 'ms visibility ' + (isZoomed ? fadeDuration : 0) + 'ms'
      },
      src: src,
      srcSet: srcSet,
      sizes: sizes,
      alt: alt
    })
  );
};

Image_Image.propTypes =  false ? undefined : {};

/* harmony default export */ var components_Image = (Image_Image);
// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/InnerImageZoom/components/ZoomImage.js



var ZoomImage_ZoomImage = function ZoomImage(props) {
  var src = props.src,
      fadeDuration = props.fadeDuration,
      top = props.top,
      left = props.left,
      isZoomed = props.isZoomed,
      onLoad = props.onLoad,
      onDragStart = props.onDragStart,
      onDragEnd = props.onDragEnd,
      onClose = props.onClose;


  return react_default.a.createElement(
    react["Fragment"],
    null,
    react_default.a.createElement('img', {
      className: 'iiz__zoom-img ' + (isZoomed ? 'iiz__zoom-img--visible' : ''),
      style: {
        top: top,
        left: left,
        transition: 'linear ' + fadeDuration + 'ms opacity, linear ' + fadeDuration + 'ms visibility'
      },
      src: src,
      onLoad: onLoad,
      onTouchStart: onDragStart,
      onTouchEnd: onDragEnd,
      onMouseDown: onDragStart,
      onMouseUp: onDragEnd,
      alt: ''
    }),
    onClose && react_default.a.createElement('button', {
      className: 'iiz__btn iiz__close ' + (isZoomed ? 'iiz__close--visible' : ''),
      style: {
        transition: 'linear ' + fadeDuration + 'ms opacity, linear ' + fadeDuration + 'ms visibility'
      },
      onClick: onClose,
      'aria-label': 'Zoom Out'
    })
  );
};

ZoomImage_ZoomImage.propTypes =  false ? undefined : {};

/* harmony default export */ var components_ZoomImage = (ZoomImage_ZoomImage);
// EXTERNAL MODULE: ./node_modules/react-dom/index.js
var react_dom = __webpack_require__(15);

// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/InnerImageZoom/components/FullscreenPortal.js
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var FullscreenPortal_FullscreenPortal = function (_Component) {
  _inherits(FullscreenPortal, _Component);

  function FullscreenPortal(props) {
    _classCallCheck(this, FullscreenPortal);

    var _this = _possibleConstructorReturn(this, _Component.call(this, props));

    _this.el = document.createElement('div');
    _this.el.classList.add(props.className);
    return _this;
  }

  FullscreenPortal.prototype.componentDidMount = function componentDidMount() {
    document.body.appendChild(this.el);
  };

  FullscreenPortal.prototype.componentWillUnmount = function componentWillUnmount() {
    document.body.removeChild(this.el);
  };

  FullscreenPortal.prototype.render = function render() {
    return Object(react_dom["createPortal"])(this.props.children, this.el);
  };

  return FullscreenPortal;
}(react["Component"]);

FullscreenPortal_FullscreenPortal.propTypes =  false ? undefined : {};

/* harmony default export */ var components_FullscreenPortal = (FullscreenPortal_FullscreenPortal);
// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/InnerImageZoom/InnerImageZoom.js
function InnerImageZoom_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function InnerImageZoom_possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function InnerImageZoom_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var InnerImageZoom_InnerImageZoom = function (_Component) {
  InnerImageZoom_inherits(InnerImageZoom, _Component);

  function InnerImageZoom(props) {
    InnerImageZoom_classCallCheck(this, InnerImageZoom);

    var _this = InnerImageZoom_possibleConstructorReturn(this, _Component.call(this, props));

    _this.handleMouseEnter = function (e) {
      _this.setState({
        isActive: true
      });

      if (_this.props.zoomType === 'hover' && !_this.state.isZoomed) {
        _this.handleClick(e);
      }
    };

    _this.handleTouchStart = function () {
      var isFullscreen = _this.props.fullscreenOnMobile && window.matchMedia && window.matchMedia('(max-width: ' + _this.props.mobileBreakpoint + 'px)').matches;

      _this.setState({
        isTouch: true,
        isFullscreen: isFullscreen,
        currentMoveType: 'drag'
      });
    };

    _this.handleClick = function (e) {
      if (_this.state.isZoomed) {
        if (!_this.state.isTouch && !_this.state.isDragging) {
          _this.zoomOut();
        }

        return;
      }

      if (_this.state.isTouch) {
        _this.setState({
          isActive: true
        });
      }

      if (_this.isLoaded) {
        _this.zoomIn(e.pageX, e.pageY);
      } else {
        _this.onLoadCallback = _this.zoomIn.bind(_this, e.pageX, e.pageY);
      }
    };

    _this.handleLoad = function (e) {
      _this.isLoaded = true;
      _this.zoomImg = e.target;
      _this.zoomImg.setAttribute('width', _this.zoomImg.offsetWidth * _this.props.zoomScale);
      _this.zoomImg.setAttribute('height', _this.zoomImg.offsetHeight * _this.props.zoomScale);
      _this.bounds = _this.getBounds(_this.img, false);
      _this.ratios = _this.getRatios(_this.bounds, _this.zoomImg);

      if (_this.onLoadCallback) {
        _this.onLoadCallback();
        _this.onLoadCallback = null;
      }
    };

    _this.handleMouseMove = function (e) {
      var left = e.pageX - _this.offsets.x;
      var top = e.pageY - _this.offsets.y;

      left = Math.max(Math.min(left, _this.bounds.width), 0);
      top = Math.max(Math.min(top, _this.bounds.height), 0);

      _this.setState({
        left: left * -_this.ratios.x,
        top: top * -_this.ratios.y
      });
    };

    _this.handleDragStart = function (e) {
      _this.offsets = _this.getOffsets(e.pageX || e.changedTouches[0].pageX, e.pageY || e.changedTouches[0].pageY, _this.zoomImg.offsetLeft, _this.zoomImg.offsetTop);
      _this.zoomImg.addEventListener(_this.state.isTouch ? 'touchmove' : 'mousemove', _this.handleDragMove, { passive: false });

      if (!_this.state.isTouch) {
        _this.eventPosition = {
          x: e.pageX,
          y: e.pageY
        };
      }
    };

    _this.handleDragMove = function (e) {
      e.preventDefault();
      e.stopPropagation();

      var left = (e.pageX || e.changedTouches[0].pageX) - _this.offsets.x;
      var top = (e.pageY || e.changedTouches[0].pageY) - _this.offsets.y;

      left = Math.max(Math.min(left, 0), (_this.zoomImg.offsetWidth - _this.bounds.width) * -1);
      top = Math.max(Math.min(top, 0), (_this.zoomImg.offsetHeight - _this.bounds.height) * -1);

      _this.setState({
        left: left,
        top: top
      });
    };

    _this.handleDragEnd = function (e) {
      _this.zoomImg.removeEventListener(_this.state.isTouch ? 'touchmove' : 'mousemove', _this.handleDragMove);

      if (!_this.state.isTouch) {
        var moveX = Math.abs(e.pageX - _this.eventPosition.x);
        var moveY = Math.abs(e.pageY - _this.eventPosition.y);

        _this.setState({
          isDragging: moveX > 5 || moveY > 5
        });
      }
    };

    _this.handleMouseLeave = function (e) {
      _this.state.currentMoveType === 'drag' && _this.state.isZoomed ? _this.handleDragEnd(e) : _this.handleClose();
    };

    _this.handleClose = function () {
      _this.zoomOut(function () {
        setTimeout(function () {
          _this.setDefaults();

          _this.setState({
            isActive: false,
            isTouch: false,
            isFullscreen: false,
            currentMoveType: _this.props.moveType
          });
        }, _this.props.fadeDuration);
      });
    };

    _this.initialMove = function (pageX, pageY) {
      _this.offsets = _this.getOffsets(window.pageXOffset, window.pageYOffset, -_this.bounds.left, -_this.bounds.top);

      _this.handleMouseMove({
        pageX: pageX,
        pageY: pageY
      });
    };

    _this.initialDragMove = function (pageX, pageY) {
      var initialPageX = (pageX - (window.pageXOffset + _this.bounds.left)) * -_this.ratios.x;
      var initialPageY = (pageY - (window.pageYOffset + _this.bounds.top)) * -_this.ratios.y;

      _this.bounds = _this.getBounds(_this.img, _this.state.isFullscreen);
      _this.offsets = _this.getOffsets(0, 0, 0, 0);

      _this.handleDragMove({
        changedTouches: [{
          pageX: initialPageX,
          pageY: initialPageY
        }],
        preventDefault: function preventDefault() {},
        stopPropagation: function stopPropagation() {}
      });
    };

    _this.zoomIn = function (pageX, pageY) {
      _this.setState({
        isZoomed: true
      }, function () {
        var initialMove = _this.state.currentMoveType === 'drag' ? _this.initialDragMove : _this.initialMove;
        initialMove(pageX, pageY);

        if (_this.props.afterZoomIn) {
          _this.props.afterZoomIn();
        }
      });
    };

    _this.zoomOut = function (callback) {
      _this.setState({
        isZoomed: false
      }, function () {
        if (_this.props.afterZoomOut) {
          _this.props.afterZoomOut();
        }

        if (callback) {
          callback();
        }
      });
    };

    _this.setDefaults = function () {
      _this.isLoaded = false;
      _this.onLoadCallback = null;
      _this.zoomImg = null;
      _this.bounds = {};
      _this.offsets = {};
      _this.ratios = {};
      _this.eventPosition = {};
    };

    _this.getBounds = function (img, isFullscreen) {
      if (isFullscreen) {
        return {
          width: window.innerWidth,
          height: window.innerHeight,
          left: 0,
          top: 0
        };
      }

      return img.getBoundingClientRect();
    };

    _this.getOffsets = function (pageX, pageY, left, top) {
      return {
        x: pageX - left,
        y: pageY - top
      };
    };

    _this.getRatios = function (bounds, zoomImg) {
      return {
        x: (zoomImg.offsetWidth - bounds.width) / bounds.width,
        y: (zoomImg.offsetHeight - bounds.height) / bounds.height
      };
    };

    _this.state = {
      isActive: props.startsActive === true ? true : false,
      isTouch: false,
      isZoomed: false,
      isFullscreen: false,
      isDragging: false,
      currentMoveType: props.moveType,
      left: 0,
      top: 0
    };

    _this.setDefaults();
    return _this;
  }

  InnerImageZoom.prototype.render = function render() {
    var _this2 = this;

    var _props = this.props,
        src = _props.src,
        srcSet = _props.srcSet,
        sizes = _props.sizes,
        sources = _props.sources,
        zoomSrc = _props.zoomSrc,
        alt = _props.alt,
        fadeDuration = _props.fadeDuration,
        className = _props.className;


    var zoomImageProps = {
      src: zoomSrc || src,
      fadeDuration: this.state.isFullscreen ? 0 : fadeDuration,
      top: this.state.top,
      left: this.state.left,
      isZoomed: this.state.isZoomed,
      onLoad: this.handleLoad,
      onDragStart: this.handleDragStart,
      onDragEnd: this.handleDragEnd,
      onClose: this.state.isTouch ? this.handleClose : null
    };

    return react_default.a.createElement(
      'figure',
      {
        className: 'iiz ' + (this.state.currentMoveType === 'drag' ? 'iiz--drag' : '') + ' ' + (className ? className : ''),
        ref: function ref(el) {
          _this2.img = el;
        },
        onTouchStart: this.handleTouchStart,
        onClick: this.handleClick,
        onMouseEnter: this.state.isTouch ? null : this.handleMouseEnter,
        onMouseMove: this.state.currentMoveType === 'drag' || !this.state.isZoomed ? null : this.handleMouseMove,
        onMouseLeave: this.state.isTouch ? null : this.handleMouseLeave
      },
      react_default.a.createElement(components_Image, {
        src: src,
        srcSet: srcSet,
        sizes: sizes,
        sources: sources,
        alt: alt,
        fadeDuration: this.props.fadeDuration,
        isZoomed: this.state.isZoomed
      }),
      this.state.isActive && react_default.a.createElement(
        react["Fragment"],
        null,
        this.state.isFullscreen ? react_default.a.createElement(
          components_FullscreenPortal,
          { className: 'iiz__zoom-portal' },
          react_default.a.createElement(components_ZoomImage, zoomImageProps)
        ) : react_default.a.createElement(components_ZoomImage, zoomImageProps)
      ),
      !this.state.isZoomed && react_default.a.createElement('span', { className: 'iiz__btn iiz__hint' })
    );
  };

  return InnerImageZoom;
}(react["Component"]);

InnerImageZoom_InnerImageZoom.propTypes =  false ? undefined : {};

InnerImageZoom_InnerImageZoom.defaultProps = {
  moveType: 'pan',
  zoomType: 'click',
  zoomScale: 1,
  fadeDuration: 150,
  mobileBreakpoint: 640
};

/* harmony default export */ var es_InnerImageZoom_InnerImageZoom = (InnerImageZoom_InnerImageZoom);
// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/InnerImageZoom/index.js

/* harmony default export */ var es_InnerImageZoom = (es_InnerImageZoom_InnerImageZoom);
// CONCATENATED MODULE: ./node_modules/react-inner-image-zoom/es/index.js

/* harmony default export */ var es = (es_InnerImageZoom);

// EXTERNAL MODULE: ./src/util/likes.js
var likes = __webpack_require__(645);

// CONCATENATED MODULE: ./src/util/Modals/modalProductDetailsList.js








function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




 // requires a loader






var modalProductDetailsList_ProductDetailsList = /*#__PURE__*/function (_Component) {
  inherits_default()(ProductDetailsList, _Component);

  var _super = _createSuper(ProductDetailsList);

  function ProductDetailsList(props) {
    var _this;

    classCallCheck_default()(this, ProductDetailsList);

    _this = _super.call(this, props);
    _this.tab = _this.tab.bind(assertThisInitialized_default()(_this));
    _this.state = {
      pdf: false,
      pdfLink: "",
      activeTab: "2"
    };
    _this.openCatalogo = _this.openCatalogo.bind(assertThisInitialized_default()(_this));
    _this.closeCatalogo = _this.closeCatalogo.bind(assertThisInitialized_default()(_this));
    return _this;
  }

  createClass_default()(ProductDetailsList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        pdf: false,
        pdfLink: ""
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(nextProps) {
      if (nextProps.item !== this.props.item) {
        this.setState({
          activeTab: "2",
          pdf: false,
          pdfLink: ""
        });
      }
    }
  }, {
    key: "tab",
    value: function tab(_tab) {
      if (this.state.activeTab !== _tab) {
        this.setState({
          activeTab: _tab
        });
      }
    }
  }, {
    key: "openCatalogo",
    value: function openCatalogo(link) {
      if (screen.width > 1023) {
        this.setState({
          pdfLink: link,
          pdf: true
        });
      } else {
        window.open(link, "_blank");
      }
    }
  }, {
    key: "closeCatalogo",
    value: function closeCatalogo() {
      this.setState({
        pdfLink: "",
        pdf: false
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var item = this.props.item;
      var key = item[0].key;
      var clientes = this.props.ClienteStock[0];
      var typeProduct = this.props.typeProduct;
      return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], {
        isOpen: this.props.active,
        toggle: this.props.toggle,
        className: "modal-dialog2 theme-color-blueHeader2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        className: "theme-color-blueHeader2 pt-5",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), !this.state.pdf ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "6"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        className: "p-1",
        style: {
          alignItems: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(js["Carousel"], {
        infiniteLoop: true,
        autoPlay: false,
        showThumbs: false,
        showIndicators: false,
        stopOnHover: true,
        showStatus: false,
        className: "carrouselWidth"
      }, item[0].Multimedia[1] ? item[0].Multimedia[1].video.map(function (picture, index) {
        return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("video", {
          className: "react-player imgDetails",
          key: index + "video",
          id: "my-video" + index,
          controls: true,
          width: "100%",
          height: "100%",
          "data-setup": "{}",
          autoPlay: true,
          muted: true
        }, /*#__PURE__*/react_default.a.createElement("source", {
          src: picture.img,
          type: picture.fileType
        })));
      }) : /*#__PURE__*/react_default.a.createElement(es, {
        moveType: "drag",
        src: item[0].Multimedia[0].imagen[0].img,
        className: "imgDetails p-2",
        alt: "logo"
      }), item[0].Multimedia[0].imagen.map(function (picture, index) {
        return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(es, {
          moveType: "drag",
          src: picture.img,
          className: "imgDetails p-2",
          alt: "logo"
        }));
      })), /*#__PURE__*/react_default.a.createElement(likes["default"], {
        item: key
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "1rem 0px 0px",
          width: "100%",
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["k" /* CardTitle */], {
        className: "mb-1",
        style: {
          fontWeight: "bold"
        }
      }, "Producto: ", item[0].NombreProducto.toUpperCase()), /*#__PURE__*/react_default.a.createElement("a", {
        style: {
          padding: "0px"
        },
        href: "https://wa.link/xbr57j",
        rel: "noopener noreferrer",
        target: "_blank"
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "whatsapp-contacto"
      }), " ")))), typeProduct == "Proximas" ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "6"
      }, item[0].Multimedia[2] ? /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          padding: "1rem 0 1rem 0",
          width: "100%",
          textAlign: "center"
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%"
        },
        className: "text-white arial m-2 bg-info"
      }, "Catalogos Disponibles"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        className: "m-2"
      }, /*#__PURE__*/react_default.a.createElement("tbody", null, item[0].Multimedia[2].pdf.map(function (item, index) {
        return /*#__PURE__*/react_default.a.createElement("tr", {
          className: "p-1 text-center",
          key: index + "catalogos"
        }, /*#__PURE__*/react_default.a.createElement("th", {
          scope: "row",
          style: {
            verticalAlign: "middle",
            backgroundColor: "#80808038"
          }
        }, "Catalogo: ", index + 1), /*#__PURE__*/react_default.a.createElement("td", null, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          className: "btn-style-Green btn btn-success btn-sm",
          onClick: function onClick() {
            window.open(item.img, "_blank");
          }
        }, "Descargar ", /*#__PURE__*/react_default.a.createElement("i", {
          className: "simple-icon-arrow-down-circle"
        }))));
      }))), /*#__PURE__*/react_default.a.createElement("hr", null), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%"
        },
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement("a", {
        target: "_blank",
        rel: "noopener noreferrer",
        href: "https://forms.gle/oMW3maYfm6pfkWXY6"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          width: "90%",
          padding: "2px",
          height: "3rem"
        },
        className: "btn-style-bluelight mr-2 btn btn-success btn-sm"
      }, "Ingresa a la encuesta de productos Aqu\xED")), /*#__PURE__*/react_default.a.createElement("i", {
        id: "survey",
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "survey"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "La finalidad de la encuesta es saber que productos son del inter\xE9s del publico para poder negociar con los proveedores,")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%"
        },
        className: "mb-2"
      }, /*#__PURE__*/react_default.a.createElement("a", {
        target: "_blank",
        rel: "noopener noreferrer",
        href: "https://fb.me/e/1Ua4kHkWT"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          width: "90%",
          padding: "2px",
          height: "3rem"
        },
        className: "btn-style-blue mr-2 btn btn-success btn-sm"
      }, "\xDAnete aqu\xED al taller GRATUITO ", /*#__PURE__*/react_default.a.createElement("br", null), " de Importaci\xF3n del 23-01")), /*#__PURE__*/react_default.a.createElement("i", {
        id: "event",
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "event"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, "Taller online donde se dar\xE1n tips para t\xFA primera importaci\xF3n")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          width: "100%"
        },
        className: "text-white arial m-2 bg-danger"
      }, "Importante : Cierre de pedidos Lunes 25 de enero")) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        className: "text-center arial tab1 ".concat(classnames_default()({
          active2: this.state.activeTab === "2"
        })),
        onClick: function onClick() {
          _this2.tab("2");
        }
      }, "Especificaciones"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["M" /* TabContent */], {
        className: "mx-auto",
        activeTab: this.state.activeTab
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto pt-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "p-0",
        xs: "12",
        md: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "p-1",
        style: {
          alignItems: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          padding: "1rem 0 1rem 0",
          width: "100%",
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], null, /*#__PURE__*/react_default.a.createElement("tbody", null, item[0].Caracteristicas.map(function (item) {
        return /*#__PURE__*/react_default.a.createElement("tr", {
          className: "p-0 text-center",
          key: item.key + "Especificaciones2"
        }, /*#__PURE__*/react_default.a.createElement("th", {
          scope: "row",
          style: {
            fontWeight: "bold",
            backgroundColor: "#80808038",
            width: "7rem"
          }
        }, item.nombre), /*#__PURE__*/react_default.a.createElement("td", null, item.descripción));
      }))))))), " ")))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "6"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "6",
        md: "6",
        className: "text-center arial tab1 ".concat(classnames_default()({
          active2: this.state.activeTab === "2"
        })),
        onClick: function onClick() {
          _this2.tab("2");
        }
      }, "Importadores asociados"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "6",
        md: "6",
        className: "text-center arial tab1 ".concat(classnames_default()({
          active2: this.state.activeTab === "1"
        })),
        onClick: function onClick() {
          _this2.tab("1");
        }
      }, "Especificaciones")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["M" /* TabContent */], {
        className: "mx-auto ",
        activeTab: this.state.activeTab
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto pt-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "p-0",
        xs: "12",
        md: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "p-1",
        style: {
          alignItems: "center"
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          padding: "1rem 0 1rem 0",
          width: "100%",
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], null, /*#__PURE__*/react_default.a.createElement("tbody", null, item[0].Caracteristicas.map(function (item) {
        return /*#__PURE__*/react_default.a.createElement("tr", {
          className: "p-0 text-center",
          key: item.key + "Especificaciones1"
        }, /*#__PURE__*/react_default.a.createElement("th", {
          scope: "row",
          style: {
            fontWeight: "bold",
            backgroundColor: "#80808038"
          }
        }, item.nombre), /*#__PURE__*/react_default.a.createElement("td", null, item.descripción));
      })))))))), " "), /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "mx-auto pt-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], defineProperty_default()({
        className: "p-0",
        xs: "12",
        md: "12"
      }, "className", "text-center"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], null, /*#__PURE__*/react_default.a.createElement("thead", null, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", null, "Contacto"), /*#__PURE__*/react_default.a.createElement("th", null, "Pa\xEDs"), /*#__PURE__*/react_default.a.createElement("th", null, "Provincia"), /*#__PURE__*/react_default.a.createElement("th", null, "Telefono"))), /*#__PURE__*/react_default.a.createElement("tbody", null, clientes.map(function (item) {
        return /*#__PURE__*/react_default.a.createElement("tr", {
          className: "p-0 text-center",
          key: item.key + "clientes"
        }, /*#__PURE__*/react_default.a.createElement("th", {
          scope: "row",
          style: {
            fontWeight: "bold",
            backgroundColor: "#80808038"
          }
        }, item.contacto), /*#__PURE__*/react_default.a.createElement("td", null, item.Pais), /*#__PURE__*/react_default.a.createElement("td", null, item.Provincia), /*#__PURE__*/react_default.a.createElement("td", null, item.telefono));
      })))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        className: "p-2 text-center"
      }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        className: "btn-style m-1 btn btn-success btn-sm",
        onClick: this.props.toggle
      }, "Volver"))) : "")));
    }
  }]);

  return ProductDetailsList;
}(react["Component"]);

/* harmony default export */ var modalProductDetailsList = __webpack_exports__["default"] = (modalProductDetailsList_ProductDetailsList);

/***/ })

}]);
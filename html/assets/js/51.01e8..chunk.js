(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[51],{

/***/ 1120:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

var _interopRequireWildcard = __webpack_require__(141);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var React = _interopRequireWildcard(__webpack_require__(1));

var _createSvgIcon = _interopRequireDefault(__webpack_require__(153));

var _default = (0, _createSvgIcon.default)( /*#__PURE__*/React.createElement("path", {
  d: "M11.67 3.87L9.9 2.1 0 12l9.9 9.9 1.77-1.77L3.54 12z"
}), 'ArrowBackIos');

exports.default = _default;

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var isProduction = "production" === 'production';
function warning(condition, message) {
  if (!isProduction) {
    if (condition) {
      return;
    }

    var text = "Warning: " + message;

    if (typeof console !== 'undefined') {
      console.warn(text);
    }

    try {
      throw Error(text);
    } catch (x) {}
  }
}

/* harmony default export */ __webpack_exports__["a"] = (warning);


/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function toVal(mix) {
	var k, y, str='';

	if (typeof mix === 'string' || typeof mix === 'number') {
		str += mix;
	} else if (typeof mix === 'object') {
		if (Array.isArray(mix)) {
			for (k=0; k < mix.length; k++) {
				if (mix[k]) {
					if (y = toVal(mix[k])) {
						str && (str += ' ');
						str += y;
					}
				}
			}
		} else {
			for (k in mix) {
				if (mix[k]) {
					str && (str += ' ');
					str += k;
				}
			}
		}
	}

	return str;
}

/* harmony default export */ __webpack_exports__["a"] = (function () {
	var i=0, tmp, x, str='';
	while (i < arguments.length) {
		if (tmp = arguments[i++]) {
			if (x = toVal(tmp)) {
				str && (str += ' ');
				str += x
			}
		}
	}
	return str;
});


/***/ }),

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (true) {
  module.exports = __webpack_require__(156);
} else {}


/***/ }),

/***/ 1525:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ PedidosJunior; });

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(845);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/CssBaseline/CssBaseline.js
var CssBaseline = __webpack_require__(1470);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Box/Box.js + 12 modules
var Box = __webpack_require__(1517);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Typography/Typography.js
var Typography = __webpack_require__(1451);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Container/Container.js
var Container = __webpack_require__(1471);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Grid/Grid.js
var Grid = __webpack_require__(1472);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Paper/Paper.js
var Paper = __webpack_require__(1440);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Link/Link.js
var Link = __webpack_require__(1468);

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__(79);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// EXTERNAL MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/Nav/NavImportadores.js + 1 modules
var NavImportadores = __webpack_require__(306);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Card/Card.js
var Card = __webpack_require__(1492);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/CardContent/CardContent.js
var CardContent = __webpack_require__(1493);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/extends.js
var esm_extends = __webpack_require__(4);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js
var objectWithoutProperties = __webpack_require__(14);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/clsx/dist/clsx.m.js
var clsx_m = __webpack_require__(143);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/withStyles.js + 1 modules
var withStyles = __webpack_require__(145);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/CardMedia/CardMedia.js







var styles = {
  /* Styles applied to the root element. */
  root: {
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
  },

  /* Styles applied to the root element if `component="video, audio, picture, iframe, or img"`. */
  media: {
    width: '100%'
  },

  /* Styles applied to the root element if `component="picture or img"`. */
  img: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: 'cover'
  }
};
var MEDIA_COMPONENTS = ['video', 'audio', 'picture', 'iframe', 'img'];
var CardMedia_CardMedia = /*#__PURE__*/react["forwardRef"](function CardMedia(props, ref) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      _props$component = props.component,
      Component = _props$component === void 0 ? 'div' : _props$component,
      image = props.image,
      src = props.src,
      style = props.style,
      other = Object(objectWithoutProperties["a" /* default */])(props, ["children", "classes", "className", "component", "image", "src", "style"]);

  var isMediaComponent = MEDIA_COMPONENTS.indexOf(Component) !== -1;
  var composedStyle = !isMediaComponent && image ? Object(esm_extends["a" /* default */])({
    backgroundImage: "url(\"".concat(image, "\")")
  }, style) : style;
  return /*#__PURE__*/react["createElement"](Component, Object(esm_extends["a" /* default */])({
    className: Object(clsx_m["a" /* default */])(classes.root, className, isMediaComponent && classes.media, "picture img".indexOf(Component) !== -1 && classes.img),
    ref: ref,
    style: composedStyle,
    src: isMediaComponent ? image || src : undefined
  }, other), children);
});
 false ? undefined : void 0;
/* harmony default export */ var esm_CardMedia_CardMedia = (Object(withStyles["a" /* default */])(styles, {
  name: 'MuiCardMedia'
})(CardMedia_CardMedia));
// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/List/List.js
var List = __webpack_require__(1442);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/ListItem/ListItem.js
var ListItem = __webpack_require__(1448);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/colors/green.js
var green = __webpack_require__(43);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/colors/red.js
var red = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/ListItemText/ListItemText.js
var ListItemText = __webpack_require__(1478);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/List/ListContext.js
var ListContext = __webpack_require__(580);

// CONCATENATED MODULE: ./node_modules/@material-ui/core/esm/ListItemAvatar/ListItemAvatar.js







var ListItemAvatar_styles = {
  /* Styles applied to the root element. */
  root: {
    minWidth: 56,
    flexShrink: 0
  },

  /* Styles applied to the root element when the parent `ListItem` uses `alignItems="flex-start"`. */
  alignItemsFlexStart: {
    marginTop: 8
  }
};
/**
 * A simple wrapper to apply `List` styles to an `Avatar`.
 */

var ListItemAvatar_ListItemAvatar = /*#__PURE__*/react["forwardRef"](function ListItemAvatar(props, ref) {
  var classes = props.classes,
      className = props.className,
      other = Object(objectWithoutProperties["a" /* default */])(props, ["classes", "className"]);

  var context = react["useContext"](ListContext["a" /* default */]);
  return /*#__PURE__*/react["createElement"]("div", Object(esm_extends["a" /* default */])({
    className: Object(clsx_m["a" /* default */])(classes.root, className, context.alignItems === 'flex-start' && classes.alignItemsFlexStart),
    ref: ref
  }, other));
});
 false ? undefined : void 0;
/* harmony default export */ var esm_ListItemAvatar_ListItemAvatar = (Object(withStyles["a" /* default */])(ListItemAvatar_styles, {
  name: 'MuiListItemAvatar'
})(ListItemAvatar_ListItemAvatar));
// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Avatar/Avatar.js + 1 modules
var Avatar = __webpack_require__(1537);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/VerPedidos/campaingList.js













var useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      display: "flex",
      background: "white"
    },
    card: {
      height: "100%",
      display: "flex",
      flexDirection: "column",
      cursor: "pointer"
    },
    cardMedia: {
      paddingTop: "80%" // 16:9

    },
    cardContent: {
      flexGrow: 1
    }
  };
});
var ListStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      width: "100%",
      boxShadow: "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
      backgroundColor: theme.palette.background.paper
    },
    inline: {
      display: "grid"
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7)
    }
  };
});
function CampaignList(_ref) {
  var data = _ref.data,
      getJuniorByCampaing = _ref.getJuniorByCampaing,
      setCampaign = _ref.setCampaign;
  var classes = useStyles();
  var classesList = ListStyles();
  return /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, Object(Utils["m" /* isMovil */])() ? /*#__PURE__*/react_default.a.createElement(List["a" /* default */], {
    className: classesList.root
  }, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
    onClick: function onClick() {
      getJuniorByCampaing(data.id_campana);
      setCampaign(data);
    },
    alignItems: "flex-start"
  }, /*#__PURE__*/react_default.a.createElement(esm_ListItemAvatar_ListItemAvatar, null, /*#__PURE__*/react_default.a.createElement(Avatar["a" /* default */], {
    variant: "square",
    alt: data.name,
    src: data.portada,
    className: classesList.large
  })), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
    className: "pl-2",
    primary: data.name,
    secondary: /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      variant: "body1",
      className: classesList.inline
    }, data.description), /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
      component: "div",
      variant: "body2",
      className: classesList.inline,
      style: {
        color: data.statusCarga == 0 ? green["a" /* default */][500] : red["a" /* default */][500]
      }
    }, data.statusCarga == 0 ? "En transito" : "Cerrada"))
  }))) : /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react_default.a.createElement(Card["a" /* default */], {
    key: data.name + "data",
    onClick: function onClick() {
      getJuniorByCampaing(data.id_campana);
      setCampaign(data);
    },
    className: classes.card
  }, /*#__PURE__*/react_default.a.createElement(esm_CardMedia_CardMedia, {
    key: data.name + "img",
    className: classes.cardMedia,
    image: data.portada,
    title: data.name
  }), /*#__PURE__*/react_default.a.createElement(CardContent["a" /* default */], {
    className: classes.cardContent
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    gutterBottom: true,
    variant: "h5",
    component: "h5"
  }, data.description), /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], null, data.description), /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    style: {
      color: data.statusCarga == 0 ? green["a" /* default */][500] : red["a" /* default */][500]
    },
    gutterBottom: true,
    variant: "h6",
    component: "h6"
  }, data.statusCarga == 0 ? "En transito" : "Cerrada")))));
}
// EXTERNAL MODULE: ./src/util/dialogAlert.js
var dialogAlert = __webpack_require__(651);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Button/Button.js
var Button = __webpack_require__(1426);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ArrowBackIos.js
var ArrowBackIos = __webpack_require__(1120);
var ArrowBackIos_default = /*#__PURE__*/__webpack_require__.n(ArrowBackIos);

// EXTERNAL MODULE: ./src/hooks/useAbonos.js + 3 modules
var useAbonos = __webpack_require__(606);

// EXTERNAL MODULE: ./src/components/spinner/index.js
var spinner = __webpack_require__(58);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/VerPedidos/PedidosJunior.js





















var Orders = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(10), __webpack_require__.e(11), __webpack_require__.e(65)]).then(__webpack_require__.bind(null, 1532));
});
var JuniorsListPedidos = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(10), __webpack_require__.e(16), __webpack_require__.e(76)]).then(__webpack_require__.bind(null, 1509));
});
var ResumenAbonosListPedidos = /*#__PURE__*/react_default.a.lazy(function () {
  return __webpack_require__.e(/* import() */ 79).then(__webpack_require__.bind(null, 1510));
});

function Copyright() {
  return /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    variant: "body2",
    color: "textSecondary",
    align: "center"
  }, "Copyright © ", /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
    color: "inherit",
    href: "https://www.pic-cargo.com/"
  }, "PIC"), " ", new Date().getFullYear(), ".");
}

var drawerWidth = 240;
var PedidosJunior_useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      display: "flex",
      background: "white"
    },
    title: {
      flexGrow: 1
    },
    content: {
      flexGrow: 1,
      height: "100vh",
      overflow: "auto",
      marginTop: "50px"
    },
    container: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4)
    },
    paper: {
      padding: theme.spacing(0),
      display: "flex",
      overflow: "auto",
      flexDirection: "column"
    },
    fixedHeight: {
      height: "auto"
    },
    button: {
      margin: theme.spacing(1)
    }
  };
});
function PedidosJunior(props) {
  var classes = PedidosJunior_useStyles();

  var _useState = Object(react["useState"])(true),
      _useState2 = slicedToArray_default()(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var _useState3 = Object(react["useState"])(false),
      _useState4 = slicedToArray_default()(_useState3, 2),
      alert = _useState4[0],
      toggleAlert = _useState4[1];

  var _useState5 = Object(react["useState"])(true),
      _useState6 = slicedToArray_default()(_useState5, 2),
      selectJuniorToggle = _useState6[0],
      setSelectJuniorToggle = _useState6[1];

  var _useState7 = Object(react["useState"])([]),
      _useState8 = slicedToArray_default()(_useState7, 2),
      juniorList = _useState8[0],
      setJuniorList = _useState8[1];

  var _useState9 = Object(react["useState"])([]),
      _useState10 = slicedToArray_default()(_useState9, 2),
      selectedJunior = _useState10[0],
      setSelectedJunior = _useState10[1];

  var _useState11 = Object(react["useState"])([]),
      _useState12 = slicedToArray_default()(_useState11, 2),
      AbonosRestante = _useState12[0],
      setAbonosRestante = _useState12[1];

  var _useState13 = Object(react["useState"])(""),
      _useState14 = slicedToArray_default()(_useState13, 2),
      idSelected = _useState14[0],
      setIdSelected = _useState14[1];

  var _useState15 = Object(react["useState"])(""),
      _useState16 = slicedToArray_default()(_useState15, 2),
      Campaign = _useState16[0],
      setCampaign = _useState16[1];

  var fixedHeightPaper = Object(clsx_m["a" /* default */])(classes.paper, classes.fixedHeight);

  var _useState17 = Object(react["useState"])(false),
      _useState18 = slicedToArray_default()(_useState17, 2),
      toggleCampaignsSelected = _useState18[0],
      setToggleCampaignsSelected = _useState18[1];

  toggleCampaignsSelected;

  var _useState19 = Object(react["useState"])({
    message: "El Junior Seleccionado, no posee pedidos actualmente.",
    type: "error"
  }),
      _useState20 = slicedToArray_default()(_useState19, 2),
      alertData = _useState20[0],
      setAlertData = _useState20[1];

  var handleDrawerOpen = function handleDrawerOpen() {
    setOpen(!open);
  };

  var getJuniorOrdersUpdate = /*#__PURE__*/function () {
    var _ref = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee(e) {
      return regenerator_default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios_default.a.get("https://point.qreport.site/rep/junior/".concat(e, "/").concat(Campaign.id_campana)).then(function (res) {
                if (res.data.length > 0) {
                  setSelectedJunior(res.data);
                  setSelectJuniorToggle(false);
                } else {
                  setSelectJuniorToggle(false);
                  toggleAlert(true);
                }
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getJuniorOrdersUpdate(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var getJuniorOrders = /*#__PURE__*/function () {
    var _ref2 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee2(e) {
      return regenerator_default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return axios_default.a.get("https://point.qreport.site/rep/junior/".concat(props.userSeniorData[0].id, "/").concat(e)).then(function (res) {
                if (res.data.length > 0) {
                  setSelectedJunior(res.data);
                  setToggleCampaignsSelected(true);
                  setSelectJuniorToggle(false);
                } else {
                  setSelectJuniorToggle(false);
                  toggleAlert(true);
                }
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function getJuniorOrders(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();

  var getJuniorList = /*#__PURE__*/function () {
    var _ref3 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee3(e) {
      return regenerator_default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return axios_default.a.get("https://point.qreport.site/rep/campana/junior_with/".concat(e, "/").concat(props.userSeniorData[0].id)).then(function (res) {
                setJuniorList(res.data);
                setToggleCampaignsSelected(true);
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function getJuniorList(_x3) {
      return _ref3.apply(this, arguments);
    };
  }();

  var getAbonosRestante = /*#__PURE__*/function () {
    var _ref4 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee4(e) {
      return regenerator_default.a.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return axios_default.a.get("https://point.qreport.site/rep/campana/senior_with/s/".concat(props.userSeniorData[0].id)).then(function (res) {
                if (res.data.length > 0) {
                  var abonos = res.data.filter(function (item) {
                    if (item.id_campana == e) {
                      return item;
                    }
                  });
                  setAbonosRestante(abonos[0]);
                }
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4);
    }));

    return function getAbonosRestante(_x4) {
      return _ref4.apply(this, arguments);
    };
  }();

  var getJuniorByCampaing = /*#__PURE__*/function () {
    var _ref5 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee5(e) {
      return regenerator_default.a.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              if (props.userSeniorData.length === 1) {
                getJuniorOrders(e);
              } else {
                getJuniorList(e);
                getAbonosRestante(e);
              }

            case 1:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5);
    }));

    return function getJuniorByCampaing(_x5) {
      return _ref5.apply(this, arguments);
    };
  }();

  var selectJunior = /*#__PURE__*/function () {
    var _ref6 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee6(e) {
      return regenerator_default.a.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.next = 2;
              return axios_default.a.get("https://point.qreport.site/rep/junior/".concat(e, "/").concat(Campaign.id_campana)).then(function (res) {
                var data = res.data;

                if (data.length > 0) {
                  setIdSelected(e);
                  setSelectedJunior(data);
                  setSelectJuniorToggle(false);
                } else {
                  toggleAlert(true);
                }
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6);
    }));

    return function selectJunior(_x6) {
      return _ref6.apply(this, arguments);
    };
  }();

  function updateData() {
    if (props.userSeniorData.length == 1) {
      getJuniorOrders(Campaign.id_campana);
    } else {
      getJuniorOrdersUpdate(idSelected);
    }
  }

  Object(react["useEffect"])(function () {
    if (screen.width <= 769) {
      setOpen(!open);
    }
  }, []);

  var _useGetCampaigns = Object(useAbonos["d" /* useGetCampaigns */])({
    idSenior: props.userSeniorData[0].id,
    tipo: props.userSeniorData.length
  }),
      loadingCampaigns = _useGetCampaigns.loadingCampaigns,
      Campaigns = _useGetCampaigns.Campaigns;

  return /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react_default.a.createElement(CssBaseline["a" /* default */], null), /*#__PURE__*/react_default.a.createElement(NavImportadores["a" /* default */], {
    userSeniorData: props.userSeniorData,
    handleLogout: props.handleLogout,
    handleDrawerOpen: handleDrawerOpen,
    open: open
  }), /*#__PURE__*/react_default.a.createElement("main", {
    className: classes.content
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    component: "h2",
    variant: "h5"
  }, "Ver Pedidos"), /*#__PURE__*/react_default.a.createElement(Container["a" /* default */], {
    maxWidth: "xxl",
    className: classes.container
  }, toggleCampaignsSelected ? /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    container: true,
    spacing: 3
  }, !selectJuniorToggle && props.userSeniorData.length > 1 && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 12
  }, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    variant: "contained",
    color: "primary",
    className: classes.button,
    startIcon: /*#__PURE__*/react_default.a.createElement(ArrowBackIos_default.a, null),
    onClick: function onClick() {
      return setSelectJuniorToggle(true);
    }
  }, "Volver")), selectJuniorToggle && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 12
  }, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    variant: "contained",
    color: "primary",
    className: classes.button,
    startIcon: /*#__PURE__*/react_default.a.createElement(ArrowBackIos_default.a, null),
    onClick: function onClick() {
      return setToggleCampaignsSelected(false);
    }
  }, "Volver")), Campaigns.length > 0 && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    align: "left",
    item: true,
    xs: 12
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    color: "secondary",
    component: "p",
    variant: "subtitle2"
  }, Campaign.description)), selectJuniorToggle && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    md: props.userSeniorData.length > 1 && AbonosRestante && AbonosRestante.abono > 0 ? 8 : 12,
    xs: 12,
    sm: 12
  }, /*#__PURE__*/react_default.a.createElement(Paper["a" /* default */], {
    className: fixedHeightPaper
  }, /*#__PURE__*/react_default.a.createElement(JuniorsListPedidos, {
    data: juniorList,
    selectJunior: selectJunior,
    seniorID: props.userSeniorData[0].id,
    AbonosRestante: AbonosRestante
  }))), selectJuniorToggle && props.userSeniorData.length > 1 && AbonosRestante && AbonosRestante.abono > 0 && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    md: props.userSeniorData.length > 1 ? 4 : 12,
    xs: 12,
    sm: 12
  }, /*#__PURE__*/react_default.a.createElement(Paper["a" /* default */], {
    className: fixedHeightPaper
  }, /*#__PURE__*/react_default.a.createElement(ResumenAbonosListPedidos, {
    total: AbonosRestante
  }))), !selectJuniorToggle && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 12
  }, /*#__PURE__*/react_default.a.createElement(Paper["a" /* default */], {
    className: fixedHeightPaper
  }, /*#__PURE__*/react_default.a.createElement(Orders, {
    setIdSelected: setIdSelected,
    updateData: updateData,
    setSelectedJunior: setSelectedJunior,
    selectedJunior: selectedJunior
  })))) : /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    container: true,
    spacing: 4
  }, /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 12,
    sm: 12,
    md: 12
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    component: "h2",
    color: "primary",
    variant: "h6"
  }, Campaigns.length > 0 ? "Seleccione una campaña" : "No tiene pedidos")), Campaigns.map(function (card) {
    return /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
      item: true,
      key: card.id_campana,
      xs: 12,
      sm: 6,
      md: 4
    }, /*#__PURE__*/react_default.a.createElement(CampaignList, {
      data: card,
      getJuniorByCampaing: getJuniorByCampaing,
      setCampaign: setCampaign
    }));
  }), loadingCampaigns && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 12,
    sm: 12,
    md: 12
  }, /*#__PURE__*/react_default.a.createElement(spinner["a" /* default */], null))), /*#__PURE__*/react_default.a.createElement(Box["a" /* default */], {
    pt: 4
  }, /*#__PURE__*/react_default.a.createElement(Copyright, null))), /*#__PURE__*/react_default.a.createElement(dialogAlert["a" /* default */], {
    open: alert,
    setAlert: toggleAlert,
    type: alertData.type,
    message: alertData.message
  })));
}

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?
Symbol.for("react.suspense_list"):60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.block"):60121,w=b?Symbol.for("react.fundamental"):60117,x=b?Symbol.for("react.responder"):60118,y=b?Symbol.for("react.scope"):60119;
function z(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case t:case r:case h:return a;default:return u}}case d:return u}}}function A(a){return z(a)===m}exports.AsyncMode=l;exports.ConcurrentMode=m;exports.ContextConsumer=k;exports.ContextProvider=h;exports.Element=c;exports.ForwardRef=n;exports.Fragment=e;exports.Lazy=t;exports.Memo=r;exports.Portal=d;
exports.Profiler=g;exports.StrictMode=f;exports.Suspense=p;exports.isAsyncMode=function(a){return A(a)||z(a)===l};exports.isConcurrentMode=A;exports.isContextConsumer=function(a){return z(a)===k};exports.isContextProvider=function(a){return z(a)===h};exports.isElement=function(a){return"object"===typeof a&&null!==a&&a.$$typeof===c};exports.isForwardRef=function(a){return z(a)===n};exports.isFragment=function(a){return z(a)===e};exports.isLazy=function(a){return z(a)===t};
exports.isMemo=function(a){return z(a)===r};exports.isPortal=function(a){return z(a)===d};exports.isProfiler=function(a){return z(a)===g};exports.isStrictMode=function(a){return z(a)===f};exports.isSuspense=function(a){return z(a)===p};
exports.isValidElementType=function(a){return"string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===w||a.$$typeof===x||a.$$typeof===y||a.$$typeof===v)};exports.typeOf=z;


/***/ }),

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var reactIs = __webpack_require__(151);

/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
  childContextTypes: true,
  contextType: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDefaultProps: true,
  getDerivedStateFromError: true,
  getDerivedStateFromProps: true,
  mixins: true,
  propTypes: true,
  type: true
};
var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true
};
var FORWARD_REF_STATICS = {
  '$$typeof': true,
  render: true,
  defaultProps: true,
  displayName: true,
  propTypes: true
};
var MEMO_STATICS = {
  '$$typeof': true,
  compare: true,
  defaultProps: true,
  displayName: true,
  propTypes: true,
  type: true
};
var TYPE_STATICS = {};
TYPE_STATICS[reactIs.ForwardRef] = FORWARD_REF_STATICS;
TYPE_STATICS[reactIs.Memo] = MEMO_STATICS;

function getStatics(component) {
  // React v16.11 and below
  if (reactIs.isMemo(component)) {
    return MEMO_STATICS;
  } // React v16.12 and above


  return TYPE_STATICS[component['$$typeof']] || REACT_STATICS;
}

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = Object.prototype;
function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components
    if (objectPrototype) {
      var inheritedComponent = getPrototypeOf(sourceComponent);

      if (inheritedComponent && inheritedComponent !== objectPrototype) {
        hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
      }
    }

    var keys = getOwnPropertyNames(sourceComponent);

    if (getOwnPropertySymbols) {
      keys = keys.concat(getOwnPropertySymbols(sourceComponent));
    }

    var targetStatics = getStatics(targetComponent);
    var sourceStatics = getStatics(sourceComponent);

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i];

      if (!KNOWN_STATICS[key] && !(blacklist && blacklist[key]) && !(sourceStatics && sourceStatics[key]) && !(targetStatics && targetStatics[key])) {
        var descriptor = getOwnPropertyDescriptor(sourceComponent, key);

        try {
          // Avoid failures from read-only properties
          defineProperty(targetComponent, key, descriptor);
        } catch (e) {}
      }
    }
  }

  return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isBrowser */
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isBrowser = (typeof window === "undefined" ? "undefined" : _typeof(window)) === "object" && (typeof document === "undefined" ? "undefined" : _typeof(document)) === 'object' && document.nodeType === 9;

/* harmony default export */ __webpack_exports__["a"] = (isBrowser);


/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);



var now = Date.now();
var fnValuesNs = "fnValues" + now;
var fnRuleNs = "fnStyle" + ++now;

var functionPlugin = function functionPlugin() {
  return {
    onCreateRule: function onCreateRule(name, decl, options) {
      if (typeof decl !== 'function') return null;
      var rule = Object(jss__WEBPACK_IMPORTED_MODULE_0__[/* createRule */ "d"])(name, {}, options);
      rule[fnRuleNs] = decl;
      return rule;
    },
    onProcessStyle: function onProcessStyle(style, rule) {
      // We need to extract function values from the declaration, so that we can keep core unaware of them.
      // We need to do that only once.
      // We don't need to extract functions on each style update, since this can happen only once.
      // We don't support function values inside of function rules.
      if (fnValuesNs in rule || fnRuleNs in rule) return style;
      var fnValues = {};

      for (var prop in style) {
        var value = style[prop];
        if (typeof value !== 'function') continue;
        delete style[prop];
        fnValues[prop] = value;
      } // $FlowFixMe[prop-missing]


      rule[fnValuesNs] = fnValues;
      return style;
    },
    onUpdate: function onUpdate(data, rule, sheet, options) {
      var styleRule = rule; // $FlowFixMe[prop-missing]

      var fnRule = styleRule[fnRuleNs]; // If we have a style function, the entire rule is dynamic and style object
      // will be returned from that function.

      if (fnRule) {
        // Empty object will remove all currently defined props
        // in case function rule returns a falsy value.
        styleRule.style = fnRule(data) || {};

        if (false) { var prop; }
      } // $FlowFixMe[prop-missing]


      var fnValues = styleRule[fnValuesNs]; // If we have a fn values map, it is a rule with function values.

      if (fnValues) {
        for (var _prop in fnValues) {
          styleRule.prop(_prop, fnValues[_prop](data), options);
        }
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["a"] = (functionPlugin);


/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



var at = '@global';
var atPrefix = '@global ';

var GlobalContainerRule =
/*#__PURE__*/
function () {
  function GlobalContainerRule(key, styles, options) {
    this.type = 'global';
    this.at = at;
    this.rules = void 0;
    this.options = void 0;
    this.key = void 0;
    this.isProcessed = false;
    this.key = key;
    this.options = options;
    this.rules = new jss__WEBPACK_IMPORTED_MODULE_1__[/* RuleList */ "a"](Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));

    for (var selector in styles) {
      this.rules.add(selector, styles[selector]);
    }

    this.rules.process();
  }
  /**
   * Get a rule.
   */


  var _proto = GlobalContainerRule.prototype;

  _proto.getRule = function getRule(name) {
    return this.rules.get(name);
  }
  /**
   * Create and register rule, run plugins.
   */
  ;

  _proto.addRule = function addRule(name, style, options) {
    var rule = this.rules.add(name, style, options);
    if (rule) this.options.jss.plugins.onProcessRule(rule);
    return rule;
  }
  /**
   * Get index of a rule.
   */
  ;

  _proto.indexOf = function indexOf(rule) {
    return this.rules.indexOf(rule);
  }
  /**
   * Generates a CSS string.
   */
  ;

  _proto.toString = function toString() {
    return this.rules.toString();
  };

  return GlobalContainerRule;
}();

var GlobalPrefixedRule =
/*#__PURE__*/
function () {
  function GlobalPrefixedRule(key, style, options) {
    this.type = 'global';
    this.at = at;
    this.options = void 0;
    this.rule = void 0;
    this.isProcessed = false;
    this.key = void 0;
    this.key = key;
    this.options = options;
    var selector = key.substr(atPrefix.length);
    this.rule = options.jss.createRule(selector, style, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      parent: this
    }));
  }

  var _proto2 = GlobalPrefixedRule.prototype;

  _proto2.toString = function toString(options) {
    return this.rule ? this.rule.toString(options) : '';
  };

  return GlobalPrefixedRule;
}();

var separatorRegExp = /\s*,\s*/g;

function addScope(selector, scope) {
  var parts = selector.split(separatorRegExp);
  var scoped = '';

  for (var i = 0; i < parts.length; i++) {
    scoped += scope + " " + parts[i].trim();
    if (parts[i + 1]) scoped += ', ';
  }

  return scoped;
}

function handleNestedGlobalContainerRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;
  var rules = style ? style[at] : null;
  if (!rules) return;

  for (var name in rules) {
    sheet.addRule(name, rules[name], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: addScope(name, rule.selector)
    }));
  }

  delete style[at];
}

function handlePrefixedGlobalRule(rule, sheet) {
  var options = rule.options,
      style = rule.style;

  for (var prop in style) {
    if (prop[0] !== '@' || prop.substr(0, at.length) !== at) continue;
    var selector = addScope(prop.substr(at.length), rule.selector);
    sheet.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
      selector: selector
    }));
    delete style[prop];
  }
}
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */


function jssGlobal() {
  function onCreateRule(name, styles, options) {
    if (!name) return null;

    if (name === at) {
      return new GlobalContainerRule(name, styles, options);
    }

    if (name[0] === '@' && name.substr(0, atPrefix.length) === atPrefix) {
      return new GlobalPrefixedRule(name, styles, options);
    }

    var parent = options.parent;

    if (parent) {
      if (parent.type === 'global' || parent.options.parent && parent.options.parent.type === 'global') {
        options.scoped = false;
      }
    }

    if (options.scoped === false) {
      options.selector = name;
    }

    return null;
  }

  function onProcessRule(rule, sheet) {
    if (rule.type !== 'style' || !sheet) return;
    handleNestedGlobalContainerRule(rule, sheet);
    handlePrefixedGlobalRule(rule, sheet);
  }

  return {
    onCreateRule: onCreateRule,
    onProcessRule: onProcessRule
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssGlobal);


/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4);



var separatorRegExp = /\s*,\s*/g;
var parentRegExp = /&/g;
var refRegExp = /\$([\w-]+)/g;
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */

function jssNested() {
  // Get a function to be used for $ref replacement.
  function getReplaceRef(container, sheet) {
    return function (match, key) {
      var rule = container.getRule(key) || sheet && sheet.getRule(key);

      if (rule) {
        rule = rule;
        return rule.selector;
      }

       false ? undefined : void 0;
      return key;
    };
  }

  function replaceParentRefs(nestedProp, parentProp) {
    var parentSelectors = parentProp.split(separatorRegExp);
    var nestedSelectors = nestedProp.split(separatorRegExp);
    var result = '';

    for (var i = 0; i < parentSelectors.length; i++) {
      var parent = parentSelectors[i];

      for (var j = 0; j < nestedSelectors.length; j++) {
        var nested = nestedSelectors[j];
        if (result) result += ', '; // Replace all & by the parent or prefix & with the parent.

        result += nested.indexOf('&') !== -1 ? nested.replace(parentRegExp, parent) : parent + " " + nested;
      }
    }

    return result;
  }

  function getOptions(rule, container, prevOptions) {
    // Options has been already created, now we only increase index.
    if (prevOptions) return Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, prevOptions, {
      index: prevOptions.index + 1 // $FlowFixMe[prop-missing]

    });
    var nestingLevel = rule.options.nestingLevel;
    nestingLevel = nestingLevel === undefined ? 1 : nestingLevel + 1;

    var options = Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, rule.options, {
      nestingLevel: nestingLevel,
      index: container.indexOf(rule) + 1 // We don't need the parent name to be set options for chlid.

    });

    delete options.name;
    return options;
  }

  function onProcessStyle(style, rule, sheet) {
    if (rule.type !== 'style') return style;
    var styleRule = rule;
    var container = styleRule.options.parent;
    var options;
    var replaceRef;

    for (var prop in style) {
      var isNested = prop.indexOf('&') !== -1;
      var isNestedConditional = prop[0] === '@';
      if (!isNested && !isNestedConditional) continue;
      options = getOptions(styleRule, container, options);

      if (isNested) {
        var selector = replaceParentRefs(prop, styleRule.selector); // Lazily create the ref replacer function just once for
        // all nested rules within the sheet.

        if (!replaceRef) replaceRef = getReplaceRef(container, sheet); // Replace all $refs.

        selector = selector.replace(refRegExp, replaceRef);
        container.addRule(selector, style[prop], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])({}, options, {
          selector: selector
        }));
      } else if (isNestedConditional) {
        // Place conditional right after the parent rule to ensure right ordering.
        container.addRule(prop, {}, options) // Flow expects more options but they aren't required
        // And flow doesn't know this will always be a StyleRule which has the addRule method
        // $FlowFixMe[incompatible-use]
        // $FlowFixMe[prop-missing]
        .addRule(styleRule.key, style[prop], {
          selector: styleRule.selector
        });
      }

      delete style[prop];
    }

    return style;
  }

  return {
    onProcessStyle: onProcessStyle
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssNested);


/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(146);


var px = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.px : 'px';
var ms = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.ms : 'ms';
var percent = jss__WEBPACK_IMPORTED_MODULE_0__[/* hasCSSTOMSupport */ "f"] && CSS ? CSS.percent : '%';
/**
 * Generated jss-plugin-default-unit CSS property units
 *
 * @type object
 */

var defaultUnits = {
  // Animation properties
  'animation-delay': ms,
  'animation-duration': ms,
  // Background properties
  'background-position': px,
  'background-position-x': px,
  'background-position-y': px,
  'background-size': px,
  // Border Properties
  border: px,
  'border-bottom': px,
  'border-bottom-left-radius': px,
  'border-bottom-right-radius': px,
  'border-bottom-width': px,
  'border-left': px,
  'border-left-width': px,
  'border-radius': px,
  'border-right': px,
  'border-right-width': px,
  'border-top': px,
  'border-top-left-radius': px,
  'border-top-right-radius': px,
  'border-top-width': px,
  'border-width': px,
  'border-block': px,
  'border-block-end': px,
  'border-block-end-width': px,
  'border-block-start': px,
  'border-block-start-width': px,
  'border-block-width': px,
  'border-inline': px,
  'border-inline-end': px,
  'border-inline-end-width': px,
  'border-inline-start': px,
  'border-inline-start-width': px,
  'border-inline-width': px,
  'border-start-start-radius': px,
  'border-start-end-radius': px,
  'border-end-start-radius': px,
  'border-end-end-radius': px,
  // Margin properties
  margin: px,
  'margin-bottom': px,
  'margin-left': px,
  'margin-right': px,
  'margin-top': px,
  'margin-block': px,
  'margin-block-end': px,
  'margin-block-start': px,
  'margin-inline': px,
  'margin-inline-end': px,
  'margin-inline-start': px,
  // Padding properties
  padding: px,
  'padding-bottom': px,
  'padding-left': px,
  'padding-right': px,
  'padding-top': px,
  'padding-block': px,
  'padding-block-end': px,
  'padding-block-start': px,
  'padding-inline': px,
  'padding-inline-end': px,
  'padding-inline-start': px,
  // Mask properties
  'mask-position-x': px,
  'mask-position-y': px,
  'mask-size': px,
  // Width and height properties
  height: px,
  width: px,
  'min-height': px,
  'max-height': px,
  'min-width': px,
  'max-width': px,
  // Position properties
  bottom: px,
  left: px,
  top: px,
  right: px,
  inset: px,
  'inset-block': px,
  'inset-block-end': px,
  'inset-block-start': px,
  'inset-inline': px,
  'inset-inline-end': px,
  'inset-inline-start': px,
  // Shadow properties
  'box-shadow': px,
  'text-shadow': px,
  // Column properties
  'column-gap': px,
  'column-rule': px,
  'column-rule-width': px,
  'column-width': px,
  // Font and text properties
  'font-size': px,
  'font-size-delta': px,
  'letter-spacing': px,
  'text-decoration-thickness': px,
  'text-indent': px,
  'text-stroke': px,
  'text-stroke-width': px,
  'word-spacing': px,
  // Motion properties
  motion: px,
  'motion-offset': px,
  // Outline properties
  outline: px,
  'outline-offset': px,
  'outline-width': px,
  // Perspective properties
  perspective: px,
  'perspective-origin-x': percent,
  'perspective-origin-y': percent,
  // Transform properties
  'transform-origin': percent,
  'transform-origin-x': percent,
  'transform-origin-y': percent,
  'transform-origin-z': percent,
  // Transition properties
  'transition-delay': ms,
  'transition-duration': ms,
  // Alignment properties
  'vertical-align': px,
  'flex-basis': px,
  // Some random properties
  'shape-margin': px,
  size: px,
  gap: px,
  // Grid properties
  grid: px,
  'grid-gap': px,
  'row-gap': px,
  'grid-row-gap': px,
  'grid-column-gap': px,
  'grid-template-rows': px,
  'grid-template-columns': px,
  'grid-auto-rows': px,
  'grid-auto-columns': px,
  // Not existing properties.
  // Used to avoid issues with jss-plugin-expand integration.
  'box-shadow-x': px,
  'box-shadow-y': px,
  'box-shadow-blur': px,
  'box-shadow-spread': px,
  'font-line-height': px,
  'text-shadow-x': px,
  'text-shadow-y': px,
  'text-shadow-blur': px
};

/**
 * Clones the object and adds a camel cased property version.
 */
function addCamelCasedVersion(obj) {
  var regExp = /(-[a-z])/g;

  var replace = function replace(str) {
    return str[1].toUpperCase();
  };

  var newObj = {};

  for (var _key in obj) {
    newObj[_key] = obj[_key];
    newObj[_key.replace(regExp, replace)] = obj[_key];
  }

  return newObj;
}

var units = addCamelCasedVersion(defaultUnits);
/**
 * Recursive deep style passing function
 */

function iterate(prop, value, options) {
  if (value == null) return value;

  if (Array.isArray(value)) {
    for (var i = 0; i < value.length; i++) {
      value[i] = iterate(prop, value[i], options);
    }
  } else if (typeof value === 'object') {
    if (prop === 'fallbacks') {
      for (var innerProp in value) {
        value[innerProp] = iterate(innerProp, value[innerProp], options);
      }
    } else {
      for (var _innerProp in value) {
        value[_innerProp] = iterate(prop + "-" + _innerProp, value[_innerProp], options);
      }
    }
  } else if (typeof value === 'number' && !Number.isNaN(value)) {
    var unit = options[prop] || units[prop]; // Add the unit if available, except for the special case of 0px.

    if (unit && !(value === 0 && unit === px)) {
      return typeof unit === 'function' ? unit(value).toString() : "" + value + unit;
    }

    return value.toString();
  }

  return value;
}
/**
 * Add unit to numeric values.
 */


function defaultUnit(options) {
  if (options === void 0) {
    options = {};
  }

  var camelCasedOptions = addCamelCasedVersion(options);

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;

    for (var prop in style) {
      style[prop] = iterate(prop, style[prop], camelCasedOptions);
    }

    return style;
  }

  function onChangeValue(value, prop) {
    return iterate(prop, value, camelCasedOptions);
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (defaultUnit);


/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var css_vendor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(201);
/* harmony import */ var jss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(146);



/**
 * Add vendor prefix to a property name when needed.
 *
 * @api public
 */

function jssVendorPrefixer() {
  function onProcessRule(rule) {
    if (rule.type === 'keyframes') {
      var atRule = rule;
      atRule.at = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedKeyframes */ "a"])(atRule.at);
    }
  }

  function prefixStyle(style) {
    for (var prop in style) {
      var value = style[prop];

      if (prop === 'fallbacks' && Array.isArray(value)) {
        style[prop] = value.map(prefixStyle);
        continue;
      }

      var changeProp = false;
      var supportedProp = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedProperty */ "b"])(prop);
      if (supportedProp && supportedProp !== prop) changeProp = true;
      var changeValue = false;
      var supportedValue$1 = Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(supportedProp, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value));
      if (supportedValue$1 && supportedValue$1 !== value) changeValue = true;

      if (changeProp || changeValue) {
        if (changeProp) delete style[prop];
        style[supportedProp || prop] = supportedValue$1 || value;
      }
    }

    return style;
  }

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;
    return prefixStyle(style);
  }

  function onChangeValue(value, prop) {
    return Object(css_vendor__WEBPACK_IMPORTED_MODULE_0__[/* supportedValue */ "c"])(prop, Object(jss__WEBPACK_IMPORTED_MODULE_1__[/* toCssValue */ "g"])(value)) || value;
  }

  return {
    onProcessRule: onProcessRule,
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssVendorPrefixer);


/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Sort props by length.
 */
function jssPropsSort() {
  var sort = function sort(prop0, prop1) {
    if (prop0.length === prop1.length) {
      return prop0 > prop1 ? 1 : -1;
    }

    return prop0.length - prop1.length;
  };

  return {
    onProcessStyle: function onProcessStyle(style, rule) {
      if (rule.type !== 'style') return style;
      var newStyle = {};
      var props = Object.keys(style).sort(sort);

      for (var i = 0; i < props.length; i++) {
        newStyle[props[i]] = style[props[i]];
      }

      return newStyle;
    }
  };
}

/* harmony default export */ __webpack_exports__["a"] = (jssPropsSort);


/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/hyphenate-style-name/index.js
/* eslint-disable no-var, prefer-template */
var uppercasePattern = /[A-Z]/g
var msPattern = /^ms-/
var cache = {}

function toHyphenLower(match) {
  return '-' + match.toLowerCase()
}

function hyphenateStyleName(name) {
  if (cache.hasOwnProperty(name)) {
    return cache[name]
  }

  var hName = name.replace(uppercasePattern, toHyphenLower)
  return (cache[name] = msPattern.test(hName) ? '-' + hName : hName)
}

/* harmony default export */ var hyphenate_style_name = (hyphenateStyleName);

// CONCATENATED MODULE: ./node_modules/jss-plugin-camel-case/dist/jss-plugin-camel-case.esm.js


/**
 * Convert camel cased property names to dash separated.
 *
 * @param {Object} style
 * @return {Object}
 */

function convertCase(style) {
  var converted = {};

  for (var prop in style) {
    var key = prop.indexOf('--') === 0 ? prop : hyphenate_style_name(prop);
    converted[key] = style[prop];
  }

  if (style.fallbacks) {
    if (Array.isArray(style.fallbacks)) converted.fallbacks = style.fallbacks.map(convertCase);else converted.fallbacks = convertCase(style.fallbacks);
  }

  return converted;
}
/**
 * Allow camel cased property names by converting them back to dasherized.
 *
 * @param {Rule} rule
 */


function camelCase() {
  function onProcessStyle(style) {
    if (Array.isArray(style)) {
      // Handle rules like @font-face, which can have multiple styles in an array
      for (var index = 0; index < style.length; index++) {
        style[index] = convertCase(style[index]);
      }

      return style;
    }

    return convertCase(style);
  }

  function onChangeValue(value, prop, rule) {
    if (prop.indexOf('--') === 0) {
      return value;
    }

    var hyphenatedProp = hyphenate_style_name(prop); // There was no camel case in place

    if (prop === hyphenatedProp) return value;
    rule.prop(hyphenatedProp, value); // Core will ignore that property value we set the proper one above.

    return null;
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue
  };
}

/* harmony default export */ var jss_plugin_camel_case_esm = __webpack_exports__["a"] = (camelCase);


/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ NavImportadores; });

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/clsx/dist/clsx.m.js
var clsx_m = __webpack_require__(143);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(845);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Drawer/Drawer.js
var Drawer = __webpack_require__(1479);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/List/List.js
var List = __webpack_require__(1442);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Divider/Divider.js
var Divider = __webpack_require__(1481);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/IconButton/IconButton.js
var IconButton = __webpack_require__(1443);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ChevronLeft.js
var ChevronLeft = __webpack_require__(590);
var ChevronLeft_default = /*#__PURE__*/__webpack_require__.n(ChevronLeft);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ChevronRight.js
var ChevronRight = __webpack_require__(591);
var ChevronRight_default = /*#__PURE__*/__webpack_require__.n(ChevronRight);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ExitToApp.js
var ExitToApp = __webpack_require__(592);
var ExitToApp_default = /*#__PURE__*/__webpack_require__.n(ExitToApp);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/ListItem/ListItem.js
var ListItem = __webpack_require__(1448);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/ListItemIcon/ListItemIcon.js
var ListItemIcon = __webpack_require__(1453);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/ListItemText/ListItemText.js
var ListItemText = __webpack_require__(1478);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ShoppingCart.js
var ShoppingCart = __webpack_require__(496);
var ShoppingCart_default = /*#__PURE__*/__webpack_require__.n(ShoppingCart);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/People.js
var People = __webpack_require__(589);
var People_default = /*#__PURE__*/__webpack_require__.n(People);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/BarChart.js
var BarChart = __webpack_require__(588);
var BarChart_default = /*#__PURE__*/__webpack_require__.n(BarChart);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Search.js
var Search = __webpack_require__(587);
var Search_default = /*#__PURE__*/__webpack_require__.n(Search);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Link.js
var Link = __webpack_require__(50);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/Nav/ListItems.js









var mainListItems = /*#__PURE__*/react_default.a.createElement("div", null, " ", /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/PedidosJunior"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(ShoppingCart_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Ver Pedidos"
}))), /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/BuscarPedido"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(Search_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Buscar Pedido"
}))), /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/Abonos"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(BarChart_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Ver abonos"
}))), /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/JuniorDashboard"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(People_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Agregar Junior"
}))));
var mainListItemsJunior = /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/PedidosJunior"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(ShoppingCart_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Ver Pedidos"
}))), /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
  to: "/BuscarPedido"
}, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
  button: true
}, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(Search_default.a, {
  className: "color-white"
})), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
  className: "color-white",
  primary: "Buscar Pedido"
}))));
// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/Nav/NavImportadores.js


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { defineProperty_default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }















var drawerWidth = 240;
var useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      display: "flex",
      background: "white"
    },
    toolbar: {
      paddingRight: 24 // keep right padding when drawer closed

    },
    toolbarIcon: _objectSpread({
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px"
    }, theme.mixins.toolbar),
    drawerPaper: {
      position: "relative",
      whiteSpace: "nowrap",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      }),
      zIndex: "1049",
      color: "white",
      background: "#0d4674"
    },
    drawerPaperClose: defineProperty_default()({
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing(7)
    }, theme.breakpoints.up("sm"), {
      width: theme.spacing(9)
    }),
    paper: {
      padding: theme.spacing(2),
      display: "flex",
      overflow: "auto",
      flexDirection: "column"
    }
  };
});
function NavImportadores(props) {
  var classes = useStyles();
  return /*#__PURE__*/react_default.a.createElement(Drawer["a" /* default */], {
    variant: "permanent",
    classes: {
      paper: Object(clsx_m["a" /* default */])(classes.drawerPaper, !props.open && classes.drawerPaperClose)
    },
    open: props.open
  }, /*#__PURE__*/react_default.a.createElement("div", {
    className: classes.toolbarIcon
  }, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    onClick: props.handleDrawerOpen
  }, props.open ? /*#__PURE__*/react_default.a.createElement(ChevronLeft_default.a, {
    className: "color-white"
  }) : /*#__PURE__*/react_default.a.createElement(ChevronRight_default.a, {
    className: "color-white"
  }))), /*#__PURE__*/react_default.a.createElement(Divider["a" /* default */], null), /*#__PURE__*/react_default.a.createElement(List["a" /* default */], null, props.userSeniorData.length == 1 ? mainListItemsJunior : mainListItems), /*#__PURE__*/react_default.a.createElement(Divider["a" /* default */], null), /*#__PURE__*/react_default.a.createElement(List["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(ListItem["a" /* default */], {
    onClick: props.handleLogout,
    button: true
  }, /*#__PURE__*/react_default.a.createElement(ListItemIcon["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(ExitToApp_default.a, {
    className: "color-white"
  })), /*#__PURE__*/react_default.a.createElement(ListItemText["a" /* default */], {
    className: "color-white",
    primary: "Cerrar Sesi\xF3n"
  }))));
}

/***/ }),

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ useAbonos; });
__webpack_require__.d(__webpack_exports__, "b", function() { return /* binding */ useAbonosTotal; });
__webpack_require__.d(__webpack_exports__, "d", function() { return /* binding */ useGetCampaigns; });
__webpack_require__.d(__webpack_exports__, "c", function() { return /* binding */ useArticles; });

// UNUSED EXPORTS: useJuniorByCampaingSenior, useArticlesTop

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/toArray.js
var toArray = __webpack_require__(722);
var toArray_default = /*#__PURE__*/__webpack_require__.n(toArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// CONCATENATED MODULE: ./src/services/importadores/settings.js
var API_URL = "https://point.qreport.site";

// CONCATENATED MODULE: ./src/services/importadores/getGrupales.js




function getAbonos(_x, _x2) {
  return _getAbonos.apply(this, arguments);
}

function _getAbonos() {
  _getAbonos = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee(campaña, id) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            apiURL = "".concat(API_URL, "/abonos/cam/").concat(campaña, "/").concat(id);
            _context.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var amount = data;
                return amount;
              }
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getAbonos.apply(this, arguments);
}

function getAbonosTotal(_x3) {
  return _getAbonosTotal.apply(this, arguments);
}

function _getAbonosTotal() {
  _getAbonosTotal = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee2(id) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/campana/senior_with/s/").concat(id, " ");
            _context2.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var amount = data;
                return amount;
              }
            });

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getAbonosTotal.apply(this, arguments);
}

function getJuniorByCampaingSenior(_x4, _x5) {
  return _getJuniorByCampaingSenior.apply(this, arguments);
}

function _getJuniorByCampaingSenior() {
  _getJuniorByCampaingSenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee3(id_campaña, id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/campana/junior_with/").concat(id_campaña, "/").concat(id_senior, " ");
            _context3.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _getJuniorByCampaingSenior.apply(this, arguments);
}

function getJuniorBySenior(_x6) {
  return _getJuniorBySenior.apply(this, arguments);
}

function _getJuniorBySenior() {
  _getJuniorBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee4(id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/junior_with/s/").concat(id_senior, " ");
            _context4.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _getJuniorBySenior.apply(this, arguments);
}

function getJuniorOrdersBySenior(_x7) {
  return _getJuniorOrdersBySenior.apply(this, arguments);
}

function _getJuniorOrdersBySenior() {
  _getJuniorOrdersBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee5(id_senior) {
    var apiURL;
    return regenerator_default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            apiURL = "".concat(API_URL, "/rep/junior/").concat(id_senior, " ");
            _context5.next = 3;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var Junior = data;
                return Junior;
              }
            });

          case 3:
            return _context5.abrupt("return", _context5.sent);

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _getJuniorOrdersBySenior.apply(this, arguments);
}

function getCampaigns() {
  return _getCampaigns.apply(this, arguments);
}

function _getCampaigns() {
  _getCampaigns = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee6() {
    return regenerator_default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return fetch("".concat(API_URL, "/campana")).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context6.abrupt("return", _context6.sent);

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _getCampaigns.apply(this, arguments);
}

function getCampaignsBySenior(_x8) {
  return _getCampaignsBySenior.apply(this, arguments);
}

function _getCampaignsBySenior() {
  _getCampaignsBySenior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee7(idSenior) {
    return regenerator_default.a.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return fetch("".concat(API_URL, "/campana/senior/").concat(idSenior)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context7.abrupt("return", _context7.sent);

          case 3:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _getCampaignsBySenior.apply(this, arguments);
}

function getCampaignsByJunior(_x9) {
  return _getCampaignsByJunior.apply(this, arguments);
}

function _getCampaignsByJunior() {
  _getCampaignsByJunior = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee8(idSenior) {
    return regenerator_default.a.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return fetch("".concat(API_URL, "/campana/junior/").concat(idSenior)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context8.abrupt("return", _context8.sent);

          case 3:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));
  return _getCampaignsByJunior.apply(this, arguments);
}

function getOrdenByCampaigns(_x10) {
  return _getOrdenByCampaigns.apply(this, arguments);
}

function _getOrdenByCampaigns() {
  _getOrdenByCampaigns = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee9(id_campana) {
    return regenerator_default.a.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return fetch("".concat(API_URL, "/rep/orden/campana/").concat(id_campana)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context9.abrupt("return", _context9.sent);

          case 3:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));
  return _getOrdenByCampaigns.apply(this, arguments);
}

function getArticulosPage() {
  return _getArticulosPage.apply(this, arguments);
}

function _getArticulosPage() {
  _getArticulosPage = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee10() {
    var _ref,
        catalogo,
        _ref$limit,
        limit,
        _ref$page,
        page,
        apiURL,
        _args10 = arguments;

    return regenerator_default.a.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _ref = _args10.length > 0 && _args10[0] !== undefined ? _args10[0] : {}, catalogo = _ref.catalogo, _ref$limit = _ref.limit, limit = _ref$limit === void 0 ? 10 : _ref$limit, _ref$page = _ref.page, page = _ref$page === void 0 ? 0 : _ref$page;
            apiURL = "".concat(API_URL, "/articulos/pag/").concat(catalogo, "/").concat(limit, "/").concat(limit * page);
            _context10.next = 4;
            return fetch(apiURL).then(function (res) {
              return res.json();
            }).then(function (response) {
              var _response, _response2;

              var data = (_response = response, _response2 = toArray_default()(_response), _response);

              if (Array.isArray(data)) {
                return data;
              }
            });

          case 4:
            return _context10.abrupt("return", _context10.sent);

          case 5:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _getArticulosPage.apply(this, arguments);
}

function getTopArticulos(_x11) {
  return _getTopArticulos.apply(this, arguments);
}

function _getTopArticulos() {
  _getTopArticulos = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee11(catalogo) {
    return regenerator_default.a.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return fetch("".concat(API_URL, "/articulos/top/").concat(catalogo)).then(function (res) {
              return res.json();
            }).then(function (response) {
              var data = response;

              if (Array.isArray(data)) {
                var campañas = data;
                return campañas;
              }
            });

          case 2:
            return _context11.abrupt("return", _context11.sent);

          case 3:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));
  return _getTopArticulos.apply(this, arguments);
}
// CONCATENATED MODULE: ./src/services/importadores/index.js

// CONCATENATED MODULE: ./src/hooks/useAbonos.js



function useAbonos(_ref) {
  var campaña = _ref.campaña,
      id = _ref.id;

  var _useState = Object(react["useState"])([]),
      _useState2 = slicedToArray_default()(_useState, 2),
      abonos = _useState2[0],
      setAbonos = _useState2[1];

  var _useState3 = Object(react["useState"])(false),
      _useState4 = slicedToArray_default()(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  Object(react["useEffect"])(function () {
    setLoading(true);
    getAbonos(campaña, id).then(function (abono) {
      setAbonos(abono);
      setLoading(false);
    });
  }, [campaña, id]);
  return {
    loading: loading,
    abonos: abonos
  };
}
function useAbonosTotal(_ref2) {
  var id = _ref2.id;

  var _useState5 = Object(react["useState"])([]),
      _useState6 = slicedToArray_default()(_useState5, 2),
      abonosTotal = _useState6[0],
      setAbonosTotal = _useState6[1];

  var _useState7 = Object(react["useState"])(false),
      _useState8 = slicedToArray_default()(_useState7, 2),
      loadingTotal = _useState8[0],
      setLoading = _useState8[1];

  Object(react["useEffect"])(function () {
    setLoading(true);
    getAbonosTotal(id).then(function (abono) {
      setAbonosTotal(abono);
      setLoading(false);
    });
  }, [id]);
  return {
    loadingTotal: loadingTotal,
    abonosTotal: abonosTotal
  };
}
function useJuniorByCampaingSenior(_ref3) {
  var idCampaña = _ref3.idCampaña,
      idSenior = _ref3.idSenior,
      tipo = _ref3.tipo;

  var _useState9 = Object(react["useState"])([]),
      _useState10 = slicedToArray_default()(_useState9, 2),
      seniorList = _useState10[0],
      setSeniorList = _useState10[1];

  var _useState11 = Object(react["useState"])(false),
      _useState12 = slicedToArray_default()(_useState11, 2),
      loading = _useState12[0],
      setLoading = _useState12[1];

  if (tipo === 1) {
    Object(react["useEffect"])(function () {
      getJuniorOrdersBySenior(idSenior).then(function (senior) {
        setSeniorList(senior);

        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  } else {
    Object(react["useEffect"])(function () {
      getJuniorByCampaingSenior(idCampaña, idSenior).then(function (senior) {
        setSeniorList(senior);

        if (senior.length > 0) {
          setLoading(true);
        } else {
          setLoading(false);
        }
      });
    }, [idCampaña, idSenior]);
  }

  return {
    loading: loading,
    seniorList: seniorList
  };
}
function useGetCampaigns(_ref4) {
  var idSenior = _ref4.idSenior,
      tipo = _ref4.tipo;

  var _useState13 = Object(react["useState"])([]),
      _useState14 = slicedToArray_default()(_useState13, 2),
      Campaigns = _useState14[0],
      setCampaigns = _useState14[1];

  var _useState15 = Object(react["useState"])(false),
      _useState16 = slicedToArray_default()(_useState15, 2),
      loadingCampaigns = _useState16[0],
      setLoading = _useState16[1];

  if (tipo === 1) {
    Object(react["useEffect"])(function () {
      setLoading(true);
      getCampaignsByJunior(idSenior).then(function (campaña) {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  } else {
    Object(react["useEffect"])(function () {
      setLoading(true);
      getCampaignsBySenior(idSenior).then(function (campaña) {
        if (campaña.length > 0) {
          setCampaigns(campaña);
          setLoading(false);
        }
      });
    }, []);
  }

  return {
    loadingCampaigns: loadingCampaigns,
    Campaigns: Campaigns
  };
}
var INITIAL_PAGE = 0;
function useArticles(_ref5) {
  var catalogo = _ref5.catalogo;

  var _useState17 = Object(react["useState"])([]),
      _useState18 = slicedToArray_default()(_useState17, 2),
      articulo = _useState18[0],
      setArticulo = _useState18[1];

  var _useState19 = Object(react["useState"])(0),
      _useState20 = slicedToArray_default()(_useState19, 2),
      limite = _useState20[0],
      setLimite = _useState20[1];

  var _useState21 = Object(react["useState"])(false),
      _useState22 = slicedToArray_default()(_useState21, 2),
      loading = _useState22[0],
      setLoading = _useState22[1];

  var _useState23 = Object(react["useState"])(false),
      _useState24 = slicedToArray_default()(_useState23, 2),
      loadingNextPage = _useState24[0],
      setLoadingNextPage = _useState24[1];

  var _useState25 = Object(react["useState"])(INITIAL_PAGE),
      _useState26 = slicedToArray_default()(_useState25, 2),
      page = _useState26[0],
      setPage = _useState26[1];

  Object(react["useEffect"])(function () {
    setLoading(true); //Recuperamos la keyword del localStorage

    getArticulosPage({
      catalogo: catalogo
    }).then(function (articulo) {
      setArticulo(articulo);
      setLimite(articulo[0].limite);
      setLoading(false);
    });
  }, [catalogo]);
  Object(react["useEffect"])(function () {
    if (page !== INITIAL_PAGE && limite / articulo.length !== 1) {
      setLoadingNextPage(true);
      getArticulosPage({
        catalogo: catalogo,
        page: page
      }).then(function (nextarticulo) {
        setArticulo(function (prevarticulo) {
          return prevarticulo.concat(nextarticulo);
        });
        setLoadingNextPage(false);
      });
    }
  }, [catalogo, page]);
  return {
    loading: loading,
    loadingNextPage: loadingNextPage,
    articulo: articulo,
    setPage: setPage,
    limite: limite,
    page: page
  };
}
function useArticlesTop(catalogo) {
  var _useState27 = Object(react["useState"])([]),
      _useState28 = slicedToArray_default()(_useState27, 2),
      articulosTop = _useState28[0],
      setArticulo = _useState28[1];

  var _useState29 = Object(react["useState"])(false),
      _useState30 = slicedToArray_default()(_useState29, 2),
      loadingTop = _useState30[0],
      setLoading = _useState30[1];

  Object(react["useEffect"])(function () {
    setLoading(true); //Recuperamos la keyword del localStorage

    getTopArticulos(catalogo).then(function (articulosTop) {
      setArticulo(articulosTop);
      setLoading(false);
    });
  }, [catalogo]);
  return {
    loadingTop: loadingTop,
    articulosTop: articulosTop
  };
}

/***/ }),

/***/ 651:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomizedSnackbars; });
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(17);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1540);
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1522);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(845);






function Alert(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
    elevation: 6,
    variant: "filled"
  }, props));
}

var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"])(function (theme) {
  return {
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2)
      }
    }
  };
});
function CustomizedSnackbars(_ref) {
  var open = _ref.open,
      setAlert = _ref.setAlert,
      message = _ref.message,
      type = _ref.type;
  var classes = useStyles();

  var handleClose = function handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    setAlert(!open);
  }; //type: error, warning, info, success


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    open: open,
    autoHideDuration: 6000,
    onClose: handleClose
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Alert, {
    onClose: handleClose,
    severity: type
  }, message)));
}

/***/ })

}]);
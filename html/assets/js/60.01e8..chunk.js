(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[60],{

/***/ 1534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// CONCATENATED MODULE: ./src/util/formItems/FileUpload.js






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




var FileUpload_FileUpload = /*#__PURE__*/function (_Component) {
  inherits_default()(FileUpload, _Component);

  var _super = _createSuper(FileUpload);

  function FileUpload() {
    var _this;

    classCallCheck_default()(this, FileUpload);

    _this = _super.call(this);
    _this.state = {
      uploadValue: 0
    };
    return _this;
  }

  createClass_default()(FileUpload, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("progress", {
        value: this.props.uploadValue,
        max: "100"
      }, this.props.uploadValue, " %"), /*#__PURE__*/react_default.a.createElement("br", null), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "exampleCustomFileBrowser"
      }, "Elige archivos multimedia "), /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        type: "file",
        id: "exampleCustomFileBrowser",
        name: "customFile",
        label: "Elige archivos",
        onChange: this.props.onUpload
      })));
    }
  }]);

  return FileUpload;
}(react["Component"]);

/* harmony default export */ var formItems_FileUpload = (FileUpload_FileUpload);
// EXTERNAL MODULE: ./src/util/Firebase.js + 4 modules
var Firebase = __webpack_require__(11);

// EXTERNAL MODULE: ./src/routes/pages/sidebar/SideBar.js + 1 modules
var SideBar = __webpack_require__(607);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// CONCATENATED MODULE: ./src/routes/pages/seccionImportadores/ImpoAdmin.js







function ImpoAdmin_createSuper(Derived) { var hasNativeReflectConstruct = ImpoAdmin_isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function ImpoAdmin_isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }







var ModalProductDetails = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(4), __webpack_require__.e(6), __webpack_require__.e(8), __webpack_require__.e(12)]).then(__webpack_require__.bind(null, 853));
});
var timer;

var ImpoAdmin_ImpoAdmin = /*#__PURE__*/function (_Component) {
  inherits_default()(ImpoAdmin, _Component);

  var _super = ImpoAdmin_createSuper(ImpoAdmin);

  function ImpoAdmin() {
    var _this;

    classCallCheck_default()(this, ImpoAdmin);

    _this = _super.call(this);
    _this.state = {
      isOpenAlert: false,
      isOpenAlertRemove: false,
      isOpenAlertUpdate: false,
      img: [],
      fileType: "",
      nombreProducto: "",
      product: [],
      items: [],
      productEdit: [],
      uploadValue: 0,
      sidebarIsOpen: true,
      isOpenItem: false,
      isOpenItemEdit: false,
      toggleProductDetails: false,
      toggleProductDetailsdata: []
    };
    _this.breakPoints = [{
      width: 1,
      itemsToShow: 1
    }, {
      width: 550,
      itemsToShow: 2,
      itemsToScroll: 2,
      pagination: false
    }, {
      width: 850,
      itemsToShow: 3
    }, {
      width: 1150,
      itemsToShow: 4,
      itemsToScroll: 2
    }, {
      width: 1450,
      itemsToShow: 5
    }, {
      width: 1750,
      itemsToShow: 6
    }];
    _this.handleRemoveSpec = _this.handleRemoveSpec.bind(assertThisInitialized_default()(_this));
    _this.handleRemoveMultime = _this.handleRemoveMultime.bind(assertThisInitialized_default()(_this));
    _this.addItem = _this.addItem.bind(assertThisInitialized_default()(_this));
    _this.handleForm = _this.handleForm.bind(assertThisInitialized_default()(_this));
    _this.handleUpload = _this.handleUpload.bind(assertThisInitialized_default()(_this));
    _this.handleRemove = _this.handleRemove.bind(assertThisInitialized_default()(_this));
    _this.handleUpdate = _this.handleUpdate.bind(assertThisInitialized_default()(_this));
    _this.handleEdit = _this.handleEdit.bind(assertThisInitialized_default()(_this));
    _this.toggleSidebar = _this.toggleSidebar.bind(assertThisInitialized_default()(_this));
    _this.toggleItem = _this.toggleItem.bind(assertThisInitialized_default()(_this));
    _this.toggleItemUpdate = _this.toggleItemUpdate.bind(assertThisInitialized_default()(_this));
    _this.toggleProductDetails = _this.toggleProductDetails.bind(assertThisInitialized_default()(_this));
    _this.toggleProductDetailsOpen = _this.toggleProductDetailsOpen.bind(assertThisInitialized_default()(_this));
    _this.toggleAlert = _this.toggleAlert.bind(assertThisInitialized_default()(_this));
    return _this;
  }

  createClass_default()(ImpoAdmin, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (screen.width <= 769) {
        this.setState(function (prevState) {
          return {
            sidebarIsOpen: !prevState.sidebarIsOpen
          };
        });
      }

      var database = Firebase["a" /* firebaseConf */].database().ref("product");
      database.on("child_added", function (snapshot) {
        var key = [{
          key: snapshot.key
        }];
        var arreglo = key.concat(snapshot.val());
        var data = Object.assign(arreglo[1], arreglo[0]);

        _this2.setState({
          product: _this2.state.product.concat(data)
        });
      });
      database.on("child_removed", function (snapshot) {
        var eliminado = snapshot.key;

        var data = _this2.state.product.filter(function (item) {
          if (eliminado !== item.key) {
            return item;
          }
        });

        _this2.setState({
          product: data
        });

        _this2.renderLoginButton();
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "toggleSidebar",
    value: function toggleSidebar() {
      this.setState(function (prevState) {
        return {
          sidebarIsOpen: !prevState.sidebarIsOpen
        };
      });
    }
  }, {
    key: "toggleItem",
    value: function toggleItem() {
      this.setState(function (prevState) {
        return {
          isOpenItem: !prevState.isOpenItem
        };
      });
      this.setState({
        img: [],
        nombreProducto: "",
        fileType: "",
        items: [],
        uploadValue: 0
      });
    }
  }, {
    key: "toggleAlert",
    value: function toggleAlert() {
      this.setState(function (prevState) {
        return {
          isOpenAlert: !prevState.isOpenAlert
        };
      });
    }
  }, {
    key: "toggleProductDetails",
    value: function toggleProductDetails(e) {
      var key = e.target.value;
      var data = this.state.product.filter(function (item) {
        if (key == item.key) {
          return item;
        }
      });

      if (data.length > 0) {
        this.setState({
          toggleProductDetailsdata: data,
          toggleProductDetails: true
        });
      }
    }
  }, {
    key: "toggleProductDetailsOpen",
    value: function toggleProductDetailsOpen() {
      this.setState(function (prevState) {
        return {
          toggleProductDetails: !prevState.toggleProductDetails
        };
      });
    }
  }, {
    key: "toggleItemUpdate",
    value: function toggleItemUpdate() {
      this.setState({
        img: [],
        fileType: "",
        nombreProducto: "",
        items: [],
        productEdit: [],
        isOpenItemEdit: false
      });
    }
  }, {
    key: "handleRemove",
    value: function handleRemove(e) {
      var _this3 = this;

      Firebase["a" /* firebaseConf */].database().ref("/product/" + e.target.value).remove();
      this.setState({
        isOpenAlertRemove: true
      });
      timer = setTimeout(function () {
        _this3.setState({
          isOpenAlertRemove: false
        });
      }, 3000);
    }
  }, {
    key: "handleUpdate",
    value: function handleUpdate() {
      var _this4 = this;

      if (this.state.nombreProducto.length > 0 && this.state.img.length > 0 && this.state.items.length > 0) {
        var video = this.state.img.filter(function (item) {
          if (item.fileType.indexOf("video") == 0) {
            return item;
          }
        });
        var imagen = this.state.img.filter(function (item) {
          if (item.fileType.indexOf("video") !== 0 && item.fileType !== "application/pdf") {
            return item;
          }
        });
        var pdf = this.state.img.filter(function (item) {
          if (item.fileType == "application/pdf") {
            return item;
          }
        });
        var record = {
          Multimedia: [{
            imagen: imagen
          }, {
            video: video
          }, {
            pdf: pdf
          }],
          NombreProducto: this.state.nombreProducto,
          Caracteristicas: this.state.items,
          fileType: this.state.fileType,
          activo: true,
          Fecha: Object(Utils["h" /* getDateWithFormat */])() + " " + Object(Utils["g" /* getCurrentTime */])()
        };
        Firebase["a" /* firebaseConf */].database().ref("/product/" + this.state.productEdit).update(record).then(function () {
          _this4.setState({
            img: [],
            nombreProducto: "",
            fileType: "",
            items: [],
            uploadValue: 0,
            isOpenItemEdit: false,
            productEdit: [],
            isOpenAlertUpdate: true
          });

          timer = setTimeout(function () {
            _this4.setState({
              isOpenAlertUpdate: false
            });
          }, 3000);
        }).catch(function (error) {
          console.error("Error writing document: ", error);
        });
      } else {
        alert("Hay campos que requieren verificación");
      }
    }
  }, {
    key: "handleEdit",
    value: function handleEdit(e) {
      var data = this.state.product.filter(function (item) {
        if (e == item.key) {
          return item;
        }
      });
      var datos;

      if (data.length > 0) {
        if (data[0].Multimedia[2] && data[0].Multimedia[1]) {
          var datapre;
          datapre = data[0].Multimedia[2].pdf.concat(data[0].Multimedia[0].imagen);
          datos = data[0].Multimedia[1].video.concat(datapre);
        } else if (data[0].Multimedia[2]) {
          datos = data[0].Multimedia[2].pdf.concat(data[0].Multimedia[0].imagen);
        } else if (data[0].Multimedia[1]) {
          datos = data[0].Multimedia[1].video.concat(data[0].Multimedia[0].imagen);
        } else {
          datos = data[0].Multimedia[0].imagen;
        }

        ;
        this.setState({
          img: datos,
          nombreProducto: data[0].NombreProducto,
          fileType: data[0].fileType,
          items: data[0].Caracteristicas,
          productEdit: data[0].key,
          isOpenItemEdit: true
        });
      }
    }
  }, {
    key: "handleRemoveSpec",
    value: function handleRemoveSpec(e) {
      var key = e.target.value;
      var data = this.state.items.filter(function (item) {
        if (key != item.key) {
          return item;
        }
      });
      this.setState({
        items: data
      });
    }
  }, {
    key: "handleRemoveMultime",
    value: function handleRemoveMultime(value) {
      var data = this.state.img.filter(function (item, index) {
        if (value !== index) {
          return item;
        }
      });

      if (data.length > 0) {
        this.setState({
          img: data
        });
      }
    }
  }, {
    key: "addItem",
    value: function addItem(e) {
      e.preventDefault();
      var _e$target$elements = e.target.elements,
          Caracteristica = _e$target$elements.Caracteristica,
          Descripción = _e$target$elements.Descripción;

      if (Caracteristica.value.length > 0 && Descripción.value.length > 0) {
        var newItem = {
          nombre: Caracteristica.value,
          descripción: Descripción.value,
          key: Date.now()
        };
        this.setState(function (prevState) {
          return {
            items: prevState.items.concat(newItem)
          };
        });
        document.getElementById("Caracteristica").value = "";
        document.getElementById("Descripción").value = "";
      }

      this.renderLoginButton();
    }
  }, {
    key: "handleUpload",
    value: function handleUpload(e) {
      var _this5 = this;

      var file = e.target.files[0];
      var storageRef = Firebase["a" /* firebaseConf */].storage().ref("fotos/".concat(file.name));
      var task = storageRef.put(file); // Listener que se ocupa del estado de la carga del fichero

      task.on("state_changed", function (snapshot) {
        // Calculamos el porcentaje de tamaño transferido y actualizamos
        // el estado del componente con el valor
        var percentage = snapshot.bytesTransferred / snapshot.totalBytes * 100;

        _this5.setState({
          uploadValue: percentage
        });
      }, function (error) {
        // Ocurre un error
        console.error(error.message);
      }, function () {
        task.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          var data = [{
            img: downloadURL,
            fileType: file.type,
            key: Date.now()
          }];

          _this5.setState({
            img: _this5.state.img.concat(data),
            fileType: file.type
          });
        });
      });
    }
  }, {
    key: "handleForm",
    value: function handleForm() {
      var _this6 = this;

      if (this.state.nombreProducto.length > 0 && this.state.img.length > 0 && this.state.items.length > 0) {
        var video = this.state.img.filter(function (item) {
          if (item.fileType.indexOf("video") == 0) {
            return item;
          }
        });
        var imagen = this.state.img.filter(function (item) {
          if (item.fileType.indexOf("video") !== 0) {
            return item;
          }
        });
        var pdf = this.state.img.filter(function (item) {
          if (item.fileType == "application/pdf") {
            return item;
          }
        });
        var record = {
          Multimedia: [{
            imagen: imagen
          }, {
            video: video
          }, {
            pdf: pdf
          }],
          catalogos: pdf,
          NombreProducto: this.state.nombreProducto,
          Caracteristicas: this.state.items,
          fileType: this.state.fileType,
          activo: true,
          rates: 0,
          Fecha: Object(Utils["h" /* getDateWithFormat */])() + " " + Object(Utils["g" /* getCurrentTime */])()
        };
        var dbRef = Firebase["a" /* firebaseConf */].database().ref("product");
        var newProduct = dbRef.push();
        newProduct.set(record);
        this.setState({
          img: [],
          nombreProducto: "",
          fileType: "",
          items: [],
          uploadValue: 0,
          isOpenItem: false,
          isOpenAlert: true
        });
        timer = setTimeout(function () {
          _this6.setState({
            isOpenAlert: false
          });
        }, 3000);
      } else {
        alert("Hay campos que requieren verificación");
      }
    }
  }, {
    key: "renderLoginButton",
    value: function renderLoginButton() {
      var _this7 = this;

      if (true) {
        return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          onClick: this.toggleItem,
          style: {
            display: this.state.isOpenItem || this.state.isOpenItemEdit ? "none" : ""
          },
          className: "btn-style-Green btn btn-success btn-sm"
        }, "Agregar producto"), !this.state.isOpenItemEdit ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["m" /* Collapse */], {
          style: {
            display: this.state.isOpenItem ? "contents" : ""
          },
          isOpen: this.state.isOpenItem
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "8",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
          className: "mx-auto m-2 p-2",
          style: {
            borderRadius: "1em",
            backgroundColor: "#d7d7d7",
            border: "1px solid rgba(0, 0, 0, 0.18)",
            fontWeight: "bold"
          }
        }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "arial mx-auto pb-1"
        }, "IMAGENES DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(formItems_FileUpload, {
          uploadValue: this.state.uploadValue,
          onUpload: this.handleUpload,
          multiple: true
        }), this.state.img.length > 0 ? this.state.img.map(function (item, index) {
          return /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
            key: index,
            style: {
              display: "inline-grid"
            },
            className: "justify-content-between p-1"
          }, item.fileType == "video/mp4" ? /*#__PURE__*/react_default.a.createElement("video", {
            key: index + "video",
            id: "my-video" + index,
            style: {
              height: "4rem"
            },
            controls: true,
            "data-setup": "{}"
          }, /*#__PURE__*/react_default.a.createElement("source", {
            src: item.img,
            type: item.fileType
          })) : /*#__PURE__*/react_default.a.createElement("img", {
            key: index + "img",
            src: item.img,
            style: {
              height: "4rem"
            },
            alt: "logo"
          }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Red btn btn-success btn-sm",
            value: index,
            style: {
              lineHeight: "1"
            },
            onClick: function onClick() {
              return _this7.handleRemoveMultime(index);
            }
          }, "x"));
        }) : ""), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          className: "arial mx-auto pb-1",
          for: "nombreProducto"
        }, "NOMBRE DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "nombreProducto",
          type: "text",
          placeholder: "nombre de producto",
          value: this.state.nombreProducto,
          onChange: function onChange(e) {
            _this7.setState({
              nombreProducto: e.target.value
            });
          }
        })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "arial mx-auto pb-1"
        }, "CARACTERISTICAS DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
          onSubmit: this.addItem
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          for: "Caracteristica"
        }, "Tipo de Caracteristica"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "Caracteristica",
          id: "Caracteristica",
          type: "text",
          placeholder: "Caracteristica"
        })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          for: "Descripci\xF3n"
        }, "descripci\xF3n de Caracteristica"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "Descripci\xF3n",
          id: "Descripci\xF3n",
          type: "text",
          placeholder: "Descripci\xF3n"
        })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
          className: "login-form-button"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          type: "submit",
          className: "btn-style-blue btn btn-success btn-sm"
        }, "Agregar Caracteristica")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["B" /* ListGroup */], null, this.state.items.map(function (item, index) {
          return /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
            key: item.key,
            className: "justify-content-between"
          }, item.nombre, ": ", item.descripción, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Red btn btn-success btn-sm",
            value: item.key,
            style: {
              lineHeight: "1"
            },
            onClick: _this7.handleRemoveSpec
          }, "x"));
        })), " "))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto m-2 p-2"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          className: "btn-style-Green btn btn-success btn-sm",
          onClick: this.handleForm
        }, "Agregar producto"), " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          className: "btn-style-Red btn btn-success btn-sm",
          onClick: this.toggleItem
        }, "Cancelar"))) : "", this.state.img.length > 0 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["m" /* Collapse */], {
          style: {
            display: this.state.isOpenItemEdit ? "contents" : ""
          },
          isOpen: this.state.isOpenItemEdit
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "8",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
          className: "mx-auto m-2 p-2",
          style: {
            borderRadius: "1em",
            backgroundColor: "#d7d7d7",
            border: "1px solid rgba(0, 0, 0, 0.18)",
            fontWeight: "bold"
          }
        }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "arial mx-auto pb-1"
        }, "IMAGENES DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(formItems_FileUpload, {
          uploadValue: this.state.uploadValue,
          onUpload: this.handleUpload,
          multiple: true
        }), this.state.img.map(function (item, index) {
          return /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
            key: index,
            style: {
              display: "inline-grid"
            },
            className: "justify-content-between p-1"
          }, item.fileType == "video/mp4" ? /*#__PURE__*/react_default.a.createElement("video", {
            key: index + "video",
            id: "my-video" + index,
            style: {
              height: "4rem"
            },
            controls: true,
            "data-setup": "{}"
          }, /*#__PURE__*/react_default.a.createElement("source", {
            src: item.img,
            type: item.fileType
          })) : /*#__PURE__*/react_default.a.createElement("img", {
            key: index + "img",
            src: item.img,
            style: {
              height: "4rem"
            },
            alt: "logo"
          }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Red btn btn-success btn-sm",
            value: index,
            style: {
              lineHeight: "1"
            },
            onClick: function onClick() {
              return _this7.handleRemoveMultime(index);
            }
          }, "x"));
        })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          className: "arial mx-auto pb-1",
          for: "nombreProducto"
        }, "NOMBRE DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "nombreProducto",
          type: "text",
          placeholder: "nombre de producto",
          value: this.state.nombreProducto,
          onChange: function onChange(e) {
            _this7.setState({
              nombreProducto: e.target.value
            });
          }
        })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "arial mx-auto pb-1"
        }, "CARACTERISTICAS DE PRODUCTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
          onSubmit: this.addItem
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          for: "Caracteristica"
        }, "Tipo de Caracteristica"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "Caracteristica",
          id: "Caracteristica",
          type: "text",
          placeholder: "Caracteristica"
        })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
          for: "Descripci\xF3n"
        }, "descripci\xF3n de Caracteristica"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
          name: "Descripci\xF3n",
          id: "Descripci\xF3n",
          type: "text",
          placeholder: "Descripci\xF3n"
        })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
          className: "login-form-button"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          type: "submit",
          className: "btn-style-blue btn btn-success btn-sm"
        }, "Agregar Caracteristica")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, this.state.items.map(function (item, index) {
          return /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
            key: item.key,
            style: {
              display: "inline-grid"
            },
            className: "justify-content-between p-1"
          }, item.nombre, ": ", item.descripción, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Red btn btn-success btn-sm",
            value: item.key,
            style: {
              lineHeight: "1"
            },
            onClick: _this7.handleRemoveSpec
          }, "x"));
        })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto m-2 p-2"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          className: "btn-style-Green btn btn-success btn-sm",
          onClick: this.handleUpdate
        }, "Actualizar producto"), " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
          className: "btn-style-Red btn btn-success btn-sm",
          onClick: this.toggleItemUpdate
        }, "Cancelar"))) : "", /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "mx-auto pb-1"
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
          className: "mx-auto m-2 p-2",
          style: {
            borderTop: "1px solid rgba(0, 0, 0, 0.18)"
          }
        }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
          xs: "12",
          md: "12",
          sm: "12",
          className: "font2 mx-auto m-2"
        }, "PRODUCTOS ACTUALES"), this.state.product.map(function (picture, index) {
          return /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
            key: index,
            xs: "6",
            md: "4",
            lg: "3",
            xl: "2",
            sm: "6",
            className: "mb-2"
          }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
            className: "p-0",
            style: {
              border: "1px #8f8f8f solid",
              alignItems: "center",
              borderRadius: "1rem"
            }
          }, "  ", _this7.state.product.length > 0 && _this7.state.product[index].Multimedia[0] && /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
            className: "imagenProductos",
            top: true,
            width: "100%",
            src: _this7.state.product[index].Multimedia[0].imagen[0].img,
            alt: "imagenProductos"
          }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
            style: {
              padding: "0",
              width: "100%"
            }
          }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["k" /* CardTitle */], {
            style: {
              backgroundColor: "#d7d7d7",
              fontSize: "0.8rem",
              height: "2rem",
              margin: "0px",
              padding: "0px",
              bordeRadius: "1rem 1rem 0rem 0rem"
            }
          }, picture.NombreProducto), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Green m-1 btn btn-success btn-sm",
            value: picture.key,
            onClick: function onClick(e) {
              return _this7.toggleProductDetails(e);
            }
          }, "Ver"), " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style m-1 btn btn-success btn-sm",
            value: picture.key,
            onClick: function onClick() {
              return _this7.handleEdit(picture.key);
            }
          }, /*#__PURE__*/react_default.a.createElement("i", {
            className: "simple-icon-pencil"
          })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
            className: "btn-style-Red m-1 btn btn-success btn-sm",
            value: picture.key,
            onClick: function onClick(e) {
              return _this7.handleRemove(e);
            }
          }, "x"))));
        })))));
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement(react["Fragment"], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "p-0"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["K" /* NavbarToggler */], {
        className: "float-right display-sm",
        style: {
          backgroundColor: "#1370b9",
          border: "solid white 1px",
          top: "70px",
          left: "0",
          position: "fixed"
        },
        onClick: this.toggleSidebar
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        lg: this.state.sidebarIsOpen ? "3" : "1",
        md: this.state.sidebarIsOpen ? "4" : "1",
        sm: "12",
        className: "p-0 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(SideBar["a" /* default */], {
        toggle: this.toggleSidebar,
        isOpen: this.state.sidebarIsOpen
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        lg: this.state.sidebarIsOpen ? "9" : "11",
        md: this.state.sidebarIsOpen ? "8" : "11",
        sm: "12",
        className: "p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "p-1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement("h2", null, "IMPORTADORES GRUPALES - Panel de control")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "info",
        isOpen: this.state.isOpenAlert,
        toggle: this.toggleAlert
      }, "\xA1Producto Agregado Exitosamente!")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "info",
        isOpen: this.state.isOpenAlertRemove,
        toggle: this.toggleAlert
      }, "\xA1Producto Removido Exitosamente!")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "info",
        isOpen: this.state.isOpenAlertUpdate,
        toggle: this.toggleAlert
      }, "\xA1Producto Actualizado Exitosamente!")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: "p-1 mx-auto"
      }, this.renderLoginButton())))), this.state.toggleProductDetailsdata.length > 0 && /*#__PURE__*/react_default.a.createElement(ModalProductDetails, {
        active: this.state.toggleProductDetails,
        item: this.state.toggleProductDetailsdata,
        toggle: this.toggleProductDetailsOpen
      }));
    }
  }]);

  return ImpoAdmin;
}(react["Component"]);

/* harmony default export */ var seccionImportadores_ImpoAdmin = __webpack_exports__["default"] = (ImpoAdmin_ImpoAdmin);

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/@fortawesome/react-fontawesome/index.es.js
var index_es = __webpack_require__(5);

// EXTERNAL MODULE: ./node_modules/@fortawesome/free-solid-svg-icons/index.es.js
var free_solid_svg_icons_index_es = __webpack_require__(6);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/classnames/index.js
var classnames = __webpack_require__(2);
var classnames_default = /*#__PURE__*/__webpack_require__.n(classnames);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Link.js
var Link = __webpack_require__(50);

// EXTERNAL MODULE: ./src/routes/pages/sidebar/App.css
var App = __webpack_require__(333);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// CONCATENATED MODULE: ./src/routes/pages/sidebar/SubMenu.js








var SubMenu_SubMenu = function SubMenu(props) {
  var _useState = Object(react["useState"])(true),
      _useState2 = slicedToArray_default()(_useState, 2),
      collapsed = _useState2[0],
      setCollapsed = _useState2[1];

  var toggle = function toggle() {
    return setCollapsed(!collapsed);
  };

  var icon = props.icon,
      title = props.title,
      items = props.items;
  return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], {
    onClick: toggle,
    className: classnames_default()({
      "menu-open": !collapsed
    })
  }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    className: "dropdown-toggle"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: icon,
    className: "mr-2"
  }), title)), /*#__PURE__*/react_default.a.createElement(reactstrap_es["m" /* Collapse */], {
    isOpen: !collapsed,
    navbar: true,
    className: classnames_default()("items-menu", {
      "mb-1": !collapsed
    })
  }, items.map(function (item, index) {
    return /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], {
      key: index,
      className: "pl-4"
    }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
      tag: Link["a" /* default */],
      to: item.target
    }, item.title));
  })));
};

/* harmony default export */ var sidebar_SubMenu = (SubMenu_SubMenu);
// CONCATENATED MODULE: ./src/routes/pages/sidebar/SideBar.js









var SideBar_SideBar = function SideBar(_ref) {
  var isOpen = _ref.isOpen,
      toggle = _ref.toggle;
  return /*#__PURE__*/react_default.a.createElement("div", {
    className: classnames_default()("sidebar", {
      "is-open": isOpen
    })
  }, /*#__PURE__*/react_default.a.createElement("div", {
    className: "sidebar-header"
  }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["K" /* NavbarToggler */], {
    className: "float-right display-lg",
    style: {
      backgroundColor: "#1370b9",
      border: "solid white 1px"
    },
    onClick: toggle
  }), /*#__PURE__*/react_default.a.createElement("span", {
    color: "info",
    onClick: toggle,
    style: {
      color: "#fff"
    }
  }, "\xD7"), /*#__PURE__*/react_default.a.createElement("h3", null, "IMPORTADORES GRUPALES")), /*#__PURE__*/react_default.a.createElement("div", {
    className: "side-menu"
  }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["H" /* Nav */], {
    vertical: true,
    className: "list-unstyled pb-3"
  }, /*#__PURE__*/react_default.a.createElement("p", null, "Panel de control"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    tag: Link["a" /* default */],
    to: "/ImpoAdmin"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: free_solid_svg_icons_index_es["f" /* faImage */],
    className: "mr-2"
  }), "Agregar Productos")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    tag: Link["a" /* default */],
    to: "/Clientes"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: free_solid_svg_icons_index_es["a" /* faBriefcase */],
    className: "mr-2"
  }), "Agregar Cliente")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    tag: Link["a" /* default */],
    to: "/Productos"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: free_solid_svg_icons_index_es["a" /* faBriefcase */],
    className: "mr-2"
  }), "Gestionar stock")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    tag: Link["a" /* default */],
    to: "/Importadores"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: free_solid_svg_icons_index_es["k" /* faQuestion */],
    className: "mr-2"
  }), "Listado de importadores")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
    tag: Link["a" /* default */],
    to: "/"
  }, /*#__PURE__*/react_default.a.createElement(index_es["a" /* FontAwesomeIcon */], {
    icon: free_solid_svg_icons_index_es["i" /* faPaperPlane */],
    className: "mr-2"
  }), "Volver a calculadora")))));
};

/* harmony default export */ var sidebar_SideBar = __webpack_exports__["a"] = (SideBar_SideBar);

/***/ })

}]);
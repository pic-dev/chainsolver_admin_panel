(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[71],{

/***/ 1126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ProductosDetallesFirebase; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
/* harmony import */ var Util_Utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(16);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(50);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(497);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var Util_RouteBack__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(226);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(594);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_9__);











var Likes = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.lazy(function () {
  return __webpack_require__.e(/* import() */ 87).then(__webpack_require__.bind(null, 645));
});
function ProductosDetallesFirebase(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])("2"),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1___default()(_useState, 2),
      activeTab = _useState2[0],
      setActiveTab = _useState2[1];

  var tab = function tab(_tab) {
    if (activeTab !== _tab) {
      setActiveTab(_tab);
    }
  };

  console.log(props);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Container */ "n"], {
    className: "mt-5 pt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: "mx-auto pt-2 pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "font text-center pb-2"
  }, "Importaciones Grupales"))), props.toggleProducto ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "10",
    sm: "12",
    style: {
      display: "flex",
      justifyContent: "space-between"
    },
    className: "mx-auto pt-2 pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Util_RouteBack__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "font2 color-blue-2 text-center pb-2"
  }, props.producto.NombreProducto.toUpperCase()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("img", {
    key: "img",
    src: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_4__[/* imgProducto */ "l"])(props.producto.PaisDesti),
    style: {
      height: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_4__[/* isMovil */ "m"])() ? "3rem" : "4rem"
    },
    alt: "logo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "10",
    sm: "12",
    className: "ModalContainer mx-auto  pt-3 p-1 text-justify  "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], {
    className: "mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Card */ "d"], {
    className: "p-1",
    style: {
      alignItems: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_6__["Carousel"], {
    infiniteLoop: true,
    autoPlay: false,
    showThumbs: false,
    showIndicators: false,
    stopOnHover: true,
    showStatus: false,
    className: "carrouselWidth"
  }, props.producto.Multimedia[1] ? props.producto.Multimedia[1].video.map(function (picture, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      key: index + "videoDiv"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("video", {
      className: "react-player imgDetails",
      key: index + "video",
      id: "my-video" + index,
      controls: true,
      width: "100%",
      height: "100%",
      "data-setup": "{}",
      autoPlay: true,
      muted: true
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("source", {
      src: picture.img,
      type: picture.fileType
    })));
  }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("img", {
    src: props.producto.Multimedia[0].imagen[0].img,
    className: "imgDetails p-2",
    alt: "logo",
    key: "img2"
  }), props.producto.Multimedia[0].imagen.map(function (picture, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      key: index + "imagenPortadaDiv"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("img", {
      key: index + "imagenPortada",
      src: picture.img,
      className: "imgDetails p-2",
      alt: "logo"
    }));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Likes, {
    item: props.keyLike
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* CardBody */ "e"], {
    style: {
      padding: "1rem 0px 0px",
      width: "100%",
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* CardTitle */ "k"], {
    className: "m-0 p-0",
    style: {
      fontWeight: "bold"
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("a", {
    style: {
      padding: "0px"
    },
    href: "https://wa.link/xbr57j",
    rel: "noopener noreferrer",
    target: "_blank"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("i", {
    className: "whatsapp-contacto"
  }), " "))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "6",
    md: "6",
    className: "text-center arial tab1 ".concat(classnames__WEBPACK_IMPORTED_MODULE_7___default()({
      active2: activeTab === "2"
    })),
    onClick: function onClick() {
      tab("2");
    }
  }, "Importadores asociados"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "6",
    md: "6",
    className: "text-center arial tab1 ".concat(classnames__WEBPACK_IMPORTED_MODULE_7___default()({
      active2: activeTab === "1"
    })),
    onClick: function onClick() {
      tab("1");
    }
  }, "Especificaciones")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* TabContent */ "M"], {
    className: "mx-auto ",
    activeTab: activeTab
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* TabPane */ "N"], {
    tabId: "1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], {
    className: "mx-auto pt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    className: "p-0",
    xs: "12",
    md: "12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "p-1",
    style: {
      alignItems: "center"
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    style: {
      padding: "1rem 0 1rem 0",
      width: "100%",
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Table */ "O"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tbody", null, props.producto.Caracteristicas.map(function (item) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", {
      className: "p-0 text-center",
      key: item.key + "Especificaciones1"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
      scope: "row",
      style: {
        fontWeight: "bold",
        backgroundColor: "#80808038"
      }
    }, item.nombre), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", null, item.descripción));
  })))))))), " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* TabPane */ "N"], {
    tabId: "2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], {
    className: "mx-auto pt-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({
    className: "p-0",
    xs: "12",
    md: "12"
  }, "className", "NoPadding text-center"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Table */ "O"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", null, "Contacto"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", null, "Pa\xEDs"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", null, "Provincia"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", null, "Telefono"))), props.ClienteStock !== undefined ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tbody", null, props.ClienteStock.map(function (item) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", {
      className: "p-0 text-center",
      key: item.key + "clientes"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
      scope: "row",
      style: {
        fontWeight: "bold",
        backgroundColor: "#80808038"
      }
    }, item.contacto), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", null, item.Pais), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", null, item.Provincia), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", null, item.telefono));
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", {
    className: "p-0 text-center",
    key: "clientes"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "row",
    style: {
      fontWeight: "bold",
      backgroundColor: "#80808038"
    },
    colSpan: "4"
  }, "Ho hay clientes asignados"))))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: "p-3",
    style: {
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], {
    to: "/Importadores"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Button */ "c"], {
    className: "btn-style-backConcentradores",
    size: "sm"
  }, "Volver a Secci\xF3n de Importadores"))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: " mx-auto text-center m-3"
  }, "Cargando... ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("span", {
    className: "loadingBlock"
  }), " ")), " ");
}

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(249);



var GoBack = function GoBack(_ref) {
  var history = _ref.history;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "BackIcon simple-icon-arrow-left-circle float-left",
    onClick: function onClick() {
      return history.goBack();
    }
  });
};

/* harmony default export */ __webpack_exports__["a"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(GoBack));

/***/ })

}]);
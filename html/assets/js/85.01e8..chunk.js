(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[85],{

/***/ 1505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(44);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(53);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(52);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(80);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _Firebase__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(11);
/* harmony import */ var _Utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(16);
/* harmony import */ var _Errores__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(615);
/* harmony import */ var _tracking__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(20);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }









var ref = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_9__["createRef"])();

var signIn = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(signIn, _Component);

  var _super = _createSuper(signIn);

  function signIn(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, signIn);

    _this = _super.call(this, props);

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "inicio", function () {
      var empty = [];
      var vacio = empty.map(function (item) {
        return item;
      });
      localStorage.distrito = vacio;
      localStorage.regulador = vacio;
      localStorage.tipoMercancia = vacio;
      localStorage.distritoN = vacio;
      localStorage.ProvinciaN = vacio;
      localStorage.monto = vacio;
      localStorage.value4 = false;
      localStorage.value5 = false;
      localStorage.value6 = false;
      localStorage.value7 = false;
      localStorage.value8 = false;
      localStorage.seguro = true;
      localStorage.check3 = false;
      localStorage.check = false;
      localStorage.check2 = false;
      localStorage.check1 = false;
      localStorage.check4 = true;
      /*switch (localStorage.tipocon) {
        case "Contenedor completo":
          Event(
            "2.7.1 Calcular Cotizacion FCL Directo",
            "2.7.1 Calcular Cotizacion FCL Directo",
            "SERVICIOS"
          );
          break;
        case "Contenedor compartido":
          Event(
            "1.8.1 Calcular Cotizacion LCL Directo",
            "1.8.1 Calcular Cotizacion LCL Directo",
            "SERVICIOS"
          );
          break;
        case "Aereo":
          Event(
            "3.8.1 Calcular Cotizacion Aereo Directo",
            "3.8.1 Calcular Cotizacion Aereo Directo",
            "SERVICIOS"
          );
          break;
         default:
      }*/
      //orifginal

      var data = JSON.parse(sessionStorage.getItem("user"));

      if (sessionStorage.usuario == "1" || data[1] == "Invitado") {
        if (sessionStorage.agotado == undefined) {
          sessionStorage.agotado = "1";
        } else if (sessionStorage.agotado == "1") {
          sessionStorage.agotado = "2";
        } else if (sessionStorage.agotado == "2") {
          sessionStorage.agotado = "3";
        } else {
          sessionStorage.agotado = "4";
        }

        localStorage.validLogin = "1";

        if (sessionStorage.usuario == "2" && sessionStorage.agotado !== "4") {
          _this.redirect();
        }
      } else {
        _this.redirect();
      }
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "invitado", function () {
      //Event("Ingresar como invitado", "Ingresar como invitado", "SERVICIOS");
      if (sessionStorage.usuario == undefined || sessionStorage.usuario !== "2") {
        var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
        sessionStorage.setItem("user", JSON.stringify(user));
      }

      sessionStorage.usuario = "2";
      _this.props.toggle;

      _this.redirect();

      window.location.reload();
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "correoClave", /*#__PURE__*/function () {
      var _ref = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
        var _e$target$elements, usuario, clave;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements = e.target.elements, usuario = _e$target$elements.usuario, clave = _e$target$elements.clave;

                if (!(usuario.value.length > 0 && clave.value.length > 0)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 6;
                return _Firebase__WEBPACK_IMPORTED_MODULE_13__[/* firebaseConf */ "a"].auth().signInWithEmailAndPassword(usuario.value, clave.value).then(function (result) {
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));
                  _this.props.toggle;
                  timer = setTimeout(function () {
                    window.location.reload();

                    _this.redirect();
                  }, 1000);
                }).catch(function (error) {
                  sessionStorage.usuario = "2";
                  var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                  sessionStorage.setItem("user", JSON.stringify(user));
                  _this.props.toggle;
                  timer = setTimeout(function () {
                    window.location.reload();

                    _this.redirect();
                  }, 1000);
                  Object(_Utils__WEBPACK_IMPORTED_MODULE_14__[/* errores */ "d"])("Logincorreo", error);
                });

              case 6:
                _context.next = 10;
                break;

              case 8:
                _this.seterror("Debe llenar todos los campos requeridos");

                timer = setTimeout(function () {
                  _this.setState({
                    error: ""
                  });
                }, 4000);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "handleSignUp", /*#__PURE__*/function () {
      var _ref2 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(e) {
        var _e$target$elements2, telefono, usuario, clave, Confirmacion;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                e.preventDefault();

                _this.setState({
                  error: ""
                });

                _e$target$elements2 = e.target.elements, telefono = _e$target$elements2.telefono, usuario = _e$target$elements2.usuario, clave = _e$target$elements2.clave, Confirmacion = _e$target$elements2.Confirmacion;

                if (!(usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value === Confirmacion.value)) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 6;
                return _Firebase__WEBPACK_IMPORTED_MODULE_13__[/* firebaseConf */ "a"].auth().createUserWithEmailAndPassword(usuario.value, clave.value).then(function (result) {
                  if (telefono.value.length > 0) {
                    if (Object(_Utils__WEBPACK_IMPORTED_MODULE_14__[/* telefonoValid */ "r"])(telefono.value)) {
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));
                      _this.props.toggle;
                      localStorage.telefono = telefono.value;
                      timer = setTimeout(function () {
                        window.location.reload();

                        _this.redirect();
                      }, 1000);
                    } else {
                      var alerta = "Verifique numero de telefono";
                      alert(alerta);
                    }
                  } else {
                    sessionStorage.usuario = "2";
                    var _user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(_user));
                    _this.props.toggle;
                    timer = setTimeout(function () {
                      window.location.reload();

                      _this.redirect();
                    }, 1000);
                  }
                }).catch(function (error) {
                  if (telefono.value.length > 0) {
                    if (telefono.value.length > 0 && Object(_Utils__WEBPACK_IMPORTED_MODULE_14__[/* telefonoValid */ "r"])(telefono.value)) {
                      sessionStorage.usuario = "2";
                      var user = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                      sessionStorage.setItem("user", JSON.stringify(user));
                      _this.props.toggle;
                      localStorage.telefono = telefono.value;
                      timer = setTimeout(function () {
                        window.location.reload();

                        _this.redirect();
                      }, 1000);
                    } else {
                      alerta = "Verifique numero de telefono";
                      alert(alerta);
                    }
                  } else {
                    sessionStorage.usuario = "2";
                    var _user2 = [["PIC"], [usuario.value], [usuario.value], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(_user2));
                    _this.props.toggle;
                    timer = setTimeout(function () {
                      window.location.reload();

                      _this.redirect();
                    }, 1000);
                  }

                  Object(_Utils__WEBPACK_IMPORTED_MODULE_14__[/* errores */ "d"])("RegistroCorreo", error);

                  _this.seterror(Object(_Utils__WEBPACK_IMPORTED_MODULE_14__[/* ErrorEspa */ "a"])(error));
                });

              case 6:
                _context2.next = 9;
                break;

              case 8:
                if (usuario.value.length > 0 && clave.value.length > 0 && Confirmacion.value.length > 0 && clave.value !== Confirmacion.value) {
                  _this.seterror("La contraseña ingresada no coincide con la confimación");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                } else {
                  _this.seterror("Debe llenar todos los campos requeridos");

                  timer = setTimeout(function () {
                    _this.setState({
                      error: ""
                    });
                  }, 4000);
                }

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_8___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), "socialLogin", function (provider, e) {
      e.preventDefault();

      _this.setState({
        error: ""
      });

      _Firebase__WEBPACK_IMPORTED_MODULE_13__[/* firebaseConf */ "a"].auth().setPersistence(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* persistencia */ "c"]).then(function () {
        _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return _Firebase__WEBPACK_IMPORTED_MODULE_13__[/* firebaseConf */ "a"].auth().signInWithPopup(provider).then(function (result) {
                    /*Event(
                      "GoogleLoginHeader",
                      "Usuario ingresó con botton de google",
                      "SERVICIOS"
                    );*/
                    var user = [[result.additionalUserInfo.profile.id], [result.additionalUserInfo.profile.name], [result.additionalUserInfo.profile.email], [result.additionalUserInfo.profile.picture]];
                    sessionStorage.setItem("user", JSON.stringify(user));
                    sessionStorage.usuario = "2";

                    _this.redirect();

                    window.location.reload();
                  }).catch(function (error) {
                    sessionStorage.usuario = "2";
                    var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
                    sessionStorage.setItem("user", JSON.stringify(user));

                    _this.redirect();

                    window.location.reload();
                  });

                case 2:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }))();
      }).catch(function (error) {});
    });

    _this.socialLogin = _this.socialLogin.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.correoClave = _this.correoClave.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.handleSignUp = _this.handleSignUp.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.setsignup = _this.setsignup.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.seterror = _this.seterror.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.invitado = _this.invitado.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.redirect = _this.redirect.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.inicio = _this.inicio.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this));
    _this.state = {
      Signup: false,
      signInChoose: false,
      Choose: true,
      agotadoChoose: false,
      error: ""
    };
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(signIn, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.inicio();
    }
  }, {
    key: "redirect",
    value: function redirect() {
      if (this.props.bandera) {
        this.props.redirect();
      }
    }
  }, {
    key: "setsignup",
    value: function setsignup(e) {
      this.setState(function () {
        return {
          signup: e
        };
      });
    }
  }, {
    key: "setsignIn",
    value: function setsignIn(e) {
      this.setState(function () {
        return {
          signIn: e
        };
      });
    }
  }, {
    key: "seterror",
    value: function seterror(e) {
      this.setState(function () {
        return {
          error: e
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Modal */ "D"], {
        isOpen: this.props.active,
        toggle: this.props.toggle,
        backdrop: false,
        size: "md",
        className: "modalConfirmatinoWidth",
        style: {
          borderRadius: "1.1rem",
          marginTop: "65px !important"
        }
      }, sessionStorage.agotado !== "4" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalBody */ "E"], {
        style: {
          padding: "0rem"
        }
      }, !this.state.signInChoose ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "\xA1IMPORTANTE!"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "p-2 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xs: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: " login-form"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mt-2 mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-list"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue"
      }, "Para\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-green"
      }, "formalizar\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue"
      }, "tu oferta\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange"
      }, "REG\xCDSTRATE\xA0")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mt-1 mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Sin Formularios"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: " mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Sin TDC ni pagos"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: " mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "simple-icon-check color-green"
      }), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Solo con tu email"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-2",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        color: "success",
        onClick: this.invitado,
        className: "btn-style-grey mr-1",
        size: "sm"
      }, "INGRESAR COMO INVITADO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        color: "success",
        onClick: function onClick() {
          return _this2.setState(function () {
            return {
              signInChoose: true
            };
          });
        },
        className: "btn-style",
        size: "sm"
      }, "REGISTRARME")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontSize: "0.9rem"
        },
        className: "text-center mb-1"
      }, "Ya tengo una cuenta -\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this2.setState(function () {
            return {
              signInChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "\xA1INGRESA\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "bggradien-orange color-white",
        style: {
          fontWeight: "bold",
          borderRadius: "5px"
        }
      }, "\xA0GRATIS!\xA0"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "mx-auto p-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xs: "12"
      }, this.state.signup ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: "login-form",
        onSubmit: this.correoClave
      }, this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "paddingLogin text-center",
        style: {
          textAling: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 font2 color-blue"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Iniciar sesi\xF3n")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        className: "GLogin ",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-google"
      }), "Ingresa con Google")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "-o-")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "login-form-button"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Iniciar sesi\xF3n"), "\xA0 O \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          _this2.setsignup(false);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "Registrate")))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: "paddingLogin text-center",
        onSubmit: this.handleSignUp
      }, this.state.Choose ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2  "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue"
      }, "INGRESA "), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-black"
      }, "TUS DATOS")), this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial color-orange "
      }, "PASO 1 -", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "N\xFAmero de tel\xE9fono -"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-grey"
      }, "(Opcional)")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "text",
        name: "telefono",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial color-orange"
      }, "PASO 2 -", " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Correo electr\xF3nico")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "o con otro "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        onClick: function onClick() {
          return _this2.setState(function () {
            return {
              Choose: false
            };
          });
        },
        className: "color-blue",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "correo electr\xF3nico")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mt-2 mb-2"
      }, "\xA0 Ya tengo una cuenta - \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        onClick: function onClick() {
          _this2.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2 font2 "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue"
      }, "INGRESA "), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-black"
      }, " ", "TUS DATOS")), this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange "
      }, "PASO 1 - "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "N\xFAmero de tel\xE9fono -"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-grey"
      }, "(Opcional)")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "text",
        name: "telefono",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontWeight: "bold"
        },
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange"
      }, "PASO 2 - "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Correo electr\xF3nico")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "Confirmacion",
        type: "password",
        placeholder: "Confirma tu Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Registrate")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mt-2 mb-2"
      }, "\xA0 Ya tengo una cuenta - \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        onClick: function onClick() {
          _this2.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N")))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* ModalBody */ "E"], {
        style: {
          padding: "0rem"
        }
      }, !this.state.agotadoChoose ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, "Has agotado el m\xE1ximo de consultas como invitado"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "p-2 text-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xs: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: " login-form"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        style: {
          fontSize: "0.9rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mt-1 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Opci\xF3n 1 -"), " ", "Consultas ilimitadas Registrate", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold"
        }
      }, " ", "GRATIS")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nombre de empresa"), " ", "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "text",
        name: "Empresa ",
        placeholder: "Ingrese aqui el nommbre de su empresa"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "N\xFAmero de tel\xE9fono"), " ", "- (Opcional)"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "text",
        name: "telefono ",
        placeholder: "Ingrese aqui su n\xFAmero de telefono"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "text-center mt-2 mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        style: {
          fontSize: "0.9rem"
        }
      }, " ", "o con otro\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this2.setState(function () {
            return {
              agotadoChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "correo electr\xF3nico")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "mb-3"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-blue",
        style: {
          fontWeight: "bold"
        }
      }, "\xA0 Opci\xF3n 2 -\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Regresa en 15 d\xEDas y goza de 3 nuevas consultas sin registro."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        style: {
          fontSize: "0.9rem"
        },
        className: "text-center m-3"
      }, "Ya tengo una cuenta -\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          return _this2.setState(function () {
            return {
              agotadoChoose: true
            };
          });
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N"))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " font NoPadding text-center pt-1 pb-1 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "1.35rem",
          borderRadius: "1.1rem 1.1rem 0rem 0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "\xA1INGRESA\xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "bggradien-orange color-white",
        style: {
          fontWeight: "bold",
          borderRadius: "5px"
        }
      }, "\xA0GRATIS!\xA0"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Row */ "L"], {
        className: "p-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xs: "12"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), !this.state.signup ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: " login-form",
        onSubmit: this.correoClave
      }, this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("div", {
        className: "paddingLogin text-center",
        style: {
          textAling: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Iniciar sesi\xF3n")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "-o-")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "login-form-button"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Iniciar sesi\xF3n"), "\xA0 O \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "arial",
        onClick: function onClick() {
          _this2.setsignup(true);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "Registrate")))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Form */ "t"], {
        className: "paddingLogin text-center",
        onSubmit: this.handleSignUp
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Registro")), this.state.error ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(_Errores__WEBPACK_IMPORTED_MODULE_15__[/* default */ "a"], {
        mensaje: this.state.error
      })) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        style: {
          borderRadius: "13px",
          fontSize: "14px"
        },
        className: "GLogin",
        onClick: function onClick(e) {
          return _this2.socialLogin(_Firebase__WEBPACK_IMPORTED_MODULE_13__[/* googleAuthProvider */ "b"], e);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("i", {
        className: "icon-gmail"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "Gmail,\xA0"), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          textDecoration: "underline"
        }
      }, "solo con un click"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], {
        className: "mb-2 arial"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", null, "-o-")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        type: "email",
        name: "usuario",
        placeholder: "Correo electr\xF3nico"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "clave",
        type: "password",
        placeholder: "Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Input */ "w"], {
        name: "Confirmacion",
        type: "password",
        placeholder: "Confirma tu Clave"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* FormGroup */ "u"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        type: "submit",
        className: "btn-style-blue btn btn-success btn-sm"
      }, "Registrate"), "\xA0 O \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("span", {
        onClick: function onClick() {
          _this2.setsignup(false);
        },
        style: {
          fontWeight: "bold",
          cursor: "pointer",
          color: "rgb(54, 94, 153)",
          textDecoration: "underline"
        }
      }, "INICIAR SESI\xD3N")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_11__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-1",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement("hr", {
        className: "ten"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__[/* Button */ "c"], {
        color: "success",
        onClick: this.props.toggle,
        className: "btn-style",
        size: "sm"
      }, " ", "VOLVER")))))));
    }
  }]);

  return signIn;
}(react__WEBPACK_IMPORTED_MODULE_9__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (signIn);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[62],{

/***/ 1127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ProductosDetallesCampaña; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(44);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(53);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3);
/* harmony import */ var Util_Utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(16);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(50);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(497);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var Util_RouteBack__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(226);
/* harmony import */ var Components_spinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(58);
/* harmony import */ var _sidebarJunior_App_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1128);
/* harmony import */ var _sidebarJunior_App_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_sidebarJunior_App_css__WEBPACK_IMPORTED_MODULE_11__);













var ArticulosList = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(2), __webpack_require__.e(49)]).then(__webpack_require__.bind(null, 1392));
});
var Pedido = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(2), __webpack_require__.e(56)]).then(__webpack_require__.bind(null, 1394));
});
var PdfMovil = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(2), __webpack_require__.e(44)]).then(__webpack_require__.bind(null, 913));
});
var ModaPedidoMovil = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(2), __webpack_require__.e(57)]).then(__webpack_require__.bind(null, 1393));
});
function ProductosDetallesCampaña(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(true),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState, 2),
      togglePDF = _useState2[0],
      setTogglePDF = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(""),
      _useState4 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState3, 2),
      catalogoSelected = _useState4[0],
      setCatalogoSelected = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])([]),
      _useState6 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState5, 2),
      articuloSelected = _useState6[0],
      setArticuloSelected = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      _useState8 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState7, 2),
      cargandoCatalogos = _useState8[0],
      setCargandoCatalogos = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])([]),
      _useState10 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState9, 2),
      catalogosDisponibles = _useState10[0],
      setCatalogosDisponibles = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(true),
      _useState12 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState11, 2),
      rubrosActivo = _useState12[0],
      setRubrosActivo = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      _useState14 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState13, 2),
      isOpen = _useState14[0],
      setIsOpen = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      _useState16 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState15, 2),
      openPedidoMovil = _useState16[0],
      setOpenPedidoMovil = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      _useState18 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState17, 2),
      isShowCatalogos = _useState18[0],
      setIsShowCatalogos = _useState18[1];

  var _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      _useState20 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState19, 2),
      fullscreen = _useState20[0],
      setIsfullscreen = _useState20[1];

  var _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(""),
      _useState22 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState21, 2),
      tipo = _useState22[0],
      setTipo = _useState22[1];

  var toggleModalPedidoMovil = function toggleModalPedidoMovil() {
    setOpenPedidoMovil(!openPedidoMovil);
  };

  var toggle = function toggle(e) {
    setCatalogoSelected(e);
    setTogglePDF(!togglePDF);
  };

  var getCatalogos = /*#__PURE__*/function () {
    var _ref = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(e) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setCargandoCatalogos(true);
              _context.next = 3;
              return fetch("https://point.qreport.site/catalogos/r/c/".concat(e, "/").concat(props.id)).then(function (res) {
                return res.json();
              }).then(function (response) {
                if (response[0].statusCatalogo == "1") {
                  setTipo("catalogos");
                  setCatalogosDisponibles(response);
                  setRubrosActivo(false);
                  setCargandoCatalogos(false);
                } else {
                  setTipo("articulos");
                  setTogglePDF(false);
                  setCatalogoSelected(response[0]);
                  setTogglePDF(false);
                  setCatalogosDisponibles(response);
                  setRubrosActivo(false);
                  setCargandoCatalogos(false);
                  setTipo("articulos");
                }
              }).catch(function (error) {
                console.log(error);
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getCatalogos(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var ArticuloSelect = function ArticuloSelect(e) {
    setArticuloSelected(e);
    toggleModalPedidoMovil();

    if (fullscreen) {
      setIsfullscreen(false);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Container */ "n"], {
    className: "mt-5 pt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: "mx-auto pt-2 pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "font text-center pb-2"
  }, "Importaciones Grupales"))), props.toggleProducto ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    lg: "10",
    sm: "12",
    style: {
      display: "flex",
      justifyContent: "space-between"
    },
    className: "NoPadding mx-auto pt-2 pb-2"
  }, togglePDF == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(Util_RouteBack__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"], null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: "BackIcon simple-icon-arrow-left-circle float-left",
    onClick: function onClick() {
      toggle("");
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "font2 color-blue-2 p-1 text-center pb-2"
  }, props.campañas.description.toUpperCase()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("img", {
    key: "img",
    src: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* imgProducto */ "l"])(props.campañas.destino),
    style: {
      height: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() ? "3rem" : "4rem"
    },
    alt: "logo"
  })), togglePDF ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    lg: "10",
    sm: "12",
    className: "ModalContainer mx-auto  pt-3 p-1 text-justify  "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], {
    className: "mx-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Card */ "d"], {
    className: "p-1",
    style: {
      alignItems: "center"
    }
  }, "  ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      border: "5px solid #d2d2d2",
      backgroundColor: "white"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("img", {
    src: props.campañas.portada,
    className: "imgDetails3",
    alt: "logo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* CardBody */ "e"], {
    style: {
      padding: "1rem 0px 0px",
      width: "100%",
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* CardTitle */ "k"], {
    className: "m-0 p-0",
    style: {
      fontWeight: "bold"
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("a", {
    style: {
      padding: "0px"
    },
    href: "https://wa.link/xbr57j",
    rel: "noopener noreferrer",
    target: "_blank"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: "whatsapp-contacto"
  }), " "))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "6"
  }, props.campañas.market === 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "btn-style-ConcentadoresPdf text-center arial mb-2 pt-3 pb-3 pt-2 pb-2 alert alert-success"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, "MUY PRONTO ")), props.rubros.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      textAlign: "right"
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      borderRadius: "0.4rem"
    },
    className: "bg-blue-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "btn-style-ConcentadoresPdf text-white arial mb-2 pt-3 pb-3 pt-2 pb-2 bg-blue-2",
    onClick: function onClick() {
      return setIsOpen(!isOpen);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, "Ver Catalogos Disponibles       ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    style: {
      backgroundColor: "white",
      borderRadius: "50%",
      fontSize: "1rem",
      padding: ".2rem",
      fontWeight: "bold",
      cursor: "pointer"
    },
    className: "ml-2 float-right color-orange ".concat(isOpen ? "simple-icon-arrow-up" : "simple-icon-arrow-down")
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Collapse */ "m"], {
    className: "p-2",
    isOpen: isOpen
  }, rubrosActivo ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Alert */ "a"], {
    className: "arial",
    color: "info",
    isOpen: cargandoCatalogos,
    toggle: function toggle() {
      return setCargandoCatalogos(!cargandoCatalogos);
    }
  }, "Cargando Catalagos...", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", {
    className: "loadingBlock"
  }))), props.rubros.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], {
      onClick: function onClick() {
        getCatalogos(item.id);
      },
      style: {
        textTransform: "uppercase",
        borderBottom: "1px solid #8f8f8f",
        fontSize: "0.8rem",
        cursor: "pointer"
      },
      className: "arial",
      key: index + "ListGroupItem"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, item.name), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      style: {
        fontSize: "0,8rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#6c757d",
      color: "white",
      fontWeight: "bold",
      fontSize: "1rem",
      cursor: "pointer"
    },
    onClick: function onClick() {
      setRubrosActivo(!rubrosActivo);
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-arrow-left-circle  mr-2"
  }), "VOLVER A LOS RUBROS"))), catalogosDisponibles.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], {
      onClick: function onClick() {
        toggle(item);
      },
      style: {
        cursor: "pointer",
        textTransform: "uppercase",
        borderBottom: "1px solid #8f8f8f",
        fontSize: "0.8rem"
      },
      className: "arial",
      key: index + "ListGroupItem2"
    }, " ", item.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      style: {
        fontSize: "0.8rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  }))))), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      textAlign: "right"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    to: "/ImportadoresVip"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "btn-style-ConcentadoresPdf text-white arial mb-2 pt-3 pb-3 pt-2 pb-2 bg-blue-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, "Inversi\xF3n M\xEDnima"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    style: {
      backgroundColor: "white",
      borderRadius: "50%",
      fontSize: "1rem",
      padding: ".2rem",
      fontWeight: "bold"
    },
    className: "mr-2 ml-2 float-right color-orange simple-icon-info"
  })), " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    to: "/FAQ"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "btn-style-ConcentadoresPdf text-white arial pt-3 pb-3 pt-2 pb-2  bg-blue-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, "Preguntas Frecuentes"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    style: {
      backgroundColor: "white",
      borderRadius: "50%",
      fontSize: "1rem",
      padding: ".2rem",
      fontWeight: "bold"
    },
    className: "mr-2 ml-2 float-right color-orange simple-icon-question"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: "p-3",
    style: {
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    to: "/Importadores"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Button */ "c"], {
    className: "btn-style-backConcentradores",
    size: "sm"
  }, "Volver a Secci\xF3n de Importadores")))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], null, props.selectImportador ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: "ModalContainer mx-auto  pt-3 p-1 text-justify  "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], {
    className: "mx-auto"
  }, tipo === "catalogos" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: classnames__WEBPACK_IMPORTED_MODULE_8___default()("openEfecto sidebar", {
      "is-open": isShowCatalogos
    }),
    style: {
      alignItems: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "sidebar-header"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* NavbarToggler */ "K"], {
    className: "float-right display-lg",
    style: {
      backgroundColor: "#0d5084 !important",
      border: "solid white 1px"
    },
    onClick: function onClick() {
      return setIsShowCatalogos(!isShowCatalogos);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", {
    className: "display-sm",
    color: "info",
    onClick: function onClick() {
      return setIsShowCatalogos(!isShowCatalogos);
    },
    style: {
      color: "#fff"
    }
  }, "\xD7")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("section", {
    className: "side-menu",
    style: {
      padding: "1rem 0 1rem 0",
      width: "100%",
      textAlign: "right"
    }
  }, rubrosActivo ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Nav */ "H"], {
    vertical: true,
    className: "list-unstyled pb-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("p", {
    style: {
      fontSize: "1rem"
    },
    className: "arial mr-5"
  }, "Catalogos Disponibles"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* NavItem */ "I"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Alert */ "a"], {
    className: "arial",
    color: "success",
    isOpen: cargandoCatalogos,
    toggle: function toggle() {
      return setCargandoCatalogos(!cargandoCatalogos);
    }
  }, "Cargando Catalagos...", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", {
    className: "loadingBlock"
  }))), props.rubros.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* NavItem */ "I"], {
      onClick: function onClick() {
        getCatalogos(item.id);
      },
      style: {
        borderBottom: "1px solid #8f8f8f",
        cursor: "pointer"
      },
      className: "arial",
      key: index + "ListGroupItemrubros"
    }, " ", item.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      onClick: function onClick() {
        getCatalogos(item.id);
      },
      style: {
        fontSize: "1rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Nav */ "H"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("p", {
    style: {
      fontSize: "1rem"
    },
    className: "arial mr-5"
  }, "Catalogos Disponibles"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* NavItem */ "I"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#6c757d",
      color: "white",
      fontWeight: "bold",
      fontSize: "1rem",
      cursor: "pointer"
    },
    onClick: function onClick() {
      setRubrosActivo(!rubrosActivo);
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-arrow-left-circle  mr-2"
  }), "VOLVER A LOS RUBROS"))), catalogosDisponibles.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* NavItem */ "I"], {
      onClick: function onClick() {
        setCatalogoSelected(item);
        setIsShowCatalogos(false);
      },
      style: {
        cursor: "pointer",
        textTransform: "uppercase",
        borderBottom: "1px solid #8f8f8f",
        fontSize: "0.8rem"
      },
      className: "arial",
      key: index + "catalogosDisponibles"
    }, " ", item.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      style: {
        fontSize: "0.8rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  })))), Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && props.selectImportador && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "stickyDiv arial text-center  mb-2 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#0d4674",
      color: "white",
      fontWeight: "bold"
    },
    onClick: function onClick() {
      toggleModalPedidoMovil();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("h2", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-list  mr-2"
  }), " REALIZAR PRE ORDEN EN LINEA"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    className: "sidePadding NoPadding",
    xs: "12",
    md: fullscreen ? "12" : "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "mb-3 pb-4"
  }, tipo === "catalogos" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Button */ "c"], {
    color: "success",
    className: "float-left btn-style-blue",
    size: "sm",
    onClick: function onClick() {
      return setIsShowCatalogos(!isShowCatalogos);
    }
  }, "VER MAS CATALOGOS", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: "simple-icon-list ml-2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Button */ "c"], {
    color: "success",
    className: "display-large float-right btn-style-bluelight",
    size: "sm",
    onClick: function onClick() {
      setIsfullscreen(!fullscreen);
    }
  }, fullscreen ? "VOLVER A PRE-ORDEN" : "AMPLIAR CATALOGO", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: "".concat(fullscreen ? "simple-icon-size-fullscreen" : "simple-icon-size-actual", " ml-2")
  }))), catalogoSelected.statusCatalogo == 1 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_3___default.a.Fragment, null, catalogoSelected && !Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("iframe", {
    style: {
      height: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() ? "30rem" : "50rem",
      width: "100%"
    },
    src: "https://point.qreport.site/files/".concat(props.JuniorData.length > 0 && catalogoSelected.statusCp == "Ok" ? catalogoSelected.ruta_cp : catalogoSelected.ruta_sp)
  }), catalogoSelected && Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(PdfMovil, {
    style: {
      height: "30rem",
      width: "100%"
    },
    url: "https://point.qreport.site/files/".concat(props.JuniorData.length > 0 && catalogoSelected.statusCp == "Ok" ? catalogoSelected.ruta_cp : catalogoSelected.ruta_sp)
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(ArticulosList, {
    ArticuloSelect: ArticuloSelect,
    fullscreen: fullscreen,
    status: props.selectImportador,
    catalago: catalogoSelected,
    JuniorData: props.JuniorData,
    toggleModal: props.toggleModal
  })), !Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    style: {
      display: fullscreen ? "none" : "unset"
    },
    className: "NoPadding",
    xs: "12",
    md: "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(Pedido, {
    JuniorData: props.JuniorData,
    catalogo: catalogoSelected,
    articulo: articuloSelected,
    setArticuloSelected: setArticuloSelected,
    tipo: tipo
  })))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "10",
    sm: "12",
    className: "ModalContainer mx-auto  pt-3 p-1 text-justify  "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], {
    className: "mx-auto"
  }, isShowCatalogos ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    className: "NoPadding",
    xs: "12",
    md: props.selectImportador ? "6" : "12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      height: "2.5rem",
      borderRadius: "0.4rem",
      textTransform: "uppercase",
      fontSize: "1rem"
    },
    className: "text-white text-center arial mb-2 p-2 bg-blue-2"
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, "Ver Catalogos Disponibles")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-1",
    style: {
      alignItems: "center"
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    style: {
      padding: "1rem 0 1rem 0",
      width: "100%",
      textAlign: "right"
    }
  }, rubrosActivo ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Alert */ "a"], {
    className: "arial",
    color: "success",
    isOpen: cargandoCatalogos,
    toggle: function toggle() {
      return setCargandoCatalogos(!cargandoCatalogos);
    }
  }, "Cargando Catalagos...", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", {
    className: "loadingBlock"
  }))), props.rubros.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], {
      onClick: function onClick() {
        getCatalogos(item.id);
      },
      style: {
        textTransform: "uppercase",
        borderBottom: "1px solid #8f8f8f",
        fontSize: "0.8rem",
        cursor: "pointer"
      },
      className: "arial",
      key: index + "ListGroupItemrubros2"
    }, " ", item.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      onClick: function onClick() {
        getCatalogos(item.id);
      },
      style: {
        fontSize: "0.8rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#6c757d",
      color: "white",
      fontWeight: "bold",
      fontSize: "1rem",
      cursor: "pointer"
    },
    onClick: function onClick() {
      setRubrosActivo(!rubrosActivo);
    }
  }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("span", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-arrow-left-circle  mr-2"
  }), "VOLVER A LOS RUBROS"))), catalogosDisponibles.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* ListGroupItem */ "C"], {
      onClick: function onClick() {
        setCatalogoSelected(item);
        setIsShowCatalogos(false);
      },
      style: {
        cursor: "pointer",
        textTransform: "uppercase",
        borderBottom: "1px solid #8f8f8f",
        fontSize: "0.8rem"
      },
      className: "arial",
      key: index + "catalogosDisponibles2"
    }, " ", item.name, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
      onClick: function onClick() {
        setCatalogoSelected(item);
        setIsShowCatalogos(false);
      },
      style: {
        fontSize: "0.8rem",
        fontWeight: "bold",
        backgroundColor: "black",
        color: "white",
        borderRadius: "50%",
        padding: ".1rem .4rem",
        cursor: "pointer"
      },
      className: "ml-2 mr-2 simple-icon-eye"
    }));
  })), " "))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    className: "NoPadding",
    xs: "12",
    md: props.selectImportador ? "6" : "12"
  }, !props.selectImportador && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "stickyDiv arial text-center  mb-2 "
  }, catalogoSelected.statusCatalogo == 1 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#FFCB06",
      color: "white",
      fontWeight: "bold"
    },
    onClick: function onClick() {
      return setIsShowCatalogos(!isShowCatalogos);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("h2", null, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-arrow-left-circle  mr-2"
  }), "VOLVER A LOS CATALOGOS")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("div", {
    className: "p-2",
    style: {
      width: "100%",
      backgroundColor: "#0d4674",
      color: "white",
      fontWeight: "bold"
    },
    onClick: function onClick() {
      props.toggleModal();
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("h2", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("i", {
    className: " simple-icon-list  mr-2"
  }), " ", "REALIZAR PRE ORDEN EN LINEA"))), catalogoSelected.statusCatalogo == 1 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_3___default.a.Fragment, null, catalogoSelected && !Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement("iframe", {
    style: {
      height: Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() ? "30rem" : "50rem",
      width: "100%"
    },
    src: "https://point.qreport.site/files/".concat(props.JuniorData.length > 0 && catalogoSelected.statusCp == "Ok" ? catalogoSelected.ruta_cp : catalogoSelected.ruta_sp)
  }), catalogoSelected && Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(PdfMovil, {
    style: {
      height: "30rem",
      width: "100%"
    },
    url: "https://point.qreport.site/files/".concat(props.JuniorData.length > 0 && catalogoSelected.statusCp == "Ok" ? catalogoSelected.ruta_cp : catalogoSelected.ruta_sp)
  })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(ArticulosList, {
    fullscreen: fullscreen,
    ArticuloSelect: ArticuloSelect,
    status: props.selectImportador,
    catalago: catalogoSelected,
    JuniorData: props.JuniorData,
    toggleModal: props.toggleModal
  })), props.selectImportador && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    className: "NoPadding",
    xs: "12",
    md: "6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(Pedido, {
    JuniorData: props.JuniorData,
    catalogo: catalogoSelected,
    articulo: articuloSelected,
    setArticuloSelected: setArticuloSelected,
    tipo: tipo
  })))), " ")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__[/* Col */ "l"], {
    xs: "12",
    md: "12",
    sm: "12",
    className: " mx-auto text-center m-3"
  }, "Cargando... ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(Components_spinner__WEBPACK_IMPORTED_MODULE_10__[/* default */ "a"], null), " ")), Object(Util_Utils__WEBPACK_IMPORTED_MODULE_5__[/* isMovil */ "m"])() && props.selectImportador && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(ModaPedidoMovil, {
    JuniorData: props.JuniorData,
    catalogo: catalogoSelected,
    articulo: articuloSelected,
    setArticuloSelected: setArticuloSelected,
    tipo: tipo,
    toggle: toggleModalPedidoMovil,
    open: openPedidoMovil
  }));
}

/***/ }),

/***/ 1128:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(249);



var GoBack = function GoBack(_ref) {
  var history = _ref.history;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "BackIcon simple-icon-arrow-left-circle float-left",
    onClick: function onClick() {
      return history.goBack();
    }
  });
};

/* harmony default export */ __webpack_exports__["a"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(GoBack));

/***/ })

}]);
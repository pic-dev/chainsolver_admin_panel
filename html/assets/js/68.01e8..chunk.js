(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[68],{

/***/ 1124:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 1535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/toConsumableArray.js
var toConsumableArray = __webpack_require__(670);
var toConsumableArray_default = /*#__PURE__*/__webpack_require__.n(toConsumableArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./src/components/CustomBootstrap/index.js
var CustomBootstrap = __webpack_require__(52);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// CONCATENATED MODULE: ./src/util/components/Item.js






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var Item_Item = /*#__PURE__*/function (_Component) {
  inherits_default()(Item, _Component);

  var _super = _createSuper(Item);

  function Item() {
    classCallCheck_default()(this, Item);

    return _super.apply(this, arguments);
  }

  createClass_default()(Item, [{
    key: "_remove",
    value: function _remove() {
      if (this.props.onRemove) this.props.onRemove();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
        className: "mb-2",
        style: {
          color: "#000000",
          backgroundColor: "#e1e2e2",
          fontSize: "1.2em",
          padding: "0.2em"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "1",
        md: "1",
        className: "NoPadding mx-auto  my-auto "
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this._remove.bind(this),
        className: "simple-icon-minus color-white",
        style: {
          cursor: "pointer",
          fontWeight: "bold",
          borderRadius: "50px",
          fontSize: "1.2em"
        }
      })), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "2",
        md: "2",
        className: "mr-2 mx-auto text-center my-auto "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["b" /* Badge */], {
        color: "light"
      }, " ", "\xA0 ", this.props.tablePosition + 1, "\xA0")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mr-2 mx-auto text-center my-auto "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["b" /* Badge */], {
        color: "light"
      }, " ", this.props.data.Bultos)), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mx-auto text-center my-auto "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["b" /* Badge */], {
        color: "light"
      }, " ", this.props.data.peso, "\xA0Kg\xA0")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mx-auto text-center my-auto "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["b" /* Badge */], {
        color: "light"
      }, " ", this.props.data.volumen, "\xA0M\xB3\xA0"))));
    }
  }]);

  return Item;
}(react["Component"]);

/* harmony default export */ var components_Item = (Item_Item);
// EXTERNAL MODULE: ./node_modules/lodash/lodash.js
var lodash = __webpack_require__(593);
var lodash_default = /*#__PURE__*/__webpack_require__.n(lodash);

// EXTERNAL MODULE: ./src/util/App.css
var util_App = __webpack_require__(1124);

// EXTERNAL MODULE: ./node_modules/react-bootstrap-typeahead/css/Typeahead.css
var Typeahead = __webpack_require__(80);

// EXTERNAL MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js + 1 modules
var es = __webpack_require__(339);

// EXTERNAL MODULE: ./src/util/tracking.js + 12 modules
var tracking = __webpack_require__(20);

// CONCATENATED MODULE: ./src/util/calculadoras.js








function calculadoras_createSuper(Derived) { var hasNativeReflectConstruct = calculadoras_isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function calculadoras_isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }










var valid;
var selected1;
var selected2;
var selected3;
var selected4;
var selected5;
var selected6;
var selected7;
var timer;

var calculadoras_App = /*#__PURE__*/function (_Component) {
  inherits_default()(App, _Component);

  var _super = calculadoras_createSuper(App);

  function App(props) {
    var _this;

    classCallCheck_default()(this, App);

    _this = _super.call(this, props);
    _this.handleFormState = _this.handleFormState.bind(assertThisInitialized_default()(_this));
    _this.calcular = _this.calcular.bind(assertThisInitialized_default()(_this));
    _this.add = _this.add.bind(assertThisInitialized_default()(_this));
    _this.showMessage = _this.showMessage.bind(assertThisInitialized_default()(_this));
    _this.scrollToTop = _this.scrollToTop.bind(assertThisInitialized_default()(_this));
    _this.scrollToBottom = _this.scrollToBottom.bind(assertThisInitialized_default()(_this));
    _this.state = {
      showMessage: false,
      showMessage2: false,
      data: [],
      form: [{
        Label: "LARGO",
        value: ""
      }, {
        Label: "ANCHO",
        value: ""
      }, {
        Label: "ALTO",
        value: ""
      }, {
        Label: "PESO",
        value: ""
      }, {
        Label: "BULTOS",
        value: ""
      }, {
        Label: "UNIDADMEDIDA",
        value: ""
      }, {
        Label: "UNIDADPESO",
        value: ""
      }, {
        Label: "TOTAL",
        value: ""
      }, {
        Label: "pesoAmetros",
        value: ""
      }]
    };
    return _this;
  }

  createClass_default()(App, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (localStorage.data !== undefined) {
        if (localStorage.data.length > 0) {
          this.setState({
            data: JSON.parse(localStorage.getItem("data"))
          });
        }
      }
      /*if (localStorage.transporte == "MARITÍMO") {
        Event(
          "1.2 Ingreso Calculadora LCL",
          "1.2 Ingreso Calculadora LCL",
          "RUTA"
        );
      } else {
        Event(
          "3.2 Ingreso Calculadora Aereo",
          "3.2 Ingreso Calculadora Aereo",
          "RUTA"
        );
      }*/

    }
  }, {
    key: "scrollToTop",
    value: function scrollToTop() {
      var element = document.getElementById("up");
      Object(es["a" /* default */])(element, {
        behavior: "smooth",
        block: "center",
        inline: "center"
      });
    }
  }, {
    key: "scrollToBottom",
    value: function scrollToBottom() {
      var element2;
      element2 = document.getElementById("down");
      Object(es["a" /* default */])(element2, {
        behavior: "smooth",
        block: "center",
        inline: "center"
      });
    }
  }, {
    key: "showMessage",
    value: function showMessage(e) {
      var _this2 = this;

      if (e == 1) {
        this.setState(function (ps) {
          return {
            showMessage: true
          };
        });
        timer = setTimeout(function () {
          _this2.setState(function (ps) {
            return {
              showMessage: false
            };
          });
        }, 15000);
      } else {
        this.setState(function (ps) {
          return {
            showMessage2: true
          };
        });
        timer = setTimeout(function () {
          _this2.setState(function (ps) {
            return {
              showMessage2: false
            };
          });
        }, 15000);
      }
    }
  }, {
    key: "handleFormState",
    value: function handleFormState(input) {
      var newState = this.state.form;

      if (input.value < 0) {
        newState[input.id].value = "";
        this.setState({
          form: newState
        });
      } else {
        newState[input.id].value = input.value;
        this.setState({
          form: newState
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "_getPesoToKg",
    value: function _getPesoToKg() {
      /* form: [
          { Label: "LARGO", value: "" },
          { Label: "ANCHO", value: "" },
          { Label: "ALTO", value: "" },
          { Label: "PESO", value: "" },
          { Label: "BULTOS", value: "" },
          { Label: "UNIDADMEDIDA", value: "" },
          { Label: "UNIDADPESO", value: "" },
          { Label: "TOTAL", value: "" },
          { Label: "pesoAmetros", value: "" },
        ]*/
      var unidad; //discriminamos por unidad de peso

      switch (this.state.form[6].value) {
        case "kg":
          return unidad = this.state.form[3].value;
          break;

        case "lb":
          var cantidad = this.state.form[3].value;
          var dividir = 1 / 2.2;
          return unidad = parseFloat(cantidad * dividir).toFixed(2);
          break;

        case "Tn":
          return (unidad = this.state.form[3].value * 1000).toFixed(2);
          break;

        default: // code block

      }
    }
  }, {
    key: "calcular",
    value: function calcular() {
      /* form: [
          { Label: "LARGO", value: "" },
          { Label: "ANCHO", value: "" },
          { Label: "ALTO", value: "" },
          { Label: "PESO", value: "" },
          { Label: "BULTOS", value: "" },
          { Label: "UNIDADMEDIDA", value: "" },
          { Label: "UNIDADPESO", value: "" },
          { Label: "TOTAL", value: "" },
          { Label: "pesoAmetros", value: "" },
        ]*/
      var resultado = 0;
      var x = this.state.form[5].value.toString(); //se hace el calculo dependiento de la unidad de medida seleccionada, multiplicado LARGO, ANCHO, ALTO * BULTOS

      switch (x) {
        case "cm":
          resultado = this.state.form[0].value / 100 * (this.state.form[1].value / 100) * (this.state.form[2].value / 100) * this.state.form[4].value;
          break;

        case "in":
          resultado = this.state.form[0].value / 39.37 * (this.state.form[1].value / 39.37) * (this.state.form[2].value / 39.37) * this.state.form[4].value;
          break;

        case "ft":
          resultado = this.state.form[0].value / 0.3048 * (this.state.form[1].value / 0.3048) * (this.state.form[2].value / 0.3048) * this.state.form[4].value;
          break;

        case "mm":
          resultado = this.state.form[0].value / 1000 * (this.state.form[1].value / 1000) * (this.state.form[2].value / 1000) * this.state.form[4].value;
          break;

        case "m":
          resultado = this.state.form[0].value * this.state.form[1].value * this.state.form[2].value * this.state.form[4].value;
          break;

        default: // code block

      }

      var newState = this.state.form; //Se redondea total de m3

      newState[7].value = Math.round(resultado * 100) / 100; //Se calcula total de peso * bultos

      newState[8].value = this._getPesoToKg() * this.state.form[4].value * 1000 / 1000;
      this.setState({
        form: newState
      });
      this.add();
    }
  }, {
    key: "_remove",
    value: function _remove(position) {
      var data = this.state.data;
      var newData = [].concat(toConsumableArray_default()(data.slice(0, position)), toConsumableArray_default()(data.slice(position + 1)));
      this.setState({
        data: newData
      });
      localStorage.setItem("data", JSON.stringify(newData));
    }
  }, {
    key: "add",
    value: function add() {
      if (this.state.form[7].value > 0.000017 && this.state.form[3].value > 0 && this.state.form[5].value.length > 0 && this.state.form[6].value.length > 0) {
        this.scrollToBottom();
        valid = "3";
        var data = this.state.data;
        var newData = [].concat(toConsumableArray_default()(data), [{
          Bultos: parseInt(this.state.form[4].value, 10),
          UNIDADMEDIDA: this.state.form[5].value,
          UNIDADPESO: this.state.form[6].value,
          peso: this.state.form[8].value,
          volumen: this.state.form[7].value,
          pesoVolumen: this.state.form[7].value * 166.7
        }]);
        this.setState({
          data: newData
        });
        var eraseForm = [{
          Label: "LARGO",
          value: ""
        }, {
          Label: "ANCHO",
          value: ""
        }, {
          Label: "ALTO",
          value: ""
        }, {
          Label: "PESO",
          value: ""
        }, {
          Label: "BULTOS",
          value: ""
        }, {
          Label: "UNIDADMEDIDA",
          value: this.state.form[5].value
        }, {
          Label: "UNIDADPESO",
          value: this.state.form[6].value
        }, {
          Label: "TOTAL",
          value: ""
        }, {
          Label: "pesoAmetros",
          value: ""
        }];
        localStorage.setItem("data", JSON.stringify(newData));
        this.setState({
          form: eraseForm,
          showMessage: false,
          showMessage2: false
        });
      } else if (this.state.form[7].value == 0 && this.state.form[0].value.length > 0 && this.state.form[1].value.length > 0 && this.state.form[2].value.length > 0 && this.state.form[3].value.length > 0 && this.state.form[4].value.length > 0 && this.state.form[5].value.length > 0 && this.state.form[6].value.length > 0) {
        this.scrollToTop();
        valid = "2";
        this.showMessage(2);
      } else {
        this.scrollToTop();
        valid = "1";
        this.showMessage(1);
      }
    }
  }, {
    key: "_getBultosTotal",
    value: function _getBultosTotal() {
      if (this.state.data.length > 0) {
        return lodash_default.a.sumBy(this.state.data, function (o) {
          return o.Bultos;
        });
      } else if (this.state.form[7].value > 0 || this.state.data.length == 0) {
        return this.state.form[4].value;
      } else {}
    }
  }, {
    key: "_getPesoTotal",
    value: function _getPesoTotal() {
      if (this.state.data.length > 0) {
        return lodash_default.a.sumBy(this.state.data, function (o) {
          return o.peso;
        });
      } else if (this.state.form[7].value > 0) {
        return this._getPesoToKg() * this.state.form[4].value;
      } else if (this.state.data.length == 0) {
        return this.state.form[4].value;
      } else {}
    }
  }, {
    key: "_getVolumenTotal",
    value: function _getVolumenTotal() {
      if (this.state.data.length > 0) {
        return lodash_default.a.sumBy(this.state.data, function (o) {
          return Math.round(o.volumen * 100) / 100;
        });
      } else if (this.state.form[7].value > 0 || this.state.data.length == 0) {
        return this.state.form[7].value;
      } else {}
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      localStorage.setItem("bultosTotal", JSON.stringify(this._getBultosTotal()));
      localStorage.setItem("pesoTotal", JSON.stringify(this._getPesoTotal()));
      localStorage.setItem("volumenTotal", JSON.stringify(this._getVolumenTotal()));
      localStorage.setItem("PesovolumenTotal", JSON.stringify(this._getVolumenTotal()));

      if (valid == "2") {
        selected1 = true;
        selected2 = true;
        selected3 = true;
        timer = setTimeout(function () {
          selected1 = false;
          selected2 = false;
          selected3 = false;
          valid = "1";
        }, 3000);
      } else {}

      if (valid === "1") {
        if (this.state.form[0].value > 0) {
          selected1 = false;
        } else {
          selected1 = true;
        }
      } else {}

      if (valid === "1") {
        if (this.state.form[1].value > 0) {
          selected2 = false;
        } else {
          selected2 = true;
        }
      } else {}

      if (valid === "1") {
        if (this.state.form[2].value > 0) {
          selected3 = false;
        } else {
          selected3 = true;
        }
      } else {}

      if (valid === "1") {
        if (this.state.form[3].value > 0) {
          selected4 = false;
        } else {
          selected4 = true;
        }
      } else {
        selected4 = false;
      }

      if (valid === "1") {
        if (this.state.form[4].value > 0) {
          selected5 = false;
        } else {
          selected5 = true;
        }
      } else {
        selected5 = false;
      }

      if (valid === "1") {
        if (this.state.form[5].value.length > 0) {
          selected6 = false;
        } else {
          selected6 = true;
        }
      } else {
        selected6 = false;
      }

      if (valid === "1") {
        if (this.state.form[6].value.length > 0) {
          selected7 = false;
        } else {
          selected7 = true;
        }
      } else {
        selected7 = false;
      }

      return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "text-center mb-2 recomendationCalculatorTitle",
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement("div", {
        id: "up"
      }, "     ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleCalculator,
        className: "plusLump  simple-icon-question ",
        id: "recomendationTooltip",
        style: {
          position: "absolute",
          right: "0.5em",
          top: "0.3em",
          cursor: "pointer"
        }
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        className: "color-blue",
        placement: "right",
        target: "recomendationTooltip"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: " color-blue text-justify"
      }, "Agrupar por bultos que tengan la misma unidad de peso y volumen."), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue mt-2 text-justify"
      }, "Ejemplo: Bultos en cent\xEDmetros NO MEZCLAR con bultos que est\xE9n en metros."), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue mt-2 text-justify"
      }, "No mezclar bultos que tengan el peso en kilos con otros que esten en libras."), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue mt-2 text-justify"
      }, "Si tenemos alguno de los ejemplos antes mencionados agregar un bulto adicional."))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        className: "text-center",
        style: {
          padding: "0em 2em 0em 2em"
        }
      }, this.state.showMessage ? /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          position: "sticky !important"
        },
        className: "alert alert-danger text-center arial  ",
        role: "alert"
      }, "Debes completar los campos marcados en rojo.") : "", this.state.showMessage2 ? /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          position: "sticky !important"
        },
        className: "alert alert-danger text-center arial  mx-auto",
        role: "alert"
      }, "Los valores de longitud (largo, ancho y alto) introducidos son muy bajos p\xE0ra realizar el calculo.") : "", /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "NoPadding",
        xs: 6,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "UNIDAD"
      }, " UNIDAD DE LONGITUD"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        invalid: selected6,
        name: "select",
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 5,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        value: ""
      }, "Elige una opci\xF3n"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "0",
        value: "cm"
      }, "Cent\xEDmetros"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "in"
      }, "Pulgadas"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "ft"
      }, "Pies"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "3",
        value: "mm"
      }, "Mil\xEDmetro"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "4",
        value: "m"
      }, "Metros")), selected6 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Campo requerido") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 6,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "UNIDADPESO"
      }, "UNIDAD DE PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        invalid: selected7,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 6,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        value: ""
      }, "Elige una opci\xF3n"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "0",
        value: "kg"
      }, "Kilogramos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "lb"
      }, "Libras"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "Tn"
      }, "Toneladas")), selected7 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Campo requerido") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "display-sm pb-1 pt-1 mb-1 arial color-blue",
        style: {
          backgroundColor: "#e1e2e2",
          fontWeight: "bold"
        },
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement("span", null, "NRO DE BULTOS")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "display-large",
        for: "BULTOS"
      }, "NRO DE BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        invalid: selected5,
        placeholder: "BULTOS",
        bsSize: "sm",
        value: this.state.form[4].value,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        }
      }), selected5 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Campo requerido") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "pt-1 pb-1 mb-1 arial color-blue",
        style: {
          backgroundColor: "#e1e2e2",
          fontWeight: "bold"
        },
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement("span", null, "MEDIDAS POR CADA BULTO"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 6,
        md: 3
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "LARGO"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        invalid: selected1,
        type: "number",
        placeholder: "LARGO",
        bsSize: "sm",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 0,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, this.state.form[5].value))), selected1 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Ingrese un valor valido.") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 6,
        md: 3
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "ANCHO"
      }, "ANCHO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        invalid: selected2,
        placeholder: "ANCHO",
        bsSize: "sm",
        value: this.state.form[1].value,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 1,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, this.state.form[5].value))), selected2 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Ingrese un valor valido.") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 6,
        md: 3
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "ALTO"
      }, "ALTO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        invalid: selected3,
        placeholder: "ALTO",
        bsSize: "sm",
        value: this.state.form[2].value,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 2,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, this.state.form[5].value))), selected3 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Ingrese un valor valido.") : "")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 6,
        md: 3
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "PESO"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        invalid: selected4,
        placeholder: "PESO",
        bsSize: "sm",
        value: this.state.form[3].value,
        onChange: function onChange(e) {
          _this3.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, this.state.form[6].value))), selected4 ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text"
      }, "Campo requerido") : ""))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12,
        className: "pt-2 text-center"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        size: "sm",
        className: "mt-3  btn-style",
        color: "success",
        onClick: this.calcular
      }, "Calcular"))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "color-white",
        style: {
          padding: "1em 0em"
        },
        xxs: "12",
        md: "12",
        sm: "12"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["B" /* ListGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["C" /* ListGroupItem */], {
        className: "mb-2",
        style: {
          fontWeight: "bold",
          padding: "0px",
          color: "#000000",
          backgroundColor: "#e1e2e2",
          fontSize: "1.2em",
          display: this.state.data.length > 0 ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "1",
        md: "1",
        className: "NoPadding mx-auto my-auto "
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "2",
        md: "2",
        className: "text-center mx-auto my-auto "
      }, "Item"), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mx-auto  text-center my-auto "
      }, "Bultos"), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mx-auto  text-center my-auto "
      }, "Peso"), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "3",
        md: "3",
        className: "mx-auto text-center my-auto "
      }, "Volumen"))), this.state.data.map(function (item, index) {
        return /*#__PURE__*/react_default.a.createElement(components_Item, {
          data: item,
          tablePosition: index,
          key: index,
          onRemove: function onRemove() {
            return _this3._remove(index);
          }
        });
      })), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "mb-2 mt-2 text-right ",
        xxs: "12",
        md: "12",
        sm: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        onClick: this.scrollToTop,
        style: {
          cursor: "pointer",
          color: "rgb(44, 123, 157)",
          fontSize: "1.4em",
          display: this.state.data.length > 0 ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "plusLump mr-2 simple-icon-plus "
      }), " Agregar otro bulto", " ")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "fontNavbar color-orange mb-2 mt-2 text-right ",
        xxs: "12",
        md: "12",
        sm: "12"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1rem",
          textAlign: "justify",
          fontWeight: "bold"
        }
      }, "Nota: Los valores de Peso y Volumen ser\xE1n convertidos autom\xE1ticamente a Kilogramos y Metros respectivamente."))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        id: "down",
        style: {
          backgroundColor: "#0d5084",
          padding: "0.2em 1em",
          color: "white",
          fontWeight: "bold"
        },
        xxs: "12",
        md: "12",
        sm: "12"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], null, localStorage.transporte == "AÉREO" ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        style: {
          fontSize: "1.2em",
          textDecoration: "underline"
        },
        className: "text-center",
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement("div", null, "TOTAL")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Bultos"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Bultos",
        id: "Bultos",
        value: this._getBultosTotal(),
        placeholder: "Bultos",
        disabled: true
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Peso"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Peso",
        id: "Peso",
        value: this._getPesoTotal(),
        placeholder: "Peso",
        disabled: true
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, "kg"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Volumen"
      }, "VOLUMEN"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Volumen",
        id: "Volumen",
        value: this._getVolumenTotal(),
        placeholder: "Volumen",
        disabled: true
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, "M\xB3")))))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        style: {
          fontSize: "1.2em",
          textDecoration: "underline"
        },
        className: "text-center",
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement("div", null, "TOTAL")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Bultos"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Bultos",
        id: "Bultos",
        value: this._getBultosTotal(),
        placeholder: "Bultos",
        disabled: true
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Peso"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Peso",
        id: "Peso",
        value: this._getPesoTotal(),
        placeholder: "Peso",
        disabled: true
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, "kg"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 4,
        md: 4
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        for: "Volumen"
      }, "VOLUMEN"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        name: "Volumen",
        id: "Volumen",
        value: this._getVolumenTotal(),
        placeholder: "Volumen",
        disabled: true
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["y" /* InputGroupAddon */], {
        addonType: "append"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["z" /* InputGroupText */], null, "M\xB3")))))))));
    }
  }]);

  return App;
}(react["Component"]);

/* harmony default export */ var calculadoras = __webpack_exports__["default"] = (calculadoras_App);

/***/ })

}]);
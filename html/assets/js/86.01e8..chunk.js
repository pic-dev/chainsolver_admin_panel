(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[86],{

/***/ 1507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(784);
/* harmony import */ var _Utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(16);







var _image;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Font */ "b"].register({
  family: "Arial Black",
  src: "../assets/fonts/arialBlack/arialBlack.ttf"
});
var styles = _react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* StyleSheet */ "e"].create({
  body: {
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 30,
    paddingBottom: 0
  },
  sectionDate: {
    lineHeight: 1.3,
    fontSize: 9,
    marginTop: 5,
    textAlign: "right"
  },
  sectionClient: {
    lineHeight: 1.3,
    fontSize: 9,
    marginTop: 5,
    textAlign: "left"
  },
  importante: {
    marginLeft: 10,
    marginTop: "3",
    width: "90%",
    flexDirection: "row"
  },
  section2: {
    flexDirection: "row",
    color: "white",
    backgroundColor: "#a43542",
    width: 400,
    fontSize: 12,
    textAlign: "center",
    borderRadius: "5"
  },
  section3: {
    textAlign: "center",
    marginLeft: 10,
    width: 110,
    flexDirection: "row",
    color: "black",
    backgroundColor: "#e4e4e4",
    fontSize: 12,
    borderRadius: "5"
  },
  bulletPoint: {
    flexDirection: "row",
    width: 20,
    fontSize: 12,
    color: "#3b83bd"
  },
  bulletPoint2: {
    flexDirection: "row",
    fontSize: 7
  },
  serviciosResumen: {
    lineHeight: 1,
    textAlign: "justify",
    flexDirection: "row",
    paddingBottom: 3,
    fontSize: 7.5
  },
  serviciosLogisticos: {
    lineHeight: 1.2,
    width: "90%",
    left: "10",
    flexDirection: "row",
    fontSize: 8
  },
  title: {
    fontSize: 12,
    textAlign: "center"
  },
  validtitle: {
    fontSize: 12,
    textAlign: "center",
    color: "grey"
  },
  title2: {
    fontSize: 12,
    textAlign: "center",
    color: "white",
    backgroundColor: "#a43542",
    borderRadius: "5"
  },
  image: {
    width: 80,
    height: 40,
    marginVertical: 0,
    marginHorizontal: 0
  },
  image3: (_image = {
    width: 35,
    height: 20,
    marginHorizontal: 0
  }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_image, "width", "10%"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_image, "flexDirection", "col"), _image),
  image2: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 100,
    marginVertical: 3
  }
});

var Pdf = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(Pdf, _Component);

  var _super = _createSuper(Pdf);

  function Pdf(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Pdf);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Pdf, [{
    key: "render",
    value: function render() {
      var formatter = new Intl.NumberFormat("de-DE");
      var impuestos;
      var subtotal;
      var total;
      var valido;
      var totalIGVCostos;
      var usd;
      var ruc;
      var contacto;
      var data = JSON.parse(sessionStorage.getItem("user"));
      var cliente;

      if (localStorage.empresa !== undefined && localStorage.empresa.length > 0) {
        cliente = localStorage.empresa.toUpperCase();
      } else {
        cliente = data[1].toString();
      }

      if (localStorage.telefono !== undefined && localStorage.telefono.length > 0) {
        contacto = data[2].toString() + " / " + localStorage.telefono;
      } else {
        contacto = data[2].toString();
      }

      impuestos = this.props.taxesResumen;
      subtotal = this.props.subtotalResumen;
      total = this.props.TotalResumen;
      totalIGVCostos = this.props.totalIGVCostos;
      valido = JSON.parse(localStorage.fechas_vigencia);
      usd = impuestos[0] == "No cotizado" ? "" : "USD";

      if (localStorage.ruc !== undefined) {
        ruc = localStorage.ruc;
      } else {
        ruc = "No requerido";
      }

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Document */ "a"], null, total.length > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Page */ "d"], {
        size: "A4"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.body
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Image */ "c"], {
        style: styles.image,
        src: "/assets/img/login/icons/LOGO.png"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.sectionDate
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "Lima,"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, Object(_Utils__WEBPACK_IMPORTED_MODULE_8__[/* getDateWithFormat */ "h"])())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "ID: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " ", JSON.parse(localStorage.token)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.sectionClient
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "Cliente: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " ", cliente)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "Cont\xE1cto: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " ", contacto)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "RUC: "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " ", ruc))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "8"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.title
      }, "PRESUPUESTO FORMAL")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "10"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.title2
      }, "DETALLES DEL SERVICIO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "10"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.validtitle
      }, "Tarifa v\xE1lida Desde el ", valido[0], " - Hasta el ", valido[1])), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "10",
          borderTop: "1 solid grey",
          borderBottom: "1 solid grey",
          padding: "3"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.validtitle
      }, "TRANSPORTE ", localStorage.transporte, " - ", localStorage.puerto, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "=>"), localStorage.puertoD)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: [styles.serviciosResumen, {
          paddingTop: 10
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          left: "35",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Tipo"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.tipocon))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Transporte a domicilio"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          right: "35",
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value7 == "true" ? localStorage.ProvinciaN + " - " + localStorage.distritoN : "NO"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.serviciosResumen
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          left: "35",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Cantidad"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.contenedoresDes))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Seguro de Mercanc\xEDa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          right: "35",
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value8 == "true" ? "SI" : "NO"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.serviciosResumen
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          left: "35",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Gastos portuarios"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value5 == "true" ? "SI" : "NO"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Impuestos de Aduana"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          right: "35",
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value6 == "true" ? "SI" : "NO"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: styles.serviciosResumen
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          left: "35",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Valor de mercanc\xEDa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value6 == "true" ? formatter.format(localStorage.monto) + " " + "USD" : "NO"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingRight: 10,
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), " Permisos de Importaci\xF3n"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          right: "35",
          textAlign: "right",
          width: "50%",
          flexDirection: "col"
        }
      }, localStorage.value6 == "true" ? localStorage.regulador : "No requiere"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          textAlign: "center",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.section2
      }, "SERVICIO LOGISTICO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.section3
      }, "MONTO"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: [styles.serviciosResumen, {
          marginTop: 5,
          marginBottom: 2,
          width: "100%",
          height: "150",
          flexDirection: "row"
        }]
      }, localStorage.tipocon == "Contenedor completo" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "85%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "Incluye")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "FLETE MARITIMO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "THCD (Manejo de naviera en Destino)"), JSON.parse(localStorage.check) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, JSON.parse(localStorage.check) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "VISTOS BUENOS") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, JSON.parse(localStorage.check) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "GATE IN (Recepci\xF3n de contenedor vacio en el puerto)") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false || JSON.parse(localStorage.check2) == false || JSON.parse(localStorage.check1) == false || !JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginTop: 1,
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "No incluye")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, JSON.parse(localStorage.check) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "VISTOS BUENOS") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, JSON.parse(localStorage.check) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "GATE IN (Recepci\xF3n de contenedor vacio en el puerto)") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.tipocon == "Contenedor compartido" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "85%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "Incluye")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "FLETE MARITIMO"), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HANDLING Y MANEJO DESTINO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PANAMA" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "DESCARGA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PANAMA" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "DMC DE SALIDA GASTOS PORTUARIOS") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PANAMA" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "DESCONSOLIDACI\xD3N") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PANAMA" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "MANEJOS") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false || JSON.parse(localStorage.check2) == false || JSON.parse(localStorage.check1) == false || !JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginTop: 1,
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "No incluye")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.transporte == "AÉREO" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "85%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "Incluye")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "FLETE AEREO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "GUIA AEREO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "GASTOS EN DESTINO DE LA AEROLINEA"), JSON.parse(localStorage.check) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false || JSON.parse(localStorage.check2) == false || JSON.parse(localStorage.check1) == false || !JSON.parse(localStorage.check4) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: ([styles.serviciosLogisticos], {
          marginTop: 1,
          marginBottom: 3,
          fontSize: 10
        })
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "#0d5084",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, "No incluye")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "ALMACEN ADUANERO") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check2) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "HONORARIOS DE AGENCIA DE ADUANA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check1) == false ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "TRANSPORTE A FABRICA IMPORTADOR") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), JSON.parse(localStorage.check4) == true ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.serviciosLogisticos
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint
      }, "\u2022 "), "SEGURO DE MERCANCIA") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          textAlign: "center",
          width: "20%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontFamily: "Arial Black",
          right: 15,
          width: "100%",
          top: 50,
          fontSize: 15
        }
      }, subtotal[0], " USD"), localStorage.paisDestino == "PERU" && totalIGVCostos > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          color: "grey",
          right: 15,
          width: "100%",
          top: 55,
          fontSize: 10
        }
      }, "+ IGV 18% ", totalIGVCostos, " USD") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: 10,
          padding: 10,
          textAlign: "center",
          color: "black",
          backgroundColor: "#e4e4e4"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "100%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Image */ "c"], {
        style: styles.image3,
        src: Object(_Utils__WEBPACK_IMPORTED_MODULE_8__[/* taxes */ "p"])(localStorage.paisDestino)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontSize: 12,
          color: "black",
          width: "40%",
          flexDirection: "col",
          marginHorizontal: 0
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, "IMPUESTOS DE ADUANA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          textAlign: "center",
          fontSize: 10,
          color: "black",
          width: "50%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingLeft: 10,
          paddingRight: 10,
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, "Impuestos Bajos")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingLeft: 10,
          paddingRight: 10,
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, "Impuestos Medio", " ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingLeft: 10,
          paddingRight: 10,
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, "Impuestos Altos")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          paddingTop: 10,
          fontSize: 10,
          width: "100%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontSize: 10,
          color: "black",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, "Valor VARIABLE seg\xFAn descripci\xF3n del producto")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          textAlign: "center",
          color: "black",
          width: "50%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, impuestos[0], " ", usd)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, impuestos[2], " ", usd)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          flexDirection: "col"
        }
      }, impuestos[1], " ", usd))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontWeight: "bold",
          padding: "5",
          textAlign: "center",
          fontSize: 10,
          color: "white",
          backgroundColor: "#a43542",
          width: "100%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          color: "white",
          backgroundColor: "#a43542",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontFamily: "Arial Black",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, impuestos[0] === "No cotizado" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "TOTAL", localStorage.paisDestino == "PERU" && totalIGVCostos > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " (incluye IGV)") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, "TOTALES", localStorage.paisDestino == "PERU" && totalIGVCostos > 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], null, " (incluye IGV)") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      })))), impuestos[0] === "No cotizado" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          color: "white",
          backgroundColor: "#a43542",
          width: "50%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontFamily: "Arial Black",
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, " ", total[0], " USD")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontFamily: "Arial Black",
          textAlign: "center",
          color: "white",
          backgroundColor: "#a43542",
          width: "50%",
          flexDirection: "row"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontWeight: "bold",
          color: "white",
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, total[0], " USD")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontWeight: "bold",
          color: "white",
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, total[2], " USD")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          fontWeight: "bold",
          color: "white",
          width: "33%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontWeight: "bold",
          flexDirection: "col"
        }
      }, total[1], " USD")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "3",
          borderBottom: "1 solid grey",
          padding: "3"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: [styles.serviciosResumen, {
          width: "100%",
          flexDirection: "row",
          paddingTop: 7
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          padding: 10,
          left: 40,
          backgroundColor: "#f5f5f5",
          width: "85%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          fontSize: 12,
          left: 150,
          width: "100%",
          flexDirection: "col"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Image */ "c"], {
        style: {
          width: 10,
          height: 10
        },
        src: "/assets/img/login/icons/infoPDF.png"
      }), "  ", "IMPORTANTE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: [styles.importante, {
          marginTop: "7"
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Tarifas no aplican para mercanc\xEDa peligrosa."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.importante
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Salidas Semanales."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.importante
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Costos adicionales sugeridos por inspecciones aleatorias de aduana, con controladas por PIC Cargo S.A.C."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.importante
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Tarifas no aplican para mercanc\xEDa con sobrepeso."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.importante
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Los documentos del embarque son responsabilidad del proveedor y consignatario."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.importante
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "El embalaje de la carga debe ser acto para transporte."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: [styles.importante, {
          color: "red"
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Tarifa basada en peso y volumen suministrado."), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: [styles.importante, {
          color: "red"
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Si el canal asignado por aduana es ROJO el costo aumentar\xE1 USD 150 - 200.") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: [styles.importante, {
          color: "red"
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Todos los servicios que se realicen dentro del territorio peruano generar\xE1n IGV de 18%.") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }), localStorage.paisDestino == "PERU" ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: [styles.importante, {
          color: "red"
        }]
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: styles.bulletPoint2
      }, "\u2022 "), "Para su informaci\xF3n el FLETE no genera IGV.") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Text */ "f"], {
        style: {
          margin: 0
        }
      }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* View */ "g"], {
        style: {
          marginTop: "3",
          borderBottom: "1 solid grey",
          padding: "3"
        }
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_react_pdf_renderer__WEBPACK_IMPORTED_MODULE_7__[/* Image */ "c"], {
        style: styles.image2,
        src: Object(_Utils__WEBPACK_IMPORTED_MODULE_8__[/* footer */ "f"])(localStorage.paisDestino)
      })) : "");
    }
  }]);

  return Pdf;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Pdf);

/***/ })

}]);
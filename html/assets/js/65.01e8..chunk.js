(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[65],{

/***/ 1532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ CollapsibleTable; });

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/slicedToArray.js
var slicedToArray = __webpack_require__(8);
var slicedToArray_default = /*#__PURE__*/__webpack_require__.n(slicedToArray);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);
var prop_types_default = /*#__PURE__*/__webpack_require__.n(prop_types);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(845);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Box/Box.js + 12 modules
var Box = __webpack_require__(1517);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Collapse/Collapse.js
var Collapse = __webpack_require__(1474);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/IconButton/IconButton.js
var IconButton = __webpack_require__(1443);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Table/Table.js
var Table = __webpack_require__(1489);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableBody/TableBody.js
var TableBody = __webpack_require__(1487);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableCell/TableCell.js
var TableCell = __webpack_require__(1455);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableContainer/TableContainer.js
var TableContainer = __webpack_require__(1490);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableHead/TableHead.js
var TableHead = __webpack_require__(1491);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TableRow/TableRow.js
var TableRow = __webpack_require__(1488);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TablePagination/TablePagination.js + 3 modules
var TablePagination = __webpack_require__(1527);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Typography/Typography.js
var Typography = __webpack_require__(1451);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Paper/Paper.js
var Paper = __webpack_require__(1440);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/KeyboardArrowDown.js
var KeyboardArrowDown = __webpack_require__(649);
var KeyboardArrowDown_default = /*#__PURE__*/__webpack_require__.n(KeyboardArrowDown);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/KeyboardArrowUp.js
var KeyboardArrowUp = __webpack_require__(648);
var KeyboardArrowUp_default = /*#__PURE__*/__webpack_require__.n(KeyboardArrowUp);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Delete.js
var Delete = __webpack_require__(644);
var Delete_default = /*#__PURE__*/__webpack_require__.n(Delete);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Edit.js
var Edit = __webpack_require__(743);
var Edit_default = /*#__PURE__*/__webpack_require__.n(Edit);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Lock.js
var Lock = __webpack_require__(855);
var Lock_default = /*#__PURE__*/__webpack_require__.n(Lock);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Visibility.js
var Visibility = __webpack_require__(652);
var Visibility_default = /*#__PURE__*/__webpack_require__.n(Visibility);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(17);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Button/Button.js
var Button = __webpack_require__(1426);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Dialog/Dialog.js
var Dialog = __webpack_require__(1482);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogActions/DialogActions.js
var DialogActions = __webpack_require__(1486);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogContent/DialogContent.js
var DialogContent = __webpack_require__(1484);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/DialogTitle/DialogTitle.js
var DialogTitle = __webpack_require__(1483);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Slide/Slide.js
var Slide = __webpack_require__(1480);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Save.js
var Save = __webpack_require__(854);
var Save_default = /*#__PURE__*/__webpack_require__.n(Save);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Grow/Grow.js
var Grow = __webpack_require__(1438);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Grid/Grid.js
var Grid = __webpack_require__(1472);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/AppBar/AppBar.js
var AppBar = __webpack_require__(1508);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Toolbar/Toolbar.js
var Toolbar = __webpack_require__(1456);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/MenuItem/MenuItem.js
var MenuItem = __webpack_require__(1447);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/TextField/TextField.js + 1 modules
var TextField = __webpack_require__(1536);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/Close.js
var Close = __webpack_require__(742);
var Close_default = /*#__PURE__*/__webpack_require__.n(Close);

// EXTERNAL MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/modalConfirmDelete.js
var modalConfirmDelete = __webpack_require__(647);

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__(79);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/VerPedidos/modalEditOrder.js
























var TablePedido = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(77)]).then(__webpack_require__.bind(null, 1511));
});
var TablePedido1 = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(78)]).then(__webpack_require__.bind(null, 1512));
});
var modalEditOrder_Transition = /*#__PURE__*/react_default.a.forwardRef(function Transition(props, ref) {
  return /*#__PURE__*/react_default.a.createElement(Slide["a" /* default */], extends_default()({
    direction: "up",
    in: props.open,
    ref: ref
  }, props));
});
var useStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "25ch"
      },
      TextField: {
        margin: theme.spacing(1),
        width: "25ch"
      },
      appBar: {
        position: "relative"
      },
      title: {
        marginLeft: theme.spacing(2),
        flex: 1
      },
      form: {
        width: "100%",
        // Fix IE 11 issue.
        marginTop: theme.spacing(3)
      },
      buttons: {
        display: "flex",
        justifyContent: "flex-end"
      },
      button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
      }
    }
  };
});
function ModalEdit(_ref) {
  var handleModal = _ref.handleModal,
      handleEdit = _ref.handleEdit,
      open = _ref.open,
      orden = _ref.orden;
  var classes = useStyles();

  var _React$useState = react_default.a.useState(false),
      _React$useState2 = slicedToArray_default()(_React$useState, 2),
      openEditForm = _React$useState2[0],
      setOpenEditForm = _React$useState2[1];

  var _React$useState3 = react_default.a.useState(false),
      _React$useState4 = slicedToArray_default()(_React$useState3, 2),
      New = _React$useState4[0],
      setNew = _React$useState4[1];

  var _React$useState5 = react_default.a.useState(false),
      _React$useState6 = slicedToArray_default()(_React$useState5, 2),
      opentable = _React$useState6[0],
      setOpenTable = _React$useState6[1];

  var _React$useState7 = react_default.a.useState([]),
      _React$useState8 = slicedToArray_default()(_React$useState7, 2),
      selectedOrden = _React$useState8[0],
      setSelectedOrden = _React$useState8[1];

  var _React$useState9 = react_default.a.useState([]),
      _React$useState10 = slicedToArray_default()(_React$useState9, 2),
      catalogosDisponibles = _React$useState10[0],
      setCatalogosDisponibles = _React$useState10[1];

  var _React$useState11 = react_default.a.useState([]),
      _React$useState12 = slicedToArray_default()(_React$useState11, 2),
      selectedEdit = _React$useState12[0],
      setSelectedEdit = _React$useState12[1];

  var _React$useState13 = react_default.a.useState(false),
      _React$useState14 = slicedToArray_default()(_React$useState13, 2),
      openModalDelete = _React$useState14[0],
      setOpenModalDelete = _React$useState14[1];

  var _React$useState15 = react_default.a.useState(""),
      _React$useState16 = slicedToArray_default()(_React$useState15, 2),
      idDelete = _React$useState16[0],
      setIdDelete = _React$useState16[1];

  Object(react["useEffect"])(function () {
    setSelectedOrden(orden.productos);
    setOpenTable(!opentable);
    getCatalogos();
  }, []);

  var handleSelected = function handleSelected(event) {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setSelectedEdit(event);
    document.getElementById("Pagina").value = event.pagina;
    document.getElementById("Producto").value = event.producto;
    document.getElementById("CodigoProducto").value = event.codigo_producto;
    document.getElementById("CantidadProducto").value = event.cantidad;
    document.getElementById("precio").value = event.precio;
  };

  var handleNew = function handleNew() {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setNew(true);
  };

  var handleDelete = function handleDelete(id) {
    var data = selectedOrden.filter(function (item) {
      if (item.id !== id) {
        return item;
      }
    });

    if (data.length > 0) {
      setSelectedOrden(data);
      setOpenModalDelete(!openModalDelete);
    }
  };

  var getCatalogos = /*#__PURE__*/function () {
    var _ref2 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee() {
      return regenerator_default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return axios_default.a.get("https://point.qreport.site/catalogos").then(function (res) {
                setCatalogosDisponibles(res.data);
              }).catch(function (error) {
                console.log(error);
              });

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function getCatalogos() {
      return _ref2.apply(this, arguments);
    };
  }();

  var handleModalDelete = function handleModalDelete(e) {
    setIdDelete(e);
    setOpenModalDelete(!openModalDelete);
  };

  var handleSubmit = /*#__PURE__*/function () {
    var _ref3 = asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee2(e) {
      var _e$target$elements, Pagina, Producto, CodigoProducto, CantidadProducto, precio, catalogo, newItem, data, arreglo;

      return regenerator_default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              e.preventDefault();
              _e$target$elements = e.target.elements, Pagina = _e$target$elements.Pagina, Producto = _e$target$elements.Producto, CodigoProducto = _e$target$elements.CodigoProducto, CantidadProducto = _e$target$elements.CantidadProducto, precio = _e$target$elements.precio, catalogo = _e$target$elements.catalogo;
              setNew(false);
              newItem = {
                id_cliente: Number(selectedOrden[0].id_cliente),
                id_catalogo: catalogo ? Number(catalogo.value) : Number(selectedEdit.id_catalogo),
                pagina: Number(Pagina.value),
                producto: Producto.value,
                codigo_producto: CodigoProducto.value ? CodigoProducto.value : "",
                cantidad: Number(CantidadProducto.value),
                precio: precio ? Number(precio.value) : 0,
                observacion: selectedEdit.observacion ? selectedEdit.observacion : "",
                subtotal: precio ? Number(precio.value) * Number(CantidadProducto.value) : 0,
                id: selectedEdit.id ? selectedEdit.id : Date.now(),
                id_orden: selectedOrden[0].id_orden
              };

              if (selectedEdit) {
                data = selectedOrden.filter(function (item) {
                  if (item.id !== selectedEdit.id) {
                    return item;
                  }
                });
                arreglo = data.concat(newItem);
              } else {
                data = selectedOrden;
                arreglo = data.concat(newItem);
              }

              if (arreglo) {
                setSelectedOrden(arreglo);
                setOpenEditForm(!openEditForm);
                setOpenTable(!opentable);
                setSelectedEdit([]);
                document.getElementById("Pagina").value = "";
                document.getElementById("Producto").value = "";
                document.getElementById("CodigoProducto").value = "";
                document.getElementById("CantidadProducto").value = "";
                document.getElementById("precio").value = "";
              }

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSubmit(_x) {
      return _ref3.apply(this, arguments);
    };
  }();

  var handleCancel = function handleCancel() {
    setOpenEditForm(!openEditForm);
    setOpenTable(!opentable);
    setSelectedEdit([]);
    document.getElementById("Pagina").value = "";
    document.getElementById("Producto").value = "";
    document.getElementById("CodigoProducto").value = "";
    document.getElementById("CantidadProducto").value = "";
    document.getElementById("precio").value = "";
  };

  return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(Dialog["a" /* default */], {
    open: open,
    TransitionComponent: modalEditOrder_Transition,
    fullWidth: true,
    fullScreen: Object(Utils["m" /* isMovil */])(),
    maxWidth: "lg",
    keepMounted: true,
    onClose: handleModal,
    "aria-labelledby": "alert-dialog-slide-title",
    "aria-describedby": "alert-dialog-slide-description"
  }, Object(Utils["m" /* isMovil */])() && /*#__PURE__*/react_default.a.createElement(AppBar["a" /* default */], {
    className: classes.appBar
  }, /*#__PURE__*/react_default.a.createElement(Toolbar["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    edge: "start",
    color: "inherit",
    onClick: handleModal,
    "aria-label": "close"
  }, /*#__PURE__*/react_default.a.createElement(Close_default.a, null)), !openEditForm && /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    edge: "end",
    color: "inherit",
    onClick: function onClick() {
      return handleEdit(selectedOrden);
    },
    "aria-label": "save"
  }, /*#__PURE__*/react_default.a.createElement(Save_default.a, null)))), /*#__PURE__*/react_default.a.createElement(DialogTitle["a" /* default */], {
    id: "alert-dialog-slide-title"
  }, "Actualizar orden"), /*#__PURE__*/react_default.a.createElement(DialogContent["a" /* default */], null, selectedOrden && opentable && !Object(Utils["m" /* isMovil */])() && /*#__PURE__*/react_default.a.createElement(TablePedido, {
    delete: handleModalDelete,
    edit: handleSelected,
    new: handleNew,
    productos: selectedOrden
  }), selectedOrden && opentable && Object(Utils["m" /* isMovil */])() && /*#__PURE__*/react_default.a.createElement(TablePedido1, {
    delete: handleModalDelete,
    edit: handleSelected,
    new: handleNew,
    productos: selectedOrden
  }), /*#__PURE__*/react_default.a.createElement(Grow["a" /* default */], {
    in: openEditForm
  }, /*#__PURE__*/react_default.a.createElement("form", {
    className: classes.form,
    style: {
      paddingTop: ".5rem"
    },
    onSubmit: handleSubmit
  }, /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    container: true,
    spacing: 4
  }, New && /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    autoComplete: "catalogo",
    name: "catalogo",
    required: true,
    select: true,
    fullWidth: true,
    InputLabelProps: {
      shrink: true
    },
    id: "catalogo",
    label: "catalogo",
    autoFocus: true
  }, catalogosDisponibles.map(function (option) {
    return /*#__PURE__*/react_default.a.createElement(MenuItem["a" /* default */], {
      key: option.id,
      value: option.id
    }, option.name);
  }))), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    autoComplete: "Pagina",
    name: "Pagina",
    required: true,
    fullWidth: true,
    InputLabelProps: {
      shrink: true
    },
    id: "Pagina",
    label: "Pagina",
    autoFocus: true
  })), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 3
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    autoComplete: "Producto",
    name: "Producto",
    required: true,
    fullWidth: true,
    InputLabelProps: {
      shrink: true
    },
    id: "Producto",
    label: "Producto"
  })), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 3
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    fullWidth: true,
    id: "CodigoProducto",
    label: "Codigo",
    InputLabelProps: {
      shrink: true
    },
    name: "CodigoProducto",
    autoComplete: "Codigo"
  })), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    required: true,
    fullWidth: true,
    id: "CantidadProducto",
    label: "Qty",
    InputLabelProps: {
      shrink: true
    },
    name: "CantidadProducto",
    autoComplete: "Cantidad"
  })), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    item: true,
    xs: 4,
    sm: 2
  }, /*#__PURE__*/react_default.a.createElement(TextField["a" /* default */], {
    fullWidth: true,
    id: "precio",
    label: "precio",
    InputLabelProps: {
      shrink: true
    },
    name: "precio",
    autoComplete: "precio"
  })), /*#__PURE__*/react_default.a.createElement(Grid["a" /* default */], {
    className: classes.buttons,
    item: true,
    xs: 12,
    sm: 12
  }, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    onClick: handleCancel,
    className: classes.button
  }, "Cancelar"), /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    type: "submit",
    color: "primary",
    className: classes.button
  }, /*#__PURE__*/react_default.a.createElement(Save_default.a, null), " Guardar"))), " ")), !openEditForm && !Object(Utils["m" /* isMovil */])() && /*#__PURE__*/react_default.a.createElement(DialogActions["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    onClick: handleModal,
    color: "primary"
  }, "Cancelar"), /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    onClick: function onClick() {
      return handleEdit(selectedOrden);
    },
    color: "primary"
  }, /*#__PURE__*/react_default.a.createElement(Save_default.a, null), " Actualizar Orden")))), /*#__PURE__*/react_default.a.createElement(modalConfirmDelete["a" /* default */], {
    id: idDelete,
    handleDelete: handleDelete,
    open: openModalDelete,
    handleModal: handleModalDelete,
    type: "Producto"
  }));
}
// EXTERNAL MODULE: ./src/util/dialogAlert.js
var dialogAlert = __webpack_require__(651);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Tooltip/Tooltip.js
var Tooltip = __webpack_require__(1445);

// EXTERNAL MODULE: ./src/components/spinner/index.js
var spinner = __webpack_require__(58);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/Accordion/Accordion.js
var Accordion = __webpack_require__(1473);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/AccordionSummary/AccordionSummary.js
var AccordionSummary = __webpack_require__(1475);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/AccordionDetails/AccordionDetails.js
var AccordionDetails = __webpack_require__(1476);

// EXTERNAL MODULE: ./node_modules/@material-ui/icons/ExpandMore.js
var ExpandMore = __webpack_require__(646);
var ExpandMore_default = /*#__PURE__*/__webpack_require__.n(ExpandMore);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/colors/green.js
var green = __webpack_require__(43);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/colors/red.js
var red = __webpack_require__(41);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/VerPedidos/verPdf.js







var PdfMovil = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(63), __webpack_require__.e(58)]).then(__webpack_require__.bind(null, 913));
});
function verPdf(_ref) {
  var img = _ref.img,
      pag = _ref.pag,
      open = _ref.open,
      handleClose = _ref.handleClose;
  return /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(Dialog["a" /* default */], {
    open: open,
    onClose: handleClose,
    "aria-labelledby": "Catalogo",
    "aria-describedby": "Catalogo"
  }, /*#__PURE__*/react_default.a.createElement(DialogTitle["a" /* default */], {
    id: "Catalogo"
  }, "Catalogo"), /*#__PURE__*/react_default.a.createElement(DialogContent["a" /* default */], null, !Object(Utils["m" /* isMovil */])() ? /*#__PURE__*/react_default.a.createElement("iframe", {
    style: {
      height: Object(Utils["m" /* isMovil */])() ? "30rem" : "50rem",
      width: "34rem"
    },
    src: "https://point.qreport.site/files/".concat(img, "#page=").concat(pag, "&zoom=110&navpanes=0&scrollbar=0&view=Fit")
  }) : /*#__PURE__*/react_default.a.createElement(PdfMovil, {
    style: {
      height: "30rem",
      width: "100%"
    },
    pag: pag,
    url: "https://point.qreport.site/files/".concat(img)
  })), /*#__PURE__*/react_default.a.createElement(DialogActions["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(Button["a" /* default */], {
    onClick: handleClose,
    color: "primary",
    autoFocus: true
  }, "Cerrar"))));
}
// EXTERNAL MODULE: ./node_modules/lodash/lodash.js
var lodash = __webpack_require__(593);
var lodash_default = /*#__PURE__*/__webpack_require__.n(lodash);

// CONCATENATED MODULE: ./src/routes/pages/importadoresDashboard/Dashboard/VerPedidos/tableRow.js





































var useRowStyles = Object(makeStyles["a" /* default */])(function (theme) {
  return {
    root: {
      width: "100%",
      "& > *": {
        borderBottom: "unset"
      }
    },
    loading: {
      width: "100%"
    },
    box: {
      boxShadow: "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
      padding: 0
    },
    heading: {
      fontSize: theme.typography.pxToRem(13),
      flexBasis: "100%",
      flexShrink: 0,
      textAlign: "left",
      width: "9rem",
      padding: 0
    }
  };
});

function ccyFormat(num) {
  return "".concat(num.toFixed(2));
}

function priceRow(qty, unit) {
  return qty * unit;
}

function subtotal(items) {
  return items.productos.map(function (_ref) {
    var subtotal = _ref.subtotal;
    return subtotal;
  }).reduce(function (sum, i) {
    return sum + i;
  }, 0);
}

function subtotalOrden(items) {
  return items.map(function (_ref2) {
    var subtotal = _ref2.subtotal;
    return subtotal;
  }).reduce(function (sum, i) {
    return sum + i;
  }, 0);
}

function statusItem(items) {
  return items.productos.map(function (_ref3) {
    var status = _ref3.status;
    return status;
  });
}

function filterDelete(items, id) {
  return items.filter(function (item) {
    if (item.id_orden != id) {
      return item;
    }
  });
}

function Row(props) {
  var row = props.row,
      updateData = props.updateData,
      ordenesData = props.ordenesData;

  var _React$useState = react_default.a.useState(false),
      _React$useState2 = slicedToArray_default()(_React$useState, 2),
      open = _React$useState2[0],
      setOpen = _React$useState2[1];

  var _useState = Object(react["useState"])(""),
      _useState2 = slicedToArray_default()(_useState, 2),
      url = _useState2[0],
      setUrl = _useState2[1];

  var _useState3 = Object(react["useState"])(""),
      _useState4 = slicedToArray_default()(_useState3, 2),
      pag = _useState4[0],
      setPag = _useState4[1];

  var _useState5 = Object(react["useState"])(false),
      _useState6 = slicedToArray_default()(_useState5, 2),
      openPdf = _useState6[0],
      setOpenPdf = _useState6[1];

  var _React$useState3 = react_default.a.useState(""),
      _React$useState4 = slicedToArray_default()(_React$useState3, 2),
      idDelete = _React$useState4[0],
      setIdDelete = _React$useState4[1];

  var _React$useState5 = react_default.a.useState([]),
      _React$useState6 = slicedToArray_default()(_React$useState5, 2),
      ordenEdit = _React$useState6[0],
      setOrdenEdit = _React$useState6[1];

  var _React$useState7 = react_default.a.useState(false),
      _React$useState8 = slicedToArray_default()(_React$useState7, 2),
      openModalDelete = _React$useState8[0],
      setOpenModalDelete = _React$useState8[1];

  var _React$useState9 = react_default.a.useState(false),
      _React$useState10 = slicedToArray_default()(_React$useState9, 2),
      openModalEdit = _React$useState10[0],
      setOpenModalEdit = _React$useState10[1];

  var _useState7 = Object(react["useState"])({}),
      _useState8 = slicedToArray_default()(_useState7, 2),
      alertData = _useState8[0],
      setAlertData = _useState8[1];

  var _useState9 = Object(react["useState"])(false),
      _useState10 = slicedToArray_default()(_useState9, 2),
      alert = _useState10[0],
      setAlert = _useState10[1];

  var classes = useRowStyles();
  var invoiceTotal = subtotal(row);
  var status = statusItem(row);

  var handleModalEdit = function handleModalEdit(e) {
    setOrdenEdit(e);
    setOpenModalEdit(!openModalEdit);
  };

  var handleModalDelete = function handleModalDelete(e) {
    setIdDelete(e);
    setOpenModalDelete(!openModalDelete);
  };

  var handleEdit = function handleEdit(e) {
    axios_default.a.delete("https://point.qreport.site/productos/orden/".concat(e[0].id_orden)).then(function (res) {
      if (res.status == 200) {
        e.map(function (item, index) {
          axios_default.a.post("https://point.qreport.site/productos", {
            cliente: Number(item.id_cliente),
            catalogo: Number(item.id_catalogo),
            orden: Number(item.id_orden),
            codigo_producto: item.codigo_producto,
            pagina: Number(item.pagina),
            producto: item.producto,
            cantidad: Number(item.cantidad),
            precio: item.precio > 0 ? item.precio : 0,
            observacion: item.observacion
          }).then(function (res) {
            if (res.data.boolean && e.length == index + 1) {
              toggleAlert(1);
              handleModalEdit([]);
              updateData();
            }
          }).catch(function (error) {
            console.log(error);
          });
        });
      }
    }).catch(function (error) {
      console.log(error);
    });
  };

  var handleDelete = function handleDelete(e) {
    axios_default.a.delete("https://point.qreport.site/productos/orden/".concat(e)).then(function (res) {
      if (res.status == 200) {
        handleModalDelete("");
        toggleAlert(2);
        props.setSelectedJunior(filterDelete(ordenesData, e));
      }
    }).catch(function (error) {
      console.log(error);
    });
  };

  var toggleAlert = function toggleAlert(e) {
    switch (e) {
      case 1:
        setAlertData({
          message: "Orden modificada exitosamente",
          type: "success"
        });
        setAlert(true);
        break;

      case 2:
        setAlertData({
          message: "Orden Eliminada exitosamente",
          type: "error"
        });
        setAlert(true);
        break;

      default:
        break;
    }
  };

  var handleClickOpen = function handleClickOpen(direccion, pagina) {
    setUrl(direccion);
    setPag(pagina);
    setOpenPdf(true);
  };

  var handleClose = function handleClose() {
    setUrl("");
    setPag("");
    setOpenPdf(false);
  };

  var _React$useState11 = react_default.a.useState(false),
      _React$useState12 = slicedToArray_default()(_React$useState11, 2),
      expanded = _React$useState12[0],
      setExpanded = _React$useState12[1];

  var handleChange = function handleChange(panel) {
    return function (event, isExpanded) {
      setExpanded(isExpanded ? panel : false);
    };
  };

  return /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, !Object(Utils["m" /* isMovil */])() ? /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
    className: classes.root
  }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    onClick: function onClick() {
      return setOpen(!open);
    },
    style: {
      padding: 1
    },
    align: "left"
  }, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    "aria-label": "expand row",
    size: "small"
  }, open ? /*#__PURE__*/react_default.a.createElement(KeyboardArrowUp_default.a, null) : /*#__PURE__*/react_default.a.createElement(KeyboardArrowDown_default.a, null)), /*#__PURE__*/react_default.a.createElement("span", {
    style: {
      fontWeight: "bold"
    }
  }, "Orden:"), " ", row.idOrden), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    onClick: function onClick() {
      return setOpen(!open);
    },
    style: {
      padding: 1
    },
    align: "left"
  }, " ", /*#__PURE__*/react_default.a.createElement("span", {
    style: {
      fontWeight: "bold"
    }
  }, "Subtotal:"), " $ " + ccyFormat(invoiceTotal)), status[0] == 1 ? /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    style: {
      padding: 1
    },
    align: "right"
  }, " ", /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    "aria-label": "update row",
    size: "small",
    onClick: function onClick() {
      return handleModalEdit(row);
    }
  }, /*#__PURE__*/react_default.a.createElement(Edit_default.a, {
    style: {
      color: green["a" /* default */][500]
    }
  })), /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    "aria-label": "delete row",
    size: "small",
    onClick: function onClick() {
      return handleModalDelete(row.idOrden);
    }
  }, /*#__PURE__*/react_default.a.createElement(Delete_default.a, {
    style: {
      color: red["a" /* default */][500]
    }
  }))) : /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    align: "right"
  }, /*#__PURE__*/react_default.a.createElement(Tooltip["a" /* default */], {
    title: "Pedido Cerrada"
  }, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    size: "small",
    "aria-label": "closed"
  }, /*#__PURE__*/react_default.a.createElement(Lock_default.a, {
    style: {
      color: red["a" /* default */][500]
    }
  }))))), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    style: {
      paddingBottom: 0,
      paddingTop: 0
    },
    colSpan: 6
  }, /*#__PURE__*/react_default.a.createElement(Collapse["a" /* default */], {
    in: open,
    timeout: "auto",
    unmountOnExit: true
  }, /*#__PURE__*/react_default.a.createElement(Box["a" /* default */], {
    margin: 1
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    variant: "h6",
    gutterBottom: true,
    component: "div"
  }, "Productos"), /*#__PURE__*/react_default.a.createElement(Table["a" /* default */], {
    size: "small",
    "aria-label": "products"
  }, /*#__PURE__*/react_default.a.createElement(TableHead["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Fecha"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Catalogo"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Producto"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Pagina"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Qty"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Codigo"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Precio Unitario"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    align: "right"
  }, "Sub-Total"))), /*#__PURE__*/react_default.a.createElement(TableBody["a" /* default */], null, row.productos.map(function (historyRow) {
    return /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      key: historyRow.id
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.created_at.substr(0, 10)), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.catalogo), /*#__PURE__*/react_default.a.createElement(Tooltip["a" /* default */], {
      title: historyRow.producto
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.producto)), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.pagina, " ", historyRow.ruta_sp && /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
      size: "small",
      "aria-label": "open",
      onClick: function onClick() {
        return handleClickOpen(historyRow.ruta_cp.length > 0 ? historyRow.ruta_cp : historyRow.ruta_sp, historyRow.pagina);
      }
    }, /*#__PURE__*/react_default.a.createElement(Visibility_default.a, null))), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.cantidad), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.codigo_producto), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, historyRow.precio > 0 ? "$ " + ccyFormat(historyRow.precio) : "-"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      align: "right"
    }, historyRow.subtotal > 0 ? "$ " + ccyFormat(historyRow.subtotal) : "-"));
  }), invoiceTotal > 0 && /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "arial text-center",
    colSpan: 6
  }, "Total"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "arial",
    align: "right"
  }, "$ " + ccyFormat(invoiceTotal))), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "text-center color-red",
    colSpan: 7
  }, "*p/c - Precio por Confirmar"))))))))) : /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, /*#__PURE__*/react_default.a.createElement(Accordion["a" /* default */], {
    expanded: expanded === "panel".concat(row.idOrden),
    onChange: handleChange("panel".concat(row.idOrden)),
    className: classes.root
  }, /*#__PURE__*/react_default.a.createElement(AccordionSummary["a" /* default */], {
    expandIcon: /*#__PURE__*/react_default.a.createElement(ExpandMore_default.a, null),
    "aria-controls": "panel1bh".concat(row.idOrden),
    id: "panel".concat(row.idOrden)
  }, " ", /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
    style: {
      fontStyle: "bold"
    }
  }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: classes.heading,
    align: "left"
  }, "Orden: ", row.idOrden, " ", /*#__PURE__*/react_default.a.createElement("br", null), " Subtotal:", " $ " + ccyFormat(invoiceTotal)), status[0] == 1 ? /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    style: {
      padding: 1,
      textAlign: "right"
    },
    align: "right"
  }, " ", /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    "aria-label": "update row",
    size: "small",
    onClick: function onClick() {
      return handleModalEdit(row);
    }
  }, /*#__PURE__*/react_default.a.createElement(Edit_default.a, {
    style: {
      color: green["a" /* default */][500]
    }
  })), /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    "aria-label": "delete row",
    size: "small",
    onClick: function onClick() {
      return handleModalDelete(row.idOrden);
    }
  }, /*#__PURE__*/react_default.a.createElement(Delete_default.a, {
    style: {
      color: red["a" /* default */][500]
    }
  }))) : /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    align: "right"
  }, /*#__PURE__*/react_default.a.createElement(Tooltip["a" /* default */], {
    title: "Pedido Cerrada"
  }, /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
    color: "danger",
    size: "small",
    "aria-label": "closed"
  }, /*#__PURE__*/react_default.a.createElement(Lock_default.a, {
    style: {
      color: red["a" /* default */][500]
    }
  })))))), row.productos.map(function (historyRow) {
    return /*#__PURE__*/react_default.a.createElement(AccordionDetails["a" /* default */], {
      className: classes.box
    }, /*#__PURE__*/react_default.a.createElement(Table["a" /* default */], {
      size: "small",
      "aria-label": "products"
    }, /*#__PURE__*/react_default.a.createElement(TableBody["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      style: {
        fontStyle: "bold"
      }
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Producto"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Pagina"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Qty")), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      key: historyRow.id
    }, /*#__PURE__*/react_default.a.createElement(Tooltip["a" /* default */], {
      title: historyRow.producto
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      style: {
        fontWeight: "300",
        textTransform: "uppercase"
      }
    }, historyRow.producto.substr(0, 25))), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      style: {
        fontWeight: "300"
      }
    }, historyRow.pagina, " ", historyRow.ruta_sp && /*#__PURE__*/react_default.a.createElement(IconButton["a" /* default */], {
      size: "small",
      "aria-label": "open",
      onClick: function onClick() {
        return handleClickOpen(historyRow.ruta_cp.length > 0 ? historyRow.ruta_cp : historyRow.ruta_sp, historyRow.pagina);
      }
    }, /*#__PURE__*/react_default.a.createElement(Visibility_default.a, null))), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      style: {
        fontWeight: "300"
      }
    }, historyRow.cantidad)), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      style: {
        fontStyle: "bold"
      }
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Codigo"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "P/U"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], null, "Monto")), /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], {
      key: historyRow.id
    }, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      style: {
        fontWeight: "300"
      }
    }, historyRow.codigo_producto), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      className: "p-1",
      style: {
        fontWeight: "300"
      }
    }, " ", historyRow.precio > 0 ? "$ " + historyRow.precio : "p/c"), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      className: "p-1",
      style: {
        fontWeight: "300"
      }
    }, historyRow.subtotal > 0 ? "$ " + ccyFormat(historyRow.subtotal) : "-")), historyRow.precio == 0 && /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
      className: "text-center color-red",
      colSpan: 4
    }, "*p/c - Precio por Confirmar")))));
  }))), /*#__PURE__*/react_default.a.createElement(modalConfirmDelete["a" /* default */], {
    id: idDelete,
    handleDelete: handleDelete,
    open: openModalDelete,
    handleModal: handleModalDelete,
    type: "Orden"
  }), openModalEdit && /*#__PURE__*/react_default.a.createElement(ModalEdit, {
    orden: ordenEdit,
    handleEdit: handleEdit,
    open: openModalEdit,
    handleModal: handleModalEdit
  }), /*#__PURE__*/react_default.a.createElement(verPdf, {
    img: url,
    pag: pag,
    open: openPdf,
    handleClose: handleClose
  }), /*#__PURE__*/react_default.a.createElement(dialogAlert["a" /* default */], {
    open: alert,
    setAlert: setAlert,
    type: alertData.type,
    message: alertData.message
  }));
}

Row.propTypes = {
  row: prop_types_default.a.shape({
    productos: prop_types_default.a.arrayOf(prop_types_default.a.shape({
      precio: prop_types_default.a.number.isRequired,
      id: prop_types_default.a.number.isRequired,
      created_at: prop_types_default.a.string.isRequired,
      catalogo: prop_types_default.a.string.isRequired,
      pagina: prop_types_default.a.number.isRequired,
      cantidad: prop_types_default.a.number.isRequired,
      codigo_producto: prop_types_default.a.string.isRequired
    })).isRequired
  }).isRequired
};
function CollapsibleTable(props) {
  var _useState11 = Object(react["useState"])([]),
      _useState12 = slicedToArray_default()(_useState11, 2),
      filteredData = _useState12[0],
      setFilteredData = _useState12[1];

  var _React$useState13 = react_default.a.useState(0),
      _React$useState14 = slicedToArray_default()(_React$useState13, 2),
      page = _React$useState14[0],
      setPage = _React$useState14[1];

  var _React$useState15 = react_default.a.useState(10),
      _React$useState16 = slicedToArray_default()(_React$useState15, 2),
      rowsPerPage = _React$useState16[0],
      setRowsPerPage = _React$useState16[1];

  react_default.a.useEffect(function () {
    var rows = lodash_default.a.chain(props.selectedJunior) // Group the elements of Array based on `color` property
    .groupBy("id_orden") // `key` is group's name (color), `value` is the array of objects
    .map(function (value, key) {
      return {
        idOrden: key,
        productos: value
      };
    }).value();

    setFilteredData(rows);
  }, [props.selectedJunior]);

  var handleChangePage = function handleChangePage(newPage) {
    setPage(newPage);
  };

  var handleChangeRowsPerPage = function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return /*#__PURE__*/react_default.a.createElement("div", null, props.selectedJunior.length > 0 && filteredData.length > 0 ? /*#__PURE__*/react_default.a.createElement(react_default.a.Fragment, null, /*#__PURE__*/react_default.a.createElement(TableContainer["a" /* default */], {
    component: Paper["a" /* default */]
  }, /*#__PURE__*/react_default.a.createElement(Table["a" /* default */], {
    size: "small",
    "aria-label": "collapsible table"
  }, /*#__PURE__*/react_default.a.createElement(TableHead["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: "text-center p-0",
    colSpan: 4
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    className: "p-2",
    style: {
      background: "#0d4674",
      color: "white"
    },
    component: "h2",
    variant: "h6",
    gutterBottom: true
  }, filteredData.length, " ", filteredData.length == 1 ? " Orden de " : " Ordenes de ", filteredData[0].productos[0].junior)))), /*#__PURE__*/react_default.a.createElement(TableBody["a" /* default */], null, filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(function (row) {
    return /*#__PURE__*/react_default.a.createElement(Row, {
      key: "orden" + row.idOrden,
      updateData: props.updateData,
      setSelectedJunior: props.setSelectedJunior,
      row: row,
      ordenesData: props.selectedJunior
    });
  })), /*#__PURE__*/react_default.a.createElement(TableHead["a" /* default */], null, Object(Utils["m" /* isMovil */])() ? /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: " p-0"
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    className: "p-2",
    component: "h6",
    variant: "h6"
  }, "TOTAL:", /*#__PURE__*/react_default.a.createElement("span", {
    style: {
      color: "red"
    }
  }, " ", ccyFormat(subtotalOrden(props.selectedJunior)), " ")))) : /*#__PURE__*/react_default.a.createElement(TableRow["a" /* default */], null, /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: " p-0"
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    className: "p-2",
    component: "h2",
    variant: "h6",
    gutterBottom: true
  }, "TOTAL")), /*#__PURE__*/react_default.a.createElement(TableCell["a" /* default */], {
    className: " p-0"
  }, /*#__PURE__*/react_default.a.createElement(Typography["a" /* default */], {
    className: "p-2",
    style: {
      color: "red"
    },
    component: "h2",
    variant: "h6",
    gutterBottom: true
  }, ccyFormat(subtotalOrden(props.selectedJunior)))))))), /*#__PURE__*/react_default.a.createElement(TablePagination["a" /* default */], {
    rowsPerPageOptions: [10, 25, 100],
    component: "div",
    count: filteredData.length,
    rowsPerPage: rowsPerPage,
    page: page,
    onChangePage: handleChangePage,
    onChangeRowsPerPage: handleChangeRowsPerPage
  })) : /*#__PURE__*/react_default.a.createElement(spinner["a" /* default */], null));
}

/***/ }),

/***/ 647:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalConfirmDelete; });
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(17);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1426);
/* harmony import */ var _material_ui_core_Dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1482);
/* harmony import */ var _material_ui_core_DialogActions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1486);
/* harmony import */ var _material_ui_core_DialogContent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1484);
/* harmony import */ var _material_ui_core_DialogContentText__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1485);
/* harmony import */ var _material_ui_core_DialogTitle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1483);
/* harmony import */ var _material_ui_core_Slide__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1480);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(845);
/* harmony import */ var _material_ui_icons_Backspace__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(850);
/* harmony import */ var _material_ui_icons_Backspace__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Backspace__WEBPACK_IMPORTED_MODULE_10__);











var Transition = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.forwardRef(function Transition(props, ref) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Slide__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
    direction: "up",
    in: props.open,
    ref: ref
  }, props));
});
var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"])(function (theme) {
  return {
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "25ch"
      },
      button: {
        margin: theme.spacing(1)
      },
      TextField: {
        margin: theme.spacing(1),
        width: "25ch"
      }
    }
  };
});
function ModalConfirmDelete(_ref) {
  var handleModal = _ref.handleModal,
      open = _ref.open,
      handleDelete = _ref.handleDelete,
      id = _ref.id,
      type = _ref.type;
  var classes = useStyles();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Dialog__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
    open: open,
    TransitionComponent: Transition,
    keepMounted: true,
    onClose: handleModal,
    "aria-labelledby": "alert-dialog-slide-title",
    "aria-describedby": "alert-dialog-slide-description"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_DialogTitle__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
    id: "alert-dialog-slide-title"
  }, "Eliminar ".concat(type)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_DialogContent__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_DialogContentText__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], {
    id: "alert-dialog-slide-description"
  }, "\xBFConfirma que deseas eliminar ", type, "?")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_DialogActions__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    onClick: handleModal,
    color: "primary"
  }, "Cancelar"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    variant: "contained",
    color: "primary",
    size: "small",
    onClick: function onClick() {
      return handleDelete(id);
    },
    className: classes.button,
    startIcon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_Backspace__WEBPACK_IMPORTED_MODULE_10___default.a, null)
  }, "Confirmar"))));
}

/***/ }),

/***/ 743:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

var _interopRequireWildcard = __webpack_require__(141);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var React = _interopRequireWildcard(__webpack_require__(1));

var _createSvgIcon = _interopRequireDefault(__webpack_require__(153));

var _default = (0, _createSvgIcon.default)( /*#__PURE__*/React.createElement("path", {
  d: "M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"
}), 'Edit');

exports.default = _default;

/***/ })

}]);
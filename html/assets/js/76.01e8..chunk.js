(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[76],{

/***/ 1509:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DataTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_data_grid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(461);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1451);
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(652);
/* harmony import */ var _material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1477);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(845);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(43);
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1443);








var useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"])(function (theme) {
  return {
    root: {
      width: 300
    }
  };
});

function LinearIndeterminate() {
  var classes = useStyles();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    animation: "wave"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    component: "h2",
    variant: "h5"
  }, "No tiene Juniors Registrados Actualmente"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_lab_Skeleton__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
    animation: "wave"
  }));
}

function subtotal(items) {
  return items.map(function (_ref) {
    var subtotal = _ref.subtotal;
    return subtotal;
  }).reduce(function (sum, i) {
    return sum + i;
  }, 0);
}

function ccyFormat(num) {
  return "".concat(num.toFixed(2));
}

function DataTable(props) {
  var columns = [{
    field: "junior",
    headerName: "Nombre",
    sortable: true,
    flex: 0.9
  }, {
    field: "subtotal",
    headerName: "subtotal",
    flex: 0.8,
    sortable: true,
    renderCell: function renderCell(params) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "$" + params.value);
    }
  }, {
    field: "id",
    headerName: "Ver",
    flex: 0.4,
    sortable: false,
    renderCell: function renderCell(params) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"], {
        "aria-label": "open",
        size: "small",
        onClick: function onClick() {
          return props.selectJunior(params.value);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Visibility__WEBPACK_IMPORTED_MODULE_3___default.a, {
        style: {
          color: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"][500]
        }
      }));
    }
  }];
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    className: "p-2",
    style: {
      background: "#0d4674",
      color: "white"
    },
    component: "h2",
    variant: "h6",
    gutterBottom: true
  }, "Seleccione un Junior"), props.data ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_data_grid__WEBPACK_IMPORTED_MODULE_1__[/* DataGrid */ "a"], {
    rows: props.data,
    columns: columns,
    pageSize: 5,
    autoHeight: true,
    autoPageSize: true,
    disableMultipleSelection: true,
    onSelectionChange: function onSelectionChange(newSelection) {
      props.selectJunior(newSelection.rowIds[0]);
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      display: "flex"
    },
    className: "text-left"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    className: "p-2",
    component: "h6",
    variant: "h6",
    gutterBottom: true
  }, "TOTAL"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    className: "p-2",
    component: "h6",
    variant: "h6",
    gutterBottom: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    style: {
      color: "red"
    }
  }, " ", ccyFormat(subtotal(props.data)))), " ")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinearIndeterminate, null));
}

/***/ })

}]);
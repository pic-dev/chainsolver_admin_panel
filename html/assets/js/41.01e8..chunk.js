(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[41,9],{

/***/ 148:
/***/ (function(module, exports) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(250);

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ 1533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(44);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/toConsumableArray.js
var toConsumableArray = __webpack_require__(670);
var toConsumableArray_default = /*#__PURE__*/__webpack_require__.n(toConsumableArray);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(53);
var asyncToGenerator_default = /*#__PURE__*/__webpack_require__.n(asyncToGenerator);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__(30);
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/createClass.js
var createClass = __webpack_require__(29);
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/assertThisInitialized.js
var assertThisInitialized = __webpack_require__(10);
var assertThisInitialized_default = /*#__PURE__*/__webpack_require__.n(assertThisInitialized);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/inherits.js
var inherits = __webpack_require__(31);
var inherits_default = /*#__PURE__*/__webpack_require__.n(inherits);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js
var possibleConstructorReturn = __webpack_require__(32);
var possibleConstructorReturn_default = /*#__PURE__*/__webpack_require__.n(possibleConstructorReturn);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/getPrototypeOf.js
var getPrototypeOf = __webpack_require__(23);
var getPrototypeOf_default = /*#__PURE__*/__webpack_require__.n(getPrototypeOf);

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__(24);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/classnames/index.js
var classnames = __webpack_require__(2);
var classnames_default = /*#__PURE__*/__webpack_require__.n(classnames);

// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.es.js + 6 modules
var reactstrap_es = __webpack_require__(3);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Redirect.js + 2 modules
var Redirect = __webpack_require__(138);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/Link.js
var Link = __webpack_require__(50);

// EXTERNAL MODULE: ./node_modules/react-router-dom/es/withRouter.js
var withRouter = __webpack_require__(249);

// EXTERNAL MODULE: ./src/components/CustomBootstrap/index.js
var CustomBootstrap = __webpack_require__(52);

// EXTERNAL MODULE: ./node_modules/react-bootstrap-typeahead/lib/index.js
var lib = __webpack_require__(511);

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__(79);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// EXTERNAL MODULE: ./src/util/timeLine.js
var timeLine = __webpack_require__(671);

// EXTERNAL MODULE: ./src/util/Utils.js
var Utils = __webpack_require__(16);

// EXTERNAL MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js + 1 modules
var es = __webpack_require__(339);

// EXTERNAL MODULE: ./src/util/tracking.js + 12 modules
var tracking = __webpack_require__(20);

// EXTERNAL MODULE: ./node_modules/react-beforeunload/lib/index.esm.js + 2 modules
var index_esm = __webpack_require__(773);

// CONCATENATED MODULE: ./node_modules/minisearch/dist/es5m/index.js
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

/** @ignore */
var ENTRIES = 'ENTRIES';
/** @ignore */
var KEYS = 'KEYS';
/** @ignore */
var VALUES = 'VALUES';
/** @ignore */
var LEAF = '';
/**
 * @private
 */
var TreeIterator = /** @class */ (function () {
    function TreeIterator(set, type) {
        var node = set._tree;
        var keys = Object.keys(node);
        this.set = set;
        this._type = type;
        this._path = keys.length > 0 ? [{ node: node, keys: keys }] : [];
    }
    TreeIterator.prototype.next = function () {
        var value = this.dive();
        this.backtrack();
        return value;
    };
    TreeIterator.prototype.dive = function () {
        if (this._path.length === 0) {
            return { done: true, value: undefined };
        }
        var _a = last(this._path), node = _a.node, keys = _a.keys;
        if (last(keys) === LEAF) {
            return { done: false, value: this.result() };
        }
        this._path.push({ node: node[last(keys)], keys: Object.keys(node[last(keys)]) });
        return this.dive();
    };
    TreeIterator.prototype.backtrack = function () {
        if (this._path.length === 0) {
            return;
        }
        last(this._path).keys.pop();
        if (last(this._path).keys.length > 0) {
            return;
        }
        this._path.pop();
        this.backtrack();
    };
    TreeIterator.prototype.key = function () {
        return this.set._prefix + this._path
            .map(function (_a) {
            var keys = _a.keys;
            return last(keys);
        })
            .filter(function (key) { return key !== LEAF; })
            .join('');
    };
    TreeIterator.prototype.value = function () {
        return last(this._path).node[LEAF];
    };
    TreeIterator.prototype.result = function () {
        if (this._type === VALUES) {
            return this.value();
        }
        if (this._type === KEYS) {
            return this.key();
        }
        return [this.key(), this.value()];
    };
    TreeIterator.prototype[Symbol.iterator] = function () {
        return this;
    };
    return TreeIterator;
}());
var last = function (array) {
    return array[array.length - 1];
};

var NONE = 0;
var CHANGE = 1;
var ADD = 2;
var DELETE = 3;
/**
 * @ignore
 */
var fuzzySearch = function (node, query, maxDistance) {
    var stack = [{ distance: 0, i: 0, key: '', node: node }];
    var results = {};
    var innerStack = [];
    var _loop_1 = function () {
        var _a = stack.pop(), node_1 = _a.node, distance = _a.distance, key = _a.key, i = _a.i, edit = _a.edit;
        Object.keys(node_1).forEach(function (k) {
            if (k === LEAF) {
                var totDistance = distance + (query.length - i);
                var _a = __read(results[key] || [null, Infinity], 2), d = _a[1];
                if (totDistance <= maxDistance && totDistance < d) {
                    results[key] = [node_1[k], totDistance];
                }
            }
            else {
                withinDistance(query, k, maxDistance - distance, i, edit, innerStack).forEach(function (_a) {
                    var d = _a.distance, i = _a.i, edit = _a.edit;
                    stack.push({ node: node_1[k], distance: distance + d, key: key + k, i: i, edit: edit });
                });
            }
        });
    };
    while (stack.length > 0) {
        _loop_1();
    }
    return results;
};
/**
 * @ignore
 */
var withinDistance = function (a, b, maxDistance, i, edit, stack) {
    stack.push({ distance: 0, ia: i, ib: 0, edit: edit });
    var results = [];
    while (stack.length > 0) {
        var _a = stack.pop(), distance = _a.distance, ia = _a.ia, ib = _a.ib, edit_1 = _a.edit;
        if (ib === b.length) {
            results.push({ distance: distance, i: ia, edit: edit_1 });
            continue;
        }
        if (a[ia] === b[ib]) {
            stack.push({ distance: distance, ia: ia + 1, ib: ib + 1, edit: NONE });
        }
        else {
            if (distance >= maxDistance) {
                continue;
            }
            if (edit_1 !== ADD) {
                stack.push({ distance: distance + 1, ia: ia, ib: ib + 1, edit: DELETE });
            }
            if (ia < a.length) {
                if (edit_1 !== DELETE) {
                    stack.push({ distance: distance + 1, ia: ia + 1, ib: ib, edit: ADD });
                }
                if (edit_1 !== DELETE && edit_1 !== ADD) {
                    stack.push({ distance: distance + 1, ia: ia + 1, ib: ib + 1, edit: CHANGE });
                }
            }
        }
    }
    return results;
};

/**
 * A class implementing the same interface as a standard JavaScript
 * [`Map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
 * with string keys, but adding support for efficiently searching entries with
 * prefix or fuzzy search. This class is used internally by [[MiniSearch]] as
 * the inverted index data structure. The implementation is a radix tree
 * (compressed prefix tree).
 *
 * Since this class can be of general utility beyond _MiniSearch_, it is
 * exported by the `minisearch` package and can be imported (or required) as
 * `minisearch/SearchableMap`.
 *
 * @typeParam T  The type of the values stored in the map.
 */
var SearchableMap = /** @class */ (function () {
    /**
     * The constructor is normally called without arguments, creating an empty
     * map. In order to create a [[SearchableMap]] from an iterable or from an
     * object, check [[SearchableMap.from]] and [[SearchableMap.fromObject]].
     *
     * The constructor arguments are for internal use, when creating derived
     * mutable views of a map at a prefix.
     */
    function SearchableMap(tree, prefix) {
        if (tree === void 0) { tree = {}; }
        if (prefix === void 0) { prefix = ''; }
        this._tree = tree;
        this._prefix = prefix;
    }
    /**
     * Creates and returns a mutable view of this [[SearchableMap]], containing only
     * entries that share the given prefix.
     *
     * ### Usage:
     *
     * ```javascript
     * let map = new SearchableMap()
     * map.set("unicorn", 1)
     * map.set("universe", 2)
     * map.set("university", 3)
     * map.set("unique", 4)
     * map.set("hello", 5)
     *
     * let uni = map.atPrefix("uni")
     * uni.get("unique") // => 4
     * uni.get("unicorn") // => 1
     * uni.get("hello") // => undefined
     *
     * let univer = map.atPrefix("univer")
     * univer.get("unique") // => undefined
     * univer.get("universe") // => 2
     * univer.get("university") // => 3
     * ```
     *
     * @param prefix  The prefix
     * @return A [[SearchableMap]] representing a mutable view of the original Map at the given prefix
     */
    SearchableMap.prototype.atPrefix = function (prefix) {
        var _a;
        if (!prefix.startsWith(this._prefix)) {
            throw new Error('Mismatched prefix');
        }
        var _b = __read(trackDown(this._tree, prefix.slice(this._prefix.length)), 2), node = _b[0], path = _b[1];
        if (node === undefined) {
            var _c = __read(last$1(path), 2), parentNode = _c[0], key_1 = _c[1];
            var nodeKey = Object.keys(parentNode).find(function (k) { return k !== LEAF && k.startsWith(key_1); });
            if (nodeKey !== undefined) {
                return new SearchableMap((_a = {}, _a[nodeKey.slice(key_1.length)] = parentNode[nodeKey], _a), prefix);
            }
        }
        return new SearchableMap(node || {}, prefix);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/clear
     */
    SearchableMap.prototype.clear = function () {
        delete this._size;
        this._tree = {};
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/delete
     * @param key  Key to delete
     */
    SearchableMap.prototype.delete = function (key) {
        delete this._size;
        return remove(this._tree, key);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/entries
     * @return An iterator iterating through `[key, value]` entries.
     */
    SearchableMap.prototype.entries = function () {
        return new TreeIterator(this, ENTRIES);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/forEach
     * @param fn  Iteration function
     */
    SearchableMap.prototype.forEach = function (fn) {
        var e_1, _a;
        try {
            for (var _b = __values(this), _c = _b.next(); !_c.done; _c = _b.next()) {
                var _d = __read(_c.value, 2), key = _d[0], value = _d[1];
                fn(key, value, this);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * Returns a key-value object of all the entries that have a key within the
     * given edit distance from the search key. The keys of the returned object are
     * the matching keys, while the values are two-elements arrays where the first
     * element is the value associated to the key, and the second is the edit
     * distance of the key to the search key.
     *
     * ### Usage:
     *
     * ```javascript
     * let map = new SearchableMap()
     * map.set('hello', 'world')
     * map.set('hell', 'yeah')
     * map.set('ciao', 'mondo')
     *
     * // Get all entries that match the key 'hallo' with a maximum edit distance of 2
     * map.fuzzyGet('hallo', 2)
     * // => { "hello": ["world", 1], "hell": ["yeah", 2] }
     *
     * // In the example, the "hello" key has value "world" and edit distance of 1
     * // (change "e" to "a"), the key "hell" has value "yeah" and edit distance of 2
     * // (change "e" to "a", delete "o")
     * ```
     *
     * @param key  The search key
     * @param maxEditDistance  The maximum edit distance (Levenshtein)
     * @return A key-value object of the matching keys to their value and edit distance
     */
    SearchableMap.prototype.fuzzyGet = function (key, maxEditDistance) {
        return fuzzySearch(this._tree, key, maxEditDistance);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/get
     * @param key  Key to get
     * @return Value associated to the key, or `undefined` if the key is not
     * found.
     */
    SearchableMap.prototype.get = function (key) {
        var node = lookup(this._tree, key);
        return node !== undefined ? node[LEAF] : undefined;
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/has
     * @param key  Key
     * @return True if the key is in the map, false otherwise
     */
    SearchableMap.prototype.has = function (key) {
        var node = lookup(this._tree, key);
        return node !== undefined && node.hasOwnProperty(LEAF);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/keys
     * @return An `Iterable` iterating through keys
     */
    SearchableMap.prototype.keys = function () {
        return new TreeIterator(this, KEYS);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/set
     * @param key  Key to set
     * @param value  Value to associate to the key
     * @return The [[SearchableMap]] itself, to allow chaining
     */
    SearchableMap.prototype.set = function (key, value) {
        if (typeof key !== 'string') {
            throw new Error('key must be a string');
        }
        delete this._size;
        var node = createPath(this._tree, key);
        node[LEAF] = value;
        return this;
    };
    Object.defineProperty(SearchableMap.prototype, "size", {
        /**
         * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/size
         */
        get: function () {
            var _this = this;
            if (this._size) {
                return this._size;
            }
            /** @ignore */
            this._size = 0;
            this.forEach(function () { _this._size += 1; });
            return this._size;
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Updates the value at the given key using the provided function. The function
     * is called with the current value at the key, and its return value is used as
     * the new value to be set.
     *
     * ### Example:
     *
     * ```javascript
     * // Increment the current value by one
     * searchableMap.update('somekey', (currentValue) => currentValue == null ? 0 : currentValue + 1)
     * ```
     *
     * @param key  The key to update
     * @param fn  The function used to compute the new value from the current one
     * @return The [[SearchableMap]] itself, to allow chaining
     */
    SearchableMap.prototype.update = function (key, fn) {
        if (typeof key !== 'string') {
            throw new Error('key must be a string');
        }
        delete this._size;
        var node = createPath(this._tree, key);
        node[LEAF] = fn(node[LEAF]);
        return this;
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/values
     * @return An `Iterable` iterating through values.
     */
    SearchableMap.prototype.values = function () {
        return new TreeIterator(this, VALUES);
    };
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/@@iterator
     */
    SearchableMap.prototype[Symbol.iterator] = function () {
        return this.entries();
    };
    /**
     * Creates a [[SearchableMap]] from an `Iterable` of entries
     *
     * @param entries  Entries to be inserted in the [[SearchableMap]]
     * @return A new [[SearchableMap]] with the given entries
     */
    SearchableMap.from = function (entries) {
        var e_2, _a;
        var tree = new SearchableMap();
        try {
            for (var entries_1 = __values(entries), entries_1_1 = entries_1.next(); !entries_1_1.done; entries_1_1 = entries_1.next()) {
                var _b = __read(entries_1_1.value, 2), key = _b[0], value = _b[1];
                tree.set(key, value);
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (entries_1_1 && !entries_1_1.done && (_a = entries_1.return)) _a.call(entries_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return tree;
    };
    /**
     * Creates a [[SearchableMap]] from the iterable properties of a JavaScript object
     *
     * @param object  Object of entries for the [[SearchableMap]]
     * @return A new [[SearchableMap]] with the given entries
     */
    SearchableMap.fromObject = function (object) {
        return SearchableMap.from(Object.entries(object));
    };
    return SearchableMap;
}());
var trackDown = function (tree, key, path) {
    if (path === void 0) { path = []; }
    if (key.length === 0 || tree == null) {
        return [tree, path];
    }
    var nodeKey = Object.keys(tree).find(function (k) { return k !== LEAF && key.startsWith(k); });
    if (nodeKey === undefined) {
        path.push([tree, key]); // performance: update in place
        return trackDown(undefined, '', path);
    }
    path.push([tree, nodeKey]); // performance: update in place
    return trackDown(tree[nodeKey], key.slice(nodeKey.length), path);
};
var lookup = function (tree, key) {
    if (key.length === 0 || tree == null) {
        return tree;
    }
    var nodeKey = Object.keys(tree).find(function (k) { return k !== LEAF && key.startsWith(k); });
    if (nodeKey === undefined) {
        return undefined;
    }
    return lookup(tree[nodeKey], key.slice(nodeKey.length));
};
var createPath = function (tree, key) {
    var _a;
    if (key.length === 0 || tree == null) {
        return tree;
    }
    var nodeKey = Object.keys(tree).find(function (k) { return k !== LEAF && key.startsWith(k); });
    if (nodeKey === undefined) {
        var toSplit = Object.keys(tree).find(function (k) { return k !== LEAF && k.startsWith(key[0]); });
        if (toSplit === undefined) {
            tree[key] = {};
        }
        else {
            var prefix = commonPrefix(key, toSplit);
            tree[prefix] = (_a = {}, _a[toSplit.slice(prefix.length)] = tree[toSplit], _a);
            delete tree[toSplit];
            return createPath(tree[prefix], key.slice(prefix.length));
        }
        return tree[key];
    }
    return createPath(tree[nodeKey], key.slice(nodeKey.length));
};
var commonPrefix = function (a, b, i, length, prefix) {
    if (i === void 0) { i = 0; }
    if (length === void 0) { length = Math.min(a.length, b.length); }
    if (prefix === void 0) { prefix = ''; }
    if (i >= length) {
        return prefix;
    }
    if (a[i] !== b[i]) {
        return prefix;
    }
    return commonPrefix(a, b, i + 1, length, prefix + a[i]);
};
var remove = function (tree, key) {
    var _a = __read(trackDown(tree, key), 2), node = _a[0], path = _a[1];
    if (node === undefined) {
        return;
    }
    delete node[LEAF];
    var keys = Object.keys(node);
    if (keys.length === 0) {
        cleanup(path);
    }
    if (keys.length === 1) {
        merge(path, keys[0], node[keys[0]]);
    }
};
var cleanup = function (path) {
    if (path.length === 0) {
        return;
    }
    var _a = __read(last$1(path), 2), node = _a[0], key = _a[1];
    delete node[key];
    if (Object.keys(node).length === 0) {
        cleanup(path.slice(0, -1));
    }
};
var merge = function (path, key, value) {
    if (path.length === 0) {
        return;
    }
    var _a = __read(last$1(path), 2), node = _a[0], nodeKey = _a[1];
    node[nodeKey + key] = value;
    delete node[nodeKey];
};
var last$1 = function (array) {
    return array[array.length - 1];
};

var _a;
var OR = 'or';
var AND = 'and';
/**
 * [[MiniSearch]] is the main entrypoint class, implementing a full-text search
 * engine in memory.
 *
 * @typeParam T  The type of the documents being indexed.
 *
 * ### Basic example:
 *
 * ```javascript
 * const documents = [
 *   {
 *     id: 1,
 *     title: 'Moby Dick',
 *     text: 'Call me Ishmael. Some years ago...',
 *     category: 'fiction'
 *   },
 *   {
 *     id: 2,
 *     title: 'Zen and the Art of Motorcycle Maintenance',
 *     text: 'I can see by my watch...',
 *     category: 'fiction'
 *   },
 *   {
 *     id: 3,
 *     title: 'Neuromancer',
 *     text: 'The sky above the port was...',
 *     category: 'fiction'
 *   },
 *   {
 *     id: 4,
 *     title: 'Zen and the Art of Archery',
 *     text: 'At first sight it must seem...',
 *     category: 'non-fiction'
 *   },
 *   // ...and more
 * ]
 *
 * // Create a search engine that indexes the 'title' and 'text' fields for
 * // full-text search. Search results will include 'title' and 'category' (plus the
 * // id field, that is always stored and returned)
 * const miniSearch = new MiniSearch({
 *   fields: ['title', 'text'],
 *   storeFields: ['title', 'category']
 * })
 *
 * // Add documents to the index
 * miniSearch.addAll(documents)
 *
 * // Search for documents:
 * let results = miniSearch.search('zen art motorcycle')
 * // => [
 * //   { id: 2, title: 'Zen and the Art of Motorcycle Maintenance', category: 'fiction', score: 2.77258 },
 * //   { id: 4, title: 'Zen and the Art of Archery', category: 'non-fiction', score: 1.38629 }
 * // ]
 * ```
 */
var MiniSearch = /** @class */ (function () {
    /**
     * @param options  Configuration options
     *
     * ### Examples:
     *
     * ```javascript
     * // Create a search engine that indexes the 'title' and 'text' fields of your
     * // documents:
     * const miniSearch = new MiniSearch({ fields: ['title', 'text'] })
     * ```
     *
     * ### ID Field:
     *
     * ```javascript
     * // Your documents are assumed to include a unique 'id' field, but if you want
     * // to use a different field for document identification, you can set the
     * // 'idField' option:
     * const miniSearch = new MiniSearch({ idField: 'key', fields: ['title', 'text'] })
     * ```
     *
     * ### Options and defaults:
     *
     * ```javascript
     * // The full set of options (here with their default value) is:
     * const miniSearch = new MiniSearch({
     *   // idField: field that uniquely identifies a document
     *   idField: 'id',
     *
     *   // extractField: function used to get the value of a field in a document.
     *   // By default, it assumes the document is a flat object with field names as
     *   // property keys and field values as string property values, but custom logic
     *   // can be implemented by setting this option to a custom extractor function.
     *   extractField: (document, fieldName) => document[fieldName],
     *
     *   // tokenize: function used to split fields into individual terms. By
     *   // default, it is also used to tokenize search queries, unless a specific
     *   // `tokenize` search option is supplied. When tokenizing an indexed field,
     *   // the field name is passed as the second argument.
     *   tokenize: (string, _fieldName) => string.split(SPACE_OR_PUNCTUATION),
     *
     *   // processTerm: function used to process each tokenized term before
     *   // indexing. It can be used for stemming and normalization. Return a falsy
     *   // value in order to discard a term. By default, it is also used to process
     *   // search queries, unless a specific `processTerm` option is supplied as a
     *   // search option. When processing a term from a indexed field, the field
     *   // name is passed as the second argument.
     *   processTerm: (term, _fieldName) => term.toLowerCase(),
     *
     *   // searchOptions: default search options, see the `search` method for
     *   // details
     *   searchOptions: undefined,
     *
     *   // fields: document fields to be indexed. Mandatory, but not set by default
     *   fields: undefined
     *
     *   // storeFields: document fields to be stored and returned as part of the
     *   // search results.
     *   storeFields: []
     * })
     * ```
     */
    function MiniSearch(options) {
        if ((options === null || options === void 0 ? void 0 : options.fields) == null) {
            throw new Error('MiniSearch: option "fields" must be provided');
        }
        this._options = __assign(__assign(__assign({}, defaultOptions), options), { searchOptions: __assign(__assign({}, defaultSearchOptions), (options.searchOptions || {})) });
        this._index = new SearchableMap();
        this._documentCount = 0;
        this._documentIds = {};
        this._fieldIds = {};
        this._fieldLength = {};
        this._averageFieldLength = {};
        this._nextId = 0;
        this._storedFields = {};
        this.addFields(this._options.fields);
    }
    /**
     * Adds a document to the index
     *
     * @param document  The document to be indexed
     */
    MiniSearch.prototype.add = function (document) {
        var _this = this;
        var _a = this._options, extractField = _a.extractField, tokenize = _a.tokenize, processTerm = _a.processTerm, fields = _a.fields, idField = _a.idField;
        var id = extractField(document, idField);
        if (id == null) {
            throw new Error("MiniSearch: document does not have ID field \"" + idField + "\"");
        }
        var shortDocumentId = this.addDocumentId(id);
        this.saveStoredFields(shortDocumentId, document);
        fields.forEach(function (field) {
            var fieldValue = extractField(document, field);
            if (fieldValue == null) {
                return;
            }
            var tokens = tokenize(fieldValue.toString(), field);
            _this.addFieldLength(shortDocumentId, _this._fieldIds[field], _this.documentCount - 1, tokens.length);
            tokens.forEach(function (term) {
                var processedTerm = processTerm(term, field);
                if (processedTerm) {
                    _this.addTerm(_this._fieldIds[field], shortDocumentId, processedTerm);
                }
            });
        });
    };
    /**
     * Adds all the given documents to the index
     *
     * @param documents  An array of documents to be indexed
     */
    MiniSearch.prototype.addAll = function (documents) {
        var _this = this;
        documents.forEach(function (document) { return _this.add(document); });
    };
    /**
     * Adds all the given documents to the index asynchronously.
     *
     * Returns a promise that resolves (to `undefined`) when the indexing is done.
     * This method is useful when index many documents, to avoid blocking the main
     * thread. The indexing is performed asynchronously and in chunks.
     *
     * @param documents  An array of documents to be indexed
     * @param options  Configuration options
     * @return A promise resolving to `undefined` when the indexing is done
     */
    MiniSearch.prototype.addAllAsync = function (documents, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var _a = options.chunkSize, chunkSize = _a === void 0 ? 10 : _a;
        var acc = { chunk: [], promise: Promise.resolve() };
        var _b = documents.reduce(function (_a, document, i) {
            var chunk = _a.chunk, promise = _a.promise;
            chunk.push(document);
            if ((i + 1) % chunkSize === 0) {
                return {
                    chunk: [],
                    promise: promise
                        .then(function () { return new Promise(function (resolve) { return setTimeout(resolve, 0); }); })
                        .then(function () { return _this.addAll(chunk); })
                };
            }
            else {
                return { chunk: chunk, promise: promise };
            }
        }, acc), chunk = _b.chunk, promise = _b.promise;
        return promise.then(function () { return _this.addAll(chunk); });
    };
    /**
     * Removes the given document from the index.
     *
     * The document to delete must NOT have changed between indexing and deletion,
     * otherwise the index will be corrupted. Therefore, when reindexing a document
     * after a change, the correct order of operations is:
     *
     *   1. remove old version
     *   2. apply changes
     *   3. index new version
     *
     * @param document  The document to be removed
     */
    MiniSearch.prototype.remove = function (document) {
        var _this = this;
        var _a = this._options, tokenize = _a.tokenize, processTerm = _a.processTerm, extractField = _a.extractField, fields = _a.fields, idField = _a.idField;
        var id = extractField(document, idField);
        if (id == null) {
            throw new Error("MiniSearch: document does not have ID field \"" + idField + "\"");
        }
        var _b = __read(Object.entries(this._documentIds)
            .find(function (_a) {
            var _b = __read(_a, 2), _ = _b[0], longId = _b[1];
            return id === longId;
        }) || [], 1), shortDocumentId = _b[0];
        if (shortDocumentId == null) {
            throw new Error("MiniSearch: cannot remove document with ID " + id + ": it is not in the index");
        }
        fields.forEach(function (field) {
            var fieldValue = extractField(document, field);
            if (fieldValue == null) {
                return;
            }
            var tokens = tokenize(fieldValue.toString(), field);
            tokens.forEach(function (term) {
                var processedTerm = processTerm(term, field);
                if (processedTerm) {
                    _this.removeTerm(_this._fieldIds[field], shortDocumentId, processedTerm);
                }
            });
        });
        delete this._storedFields[shortDocumentId];
        delete this._documentIds[shortDocumentId];
        this._documentCount -= 1;
    };
    /**
     * Removes all the given documents from the index. If called with no arguments,
     * it removes _all_ documents from the index.
     *
     * @param documents  The documents to be removed. If this argument is omitted,
     * all documents are removed. Note that, for removing all documents, it is
     * more efficient to call this method with no arguments than to pass all
     * documents.
     */
    MiniSearch.prototype.removeAll = function (documents) {
        var _this = this;
        if (documents) {
            documents.forEach(function (document) { return _this.remove(document); });
        }
        else if (arguments.length > 0) {
            throw new Error('Expected documents to be present. Omit the argument to remove all documents.');
        }
        else {
            this._index = new SearchableMap();
            this._documentCount = 0;
            this._documentIds = {};
            this._fieldLength = {};
            this._averageFieldLength = {};
            this._storedFields = {};
            this._nextId = 0;
        }
    };
    /**
     * Search for documents matching the given search query.
     *
     * The result is a list of scored document IDs matching the query, sorted by
     * descending score, and each including data about which terms were matched and
     * in which fields.
     *
     * ### Basic usage:
     *
     * ```javascript
     * // Search for "zen art motorcycle" with default options: terms have to match
     * // exactly, and individual terms are joined with OR
     * miniSearch.search('zen art motorcycle')
     * // => [ { id: 2, score: 2.77258, match: { ... } }, { id: 4, score: 1.38629, match: { ... } } ]
     * ```
     *
     * ### Restrict search to specific fields:
     *
     * ```javascript
     * // Search only in the 'title' field
     * miniSearch.search('zen', { fields: ['title'] })
     * ```
     *
     * ### Field boosting:
     *
     * ```javascript
     * // Boost a field
     * miniSearch.search('zen', { boost: { title: 2 } })
     * ```
     *
     * ### Prefix search:
     *
     * ```javascript
     * // Search for "moto" with prefix search (it will match documents
     * // containing terms that start with "moto" or "neuro")
     * miniSearch.search('moto neuro', { prefix: true })
     * ```
     *
     * ### Fuzzy search:
     *
     * ```javascript
     * // Search for "ismael" with fuzzy search (it will match documents containing
     * // terms similar to "ismael", with a maximum edit distance of 0.2 term.length
     * // (rounded to nearest integer)
     * miniSearch.search('ismael', { fuzzy: 0.2 })
     * ```
     *
     * ### Combining strategies:
     *
     * ```javascript
     * // Mix of exact match, prefix search, and fuzzy search
     * miniSearch.search('ismael mob', {
     *  prefix: true,
     *  fuzzy: 0.2
     * })
     * ```
     *
     * ### Advanced prefix and fuzzy search:
     *
     * ```javascript
     * // Perform fuzzy and prefix search depending on the search term. Here
     * // performing prefix and fuzzy search only on terms longer than 3 characters
     * miniSearch.search('ismael mob', {
     *  prefix: term => term.length > 3
     *  fuzzy: term => term.length > 3 ? 0.2 : null
     * })
     * ```
     *
     * ### Combine with AND:
     *
     * ```javascript
     * // Combine search terms with AND (to match only documents that contain both
     * // "motorcycle" and "art")
     * miniSearch.search('motorcycle art', { combineWith: 'AND' })
     * ```
     *
     * ### Filtering results:
     *
     * ```javascript
     * // Filter only results in the 'fiction' category (assuming that 'category'
     * // is a stored field)
     * miniSearch.search('motorcycle art', {
     *   filter: (result) => result.category === 'fiction'
     * })
     * ```
     *
     * @param queryString  Query string to search for
     * @param options  Search options. Each option, if not given, defaults to the corresponding value of `searchOptions` given to the constructor, or to the library default.
     */
    MiniSearch.prototype.search = function (queryString, searchOptions) {
        var _this = this;
        if (searchOptions === void 0) { searchOptions = {}; }
        var _a = this._options, tokenize = _a.tokenize, processTerm = _a.processTerm, globalSearchOptions = _a.searchOptions;
        var options = __assign(__assign({ tokenize: tokenize, processTerm: processTerm }, globalSearchOptions), searchOptions);
        var searchTokenize = options.tokenize, searchProcessTerm = options.processTerm;
        var terms = searchTokenize(queryString)
            .map(function (term) { return searchProcessTerm(term); })
            .filter(function (term) { return !!term; });
        var queries = terms.map(termToQuery(options));
        var results = queries.map(function (query) { return _this.executeQuery(query, options); });
        var combinedResults = this.combineResults(results, options.combineWith);
        return Object.entries(combinedResults)
            .reduce(function (results, _a) {
            var _b = __read(_a, 2), docId = _b[0], _c = _b[1], score = _c.score, match = _c.match, terms = _c.terms;
            var result = {
                id: _this._documentIds[docId],
                terms: uniq(terms),
                score: score,
                match: match
            };
            Object.assign(result, _this._storedFields[docId]);
            if (options.filter == null || options.filter(result)) {
                results.push(result);
            }
            return results;
        }, [])
            .sort(function (_a, _b) {
            var a = _a.score;
            var b = _b.score;
            return a < b ? 1 : -1;
        });
    };
    /**
     * Provide suggestions for the given search query
     *
     * The result is a list of suggested modified search queries, derived from the
     * given search query, each with a relevance score, sorted by descending score.
     *
     * ### Basic usage:
     *
     * ```javascript
     * // Get suggestions for 'neuro':
     * miniSearch.autoSuggest('neuro')
     * // => [ { suggestion: 'neuromancer', terms: [ 'neuromancer' ], score: 0.46240 } ]
     * ```
     *
     * ### Multiple words:
     *
     * ```javascript
     * // Get suggestions for 'zen ar':
     * miniSearch.autoSuggest('zen ar')
     * // => [
     * //  { suggestion: 'zen archery art', terms: [ 'zen', 'archery', 'art' ], score: 1.73332 },
     * //  { suggestion: 'zen art', terms: [ 'zen', 'art' ], score: 1.21313 }
     * // ]
     * ```
     *
     * ### Fuzzy suggestions:
     *
     * ```javascript
     * // Correct spelling mistakes using fuzzy search:
     * miniSearch.autoSuggest('neromancer', { fuzzy: 0.2 })
     * // => [ { suggestion: 'neuromancer', terms: [ 'neuromancer' ], score: 1.03998 } ]
     * ```
     *
     * ### Filtering:
     *
     * ```javascript
     * // Get suggestions for 'zen ar', but only within the 'fiction' category
     * // (assuming that 'category' is a stored field):
     * miniSearch.autoSuggest('zen ar', {
     *   filter: (result) => result.category === 'fiction'
     * })
     * // => [
     * //  { suggestion: 'zen archery art', terms: [ 'zen', 'archery', 'art' ], score: 1.73332 },
     * //  { suggestion: 'zen art', terms: [ 'zen', 'art' ], score: 1.21313 }
     * // ]
     * ```
     *
     * @param queryString  Query string to be expanded into suggestions
     * @param options  Search options. The supported options and default values
     * are the same as for the `search` method, except that by default prefix
     * search is performed on the last term in the query.
     * @return  A sorted array of suggestions sorted by relevance score.
     */
    MiniSearch.prototype.autoSuggest = function (queryString, options) {
        if (options === void 0) { options = {}; }
        options = __assign(__assign({}, defaultAutoSuggestOptions), options);
        var suggestions = this.search(queryString, options).reduce(function (suggestions, _a) {
            var score = _a.score, terms = _a.terms;
            var phrase = terms.join(' ');
            if (suggestions[phrase] == null) {
                suggestions[phrase] = { score: score, terms: terms, count: 1 };
            }
            else {
                suggestions[phrase].score += score;
                suggestions[phrase].count += 1;
            }
            return suggestions;
        }, {});
        return Object.entries(suggestions)
            .map(function (_a) {
            var _b = __read(_a, 2), suggestion = _b[0], _c = _b[1], score = _c.score, terms = _c.terms, count = _c.count;
            return ({ suggestion: suggestion, terms: terms, score: score / count });
        })
            .sort(function (_a, _b) {
            var a = _a.score;
            var b = _b.score;
            return a < b ? 1 : -1;
        });
    };
    Object.defineProperty(MiniSearch.prototype, "documentCount", {
        /**
         * Number of documents in the index
         */
        get: function () {
            return this._documentCount;
        },
        enumerable: false,
        configurable: true
    });
    /**
     * Deserializes a JSON index (serialized with `miniSearch.toJSON()`) and
     * instantiates a MiniSearch instance. It should be given the same options
     * originally used when serializing the index.
     *
     * ### Usage:
     *
     * ```javascript
     * // If the index was serialized with:
     * let miniSearch = new MiniSearch({ fields: ['title', 'text'] })
     * miniSearch.addAll(documents)
     *
     * const json = JSON.stringify(miniSearch)
     * // It can later be deserialized like this:
     * miniSearch = MiniSearch.loadJSON(json, { fields: ['title', 'text'] })
     * ```
     *
     * @param json  JSON-serialized index
     * @param options  configuration options, same as the constructor
     * @return An instance of MiniSearch deserialized from the given JSON.
     */
    MiniSearch.loadJSON = function (json, options) {
        if (options == null) {
            throw new Error('MiniSearch: loadJSON should be given the same options used when serializing the index');
        }
        return MiniSearch.loadJS(JSON.parse(json), options);
    };
    /**
     * Returns the default value of an option. It will throw an error if no option
     * with the given name exists.
     *
     * @param optionName  Name of the option
     * @return The default value of the given option
     *
     * ### Usage:
     *
     * ```javascript
     * // Get default tokenizer
     * MiniSearch.getDefault('tokenize')
     *
     * // Get default term processor
     * MiniSearch.getDefault('processTerm')
     *
     * // Unknown options will throw an error
     * MiniSearch.getDefault('notExisting')
     * // => throws 'MiniSearch: unknown option "notExisting"'
     * ```
     */
    MiniSearch.getDefault = function (optionName) {
        if (defaultOptions.hasOwnProperty(optionName)) {
            return getOwnProperty(defaultOptions, optionName);
        }
        else {
            throw new Error("MiniSearch: unknown option \"" + optionName + "\"");
        }
    };
    /**
     * @ignore
     */
    MiniSearch.loadJS = function (js, options) {
        var index = js.index, documentCount = js.documentCount, nextId = js.nextId, documentIds = js.documentIds, fieldIds = js.fieldIds, fieldLength = js.fieldLength, averageFieldLength = js.averageFieldLength, storedFields = js.storedFields;
        var miniSearch = new MiniSearch(options);
        miniSearch._index = new SearchableMap(index._tree, index._prefix);
        miniSearch._documentCount = documentCount;
        miniSearch._nextId = nextId;
        miniSearch._documentIds = documentIds;
        miniSearch._fieldIds = fieldIds;
        miniSearch._fieldLength = fieldLength;
        miniSearch._averageFieldLength = averageFieldLength;
        miniSearch._fieldIds = fieldIds;
        miniSearch._storedFields = storedFields || {};
        return miniSearch;
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.executeQuery = function (query, searchOptions) {
        var _this = this;
        var options = __assign(__assign({}, this._options.searchOptions), searchOptions);
        var boosts = (options.fields || this._options.fields).reduce(function (boosts, field) {
            var _a;
            return (__assign(__assign({}, boosts), (_a = {}, _a[field] = getOwnProperty(boosts, field) || 1, _a)));
        }, options.boost || {});
        var boostDocument = options.boostDocument, weights = options.weights;
        var _a = __assign(__assign({}, defaultSearchOptions.weights), weights), fuzzyWeight = _a.fuzzy, prefixWeight = _a.prefix;
        var exactMatch = this.termResults(query.term, boosts, boostDocument, this._index.get(query.term));
        if (!query.fuzzy && !query.prefix) {
            return exactMatch;
        }
        var results = [exactMatch];
        if (query.prefix) {
            this._index.atPrefix(query.term).forEach(function (term, data) {
                var weightedDistance = (0.3 * (term.length - query.term.length)) / term.length;
                results.push(_this.termResults(term, boosts, boostDocument, data, prefixWeight, weightedDistance));
            });
        }
        if (query.fuzzy) {
            var fuzzy = (query.fuzzy === true) ? 0.2 : query.fuzzy;
            var maxDistance = fuzzy < 1 ? Math.round(query.term.length * fuzzy) : fuzzy;
            Object.entries(this._index.fuzzyGet(query.term, maxDistance)).forEach(function (_a) {
                var _b = __read(_a, 2), term = _b[0], _c = __read(_b[1], 2), data = _c[0], distance = _c[1];
                var weightedDistance = distance / term.length;
                results.push(_this.termResults(term, boosts, boostDocument, data, fuzzyWeight, weightedDistance));
            });
        }
        return results.reduce(combinators[OR], {});
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.combineResults = function (results, combineWith) {
        if (combineWith === void 0) { combineWith = OR; }
        if (results.length === 0) {
            return {};
        }
        var operator = combineWith.toLowerCase();
        return results.reduce(combinators[operator], null) || {};
    };
    /**
     * Allows serialization of the index to JSON, to possibly store it and later
     * deserialize it with `MiniSearch.loadJSON`.
     *
     * Normally one does not directly call this method, but rather call the
     * standard JavaScript `JSON.stringify()` passing the `MiniSearch` instance,
     * and JavaScript will internally call this method. Upon deserialization, one
     * must pass to `loadJSON` the same options used to create the original
     * instance that was serialized.
     *
     * ### Usage:
     *
     * ```javascript
     * // Serialize the index:
     * let miniSearch = new MiniSearch({ fields: ['title', 'text'] })
     * miniSearch.addAll(documents)
     * const json = JSON.stringify(miniSearch)
     *
     * // Later, to deserialize it:
     * miniSearch = MiniSearch.loadJSON(json, { fields: ['title', 'text'] })
     * ```
     *
     * @return A plain-object serializeable representation of the search index.
     */
    MiniSearch.prototype.toJSON = function () {
        return {
            index: this._index,
            documentCount: this._documentCount,
            nextId: this._nextId,
            documentIds: this._documentIds,
            fieldIds: this._fieldIds,
            fieldLength: this._fieldLength,
            averageFieldLength: this._averageFieldLength,
            storedFields: this._storedFields
        };
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.termResults = function (term, boosts, boostDocument, indexData, weight, editDistance) {
        var _this = this;
        if (editDistance === void 0) { editDistance = 0; }
        if (indexData == null) {
            return {};
        }
        return Object.entries(boosts).reduce(function (results, _a) {
            var _b = __read(_a, 2), field = _b[0], boost = _b[1];
            var fieldId = _this._fieldIds[field];
            var _c = indexData[fieldId] || { ds: {} }, df = _c.df, ds = _c.ds;
            Object.entries(ds).forEach(function (_a) {
                var _b = __read(_a, 2), documentId = _b[0], tf = _b[1];
                var docBoost = boostDocument ? boostDocument(_this._documentIds[documentId], term) : 1;
                if (!docBoost) {
                    return;
                }
                var normalizedLength = _this._fieldLength[documentId][fieldId] / _this._averageFieldLength[fieldId];
                results[documentId] = results[documentId] || { score: 0, match: {}, terms: [] };
                results[documentId].terms.push(term);
                results[documentId].match[term] = getOwnProperty(results[documentId].match, term) || [];
                results[documentId].score += docBoost * score(tf, df, _this._documentCount, normalizedLength, boost, editDistance);
                results[documentId].match[term].push(field);
            });
            return results;
        }, {});
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.addTerm = function (fieldId, documentId, term) {
        this._index.update(term, function (indexData) {
            var _a;
            indexData = indexData || {};
            var fieldIndex = indexData[fieldId] || { df: 0, ds: {} };
            if (fieldIndex.ds[documentId] == null) {
                fieldIndex.df += 1;
            }
            fieldIndex.ds[documentId] = (fieldIndex.ds[documentId] || 0) + 1;
            return __assign(__assign({}, indexData), (_a = {}, _a[fieldId] = fieldIndex, _a));
        });
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.removeTerm = function (fieldId, documentId, term) {
        var _this = this;
        if (!this._index.has(term)) {
            this.warnDocumentChanged(documentId, fieldId, term);
            return;
        }
        this._index.update(term, function (indexData) {
            var _a;
            var fieldIndex = indexData[fieldId];
            if (fieldIndex == null || fieldIndex.ds[documentId] == null) {
                _this.warnDocumentChanged(documentId, fieldId, term);
                return indexData;
            }
            if (fieldIndex.ds[documentId] <= 1) {
                if (fieldIndex.df <= 1) {
                    delete indexData[fieldId];
                    return indexData;
                }
                fieldIndex.df -= 1;
            }
            if (fieldIndex.ds[documentId] <= 1) {
                delete fieldIndex.ds[documentId];
                return indexData;
            }
            fieldIndex.ds[documentId] -= 1;
            return __assign(__assign({}, indexData), (_a = {}, _a[fieldId] = fieldIndex, _a));
        });
        if (Object.keys(this._index.get(term)).length === 0) {
            this._index.delete(term);
        }
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.warnDocumentChanged = function (shortDocumentId, fieldId, term) {
        if (console == null || console.warn == null) {
            return;
        }
        var fieldName = Object.entries(this._fieldIds).find(function (_a) {
            var _b = __read(_a, 2), name = _b[0], id = _b[1];
            return id === fieldId;
        })[0];
        console.warn("MiniSearch: document with ID " + this._documentIds[shortDocumentId] + " has changed before removal: term \"" + term + "\" was not present in field \"" + fieldName + "\". Removing a document after it has changed can corrupt the index!");
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.addDocumentId = function (documentId) {
        var shortDocumentId = this._nextId.toString(36);
        this._documentIds[shortDocumentId] = documentId;
        this._documentCount += 1;
        this._nextId += 1;
        return shortDocumentId;
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.addFields = function (fields) {
        var _this = this;
        fields.forEach(function (field, i) { _this._fieldIds[field] = i; });
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.addFieldLength = function (documentId, fieldId, count, length) {
        this._averageFieldLength[fieldId] = this._averageFieldLength[fieldId] || 0;
        var totalLength = (this._averageFieldLength[fieldId] * count) + length;
        this._fieldLength[documentId] = this._fieldLength[documentId] || {};
        this._fieldLength[documentId][fieldId] = length;
        this._averageFieldLength[fieldId] = totalLength / (count + 1);
    };
    /**
     * @ignore
     */
    MiniSearch.prototype.saveStoredFields = function (documentId, doc) {
        var _this = this;
        var _a = this._options, storeFields = _a.storeFields, extractField = _a.extractField;
        if (storeFields == null || storeFields.length === 0) {
            return;
        }
        this._storedFields[documentId] = this._storedFields[documentId] || {};
        storeFields.forEach(function (fieldName) {
            var fieldValue = extractField(doc, fieldName);
            if (fieldValue === undefined) {
                return;
            }
            _this._storedFields[documentId][fieldName] = fieldValue;
        });
    };
    return MiniSearch;
}());
var getOwnProperty = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property) ? object[property] : undefined;
};
var combinators = (_a = {},
    _a[OR] = function (a, b) {
        return Object.entries(b).reduce(function (combined, _a) {
            var _b;
            var _c = __read(_a, 2), documentId = _c[0], _d = _c[1], score = _d.score, match = _d.match, terms = _d.terms;
            if (combined[documentId] == null) {
                combined[documentId] = { score: score, match: match, terms: terms };
            }
            else {
                combined[documentId].score += score;
                combined[documentId].score *= 1.5;
                (_b = combined[documentId].terms).push.apply(_b, __spread(terms));
                Object.assign(combined[documentId].match, match);
            }
            return combined;
        }, a || {});
    },
    _a[AND] = function (a, b) {
        if (a == null) {
            return b;
        }
        return Object.entries(b).reduce(function (combined, _a) {
            var _b = __read(_a, 2), documentId = _b[0], _c = _b[1], score = _c.score, match = _c.match, terms = _c.terms;
            if (a[documentId] === undefined) {
                return combined;
            }
            combined[documentId] = combined[documentId] || {};
            combined[documentId].score = a[documentId].score + score;
            combined[documentId].match = __assign(__assign({}, a[documentId].match), match);
            combined[documentId].terms = __spread(a[documentId].terms, terms);
            return combined;
        }, {});
    },
    _a);
var tfIdf = function (tf, df, n) { return tf * Math.log(n / df); };
var score = function (termFrequency, documentFrequency, documentCount, normalizedLength, boost, editDistance) {
    var weight = boost / (1 + (0.333 * boost * editDistance));
    return weight * tfIdf(termFrequency, documentFrequency, documentCount) / normalizedLength;
};
var termToQuery = function (options) { return function (term, i, terms) {
    var fuzzy = (typeof options.fuzzy === 'function')
        ? options.fuzzy(term, i, terms)
        : (options.fuzzy || false);
    var prefix = (typeof options.prefix === 'function')
        ? options.prefix(term, i, terms)
        : (options.prefix === true);
    return { term: term, fuzzy: fuzzy, prefix: prefix };
}; };
var uniq = function (array) {
    return array.filter(function (element, i, array) { return array.indexOf(element) === i; });
};
var defaultOptions = {
    idField: 'id',
    extractField: function (document, fieldName) { return document[fieldName]; },
    tokenize: function (text, fieldName) { return text.split(SPACE_OR_PUNCTUATION); },
    processTerm: function (term, fieldName) { return term.toLowerCase(); },
    fields: undefined,
    searchOptions: undefined,
    storeFields: []
};
var defaultSearchOptions = {
    combineWith: OR,
    prefix: false,
    fuzzy: false,
    boost: {},
    weights: { fuzzy: 0.9, prefix: 0.75 }
};
var defaultAutoSuggestOptions = {
    prefix: function (term, i, terms) {
        return i === terms.length - 1;
    }
};
// This regular expression matches any Unicode space or punctuation character
// Adapted from https://unicode.org/cldr/utility/list-unicodeset.jsp?a=%5Cp%7BZ%7D%5Cp%7BP%7D&abb=on&c=on&esc=on
var SPACE_OR_PUNCTUATION = /[\n\r -#%-*,-/:;?@[-\]_{}\u00A0\u00A1\u00A7\u00AB\u00B6\u00B7\u00BB\u00BF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0A76\u0AF0\u0C77\u0C84\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166E\u1680\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2000-\u200A\u2010-\u2029\u202F-\u2043\u2045-\u2051\u2053-\u205F\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E4F\u3000-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]+/u;

/* harmony default export */ var es5m = (MiniSearch);
//# sourceMappingURL=index.js.map

// EXTERNAL MODULE: ./src/util/helmet.js
var helmet = __webpack_require__(182);

// EXTERNAL MODULE: ./src/util/chat.js
var chat = __webpack_require__(341);

// CONCATENATED MODULE: ./src/routes/pages/calculadoraFletes/Ruta.js











function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = getPrototypeOf_default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = getPrototypeOf_default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return possibleConstructorReturn_default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
















var Calculadora = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(11), __webpack_require__.e(68)]).then(__webpack_require__.bind(null, 1535));
});
var ModalSignInRuta = /*#__PURE__*/react_default.a.lazy(function () {
  return Promise.all(/* import() */[__webpack_require__.e(19), __webpack_require__.e(85)]).then(__webpack_require__.bind(null, 1505));
});

var Ruta_renderLoader = function renderLoader() {
  return /*#__PURE__*/react_default.a.createElement("div", {
    className: "loading"
  });
};

var timer;
var validated;
var valid;

var Ruta_ruta = /*#__PURE__*/function (_Component) {
  inherits_default()(ruta, _Component);

  var _super = _createSuper(ruta);

  function ruta(props) {
    var _this;

    classCallCheck_default()(this, ruta);

    _this = _super.call(this, props);

    defineProperty_default()(assertThisInitialized_default()(_this), "EventDestino", function (tipo, seleccionado) {
      _this.setState({
        resultContainer: false,
        showResult: []
      });

      var value = seleccionado.filter(function (item) {
        if (item.puerto == "PERÚ - OTRAS PROVINCIAS") {
          return item.id;
        }
      });

      switch (tipo) {
        case 1:
          //Event("3.4 Puerto Destino Aereo", "3.4 Puerto Destino Aereo", "RUTA");
          break;

        case 2:
          if (localStorage.tipocon == "Contenedor compartido") {//Event("1.4 Puerto Destino LCL", "1.4 Puerto Destino LCL", "RUTA");
          } else {//Event("2.3 Puerto Destino FCL", "2.3 Puerto Destino FCL", "RUTA");
            }

          break;

        default: // code block

      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "EventOrigen", function (e) {
      _this.setState({
        resultContainer: false,
        showResult: []
      });

      switch (e) {
        case 1:
          //Event("3.3 Puerto Origen Aereo", "3.3 Puerto Origen Aereo", "RUTA");
          break;

        case 2:
          if (localStorage.tipocon == "Contenedor compartido") {//Event("1.3 Puerto Origen LCL", "1.3 Puerto Origen LCL", "RUTA");
          } else {//Event("2.2 Puerto Origen FCL", "2.2 Puerto Origen FCL", "RUTA");
            }

          break;

        default: // code block

      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "EventGuardar", function (e) {
      switch (e) {
        case 1:
          /*Event(
            "3.3.1 guardar elección de mercancia aereo",
            "3.3.1 guardar elección de mercancia aereo",
            "RUTA"
          );*/
          break;

        case 2:
          /*Event(
            "3.3.1 guardar elección de mercancia LCL",
            "3.3.1 guardar elección de mercancia LCL",
            "RUTA"
          );*/
          break;

        default: // code block

      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "eventOpcion1", function () {
      switch (localStorage.tipocon) {
        case "Contenedor completo":
          /*Event(
            "2.7.2 Calcular Opcion 1 FCL",
            "2.7.2 Calcular Opcion 1 FCL",
            "RUTA"
          );*/
          break;

        case "Contenedor compartido":
          /*Event(
            "1.8.2 Calcular Opcion 1 LCL",
            "1.8.2 Calcular Opcion 1 LCL",
            "RUTA"
          );*/
          break;

        case "Aereo":
          /*Event(
            "3.8.2 Calcular Opcion 1 AEREO",
            "3.8.2 Calcular Opcion 1 AEREO",
            "RUTA"
          );*/
          break;

        default:
      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "eventAgregarToque", function () {
      switch (localStorage.tipocon) {
        case "Contenedor completo":
          /*Event(
            "2.4.1 Agregar Servicios FCL toque",
            "2.4.1 Agregar Servicios FCL toque",
            "RUTA"
          );*/
          break;

        case "Contenedor compartido":
          /*Event(
            "1.5.1 Agregar Servicios LCL toque",
            "1.5.1 Agregar Servicios LCL toque",
            "RUTA"
          );*/
          break;

        case "Aereo":
          /*Event(
            "3.5.1 Agregar Servicios Aereo toque",
            "3.5.1 Agregar Servicios Aereo toque",
            "RUTA"
          );*/
          break;

        default:
      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "Destiny", function (origen) {
      var origen1 = origen.map(function (item) {
        return item.id;
      });
      var puertos = [257, 376, 347, 372, 389, 350, 344, 300, 390, 301, 302, 355, 343, 391, 350, 536, 537, 538, 539, 540];

      if (origen.length > 0) {
        var value = puertos.includes(origen1[0]);
        localStorage.UsaDestiny = value;
        var datos;
        var word = origen1[0].toString();
        var url;

        if (localStorage.tipocon == "Contenedor compartido") {
          datos = {
            word: word,
            port: "MARITIMO LCL"
          };
          url = _this.props.api + "maritimo_v2/find_port_to_mar";
        } else if (localStorage.tipocon == "Contenedor completo") {
          datos = {
            word: word,
            port: "MARITIMO FCL"
          };
          url = _this.props.api + "maritimo_v2/find_port_to_mar";
        } else {
          datos = {
            word: word
          };
          url = _this.props.api + "aereo_v2/find_port_to_aereo";
        }

        asyncToGenerator_default()( /*#__PURE__*/regenerator_default.a.mark(function _callee() {
          return regenerator_default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.t0 = axios_default.a;
                  _context.t1 = url;
                  _context.next = 4;
                  return datos;

                case 4:
                  _context.t2 = _context.sent;

                  _context.t0.post.call(_context.t0, _context.t1, _context.t2).then(function (res) {
                    var response;
                    var data = res.data.data.r_dat;
                    var value = data.filter(function (item) {
                      if (item.id == 100 || item.id == 251 || item.id == 598 || item.id == 646 || item.id == 663) {
                        return item.id;
                      }
                    });
                    var valueLCL = data.filter(function (item) {
                      if (item.id == 525) {
                        return item.id;
                      }
                    });

                    if (value.length > 0 && localStorage.tipocon == "Contenedor completo") {
                      response = [].concat(toConsumableArray_default()(data), [{
                        id: 100,
                        puerto: "PERÚ - OTRAS PROVINCIAS"
                      }]);
                    } else if (localStorage.tipocon == "Contenedor compartido") {
                      if (value.length > 0 && valueLCL.length > 0) {
                        response = [{
                          id: 525,
                          puerto: "PANAMÁ - PANAMÁ CITY"
                        }, {
                          id: 525,
                          puerto: "PANAMÁ - OTRAS PROVINCIAS"
                        }].concat(toConsumableArray_default()(data), [{
                          id: 100,
                          puerto: "PERÚ - OTRAS PROVINCIAS"
                        }]);
                      } else if (value.length > 0) {
                        response = [].concat(toConsumableArray_default()(data), [{
                          id: 100,
                          puerto: "PERÚ - OTRAS PROVINCIAS"
                        }]);
                      } else if (valueLCL.length > 0) {
                        response = [].concat(toConsumableArray_default()(data), [{
                          id: 525,
                          puerto: "PANAMÁ - PANAMÁ CITY"
                        }, {
                          id: 525,
                          puerto: "PANAMÁ - OTRAS PROVINCIAS"
                        }]);
                      } else {
                        response = data;
                      }
                    } else if (value.length > 0 && localStorage.transporte == "AÉREO") {
                      response = [].concat(toConsumableArray_default()(data), [{
                        id: 251,
                        puerto: "PERÚ - OTRAS PROVINCIAS"
                      }]);
                    } else {
                      response = data;
                    }

                    _this.setState({
                      portDestiny: response
                    });
                  }).catch(function (error) {});

                case 6:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }))();
      } else {
        _this.setState({
          portDestiny: [],
          portDestinySelected: []
        });
      }
    });

    defineProperty_default()(assertThisInitialized_default()(_this), "checkOrigen", function () {
      if (_this.state.portOriginSelected.length > 0) {} else {
        var instance = _this._typeahead.getInstance();

        instance.clear();
        instance.focus();
        alert("Debe completar el puerto origen con una opción válida.");
      }
    });

    _this.toggleModalPreview = _this.toggleModalPreview.bind(assertThisInitialized_default()(_this));
    _this.toggleCalculator = _this.toggleCalculator.bind(assertThisInitialized_default()(_this));
    _this.toggle2 = _this.toggle2.bind(assertThisInitialized_default()(_this));
    _this.toggle = _this.toggle.bind(assertThisInitialized_default()(_this));
    _this.minusCount = _this.minusCount.bind(assertThisInitialized_default()(_this));
    _this.plusCount = _this.plusCount.bind(assertThisInitialized_default()(_this));
    _this.tabs = _this.tabs.bind(assertThisInitialized_default()(_this));
    _this.handleFormState = _this.handleFormState.bind(assertThisInitialized_default()(_this));
    _this.portOrigin = _this.portOrigin.bind(assertThisInitialized_default()(_this));
    _this.portDestiny = _this.portDestiny.bind(assertThisInitialized_default()(_this));
    _this.get_ctz_fcl = _this.get_ctz_fcl.bind(assertThisInitialized_default()(_this));
    _this.get_ctz_lcl = _this.get_ctz_lcl.bind(assertThisInitialized_default()(_this));
    _this.get_ctz_aereo = _this.get_ctz_aereo.bind(assertThisInitialized_default()(_this));
    _this.get_ctz = _this.get_ctz.bind(assertThisInitialized_default()(_this));
    _this.showResult = _this.showResult.bind(assertThisInitialized_default()(_this));
    _this.changePlaceholder = _this.changePlaceholder.bind(assertThisInitialized_default()(_this));
    _this.array_js_to_pg = _this.array_js_to_pg.bind(assertThisInitialized_default()(_this));
    _this.contenedores = _this.contenedores.bind(assertThisInitialized_default()(_this));
    _this.validation = _this.validation.bind(assertThisInitialized_default()(_this));
    _this.setValues = _this.setValues.bind(assertThisInitialized_default()(_this));
    _this.setvolume = _this.setvolume.bind(assertThisInitialized_default()(_this));
    _this.scrollTo = _this.scrollTo.bind(assertThisInitialized_default()(_this));
    _this.handleBoxToggle = _this.handleBoxToggle.bind(assertThisInitialized_default()(_this));
    _this.Destiny = _this.Destiny.bind(assertThisInitialized_default()(_this));
    _this.checkOrigen = _this.checkOrigen.bind(assertThisInitialized_default()(_this));
    _this.EventOrigen = _this.EventOrigen.bind(assertThisInitialized_default()(_this));
    _this.EventDestino = _this.EventDestino.bind(assertThisInitialized_default()(_this));
    _this.redirect = _this.redirect.bind(assertThisInitialized_default()(_this));
    _this.ResetCont = _this.ResetCont.bind(assertThisInitialized_default()(_this));
    _this.toggleAlertOpcion = _this.toggleAlertOpcion.bind(assertThisInitialized_default()(_this));
    _this.fuzzy = _this.fuzzy.bind(assertThisInitialized_default()(_this));
    _this.state = {
      fuzzyOptions: [],
      msjActivo: false,
      isOpenAlertOpcion: false,
      OpcionValue: "",
      banderatipoLCL: false,
      count1: JSON.parse(localStorage.getItem("count1")) + 0,
      count2: JSON.parse(localStorage.getItem("count2")) + 0,
      count3: JSON.parse(localStorage.getItem("count3")) + 0,
      count4: JSON.parse(localStorage.getItem("count4")) + 0,
      setValuesMsj: false,
      placeholder: "Elija una opcion",
      placeholder2: "Cargue Contenido a Enviar",
      Calculator: false,
      dropdownOpen: false,
      dropdownOpen2: false,
      showLoading: false,
      toggleAlert: false,
      resultContainer: false,
      hide: false,
      hide2: false,
      activeTab: "false",
      portOriginSelected: [],
      portDestinySelected: [],
      portOrigin: [],
      portDestiny: [],
      productPerm: [],
      result: [],
      showResult: [],
      validated1: false,
      validated2: false,
      validated3: false,
      showBox1: false,
      showBox2: false,
      showBox3: false,
      showBox4: false,
      showBoxmb1: false,
      showBoxmb2: false,
      showBoxmb3: false,
      showBoxmb4: false,
      showCalmsj: false,
      IsVisiblePreview: false,
      isLoading: false,
      form: [{
        Label: "lumps",
        value: "1"
      }, {
        Label: "weight",
        value: "1"
      }, {
        Label: "volume",
        value: ""
      }, {
        Label: "weightUnit",
        value: "2"
      }, {
        Label: "volumeUnit",
        value: "1"
      }, {
        Label: "weightVolume",
        value: ""
      }]
    };
    return _this;
  }

  createClass_default()(ruta, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      localStorage.removeItem("token");

      if (true) {
        Object(tracking["b" /* PageView */])();
      }

      valid = "2";
      localStorage.page = "2";

      if (localStorage.transporte == "AÉREO") {
        var url1 = this.props.api + "aereo_v2/get_find_port_aereo_begin";
        var datos1 = {
          word: ""
        };
        localStorage.tipocon = "Aereo";
        /*Event("3.1 Tipo Envio Aereo", "3.1 Tipo Envio Aereo", "RUTA"); */

        this.apiPost(url1, datos1, this.portOrigin);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearTimeout(timer);
    }
  }, {
    key: "redirect",
    value: function redirect() {
      this.props.history.push("/Presupuesto");
    }
  }, {
    key: "handleBoxToggle",
    value: function handleBoxToggle(e, value) {
      switch (value) {
        case 0:
          this.setState({
            showBox1: e
          });
          break;

        case 1:
          this.setState({
            showBox2: e
          });
          break;

        case 2:
          this.setState({
            showBox3: e
          });
          break;

        case 3:
          this.setState({
            showBox4: e
          });
          break;

        default: // code block

      }
    }
  }, {
    key: "setvolume",
    value: function setvolume(value) {
      switch (value) {
        case 0:
          if (this.state.form[0].value < 1 || this.state.form[1].value < 1 || this.state.form[2].value < 0.3) {
            this.setState({
              setValuesMsj: true
            });
          } else {
            this.setState({
              placeholder2: "Contenido a Enviar",
              hide: false,
              hide2: false,
              setValuesMsj: false
            });
          }

          break;

        case 1:
          this.setState({
            setValuesMsj: false
          });
          break;

        case 2:
          var newState = this.state.form;

          if (this.state.form[0].value == 0) {
            newState[0].value = 1;
          }

          if (this.state.form[1].value == 0) {
            newState[1].value = 1;
          }

          if (this.state.form[2].value == 0 && localStorage.transporte == "MARITÍMO") {
            newState[2].value = 1;
          } else if (this.state.form[2].value == 0 && localStorage.transporte == "AÉREO") {
            newState[2].value = 0.6;
            newState[5].value = 1;
          } else {}

          this.setState({
            placeholder2: "Contenido a Enviar",
            form: newState,
            hide: false,
            hide2: false,
            setValuesMsj: false
          });
          break;

        default: // code block

      }
    }
  }, {
    key: "apiPost",
    value: function apiPost(url, datos, fn) {
      axios_default.a.post(url, datos).then(function (res) {
        var response = res.data.data.r_dat; //this.setState({ response });

        fn(response);
      }).catch(function (error) {
        Object(Utils["d" /* errores */])("ApipostServicios", error);
      });
    }
  }, {
    key: "fuzzy",
    value: function fuzzy(query) {
      this.setState({
        isLoading: true
      });
      var miniSearch = new es5m({
        fields: ["puerto"],
        // fields to index for full-text search
        storeFields: ["puerto"],
        // fields to return with search results
        searchOptions: {
          fuzzy: 0.2,
          prefix: true
        }
      });
      miniSearch.addAll(this.state.portOrigin);
      var results = miniSearch.search(query);
      this.setState({
        fuzzyOptions: results,
        isLoading: false,
        tokenize: function tokenize(puerto) {
          return puerto.split("-");
        },
        // indexing tokenizer
        searchOptions: {
          tokenize: function tokenize(puerto) {
            return puerto.split(/[\s-]+/);
          } // search query tokenizer

        }
      });
    }
  }, {
    key: "setValues",
    value: function setValues() {
      var _this2 = this;

      if (localStorage.volumenTotal > 0) {
        var PesovolumenTotal = JSON.parse(localStorage.getItem("PesovolumenTotal"));

        if (PesovolumenTotal > 0 && PesovolumenTotal < 1) {
          PesovolumenTotal.toFixed(2);
        }

        var _Form = [{
          Label: "lumps",
          value: JSON.parse(localStorage.getItem("bultosTotal"))
        }, {
          Label: "weight",
          value: JSON.parse(localStorage.getItem("pesoTotal"))
        }, {
          Label: "volume",
          value: JSON.parse(localStorage.getItem("volumenTotal"))
        }, {
          Label: "weightUnit",
          value: "2"
        }, {
          Label: "volumeUnit",
          value: "1"
        }, {
          Label: "weightVolume",
          value: PesovolumenTotal
        }];
        this.setState({
          form: _Form
        });
        this.setState(function (ps) {
          return {
            Calculator: !ps.Calculator
          };
        });
      } else {
        this.setState({
          toggleAlert: true
        });
        timer = setTimeout(function () {
          _this2.setState({
            toggleAlert: false
          });
        }, 5000);
      }
    }
  }, {
    key: "handleFormState",
    value: function handleFormState(input) {
      switch (input.id) {
        case 0:
          if (input.value < 0) {} else {
            localStorage.setItem("lumps", JSON.stringify(this.state.form[0].value));
          }

          break;

        case 1:
          if (input.value < 0) {} else {
            localStorage.setItem("weight", JSON.stringify(this.state.form[1].value));
          }

          break;

        case 2:
          if (input.value < 0) {} else {
            localStorage.setItem("volume", JSON.stringify(this.state.form[2].value));

            if (input.value > 15) {
              this.setState({
                msjActivo: true
              });
            }
          }

          break;

        case 3:
          localStorage.setItem("weightUnit", JSON.stringify(this.state.form[3].value));
          break;

        default: // code block

      }

      if (input.value < 0) {
        var newState = this.state.form;
        newState[input.id].value = 0;
        this.setState({
          form: newState
        });
      } else {
        Number(input.value);
        var _newState = this.state.form;
        _newState[input.id].value = input.value;
        this.setState({
          form: _newState
        });
      }
    }
  }, {
    key: "validation",
    value: function validation() {
      if (valid === "1") {
        if ((this.state.activeTab =  true && this.state.count1 === 0 && this.state.count2 === 0 && this.state.count3 === 0 && this.state.count4 === 0) && this.state.portOriginSelected.length === 0 && this.state.portDestinySelected.length === 0 || (this.state.activeTab =  true && this.state.form[0].length === 0 && this.state.form[1].length === 0 && this.state.form[2].length === 0) && this.state.portOriginSelected.length === 0 && this.state.portDestinySelected.length === 0) {
          this.setState({
            validated1: true,
            validated2: true,
            validated3: true
          });
        } else if ((this.state.activeTab =  true && this.state.count1 === 0 && this.state.count2 === 0 && this.state.count3 === 0 && this.state.count4 === 0) || (this.state.activeTab =  true && this.state.form[0].length === 0 && this.state.form[1].length === 0 && this.state.form[2].length === 0)) {
          this.setState({
            validated1: true,
            validated2: false,
            validated3: false
          });
        } else if (this.state.portOriginSelected.length === 0) {
          this.setState({
            validated2: true,
            validated1: false,
            validated3: false
          });
        } else if (this.state.portDestinySelected.length === 0) {
          this.setState({
            validated3: true,
            validated2: false,
            validated1: false
          });
        } else {
          this.setState({
            validated1: false,
            validated2: false,
            validated3: false
          });
        }
      } else {}
    }
  }, {
    key: "portOrigin",
    value: function portOrigin(fn) {
      this.setState({
        portOrigin: fn
      });
    }
  }, {
    key: "changePlaceholder",
    value: function changePlaceholder(e) {
      switch (e) {
        case 1:
          this.setState({
            placeholder: "Contenedor completo"
          });
          localStorage.tipocon = "Contenedor completo";

          if (localStorage.transporte == "MARITÍMO") {
            var url = this.props.api + "maritimo_v2/get_find_port_mar_begin";
            var datos = {
              word: "",
              port: "MARITIMO FCL"
            };
            this.apiPost(url, datos, this.portOrigin);
            /*Event("2.1 Tipo Envio FCL", "2.1 Tipo Envio FCL", "RUTA");*/
          } else {
            localStorage.tipocon = "Aereo";
          }

          break;

        case 2:
          this.setState({
            placeholder: "Contenedor compartido"
          });

          if (localStorage.transporte == "MARITÍMO") {
            localStorage.tipocon = "Contenedor compartido";
            var url1 = this.props.api + "maritimo_v2/get_find_port_mar_begin";
            var datos1 = {
              word: "",
              port: "MARITIMO LCL"
            };
            this.apiPost(url1, datos1, this.portOrigin);

            if (!this.state.banderatipoLCL) {
              /*Event("1.1 Tipo Envio LCL", "1.1 Tipo Envio LCL", "RUTA");*/
              this.setState({
                banderatipoLCL: true
              });
            }
          } else {
            localStorage.tipocon = "Aereo";
          }

          break;

        default: // code block

      }
    }
  }, {
    key: "portDestiny",
    value: function portDestiny(fn) {
      this.setState({
        portDestiny: fn
      });
    }
  }, {
    key: "ResetCont",
    value: function ResetCont(e) {
      var x = e.map(function (item) {
        return item.detalles;
      });
      var count1;
      var count2;
      var count3;
      var count4;

      if (x.includes("20' Standard")) {
        this.setState({
          count1: this.state.count1 * 0
        });
        count1 = this.state.count1 * 0;
      } else {
        count1 = this.state.count1;
      }

      if (x.includes("40' Standard")) {
        this.setState({
          count2: this.state.count2 * 0
        });
        count2 = this.state.count2 * 0;
      } else {
        count2 = this.state.count2;
      }

      if (x.includes("40' High Cube")) {
        this.setState({
          count3: this.state.count3 * 0
        });
        count3 = this.state.count3 * 0;
      } else {
        count3 = this.state.count3;
      }

      if (x.includes("40' NOR")) {
        this.setState({
          count4: this.state.count4 * 0
        });
        count4 = this.state.count4;
      } else {
        count4 = this.state.count4;
      }

      var retorno = " ";

      if (count1 > 0) {
        retorno += count1;
        retorno += "*20' Std, ";
      }

      if (count2 > 0) {
        retorno += count2;
        retorno += "*40' Std, ";
      }

      if (count3 > 0) {
        retorno += count3;
        retorno += "*40' HIGH CUBE, ";
      }

      if (count4 > 0) {
        retorno += count4;
        retorno += "*40' NOR";
      }

      var conte = [[1, count1], [2, count2], [3, count3], [4, count4]];
      var value8 = conte.filter(function (valor) {
        if (!valor.includes(0)) {
          return valor;
        }
      });
      localStorage.contenedores = this.array_js_to_pg(value8);
      localStorage.contenedoresDes = retorno;
    }
  }, {
    key: "showResult",
    value: function showResult(e) {
      var formatter = new Intl.NumberFormat("de-DE");
      this.setState({
        showResult: []
      });
      var resultado;
      var resultvalue;
      var resultNULL;
      var resulValuetNULL;
      var result;
      var filtro;

      if (this.state.placeholder === "Contenedor completo" && localStorage.transporte === "MARITÍMO") {
        filtro = e.filter(function (item) {
          return item.tipo == "FLETE MARÍTIMO" && item.cantidad > 0 && item.valor !== null;
        });
        resultNULL = e.filter(function (item) {
          if (item.valor == null) {
            return item;
          }
        });

        if (resultNULL.length > 0) {
          resulValuetNULL = true;
        } else {
          resulValuetNULL = false;
        }

        if (filtro.length > 0) {
          resultvalue = true;
        } else {
          resultvalue = false;
        }

        if (resultvalue && !resulValuetNULL) {} else if (resulValuetNULL) {
          this.ResetCont(resultNULL);
          resultado = /*#__PURE__*/react_default.a.createElement("div", {
            className: "alert alert-danger"
          }, " ", "Actualmente NO HAY TARIFAS para", " ", resultNULL.map(function (item) {
            return /*#__PURE__*/react_default.a.createElement("span", null, item.detalles, " , ");
          }), " ", "Con el destino seleccionado");
        } else {
          resultado = /*#__PURE__*/react_default.a.createElement("div", {
            className: "alert alert-danger"
          }, " ", "Actualmente NO HAY TARIFAS para el destino seleccionado.");
        }
      } else {
        resultvalue = e.map(function (item) {
          if (item > 0 || item !== null) {
            return true;
          } else {
            return false;
          }
        });

        if (resultvalue[0]) {} else if (this.state.placeholder === "Contenedor compartido" && localStorage.transporte == "MARITÍMO") {
          if (this.state.form[2].value > 16) {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "El"), " VOLUMEN", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "registrado no debe"), " EXCEDER DE 16M3, ", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "seleccione"), "Contenedor Completo o Contacte a un ASESOR +51-972530303.");
          } else if (this.state.form[1].value > 15000) {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "El"), " PESO", " ", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "registrado no debe"), " EXCEDER DE 20.000,00 kgs,", " ", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "modifique o seleccione"), " ", "Contenedor Completo.");
          } else {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "No hay tarifa con estos datos, INTENTE CON OTRA RUTA."), " ");
          }
        } else if (localStorage.transporte === "AÉREO") {
          if (this.state.form[5].value > this.state.form[1].value && this.state.form[5].value < 0.6) {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, "El", " ", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "PESO VOLUMEN minimo a registrar es 0.6 M3"), ", favor colocar un Valor Superior.");
          } else if (this.state.form[1].value > this.state.form[5].value && this.state.form[1].value < 60) {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, "El", " ", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, "PESO MINIMO a registrar debe ser MAYOR A 60 KG 0 0.6m3"), ", favor colocar un Valor Superior.");
          } else if (this.state.form[1].value === this.state.form[5].value && (this.state.form[1].value < 60 || this.state.form[5].value < 0.6)) {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, "El valor de", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, " VOLUMEN O PESO KILOS"), ", debe ser", /*#__PURE__*/react_default.a.createElement("span", {
              className: "color-blue"
            }, " SUPERIOR A 60 KG 0 0.6m3 "), "favor registrar un Valor Superior.");
          } else {
            resultado = /*#__PURE__*/react_default.a.createElement("div", {
              className: "alert alert-danger"
            }, " ", "No hay tarifa con estos datos, INTENTE CON OTRA RUTA.");
          }
        } else {
          resultado = /*#__PURE__*/react_default.a.createElement("div", {
            className: "alert alert-danger"
          }, " ", "Actualmente NO HAY TARIFAS Con el destino seleccionado");
        }
      }

      this.setState({
        showResult: resultado
      });
    }
  }, {
    key: "scrollTo",
    value: function scrollTo(e) {
      var element;

      switch (e) {
        case 1:
          element = document.getElementById("link5");
          Object(es["a" /* default */])(element, {
            behavior: "smooth",
            block: "center",
            inline: "center"
          });
          break;

        case 2:
          element = document.getElementById("alert");
          Object(es["a" /* default */])(element, {
            behavior: "smooth",
            block: "center",
            inline: "center"
          });
          break;

        case 3:
          break;

        default: // code block

      }
    }
  }, {
    key: "get_ctz",
    value: function get_ctz() {
      valid = "1";

      switch (validated) {
        case "2":
          localStorage.bottomChange = "2";
          this.setState({
            resultContainer: false
          });
          break;

        case "3":
          this.setState({
            showLoading: true
          });
          this.get_ctz_fcl();
          break;

        case "4":
          this.setState({
            showLoading: true
          });
          this.get_ctz_lcl();
          break;

        case "5":
          this.setState({
            showLoading: true
          });
          this.get_ctz_aereo();
          break;

        default: // code block

      }
    }
  }, {
    key: "get_ctz_fcl",
    value: function get_ctz_fcl() {
      var _this3 = this;

      valid = "2";
      var puerto;
      var value = this.state.portOriginSelected.map(function (item) {
        return item.id;
      });
      localStorage.puerto = this.state.portOriginSelected.map(function (item) {
        return item.puerto;
      });
      localStorage.puertoD = puerto = this.state.portDestinySelected.map(function (item) {
        return item.puerto;
      });
      var portDestiny = this.state.portDestinySelected.map(function (item) {
        return item.id;
      });
      var value2 = 0;
      var value3 = 0;
      var value4 = false;
      var value5 = false;
      var value6 = false;
      var value7 = false;
      var conte = [[1, this.state.count1], [2, this.state.count2], [3, this.state.count3], [4, this.state.count4]];
      var value8 = conte.filter(function (valor) {
        if (!valor.includes(0)) {
          return valor;
        }
      });

      if (puerto[0].includes("PERÚ")) {
        localStorage.paisDestino = "PERU";
      } else if (puerto[0].includes("PANAMÁ")) {
        localStorage.paisDestino = "PANAMA";
      } else {
        localStorage.paisDestino = "VENEZUELA";
      }

      var url = this.props.api + "maritimo_v2/get_ctz_fcl_flete";
      var datos = {
        datos: [value.toString(), portDestiny.toString(), this.array_js_to_pg(value8), value2.toString(), value3.toString(), value4, value5, value6, value7]
      };
      axios_default.a.post(url, datos).then(function (res) {
        var response = res.data.data.r_dat;
        var filtro = response.filter(function (item) {
          return item.tipo == "FLETE MARÍTIMO" && item.cantidad > 0 && item.valor !== null;
        });
        var result;

        if (filtro.length > 0) {
          result = filtro.map(function (duration) {
            return duration.valor;
          }).reduce(function (accumulator, current) {
            return [+accumulator + +current];
          });
          /*Event(
            "2.4 Agregar Servicios FCL",
            "2.4 Agregar Servicios FCL",
            "RUTA"
          );*/
        } else {
          result = filtro;
        }

        localStorage.bottomChange = "1";

        _this3.setState({
          result: result,
          showLoading: false,
          resultContainer: true,
          hide: false
        });

        _this3.scrollTo(1);

        _this3.showResult(response);
      }).catch(function (error) {
        Object(Utils["d" /* errores */])("CTZ_FCL_Ruta", error);
      });
      localStorage.origen = value;
      localStorage.destino = portDestiny;
      localStorage.contenedores = this.array_js_to_pg(value8);
      localStorage.contenedoresDes = this.contenedores();
    }
  }, {
    key: "get_ctz_lcl",
    value: function get_ctz_lcl() {
      var _this4 = this;

      valid = "2";
      var puerto;
      var destino;
      var portOrigin = this.state.portOriginSelected.map(function (item) {
        return item.id;
      });
      localStorage.puerto = this.state.portOriginSelected.map(function (item) {
        return item.puerto;
      });
      localStorage.puertoD = puerto = this.state.portDestinySelected.map(function (item) {
        return item.puerto;
      });
      var portDestiny = this.state.portDestinySelected.map(function (item) {
        return item.id;
      });
      var volumenValue;

      if (this.state.form[4].value != "1") {
        volumenValue = this.state.form[2].value / 35.315;
      } else {
        volumenValue = this.state.form[2].value;
      }

      if (puerto[0].includes("PERÚ")) {
        localStorage.paisDestino = "PERU";
        destino = "PERU";
      } else if (puerto[0].includes("PANAMÁ")) {
        localStorage.paisDestino = "PANAMA";
        destino = "PANAMA";
      } else {
        localStorage.paisDestino = "VENEZUELA";
        destino = "VENEZUELA";
      }

      var weight = this.state.form[1].value;
      var weightUnit = this.state.form[3].value;
      var volumen = volumenValue;
      var volumentUnit = 5;
      var lumps = this.state.form[0].value;
      var value2 = 0;
      var value3 = 0;
      var value4 = false;
      var value5 = false;
      var value6 = false;
      var value7 = false;
      localStorage.origen = portOrigin;
      localStorage.destino = portDestiny;
      localStorage.contenedores = weightUnit;
      localStorage.weight = weight;
      localStorage.contenedoresDes = this.contenedores();
      localStorage.volumen = volumen;
      localStorage.volumentUnit = volumentUnit;
      localStorage.lumps = lumps;
      var url = this.props.api + "maritimo_v2/get_ctz_lcl_flete";
      var datos = {
        datos: [portOrigin.toString(), portDestiny.toString(), weight.toString(), weightUnit.toString(), volumen.toString(), volumentUnit.toString(), lumps.toString(), value2.toString(), value3.toString(), value4, value5, value6, value7]
      };
      axios_default.a.post(url, datos).then(function (res) {
        var response = res.data.data.r_dat;
        var result = response.filter(function (item) {
          return item.tipo == "FLETE MARÍTIMO";
        }).map(function (duration) {
          return duration.valor;
        });
        result.map(function (item) {
          if (item > 0) {
            /*Event(
              "1.5 Agregar Servicios LCL",
              "1.5 Agregar Servicios LCL",
              "RUTA"
            );*/
            return item;
          }
        });
        localStorage.bottomChange = "1";

        if (result[0] > 0 && destino == "VENEZUELA") {
          _this4.toggleModalPreview("2");
        } else {
          _this4.setState({
            result: result,
            showLoading: false,
            resultContainer: true,
            hide: false
          });

          _this4.scrollTo(1);

          _this4.showResult(result);
        }
      }).catch(function (error) {
        Object(Utils["d" /* errores */])("CTZ_LCL_Ruta", error);
      });
    }
  }, {
    key: "get_ctz_aereo",
    value: function get_ctz_aereo() {
      var _this5 = this;

      valid = "2";
      var puerto;
      var portOrigin = this.state.portOriginSelected.map(function (item) {
        return item.id;
      });
      localStorage.puerto = this.state.portOriginSelected.map(function (item) {
        return item.puerto;
      });
      localStorage.puertoD = puerto = this.state.portDestinySelected.map(function (item) {
        return item.puerto;
      });
      var portDestiny = this.state.portDestinySelected.map(function (item) {
        return item.id;
      });

      if (puerto[0].includes("PERÚ")) {
        localStorage.paisDestino = "PERU";
      } else if (puerto[0].includes("PANAMÁ")) {
        localStorage.paisDestino = "PANAMA";
      } else {
        localStorage.paisDestino = "VENEZUELA";
      }

      var volumenValue;

      if (this.state.form[5].value) {
        volumenValue = this.state.form[5].value;
      } else {
        volumenValue = this.state.form[2].value;
      }

      var weight = this.state.form[1].value.toString();
      var weightUnit = this.state.form[3].value.toString();
      var volumen = volumenValue.toString();
      var volumentUnit = 5;
      var lumps = this.state.form[0].value.toString();
      var value2 = 0;
      var value3 = 0;
      var value4 = false;
      var value5 = false;
      var value6 = false;
      var value7 = false;
      var url = this.props.api + "aereo_v2/get_ctz_aereo_flete";
      var datos = {
        datos: [portOrigin.toString(), portDestiny.toString(), weight.toString(), weightUnit.toString(), volumen.toString(), volumentUnit.toString(), lumps.toString(), value2.toString(), value3.toString(), value4, value5, value6, value7]
      };
      axios_default.a.post(url, datos).then(function (res) {
        var response = res.data.data.r_dat;
        var result = response.filter(function (item) {
          return item.tipo == "FLETE AÉREO";
        }).map(function (duration) {
          return duration.valor;
        });
        result.map(function (item) {
          if (item > 0) {
            /*Event(
              "3.5 Agregar Servicios Aereo",
              "3.5 Agregar Servicios Aereo",
              "RUTA"
            );*/
            return item;
          }
        });
        localStorage.bottomChange = "1";

        _this5.setState({
          result: result,
          showLoading: false,
          resultContainer: true,
          hide: false
        });

        _this5.scrollTo(1);

        _this5.showResult(result);
      }).catch(function (error) {
        Object(Utils["d" /* errores */])("CTZ_AEREO_Ruta", error);
      });
      localStorage.origen = portOrigin;
      localStorage.destino = portDestiny;
      localStorage.contenedores = weightUnit;
      localStorage.weight = weight;
      localStorage.contenedoresDes = this.contenedores();
      localStorage.volumen = volumen;
      localStorage.volumentUnit = volumentUnit;
      localStorage.lumps = lumps;
    }
  }, {
    key: "array_js_to_pg",
    value: function array_js_to_pg(array) {
      var retorno = "{";
      var x = 1;
      array.forEach(function (element) {
        retorno += "{";

        for (var i = 0; i < element.length; i++) {
          retorno += element[i];
          if (i + 1 < element.length) retorno += ",";
        }

        retorno += "}";
        if (x < array.length) retorno += ",";
        x++;
      });
      retorno += "}";
      return retorno;
    }
  }, {
    key: "tabs",
    value: function tabs(tab) {
      var _this6 = this;

      if (this.state.activeTab !== tab) {
        if (tab === "2") {
          this.setState({
            showCalmsj: false
          });

          if (screen.width < 769) {
            timer = setTimeout(function () {
              _this6.setState({
                showCalmsj: false
              });
            }, 6000);
          }

          this.setState({
            activeTab: tab
          });
        } else {
          this.setState({
            activeTab: tab
          });
        }

        this.scrollTo(3);
      }
    }
  }, {
    key: "minusCount",
    value: function minusCount(e) {
      switch (e) {
        case 1:
          if (this.state.count1 >= 1) {
            this.setState({
              count1: this.state.count1 - 1
            });
            localStorage.setItem("count1", JSON.stringify(this.state.count1 - 1));
          }

          break;

        case 2:
          if (this.state.count2 >= 1) {
            this.setState({
              count2: this.state.count2 - 1
            });
            localStorage.setItem("count2", JSON.stringify(this.state.count2 - 1));
          }

          break;

        case 3:
          if (this.state.count3 >= 1) {
            this.setState({
              count3: this.state.count3 - 1
            });
            localStorage.setItem("count3", JSON.stringify(this.state.count3 - 1));
          }

          break;

        case 4:
          if (this.state.count4 >= 1) {
            this.setState({
              count4: this.state.count4 - 1
            });
            localStorage.setItem("count4", JSON.stringify(this.state.count4 - 1));
          }

          break;

        default: // code block

      }
    }
  }, {
    key: "plusCount",
    value: function plusCount(e) {
      switch (e) {
        case 1:
          this.setState({
            count1: this.state.count1 + 1
          });
          localStorage.count1 = this.state.count1 + 1;
          break;

        case 2:
          this.setState({
            count2: this.state.count2 + 1
          });
          localStorage.count2 = this.state.count2 + 1;
          break;

        case 3:
          this.setState({
            count3: this.state.count3 + 1
          });
          localStorage.count3 = this.state.count3 + 1;
          break;

        case 4:
          this.setState({
            count4: this.state.count4 + 1
          });
          localStorage.count4 = this.state.count4 + 1;
          break;

        default: // code block

      }
    }
  }, {
    key: "contenedores",
    value: function contenedores() {
      if (this.state.placeholder === "Contenedor completo") {
        var retorno = " ";

        if (this.state.count1 > 0) {
          retorno += this.state.count1;
          retorno += "*20' Std, ";
        }

        if (this.state.count2 > 0) {
          retorno += this.state.count2;
          retorno += "*40' Std, ";
        }

        if (this.state.count3 > 0) {
          retorno += this.state.count3;
          retorno += "*40' HIGH CUBE, ";
        }

        if (this.state.count4 > 0) {
          retorno += this.state.count4;
          retorno += "*40' NOR";
        }

        return retorno;
      } else {
        var _retorno = " ";
        _retorno += this.state.form[0].value;

        if (this.state.form[0].value == 1) {
          _retorno += " Bulto de ";
        } else {
          _retorno += " Bultos de ";
        }

        _retorno += this.state.form[1].value;
        _retorno += " ";
        _retorno += localStorage.peso;
        _retorno += " y ";
        _retorno += this.state.form[2].value;
        _retorno += " M³";
        return _retorno;
      }
    }
  }, {
    key: "toggleCalculator",
    value: function toggleCalculator() {
      this.setState(function (ps) {
        return {
          Calculator: !ps.Calculator
        };
      });
    }
  }, {
    key: "toggle2",
    value: function toggle2() {
      this.setState(function (ps) {
        return {
          hide2: !ps.hide2,
          resultContainer: false
        };
      });

      if (this.state.hide2 == true) {
        this.scrollTo(3);
      } else {
        this.scrollTo(4);
      }
    }
  }, {
    key: "toggle",
    value: function toggle() {
      this.setState(function (ps) {
        return {
          hide: !ps.hide,
          resultContainer: false,
          showResult: []
        };
      });

      if (this.state.hide == true) {
        this.scrollTo(3);
      } else {
        this.scrollTo(4);
      }
    }
  }, {
    key: "toggleModalPreview",
    value: function toggleModalPreview(OpcionValue) {
      switch (OpcionValue) {
        case "1":
          this.eventOpcion1();
          this.props.history.push("/Servicios");
          break;

        case "2":
          var data = JSON.parse(sessionStorage.getItem("user"));

          if (sessionStorage.user !== undefined && sessionStorage.usuario == "2" && data[1] !== "Invitado") {
            var empty = [];
            var vacio = empty.map(function (item) {
              return item;
            });
            localStorage.distrito = vacio;
            localStorage.regulador = vacio;
            localStorage.tipoMercancia = vacio;
            localStorage.distritoN = vacio;
            localStorage.ProvinciaN = vacio;
            localStorage.monto = vacio;
            localStorage.value4 = false;
            localStorage.value5 = false;
            localStorage.value6 = false;
            localStorage.value7 = false;
            localStorage.value8 = false;
            localStorage.seguro = true;
            localStorage.check3 = false;
            localStorage.check = false;
            localStorage.check2 = false;
            localStorage.check1 = false;
            localStorage.check4 = true;
            /*switch (localStorage.tipocon) {
              case "Contenedor completo":
                Event(
                  "2.7.1 Calcular Cotizacion FCL Directo",
                  "2.7.1 Calcular Cotizacion FCL Directo",
                  "SERVICIOS"
                );
                break;
              case "Contenedor compartido":
                Event(
                  "1.8.1 Calcular Cotizacion LCL Directo",
                  "1.8.1 Calcular Cotizacion LCL Directo",
                  "SERVICIOS"
                );
                break;
              case "Aereo":
                Event(
                  "3.8.1 Calcular Cotizacion Aereo Directo",
                  "3.8.1 Calcular Cotizacion Aereo Directo",
                  "SERVICIOS"
                );
                break;
               default:
            }*/

            this.redirect();
          } else {
            var user = [["PIC"], ["Invitado"], ["Invitado"], ["/assets/img/profile-pic-l.png"]];
            sessionStorage.setItem("user", JSON.stringify(user));
            sessionStorage.usuario = "2";
            this.redirect(); //this.setState((ps) => ({ IsVisiblePreview: !ps.IsVisiblePreview }));
          }

          break;

        default:
          break;
      }
    }
  }, {
    key: "toggleAlertOpcion",
    value: function toggleAlertOpcion() {
      this.setState(function (prevState) {
        return {
          isOpenAlertOpcion: !prevState.isOpenAlertOpcion
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this7 = this;

      if (localStorage.transporte == undefined) {
        return /*#__PURE__*/react_default.a.createElement(Redirect["a" /* default */], {
          to: "/"
        });
      } else {}

      var selected;
      var selected2;
      var selected3;
      var portOrigin = this.state.portOrigin;
      var formatter = new Intl.NumberFormat("de-DE");

      if (valid === "1") {
        if (this.state.portDestinySelected.length > 0) {
          selected2 = false;
        } else {
          selected2 = true;
        }
      } else {
        selected2 = false;
      }

      var PortEmptyLabel;
      var PortEmptyLabel2;

      if (this.state.activeTab == "1" && this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0 || this.state.activeTab == "2" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0)) {
        PortEmptyLabel = "Origen no disponible";
        PortEmptyLabel2 = "Cargando destinos";
      } else {
        PortEmptyLabel = "Debe selecionar tipo de envio";
        PortEmptyLabel2 = "Debe selecionar tipo de envio";
      }

      if (valid === "1") {
        if (this.state.activeTab == "1" && this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0 || this.state.activeTab == "2" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0)) {
          selected3 = false;
        } else {
          selected3 = true;
        }
      } else {
        if (this.state.activeTab == "1" && this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0 || this.state.activeTab == "2" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0)) {
          selected3 = false;
        } else {
          selected3 = true;
        }
      }

      if (valid === "1") {
        if (this.state.portOriginSelected.length > 0) {
          selected = false;
        } else {
          selected = true;
        }
      } else {
        if (this.state.portOriginSelected.length > 0) {
          selected = false;
        } else if (this.state.portOriginSelected.length < 1 && (this.state.activeTab == "1" && this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0 || this.state.activeTab == "2" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0))) {
          selected = true;
        } else {
          selected = false;
        }
      }

      if (selected === false && selected2 === false && selected3 === false && valid === "1") {
        if (localStorage.transporte == "MARITÍMO") {
          if (localStorage.tipocon == "Contenedor completo") {
            validated = "3";
          } else {
            validated = "4";
          }
        } else {
          validated = "5";
        }
      } else {
        validated = "2";
      }

      localStorage.page = "2";
      var unidad;

      switch (this.state.form[3].value) {
        case "2":
          unidad = "Kg";
          localStorage.peso = "Kg";
          break;

        case "1":
          unidad = "Lb";
          localStorage.peso = "Lb";
          break;

        case "4":
          unidad = "T";
          localStorage.peso = "T";
          break;

        default: // code block

      }

      return /*#__PURE__*/react_default.a.createElement(react["Fragment"], null, /*#__PURE__*/react_default.a.createElement(helmet["a" /* Title */], null, /*#__PURE__*/react_default.a.createElement("title", null, window.location.pathname.replace('/', '') + " | " + "PIC  - Calculadora de Fletes")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "container2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        className: "h-100"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mb-3 "
      }, /*#__PURE__*/react_default.a.createElement(Link["a" /* default */], {
        to: "/"
      }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        style: {
          fontSize: "0.8rem",
          padding: "1px"
        },
        className: "btn-style",
        size: "sm"
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      }), " VOLVER"))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xl: "10",
        lg: "11",
        className: "mx-auto my-auto"
      }, localStorage.transporte == "MARITÍMO" ? /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mb-3 "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "step-style"
      }, "PASO 2. Indica Ruta y Tipo de Contenedor"), " ") : /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mb-3"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "mb-2 step-style"
      }, "PASO 2. Indica Ruta y Volumen de mercancia"), " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", null, "SOLO TRANSPORTAMOS CARGAS "), " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "MAYORES A 60 KG 0 0.6 m3")), " "), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto my-auto choseService"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true,
        id: "link2"
      }, localStorage.transporte == "MARITÍMO" ? /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        lg: 3,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "fontNavbar label-input",
        style: {
          width: "max-content",
          marginLeft: "-2em"
        },
        htmlFor: "TypeOfContainers"
      }, "Tipo de env\xEDo"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["p" /* Dropdown */], {
        className: "mr-2",
        isOpen: this.state.dropdownOpen,
        toggle: this.toggle
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["s" /* DropdownToggle */], {
        color: "light",
        caret: true,
        className: "form-container-type"
      }, this.state.placeholder)), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "container-info"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.placeholder === "Contenedor completo" && (this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0) ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count1 === 0 ? "none" : "inline-block"
        }
      }, this.state.count1, "*20' Std,", " "), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count2 === 0 ? "none" : "inline-block"
        }
      }, this.state.count2, "*40' Std,"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count3 === 0 ? "none" : "inline-block"
        }
      }, this.state.count3, "*40' HC,"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count4 === 0 ? "none" : "inline-block"
        }
      }, this.state.count4, "*40' NOR")), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.placeholder === "Contenedor compartido" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0) ? "" : "none"
        }
      }, "Bultos = ", this.state.form[0].value, ", Peso =", " ", this.state.form[1].value, " ", unidad, ",", " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          display: this.state.form[2].value > 0 && localStorage.transporte == "MARITÍMO" ? "" : "none"
        }
      }, "Volumen = ", this.state.form[2].value, " ", this.state.form[4].value == "1" ? "M³" : "Ft³"))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "validation-text",
        style: {
          display: selected3 === false ? "none" : ""
        }
      }, "Debe escoger tipo de env\xEDo"))) : /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        lg: 3,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "fontNavbar label-input",
        style: {
          width: "max-content"
        },
        htmlFor: "TypeOfContainers"
      }, "Contenido a Enviar"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["p" /* Dropdown */], {
        className: "mr-2",
        onClick: function onClick() {
          _this7.tabs("2");
        },
        isOpen: this.state.dropdownOpen2,
        toggle: this.toggle2
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["s" /* DropdownToggle */], {
        color: "light",
        caret: true,
        className: "form-container-type"
      }, this.state.placeholder2)), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "container-info"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.placeholder === "Contenedor completo" && (this.state.count1 > 0 || this.state.count2 > 0 || this.state.count3 > 0 || this.state.count4 > 0) ? "" : "none"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count1 === 0 ? "none" : "inline-block"
        }
      }, this.state.count1, "*20' Std,", " "), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count2 === 0 ? "none" : "inline-block"
        }
      }, this.state.count2, "*40' Std,"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count3 === 0 ? "none" : "inline-block"
        }
      }, this.state.count3, "*40' HC,"), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.count4 === 0 ? "none" : "inline-block"
        }
      }, this.state.count4, "*40' NOR")), /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          display: this.state.placeholder === "Contenedor compartido" && this.state.form[0].value > 0 && this.state.form[1].value > 0 && (this.state.form[2].value > 0 || this.state.form[5].value > 0) ? "" : "none"
        }
      }, "Bultos = ", this.state.form[0].value, ", Peso =", " ", this.state.form[1].value, " ", unidad, ",", " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          display: this.state.form[2].value > 0
        }
      }, "Volumen = ", this.state.form[2].value, " ", this.state.form[4].value == "1" ? "M³" : "Ft³"))), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontWeight: "bold",
          fontSize: "12px",
          display: selected3 === false ? "none" : ""
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "* "), " INGRESE LOS VALORES DE LA CARGA"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-orange",
        style: {
          fontWeight: "bold",
          fontSize: "10px",
          display: selected3 === false ? "none" : ""
        }
      }, "SOLO CARGAS MAYORES A 60 KG 0 0.6m3"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "typeOfContainer display-sm",
        style: {
          display: this.state.hide === false ? "none" : "contents"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "fontNavbar number-of-containers",
        style: {
          width: "100%"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["H" /* Nav */], {
        tabs: true
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], {
        className: "".concat(this.state.activeTab === "false" ? "colorTab" : "")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
        style: {
          padding: "1em 0em 0.5em 0.5em",
          borderBottom: "0.2em #0d5084",
          borderBottomStyle: "ridge"
        },
        className: classnames_default()({
          active: this.state.activeTab === "1"
        }),
        onClick: function onClick() {
          _this7.tabs("1");

          _this7.changePlaceholder(1);
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-FCL mr-2"
      }), " CONTENEDOR COMPLETO(FCL)", /*#__PURE__*/react_default.a.createElement("br", null), " ", /*#__PURE__*/react_default.a.createElement("span", null, "Un contenedor - Un cliente"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
        style: {
          padding: "1em 0em 0.5em 0.5em"
        },
        className: classnames_default()({
          active: this.state.activeTab === "2"
        }),
        onClick: function onClick() {
          _this7.tabs("2");

          _this7.changePlaceholder(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-LCL ml-2 mr-2"
      }), " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("div", null, "CONTENEDOR COMPARTIDO(LCL)", /*#__PURE__*/react_default.a.createElement("div", null, "\xF3 CARGA CONSOLIDADA")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "pl-5 ml-3"
      }, "Un contenedor - Varios cliente"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["M" /* TabContent */], {
        activeTab: this.state.activeTab
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "1"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "cost-top"
      }, /*#__PURE__*/react_default.a.createElement("dl", {
        className: "persons_quantity"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "20' STANDARD", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb1: true
            };
          });
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        value: this.state.count1,
        onClick: function onClick() {
          return _this7.minusCount(1);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count1,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 1,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count1,
        onClick: function onClick() {
          return _this7.plusCount(1);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' STANDARD", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb2: true
            };
          });
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count2,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 2,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count2,
        onClick: function onClick() {
          return _this7.plusCount(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' HC", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb3: true
            };
          });
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(3);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count3,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 3,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count3,
        onClick: function onClick() {
          return _this7.plusCount(3);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' NOR", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb4: true
            };
          });
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(4);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count4,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 4,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count4,
        onClick: function onClick() {
          return _this7.plusCount(4);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER", " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR")))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        id: "typeOfContainer"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true,
        className: "p-1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 col-md-3 pt-1 fontNavbar",
        for: "exampleEmail"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Nro de bultos",
        bsSize: "sm",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 0,
            value: e.target.value
          });
        }
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Peso",
        bsSize: "sm",
        value: this.state.form[1].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 1,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "2",
        value: "2"
      }, "Kilogramos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "1"
      }, "Libras"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "4",
        value: "4"
      }, "Toneladas"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-12 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "VOLUMEN", " ", /*#__PURE__*/react_default.a.createElement("i", {
        id: "volumen1",
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "volumen1"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "Es el resultado de multplicar largo x ancho x el alto del producto"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Ejemplo:"), " ", "Una mesa", /*#__PURE__*/react_default.a.createElement("ul", null, /*#__PURE__*/react_default.a.createElement("li", null, "Largo 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Ancho 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Alto 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Seria 1 x 1 x 1 = 1m3")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-12 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Volumen",
        bsSize: "sm",
        value: this.state.form[2].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 2,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "1",
        value: "1"
      }, "Metros c\xFAbicos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "2"
      }, "Pies c\xFAbicos"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "regulation-text mr-1 ml-1",
        style: {
          backgroundColor: "aliceblue",
          display: this.state.msjActivo === false ? "none" : ""
        }
      }, " ", "El presupuesto final que arroja la calculadora para cargas mayores a 15m3 esta sujeto a aprobaci\xF3n."))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "calculatorOpenContainer arial mx-auto  p-1 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        onClick: this.toggleCalculator
      }, /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          fontWeight: "bold",
          verticalAlign: "middle"
        },
        className: "mr-1 icon-doubt  "
      }), "\xA0 AYUDA - \xA1CALCULA VOLUMEN (m3) AQUI!")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER", " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this7.setvolume(0);

          _this7.tabs("2");

          _this7.changePlaceholder(2);
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR"), /*#__PURE__*/react_default.a.createElement("span", {
        id: "bottom"
      })))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "typeOfContainer display-sm",
        style: {
          display: this.state.hide2 === false ? "none" : "contents"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "fontNavbar number-of-containers",
        style: {
          width: "100%"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "navAereo"
      }, "CONTENIDO DE ENV\xCDO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        id: "typeOfContainer4"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true,
        className: "p-1"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 col-md-3 pt-1 fontNavbar",
        for: "exampleEmail"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Nro de bultos",
        bsSize: "sm",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 0,
            value: e.target.value
          });
        }
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Peso",
        bsSize: "sm",
        value: this.state.form[1].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 1,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "2",
        value: "2"
      }, "Kilogramos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "1"
      }, "Libras"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "4",
        value: "4"
      }, "Toneladas"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-12 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "VOLUMEN", " ", /*#__PURE__*/react_default.a.createElement("i", {
        id: "volumen3",
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "volumen3"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "Es el resultado de multplicar largo x ancho x el alto del producto"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Ejemplo:"), " ", "Una mesa", /*#__PURE__*/react_default.a.createElement("ul", null, /*#__PURE__*/react_default.a.createElement("li", null, "Largo 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Ancho 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Alto 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Seria 1 x 1 x 1 = 1m3")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-12 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Volumen",
        bsSize: "sm",
        value: this.state.form[2].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 2,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "1",
        value: "1"
      }, "Metros c\xFAbicos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "2"
      }, "Pies c\xFAbicos"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "mx-auto",
        style: {
          fontWeight: "bold",
          fontSize: "10px"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", null, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "* "), " ", "INGRESE UN VALOR", " "), /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, " ", "MAYOR A 60 KG 0 0.6m3")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "calculatorOpenContainer arial mx-auto mb-2 p-1"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        onClick: this.toggleCalculator
      }, /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          fontWeight: "bold",
          verticalAlign: "middle"
        },
        className: "mr-1 icon-doubt  "
      }), "\xA0AYUDA -\xA1CALCULA VOLUMEN AQUI!")), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide2: !prevState.hide2
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER", " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this7.setvolume(0);

          _this7.tabs("2");

          _this7.changePlaceholder(2);
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR"), /*#__PURE__*/react_default.a.createElement("span", {
        id: "bottom"
      })))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        lg: 3,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "fontNavbar label-input",
        htmlFor: "origin"
      }, "Origen"), /*#__PURE__*/react_default.a.createElement(lib["AsyncTypeahead"], {
        id: "originInput",
        isValid: selected,
        emptyLabel: localStorage.transporte == "MARITÍMO" ? PortEmptyLabel : "Aeropuerto no disponible",
        flip: true,
        delay: 10,
        placeholder: localStorage.transporte == "MARITÍMO" ? "ESCRIBA Pais,Ciudad o Puerto" : "Pais o Ciudad",
        isLoading: this.state.isLoading,
        labelKey: function labelKey(option) {
          return "".concat(option.puerto);
        },
        filterBy: function filterBy() {
          return true;
        },
        renderMenuItemChildren: function renderMenuItemChildren(option) {
          return /*#__PURE__*/react_default.a.createElement("div", null, option.puerto);
        },
        minLength: 1,
        onSearch: function onSearch(query) {
          _this7.fuzzy(query);
        },
        onChange: function onChange(portOriginSelected) {
          _this7.setState({
            portOriginSelected: portOriginSelected
          });

          _this7.Destiny(portOriginSelected);

          _this7.get_ctz();

          _this7.EventOrigen(localStorage.transporte == "MARITÍMO" ? 2 : 1);
        },
        ref: function ref(_ref2) {
          return _this7._typeahead = _ref2;
        },
        onFocus: function onFocus() {
          return _this7.setState(function () {
            return {
              hide: false,
              hide2: false
            };
          });
        },
        selected: this.state.portOriginSelected,
        options: this.state.fuzzyOptions
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected === false ? "none" : ""
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "arial"
      }, "ESCRIBA"), " Pais o Puerto de Origen"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        lg: 3,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "fontNavbar label-input",
        htmlFor: "destiny"
      }, "Destino"), /*#__PURE__*/react_default.a.createElement(lib["Typeahead"], {
        isValid: selected2,
        flip: true,
        id: "1",
        emptyLabel: localStorage.transporte == "MARITÍMO" ? PortEmptyLabel2 : "Aeropuerto no disponible",
        labelKey: function labelKey(option) {
          return "".concat(option.puerto);
        },
        renderMenuItemChildren: function renderMenuItemChildren(option) {
          return /*#__PURE__*/react_default.a.createElement("div", null, option.puerto);
        },
        autocomplete: false,
        multiple: false,
        options: this.state.portDestiny,
        placeholder: localStorage.transporte == "MARITÍMO" ? "Pais,Ciudad o Puerto" : "Pais o Ciudad",
        onChange: function onChange(portDestinySelected) {
          _this7.setState({
            portDestinySelected: portDestinySelected
          });

          _this7.EventDestino(localStorage.transporte == "MARITÍMO" ? 2 : 1, portDestinySelected);
        },
        onInputChange: this.checkOrigen,
        onFocus: this.checkOrigen,
        selected: this.state.portDestinySelected
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "validation-text",
        style: {
          display: selected2 === false ? "none" : ""
        }
      }, "Seleccione un puerto de destino valido"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        lg: 3,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        style: {
          display: this.state.resultContainer ? "none" : "",
          marginTop: "25px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        type: "submit",
        className: "btn-style-blue ",
        size: "sm",
        onClick: function onClick() {
          _this7.get_ctz();

          _this7.eventAgregarToque();
        },
        color: "success"
      }, "SIGUIENTE"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "typeOfContainer display-large",
        style: {
          display: this.state.hide === false ? "none" : "block"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "fontNavbar number-of-containers"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["H" /* Nav */], {
        tabs: true
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], {
        className: "".concat(this.state.activeTab === "false" ? "colorTab" : "")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
        style: {
          borderBottom: "0.2em #0d5084",
          borderBottomStyle: "ridge"
        },
        className: classnames_default()({
          active: this.state.activeTab === "1"
        }),
        onClick: function onClick() {
          _this7.tabs("1");

          _this7.changePlaceholder(1);
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-FCL mr-2"
      }), " CONTENEDOR COMPLETO(FCL)", /*#__PURE__*/react_default.a.createElement("br", null), " ", /*#__PURE__*/react_default.a.createElement("span", null, "Un contenedor - Un cliente"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["I" /* NavItem */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["J" /* NavLink */], {
        style: {
          padding: "1em 0em 0.5em 0.5em"
        },
        className: classnames_default()({
          active: this.state.activeTab === "2"
        }),
        onClick: function onClick() {
          _this7.tabs("2");

          _this7.changePlaceholder(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "icon-LCL ml-2 mr-2"
      }), " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("div", null, "CONTENEDOR COMPARTIDO(LCL)", /*#__PURE__*/react_default.a.createElement("div", null, "\xF3 CARGA CONSOLIDADA")), /*#__PURE__*/react_default.a.createElement("div", {
        className: "pl-5 ml-3"
      }, "Un contenedor - Varios cliente"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["M" /* TabContent */], {
        activeTab: this.state.activeTab
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "1"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "cost-top"
      }, /*#__PURE__*/react_default.a.createElement("dl", {
        className: "persons_quantity"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "20' STANDARD", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 0);
        },
        onMouseEnter: function onMouseEnter() {
          return _this7.handleBoxToggle(true, 0);
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 0);
        },
        className: "tooltip-inner-cont ".concat(this.state.showBox1 ? "show-toolkit-cont" : "hide-toolkit")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, "20' STANDARD"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/20std.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "33 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-blue",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "5.89 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        value: this.state.count1,
        onClick: function onClick() {
          return _this7.minusCount(1);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count1,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 1,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count1,
        onClick: function onClick() {
          return _this7.plusCount(1);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' STANDARD", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 1);
        },
        onMouseEnter: function onMouseEnter() {
          return _this7.handleBoxToggle(true, 1);
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 1);
        },
        className: "tooltip-inner-cont ".concat(this.state.showBox2 ? "show-toolkit-cont" : "hide-toolkit")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' STANDARD"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40std.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "67 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-red",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count2,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 2,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count2,
        onClick: function onClick() {
          return _this7.plusCount(2);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' HC", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 2);
        },
        onMouseEnter: function onMouseEnter() {
          return _this7.handleBoxToggle(true, 2);
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 2);
        },
        className: "tooltip-inner-cont ".concat(this.state.showBox3 ? "show-toolkit-cont" : "hide-toolkit")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' HC"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40hq.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "76 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-green",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.69 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(3);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count3,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 3,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count3,
        onClick: function onClick() {
          return _this7.plusCount(3);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement("span", null, "40' NOR", " ", /*#__PURE__*/react_default.a.createElement("i", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 3);
        },
        onMouseEnter: function onMouseEnter() {
          return _this7.handleBoxToggle(true, 3);
        },
        style: {
          cursor: "pointer"
        },
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement("div", {
        onMouseLeave: function onMouseLeave() {
          return _this7.handleBoxToggle(false, 3);
        },
        className: "tooltip-inner-cont ".concat(this.state.showBox4 ? "show-toolkit-cont" : "hide-toolkit")
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' NOR"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40nor.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "48 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-blue",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS EXTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "Es un contenedor con un equipo de refrigeraci\xF3n interno que esta apagado, el cual se utiliza para para cargas de menor volumen ya que tienes 10 M3 menos que el 40 ST debido al equipo que tiene interno.", " "))))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, " ", /*#__PURE__*/react_default.a.createElement("span", {
        "data-uil": "minus",
        className: "minus fal fa-minus",
        onClick: function onClick() {
          return _this7.minusCount(4);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-minus "
      }))), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        name: "adt",
        type: "text",
        className: "value adults",
        value: this.state.count4,
        readOnly: "readonly"
      })), /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement("span", {
        key: 4,
        "data-uil": "plus",
        className: "plus fal fa-plus",
        value: this.state.count4,
        onClick: function onClick() {
          return _this7.plusCount(4);
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-plus"
      })))))), /*#__PURE__*/react_default.a.createElement("div", {
        className: "quantity_wrap"
      }, /*#__PURE__*/react_default.a.createElement("dt", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER", " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      }))), /*#__PURE__*/react_default.a.createElement("dd", null, /*#__PURE__*/react_default.a.createElement("ul", {
        className: "number"
      }, /*#__PURE__*/react_default.a.createElement("li", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR")))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["N" /* TabPane */], {
        tabId: "2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        id: "typeOfContainer2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true,
        className: "p-1 mt-2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 col-md-3 pt-1 fontNavbar",
        for: "exampleEmail"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Nro de bultos",
        bsSize: "sm",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 0,
            value: e.target.value
          });
        }
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Peso",
        bsSize: "sm",
        value: this.state.form[1].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 1,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "2",
        value: "2"
      }, "Kilogramos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "1"
      }, "Libras"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "4",
        value: "4"
      }, "Toneladas"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-12 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "VOLUMEN", " ", /*#__PURE__*/react_default.a.createElement("i", {
        id: "volumen2",
        className: "simple-icon-question question-style2"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "volumen2"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "Es el resultado de multplicar largo x ancho x el alto del producto"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Ejemplo:"), " ", "Una mesa", /*#__PURE__*/react_default.a.createElement("ul", null, /*#__PURE__*/react_default.a.createElement("li", null, "Largo 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Ancho 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Alto 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Seria 1 x 1 x 1 = 1m3"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-12 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Volumen",
        bsSize: "sm",
        value: this.state.form[2].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 2,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "1",
        value: "1"
      }, "Metros c\xFAbicos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "2"
      }, "Pies c\xFAbicos"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "regulation-text mr-1 ml-1",
        style: {
          backgroundColor: "aliceblue",
          display: this.state.msjActivo === false ? "none" : ""
        }
      }, " ", "El presupuesto final que arroja la calculadora para cargas mayores a 15m3 esta sujeto a aprobaci\xF3n."))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "calculatorOpenContainer arial mx-auto  p-1 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        onClick: this.toggleCalculator
      }, /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          fontWeight: "bold",
          verticalAlign: "middle"
        },
        className: "mr-1 icon-doubt  "
      }), "\xA0AYUDA - \xA1CALCULA VOLUMEN (m3) AQUI!"), /*#__PURE__*/react_default.a.createElement("div", defineProperty_default()({
        className: "color-white",
        style: {
          border: "1px solid grey",
          top: "-22em",
          height: "28em",
          zIndex: "1150 !important"
        }
      }, "className", "up-izquierda  tooltip-inner-cont ".concat(this.state.showCalmsj === false ? "hide-toolkit" : "show-toolkit-cont")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          borderRadius: "2rem"
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              showCalmsj: !prevState.showCalmsj
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          height: "14em"
        },
        className: "theme-color-blueHeader arial"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "6",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        }
      }, "\xBFDudas con el VOLUMEN (Metros C\xFAbicos)?")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "6",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleCalculator,
        className: "icon-pop-up"
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          top: "18em",
          position: "absolute"
        }
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        onClick: this.toggleCalculator,
        xxs: "12",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        },
        className: "color-blue"
      }, "\xA1Calcula tu Volumen (Metros C\xFAbicos)", " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-green"
      }, " ", "AQUI!"))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2 mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        className: "color-grey",
        style: {
          fontSize: "0.7rem"
        },
        id: "SelectedOnce3",
        type: "checkbox",
        onChange: this.SelectedOnce,
        label: "No volver a mostrar este mensaje"
      })))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide: !prevState.hide
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER", " ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this7.setvolume(0);

          _this7.tabs("2");

          _this7.changePlaceholder(2);
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR"), /*#__PURE__*/react_default.a.createElement("span", {
        id: "bottom"
      })))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        className: "typeOfContainer display-large",
        style: {
          display: this.state.hide2 === false ? "none" : "block"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "fontNavbar number-of-containers"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "navAereo"
      }, "CONTENIDO DE ENV\xCDO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["t" /* Form */], {
        id: "typeOfContainer3"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        form: true,
        className: "p-1 mt-2 "
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 col-md-3 pt-1 fontNavbar",
        for: "exampleEmail"
      }, "BULTOS"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Nro de bultos",
        bsSize: "sm",
        value: this.state.form[0].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 0,
            value: e.target.value
          });
        }
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-3 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "PESO"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-9 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Peso",
        bsSize: "sm",
        value: this.state.form[1].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 1,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 3,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "2",
        value: "2"
      }, "Kilogramos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "1",
        value: "1"
      }, "Libras"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "4",
        value: "4"
      }, "Toneladas"))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        className: "form-row"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        className: "col-12 pt-1 col-md-3 fontNavbar",
        for: "examplePassword"
      }, "VOLUMEN", " ", /*#__PURE__*/react_default.a.createElement("i", {
        id: "volumen4",
        className: "simple-icon-question question-style2"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["Q" /* UncontrolledTooltip */], {
        placement: "right",
        target: "volumen4"
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " ", "Es el resultado de multplicar largo x ancho x el alto del producto"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "color-blue text-justify"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Ejemplo:"), " ", "Una mesa", /*#__PURE__*/react_default.a.createElement("ul", null, /*#__PURE__*/react_default.a.createElement("li", null, "Largo 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Ancho 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Alto 1 metro"), /*#__PURE__*/react_default.a.createElement("li", null, "Seria 1 x 1 x 1 = 1m3")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["x" /* InputGroup */], {
        className: "col-12 col-md-9"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "number",
        placeholder: "Ingrese Volumen",
        bsSize: "sm",
        value: this.state.form[2].value,
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 2,
            value: e.target.value
          });
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["w" /* Input */], {
        type: "select",
        name: "select",
        onChange: function onChange(e) {
          _this7.handleFormState({
            id: 4,
            value: e.target.value || ""
          });
        }
      }, /*#__PURE__*/react_default.a.createElement("option", {
        defaultValue: true,
        key: "1",
        value: "1"
      }, "Metros c\xFAbicos"), /*#__PURE__*/react_default.a.createElement("option", {
        key: "2",
        value: "2"
      }, "Pies c\xFAbicos"))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["v" /* FormText */], {
        className: "mx-auto",
        style: {
          fontWeight: "bold",
          fontSize: "10px"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", null, /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, "* "), " INGRESE UN VALOR", " "), /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-orange"
      }, " ", "MAYOR A 60 KG 0 0.6m3")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12,
        className: "calculatorOpenContainer arial mx-auto  p-1 mb-2"
      }, /*#__PURE__*/react_default.a.createElement("span", {
        onClick: this.toggleCalculator
      }, /*#__PURE__*/react_default.a.createElement("i", {
        style: {
          fontWeight: "bold",
          verticalAlign: "middle"
        },
        className: "mr-1 icon-doubt  "
      }), "\xA0AYUDA - \xA1CALCULA VOLUMEN AQUI!"), /*#__PURE__*/react_default.a.createElement("div", defineProperty_default()({
        className: "bg-white",
        style: {
          border: "1px solid grey",
          top: "-22em",
          height: "28em",
          zIndex: "1150 !important"
        }
      }, "className", "up-izquierda  tooltip-inner-cont ".concat(this.state.showCalmsj === false ? "hide-toolkit" : "show-toolkit-cont")), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          borderRadius: "2rem"
        }
      }, " ", /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              showCalmsj: !prevState.showCalmsj
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          height: "14em"
        },
        className: "theme-color-blueHeader arial"
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        className: "mt-1",
        xxs: "6",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        }
      }, "\xBFDudas con el VOLUMEN ?")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "6",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleCalculator,
        className: "icon-pop-up"
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], {
        style: {
          top: "18em",
          position: "absolute"
        }
      }, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        onClick: this.toggleCalculator,
        xxs: "12",
        md: "12"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontSize: "1.3rem"
        },
        className: "color-blue"
      }, "\xA1Calcula tu Volumen", " ", /*#__PURE__*/react_default.a.createElement("span", {
        className: "color-green"
      }, " AQUI!"))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mt-2 mb-2"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        className: "color-grey",
        style: {
          fontSize: "0.7rem"
        },
        id: "SelectedOnce4",
        type: "checkbox",
        onChange: this.SelectedOnce,
        label: "No volver a mostrar este mensaje"
      })))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setState(function (prevState) {
            return {
              hide2: !prevState.hide2
            };
          });
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          _this7.setvolume(0);

          _this7.tabs("2");

          _this7.changePlaceholder(2);
        },
        className: "btn-style-blue",
        size: "sm"
      }, "GUARDAR"), /*#__PURE__*/react_default.a.createElement("span", {
        id: "bottom"
      })))))), this.state.showLoading ? /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "7",
        sm: "12",
        className: "NoPadding mx-auto mt-2 ",
        style: {
          display: "block"
        }
      }, /*#__PURE__*/react_default.a.createElement("div", {
        className: " mx-auto ",
        xxs: "12",
        md: "7",
        sm: "12",
        style: {
          fontWeight: "bold"
        }
      }, "CALCULANDO"), /*#__PURE__*/react_default.a.createElement("div", {
        className: "   mx-auto loading2",
        xxs: "12",
        md: "7",
        sm: "12"
      })) : "", " ", this.state.showResult == 0 ? "" : /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "8",
        sm: "12",
        className: "arial color-orange NoPadding mx-auto"
      }, this.state.showResult), this.state.resultContainer && this.state.result > 0 && this.state.portOriginSelected.length > 0 && this.state.portDestinySelected.length > 0 ? /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        xl: "8",
        lg: "12",
        className: "NoPadding mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        className: "mx-auto mb-3 "
      }, /*#__PURE__*/react_default.a.createElement("span", {
        className: "step-style"
      }, "2.1 Elige una opcion"), " "), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: "12",
        md: "12",
        sm: "12",
        className: " font2 p-1 mx-auto"
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["a" /* Alert */], {
        color: "danger",
        isOpen: this.state.isOpenAlertOpcion,
        toggle: this.toggleAlertOpcion
      }, "Debe selecionar una opci\xF3n.")), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "NoPadding mx-auto"
      }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        onClick: function onClick() {
          _this7.toggleModalPreview("1");
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        check: true
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontWeight: "bold"
        },
        className: "divAvisoRuta color-white text-center m-2 mx-auto theme-color-blue"
      }, /*#__PURE__*/react_default.a.createElement("div", null, "OPCION 1"), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          display: "flex"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        id: "opcion1",
        type: "radio",
        value: "1",
        name: "radio1",
        onChange: function onChange(e) {
          _this7.setState({
            OpcionValue: e.target.value || ""
          });
        }
      }), "AGREGAR SERVICIOS DE ADUANA EN DESTINO"))))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "6",
        sm: "12",
        className: "NoPadding mx-auto"
      }, " ", /*#__PURE__*/react_default.a.createElement(reactstrap_es["u" /* FormGroup */], {
        onClick: function onClick() {
          _this7.toggleModalPreview("2");
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["A" /* Label */], {
        check: true
      }, /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          fontWeight: "bold"
        },
        className: "divAvisoRuta m-2 mx-auto text-center theme-color-white"
      }, /*#__PURE__*/react_default.a.createElement("div", null, "OPCION 2"), /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          display: "flex"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["o" /* CustomInput */], {
        id: "opcion2",
        type: "radio",
        value: "2",
        name: "radio1",
        onChange: function onChange(e) {
          _this7.setState({
            OpcionValue: e.target.value || ""
          });
        }
      }), "NO AGREGAR SERVICIOS **SOLO DESEO FLETE**"))))))) : "", /*#__PURE__*/react_default.a.createElement("span", {
        id: "link5"
      }), /*#__PURE__*/react_default.a.createElement(timeLine["a" /* default */], {
        data: this.state.resultContainer
      })))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], {
        isOpen: this.state.Calculator,
        toggle: this.toggleCalculator,
        backdrop: true,
        size: "lg",
        style: {
          borderRadius: "1.1rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        style: {
          padding: "1em 0px 1em 0px"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: this.toggleCalculator,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        id: "alert",
        className: "font mt-2 mb-2",
        style: {
          textAlign: "center"
        }
      }, "CALCULADORA VOLUMETRICA"), this.state.toggleAlert ? /*#__PURE__*/react_default.a.createElement("div", {
        style: {
          position: "sticky !important"
        },
        className: "alert alert-danger text-center arial  mx-auto",
        role: "alert"
      }, "Debe Agregar Medidas O Volver.") : "", /*#__PURE__*/react_default.a.createElement(react_default.a.Suspense, {
        fallback: Ruta_renderLoader()
      }, /*#__PURE__*/react_default.a.createElement(Calculadora, null)), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        size: "sm",
        className: "btn-style mt-2 float-left",
        color: "success",
        onClick: this.toggleCalculator
      }, "VOLVER ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      }))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["l" /* Col */], {
        xs: 12,
        md: 12
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        size: "sm",
        className: "btn-style-blue mt-2 float-right",
        color: "success",
        onClick: this.setValues
      }, "AGREGAR CALCULO")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], {
        isOpen: this.state.setValuesMsj,
        size: "md",
        className: "VolumenModal ",
        style: {
          borderRadius: "1.1rem",
          marginTop: "65px !important"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setvolume(1);
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer",
          zIndex: "999"
        }
      }), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "text-center pt-2 pb-2 arial color-white",
        style: {
          backgroundColor: "#0d5084",
          fontSize: "2em"
        }
      }, "ATENCI\xD3N"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto pb-2 pt-2 arial",
        style: {
          textAlign: "justify"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontSize: "1rem",
          color: "#0d5084"
        }
      }, "Los siguientes campos fueron dejados con su valor por defecto:", /*#__PURE__*/react_default.a.createElement("br", null), /*#__PURE__*/react_default.a.createElement("ul", {
        className: "color-orange"
      }, this.state.form[0].value == 0 ? /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("li", null, "Bultos."), " ") : "", this.state.form[1].value == 0 ? /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("li", null, "Peso. "), " ") : "", this.state.form[2].value == 0 ? /*#__PURE__*/react_default.a.createElement("div", null, " ", /*#__PURE__*/react_default.a.createElement("li", null, "Volumen."), " ") : ""))), /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "p-1 arial color-white",
        style: {
          fontSize: "1rem",
          textAlign: "center",
          backgroundColor: "#0d5084"
        }
      }, /*#__PURE__*/react_default.a.createElement("span", null, "Los campos vac\xEDos se COMPLETAR\xC1N AUTOM\xC1TICAMENTE con un VALOR de 1.")))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["F" /* ModalFooter */], null, /*#__PURE__*/react_default.a.createElement(CustomBootstrap["a" /* Colxx */], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto ",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setvolume(1);
        },
        className: "btn-style mr-2",
        size: "sm"
      }, "VOLVER ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-left-circle"
      })), /*#__PURE__*/react_default.a.createElement(reactstrap_es["c" /* Button */], {
        color: "success",
        onClick: function onClick() {
          return _this7.setvolume(2);
        },
        className: "btn-style-blue",
        size: "sm"
      }, "CONTINUAR ", /*#__PURE__*/react_default.a.createElement("i", {
        className: "simple-icon-arrow-right-circle"
      }))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], defineProperty_default()({
        isOpen: this.state.showBoxmb1,
        backdrop: true,
        style: {
          borderRadius: "1.8rem"
        },
        className: "NoPadding atencion"
      }, "style", {
        padding: "0rem"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        className: "NoPadding",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb1: false
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, "20' STANDARD"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/20std.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "33 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-blue",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "5.89 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], defineProperty_default()({
        isOpen: this.state.showBoxmb2,
        backdrop: true,
        style: {
          borderRadius: "1.8rem"
        },
        className: "NoPadding atencion"
      }, "style", {
        padding: "0rem"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        className: "NoPadding",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb2: false
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' STANDARD"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40std.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "67 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-red",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], defineProperty_default()({
        isOpen: this.state.showBoxmb3,
        backdrop: true,
        style: {
          borderRadius: "1.8rem"
        },
        className: "NoPadding atencion"
      }, "style", {
        padding: "0rem"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        className: "NoPadding",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb3: false
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' HC"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40hq.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "76 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-green",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS INTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("td", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.69 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " El peso m\xE1ximo permitido variara de acuerdo a las normativas establecidas por cada naviera.", " "))))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["D" /* Modal */], defineProperty_default()({
        isOpen: this.state.showBoxmb4,
        backdrop: true,
        style: {
          borderRadius: "1.8rem"
        },
        className: "NoPadding atencion"
      }, "style", {
        padding: "0rem"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["E" /* ModalBody */], {
        className: "NoPadding",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react_default.a.createElement("i", {
        onClick: function onClick() {
          return _this7.setState(function () {
            return {
              showBoxmb4: false
            };
          });
        },
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.2em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["L" /* Row */], null, /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["d" /* Card */], {
        style: {
          borderRadius: "2rem"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["g" /* CardHeader */], {
        className: "theme-color-blueHeader arial",
        style: {
          padding: "0px"
        }
      }, " ", "40' NOR"), /*#__PURE__*/react_default.a.createElement(reactstrap_es["e" /* CardBody */], {
        style: {
          padding: "0px"
        }
      }, /*#__PURE__*/react_default.a.createElement(reactstrap_es["h" /* CardImg */], {
        top: true,
        width: "1em",
        src: "/assets/img/contenedores/40nor.png",
        alt: "1em"
      }), /*#__PURE__*/react_default.a.createElement(reactstrap_es["j" /* CardText */], null, /*#__PURE__*/react_default.a.createElement(reactstrap_es["O" /* Table */], {
        style: {
          textAlign: "center",
          fontSize: "0.7rem"
        },
        striped: true
      }, /*#__PURE__*/react_default.a.createElement("tbody", {
        style: {}
      }, /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "PESO MAX. PERMITIDO"), /*#__PURE__*/react_default.a.createElement("td", null, "26 TONELADAS")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "3"
      }, "VOL. MAX. CARGABLE"), /*#__PURE__*/react_default.a.createElement("td", null, "48 M3")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        className: "theme-color-blue",
        scope: "row",
        colSpan: "4"
      }, "MEDIDAS EXTERNAS CONTENEDOR")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "LARGO"), /*#__PURE__*/react_default.a.createElement("td", null, "ANCHO"), /*#__PURE__*/react_default.a.createElement("td", null, "ALTO")), /*#__PURE__*/react_default.a.createElement("tr", null, /*#__PURE__*/react_default.a.createElement("th", {
        scope: "row",
        colSpan: "2"
      }, "12.03 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.35 MTS"), /*#__PURE__*/react_default.a.createElement("td", null, "2.38 MTS")))))), /*#__PURE__*/react_default.a.createElement(reactstrap_es["f" /* CardFooter */], {
        className: "theme-color-redFooter"
      }, " ", /*#__PURE__*/react_default.a.createElement("div", null, /*#__PURE__*/react_default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota:"), " Es un contenedor con un equipo de refrigeraci\xF3n interno que esta apagado, el cual se utiliza para para cargas de menor volumen ya que tienes 10 M3 menos que el 40 ST debido al equipo que tiene interno.", " "))))))), /*#__PURE__*/react_default.a.createElement(react_default.a.Suspense, {
        fallback: Ruta_renderLoader()
      }, /*#__PURE__*/react_default.a.createElement(ModalSignInRuta, {
        active: this.state.IsVisiblePreview,
        toggle: this.toggleModalPreview,
        redirect: this.redirect,
        bandera: this.state.resultContainer
      })), /*#__PURE__*/react_default.a.createElement(index_esm["a" /* Beforeunload */], {
        onBeforeunload: function onBeforeunload() {
          return "Si sales perderas los datosa no guardados";
        }
      }), /*#__PURE__*/react_default.a.createElement(chat["a" /* default */], null));
    }
  }]);

  return ruta;
}(react["Component"]);

/* harmony default export */ var Ruta = __webpack_exports__["default"] = (Object(withRouter["a" /* default */])(Ruta_ruta));

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = warn;
exports.resetWarned = resetWarned;

var _warning = _interopRequireDefault(__webpack_require__(7));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * This code is copied from: https://github.com/ReactTraining/react-router/blob/master/modules/routerWarning.js
 */
var warned = {};

function warn(falseToWarn, message) {
  // Only issue deprecation warnings once.
  if (!falseToWarn && message.indexOf('deprecated') !== -1) {
    if (warned[message]) {
      return;
    }

    warned[message] = true;
  }

  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  _warning["default"].apply(void 0, [falseToWarn, "[react-bootstrap-typeahead] ".concat(message)].concat(args));
}

function resetWarned() {
  warned = {};
}

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "addCustomOption", {
  enumerable: true,
  get: function get() {
    return _addCustomOption2["default"];
  }
});
Object.defineProperty(exports, "areEqual", {
  enumerable: true,
  get: function get() {
    return _areEqual2["default"];
  }
});
Object.defineProperty(exports, "defaultFilterBy", {
  enumerable: true,
  get: function get() {
    return _defaultFilterBy2["default"];
  }
});
Object.defineProperty(exports, "getAccessibilityStatus", {
  enumerable: true,
  get: function get() {
    return _getAccessibilityStatus2["default"];
  }
});
Object.defineProperty(exports, "getDisplayName", {
  enumerable: true,
  get: function get() {
    return _getDisplayName2["default"];
  }
});
Object.defineProperty(exports, "getHintText", {
  enumerable: true,
  get: function get() {
    return _getHintText2["default"];
  }
});
Object.defineProperty(exports, "getInputText", {
  enumerable: true,
  get: function get() {
    return _getInputText2["default"];
  }
});
Object.defineProperty(exports, "getIsOnlyResult", {
  enumerable: true,
  get: function get() {
    return _getIsOnlyResult2["default"];
  }
});
Object.defineProperty(exports, "getMatchBounds", {
  enumerable: true,
  get: function get() {
    return _getMatchBounds2["default"];
  }
});
Object.defineProperty(exports, "getMenuItemId", {
  enumerable: true,
  get: function get() {
    return _getMenuItemId2["default"];
  }
});
Object.defineProperty(exports, "getOptionLabel", {
  enumerable: true,
  get: function get() {
    return _getOptionLabel2["default"];
  }
});
Object.defineProperty(exports, "getStringLabelKey", {
  enumerable: true,
  get: function get() {
    return _getStringLabelKey2["default"];
  }
});
Object.defineProperty(exports, "getTruncatedOptions", {
  enumerable: true,
  get: function get() {
    return _getTruncatedOptions2["default"];
  }
});
Object.defineProperty(exports, "isSelectable", {
  enumerable: true,
  get: function get() {
    return _isSelectable2["default"];
  }
});
Object.defineProperty(exports, "isShown", {
  enumerable: true,
  get: function get() {
    return _isShown2["default"];
  }
});
Object.defineProperty(exports, "pluralize", {
  enumerable: true,
  get: function get() {
    return _pluralize2["default"];
  }
});
Object.defineProperty(exports, "preventInputBlur", {
  enumerable: true,
  get: function get() {
    return _preventInputBlur2["default"];
  }
});
Object.defineProperty(exports, "scrollIntoViewIfNeeded", {
  enumerable: true,
  get: function get() {
    return _scrollIntoViewIfNeeded2["default"];
  }
});
Object.defineProperty(exports, "shouldSelectHint", {
  enumerable: true,
  get: function get() {
    return _shouldSelectHint2["default"];
  }
});
Object.defineProperty(exports, "stripDiacritics", {
  enumerable: true,
  get: function get() {
    return _stripDiacritics2["default"];
  }
});
Object.defineProperty(exports, "warn", {
  enumerable: true,
  get: function get() {
    return _warn2["default"];
  }
});

var _addCustomOption2 = _interopRequireDefault(__webpack_require__(524));

var _areEqual2 = _interopRequireDefault(__webpack_require__(525));

var _defaultFilterBy2 = _interopRequireDefault(__webpack_require__(526));

var _getAccessibilityStatus2 = _interopRequireDefault(__webpack_require__(527));

var _getDisplayName2 = _interopRequireDefault(__webpack_require__(528));

var _getHintText2 = _interopRequireDefault(__webpack_require__(529));

var _getInputText2 = _interopRequireDefault(__webpack_require__(531));

var _getIsOnlyResult2 = _interopRequireDefault(__webpack_require__(532));

var _getMatchBounds2 = _interopRequireDefault(__webpack_require__(316));

var _getMenuItemId2 = _interopRequireDefault(__webpack_require__(533));

var _getOptionLabel2 = _interopRequireDefault(__webpack_require__(228));

var _getStringLabelKey2 = _interopRequireDefault(__webpack_require__(252));

var _getTruncatedOptions2 = _interopRequireDefault(__webpack_require__(534));

var _isSelectable2 = _interopRequireDefault(__webpack_require__(317));

var _isShown2 = _interopRequireDefault(__webpack_require__(535));

var _pluralize2 = _interopRequireDefault(__webpack_require__(536));

var _preventInputBlur2 = _interopRequireDefault(__webpack_require__(537));

var _scrollIntoViewIfNeeded2 = _interopRequireDefault(__webpack_require__(538));

var _shouldSelectHint2 = _interopRequireDefault(__webpack_require__(539));

var _stripDiacritics2 = _interopRequireDefault(__webpack_require__(265));

var _warn2 = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 157:
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    getRawTag = __webpack_require__(344),
    objectToString = __webpack_require__(345);

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ 159:
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

var baseIsNative = __webpack_require__(358),
    getValue = __webpack_require__(361);

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;


/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_LABELKEY = exports.DOWN = exports.RIGHT = exports.UP = exports.LEFT = exports.SPACE = exports.ESC = exports.RETURN = exports.TAB = exports.BACKSPACE = void 0;

/**
 * Common (non-printable) keycodes for `keydown` and `keyup` events. Note that
 * `keypress` handles things differently and may not return the same values.
 */
var BACKSPACE = 8;
exports.BACKSPACE = BACKSPACE;
var TAB = 9;
exports.TAB = TAB;
var RETURN = 13;
exports.RETURN = RETURN;
var ESC = 27;
exports.ESC = ESC;
var SPACE = 32;
exports.SPACE = SPACE;
var LEFT = 37;
exports.LEFT = LEFT;
var UP = 38;
exports.UP = UP;
var RIGHT = 39;
exports.RIGHT = RIGHT;
var DOWN = 40;
exports.DOWN = DOWN;
var DEFAULT_LABELKEY = 'label';
exports.DEFAULT_LABELKEY = DEFAULT_LABELKEY;

/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

var isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = toKey;


/***/ }),

/***/ 168:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys = __webpack_require__(269);
var hasSymbols = typeof Symbol === 'function' && typeof Symbol('foo') === 'symbol';

var toStr = Object.prototype.toString;
var concat = Array.prototype.concat;
var origDefineProperty = Object.defineProperty;

var isFunction = function (fn) {
	return typeof fn === 'function' && toStr.call(fn) === '[object Function]';
};

var arePropertyDescriptorsSupported = function () {
	var obj = {};
	try {
		origDefineProperty(obj, 'x', { enumerable: false, value: obj });
		// eslint-disable-next-line no-unused-vars, no-restricted-syntax
		for (var _ in obj) { // jscs:ignore disallowUnusedVariables
			return false;
		}
		return obj.x === obj;
	} catch (e) { /* this is IE 8. */
		return false;
	}
};
var supportsDescriptors = origDefineProperty && arePropertyDescriptorsSupported();

var defineProperty = function (object, name, value, predicate) {
	if (name in object && (!isFunction(predicate) || !predicate())) {
		return;
	}
	if (supportsDescriptors) {
		origDefineProperty(object, name, {
			configurable: true,
			enumerable: false,
			value: value,
			writable: true
		});
	} else {
		object[name] = value;
	}
};

var defineProperties = function (object, map) {
	var predicates = arguments.length > 2 ? arguments[2] : {};
	var props = keys(map);
	if (hasSymbols) {
		props = concat.call(props, Object.getOwnPropertySymbols(map));
	}
	for (var i = 0; i < props.length; i += 1) {
		defineProperty(object, props[i], map[props[i]], predicates[props[i]]);
	}
};

defineProperties.supportsDescriptors = !!supportsDescriptors;

module.exports = defineProperties;


/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(208);
/* harmony import */ var hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(55);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }






/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ["wrappedComponentRef"]);

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Route__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], {
      children: function children(routeComponentProps) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, _extends({}, remainingProps, routeComponentProps, {
          ref: wrappedComponentRef
        }));
      }
    });
  };

  C.displayName = "withRouter(" + (Component.displayName || Component.name) + ")";
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
  };

  return hoist_non_react_statics__WEBPACK_IMPORTED_MODULE_2___default()(C, Component);
};

/* harmony default export */ __webpack_exports__["a"] = (withRouter);

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Title; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(233);



function Title(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_2__[/* Helmet */ "a"], null, props.children);
}
Title.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};

/***/ }),

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

var listCacheClear = __webpack_require__(348),
    listCacheDelete = __webpack_require__(349),
    listCacheGet = __webpack_require__(350),
    listCacheHas = __webpack_require__(351),
    listCacheSet = __webpack_require__(352);

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

module.exports = ListCache;


/***/ }),

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

var eq = __webpack_require__(186);

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

module.exports = assocIndexOf;


/***/ }),

/***/ 186:
/***/ (function(module, exports) {

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

module.exports = eq;


/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160);

/* Built-in method references that are verified to be native. */
var nativeCreate = getNative(Object, 'create');

module.exports = nativeCreate;


/***/ }),

/***/ 188:
/***/ (function(module, exports, __webpack_require__) {

var isKeyable = __webpack_require__(370);

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

module.exports = getMapData;


/***/ }),

/***/ 189:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;


/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(148),
    isKey = __webpack_require__(221),
    stringToPath = __webpack_require__(409),
    toString = __webpack_require__(262);

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }
  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

module.exports = castPath;


/***/ }),

/***/ 208:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
var REACT_STATICS = {
    childContextTypes: true,
    contextTypes: true,
    defaultProps: true,
    displayName: true,
    getDefaultProps: true,
    getDerivedStateFromProps: true,
    mixins: true,
    propTypes: true,
    type: true
};

var KNOWN_STATICS = {
    name: true,
    length: true,
    prototype: true,
    caller: true,
    callee: true,
    arguments: true,
    arity: true
};

var defineProperty = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = getPrototypeOf && getPrototypeOf(Object);

function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
    if (typeof sourceComponent !== 'string') { // don't hoist over string (html) components

        if (objectPrototype) {
            var inheritedComponent = getPrototypeOf(sourceComponent);
            if (inheritedComponent && inheritedComponent !== objectPrototype) {
                hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
            }
        }

        var keys = getOwnPropertyNames(sourceComponent);

        if (getOwnPropertySymbols) {
            keys = keys.concat(getOwnPropertySymbols(sourceComponent));
        }

        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];
            if (!REACT_STATICS[key] && !KNOWN_STATICS[key] && (!blacklist || !blacklist[key])) {
                var descriptor = getOwnPropertyDescriptor(sourceComponent, key);
                try { // Avoid failures from read-only properties
                    defineProperty(targetComponent, key, descriptor);
                } catch (e) {}
            }
        }

        return targetComponent;
    }

    return targetComponent;
}

module.exports = hoistNonReactStatics;


/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqualDeep = __webpack_require__(347),
    isObjectLike = __webpack_require__(159);

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

module.exports = baseIsEqual;


/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map');

module.exports = Map;


/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

var mapCacheClear = __webpack_require__(362),
    mapCacheDelete = __webpack_require__(369),
    mapCacheGet = __webpack_require__(371),
    mapCacheHas = __webpack_require__(372),
    mapCacheSet = __webpack_require__(373);

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

module.exports = MapCache;


/***/ }),

/***/ 216:
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(388),
    baseKeys = __webpack_require__(395),
    isArrayLike = __webpack_require__(219);

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;


/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(390),
    isObjectLike = __webpack_require__(159);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;


/***/ }),

/***/ 218:
/***/ (function(module, exports) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(229),
    isLength = __webpack_require__(218);

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(190),
    toKey = __webpack_require__(167);

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;


/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(148),
    isSymbol = __webpack_require__(183);

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

module.exports = isKey;


/***/ }),

/***/ 222:
/***/ (function(module, exports) {

/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

module.exports = noop;


/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(446);

module.exports = Function.prototype.bind || implementation;


/***/ }),

/***/ 224:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(223);
var GetIntrinsic = __webpack_require__(272);

var $apply = GetIntrinsic('%Function.prototype.apply%');
var $call = GetIntrinsic('%Function.prototype.call%');
var $reflectApply = GetIntrinsic('%Reflect.apply%', true) || bind.call($call, $apply);

var $gOPD = GetIntrinsic('%Object.getOwnPropertyDescriptor%', true);
var $defineProperty = GetIntrinsic('%Object.defineProperty%', true);
var $max = GetIntrinsic('%Math.max%');

if ($defineProperty) {
	try {
		$defineProperty({}, 'a', { value: 1 });
	} catch (e) {
		// IE 8 has a broken defineProperty
		$defineProperty = null;
	}
}

module.exports = function callBind(originalFunction) {
	var func = $reflectApply(bind, $call, arguments);
	if ($gOPD && $defineProperty) {
		var desc = $gOPD(func, 'length');
		if (desc.configurable) {
			// original length, plus the receiver, minus any additional arguments (after the receiver)
			$defineProperty(
				func,
				'length',
				{ value: 1 + $max(0, originalFunction.length - (arguments.length - 1)) }
			);
		}
	}
	return func;
};

var applyBind = function applyBind() {
	return $reflectApply(bind, $apply, arguments);
};

if ($defineProperty) {
	$defineProperty(module.exports, 'apply', { value: applyBind });
} else {
	module.exports.apply = applyBind;
}


/***/ }),

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _invariant = _interopRequireDefault(__webpack_require__(12));

var _isPlainObject = _interopRequireDefault(__webpack_require__(299));

var _getStringLabelKey = _interopRequireDefault(__webpack_require__(252));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Retrieves the display string from an option. Options can be the string
 * themselves, or an object with a defined display string. Anything else throws
 * an error.
 */
function getOptionLabel(option, labelKey) {
  if (option.paginationOption || option.customOption) {
    return option[(0, _getStringLabelKey["default"])(labelKey)];
  }

  var optionLabel;

  if (typeof option === 'string') {
    optionLabel = option;
  }

  if (typeof labelKey === 'function') {
    // This overwrites string options, but we assume the consumer wants to do
    // something custom if `labelKey` is a function.
    optionLabel = labelKey(option);
  } else if (typeof labelKey === 'string' && (0, _isPlainObject["default"])(option)) {
    optionLabel = option[labelKey];
  }

  !(typeof optionLabel === 'string') ?  false ? undefined : invariant(false) : void 0;
  return optionLabel;
}

var _default = getOptionLabel;
exports["default"] = _default;

/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObject = __webpack_require__(157);

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),

/***/ 230:
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(213);

/**
 * Performs a deep comparison between two values to determine if they are
 * equivalent.
 *
 * **Note:** This method supports comparing arrays, array buffers, booleans,
 * date objects, error objects, maps, numbers, `Object` objects, regexes,
 * sets, strings, symbols, and typed arrays. `Object` objects are compared
 * by their own, not inherited, enumerable properties. Functions and DOM
 * nodes are compared by strict equality, i.e. `===`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.isEqual(object, other);
 * // => true
 *
 * object === other;
 * // => false
 */
function isEqual(value, other) {
  return baseIsEqual(value, other);
}

module.exports = isEqual;


/***/ }),

/***/ 242:
/***/ (function(module, exports) {

/**
 * Gets the first element of `array`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @alias first
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the first element of `array`.
 * @example
 *
 * _.head([1, 2, 3]);
 * // => 1
 *
 * _.head([]);
 * // => undefined
 */
function head(array) {
  return (array && array.length) ? array[0] : undefined;
}

module.exports = head;


/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

var basePick = __webpack_require__(426),
    flatRest = __webpack_require__(431);

/**
 * Creates an object composed of the picked `object` properties.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {...(string|string[])} [paths] The property paths to pick.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'a': 1, 'b': '2', 'c': 3 };
 *
 * _.pick(object, ['a', 'c']);
 * // => { 'a': 1, 'c': 3 }
 */
var pick = flatRest(function(object, paths) {
  return object == null ? {} : basePick(object, paths);
});

module.exports = pick;


/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(179);
// Written in this round about way for babel-transform-imports


/* harmony default export */ __webpack_exports__["a"] = (react_router_es_withRouter__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);

/***/ }),

/***/ 250:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 251:
/***/ (function(module, exports) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),

/***/ 252:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getStringLabelKey;

var _constants = __webpack_require__(166);

function getStringLabelKey(labelKey) {
  return typeof labelKey === 'string' ? labelKey : _constants.DEFAULT_LABELKEY;
}

/***/ }),

/***/ 253:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184),
    stackClear = __webpack_require__(353),
    stackDelete = __webpack_require__(354),
    stackGet = __webpack_require__(355),
    stackHas = __webpack_require__(356),
    stackSet = __webpack_require__(357);

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  var data = this.__data__ = new ListCache(entries);
  this.size = data.size;
}

// Add methods to `Stack`.
Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;

module.exports = Stack;


/***/ }),

/***/ 254:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

module.exports = toSource;


/***/ }),

/***/ 255:
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(374),
    arraySome = __webpack_require__(256),
    cacheHas = __webpack_require__(377);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Check that cyclic values are equal.
  var arrStacked = stack.get(array);
  var othStacked = stack.get(other);
  if (arrStacked && othStacked) {
    return arrStacked == other && othStacked == array;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

module.exports = equalArrays;


/***/ }),

/***/ 256:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;


/***/ }),

/***/ 257:
/***/ (function(module, exports) {

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;


/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var root = __webpack_require__(150),
    stubFalse = __webpack_require__(391);

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(230)(module)))

/***/ }),

/***/ 259:
/***/ (function(module, exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(392),
    baseUnary = __webpack_require__(393),
    nodeUtil = __webpack_require__(394);

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;


/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157);

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;


/***/ }),

/***/ 261:
/***/ (function(module, exports) {

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

module.exports = matchesStrictComparable;


/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var baseToString = __webpack_require__(412);

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;


/***/ }),

/***/ 263:
/***/ (function(module, exports, __webpack_require__) {

var baseHasIn = __webpack_require__(414),
    hasPath = __webpack_require__(415);

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

module.exports = hasIn;


/***/ }),

/***/ 264:
/***/ (function(module, exports) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;


/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = stripDiacritics;

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Taken from: http://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript/18391901#18391901
 */

/* eslint-disable max-len */
var map = [{
  base: 'A',
  letters: "A\u24B6\uFF21\xC0\xC1\xC2\u1EA6\u1EA4\u1EAA\u1EA8\xC3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\xC4\u01DE\u1EA2\xC5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F"
}, {
  base: 'AA',
  letters: "\uA732"
}, {
  base: 'AE',
  letters: "\xC6\u01FC\u01E2"
}, {
  base: 'AO',
  letters: "\uA734"
}, {
  base: 'AU',
  letters: "\uA736"
}, {
  base: 'AV',
  letters: "\uA738\uA73A"
}, {
  base: 'AY',
  letters: "\uA73C"
}, {
  base: 'B',
  letters: "B\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181"
}, {
  base: 'C',
  letters: "C\u24B8\uFF23\u0106\u0108\u010A\u010C\xC7\u1E08\u0187\u023B\uA73E"
}, {
  base: 'D',
  letters: "D\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\xD0"
}, {
  base: 'DZ',
  letters: "\u01F1\u01C4"
}, {
  base: 'Dz',
  letters: "\u01F2\u01C5"
}, {
  base: 'E',
  letters: "E\u24BA\uFF25\xC8\xC9\xCA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\xCB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E"
}, {
  base: 'F',
  letters: "F\u24BB\uFF26\u1E1E\u0191\uA77B"
}, {
  base: 'G',
  letters: "G\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E"
}, {
  base: 'H',
  letters: "H\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D"
}, {
  base: 'I',
  letters: "I\u24BE\uFF29\xCC\xCD\xCE\u0128\u012A\u012C\u0130\xCF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197"
}, {
  base: 'J',
  letters: "J\u24BF\uFF2A\u0134\u0248"
}, {
  base: 'K',
  letters: "K\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2"
}, {
  base: 'L',
  letters: "L\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780"
}, {
  base: 'LJ',
  letters: "\u01C7"
}, {
  base: 'Lj',
  letters: "\u01C8"
}, {
  base: 'M',
  letters: "M\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C"
}, {
  base: 'N',
  letters: "N\u24C3\uFF2E\u01F8\u0143\xD1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4"
}, {
  base: 'NJ',
  letters: "\u01CA"
}, {
  base: 'Nj',
  letters: "\u01CB"
}, {
  base: 'O',
  letters: "O\u24C4\uFF2F\xD2\xD3\xD4\u1ED2\u1ED0\u1ED6\u1ED4\xD5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\xD6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\xD8\u01FE\u0186\u019F\uA74A\uA74C"
}, {
  base: 'OI',
  letters: "\u01A2"
}, {
  base: 'OO',
  letters: "\uA74E"
}, {
  base: 'OU',
  letters: "\u0222"
}, {
  base: 'OE',
  letters: "\x8C\u0152"
}, {
  base: 'oe',
  letters: "\x9C\u0153"
}, {
  base: 'P',
  letters: "P\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754"
}, {
  base: 'Q',
  letters: "Q\u24C6\uFF31\uA756\uA758\u024A"
}, {
  base: 'R',
  letters: "R\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782"
}, {
  base: 'S',
  letters: "S\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784"
}, {
  base: 'T',
  letters: "T\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786"
}, {
  base: 'TZ',
  letters: "\uA728"
}, {
  base: 'U',
  letters: "U\u24CA\uFF35\xD9\xDA\xDB\u0168\u1E78\u016A\u1E7A\u016C\xDC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244"
}, {
  base: 'V',
  letters: "V\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245"
}, {
  base: 'VY',
  letters: "\uA760"
}, {
  base: 'W',
  letters: "W\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72"
}, {
  base: 'X',
  letters: "X\u24CD\uFF38\u1E8A\u1E8C"
}, {
  base: 'Y',
  letters: "Y\u24CE\uFF39\u1EF2\xDD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE"
}, {
  base: 'Z',
  letters: "Z\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762"
}, {
  base: 'a',
  letters: "a\u24D0\uFF41\u1E9A\xE0\xE1\xE2\u1EA7\u1EA5\u1EAB\u1EA9\xE3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\xE4\u01DF\u1EA3\xE5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250"
}, {
  base: 'aa',
  letters: "\uA733"
}, {
  base: 'ae',
  letters: "\xE6\u01FD\u01E3"
}, {
  base: 'ao',
  letters: "\uA735"
}, {
  base: 'au',
  letters: "\uA737"
}, {
  base: 'av',
  letters: "\uA739\uA73B"
}, {
  base: 'ay',
  letters: "\uA73D"
}, {
  base: 'b',
  letters: "b\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253"
}, {
  base: 'c',
  letters: "c\u24D2\uFF43\u0107\u0109\u010B\u010D\xE7\u1E09\u0188\u023C\uA73F\u2184"
}, {
  base: 'd',
  letters: "d\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A"
}, {
  base: 'dz',
  letters: "\u01F3\u01C6"
}, {
  base: 'e',
  letters: "e\u24D4\uFF45\xE8\xE9\xEA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\xEB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD"
}, {
  base: 'f',
  letters: "f\u24D5\uFF46\u1E1F\u0192\uA77C"
}, {
  base: 'g',
  letters: "g\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F"
}, {
  base: 'h',
  letters: "h\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265"
}, {
  base: 'hv',
  letters: "\u0195"
}, {
  base: 'i',
  letters: "i\u24D8\uFF49\xEC\xED\xEE\u0129\u012B\u012D\xEF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131"
}, {
  base: 'j',
  letters: "j\u24D9\uFF4A\u0135\u01F0\u0249"
}, {
  base: 'k',
  letters: "k\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3"
}, {
  base: 'l',
  letters: "l\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747"
}, {
  base: 'lj',
  letters: "\u01C9"
}, {
  base: 'm',
  letters: "m\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F"
}, {
  base: 'n',
  letters: "n\u24DD\uFF4E\u01F9\u0144\xF1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5"
}, {
  base: 'nj',
  letters: "\u01CC"
}, {
  base: 'o',
  letters: "o\u24DE\uFF4F\xF2\xF3\xF4\u1ED3\u1ED1\u1ED7\u1ED5\xF5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\xF6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\xF8\u01FF\u0254\uA74B\uA74D\u0275"
}, {
  base: 'oi',
  letters: "\u01A3"
}, {
  base: 'ou',
  letters: "\u0223"
}, {
  base: 'oo',
  letters: "\uA74F"
}, {
  base: 'p',
  letters: "p\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755"
}, {
  base: 'q',
  letters: "q\u24E0\uFF51\u024B\uA757\uA759"
}, {
  base: 'r',
  letters: "r\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783"
}, {
  base: 's',
  letters: "s\u24E2\uFF53\xDF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B"
}, {
  base: 't',
  letters: "t\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787"
}, {
  base: 'tz',
  letters: "\uA729"
}, {
  base: 'u',
  letters: "u\u24E4\uFF55\xF9\xFA\xFB\u0169\u1E79\u016B\u1E7B\u016D\xFC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289"
}, {
  base: 'v',
  letters: "v\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C"
}, {
  base: 'vy',
  letters: "\uA761"
}, {
  base: 'w',
  letters: "w\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73"
}, {
  base: 'x',
  letters: "x\u24E7\uFF58\u1E8B\u1E8D"
}, {
  base: 'y',
  letters: "y\u24E8\uFF59\u1EF3\xFD\u0177\u1EF9\u0233\u1E8F\xFF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF"
}, {
  base: 'z',
  letters: "z\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763"
}];
/* eslint-enable max-len */

var diacriticsMap = {};

for (var ii = 0; ii < map.length; ii++) {
  var letters = map[ii].letters;

  for (var jj = 0; jj < letters.length; jj++) {
    diacriticsMap[letters[jj]] = map[ii].base;
  }
} // "what?" version ... http://jsperf.com/diacritics/12


function stripDiacritics(str) {
  return str.replace(/[\u0300-\u036F]/g, '') // Remove combining diacritics

  /* eslint-disable-next-line no-control-regex */
  .replace(/[^\u0000-\u007E]/g, function (a) {
    return diacriticsMap[a] || a;
  });
}

/***/ }),

/***/ 266:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160);

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

module.exports = defineProperty;


/***/ }),

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _createChainableTypeChecker = __webpack_require__(542);

var _createChainableTypeChecker2 = _interopRequireDefault(_createChainableTypeChecker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function validate(props, propName, componentName, location, propFullName) {
  var propValue = props[propName];
  var propType = typeof propValue === 'undefined' ? 'undefined' : _typeof(propValue);

  if (_react2.default.isValidElement(propValue)) {
    return new Error('Invalid ' + location + ' `' + propFullName + '` of type ReactElement ' + ('supplied to `' + componentName + '`, expected a ReactComponent or a ') + 'DOMElement. You can usually obtain a ReactComponent or DOMElement ' + 'from a ReactElement by attaching a ref to it.');
  }

  if ((propType !== 'object' || typeof propValue.render !== 'function') && propValue.nodeType !== 1) {
    return new Error('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected a ReactComponent or a ') + 'DOMElement.');
  }

  return null;
}

exports.default = (0, _createChainableTypeChecker2.default)(validate);
module.exports = exports['default'];

/***/ }),

/***/ 268:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (componentOrElement) {
  return (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(componentOrElement));
};

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ownerDocument = __webpack_require__(544);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports['default'];

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var slice = Array.prototype.slice;
var isArgs = __webpack_require__(270);

var origKeys = Object.keys;
var keysShim = origKeys ? function keys(o) { return origKeys(o); } : __webpack_require__(443);

var originalKeys = Object.keys;

keysShim.shim = function shimObjectKeys() {
	if (Object.keys) {
		var keysWorksWithArguments = (function () {
			// Safari 5.0 bug
			var args = Object.keys(arguments);
			return args && args.length === arguments.length;
		}(1, 2));
		if (!keysWorksWithArguments) {
			Object.keys = function keys(object) { // eslint-disable-line func-name-matching
				if (isArgs(object)) {
					return originalKeys(slice.call(object));
				}
				return originalKeys(object);
			};
		}
	} else {
		Object.keys = keysShim;
	}
	return Object.keys || keysShim;
};

module.exports = keysShim;


/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

module.exports = function isArguments(value) {
	var str = toStr.call(value);
	var isArgs = str === '[object Arguments]';
	if (!isArgs) {
		isArgs = str !== '[object Array]' &&
			value !== null &&
			typeof value === 'object' &&
			typeof value.length === 'number' &&
			value.length >= 0 &&
			toStr.call(value.callee) === '[object Function]';
	}
	return isArgs;
};


/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var GetIntrinsic = __webpack_require__(272);

var callBind = __webpack_require__(224);

var $indexOf = callBind(GetIntrinsic('String.prototype.indexOf'));

module.exports = function callBoundIntrinsic(name, allowMissing) {
	var intrinsic = GetIntrinsic(name, !!allowMissing);
	if (typeof intrinsic === 'function' && $indexOf(name, '.prototype.') > -1) {
		return callBind(intrinsic);
	}
	return intrinsic;
};


/***/ }),

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* globals
	AggregateError,
	SharedArrayBuffer,
*/

var undefined;

var $SyntaxError = SyntaxError;
var $Function = Function;
var $TypeError = TypeError;

// eslint-disable-next-line consistent-return
var getEvalledConstructor = function (expressionSyntax) {
	try {
		return $Function('"use strict"; return (' + expressionSyntax + ').constructor;')();
	} catch (e) {}
};

var $gOPD = Object.getOwnPropertyDescriptor;
if ($gOPD) {
	try {
		$gOPD({}, '');
	} catch (e) {
		$gOPD = null; // this is IE 8, which has a broken gOPD
	}
}

var throwTypeError = function () {
	throw new $TypeError();
};
var ThrowTypeError = $gOPD
	? (function () {
		try {
			// eslint-disable-next-line no-unused-expressions, no-caller, no-restricted-properties
			arguments.callee; // IE 8 does not throw here
			return throwTypeError;
		} catch (calleeThrows) {
			try {
				// IE 8 throws on Object.getOwnPropertyDescriptor(arguments, '')
				return $gOPD(arguments, 'callee').get;
			} catch (gOPDthrows) {
				return throwTypeError;
			}
		}
	}())
	: throwTypeError;

var hasSymbols = __webpack_require__(273)();

var getProto = Object.getPrototypeOf || function (x) { return x.__proto__; }; // eslint-disable-line no-proto

var needsEval = {};

var TypedArray = typeof Uint8Array === 'undefined' ? undefined : getProto(Uint8Array);

var INTRINSICS = {
	'%AggregateError%': typeof AggregateError === 'undefined' ? undefined : AggregateError,
	'%Array%': Array,
	'%ArrayBuffer%': typeof ArrayBuffer === 'undefined' ? undefined : ArrayBuffer,
	'%ArrayIteratorPrototype%': hasSymbols ? getProto([][Symbol.iterator]()) : undefined,
	'%AsyncFromSyncIteratorPrototype%': undefined,
	'%AsyncFunction%': needsEval,
	'%AsyncGenerator%': needsEval,
	'%AsyncGeneratorFunction%': needsEval,
	'%AsyncIteratorPrototype%': needsEval,
	'%Atomics%': typeof Atomics === 'undefined' ? undefined : Atomics,
	'%BigInt%': typeof BigInt === 'undefined' ? undefined : BigInt,
	'%Boolean%': Boolean,
	'%DataView%': typeof DataView === 'undefined' ? undefined : DataView,
	'%Date%': Date,
	'%decodeURI%': decodeURI,
	'%decodeURIComponent%': decodeURIComponent,
	'%encodeURI%': encodeURI,
	'%encodeURIComponent%': encodeURIComponent,
	'%Error%': Error,
	'%eval%': eval, // eslint-disable-line no-eval
	'%EvalError%': EvalError,
	'%Float32Array%': typeof Float32Array === 'undefined' ? undefined : Float32Array,
	'%Float64Array%': typeof Float64Array === 'undefined' ? undefined : Float64Array,
	'%FinalizationRegistry%': typeof FinalizationRegistry === 'undefined' ? undefined : FinalizationRegistry,
	'%Function%': $Function,
	'%GeneratorFunction%': needsEval,
	'%Int8Array%': typeof Int8Array === 'undefined' ? undefined : Int8Array,
	'%Int16Array%': typeof Int16Array === 'undefined' ? undefined : Int16Array,
	'%Int32Array%': typeof Int32Array === 'undefined' ? undefined : Int32Array,
	'%isFinite%': isFinite,
	'%isNaN%': isNaN,
	'%IteratorPrototype%': hasSymbols ? getProto(getProto([][Symbol.iterator]())) : undefined,
	'%JSON%': typeof JSON === 'object' ? JSON : undefined,
	'%Map%': typeof Map === 'undefined' ? undefined : Map,
	'%MapIteratorPrototype%': typeof Map === 'undefined' || !hasSymbols ? undefined : getProto(new Map()[Symbol.iterator]()),
	'%Math%': Math,
	'%Number%': Number,
	'%Object%': Object,
	'%parseFloat%': parseFloat,
	'%parseInt%': parseInt,
	'%Promise%': typeof Promise === 'undefined' ? undefined : Promise,
	'%Proxy%': typeof Proxy === 'undefined' ? undefined : Proxy,
	'%RangeError%': RangeError,
	'%ReferenceError%': ReferenceError,
	'%Reflect%': typeof Reflect === 'undefined' ? undefined : Reflect,
	'%RegExp%': RegExp,
	'%Set%': typeof Set === 'undefined' ? undefined : Set,
	'%SetIteratorPrototype%': typeof Set === 'undefined' || !hasSymbols ? undefined : getProto(new Set()[Symbol.iterator]()),
	'%SharedArrayBuffer%': typeof SharedArrayBuffer === 'undefined' ? undefined : SharedArrayBuffer,
	'%String%': String,
	'%StringIteratorPrototype%': hasSymbols ? getProto(''[Symbol.iterator]()) : undefined,
	'%Symbol%': hasSymbols ? Symbol : undefined,
	'%SyntaxError%': $SyntaxError,
	'%ThrowTypeError%': ThrowTypeError,
	'%TypedArray%': TypedArray,
	'%TypeError%': $TypeError,
	'%Uint8Array%': typeof Uint8Array === 'undefined' ? undefined : Uint8Array,
	'%Uint8ClampedArray%': typeof Uint8ClampedArray === 'undefined' ? undefined : Uint8ClampedArray,
	'%Uint16Array%': typeof Uint16Array === 'undefined' ? undefined : Uint16Array,
	'%Uint32Array%': typeof Uint32Array === 'undefined' ? undefined : Uint32Array,
	'%URIError%': URIError,
	'%WeakMap%': typeof WeakMap === 'undefined' ? undefined : WeakMap,
	'%WeakRef%': typeof WeakRef === 'undefined' ? undefined : WeakRef,
	'%WeakSet%': typeof WeakSet === 'undefined' ? undefined : WeakSet
};

var doEval = function doEval(name) {
	var value;
	if (name === '%AsyncFunction%') {
		value = getEvalledConstructor('async function () {}');
	} else if (name === '%GeneratorFunction%') {
		value = getEvalledConstructor('function* () {}');
	} else if (name === '%AsyncGeneratorFunction%') {
		value = getEvalledConstructor('async function* () {}');
	} else if (name === '%AsyncGenerator%') {
		var fn = doEval('%AsyncGeneratorFunction%');
		if (fn) {
			value = fn.prototype;
		}
	} else if (name === '%AsyncIteratorPrototype%') {
		var gen = doEval('%AsyncGenerator%');
		if (gen) {
			value = getProto(gen.prototype);
		}
	}

	INTRINSICS[name] = value;

	return value;
};

var LEGACY_ALIASES = {
	'%ArrayBufferPrototype%': ['ArrayBuffer', 'prototype'],
	'%ArrayPrototype%': ['Array', 'prototype'],
	'%ArrayProto_entries%': ['Array', 'prototype', 'entries'],
	'%ArrayProto_forEach%': ['Array', 'prototype', 'forEach'],
	'%ArrayProto_keys%': ['Array', 'prototype', 'keys'],
	'%ArrayProto_values%': ['Array', 'prototype', 'values'],
	'%AsyncFunctionPrototype%': ['AsyncFunction', 'prototype'],
	'%AsyncGenerator%': ['AsyncGeneratorFunction', 'prototype'],
	'%AsyncGeneratorPrototype%': ['AsyncGeneratorFunction', 'prototype', 'prototype'],
	'%BooleanPrototype%': ['Boolean', 'prototype'],
	'%DataViewPrototype%': ['DataView', 'prototype'],
	'%DatePrototype%': ['Date', 'prototype'],
	'%ErrorPrototype%': ['Error', 'prototype'],
	'%EvalErrorPrototype%': ['EvalError', 'prototype'],
	'%Float32ArrayPrototype%': ['Float32Array', 'prototype'],
	'%Float64ArrayPrototype%': ['Float64Array', 'prototype'],
	'%FunctionPrototype%': ['Function', 'prototype'],
	'%Generator%': ['GeneratorFunction', 'prototype'],
	'%GeneratorPrototype%': ['GeneratorFunction', 'prototype', 'prototype'],
	'%Int8ArrayPrototype%': ['Int8Array', 'prototype'],
	'%Int16ArrayPrototype%': ['Int16Array', 'prototype'],
	'%Int32ArrayPrototype%': ['Int32Array', 'prototype'],
	'%JSONParse%': ['JSON', 'parse'],
	'%JSONStringify%': ['JSON', 'stringify'],
	'%MapPrototype%': ['Map', 'prototype'],
	'%NumberPrototype%': ['Number', 'prototype'],
	'%ObjectPrototype%': ['Object', 'prototype'],
	'%ObjProto_toString%': ['Object', 'prototype', 'toString'],
	'%ObjProto_valueOf%': ['Object', 'prototype', 'valueOf'],
	'%PromisePrototype%': ['Promise', 'prototype'],
	'%PromiseProto_then%': ['Promise', 'prototype', 'then'],
	'%Promise_all%': ['Promise', 'all'],
	'%Promise_reject%': ['Promise', 'reject'],
	'%Promise_resolve%': ['Promise', 'resolve'],
	'%RangeErrorPrototype%': ['RangeError', 'prototype'],
	'%ReferenceErrorPrototype%': ['ReferenceError', 'prototype'],
	'%RegExpPrototype%': ['RegExp', 'prototype'],
	'%SetPrototype%': ['Set', 'prototype'],
	'%SharedArrayBufferPrototype%': ['SharedArrayBuffer', 'prototype'],
	'%StringPrototype%': ['String', 'prototype'],
	'%SymbolPrototype%': ['Symbol', 'prototype'],
	'%SyntaxErrorPrototype%': ['SyntaxError', 'prototype'],
	'%TypedArrayPrototype%': ['TypedArray', 'prototype'],
	'%TypeErrorPrototype%': ['TypeError', 'prototype'],
	'%Uint8ArrayPrototype%': ['Uint8Array', 'prototype'],
	'%Uint8ClampedArrayPrototype%': ['Uint8ClampedArray', 'prototype'],
	'%Uint16ArrayPrototype%': ['Uint16Array', 'prototype'],
	'%Uint32ArrayPrototype%': ['Uint32Array', 'prototype'],
	'%URIErrorPrototype%': ['URIError', 'prototype'],
	'%WeakMapPrototype%': ['WeakMap', 'prototype'],
	'%WeakSetPrototype%': ['WeakSet', 'prototype']
};

var bind = __webpack_require__(223);
var hasOwn = __webpack_require__(447);
var $concat = bind.call(Function.call, Array.prototype.concat);
var $spliceApply = bind.call(Function.apply, Array.prototype.splice);
var $replace = bind.call(Function.call, String.prototype.replace);
var $strSlice = bind.call(Function.call, String.prototype.slice);

/* adapted from https://github.com/lodash/lodash/blob/4.17.15/dist/lodash.js#L6735-L6744 */
var rePropName = /[^%.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|%$))/g;
var reEscapeChar = /\\(\\)?/g; /** Used to match backslashes in property paths. */
var stringToPath = function stringToPath(string) {
	var first = $strSlice(string, 0, 1);
	var last = $strSlice(string, -1);
	if (first === '%' && last !== '%') {
		throw new $SyntaxError('invalid intrinsic syntax, expected closing `%`');
	} else if (last === '%' && first !== '%') {
		throw new $SyntaxError('invalid intrinsic syntax, expected opening `%`');
	}
	var result = [];
	$replace(string, rePropName, function (match, number, quote, subString) {
		result[result.length] = quote ? $replace(subString, reEscapeChar, '$1') : number || match;
	});
	return result;
};
/* end adaptation */

var getBaseIntrinsic = function getBaseIntrinsic(name, allowMissing) {
	var intrinsicName = name;
	var alias;
	if (hasOwn(LEGACY_ALIASES, intrinsicName)) {
		alias = LEGACY_ALIASES[intrinsicName];
		intrinsicName = '%' + alias[0] + '%';
	}

	if (hasOwn(INTRINSICS, intrinsicName)) {
		var value = INTRINSICS[intrinsicName];
		if (value === needsEval) {
			value = doEval(intrinsicName);
		}
		if (typeof value === 'undefined' && !allowMissing) {
			throw new $TypeError('intrinsic ' + name + ' exists, but is not available. Please file an issue!');
		}

		return {
			alias: alias,
			name: intrinsicName,
			value: value
		};
	}

	throw new $SyntaxError('intrinsic ' + name + ' does not exist!');
};

module.exports = function GetIntrinsic(name, allowMissing) {
	if (typeof name !== 'string' || name.length === 0) {
		throw new $TypeError('intrinsic name must be a non-empty string');
	}
	if (arguments.length > 1 && typeof allowMissing !== 'boolean') {
		throw new $TypeError('"allowMissing" argument must be a boolean');
	}

	var parts = stringToPath(name);
	var intrinsicBaseName = parts.length > 0 ? parts[0] : '';

	var intrinsic = getBaseIntrinsic('%' + intrinsicBaseName + '%', allowMissing);
	var intrinsicRealName = intrinsic.name;
	var value = intrinsic.value;
	var skipFurtherCaching = false;

	var alias = intrinsic.alias;
	if (alias) {
		intrinsicBaseName = alias[0];
		$spliceApply(parts, $concat([0, 1], alias));
	}

	for (var i = 1, isOwn = true; i < parts.length; i += 1) {
		var part = parts[i];
		var first = $strSlice(part, 0, 1);
		var last = $strSlice(part, -1);
		if (
			(
				(first === '"' || first === "'" || first === '`')
				|| (last === '"' || last === "'" || last === '`')
			)
			&& first !== last
		) {
			throw new $SyntaxError('property names with quotes must have matching quotes');
		}
		if (part === 'constructor' || !isOwn) {
			skipFurtherCaching = true;
		}

		intrinsicBaseName += '.' + part;
		intrinsicRealName = '%' + intrinsicBaseName + '%';

		if (hasOwn(INTRINSICS, intrinsicRealName)) {
			value = INTRINSICS[intrinsicRealName];
		} else if (value != null) {
			if (!(part in value)) {
				if (!allowMissing) {
					throw new $TypeError('base intrinsic for ' + name + ' exists, but the property is not available.');
				}
				return void undefined;
			}
			if ($gOPD && (i + 1) >= parts.length) {
				var desc = $gOPD(value, part);
				isOwn = !!desc;

				// By convention, when a data property is converted to an accessor
				// property to emulate a data property that does not suffer from
				// the override mistake, that accessor's getter is marked with
				// an `originalValue` property. Here, when we detect this, we
				// uphold the illusion by pretending to see that original data
				// property, i.e., returning the value rather than the getter
				// itself.
				if (isOwn && 'get' in desc && !('originalValue' in desc.get)) {
					value = desc.get;
				} else {
					value = value[part];
				}
			} else {
				isOwn = hasOwn(value, part);
				value = value[part];
			}

			if (isOwn && !skipFurtherCaching) {
				INTRINSICS[intrinsicRealName] = value;
			}
		}
	}
	return value;
};


/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var origSymbol = global.Symbol;
var hasSymbolSham = __webpack_require__(445);

module.exports = function hasNativeSymbols() {
	if (typeof origSymbol !== 'function') { return false; }
	if (typeof Symbol !== 'function') { return false; }
	if (typeof origSymbol('foo') !== 'symbol') { return false; }
	if (typeof Symbol('bar') !== 'symbol') { return false; }

	return hasSymbolSham();
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var numberIsNaN = function (value) {
	return value !== value;
};

module.exports = function is(a, b) {
	if (a === 0 && b === 0) {
		return 1 / a === 1 / b;
	}
	if (a === b) {
		return true;
	}
	if (numberIsNaN(a) && numberIsNaN(b)) {
		return true;
	}
	return false;
};



/***/ }),

/***/ 275:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(274);

module.exports = function getPolyfill() {
	return typeof Object.is === 'function' ? Object.is : implementation;
};


/***/ }),

/***/ 276:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $Object = Object;
var $TypeError = TypeError;

module.exports = function flags() {
	if (this != null && this !== $Object(this)) {
		throw new $TypeError('RegExp.prototype.flags getter called on non-object');
	}
	var result = '';
	if (this.global) {
		result += 'g';
	}
	if (this.ignoreCase) {
		result += 'i';
	}
	if (this.multiline) {
		result += 'm';
	}
	if (this.dotAll) {
		result += 's';
	}
	if (this.unicode) {
		result += 'u';
	}
	if (this.sticky) {
		result += 'y';
	}
	return result;
};


/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__(276);

var supportsDescriptors = __webpack_require__(168).supportsDescriptors;
var $gOPD = Object.getOwnPropertyDescriptor;
var $TypeError = TypeError;

module.exports = function getPolyfill() {
	if (!supportsDescriptors) {
		throw new $TypeError('RegExp.prototype.flags requires a true ES5 environment that supports property descriptors');
	}
	if ((/a/mig).flags === 'gim') {
		var descriptor = $gOPD(RegExp.prototype, 'flags');
		if (descriptor && typeof descriptor.get === 'function' && typeof (/a/).dotAll === 'boolean') {
			return descriptor.get;
		}
	}
	return implementation;
};


/***/ }),

/***/ 278:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = void 0;

var _default = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 279:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.withContext = void 0;

var _noop = _interopRequireDefault(__webpack_require__(222));

var _pick = _interopRequireDefault(__webpack_require__(243));

var _createReactContext = _interopRequireDefault(__webpack_require__(321));

var _react = _interopRequireDefault(__webpack_require__(1));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TypeaheadContext = (0, _createReactContext["default"])({
  activeIndex: -1,
  hintText: '',
  initialItem: null,
  isOnlyResult: false,
  onActiveItemChange: _noop["default"],
  onAdd: _noop["default"],
  onInitialItemChange: _noop["default"],
  onMenuItemClick: _noop["default"],
  selectHintOnEnter: false
});

var withContext = function withContext(Component, values) {
  // Note: Use a class instead of function component to support refs.

  /* eslint-disable-next-line react/prefer-stateless-function */
  return (
    /*#__PURE__*/
    function (_React$Component) {
      _inherits(_class, _React$Component);

      function _class() {
        _classCallCheck(this, _class);

        return _possibleConstructorReturn(this, _getPrototypeOf(_class).apply(this, arguments));
      }

      _createClass(_class, [{
        key: "render",
        value: function render() {
          var _this = this;

          return _react["default"].createElement(TypeaheadContext.Consumer, null, function (context) {
            return _react["default"].createElement(Component, _extends({}, _this.props, (0, _pick["default"])(context, values)));
          });
        }
      }]);

      return _class;
    }(_react["default"].Component)
  );
};

exports.withContext = withContext;
var _default = TypeaheadContext;
exports["default"] = _default;

/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.BaseMenuItem = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _noop = _interopRequireDefault(__webpack_require__(222));

var _react = _interopRequireDefault(__webpack_require__(1));

var _menuItemContainer = _interopRequireDefault(__webpack_require__(331));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BaseMenuItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BaseMenuItem, _React$Component);

  function BaseMenuItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, BaseMenuItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(BaseMenuItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_handleClick", function (e) {
      var _this$props = _this.props,
          disabled = _this$props.disabled,
          onClick = _this$props.onClick;
      e.preventDefault();
      !disabled && onClick(e);
    });

    return _this;
  }

  _createClass(BaseMenuItem, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          active = _this$props2.active,
          children = _this$props2.children,
          className = _this$props2.className,
          disabled = _this$props2.disabled,
          onClick = _this$props2.onClick,
          onMouseDown = _this$props2.onMouseDown,
          props = _objectWithoutProperties(_this$props2, ["active", "children", "className", "disabled", "onClick", "onMouseDown"]);

      var conditionalClassNames = {
        active: active,
        disabled: disabled
      };
      return (
        /* eslint-disable jsx-a11y/anchor-is-valid */
        _react["default"].createElement("li", _extends({}, props, {
          className: (0, _classnames["default"])(conditionalClassNames, className)
        }), _react["default"].createElement("a", {
          className: (0, _classnames["default"])('dropdown-item', conditionalClassNames),
          href: "#",
          onClick: this._handleClick,
          onMouseDown: onMouseDown
        }, children))
        /* eslint-enable jsx-a11y/anchor-is-valid */

      );
    }
  }]);

  return BaseMenuItem;
}(_react["default"].Component);

exports.BaseMenuItem = BaseMenuItem;
BaseMenuItem.defaultProps = {
  onClick: _noop["default"]
};

var _default = (0, _menuItemContainer["default"])(BaseMenuItem);

exports["default"] = _default;

/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    getPrototype = __webpack_require__(346),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var objectTag = '[object Object]';

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to infer the `Object` constructor. */
var objectCtorString = funcToString.call(Object);

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * @static
 * @memberOf _
 * @since 0.8.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  if (!isObjectLike(value) || baseGetTag(value) != objectTag) {
    return false;
  }
  var proto = getPrototype(value);
  if (proto === null) {
    return true;
  }
  var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
    funcToString.call(Ctor) == objectCtorString;
}

module.exports = isPlainObject;


/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _debounce = _interopRequireDefault(__webpack_require__(475));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes2 = __webpack_require__(315);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DEFAULT_DELAY_MS = 200;
/**
 * HoC that encapsulates common behavior and functionality for doing
 * asynchronous searches, including:
 *
 *  - Debouncing user input
 *  - Optional query caching
 *  - Search prompt and empty results behaviors
 */

var asyncContainer = function asyncContainer(Typeahead) {
  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_cache", {});

      _defineProperty(_assertThisInitialized(_this), "_query", _this.props.defaultInputValue || '');

      _defineProperty(_assertThisInitialized(_this), "_getEmptyLabel", function () {
        var _this$props = _this.props,
            emptyLabel = _this$props.emptyLabel,
            isLoading = _this$props.isLoading,
            promptText = _this$props.promptText,
            searchText = _this$props.searchText;

        if (!_this._query.length) {
          return promptText;
        }

        if (isLoading) {
          return searchText;
        }

        return emptyLabel;
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInputChange", function (query, e) {
        _this.props.onInputChange && _this.props.onInputChange(query, e);

        _this._handleSearchDebounced(query);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSearch", function (query) {
        _this._query = query;
        var _this$props2 = _this.props,
            minLength = _this$props2.minLength,
            onSearch = _this$props2.onSearch,
            useCache = _this$props2.useCache;

        if (!query || minLength && query.length < minLength) {
          return;
        } // Use cached results, if applicable.


        if (useCache && _this._cache[query]) {
          // Re-render the component with the cached results.
          _this.forceUpdate();

          return;
        } // Perform the search.


        onSearch(query);
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this._handleSearchDebounced = (0, _debounce["default"])(this._handleSearch, this.props.delay);
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        if (prevProps.isLoading && this.props.useCache) {
          this._cache[this._query] = this.props.options;
        }
      }
    }, {
      key: "componentWillUnmount",
      value: function componentWillUnmount() {
        this._cache = {};
        this._query = '';

        this._handleSearchDebounced.cancel();
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var _this$props3 = this.props,
            options = _this$props3.options,
            useCache = _this$props3.useCache,
            props = _objectWithoutProperties(_this$props3, ["options", "useCache"]);

        var cachedQuery = this._cache[this._query]; // Disable custom selections during a search unless `allowNew` is a
        // function.

        var allowNew = typeof props.allowNew === 'function' ? props.allowNew : props.allowNew && !props.isLoading;
        return _react["default"].createElement(Typeahead, _extends({}, props, {
          allowNew: allowNew,
          emptyLabel: this._getEmptyLabel(),
          onInputChange: this._handleInputChange,
          options: useCache && cachedQuery ? cachedQuery : options,
          ref: function ref(instance) {
            return _this2._instance = instance;
          }
        }));
      }
      /**
       * Make the component instance available.
       */

    }, {
      key: "getInstance",
      value: function getInstance() {
        return this._instance;
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  WrappedTypeahead.displayName = "AsyncContainer(".concat((0, _utils.getDisplayName)(Typeahead), ")");
  WrappedTypeahead.propTypes = {
    /**
     * Delay, in milliseconds, before performing search.
     */
    delay: _propTypes["default"].number,

    /**
     * Whether or not a request is currently pending. Necessary for the
     * container to know when new results are available.
     */
    isLoading: _propTypes["default"].bool.isRequired,

    /**
     * Number of input characters that must be entered before showing results.
     */
    minLength: _propTypes["default"].number,

    /**
     * Callback to perform when the search is executed.
     */
    onSearch: _propTypes["default"].func.isRequired,

    /**
     * Options to be passed to the typeahead. Will typically be the query
     * results, but can also be initial default options.
     */
    options: _propTypes2.optionType,

    /**
     * Message displayed in the menu when there is no user input.
     */
    promptText: _propTypes["default"].node,

    /**
     * Message displayed in the menu while the request is pending.
     */
    searchText: _propTypes["default"].node,

    /**
     * Whether or not the component should cache query results.
     */
    useCache: _propTypes["default"].bool
  };
  WrappedTypeahead.defaultProps = {
    delay: DEFAULT_DELAY_MS,
    minLength: 2,
    options: [],
    promptText: 'Type to search...',
    searchText: 'Searching...',
    useCache: true
  };
  return WrappedTypeahead;
};

var _default = asyncContainer;
exports["default"] = _default;

/***/ }),

/***/ 315:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "caseSensitiveType", {
  enumerable: true,
  get: function get() {
    return _caseSensitiveType2["default"];
  }
});
Object.defineProperty(exports, "checkPropType", {
  enumerable: true,
  get: function get() {
    return _checkPropType2["default"];
  }
});
Object.defineProperty(exports, "defaultInputValueType", {
  enumerable: true,
  get: function get() {
    return _defaultInputValueType2["default"];
  }
});
Object.defineProperty(exports, "emptyLabelType", {
  enumerable: true,
  get: function get() {
    return _emptyLabelType2["default"];
  }
});
Object.defineProperty(exports, "highlightOnlyResultType", {
  enumerable: true,
  get: function get() {
    return _highlightOnlyResultType2["default"];
  }
});
Object.defineProperty(exports, "idType", {
  enumerable: true,
  get: function get() {
    return _idType2["default"];
  }
});
Object.defineProperty(exports, "ignoreDiacriticsType", {
  enumerable: true,
  get: function get() {
    return _ignoreDiacriticsType2["default"];
  }
});
Object.defineProperty(exports, "inputPropsType", {
  enumerable: true,
  get: function get() {
    return _inputPropsType2["default"];
  }
});
Object.defineProperty(exports, "labelKeyType", {
  enumerable: true,
  get: function get() {
    return _labelKeyType2["default"];
  }
});
Object.defineProperty(exports, "optionType", {
  enumerable: true,
  get: function get() {
    return _optionType2["default"];
  }
});
Object.defineProperty(exports, "selectedType", {
  enumerable: true,
  get: function get() {
    return _selectedType2["default"];
  }
});

var _caseSensitiveType2 = _interopRequireDefault(__webpack_require__(513));

var _checkPropType2 = _interopRequireDefault(__webpack_require__(514));

var _defaultInputValueType2 = _interopRequireDefault(__webpack_require__(515));

var _emptyLabelType2 = _interopRequireDefault(__webpack_require__(516));

var _highlightOnlyResultType2 = _interopRequireDefault(__webpack_require__(517));

var _idType2 = _interopRequireDefault(__webpack_require__(518));

var _ignoreDiacriticsType2 = _interopRequireDefault(__webpack_require__(519));

var _inputPropsType2 = _interopRequireDefault(__webpack_require__(520));

var _labelKeyType2 = _interopRequireDefault(__webpack_require__(521));

var _optionType2 = _interopRequireDefault(__webpack_require__(522));

var _selectedType2 = _interopRequireDefault(__webpack_require__(523));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 316:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getMatchBounds;

var _escapeStringRegexp = _interopRequireDefault(__webpack_require__(530));

var _stripDiacritics = _interopRequireDefault(__webpack_require__(265));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var CASE_INSENSITIVE = 'i';
var COMBINING_MARKS = /[\u0300-\u036F]/;

function getMatchBounds(subject, str) {
  var search = new RegExp((0, _escapeStringRegexp["default"])((0, _stripDiacritics["default"])(str)), CASE_INSENSITIVE);
  var matches = search.exec((0, _stripDiacritics["default"])(subject));

  if (!matches) {
    return null;
  }

  var start = matches.index;
  var matchLength = matches[0].length; // Account for combining marks, which changes the indices.

  if (COMBINING_MARKS.test(subject)) {
    // Starting at the beginning of the subject string, check for the number of
    // combining marks and increment the start index whenever one is found.
    for (var ii = 0; ii <= start; ii++) {
      if (COMBINING_MARKS.test(subject[ii])) {
        start += 1;
      }
    } // Similarly, increment the length of the match string if it contains a
    // combining mark.


    for (var _ii = start; _ii <= start + matchLength; _ii++) {
      if (COMBINING_MARKS.test(subject[_ii])) {
        matchLength += 1;
      }
    }
  }

  return {
    end: start + matchLength,
    start: start
  };
}

/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = isSelectable;

/**
 * Check if an input type is selectable, based on WHATWG spec.
 *
 * See:
 *  - https://stackoverflow.com/questions/21177489/selectionstart-selectionend-on-input-type-number-no-longer-allowed-in-chrome/24175357
 *  - https://html.spec.whatwg.org/multipage/input.html#do-not-apply
 */
function isSelectable(inputNode) {
  return inputNode.selectionStart != null;
}

/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _pick = _interopRequireDefault(__webpack_require__(243));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _reactDom = __webpack_require__(15);

var _ClearButton = _interopRequireDefault(__webpack_require__(319));

var _Loader = _interopRequireDefault(__webpack_require__(540));

var _Overlay = _interopRequireDefault(__webpack_require__(541));

var _TypeaheadInputMulti = _interopRequireDefault(__webpack_require__(552));

var _TypeaheadInputSingle = _interopRequireDefault(__webpack_require__(557));

var _TypeaheadMenu = _interopRequireDefault(__webpack_require__(328));

var _typeaheadContainer = _interopRequireDefault(__webpack_require__(559));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Typeahead =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Typeahead, _React$Component);

  function Typeahead() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Typeahead);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Typeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderInput", function (inputProps) {
      var Input = inputProps.multiple ? _TypeaheadInputMulti["default"] : _TypeaheadInputSingle["default"];
      return _react["default"].createElement(Input, inputProps);
    });

    _defineProperty(_assertThisInitialized(_this), "_renderAux", function () {
      var _this$props = _this.props,
          bsSize = _this$props.bsSize,
          clearButton = _this$props.clearButton,
          disabled = _this$props.disabled,
          isLoading = _this$props.isLoading,
          onClear = _this$props.onClear,
          selected = _this$props.selected;
      var content;

      if (isLoading) {
        content = _react["default"].createElement(_Loader["default"], {
          bsSize: bsSize
        });
      } else if (clearButton && !disabled && selected.length) {
        content = _react["default"].createElement(_ClearButton["default"], {
          bsSize: bsSize,
          onClick: onClear,
          onFocus: function onFocus(e) {
            // Prevent the main input from auto-focusing again.
            e.stopPropagation();
          },
          onMouseDown: _utils.preventInputBlur
        });
      }

      return content ? _react["default"].createElement("div", {
        className: (0, _classnames["default"])('rbt-aux', {
          'rbt-aux-lg': bsSize === 'large' || bsSize === 'lg'
        })
      }, content) : null;
    });

    return _this;
  }

  _createClass(Typeahead, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          bodyContainer = _this$props2.bodyContainer,
          children = _this$props2.children,
          className = _this$props2.className,
          isMenuShown = _this$props2.isMenuShown,
          menuId = _this$props2.menuId,
          renderMenu = _this$props2.renderMenu,
          results = _this$props2.results;
      var inputProps = (0, _pick["default"])(this.props, ['activeIndex', 'activeItem', 'bsSize', 'disabled', 'inputProps', 'inputRef', 'isFocused', 'isInvalid', 'isMenuShown', 'isValid', 'labelKey', 'menuId', 'multiple', 'onBlur', 'onChange', 'onFocus', 'onKeyDown', 'onRemove', 'placeholder', 'renderToken', 'selected', 'text']);
      var overlayProps = (0, _pick["default"])(this.props, ['align', 'className', 'dropup', 'flip', 'onMenuHide', 'onMenuShow', 'onMenuToggle']);
      var menuProps = (0, _pick["default"])(this.props, ['emptyLabel', 'labelKey', 'maxHeight', 'newSelectionPrefix', 'renderMenuItemChildren', 'text']);

      var auxContent = this._renderAux();

      return _react["default"].createElement("div", {
        className: (0, _classnames["default"])('rbt', 'clearfix', 'open', {
          'has-aux': !!auxContent
        }, className),
        style: {
          position: 'relative'
        },
        tabIndex: -1
      }, this._renderInput(_objectSpread({}, inputProps, {
        // Use `findDOMNode` here since it's easier and less fragile than
        // forwarding refs down to the input's container.
        // TODO: Consider using `forwardRef` when React 16.3 usage is higher.

        /* eslint-disable-next-line react/no-find-dom-node */
        ref: function ref(node) {
          return _this2._inputContainer = (0, _reactDom.findDOMNode)(node);
        }
      })), typeof children === 'function' ? children(this.props) : children, auxContent, _react["default"].createElement(_Overlay["default"], _extends({}, overlayProps, {
        container: bodyContainer ? document.body : this,
        referenceElement: this._inputContainer,
        show: isMenuShown
      }), renderMenu(results, _objectSpread({}, menuProps, {
        id: menuId
      }))), _react["default"].createElement("div", {
        "aria-atomic": true,
        "aria-live": "polite",
        className: "sr-only rbt-sr-status",
        role: "status"
      }, (0, _utils.getAccessibilityStatus)(this.props)));
    }
  }]);

  return Typeahead;
}(_react["default"].Component);

Typeahead.propTypes = {
  renderMenu: _propTypes["default"].func
};
Typeahead.defaultProps = {
  renderMenu: function renderMenu(results, menuProps) {
    return _react["default"].createElement(_TypeaheadMenu["default"], _extends({}, menuProps, {
      options: results
    }));
  }
};

var _default = (0, _typeaheadContainer["default"])(Typeahead);

exports["default"] = _default;

/***/ }),

/***/ 319:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * ClearButton
 *
 * http://getbootstrap.com/css/#helper-classes-close
 */
var ClearButton = function ClearButton(_ref) {
  var bsSize = _ref.bsSize,
      className = _ref.className,
      label = _ref.label,
      _onClick = _ref.onClick,
      props = _objectWithoutProperties(_ref, ["bsSize", "className", "label", "onClick"]);

  return _react["default"].createElement("button", _extends({}, props, {
    "aria-label": label,
    className: (0, _classnames["default"])('close', 'rbt-close', {
      'rbt-close-lg': bsSize === 'large' || bsSize === 'lg'
    }, className),
    onClick: function onClick(e) {
      e.stopPropagation();

      _onClick(e);
    },
    type: "button"
  }), _react["default"].createElement("span", {
    "aria-hidden": "true"
  }, "\xD7"), _react["default"].createElement("span", {
    className: "sr-only"
  }, label));
};

ClearButton.propTypes = {
  bsSize: _propTypes["default"].oneOf(['large', 'lg', 'small', 'sm']),
  label: _propTypes["default"].string,
  onClick: _propTypes["default"].func.isRequired
};
ClearButton.defaultProps = {
  label: 'Clear'
};
var _default = ClearButton;
exports["default"] = _default;

/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = getContainer;

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getContainer(container, defaultContainer) {
  container = typeof container === 'function' ? container() : container;
  return _reactDom2.default.findDOMNode(container) || defaultContainer;
}
module.exports = exports['default'];

/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _implementation = __webpack_require__(549);

var _implementation2 = _interopRequireDefault(_implementation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createContext || _implementation2.default;
module.exports = exports['default'];

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SIZER_STYLE = {
  height: 0,
  left: 0,
  overflow: 'scroll',
  position: 'absolute',
  top: 0,
  visibility: 'hidden',
  whiteSpace: 'pre'
};
var INPUT_PROPS_BLACKLIST = ['inputClassName', 'inputRef', 'inputStyle'];
var MIN_WIDTH = 1;

var cleanInputProps = function cleanInputProps(inputProps) {
  var cleanProps = {};
  Object.keys(inputProps).forEach(function (key) {
    if (INPUT_PROPS_BLACKLIST.indexOf(key) === -1) {
      cleanProps[key] = inputProps[key];
    }
  });
  return cleanProps;
};

var copyStyles = function copyStyles(styles, node) {
  /* eslint-disable no-param-reassign */
  node.style.fontSize = styles.fontSize;
  node.style.fontFamily = styles.fontFamily;
  node.style.fontWeight = styles.fontWeight;
  node.style.fontStyle = styles.fontStyle;
  node.style.letterSpacing = styles.letterSpacing;
  node.style.textTransform = styles.textTransform;
  /* eslint-enable no-param-reassign */
};

var AutosizeInput =
/*#__PURE__*/
function (_React$Component) {
  _inherits(AutosizeInput, _React$Component);

  function AutosizeInput() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, AutosizeInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(AutosizeInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      inputWidth: MIN_WIDTH
    });

    _defineProperty(_assertThisInitialized(_this), "_copyInputStyles", function () {
      var inputStyles = _this._input && window.getComputedStyle && window.getComputedStyle(_this._input);

      if (!inputStyles) {
        return;
      }

      copyStyles(inputStyles, _this._sizer);

      if (_this._placeHolderSizer) {
        copyStyles(inputStyles, _this._placeHolderSizer);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "_updateInputWidth", function () {
      if (!_this._sizer || _this._sizer.scrollWidth === undefined) {
        return;
      }

      _this._copyInputStyles();

      var placeholderWidth = _this._placeHolderSizer && _this._placeHolderSizer.scrollWidth || MIN_WIDTH;
      var inputWidth = Math.max(_this._sizer.scrollWidth, placeholderWidth) + 2;

      if (inputWidth !== _this.state.inputWidth) {
        _this.setState({
          inputWidth: inputWidth
        });
      }
    });

    return _this;
  }

  _createClass(AutosizeInput, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this._updateInputWidth();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      this._updateInputWidth();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          className = _this$props.className,
          defaultValue = _this$props.defaultValue,
          placeholder = _this$props.placeholder,
          value = _this$props.value;

      var wrapperStyle = _objectSpread({}, this.props.style);

      if (!wrapperStyle.display) {
        wrapperStyle.display = 'inline-block';
      }

      var inputProps = cleanInputProps(_objectSpread({}, this.props, {
        className: this.props.inputClassName,
        style: _objectSpread({}, this.props.inputStyle, {
          boxSizing: 'content-box',
          width: "".concat(this.state.inputWidth, "px")
        })
      }));
      return _react["default"].createElement("div", {
        className: className,
        style: wrapperStyle
      }, _react["default"].createElement("input", _extends({}, inputProps, {
        ref: function ref(el) {
          _this2._input = el;

          if (typeof _this2.props.inputRef === 'function') {
            _this2.props.inputRef(el);
          }
        }
      })), _react["default"].createElement("div", {
        ref: function ref(el) {
          return _this2._sizer = el;
        },
        style: SIZER_STYLE
      }, defaultValue || value || ''), placeholder ? _react["default"].createElement("div", {
        ref: function ref(el) {
          return _this2._placeHolderSizer = el;
        },
        style: SIZER_STYLE
      }, placeholder) : null);
    }
  }]);

  return AutosizeInput;
}(_react["default"].Component);

AutosizeInput.propTypes = {
  /**
   * ClassName for the input element.
   */
  inputClassName: _propTypes["default"].string,

  /**
   * Ref callback for the input element.
   */
  inputRef: _propTypes["default"].func,

  /**
   * CSS styles for the input element.
   */

  /* eslint-disable-next-line react/forbid-prop-types */
  inputStyle: _propTypes["default"].object
};
var _default = AutosizeInput;
exports["default"] = _default;

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _ClearButton = _interopRequireDefault(__webpack_require__(319));

var _tokenContainer = _interopRequireDefault(__webpack_require__(324));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Token
 *
 * Individual token component, generally displayed within the TokenizerInput
 * component, but can also be rendered on its own.
 */
var Token =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Token, _React$Component);

  function Token() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Token);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Token)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderRemoveableToken", function () {
      var _this$props = _this.props,
          active = _this$props.active,
          children = _this$props.children,
          className = _this$props.className,
          onRemove = _this$props.onRemove,
          props = _objectWithoutProperties(_this$props, ["active", "children", "className", "onRemove"]);

      return _react["default"].createElement("div", _extends({}, props, {
        className: (0, _classnames["default"])('rbt-token', 'rbt-token-removeable', {
          'rbt-token-active': active
        }, className)
      }), children, _react["default"].createElement(_ClearButton["default"], {
        className: "rbt-token-remove-button",
        label: "Remove",
        onClick: onRemove,
        onKeyDown: _this._handleRemoveButtonKeydown,
        tabIndex: -1
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "_renderToken", function () {
      var _this$props2 = _this.props,
          children = _this$props2.children,
          className = _this$props2.className,
          disabled = _this$props2.disabled,
          href = _this$props2.href;
      var classnames = (0, _classnames["default"])('rbt-token', {
        'rbt-token-disabled': disabled
      }, className);

      if (href) {
        return _react["default"].createElement("a", {
          className: classnames,
          href: href
        }, children);
      }

      return _react["default"].createElement("div", {
        className: classnames
      }, children);
    });

    _defineProperty(_assertThisInitialized(_this), "_handleRemoveButtonKeydown", function (e) {
      switch (e.keyCode) {
        case _constants.RETURN:
          _this.props.onRemove();

          break;

        default:
          break;
      }
    });

    return _this;
  }

  _createClass(Token, [{
    key: "render",
    value: function render() {
      return this.props.onRemove && !this.props.disabled ? this._renderRemoveableToken() : this._renderToken();
    }
  }]);

  return Token;
}(_react["default"].Component);

Token.propTypes = {
  active: _propTypes["default"].bool,

  /**
   * Handler for removing/deleting the token. If not defined, the token will
   * be rendered in a read-only state.
   */
  onRemove: _propTypes["default"].func,
  tabIndex: _propTypes["default"].number
};
Token.defaultProps = {
  active: false,
  tabIndex: 0
};

var _default = (0, _tokenContainer["default"])(Token);

exports["default"] = _default;

/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _RootCloseWrapper = _interopRequireDefault(__webpack_require__(325));

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Higher-order component that encapsulates Token behaviors, allowing them to
 * be easily re-used.
 */
var tokenContainer = function tokenContainer(Component) {
  var WrappedComponent =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedComponent, _React$Component);

    function WrappedComponent() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedComponent);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "state", {
        active: false
      });

      _defineProperty(_assertThisInitialized(_this), "_handleBlur", function (e) {
        _this.setState({
          active: false
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        switch (e.keyCode) {
          case _constants.BACKSPACE:
            if (_this.state.active) {
              // Prevent backspace keypress from triggering the browser "back"
              // action.
              e.preventDefault();

              _this.props.onRemove();
            }

            break;

          default:
            break;
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActive", function (e) {
        e.stopPropagation();

        _this.setState({
          active: true
        });
      });

      return _this;
    }

    _createClass(WrappedComponent, [{
      key: "render",
      value: function render() {
        return _react["default"].createElement(_RootCloseWrapper["default"], {
          onRootClose: this._handleBlur
        }, _react["default"].createElement(Component, _extends({}, this.props, this.state, {
          onBlur: this._handleBlur,
          onClick: this._handleActive,
          onFocus: this._handleActive,
          onKeyDown: this._handleKeyDown
        })));
      }
    }]);

    return WrappedComponent;
  }(_react["default"].Component);

  WrappedComponent.displayName = "TokenContainer(".concat((0, _utils.getDisplayName)(Component), ")");
  return WrappedComponent;
};

var _default = tokenContainer;
exports["default"] = _default;

/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _contains = __webpack_require__(553);

var _contains2 = _interopRequireDefault(_contains);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _addEventListener = __webpack_require__(554);

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var escapeKeyCode = 27;

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

/**
 * The `<RootCloseWrapper/>` component registers your callback on the document
 * when rendered. Powers the `<Overlay/>` component. This is used achieve modal
 * style behavior where your callback is triggered when the user tries to
 * interact with the rest of the document or hits the `esc` key.
 */

var RootCloseWrapper = function (_React$Component) {
  _inherits(RootCloseWrapper, _React$Component);

  function RootCloseWrapper(props, context) {
    _classCallCheck(this, RootCloseWrapper);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props, context));

    _this.addEventListeners = function () {
      var event = _this.props.event;

      var doc = (0, _ownerDocument2.default)(_this);

      // Use capture for this listener so it fires before React's listener, to
      // avoid false positives in the contains() check below if the target DOM
      // element is removed in the React mouse callback.
      _this.documentMouseCaptureListener = (0, _addEventListener2.default)(doc, event, _this.handleMouseCapture, true);

      _this.documentMouseListener = (0, _addEventListener2.default)(doc, event, _this.handleMouse);

      _this.documentKeyupListener = (0, _addEventListener2.default)(doc, 'keyup', _this.handleKeyUp);
    };

    _this.removeEventListeners = function () {
      if (_this.documentMouseCaptureListener) {
        _this.documentMouseCaptureListener.remove();
      }

      if (_this.documentMouseListener) {
        _this.documentMouseListener.remove();
      }

      if (_this.documentKeyupListener) {
        _this.documentKeyupListener.remove();
      }
    };

    _this.handleMouseCapture = function (e) {
      _this.preventMouseRootClose = isModifiedEvent(e) || !isLeftClickEvent(e) || (0, _contains2.default)(_reactDom2.default.findDOMNode(_this), e.target);
    };

    _this.handleMouse = function (e) {
      if (!_this.preventMouseRootClose && _this.props.onRootClose) {
        _this.props.onRootClose(e);
      }
    };

    _this.handleKeyUp = function (e) {
      if (e.keyCode === escapeKeyCode && _this.props.onRootClose) {
        _this.props.onRootClose(e);
      }
    };

    _this.preventMouseRootClose = false;
    return _this;
  }

  RootCloseWrapper.prototype.componentDidMount = function componentDidMount() {
    if (!this.props.disabled) {
      this.addEventListeners();
    }
  };

  RootCloseWrapper.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
    if (!this.props.disabled && prevProps.disabled) {
      this.addEventListeners();
    } else if (this.props.disabled && !prevProps.disabled) {
      this.removeEventListeners();
    }
  };

  RootCloseWrapper.prototype.componentWillUnmount = function componentWillUnmount() {
    if (!this.props.disabled) {
      this.removeEventListeners();
    }
  };

  RootCloseWrapper.prototype.render = function render() {
    return this.props.children;
  };

  return RootCloseWrapper;
}(_react2.default.Component);

RootCloseWrapper.displayName = 'RootCloseWrapper';

RootCloseWrapper.propTypes = {
  /**
   * Callback fired after click or mousedown. Also triggers when user hits `esc`.
   */
  onRootClose: _propTypes2.default.func,
  /**
   * Children to render.
   */
  children: _propTypes2.default.element,
  /**
   * Disable the the RootCloseWrapper, preventing it from triggering `onRootClose`.
   */
  disabled: _propTypes2.default.bool,
  /**
   * Choose which document mouse event to bind to.
   */
  event: _propTypes2.default.oneOf(['click', 'mousedown'])
};

RootCloseWrapper.defaultProps = {
  event: 'click'
};

exports.default = RootCloseWrapper;
module.exports = exports['default'];

/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _AutosizeInput = _interopRequireDefault(__webpack_require__(322));

var _TypeaheadContext = __webpack_require__(279);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// IE doesn't seem to get the composite computed value (eg: 'padding',
// 'borderStyle', etc.), so generate these from the individual values.
function interpolateStyle(styles, attr) {
  var subattr = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  // Title-case the sub-attribute.
  if (subattr) {
    /* eslint-disable-next-line no-param-reassign */
    subattr = subattr.replace(subattr[0], subattr[0].toUpperCase());
  }

  return ['Top', 'Right', 'Bottom', 'Left'].map(function (dir) {
    return styles[attr + dir + subattr];
  }).join(' ');
}

function copyStyles(inputNode, hintNode) {
  var inputStyle = window.getComputedStyle(inputNode);
  /* eslint-disable no-param-reassign */

  hintNode.style.borderStyle = interpolateStyle(inputStyle, 'border', 'style');
  hintNode.style.borderWidth = interpolateStyle(inputStyle, 'border', 'width');
  hintNode.style.fontSize = inputStyle.fontSize;
  hintNode.style.lineHeight = inputStyle.lineHeight;
  hintNode.style.margin = interpolateStyle(inputStyle, 'margin');
  hintNode.style.padding = interpolateStyle(inputStyle, 'padding');
  /* eslint-enable no-param-reassign */
}

function hintContainer(Input) {
  var HintedInput =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(HintedInput, _React$Component);

    function HintedInput() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, HintedInput);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(HintedInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        var _this$props = _this.props,
            initialItem = _this$props.initialItem,
            onAdd = _this$props.onAdd,
            onKeyDown = _this$props.onKeyDown;

        if ((0, _utils.shouldSelectHint)(e, _this.props)) {
          e.preventDefault(); // Prevent input from blurring on TAB.

          onAdd(initialItem);
        }

        onKeyDown(e);
      });

      return _this;
    }

    _createClass(HintedInput, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        copyStyles(this._input, this._hint);
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate() {
        copyStyles(this._input, this._hint);
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var _this$props2 = this.props,
            hintText = _this$props2.hintText,
            initialItem = _this$props2.initialItem,
            _inputRef = _this$props2.inputRef,
            onAdd = _this$props2.onAdd,
            selectHintOnEnter = _this$props2.selectHintOnEnter,
            props = _objectWithoutProperties(_this$props2, ["hintText", "initialItem", "inputRef", "onAdd", "selectHintOnEnter"]);

        return _react["default"].createElement("div", {
          className: "rbt-input-hint-container",
          style: {
            position: 'relative'
          }
        }, _react["default"].createElement(Input, _extends({}, props, {
          inputRef: function inputRef(input) {
            _this2._input = input;

            _inputRef(input);
          },
          onKeyDown: this._handleKeyDown
        })), _react["default"].createElement(_AutosizeInput["default"], {
          "aria-hidden": true,
          className: "rbt-input-hint",
          inputRef: function inputRef(hint) {
            return _this2._hint = hint;
          },
          inputStyle: {
            backgroundColor: 'transparent',
            borderColor: 'transparent',
            boxShadow: 'none',
            color: 'rgba(0, 0, 0, 0.35)'
          },
          readOnly: true,
          style: {
            left: 0,
            pointerEvents: 'none',
            position: 'absolute',
            top: 0
          },
          tabIndex: -1,
          value: hintText
        }));
      }
    }]);

    return HintedInput;
  }(_react["default"].Component);

  HintedInput.displayName = "HintContainer(".concat((0, _utils.getDisplayName)(Input), ")");
  return (0, _TypeaheadContext.withContext)(HintedInput, ['hintText', 'initialItem', 'onAdd', 'selectHintOnEnter']);
}

var _default = hintContainer;
exports["default"] = _default;

/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function inputContainer(Input) {
  var WrappedInput =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedInput, _React$Component);

    function WrappedInput() {
      _classCallCheck(this, WrappedInput);

      return _possibleConstructorReturn(this, _getPrototypeOf(WrappedInput).apply(this, arguments));
    }

    _createClass(WrappedInput, [{
      key: "render",
      value: function render() {
        var _cx;

        var _this$props = this.props,
            activeIndex = _this$props.activeIndex,
            bsSize = _this$props.bsSize,
            disabled = _this$props.disabled,
            inputRef = _this$props.inputRef,
            isFocused = _this$props.isFocused,
            isInvalid = _this$props.isInvalid,
            isMenuShown = _this$props.isMenuShown,
            isValid = _this$props.isValid,
            labelKey = _this$props.labelKey,
            menuId = _this$props.menuId,
            multiple = _this$props.multiple,
            onBlur = _this$props.onBlur,
            onChange = _this$props.onChange,
            onFocus = _this$props.onFocus,
            onKeyDown = _this$props.onKeyDown,
            onRemove = _this$props.onRemove,
            placeholder = _this$props.placeholder,
            renderToken = _this$props.renderToken,
            selected = _this$props.selected;
        var _this$props$inputProp = this.props.inputProps,
            autoComplete = _this$props$inputProp.autoComplete,
            type = _this$props$inputProp.type; // Add a11y-related props.

        var inputProps = _objectSpread({}, this.props.inputProps, {
          'aria-activedescendant': activeIndex >= 0 ? (0, _utils.getMenuItemId)(activeIndex) : undefined,
          'aria-autocomplete': multiple ? 'list' : 'both',
          'aria-expanded': isMenuShown,
          'aria-haspopup': 'listbox',
          'aria-owns': isMenuShown ? menuId : undefined,
          autoComplete: autoComplete || 'nope',
          disabled: disabled,
          inputRef: inputRef,
          onBlur: onBlur,
          onChange: onChange,
          // Re-open the menu, eg: if it's closed via ESC.
          onClick: onFocus,
          onFocus: onFocus,
          onKeyDown: onKeyDown,
          placeholder: selected.length ? null : placeholder,
          // Comboboxes are single-select by definition:
          // https://www.w3.org/TR/wai-aria-practices-1.1/#combobox
          role: 'combobox',
          type: type || 'text',
          value: (0, _utils.getInputText)(this.props)
        });

        var className = inputProps.className || '';

        if (multiple) {
          inputProps = _objectSpread({}, inputProps, {
            'aria-expanded': undefined,
            inputClassName: className,
            labelKey: labelKey,
            onRemove: onRemove,
            renderToken: renderToken,
            role: undefined,
            selected: selected
          });
        }

        return _react["default"].createElement(Input, _extends({}, inputProps, {
          className: (0, _classnames["default"])('rbt-input', (_cx = {}, _defineProperty(_cx, className, !multiple), _defineProperty(_cx, "focus", isFocused), _defineProperty(_cx, 'input-lg form-control-lg', bsSize === 'large' || bsSize === 'lg'), _defineProperty(_cx, 'input-sm form-control-sm', bsSize === 'small' || bsSize === 'sm'), _defineProperty(_cx, 'is-invalid', isInvalid), _defineProperty(_cx, 'is-valid', isValid), _cx))
        }));
      }
    }]);

    return WrappedInput;
  }(_react["default"].Component);

  WrappedInput.displayName = "InputContainer(".concat((0, _utils.getDisplayName)(Input), ")");
  return WrappedInput;
}

var _default = inputContainer;
exports["default"] = _default;

/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _Highlighter = _interopRequireDefault(__webpack_require__(329));

var _Menu = _interopRequireDefault(__webpack_require__(330));

var _MenuItem = _interopRequireDefault(__webpack_require__(280));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TypeaheadMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadMenu, _React$Component);

  function TypeaheadMenu() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TypeaheadMenu);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TypeaheadMenu)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderMenuItem", function (option, idx) {
      var _this$props = _this.props,
          labelKey = _this$props.labelKey,
          newSelectionPrefix = _this$props.newSelectionPrefix,
          renderMenuItemChildren = _this$props.renderMenuItemChildren,
          text = _this$props.text;
      var label = (0, _utils.getOptionLabel)(option, labelKey);
      var menuItemProps = {
        disabled: option.disabled,
        key: idx,
        label: label,
        option: option,
        position: idx
      };

      if (option.customOption) {
        return _react["default"].createElement(_MenuItem["default"], _extends({}, menuItemProps, {
          className: "rbt-menu-custom-option",
          label: newSelectionPrefix + label
        }), newSelectionPrefix, _react["default"].createElement(_Highlighter["default"], {
          search: text
        }, label));
      }

      if (option.paginationOption) {
        return [_react["default"].createElement(_Menu["default"].Divider, {
          key: "pagination-item-divider"
        }), _react["default"].createElement(_MenuItem["default"], _extends({}, menuItemProps, {
          className: "rbt-menu-pagination-option",
          key: "pagination-item"
        }), label)];
      }

      return _react["default"].createElement(_MenuItem["default"], menuItemProps, renderMenuItemChildren(option, _this.props, idx));
    });

    return _this;
  }

  _createClass(TypeaheadMenu, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          labelKey = _this$props2.labelKey,
          newSelectionPrefix = _this$props2.newSelectionPrefix,
          options = _this$props2.options,
          renderMenuItemChildren = _this$props2.renderMenuItemChildren,
          menuProps = _objectWithoutProperties(_this$props2, ["labelKey", "newSelectionPrefix", "options", "renderMenuItemChildren"]);

      return _react["default"].createElement(_Menu["default"], menuProps, options.map(this._renderMenuItem));
    }
  }]);

  return TypeaheadMenu;
}(_react["default"].Component);

TypeaheadMenu.propTypes = {
  /**
   * Provides the ability to specify a prefix before the user-entered text to
   * indicate that the selection will be new. No-op unless `allowNew={true}`.
   */
  newSelectionPrefix: _propTypes["default"].string,

  /**
   * Provides a hook for customized rendering of menu item contents.
   */
  renderMenuItemChildren: _propTypes["default"].func
};
TypeaheadMenu.defaultProps = {
  newSelectionPrefix: 'New selection: ',
  renderMenuItemChildren: function renderMenuItemChildren(option, props, idx) {
    return _react["default"].createElement(_Highlighter["default"], {
      search: props.text
    }, (0, _utils.getOptionLabel)(option, props.labelKey));
  }
};
var _default = TypeaheadMenu;
exports["default"] = _default;

/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Stripped-down version of https://github.com/helior/react-highlighter
 *
 * Results are already filtered by the time the component is used internally so
 * we can safely ignore case and diacritical marks for the purposes of matching.
 */
var Highlighter =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Highlighter, _React$Component);

  function Highlighter() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Highlighter);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Highlighter)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_count", 0);

    return _this;
  }

  _createClass(Highlighter, [{
    key: "render",
    value: function render() {
      var children = this.props.search ? this._renderHighlightedChildren() : this.props.children;
      return _react["default"].createElement("span", null, children);
    }
  }, {
    key: "_renderHighlightedChildren",
    value: function _renderHighlightedChildren() {
      var children = [];
      var remaining = this.props.children;

      while (remaining) {
        var bounds = (0, _utils.getMatchBounds)(remaining, this.props.search);

        if (!bounds) {
          this._count += 1;
          children.push(_react["default"].createElement("span", {
            key: this._count
          }, remaining));
          return children;
        } // Capture the string that leads up to a match...


        var nonMatch = remaining.slice(0, bounds.start);

        if (nonMatch) {
          this._count += 1;
          children.push(_react["default"].createElement("span", {
            key: this._count
          }, nonMatch));
        } // Now, capture the matching string...


        var match = remaining.slice(bounds.start, bounds.end);

        if (match) {
          this._count += 1;
          children.push(_react["default"].createElement("mark", {
            className: "rbt-highlight-text",
            key: this._count
          }, match));
        } // And if there's anything left over, continue the loop.


        remaining = remaining.slice(bounds.end);
      }

      return children;
    }
  }]);

  return Highlighter;
}(_react["default"].Component);

Highlighter.propTypes = {
  children: _propTypes["default"].string.isRequired,
  search: _propTypes["default"].string.isRequired
};
var _default = Highlighter;
exports["default"] = _default;

/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _isRequiredForA11y = _interopRequireDefault(__webpack_require__(558));

var _react = _interopRequireWildcard(__webpack_require__(1));

var _MenuItem = __webpack_require__(280);

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 * Menu component that handles empty state when passed a set of results.
 */
var Menu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Menu, _React$Component);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, _getPrototypeOf(Menu).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this$props = this.props,
          inputHeight = _this$props.inputHeight,
          scheduleUpdate = _this$props.scheduleUpdate; // Update the menu position if the height of the input changes.

      if (inputHeight !== prevProps.inputHeight) {
        scheduleUpdate();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          children = _this$props2.children,
          className = _this$props2.className,
          emptyLabel = _this$props2.emptyLabel,
          id = _this$props2.id,
          innerRef = _this$props2.innerRef,
          maxHeight = _this$props2.maxHeight,
          style = _this$props2.style,
          text = _this$props2.text;
      var contents = _react.Children.count(children) === 0 ? _react["default"].createElement(_MenuItem.BaseMenuItem, {
        disabled: true
      }, emptyLabel) : children;
      return _react["default"].createElement("ul", {
        className: (0, _classnames["default"])('rbt-menu', 'dropdown-menu', 'show', className),
        id: id,
        key: // Force a re-render if the text changes to ensure that menu
        // positioning updates correctly.
        text,
        ref: innerRef,
        role: "listbox",
        style: _objectSpread({}, style, {
          display: 'block',
          maxHeight: maxHeight,
          overflow: 'auto'
        })
      }, contents);
    }
  }]);

  return Menu;
}(_react["default"].Component);

Menu.propTypes = {
  /**
   * Needed for accessibility.
   */
  id: (0, _isRequiredForA11y["default"])(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string])),

  /**
   * Maximum height of the dropdown menu.
   */
  maxHeight: _propTypes["default"].string
};
Menu.defaultProps = {
  maxHeight: '300px'
};

Menu.Divider = function (props) {
  return _react["default"].createElement("li", {
    className: "divider dropdown-divider",
    role: "separator"
  });
};

Menu.Header = function (props) {
  return _react["default"].createElement("li", _extends({}, props, {
    className: "dropdown-header"
  }));
};

var _default = Menu;
exports["default"] = _default;

/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _reactDom = __webpack_require__(15);

var _TypeaheadContext = __webpack_require__(279);

var _utils = __webpack_require__(155);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var menuItemContainer = function menuItemContainer(Component) {
  var WrappedMenuItem =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedMenuItem, _React$Component);

    function WrappedMenuItem() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedMenuItem);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedMenuItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleClick", function (e) {
        var _this$props = _this.props,
            onMenuItemClick = _this$props.onMenuItemClick,
            option = _this$props.option,
            onClick = _this$props.onClick;
        onMenuItemClick(option, e);
        onClick && onClick(e);
      });

      _defineProperty(_assertThisInitialized(_this), "_maybeUpdateItem", function () {
        var _this$props2 = _this.props,
            activeIndex = _this$props2.activeIndex,
            onActiveItemChange = _this$props2.onActiveItemChange,
            onInitialItemChange = _this$props2.onInitialItemChange,
            option = _this$props2.option,
            position = _this$props2.position;

        if (position === 0) {
          onInitialItemChange(option);
        }

        if (position === activeIndex) {
          // Ensures that if the menu items exceed the bounds of the menu, the
          // menu will scroll up or down as the user hits the arrow keys.

          /* eslint-disable-next-line react/no-find-dom-node */
          (0, _utils.scrollIntoViewIfNeeded)((0, _reactDom.findDOMNode)(_assertThisInitialized(_this)));
          onActiveItemChange(option);
        }
      });

      return _this;
    }

    _createClass(WrappedMenuItem, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this._maybeUpdateItem();
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        this._maybeUpdateItem();
      }
    }, {
      key: "render",
      value: function render() {
        var _this$props3 = this.props,
            activeIndex = _this$props3.activeIndex,
            isOnlyResult = _this$props3.isOnlyResult,
            label = _this$props3.label,
            onActiveItemChange = _this$props3.onActiveItemChange,
            onInitialItemChange = _this$props3.onInitialItemChange,
            onMenuItemClick = _this$props3.onMenuItemClick,
            option = _this$props3.option,
            position = _this$props3.position,
            props = _objectWithoutProperties(_this$props3, ["activeIndex", "isOnlyResult", "label", "onActiveItemChange", "onInitialItemChange", "onMenuItemClick", "option", "position"]);

        var active = isOnlyResult || activeIndex === position;
        return _react["default"].createElement(Component, _extends({}, props, {
          active: active,
          "aria-label": label,
          "aria-selected": active,
          id: (0, _utils.getMenuItemId)(position),
          onClick: this._handleClick,
          onMouseDown: _utils.preventInputBlur,
          role: "option"
        }));
      }
    }]);

    return WrappedMenuItem;
  }(_react["default"].Component);

  WrappedMenuItem.displayName = "MenuItemContainer(".concat((0, _utils.getDisplayName)(Component), ")");
  WrappedMenuItem.propTypes = {
    option: _propTypes["default"].oneOfType([_propTypes["default"].object, _propTypes["default"].string]).isRequired,
    position: _propTypes["default"].number
  };
  return (0, _TypeaheadContext.withContext)(WrappedMenuItem, ['activeIndex', 'isOnlyResult', 'onActiveItemChange', 'onInitialItemChange', 'onMenuItemClick']);
};

var _default = menuItemContainer;
exports["default"] = _default;

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/compute-scroll-into-view/dist/index.module.js
function t(t){return null!=t&&"object"==typeof t&&1===t.nodeType}function e(t,e){return(!e||"hidden"!==t)&&"visible"!==t&&"clip"!==t}function n(t,n){if(t.clientHeight<t.scrollHeight||t.clientWidth<t.scrollWidth){var r=getComputedStyle(t,null);return e(r.overflowY,n)||e(r.overflowX,n)||function(t){var e=function(t){if(!t.ownerDocument||!t.ownerDocument.defaultView)return null;try{return t.ownerDocument.defaultView.frameElement}catch(t){return null}}(t);return!!e&&(e.clientHeight<t.scrollHeight||e.clientWidth<t.scrollWidth)}(t)}return!1}function r(t,e,n,r,i,o,l,d){return o<t&&l>e||o>t&&l<e?0:o<=t&&d<=n||l>=e&&d>=n?o-t-r:l>e&&d<n||o<t&&d>n?l-e+i:0}/* harmony default export */ var index_module = (function(e,i){var o=window,l=i.scrollMode,d=i.block,u=i.inline,h=i.boundary,a=i.skipOverflowHiddenElements,c="function"==typeof h?h:function(t){return t!==h};if(!t(e))throw new TypeError("Invalid target");for(var f=document.scrollingElement||document.documentElement,s=[],p=e;t(p)&&c(p);){if((p=p.parentNode)===f){s.push(p);break}p===document.body&&n(p)&&!n(document.documentElement)||n(p,a)&&s.push(p)}for(var g=o.visualViewport?o.visualViewport.width:innerWidth,m=o.visualViewport?o.visualViewport.height:innerHeight,w=window.scrollX||pageXOffset,v=window.scrollY||pageYOffset,W=e.getBoundingClientRect(),b=W.height,H=W.width,y=W.top,M=W.right,E=W.bottom,V=W.left,x="start"===d||"nearest"===d?y:"end"===d?E:y+b/2,I="center"===u?V+H/2:"end"===u?M:V,C=[],T=0;T<s.length;T++){var k=s[T],B=k.getBoundingClientRect(),D=B.height,O=B.width,R=B.top,X=B.right,Y=B.bottom,L=B.left;if("if-needed"===l&&y>=0&&V>=0&&E<=m&&M<=g&&y>=R&&E<=Y&&V>=L&&M<=X)return C;var S=getComputedStyle(k),j=parseInt(S.borderLeftWidth,10),N=parseInt(S.borderTopWidth,10),q=parseInt(S.borderRightWidth,10),z=parseInt(S.borderBottomWidth,10),A=0,F=0,G="offsetWidth"in k?k.offsetWidth-k.clientWidth-j-q:0,J="offsetHeight"in k?k.offsetHeight-k.clientHeight-N-z:0;if(f===k)A="start"===d?x:"end"===d?x-m:"nearest"===d?r(v,v+m,m,N,z,v+x,v+x+b,b):x-m/2,F="start"===u?I:"center"===u?I-g/2:"end"===u?I-g:r(w,w+g,g,j,q,w+I,w+I+H,H),A=Math.max(0,A+v),F=Math.max(0,F+w);else{A="start"===d?x-R-N:"end"===d?x-Y+z+J:"nearest"===d?r(R,Y,D,N,z+J,x,x+b,b):x-(R+D/2)+J/2,F="start"===u?I-L-j:"center"===u?I-(L+O/2)+G/2:"end"===u?I-X+q+G:r(L,X,O,j,q+G,I,I+H,H);var K=k.scrollLeft,P=k.scrollTop;x+=P-(A=Math.max(0,Math.min(P+A,k.scrollHeight-D+J))),I+=K-(F=Math.max(0,Math.min(K+F,k.scrollWidth-O+G)))}C.push({el:k,top:A,left:F})}return C});
//# sourceMappingURL=index.module.js.map

// CONCATENATED MODULE: ./node_modules/scroll-into-view-if-needed/es/index.js


function isOptionsObject(options) {
  return options === Object(options) && Object.keys(options).length !== 0;
}

function defaultBehavior(actions, behavior) {
  if (behavior === void 0) {
    behavior = 'auto';
  }

  var canSmoothScroll = ('scrollBehavior' in document.body.style);
  actions.forEach(function (_ref) {
    var el = _ref.el,
        top = _ref.top,
        left = _ref.left;

    if (el.scroll && canSmoothScroll) {
      el.scroll({
        top: top,
        left: left,
        behavior: behavior
      });
    } else {
      el.scrollTop = top;
      el.scrollLeft = left;
    }
  });
}

function getOptions(options) {
  if (options === false) {
    return {
      block: 'end',
      inline: 'nearest'
    };
  }

  if (isOptionsObject(options)) {
    return options;
  }

  return {
    block: 'start',
    inline: 'nearest'
  };
}

function scrollIntoView(target, options) {
  var targetIsDetached = !target.ownerDocument.documentElement.contains(target);

  if (isOptionsObject(options) && typeof options.behavior === 'function') {
    return options.behavior(targetIsDetached ? [] : index_module(target, options));
  }

  if (targetIsDetached) {
    return;
  }

  var computeOptions = getOptions(options);
  return defaultBehavior(index_module(target, computeOptions), computeOptions.behavior);
}

/* harmony default export */ var es = __webpack_exports__["a"] = (scrollIntoView);

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var BotChat = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(BotChat, _Component);

  var _super = _createSuper(BotChat);

  function BotChat(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, BotChat);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(BotChat, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      (function (d, m) {
        /*---------------- Kommunicate settings start ----------------*/
        var defaultSettings = {
          "defaultBotIds": ["charley-erttt"],
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "defaultAssignee": "charley-erttt",
          // Replace <BOT_ID> with your bot ID which you can find in bot section of dashboard
          "skipRouting": true
        };
        var kommunicateSettings = {
          "appId": "2b0d8db8368a2f22263da303b739e204a",
          // Replace <APP_ID> with your APP_ID which you can find in install section of dashboard
          "automaticChatOpenOnNavigation": false,
          "onInit": function onInit() {
            Kommunicate.updateSettings(defaultSettings);
          }
        };
        /*----------------- Kommunicate settings end ------------------*/

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://widget.kommunicate.io/v2/kommunicate.app";
        var h = document.getElementsByTagName("head")[0];
        h.appendChild(s);
        window.kommunicate = m;
        m._globals = kommunicateSettings;
      })(document, window.kommunicate || {});
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", null);
    }
  }]);

  return BotChat;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (BotChat);

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157),
    isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ 345:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(251);

/** Built-in value references. */
var getPrototype = overArg(Object.getPrototypeOf, Object);

module.exports = getPrototype;


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(253),
    equalArrays = __webpack_require__(255),
    equalByTag = __webpack_require__(378),
    equalObjects = __webpack_require__(382),
    getTag = __webpack_require__(398),
    isArray = __webpack_require__(148),
    isBuffer = __webpack_require__(258),
    isTypedArray = __webpack_require__(259);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag : getTag(object),
      othTag = othIsArr ? arrayTag : getTag(other);

  objTag = objTag == argsTag ? objectTag : objTag;
  othTag = othTag == argsTag ? objectTag : othTag;

  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

module.exports = baseIsEqualDeep;


/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

module.exports = listCacheClear;


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/** Used for built-in method references. */
var arrayProto = Array.prototype;

/** Built-in value references. */
var splice = arrayProto.splice;

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}

module.exports = listCacheDelete;


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

module.exports = listCacheGet;


/***/ }),

/***/ 351:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

module.exports = listCacheHas;


/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var assocIndexOf = __webpack_require__(185);

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

module.exports = listCacheSet;


/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184);

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new ListCache;
  this.size = 0;
}

module.exports = stackClear;


/***/ }),

/***/ 354:
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);

  this.size = data.size;
  return result;
}

module.exports = stackDelete;


/***/ }),

/***/ 355:
/***/ (function(module, exports) {

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

module.exports = stackGet;


/***/ }),

/***/ 356:
/***/ (function(module, exports) {

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

module.exports = stackHas;


/***/ }),

/***/ 357:
/***/ (function(module, exports, __webpack_require__) {

var ListCache = __webpack_require__(184),
    Map = __webpack_require__(214),
    MapCache = __webpack_require__(215);

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var data = this.__data__;
  if (data instanceof ListCache) {
    var pairs = data.__data__;
    if (!Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }
    data = this.__data__ = new MapCache(pairs);
  }
  data.set(key, value);
  this.size = data.size;
  return this;
}

module.exports = stackSet;


/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(229),
    isMasked = __webpack_require__(359),
    isObject = __webpack_require__(157),
    toSource = __webpack_require__(254);

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

var coreJsData = __webpack_require__(360);

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

module.exports = isMasked;


/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

module.exports = coreJsData;


/***/ }),

/***/ 361:
/***/ (function(module, exports) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;


/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

var Hash = __webpack_require__(363),
    ListCache = __webpack_require__(184),
    Map = __webpack_require__(214);

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

module.exports = mapCacheClear;


/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

var hashClear = __webpack_require__(364),
    hashDelete = __webpack_require__(365),
    hashGet = __webpack_require__(366),
    hashHas = __webpack_require__(367),
    hashSet = __webpack_require__(368);

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

module.exports = Hash;


/***/ }),

/***/ 364:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
  this.size = 0;
}

module.exports = hashClear;


/***/ }),

/***/ 365:
/***/ (function(module, exports) {

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = hashDelete;


/***/ }),

/***/ 366:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

module.exports = hashGet;


/***/ }),

/***/ 367:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? (data[key] !== undefined) : hasOwnProperty.call(data, key);
}

module.exports = hashHas;


/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

var nativeCreate = __webpack_require__(187);

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

module.exports = hashSet;


/***/ }),

/***/ 369:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  var result = getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = mapCacheDelete;


/***/ }),

/***/ 370:
/***/ (function(module, exports) {

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

module.exports = isKeyable;


/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

module.exports = mapCacheGet;


/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

module.exports = mapCacheHas;


/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

var getMapData = __webpack_require__(188);

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  var data = getMapData(this, key),
      size = data.size;

  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

module.exports = mapCacheSet;


/***/ }),

/***/ 374:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(215),
    setCacheAdd = __webpack_require__(375),
    setCacheHas = __webpack_require__(376);

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

module.exports = SetCache;


/***/ }),

/***/ 375:
/***/ (function(module, exports) {

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

module.exports = setCacheAdd;


/***/ }),

/***/ 376:
/***/ (function(module, exports) {

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

module.exports = setCacheHas;


/***/ }),

/***/ 377:
/***/ (function(module, exports) {

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

module.exports = cacheHas;


/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    Uint8Array = __webpack_require__(379),
    eq = __webpack_require__(186),
    equalArrays = __webpack_require__(255),
    mapToArray = __webpack_require__(380),
    setToArray = __webpack_require__(381);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

module.exports = equalByTag;


/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(150);

/** Built-in value references. */
var Uint8Array = root.Uint8Array;

module.exports = Uint8Array;


/***/ }),

/***/ 380:
/***/ (function(module, exports) {

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

module.exports = mapToArray;


/***/ }),

/***/ 381:
/***/ (function(module, exports) {

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

module.exports = setToArray;


/***/ }),

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

var getAllKeys = __webpack_require__(383);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  // Check that cyclic values are equal.
  var objStacked = stack.get(object);
  var othStacked = stack.get(other);
  if (objStacked && othStacked) {
    return objStacked == other && othStacked == object;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

module.exports = equalObjects;


/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

var baseGetAllKeys = __webpack_require__(384),
    getSymbols = __webpack_require__(385),
    keys = __webpack_require__(216);

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeys(object) {
  return baseGetAllKeys(object, keys, getSymbols);
}

module.exports = getAllKeys;


/***/ }),

/***/ 384:
/***/ (function(module, exports, __webpack_require__) {

var arrayPush = __webpack_require__(257),
    isArray = __webpack_require__(148);

/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */
function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
}

module.exports = baseGetAllKeys;


/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

var arrayFilter = __webpack_require__(386),
    stubArray = __webpack_require__(387);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
  if (object == null) {
    return [];
  }
  object = Object(object);
  return arrayFilter(nativeGetSymbols(object), function(symbol) {
    return propertyIsEnumerable.call(object, symbol);
  });
};

module.exports = getSymbols;


/***/ }),

/***/ 386:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];
    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }
  return result;
}

module.exports = arrayFilter;


/***/ }),

/***/ 387:
/***/ (function(module, exports) {

/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

module.exports = stubArray;


/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

var baseTimes = __webpack_require__(389),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148),
    isBuffer = __webpack_require__(258),
    isIndex = __webpack_require__(189),
    isTypedArray = __webpack_require__(259);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;


/***/ }),

/***/ 389:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;


/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;


/***/ }),

/***/ 391:
/***/ (function(module, exports) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;


/***/ }),

/***/ 392:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isLength = __webpack_require__(218),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;


/***/ }),

/***/ 393:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;


/***/ }),

/***/ 394:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var freeGlobal = __webpack_require__(250);

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(230)(module)))

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

var isPrototype = __webpack_require__(396),
    nativeKeys = __webpack_require__(397);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),

/***/ 396:
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

var overArg = __webpack_require__(251);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

var DataView = __webpack_require__(399),
    Map = __webpack_require__(214),
    Promise = __webpack_require__(400),
    Set = __webpack_require__(401),
    WeakMap = __webpack_require__(402),
    baseGetTag = __webpack_require__(158),
    toSource = __webpack_require__(254);

/** `Object#toString` result references. */
var mapTag = '[object Map]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    setTag = '[object Set]',
    weakMapTag = '[object WeakMap]';

var dataViewTag = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
    (Map && getTag(new Map) != mapTag) ||
    (Promise && getTag(Promise.resolve()) != promiseTag) ||
    (Set && getTag(new Set) != setTag) ||
    (WeakMap && getTag(new WeakMap) != weakMapTag)) {
  getTag = function(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag;
        case mapCtorString: return mapTag;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag;
        case weakMapCtorString: return weakMapTag;
      }
    }
    return result;
  };
}

module.exports = getTag;


/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView');

module.exports = DataView;


/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Promise = getNative(root, 'Promise');

module.exports = Promise;


/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var Set = getNative(root, 'Set');

module.exports = Set;


/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

var getNative = __webpack_require__(160),
    root = __webpack_require__(150);

/* Built-in method references that are verified to be native. */
var WeakMap = getNative(root, 'WeakMap');

module.exports = WeakMap;


/***/ }),

/***/ 403:
/***/ (function(module, exports, __webpack_require__) {

var baseMatches = __webpack_require__(404),
    baseMatchesProperty = __webpack_require__(407),
    identity = __webpack_require__(264),
    isArray = __webpack_require__(148),
    property = __webpack_require__(416);

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

module.exports = baseIteratee;


/***/ }),

/***/ 404:
/***/ (function(module, exports, __webpack_require__) {

var baseIsMatch = __webpack_require__(405),
    getMatchData = __webpack_require__(406),
    matchesStrictComparable = __webpack_require__(261);

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

module.exports = baseMatches;


/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(253),
    baseIsEqual = __webpack_require__(213);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;


/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

var isStrictComparable = __webpack_require__(260),
    keys = __webpack_require__(216);

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

module.exports = getMatchData;


/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(213),
    get = __webpack_require__(408),
    hasIn = __webpack_require__(263),
    isKey = __webpack_require__(221),
    isStrictComparable = __webpack_require__(260),
    matchesStrictComparable = __webpack_require__(261),
    toKey = __webpack_require__(167);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
  };
}

module.exports = baseMatchesProperty;


/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220);

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

module.exports = get;


/***/ }),

/***/ 409:
/***/ (function(module, exports, __webpack_require__) {

var memoizeCapped = __webpack_require__(410);

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoizeCapped(function(string) {
  var result = [];
  if (string.charCodeAt(0) === 46 /* . */) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

module.exports = stringToPath;


/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

var memoize = __webpack_require__(411);

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

module.exports = memoizeCapped;


/***/ }),

/***/ 411:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(215);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = MapCache;

module.exports = memoize;


/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    arrayMap = __webpack_require__(413),
    isArray = __webpack_require__(148),
    isSymbol = __webpack_require__(183);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = baseToString;


/***/ }),

/***/ 413:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;


/***/ }),

/***/ 414:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

module.exports = baseHasIn;


/***/ }),

/***/ 415:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(190),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148),
    isIndex = __webpack_require__(189),
    isLength = __webpack_require__(218),
    toKey = __webpack_require__(167);

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

module.exports = hasPath;


/***/ }),

/***/ 416:
/***/ (function(module, exports, __webpack_require__) {

var baseProperty = __webpack_require__(417),
    basePropertyDeep = __webpack_require__(418),
    isKey = __webpack_require__(221),
    toKey = __webpack_require__(167);

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = property;


/***/ }),

/***/ 417:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;


/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220);

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

module.exports = basePropertyDeep;


/***/ }),

/***/ 419:
/***/ (function(module, exports, __webpack_require__) {

var baseEach = __webpack_require__(420);

/**
 * The base implementation of `_.some` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function baseSome(collection, predicate) {
  var result;

  baseEach(collection, function(value, index, collection) {
    result = predicate(value, index, collection);
    return !result;
  });
  return !!result;
}

module.exports = baseSome;


/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {

var baseForOwn = __webpack_require__(421),
    createBaseEach = __webpack_require__(424);

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = createBaseEach(baseForOwn);

module.exports = baseEach;


/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

var baseFor = __webpack_require__(422),
    keys = __webpack_require__(216);

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;


/***/ }),

/***/ 422:
/***/ (function(module, exports, __webpack_require__) {

var createBaseFor = __webpack_require__(423);

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;


/***/ }),

/***/ 423:
/***/ (function(module, exports) {

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;


/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

var isArrayLike = __webpack_require__(219);

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

module.exports = createBaseEach;


/***/ }),

/***/ 425:
/***/ (function(module, exports, __webpack_require__) {

var eq = __webpack_require__(186),
    isArrayLike = __webpack_require__(219),
    isIndex = __webpack_require__(189),
    isObject = __webpack_require__(157);

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

module.exports = isIterateeCall;


/***/ }),

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

var basePickBy = __webpack_require__(427),
    hasIn = __webpack_require__(263);

/**
 * The base implementation of `_.pick` without support for individual
 * property identifiers.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @returns {Object} Returns the new object.
 */
function basePick(object, paths) {
  return basePickBy(object, paths, function(value, path) {
    return hasIn(object, path);
  });
}

module.exports = basePick;


/***/ }),

/***/ 427:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(220),
    baseSet = __webpack_require__(428),
    castPath = __webpack_require__(190);

/**
 * The base implementation of  `_.pickBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @param {Function} predicate The function invoked per property.
 * @returns {Object} Returns the new object.
 */
function basePickBy(object, paths, predicate) {
  var index = -1,
      length = paths.length,
      result = {};

  while (++index < length) {
    var path = paths[index],
        value = baseGet(object, path);

    if (predicate(value, path)) {
      baseSet(result, castPath(path, object), value);
    }
  }
  return result;
}

module.exports = basePickBy;


/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

var assignValue = __webpack_require__(429),
    castPath = __webpack_require__(190),
    isIndex = __webpack_require__(189),
    isObject = __webpack_require__(157),
    toKey = __webpack_require__(167);

/**
 * The base implementation of `_.set`.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @param {Function} [customizer] The function to customize path creation.
 * @returns {Object} Returns `object`.
 */
function baseSet(object, path, value, customizer) {
  if (!isObject(object)) {
    return object;
  }
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = toKey(path[index]),
        newValue = value;

    if (key === '__proto__' || key === 'constructor' || key === 'prototype') {
      return object;
    }

    if (index != lastIndex) {
      var objValue = nested[key];
      newValue = customizer ? customizer(objValue, key, nested) : undefined;
      if (newValue === undefined) {
        newValue = isObject(objValue)
          ? objValue
          : (isIndex(path[index + 1]) ? [] : {});
      }
    }
    assignValue(nested, key, newValue);
    nested = nested[key];
  }
  return object;
}

module.exports = baseSet;


/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

var baseAssignValue = __webpack_require__(430),
    eq = __webpack_require__(186);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

module.exports = assignValue;


/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

var defineProperty = __webpack_require__(266);

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && defineProperty) {
    defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

module.exports = baseAssignValue;


/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

var flatten = __webpack_require__(432),
    overRest = __webpack_require__(435),
    setToString = __webpack_require__(437);

/**
 * A specialized version of `baseRest` which flattens the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @returns {Function} Returns the new function.
 */
function flatRest(func) {
  return setToString(overRest(func, undefined, flatten), func + '');
}

module.exports = flatRest;


/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

var baseFlatten = __webpack_require__(433);

/**
 * Flattens `array` a single level deep.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to flatten.
 * @returns {Array} Returns the new flattened array.
 * @example
 *
 * _.flatten([1, [2, [3, [4]], 5]]);
 * // => [1, 2, [3, [4]], 5]
 */
function flatten(array) {
  var length = array == null ? 0 : array.length;
  return length ? baseFlatten(array, 1) : [];
}

module.exports = flatten;


/***/ }),

/***/ 433:
/***/ (function(module, exports, __webpack_require__) {

var arrayPush = __webpack_require__(257),
    isFlattenable = __webpack_require__(434);

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;


/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(165),
    isArguments = __webpack_require__(217),
    isArray = __webpack_require__(148);

/** Built-in value references. */
var spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

module.exports = isFlattenable;


/***/ }),

/***/ 435:
/***/ (function(module, exports, __webpack_require__) {

var apply = __webpack_require__(436);

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */
function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

module.exports = overRest;


/***/ }),

/***/ 436:
/***/ (function(module, exports) {

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

module.exports = apply;


/***/ }),

/***/ 437:
/***/ (function(module, exports, __webpack_require__) {

var baseSetToString = __webpack_require__(438),
    shortOut = __webpack_require__(440);

/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var setToString = shortOut(baseSetToString);

module.exports = setToString;


/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

var constant = __webpack_require__(439),
    defineProperty = __webpack_require__(266),
    identity = __webpack_require__(264);

/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var baseSetToString = !defineProperty ? identity : function(func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};

module.exports = baseSetToString;


/***/ }),

/***/ 439:
/***/ (function(module, exports) {

/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function() {
    return value;
  };
}

module.exports = constant;


/***/ }),

/***/ 440:
/***/ (function(module, exports) {

/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeNow = Date.now;

/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */
function shortOut(func) {
  var count = 0,
      lastCalled = 0;

  return function() {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);

    lastCalled = stamp;
    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }
    return func.apply(undefined, arguments);
  };
}

module.exports = shortOut;


/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

var objectKeys = __webpack_require__(269);
var isArguments = __webpack_require__(444);
var is = __webpack_require__(448);
var isRegex = __webpack_require__(450);
var flags = __webpack_require__(451);
var isDate = __webpack_require__(453);

var getTime = Date.prototype.getTime;

function deepEqual(actual, expected, options) {
  var opts = options || {};

  // 7.1. All identical values are equivalent, as determined by ===.
  if (opts.strict ? is(actual, expected) : actual === expected) {
    return true;
  }

  // 7.3. Other pairs that do not both pass typeof value == 'object', equivalence is determined by ==.
  if (!actual || !expected || (typeof actual !== 'object' && typeof expected !== 'object')) {
    return opts.strict ? is(actual, expected) : actual == expected;
  }

  /*
   * 7.4. For all other Object pairs, including Array objects, equivalence is
   * determined by having the same number of owned properties (as verified
   * with Object.prototype.hasOwnProperty.call), the same set of keys
   * (although not necessarily the same order), equivalent values for every
   * corresponding key, and an identical 'prototype' property. Note: this
   * accounts for both named and indexed properties on Arrays.
   */
  // eslint-disable-next-line no-use-before-define
  return objEquiv(actual, expected, opts);
}

function isUndefinedOrNull(value) {
  return value === null || value === undefined;
}

function isBuffer(x) {
  if (!x || typeof x !== 'object' || typeof x.length !== 'number') {
    return false;
  }
  if (typeof x.copy !== 'function' || typeof x.slice !== 'function') {
    return false;
  }
  if (x.length > 0 && typeof x[0] !== 'number') {
    return false;
  }
  return true;
}

function objEquiv(a, b, opts) {
  /* eslint max-statements: [2, 50] */
  var i, key;
  if (typeof a !== typeof b) { return false; }
  if (isUndefinedOrNull(a) || isUndefinedOrNull(b)) { return false; }

  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) { return false; }

  if (isArguments(a) !== isArguments(b)) { return false; }

  var aIsRegex = isRegex(a);
  var bIsRegex = isRegex(b);
  if (aIsRegex !== bIsRegex) { return false; }
  if (aIsRegex || bIsRegex) {
    return a.source === b.source && flags(a) === flags(b);
  }

  if (isDate(a) && isDate(b)) {
    return getTime.call(a) === getTime.call(b);
  }

  var aIsBuffer = isBuffer(a);
  var bIsBuffer = isBuffer(b);
  if (aIsBuffer !== bIsBuffer) { return false; }
  if (aIsBuffer || bIsBuffer) { // && would work too, because both are true or both false here
    if (a.length !== b.length) { return false; }
    for (i = 0; i < a.length; i++) {
      if (a[i] !== b[i]) { return false; }
    }
    return true;
  }

  if (typeof a !== typeof b) { return false; }

  try {
    var ka = objectKeys(a);
    var kb = objectKeys(b);
  } catch (e) { // happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates hasOwnProperty)
  if (ka.length !== kb.length) { return false; }

  // the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  // ~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i]) { return false; }
  }
  // equivalent values for every corresponding key, and ~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!deepEqual(a[key], b[key], opts)) { return false; }
  }

  return true;
}

module.exports = deepEqual;


/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keysShim;
if (!Object.keys) {
	// modified from https://github.com/es-shims/es5-shim
	var has = Object.prototype.hasOwnProperty;
	var toStr = Object.prototype.toString;
	var isArgs = __webpack_require__(270); // eslint-disable-line global-require
	var isEnumerable = Object.prototype.propertyIsEnumerable;
	var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
	var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
	var dontEnums = [
		'toString',
		'toLocaleString',
		'valueOf',
		'hasOwnProperty',
		'isPrototypeOf',
		'propertyIsEnumerable',
		'constructor'
	];
	var equalsConstructorPrototype = function (o) {
		var ctor = o.constructor;
		return ctor && ctor.prototype === o;
	};
	var excludedKeys = {
		$applicationCache: true,
		$console: true,
		$external: true,
		$frame: true,
		$frameElement: true,
		$frames: true,
		$innerHeight: true,
		$innerWidth: true,
		$onmozfullscreenchange: true,
		$onmozfullscreenerror: true,
		$outerHeight: true,
		$outerWidth: true,
		$pageXOffset: true,
		$pageYOffset: true,
		$parent: true,
		$scrollLeft: true,
		$scrollTop: true,
		$scrollX: true,
		$scrollY: true,
		$self: true,
		$webkitIndexedDB: true,
		$webkitStorageInfo: true,
		$window: true
	};
	var hasAutomationEqualityBug = (function () {
		/* global window */
		if (typeof window === 'undefined') { return false; }
		for (var k in window) {
			try {
				if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
					try {
						equalsConstructorPrototype(window[k]);
					} catch (e) {
						return true;
					}
				}
			} catch (e) {
				return true;
			}
		}
		return false;
	}());
	var equalsConstructorPrototypeIfNotBuggy = function (o) {
		/* global window */
		if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
			return equalsConstructorPrototype(o);
		}
		try {
			return equalsConstructorPrototype(o);
		} catch (e) {
			return false;
		}
	};

	keysShim = function keys(object) {
		var isObject = object !== null && typeof object === 'object';
		var isFunction = toStr.call(object) === '[object Function]';
		var isArguments = isArgs(object);
		var isString = isObject && toStr.call(object) === '[object String]';
		var theKeys = [];

		if (!isObject && !isFunction && !isArguments) {
			throw new TypeError('Object.keys called on a non-object');
		}

		var skipProto = hasProtoEnumBug && isFunction;
		if (isString && object.length > 0 && !has.call(object, 0)) {
			for (var i = 0; i < object.length; ++i) {
				theKeys.push(String(i));
			}
		}

		if (isArguments && object.length > 0) {
			for (var j = 0; j < object.length; ++j) {
				theKeys.push(String(j));
			}
		} else {
			for (var name in object) {
				if (!(skipProto && name === 'prototype') && has.call(object, name)) {
					theKeys.push(String(name));
				}
			}
		}

		if (hasDontEnumBug) {
			var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

			for (var k = 0; k < dontEnums.length; ++k) {
				if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
					theKeys.push(dontEnums[k]);
				}
			}
		}
		return theKeys;
	};
}
module.exports = keysShim;


/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';
var callBound = __webpack_require__(271);

var $toString = callBound('Object.prototype.toString');

var isStandardArguments = function isArguments(value) {
	if (hasToStringTag && value && typeof value === 'object' && Symbol.toStringTag in value) {
		return false;
	}
	return $toString(value) === '[object Arguments]';
};

var isLegacyArguments = function isArguments(value) {
	if (isStandardArguments(value)) {
		return true;
	}
	return value !== null &&
		typeof value === 'object' &&
		typeof value.length === 'number' &&
		value.length >= 0 &&
		$toString(value) !== '[object Array]' &&
		$toString(value.callee) === '[object Function]';
};

var supportsStandardArguments = (function () {
	return isStandardArguments(arguments);
}());

isStandardArguments.isLegacyArguments = isLegacyArguments; // for tests

module.exports = supportsStandardArguments ? isStandardArguments : isLegacyArguments;


/***/ }),

/***/ 445:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint complexity: [2, 18], max-statements: [2, 33] */
module.exports = function hasSymbols() {
	if (typeof Symbol !== 'function' || typeof Object.getOwnPropertySymbols !== 'function') { return false; }
	if (typeof Symbol.iterator === 'symbol') { return true; }

	var obj = {};
	var sym = Symbol('test');
	var symObj = Object(sym);
	if (typeof sym === 'string') { return false; }

	if (Object.prototype.toString.call(sym) !== '[object Symbol]') { return false; }
	if (Object.prototype.toString.call(symObj) !== '[object Symbol]') { return false; }

	// temp disabled per https://github.com/ljharb/object.assign/issues/17
	// if (sym instanceof Symbol) { return false; }
	// temp disabled per https://github.com/WebReflection/get-own-property-symbols/issues/4
	// if (!(symObj instanceof Symbol)) { return false; }

	// if (typeof Symbol.prototype.toString !== 'function') { return false; }
	// if (String(sym) !== Symbol.prototype.toString.call(sym)) { return false; }

	var symVal = 42;
	obj[sym] = symVal;
	for (sym in obj) { return false; } // eslint-disable-line no-restricted-syntax
	if (typeof Object.keys === 'function' && Object.keys(obj).length !== 0) { return false; }

	if (typeof Object.getOwnPropertyNames === 'function' && Object.getOwnPropertyNames(obj).length !== 0) { return false; }

	var syms = Object.getOwnPropertySymbols(obj);
	if (syms.length !== 1 || syms[0] !== sym) { return false; }

	if (!Object.prototype.propertyIsEnumerable.call(obj, sym)) { return false; }

	if (typeof Object.getOwnPropertyDescriptor === 'function') {
		var descriptor = Object.getOwnPropertyDescriptor(obj, sym);
		if (descriptor.value !== symVal || descriptor.enumerable !== true) { return false; }
	}

	return true;
};


/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice = Array.prototype.slice;
var toStr = Object.prototype.toString;
var funcType = '[object Function]';

module.exports = function bind(that) {
    var target = this;
    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
        throw new TypeError(ERROR_MESSAGE + target);
    }
    var args = slice.call(arguments, 1);

    var bound;
    var binder = function () {
        if (this instanceof bound) {
            var result = target.apply(
                this,
                args.concat(slice.call(arguments))
            );
            if (Object(result) === result) {
                return result;
            }
            return this;
        } else {
            return target.apply(
                that,
                args.concat(slice.call(arguments))
            );
        }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];
    for (var i = 0; i < boundLength; i++) {
        boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
        var Empty = function Empty() {};
        Empty.prototype = target.prototype;
        bound.prototype = new Empty();
        Empty.prototype = null;
    }

    return bound;
};


/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(223);

module.exports = bind.call(Function.call, Object.prototype.hasOwnProperty);


/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(168);
var callBind = __webpack_require__(224);

var implementation = __webpack_require__(274);
var getPolyfill = __webpack_require__(275);
var shim = __webpack_require__(449);

var polyfill = callBind(getPolyfill(), Object);

define(polyfill, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = polyfill;


/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getPolyfill = __webpack_require__(275);
var define = __webpack_require__(168);

module.exports = function shimObjectIs() {
	var polyfill = getPolyfill();
	define(Object, { is: polyfill }, {
		is: function testObjectIs() {
			return Object.is !== polyfill;
		}
	});
	return polyfill;
};


/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var callBound = __webpack_require__(271);
var hasSymbols = __webpack_require__(273)();
var hasToStringTag = hasSymbols && typeof Symbol.toStringTag === 'symbol';
var has;
var $exec;
var isRegexMarker;
var badStringifier;

if (hasToStringTag) {
	has = callBound('Object.prototype.hasOwnProperty');
	$exec = callBound('RegExp.prototype.exec');
	isRegexMarker = {};

	var throwRegexMarker = function () {
		throw isRegexMarker;
	};
	badStringifier = {
		toString: throwRegexMarker,
		valueOf: throwRegexMarker
	};

	if (typeof Symbol.toPrimitive === 'symbol') {
		badStringifier[Symbol.toPrimitive] = throwRegexMarker;
	}
}

var $toString = callBound('Object.prototype.toString');
var gOPD = Object.getOwnPropertyDescriptor;
var regexClass = '[object RegExp]';

module.exports = hasToStringTag
	// eslint-disable-next-line consistent-return
	? function isRegex(value) {
		if (!value || typeof value !== 'object') {
			return false;
		}

		var descriptor = gOPD(value, 'lastIndex');
		var hasLastIndexDataProperty = descriptor && has(descriptor, 'value');
		if (!hasLastIndexDataProperty) {
			return false;
		}

		try {
			$exec(value, badStringifier);
		} catch (e) {
			return e === isRegexMarker;
		}
	}
	: function isRegex(value) {
		// In older browsers, typeof regex incorrectly returns 'function'
		if (!value || (typeof value !== 'object' && typeof value !== 'function')) {
			return false;
		}

		return $toString(value) === regexClass;
	};


/***/ }),

/***/ 451:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__(168);
var callBind = __webpack_require__(224);

var implementation = __webpack_require__(276);
var getPolyfill = __webpack_require__(277);
var shim = __webpack_require__(452);

var flagsBound = callBind(implementation);

define(flagsBound, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = flagsBound;


/***/ }),

/***/ 452:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var supportsDescriptors = __webpack_require__(168).supportsDescriptors;
var getPolyfill = __webpack_require__(277);
var gOPD = Object.getOwnPropertyDescriptor;
var defineProperty = Object.defineProperty;
var TypeErr = TypeError;
var getProto = Object.getPrototypeOf;
var regex = /a/;

module.exports = function shimFlags() {
	if (!supportsDescriptors || !getProto) {
		throw new TypeErr('RegExp.prototype.flags requires a true ES5 environment that supports property descriptors');
	}
	var polyfill = getPolyfill();
	var proto = getProto(regex);
	var descriptor = gOPD(proto, 'flags');
	if (!descriptor || descriptor.get !== polyfill) {
		defineProperty(proto, 'flags', {
			configurable: true,
			enumerable: false,
			get: polyfill
		});
	}
	return polyfill;
};


/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var getDay = Date.prototype.getDay;
var tryDateObject = function tryDateGetDayCall(value) {
	try {
		getDay.call(value);
		return true;
	} catch (e) {
		return false;
	}
};

var toStr = Object.prototype.toString;
var dateClass = '[object Date]';
var hasToStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag === 'symbol';

module.exports = function isDateObject(value) {
	if (typeof value !== 'object' || value === null) {
		return false;
	}
	return hasToStringTag ? tryDateObject(value) : toStr.call(value) === dateClass;
};


/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(157),
    now = __webpack_require__(342),
    toNumber = __webpack_require__(343);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        timeWaiting = wait - timeSinceLastCall;

    return maxing
      ? nativeMin(timeWaiting, maxWait - timeSinceLastInvoke)
      : timeWaiting;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        clearTimeout(timerId);
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;


/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(158),
    isArray = __webpack_require__(148),
    isObjectLike = __webpack_require__(159);

/** `Object#toString` result references. */
var stringTag = '[object String]';

/**
 * Checks if `value` is classified as a `String` primitive or object.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
 * @example
 *
 * _.isString('abc');
 * // => true
 *
 * _.isString(1);
 * // => false
 */
function isString(value) {
  return typeof value == 'string' ||
    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag);
}

module.exports = isString;


/***/ }),

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

var arraySome = __webpack_require__(256),
    baseIteratee = __webpack_require__(403),
    baseSome = __webpack_require__(419),
    isArray = __webpack_require__(148),
    isIterateeCall = __webpack_require__(425);

/**
 * Checks if `predicate` returns truthy for **any** element of `collection`.
 * Iteration is stopped once `predicate` returns truthy. The predicate is
 * invoked with three arguments: (value, index|key, collection).
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 * @example
 *
 * _.some([null, 0, 'yes', false], Boolean);
 * // => true
 *
 * var users = [
 *   { 'user': 'barney', 'active': true },
 *   { 'user': 'fred',   'active': false }
 * ];
 *
 * // The `_.matches` iteratee shorthand.
 * _.some(users, { 'user': 'barney', 'active': false });
 * // => false
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.some(users, ['active', false]);
 * // => true
 *
 * // The `_.property` iteratee shorthand.
 * _.some(users, 'active');
 * // => true
 */
function some(collection, predicate, guard) {
  var func = isArray(collection) ? arraySome : baseSome;
  if (guard && isIterateeCall(collection, predicate, guard)) {
    predicate = undefined;
  }
  return func(collection, baseIteratee(predicate, 3));
}

module.exports = some;


/***/ }),

/***/ 478:
/***/ (function(module, exports, __webpack_require__) {

var toString = __webpack_require__(262);

/** Used to generate unique IDs. */
var idCounter = 0;

/**
 * Generates a unique ID. If `prefix` is given, the ID is appended to it.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {string} [prefix=''] The value to prefix the ID with.
 * @returns {string} Returns the unique ID.
 * @example
 *
 * _.uniqueId('contact_');
 * // => 'contact_104'
 *
 * _.uniqueId();
 * // => '105'
 */
function uniqueId(prefix) {
  var id = ++idCounter;
  return toString(prefix) + id;
}

module.exports = uniqueId;


/***/ }),

/***/ 511:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AsyncTypeahead", {
  enumerable: true,
  get: function get() {
    return _AsyncTypeahead2["default"];
  }
});
Object.defineProperty(exports, "Highlighter", {
  enumerable: true,
  get: function get() {
    return _Highlighter2["default"];
  }
});
Object.defineProperty(exports, "Menu", {
  enumerable: true,
  get: function get() {
    return _Menu2["default"];
  }
});
Object.defineProperty(exports, "MenuItem", {
  enumerable: true,
  get: function get() {
    return _MenuItem2["default"];
  }
});
Object.defineProperty(exports, "Token", {
  enumerable: true,
  get: function get() {
    return _Token2["default"];
  }
});
Object.defineProperty(exports, "Typeahead", {
  enumerable: true,
  get: function get() {
    return _Typeahead2["default"];
  }
});
Object.defineProperty(exports, "TypeaheadMenu", {
  enumerable: true,
  get: function get() {
    return _TypeaheadMenu2["default"];
  }
});
Object.defineProperty(exports, "asyncContainer", {
  enumerable: true,
  get: function get() {
    return _asyncContainer2["default"];
  }
});
Object.defineProperty(exports, "menuItemContainer", {
  enumerable: true,
  get: function get() {
    return _menuItemContainer2["default"];
  }
});
Object.defineProperty(exports, "tokenContainer", {
  enumerable: true,
  get: function get() {
    return _tokenContainer2["default"];
  }
});

var _AsyncTypeahead2 = _interopRequireDefault(__webpack_require__(512));

var _Highlighter2 = _interopRequireDefault(__webpack_require__(329));

var _Menu2 = _interopRequireDefault(__webpack_require__(330));

var _MenuItem2 = _interopRequireDefault(__webpack_require__(280));

var _Token2 = _interopRequireDefault(__webpack_require__(323));

var _Typeahead2 = _interopRequireDefault(__webpack_require__(318));

var _TypeaheadMenu2 = _interopRequireDefault(__webpack_require__(328));

var _asyncContainer2 = _interopRequireDefault(__webpack_require__(314));

var _menuItemContainer2 = _interopRequireDefault(__webpack_require__(331));

var _tokenContainer2 = _interopRequireDefault(__webpack_require__(324));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/***/ }),

/***/ 512:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _asyncContainer = _interopRequireDefault(__webpack_require__(314));

var _Typeahead = _interopRequireDefault(__webpack_require__(318));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = (0, _asyncContainer["default"])(_Typeahead["default"]);

exports["default"] = _default;

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = caseSensitiveType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function caseSensitiveType(props, propName, componentName) {
  var caseSensitive = props.caseSensitive,
      filterBy = props.filterBy;
  (0, _warn["default"])(!caseSensitive || typeof filterBy !== 'function', 'Your `filterBy` function will override the `caseSensitive` prop.');
}

/***/ }),

/***/ 514:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = checkPropType;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Allows additional warnings or messaging related to prop validation.
 */
function checkPropType(validator, callback) {
  return function (props, propName, componentName) {
    _propTypes["default"].checkPropTypes(_defineProperty({}, propName, validator), props, 'prop', componentName);

    typeof callback === 'function' && callback(props, propName, componentName);
  };
}

/***/ }),

/***/ 515:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = defaultInputValueType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function defaultInputValueType(props, propName, componentName) {
  var defaultInputValue = props.defaultInputValue,
      defaultSelected = props.defaultSelected,
      multiple = props.multiple,
      selected = props.selected;
  var name = defaultSelected.length ? 'defaultSelected' : 'selected';
  (0, _warn["default"])(!(!multiple && defaultInputValue && (defaultSelected.length || selected && selected.length)), "`defaultInputValue` will be overridden by the value from `".concat(name, "`."));
}

/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = emptyLabelType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function emptyLabelType(props, propName, componentName) {
  var emptyLabel = props.emptyLabel;
  (0, _warn["default"])(!!emptyLabel, 'Passing a falsy `emptyLabel` value to hide the menu when the result set ' + 'is empty is deprecated. Use `renderMenu` to return `null` instead.');
}

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = highlightOnlyResultType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function highlightOnlyResultType(props, propName, componentName) {
  var allowNew = props.allowNew,
      highlightOnlyResult = props.highlightOnlyResult;
  (0, _warn["default"])(!(highlightOnlyResult && allowNew), '`highlightOnlyResult` will not work with `allowNew`.');
}

/***/ }),

/***/ 518:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = idType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function idType(props, propName, componentName) {
  var id = props.id,
      menuId = props.menuId;
  (0, _warn["default"])(menuId == null, 'The `menuId` prop is deprecated. Use `id` instead.');
  (0, _warn["default"])(id != null, 'The `id` prop will be required in future versions to make the component ' + 'accessible for users of assistive technologies such as screen readers.');
}

/***/ }),

/***/ 519:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = ignoreDiacriticsType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ignoreDiacriticsType(props, propName, componentName) {
  var filterBy = props.filterBy,
      ignoreDiacritics = props.ignoreDiacritics;
  (0, _warn["default"])(ignoreDiacritics || typeof filterBy !== 'function', 'Your `filterBy` function will override the `ignoreDiacritics` prop.');
}

/***/ }),

/***/ 520:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = inputPropsType;

var _isPlainObject = _interopRequireDefault(__webpack_require__(299));

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var BLACKLIST = [{
  alt: 'onBlur',
  prop: 'onBlur'
}, {
  alt: 'onInputChange',
  prop: 'onChange'
}, {
  alt: 'onFocus',
  prop: 'onFocus'
}, {
  alt: 'onKeyDown',
  prop: 'onKeyDown'
}];

function inputPropsType(props, propName, componentName) {
  var inputProps = props.inputProps;

  if (!(inputProps && (0, _isPlainObject["default"])(inputProps))) {
    return;
  } // Blacklisted properties.


  BLACKLIST.forEach(function (_ref) {
    var alt = _ref.alt,
        prop = _ref.prop;
    var msg = alt ? " Use the top-level `".concat(alt, "` prop instead.") : null;
    (0, _warn["default"])(!inputProps[prop], "The `".concat(prop, "` property of `inputProps` will be ignored.").concat(msg));
  });
}

/***/ }),

/***/ 521:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = labelKeyType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function labelKeyType(props, propName, componentName) {
  var allowNew = props.allowNew,
      labelKey = props.labelKey;
  (0, _warn["default"])(!(typeof labelKey === 'function' && allowNew), '`labelKey` must be a string when `allowNew={true}`.');
}

/***/ }),

/***/ 522:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _propTypes["default"].oneOfType([_propTypes["default"].arrayOf(_propTypes["default"].object.isRequired), _propTypes["default"].arrayOf(_propTypes["default"].string.isRequired)]);

exports["default"] = _default;

/***/ }),

/***/ 523:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = selectedType;

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function selectedType(props, propName, componentName) {
  var onChange = props.onChange,
      selected = props.selected;
  (0, _warn["default"])(!selected || selected && typeof onChange === 'function', 'You provided a `selected` prop without an `onChange` handler. If you ' + 'want the typeahead to be uncontrolled, use `defaultSelected`. ' + 'Otherwise, set `onChange`.');
}

/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function addCustomOption(results, props) {
  var allowNew = props.allowNew,
      labelKey = props.labelKey,
      text = props.text;

  if (!allowNew || !text.trim()) {
    return false;
  } // If the consumer has provided a callback, use that to determine whether or
  // not to add the custom option.


  if (typeof allowNew === 'function') {
    return allowNew(results, props);
  } // By default, don't add the custom option if there is an exact text match
  // with an existing option.


  return !results.some(function (o) {
    return (0, _getOptionLabel["default"])(o, labelKey) === text;
  });
}

var _default = addCustomOption;
exports["default"] = _default;

/***/ }),

/***/ 525:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = areEqual;

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _getStringLabelKey = _interopRequireDefault(__webpack_require__(252));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Compare whether items are the same. For custom items, compare the
 * `labelKey` values since a unique id is generated each time, causing the
 * comparison to fail.
 */
function areEqual(newItem, existingItem, labelKey) {
  var stringLabelKey = (0, _getStringLabelKey["default"])(labelKey);

  if (newItem && newItem.customOption && existingItem && existingItem.customOption) {
    return newItem[stringLabelKey] === existingItem[stringLabelKey];
  }

  return (0, _isEqual["default"])(newItem, existingItem);
}

/***/ }),

/***/ 526:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = defaultFilterBy;

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _isFunction = _interopRequireDefault(__webpack_require__(229));

var _isString = _interopRequireDefault(__webpack_require__(476));

var _some = _interopRequireDefault(__webpack_require__(477));

var _stripDiacritics = _interopRequireDefault(__webpack_require__(265));

var _warn = _interopRequireDefault(__webpack_require__(154));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function isMatch(input, string, props) {
  var searchStr = input;
  var str = string;

  if (!props.caseSensitive) {
    searchStr = searchStr.toLowerCase();
    str = str.toLowerCase();
  }

  if (props.ignoreDiacritics) {
    searchStr = (0, _stripDiacritics["default"])(searchStr);
    str = (0, _stripDiacritics["default"])(str);
  }

  return str.indexOf(searchStr) !== -1;
}
/**
 * Default algorithm for filtering results.
 */


function defaultFilterBy(option, props) {
  var filterBy = props.filterBy,
      labelKey = props.labelKey,
      multiple = props.multiple,
      selected = props.selected,
      text = props.text; // Don't show selected options in the menu for the multi-select case.

  if (multiple && selected.some(function (o) {
    return (0, _isEqual["default"])(o, option);
  })) {
    return false;
  }

  var fields = filterBy.slice();

  if ((0, _isFunction["default"])(labelKey) && isMatch(text, labelKey(option), props)) {
    return true;
  }

  if ((0, _isString["default"])(labelKey)) {
    // Add the `labelKey` field to the list of fields if it isn't already there.
    if (fields.indexOf(labelKey) === -1) {
      fields.unshift(labelKey);
    }
  }

  if ((0, _isString["default"])(option)) {
    (0, _warn["default"])(fields.length <= 1, 'You cannot filter by properties when `option` is a string.');
    return isMatch(text, option, props);
  }

  return (0, _some["default"])(fields, function (field) {
    var value = option[field];

    if (!(0, _isString["default"])(value)) {
      (0, _warn["default"])(false, 'Fields passed to `filterBy` should have string values. Value will ' + 'be converted to a string; results may be unexpected.'); // Coerce to string since `toString` isn't null-safe.

      value = "".concat(value);
    }

    return isMatch(text, value, props);
  });
}

/***/ }),

/***/ 527:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function getAccessibilityStatus(props) {
  var a11yNumResults = props.a11yNumResults,
      a11yNumSelected = props.a11yNumSelected,
      emptyLabel = props.emptyLabel,
      isMenuShown = props.isMenuShown,
      results = props.results,
      selected = props.selected; // If the menu is hidden, display info about the number of selections.

  if (!isMenuShown) {
    return a11yNumSelected(selected);
  } // Display info about the number of matches.


  if (results.length === 0) {
    return emptyLabel;
  }

  return a11yNumResults(results);
}

var _default = getAccessibilityStatus;
exports["default"] = _default;

/***/ }),

/***/ 528:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getDisplayName;

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

/***/ }),

/***/ 529:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getMatchBounds = _interopRequireDefault(__webpack_require__(316));

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getHintText(_ref) {
  var activeItem = _ref.activeItem,
      initialItem = _ref.initialItem,
      isFocused = _ref.isFocused,
      isMenuShown = _ref.isMenuShown,
      labelKey = _ref.labelKey,
      multiple = _ref.multiple,
      selected = _ref.selected,
      text = _ref.text;

  // Don't display a hint under the following conditions:
  if ( // No text entered.
  !text || // The input is not focused.
  !isFocused || // The menu is hidden.
  !isMenuShown || // No item in the menu.
  !initialItem || // The initial item is a custom option.
  initialItem.customOption || // One of the menu items is active.
  activeItem || // There's already a selection in single-select mode.
  !!selected.length && !multiple) {
    return '';
  }

  var initialItemStr = (0, _getOptionLabel["default"])(initialItem, labelKey);
  var bounds = (0, _getMatchBounds["default"])(initialItemStr.toLowerCase(), text.toLowerCase());

  if (!(bounds && bounds.start === 0)) {
    return '';
  } // Text matching is case- and accent-insensitive, so to display the hint
  // correctly, splice the input string with the hint string.


  return text + initialItemStr.slice(bounds.end, initialItemStr.length);
}

var _default = getHintText;
exports["default"] = _default;

/***/ }),

/***/ 530:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;

module.exports = function (str) {
	if (typeof str !== 'string') {
		throw new TypeError('Expected a string');
	}

	return str.replace(matchOperatorsRe, '\\$&');
};


/***/ }),

/***/ 531:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

var _getOptionLabel = _interopRequireDefault(__webpack_require__(228));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getInputText(_ref) {
  var activeItem = _ref.activeItem,
      labelKey = _ref.labelKey,
      multiple = _ref.multiple,
      selected = _ref.selected,
      text = _ref.text;

  if (activeItem) {
    // Display the input value if the pagination item is active.
    return activeItem.paginationOption ? text : (0, _getOptionLabel["default"])(activeItem, labelKey);
  }

  var selectedItem = !multiple && !!selected.length && (0, _head["default"])(selected);

  if (selectedItem) {
    return (0, _getOptionLabel["default"])(selectedItem, labelKey);
  }

  return text;
}

var _default = getInputText;
exports["default"] = _default;

/***/ }),

/***/ 532:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getIsOnlyResult(_ref) {
  var allowNew = _ref.allowNew,
      highlightOnlyResult = _ref.highlightOnlyResult,
      results = _ref.results;

  if (!highlightOnlyResult || allowNew) {
    return false;
  }

  return results.length === 1 && !(0, _head["default"])(results).disabled;
}

var _default = getIsOnlyResult;
exports["default"] = _default;

/***/ }),

/***/ 533:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = getMenuItemId;

function getMenuItemId(position) {
  return "rbt-menu-item-".concat(position);
}

/***/ }),

/***/ 534:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Truncates the result set based on `maxResults` and returns the new set.
 */
function getTruncatedOptions(options, maxResults) {
  if (!maxResults || maxResults >= options.length) {
    return options;
  }

  return options.slice(0, maxResults);
}

var _default = getTruncatedOptions;
exports["default"] = _default;

/***/ }),

/***/ 535:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = isShown;

function isShown(results, props) {
  var emptyLabel = props.emptyLabel,
      open = props.open,
      minLength = props.minLength,
      showMenu = props.showMenu,
      text = props.text; // If menu visibility is controlled via props, that value takes precedence.

  if (open || open === false) {
    return open;
  }

  if (!showMenu) {
    return false;
  }

  if (text.length < minLength) {
    return false;
  }

  return !!(results.length || emptyLabel);
}

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = pluralize;

/**
 * Basic util for pluralizing words. By default, simply adds an 's' to the word.
 * Also allows for a custom plural version.
 */
function pluralize(text, count, plural) {
  var pluralText = plural || "".concat(text, "s");
  return "".concat(count, " ").concat(count === 1 ? text : pluralText);
}

/***/ }),

/***/ 537:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = preventInputBlur;

/**
 * Prevent the main input from blurring when a menu item or the clear button is
 * clicked. (#226 & #310)
 */
function preventInputBlur(e) {
  e.preventDefault();
}

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Partial polyfill for webkit `scrollIntoViewIfNeeded()` method. Addresses
 * vertical scrolling only.
 *
 * Inspired by https://gist.github.com/hsablonniere/2581101, but uses
 * `getBoundingClientRect`.
 */
function scrollIntoViewIfNeeded(node) {
  // Webkit browsers
  if (Element.prototype.scrollIntoViewIfNeeded) {
    node.scrollIntoViewIfNeeded();
    return;
  } // FF, IE, etc.


  var rect = node.getBoundingClientRect();
  var parent = node.parentNode;
  var parentRect = parent.getBoundingClientRect();
  var parentComputedStyle = window.getComputedStyle(parent, null);
  var parentBorderTopWidth = parseInt(parentComputedStyle.getPropertyValue('border-top-width'), 10);

  if (rect.top < parentRect.top || rect.bottom > parentRect.bottom) {
    parent.scrollTop = node.offsetTop - parent.offsetTop - parent.clientHeight / 2 - parentBorderTopWidth + node.clientHeight / 2;
  }
}

var _default = scrollIntoViewIfNeeded;
exports["default"] = _default;

/***/ }),

/***/ 539:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = shouldSelectHint;

var _isSelectable = _interopRequireDefault(__webpack_require__(317));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function shouldSelectHint(e, props) {
  var hintText = props.hintText,
      selectHintOnEnter = props.selectHintOnEnter,
      value = props.value;

  if (!hintText) {
    return false;
  }

  if (e.keyCode === _constants.RIGHT) {
    // For selectable input types ("text", "search"), only select the hint if
    // it's at the end of the input value. For non-selectable types ("email",
    // "number"), always select the hint.
    return (0, _isSelectable["default"])(e.target) ? e.target.selectionStart === value.length : true;
  }

  if (e.keyCode === _constants.TAB) {
    return true;
  }

  if (e.keyCode === _constants.RETURN && selectHintOnEnter) {
    return true;
  }

  return false;
}

/***/ }),

/***/ 540:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Loader = function Loader(_ref) {
  var bsSize = _ref.bsSize;
  return _react["default"].createElement("div", {
    className: (0, _classnames["default"])('rbt-loader', {
      'rbt-loader-lg': bsSize === 'large' || bsSize === 'lg',
      'rbt-loader-sm': bsSize === 'small' || bsSize === 'sm'
    })
  });
};

Loader.propTypes = {
  bsSize: _propTypes["default"].oneOf(['large', 'lg', 'small', 'sm'])
};
var _default = Loader;
exports["default"] = _default;

/***/ }),

/***/ 541:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _noop = _interopRequireDefault(__webpack_require__(222));

var _react = _interopRequireWildcard(__webpack_require__(1));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _componentOrElement = _interopRequireDefault(__webpack_require__(267));

var _Portal = _interopRequireDefault(__webpack_require__(543));

var _Popper = _interopRequireDefault(__webpack_require__(546));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BODY_CLASS = 'rbt-body-container';

function getModifiers(_ref) {
  var align = _ref.align,
      flip = _ref.flip;
  return {
    computeStyles: {
      enabled: true,
      fn: function fn(data) {
        // Use the following condition instead of `align === 'justify'` since
        // it allows the component to fall back to justifying the menu width
        // even when `align` is undefined.
        if (align !== 'right' && align !== 'left') {
          // Set the popper width to match the target width.

          /* eslint-disable-next-line no-param-reassign */
          data.styles.width = data.offsets.reference.width;
        }

        return data;
      }
    },
    flip: {
      enabled: flip
    },
    preventOverflow: {
      escapeWithReference: true
    }
  };
}

function isBody(container) {
  return container === document.body;
}
/**
 * Custom `Overlay` component, since the version in `react-overlays` doesn't
 * work for our needs. Specifically, the `Position` component doesn't provide
 * the customized placement we need.
 */


var Overlay =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Overlay, _React$Component);

  function Overlay() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Overlay);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Overlay)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_update", function () {
      var _container$classList;

      var _this$props = _this.props,
          className = _this$props.className,
          container = _this$props.container,
          show = _this$props.show;

      if (!(show && isBody(container))) {
        return;
      } // Set a classname on the body for scoping purposes.


      container.classList.add(BODY_CLASS);
      !!className && (_container$classList = container.classList).add.apply(_container$classList, _toConsumableArray(className.split(' ')));
    });

    return _this;
  }

  _createClass(Overlay, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this._update();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this$props2 = this.props,
          onMenuHide = _this$props2.onMenuHide,
          onMenuShow = _this$props2.onMenuShow,
          onMenuToggle = _this$props2.onMenuToggle,
          show = _this$props2.show;

      if (show !== prevProps.show) {
        show ? onMenuShow() : onMenuHide();
        onMenuToggle(show);
      } // Remove scoping classes if menu isn't being appended to document body.


      var className = prevProps.className,
          container = prevProps.container;

      if (isBody(container) && !isBody(this.props.container)) {
        var _container$classList2;

        container.classList.remove(BODY_CLASS);
        !!className && (_container$classList2 = container.classList).remove.apply(_container$classList2, _toConsumableArray(className.split(' ')));
      }

      this._update();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          align = _this$props3.align,
          children = _this$props3.children,
          container = _this$props3.container,
          dropup = _this$props3.dropup,
          referenceElement = _this$props3.referenceElement,
          show = _this$props3.show;

      if (!(show && _react.Children.count(children))) {
        return null;
      }

      var child = _react.Children.only(children);

      var xPlacement = align === 'right' ? 'end' : 'start';
      var yPlacement = dropup ? 'top' : 'bottom';
      return _react["default"].createElement(_Portal["default"], {
        container: container
      }, _react["default"].createElement(_Popper["default"], {
        modifiers: getModifiers(this.props),
        placement: "".concat(yPlacement, "-").concat(xPlacement),
        referenceElement: referenceElement
      }, function (_ref2) {
        var ref = _ref2.ref,
            props = _objectWithoutProperties(_ref2, ["ref"]);

        return (0, _react.cloneElement)(child, _objectSpread({}, child.props, {}, props, {
          innerRef: ref,
          inputHeight: referenceElement ? referenceElement.offsetHeight : 0
        }));
      }));
    }
  }]);

  return Overlay;
}(_react["default"].Component);

Overlay.propTypes = {
  children: _propTypes["default"].element,
  container: _componentOrElement["default"].isRequired,
  onMenuHide: _propTypes["default"].func,
  onMenuShow: _propTypes["default"].func,
  onMenuToggle: _propTypes["default"].func,
  referenceElement: _componentOrElement["default"],
  show: _propTypes["default"].bool
};
Overlay.defaultProps = {
  onMenuHide: _noop["default"],
  onMenuShow: _noop["default"],
  onMenuToggle: _noop["default"],
  show: false
};
var _default = Overlay;
exports["default"] = _default;

/***/ }),

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createChainableTypeChecker;
/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

// Mostly taken from ReactPropTypes.

function createChainableTypeChecker(validate) {
  function checkType(isRequired, props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] == null) {
      if (isRequired) {
        return new Error('Required ' + location + ' `' + propFullNameSafe + '` was not specified ' + ('in `' + componentNameSafe + '`.'));
      }

      return null;
    }

    for (var _len = arguments.length, args = Array(_len > 6 ? _len - 6 : 0), _key = 6; _key < _len; _key++) {
      args[_key - 6] = arguments[_key];
    }

    return validate.apply(undefined, [props, propName, componentNameSafe, location, propFullNameSafe].concat(args));
  }

  var chainedCheckType = checkType.bind(null, false);
  chainedCheckType.isRequired = checkType.bind(null, true);

  return chainedCheckType;
}
module.exports = exports['default'];

/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _componentOrElement = __webpack_require__(267);

var _componentOrElement2 = _interopRequireDefault(_componentOrElement);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _getContainer = __webpack_require__(320);

var _getContainer2 = _interopRequireDefault(_getContainer);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _LegacyPortal = __webpack_require__(545);

var _LegacyPortal2 = _interopRequireDefault(_LegacyPortal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * The `<Portal/>` component renders its children into a new "subtree" outside of current component hierarchy.
 * You can think of it as a declarative `appendChild()`, or jQuery's `$.fn.appendTo()`.
 * The children of `<Portal/>` component will be appended to the `container` specified.
 */
var Portal = function (_React$Component) {
  _inherits(Portal, _React$Component);

  function Portal() {
    var _temp, _this, _ret;

    _classCallCheck(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.setContainer = function () {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _this.props;

      _this._portalContainerNode = (0, _getContainer2.default)(props.container, (0, _ownerDocument2.default)(_this).body);
    }, _this.getMountNode = function () {
      return _this._portalContainerNode;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Portal.prototype.componentDidMount = function componentDidMount() {
    this.setContainer();
    this.forceUpdate(this.props.onRendered);
  };

  Portal.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (nextProps.container !== this.props.container) {
      this.setContainer(nextProps);
    }
  };

  Portal.prototype.componentWillUnmount = function componentWillUnmount() {
    this._portalContainerNode = null;
  };

  Portal.prototype.render = function render() {
    return this.props.children && this._portalContainerNode ? _reactDom2.default.createPortal(this.props.children, this._portalContainerNode) : null;
  };

  return Portal;
}(_react2.default.Component);

Portal.displayName = 'Portal';
Portal.propTypes = {
  /**
   * A Node, Component instance, or function that returns either. The `container` will have the Portal children
   * appended to it.
   */
  container: _propTypes2.default.oneOfType([_componentOrElement2.default, _propTypes2.default.func]),

  onRendered: _propTypes2.default.func
};
exports.default = _reactDom2.default.createPortal ? Portal : _LegacyPortal2.default;
module.exports = exports['default'];

/***/ }),

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = ownerDocument;

function ownerDocument(node) {
  return node && node.ownerDocument || document;
}

module.exports = exports["default"];

/***/ }),

/***/ 545:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _componentOrElement = __webpack_require__(267);

var _componentOrElement2 = _interopRequireDefault(_componentOrElement);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(15);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _getContainer = __webpack_require__(320);

var _getContainer2 = _interopRequireDefault(_getContainer);

var _ownerDocument = __webpack_require__(268);

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * The `<Portal/>` component renders its children into a new "subtree" outside of current component hierarchy.
 * You can think of it as a declarative `appendChild()`, or jQuery's `$.fn.appendTo()`.
 * The children of `<Portal/>` component will be appended to the `container` specified.
 */
var Portal = function (_React$Component) {
  _inherits(Portal, _React$Component);

  function Portal() {
    var _temp, _this, _ret;

    _classCallCheck(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this._mountOverlayTarget = function () {
      if (!_this._overlayTarget) {
        _this._overlayTarget = document.createElement('div');
        _this._portalContainerNode = (0, _getContainer2.default)(_this.props.container, (0, _ownerDocument2.default)(_this).body);
        _this._portalContainerNode.appendChild(_this._overlayTarget);
      }
    }, _this._unmountOverlayTarget = function () {
      if (_this._overlayTarget) {
        _this._portalContainerNode.removeChild(_this._overlayTarget);
        _this._overlayTarget = null;
      }
      _this._portalContainerNode = null;
    }, _this._renderOverlay = function () {
      var overlay = !_this.props.children ? null : _react2.default.Children.only(_this.props.children);

      // Save reference for future access.
      if (overlay !== null) {
        _this._mountOverlayTarget();

        var initialRender = !_this._overlayInstance;

        _this._overlayInstance = _reactDom2.default.unstable_renderSubtreeIntoContainer(_this, overlay, _this._overlayTarget, function () {
          if (initialRender && _this.props.onRendered) {
            _this.props.onRendered();
          }
        });
      } else {
        // Unrender if the component is null for transitions to null
        _this._unrenderOverlay();
        _this._unmountOverlayTarget();
      }
    }, _this._unrenderOverlay = function () {
      if (_this._overlayTarget) {
        _reactDom2.default.unmountComponentAtNode(_this._overlayTarget);
        _this._overlayInstance = null;
      }
    }, _this.getMountNode = function () {
      return _this._overlayTarget;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Portal.prototype.componentDidMount = function componentDidMount() {
    this._isMounted = true;
    this._renderOverlay();
  };

  Portal.prototype.componentDidUpdate = function componentDidUpdate() {
    this._renderOverlay();
  };

  Portal.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (this._overlayTarget && nextProps.container !== this.props.container) {
      this._portalContainerNode.removeChild(this._overlayTarget);
      this._portalContainerNode = (0, _getContainer2.default)(nextProps.container, (0, _ownerDocument2.default)(this).body);
      this._portalContainerNode.appendChild(this._overlayTarget);
    }
  };

  Portal.prototype.componentWillUnmount = function componentWillUnmount() {
    this._isMounted = false;
    this._unrenderOverlay();
    this._unmountOverlayTarget();
  };

  Portal.prototype.render = function render() {
    return null;
  };

  return Portal;
}(_react2.default.Component);

Portal.displayName = 'Portal';
Portal.propTypes = {
  /**
   * A Node, Component instance, or function that returns either. The `container` will have the Portal children
   * appended to it.
   */
  container: _propTypes2.default.oneOfType([_componentOrElement2.default, _propTypes2.default.func]),

  onRendered: _propTypes2.default.func
};
exports.default = Portal;
module.exports = exports['default'];

/***/ }),

/***/ 546:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(141);

var _interopRequireDefault = __webpack_require__(142);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Popper;
exports.placements = exports.InnerPopper = void 0;

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(__webpack_require__(547));

var _extends2 = _interopRequireDefault(__webpack_require__(17));

var _assertThisInitialized2 = _interopRequireDefault(__webpack_require__(10));

var _inheritsLoose2 = _interopRequireDefault(__webpack_require__(441));

var _defineProperty2 = _interopRequireDefault(__webpack_require__(24));

var _deepEqual = _interopRequireDefault(__webpack_require__(442));

var React = _interopRequireWildcard(__webpack_require__(1));

var _popper = _interopRequireDefault(__webpack_require__(46));

var _Manager = __webpack_require__(548);

var _utils = __webpack_require__(551);

var initialStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  opacity: 0,
  pointerEvents: 'none'
};
var initialArrowStyle = {};

var InnerPopper =
/*#__PURE__*/
function (_React$Component) {
  (0, _inheritsLoose2.default)(InnerPopper, _React$Component);

  function InnerPopper() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "state", {
      data: undefined,
      placement: undefined
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "popperInstance", void 0);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "popperNode", null);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "arrowNode", null);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setPopperNode", function (popperNode) {
      if (!popperNode || _this.popperNode === popperNode) return;
      (0, _utils.setRef)(_this.props.innerRef, popperNode);
      _this.popperNode = popperNode;

      _this.updatePopperInstance();
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setArrowNode", function (arrowNode) {
      _this.arrowNode = arrowNode;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "updateStateModifier", {
      enabled: true,
      order: 900,
      fn: function fn(data) {
        var placement = data.placement;

        _this.setState({
          data: data,
          placement: placement
        });

        return data;
      }
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getOptions", function () {
      return {
        placement: _this.props.placement,
        eventsEnabled: _this.props.eventsEnabled,
        positionFixed: _this.props.positionFixed,
        modifiers: (0, _extends2.default)({}, _this.props.modifiers, {
          arrow: (0, _extends2.default)({}, _this.props.modifiers && _this.props.modifiers.arrow, {
            enabled: !!_this.arrowNode,
            element: _this.arrowNode
          }),
          applyStyle: {
            enabled: false
          },
          updateStateModifier: _this.updateStateModifier
        })
      };
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getPopperStyle", function () {
      return !_this.popperNode || !_this.state.data ? initialStyle : (0, _extends2.default)({
        position: _this.state.data.offsets.popper.position
      }, _this.state.data.styles);
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getPopperPlacement", function () {
      return !_this.state.data ? undefined : _this.state.placement;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getArrowStyle", function () {
      return !_this.arrowNode || !_this.state.data ? initialArrowStyle : _this.state.data.arrowStyles;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "getOutOfBoundariesState", function () {
      return _this.state.data ? _this.state.data.hide : undefined;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "destroyPopperInstance", function () {
      if (!_this.popperInstance) return;

      _this.popperInstance.destroy();

      _this.popperInstance = null;
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "updatePopperInstance", function () {
      _this.destroyPopperInstance();

      var _assertThisInitialize = (0, _assertThisInitialized2.default)(_this),
          popperNode = _assertThisInitialize.popperNode;

      var referenceElement = _this.props.referenceElement;
      if (!referenceElement || !popperNode) return;
      _this.popperInstance = new _popper.default(referenceElement, popperNode, _this.getOptions());
    });
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "scheduleUpdate", function () {
      if (_this.popperInstance) {
        _this.popperInstance.scheduleUpdate();
      }
    });
    return _this;
  }

  var _proto = InnerPopper.prototype;

  _proto.componentDidUpdate = function componentDidUpdate(prevProps, prevState) {
    // If the Popper.js options have changed, update the instance (destroy + create)
    if (this.props.placement !== prevProps.placement || this.props.referenceElement !== prevProps.referenceElement || this.props.positionFixed !== prevProps.positionFixed || !(0, _deepEqual.default)(this.props.modifiers, prevProps.modifiers, {
      strict: true
    })) {
      // develop only check that modifiers isn't being updated needlessly
      if (false) {}

      this.updatePopperInstance();
    } else if (this.props.eventsEnabled !== prevProps.eventsEnabled && this.popperInstance) {
      this.props.eventsEnabled ? this.popperInstance.enableEventListeners() : this.popperInstance.disableEventListeners();
    } // A placement difference in state means popper determined a new placement
    // apart from the props value. By the time the popper element is rendered with
    // the new position Popper has already measured it, if the place change triggers
    // a size change it will result in a misaligned popper. So we schedule an update to be sure.


    if (prevState.placement !== this.state.placement) {
      this.scheduleUpdate();
    }
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    (0, _utils.setRef)(this.props.innerRef, null);
    this.destroyPopperInstance();
  };

  _proto.render = function render() {
    return (0, _utils.unwrapArray)(this.props.children)({
      ref: this.setPopperNode,
      style: this.getPopperStyle(),
      placement: this.getPopperPlacement(),
      outOfBoundaries: this.getOutOfBoundariesState(),
      scheduleUpdate: this.scheduleUpdate,
      arrowProps: {
        ref: this.setArrowNode,
        style: this.getArrowStyle()
      }
    });
  };

  return InnerPopper;
}(React.Component);

exports.InnerPopper = InnerPopper;
(0, _defineProperty2.default)(InnerPopper, "defaultProps", {
  placement: 'bottom',
  eventsEnabled: true,
  referenceElement: undefined,
  positionFixed: false
});
var placements = _popper.default.placements;
exports.placements = placements;

function Popper(_ref) {
  var referenceElement = _ref.referenceElement,
      props = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["referenceElement"]);
  return React.createElement(_Manager.ManagerReferenceNodeContext.Consumer, null, function (referenceNode) {
    return React.createElement(InnerPopper, (0, _extends2.default)({
      referenceElement: referenceElement !== undefined ? referenceElement : referenceNode
    }, props));
  });
}

/***/ }),

/***/ 548:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(141);

var _interopRequireDefault = __webpack_require__(142);

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ManagerReferenceNodeSetterContext = exports.ManagerReferenceNodeContext = void 0;

var _assertThisInitialized2 = _interopRequireDefault(__webpack_require__(10));

var _inheritsLoose2 = _interopRequireDefault(__webpack_require__(441));

var _defineProperty2 = _interopRequireDefault(__webpack_require__(24));

var React = _interopRequireWildcard(__webpack_require__(1));

var _createReactContext = _interopRequireDefault(__webpack_require__(321));

var ManagerReferenceNodeContext = (0, _createReactContext.default)();
exports.ManagerReferenceNodeContext = ManagerReferenceNodeContext;
var ManagerReferenceNodeSetterContext = (0, _createReactContext.default)();
exports.ManagerReferenceNodeSetterContext = ManagerReferenceNodeSetterContext;

var Manager =
/*#__PURE__*/
function (_React$Component) {
  (0, _inheritsLoose2.default)(Manager, _React$Component);

  function Manager() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "referenceNode", void 0);
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "setReferenceNode", function (newReferenceNode) {
      if (newReferenceNode && _this.referenceNode !== newReferenceNode) {
        _this.referenceNode = newReferenceNode;

        _this.forceUpdate();
      }
    });
    return _this;
  }

  var _proto = Manager.prototype;

  _proto.componentWillUnmount = function componentWillUnmount() {
    this.referenceNode = null;
  };

  _proto.render = function render() {
    return React.createElement(ManagerReferenceNodeContext.Provider, {
      value: this.referenceNode
    }, React.createElement(ManagerReferenceNodeSetterContext.Provider, {
      value: this.setReferenceNode
    }, this.props.children));
  };

  return Manager;
}(React.Component);

exports.default = Manager;

/***/ }),

/***/ 549:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _gud = __webpack_require__(550);

var _gud2 = _interopRequireDefault(_gud);

var _warning = __webpack_require__(7);

var _warning2 = _interopRequireDefault(_warning);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MAX_SIGNED_31_BIT_INT = 1073741823;

// Inlined Object.is polyfill.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
function objectIs(x, y) {
  if (x === y) {
    return x !== 0 || 1 / x === 1 / y;
  } else {
    return x !== x && y !== y;
  }
}

function createEventEmitter(value) {
  var handlers = [];
  return {
    on: function on(handler) {
      handlers.push(handler);
    },
    off: function off(handler) {
      handlers = handlers.filter(function (h) {
        return h !== handler;
      });
    },
    get: function get() {
      return value;
    },
    set: function set(newValue, changedBits) {
      value = newValue;
      handlers.forEach(function (handler) {
        return handler(value, changedBits);
      });
    }
  };
}

function onlyChild(children) {
  return Array.isArray(children) ? children[0] : children;
}

function createReactContext(defaultValue, calculateChangedBits) {
  var _Provider$childContex, _Consumer$contextType;

  var contextProp = '__create-react-context-' + (0, _gud2.default)() + '__';

  var Provider = function (_Component) {
    _inherits(Provider, _Component);

    function Provider() {
      var _temp, _this, _ret;

      _classCallCheck(this, Provider);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = _possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.emitter = createEventEmitter(_this.props.value), _temp), _possibleConstructorReturn(_this, _ret);
    }

    Provider.prototype.getChildContext = function getChildContext() {
      var _ref;

      return _ref = {}, _ref[contextProp] = this.emitter, _ref;
    };

    Provider.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      if (this.props.value !== nextProps.value) {
        var oldValue = this.props.value;
        var newValue = nextProps.value;
        var changedBits = void 0;

        if (objectIs(oldValue, newValue)) {
          changedBits = 0; // No change
        } else {
          changedBits = typeof calculateChangedBits === 'function' ? calculateChangedBits(oldValue, newValue) : MAX_SIGNED_31_BIT_INT;
          if (false) {}

          changedBits |= 0;

          if (changedBits !== 0) {
            this.emitter.set(nextProps.value, changedBits);
          }
        }
      }
    };

    Provider.prototype.render = function render() {
      return this.props.children;
    };

    return Provider;
  }(_react.Component);

  Provider.childContextTypes = (_Provider$childContex = {}, _Provider$childContex[contextProp] = _propTypes2.default.object.isRequired, _Provider$childContex);

  var Consumer = function (_Component2) {
    _inherits(Consumer, _Component2);

    function Consumer() {
      var _temp2, _this2, _ret2;

      _classCallCheck(this, Consumer);

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, _Component2.call.apply(_Component2, [this].concat(args))), _this2), _this2.state = {
        value: _this2.getValue()
      }, _this2.onUpdate = function (newValue, changedBits) {
        var observedBits = _this2.observedBits | 0;
        if ((observedBits & changedBits) !== 0) {
          _this2.setState({ value: _this2.getValue() });
        }
      }, _temp2), _possibleConstructorReturn(_this2, _ret2);
    }

    Consumer.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      var observedBits = nextProps.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentDidMount = function componentDidMount() {
      if (this.context[contextProp]) {
        this.context[contextProp].on(this.onUpdate);
      }
      var observedBits = this.props.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentWillUnmount = function componentWillUnmount() {
      if (this.context[contextProp]) {
        this.context[contextProp].off(this.onUpdate);
      }
    };

    Consumer.prototype.getValue = function getValue() {
      if (this.context[contextProp]) {
        return this.context[contextProp].get();
      } else {
        return defaultValue;
      }
    };

    Consumer.prototype.render = function render() {
      return onlyChild(this.props.children)(this.state.value);
    };

    return Consumer;
  }(_react.Component);

  Consumer.contextTypes = (_Consumer$contextType = {}, _Consumer$contextType[contextProp] = _propTypes2.default.object, _Consumer$contextType);


  return {
    Provider: Provider,
    Consumer: Consumer
  };
}

exports.default = createReactContext;
module.exports = exports['default'];

/***/ }),

/***/ 550:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {// @flow


var key = '__global_unique_id__';

module.exports = function() {
  return global[key] = (global[key] || 0) + 1;
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(19)))

/***/ }),

/***/ 551:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setRef = exports.shallowEqual = exports.safeInvoke = exports.unwrapArray = void 0;

/**
 * Takes an argument and if it's an array, returns the first item in the array,
 * otherwise returns the argument. Used for Preact compatibility.
 */
var unwrapArray = function unwrapArray(arg) {
  return Array.isArray(arg) ? arg[0] : arg;
};
/**
 * Takes a maybe-undefined function and arbitrary args and invokes the function
 * only if it is defined.
 */


exports.unwrapArray = unwrapArray;

var safeInvoke = function safeInvoke(fn) {
  if (typeof fn === "function") {
    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    return fn.apply(void 0, args);
  }
};
/**
 * Does a shallow equality check of two objects by comparing the reference
 * equality of each value.
 */


exports.safeInvoke = safeInvoke;

var shallowEqual = function shallowEqual(objA, objB) {
  var aKeys = Object.keys(objA);
  var bKeys = Object.keys(objB);

  if (bKeys.length !== aKeys.length) {
    return false;
  }

  for (var i = 0; i < bKeys.length; i++) {
    var key = aKeys[i];

    if (objA[key] !== objB[key]) {
      return false;
    }
  }

  return true;
};
/**
 * Sets a ref using either a ref callback or a ref object
 */


exports.shallowEqual = shallowEqual;

var setRef = function setRef(ref, node) {
  // if its a function call it
  if (typeof ref === "function") {
    return safeInvoke(ref, node);
  } // otherwise we should treat it as a ref object
  else if (ref != null) {
      ref.current = node;
    }
};

exports.setRef = setRef;

/***/ }),

/***/ 552:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _react = _interopRequireDefault(__webpack_require__(1));

var _AutosizeInput = _interopRequireDefault(__webpack_require__(322));

var _Token = _interopRequireDefault(__webpack_require__(323));

var _utils = __webpack_require__(155);

var _hintContainer = _interopRequireDefault(__webpack_require__(326));

var _inputContainer = _interopRequireDefault(__webpack_require__(327));

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HintedAutosizeInput = (0, _hintContainer["default"])(_AutosizeInput["default"]);

var TypeaheadInputMulti =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadInputMulti, _React$Component);

  function TypeaheadInputMulti() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TypeaheadInputMulti);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TypeaheadInputMulti)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_renderToken", function (option, idx) {
      var _this$props = _this.props,
          _onRemove = _this$props.onRemove,
          renderToken = _this$props.renderToken;

      var props = _objectSpread({}, _this.props, {
        onRemove: function onRemove() {
          return _onRemove(option);
        }
      });

      return renderToken(option, props, idx);
    });

    _defineProperty(_assertThisInitialized(_this), "_handleContainerClickOrFocus", function (e) {
      // Don't focus the input if it's disabled.
      if (_this.props.disabled) {
        e.target.blur();
        return;
      } // Move cursor to the end if the user clicks outside the actual input.


      var inputNode = _this._input;

      if (e.target !== inputNode && (0, _utils.isSelectable)(inputNode)) {
        inputNode.selectionStart = inputNode.value.length;
      }

      inputNode.focus();
    });

    _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
      var _this$props2 = _this.props,
          onKeyDown = _this$props2.onKeyDown,
          selected = _this$props2.selected,
          value = _this$props2.value;

      switch (e.keyCode) {
        case _constants.BACKSPACE:
          if (e.target === _this._input && selected.length && !value) {
            // Prevent browser from going back.
            e.preventDefault(); // If the input is selected and there is no text, focus the last
            // token when the user hits backspace.

            var children = _this._wrapper.children;
            var lastToken = children[children.length - 2];
            lastToken && lastToken.focus();
          }

          break;

        default:
          break;
      }

      onKeyDown(e);
    });

    return _this;
  }

  _createClass(TypeaheadInputMulti, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          className = _this$props3.className,
          inputClassName = _this$props3.inputClassName,
          labelKey = _this$props3.labelKey,
          onRemove = _this$props3.onRemove,
          renderToken = _this$props3.renderToken,
          selected = _this$props3.selected,
          props = _objectWithoutProperties(_this$props3, ["className", "inputClassName", "labelKey", "onRemove", "renderToken", "selected"]);

      return (
        /* eslint-disable jsx-a11y/no-static-element-interactions */

        /* eslint-disable jsx-a11y/click-events-have-key-events */
        _react["default"].createElement("div", {
          className: (0, _classnames["default"])('form-control', 'rbt-input-multi', className),
          disabled: props.disabled,
          onClick: this._handleContainerClickOrFocus,
          onFocus: this._handleContainerClickOrFocus,
          tabIndex: -1
        }, _react["default"].createElement("div", {
          className: "rbt-input-wrapper",
          ref: function ref(el) {
            return _this2._wrapper = el;
          }
        }, selected.map(this._renderToken), _react["default"].createElement(HintedAutosizeInput, _extends({}, props, {
          inputClassName: (0, _classnames["default"])('rbt-input-main', inputClassName),
          inputRef: function inputRef(input) {
            _this2._input = input;

            _this2.props.inputRef(input);
          },
          inputStyle: {
            backgroundColor: 'transparent',
            border: 0,
            boxShadow: 'none',
            cursor: 'inherit',
            outline: 'none',
            padding: 0
          },
          onKeyDown: this._handleKeyDown,
          style: {
            position: 'relative',
            zIndex: 1
          }
        }))))
        /* eslint-enable jsx-a11y/no-static-element-interactions */

        /* eslint-enable jsx-a11y/click-events-have-key-events */

      );
    }
  }]);

  return TypeaheadInputMulti;
}(_react["default"].Component);

TypeaheadInputMulti.propTypes = {
  /**
   * Provides a hook for customized rendering of tokens when multiple
   * selections are enabled.
   */
  renderToken: _propTypes["default"].func
};
TypeaheadInputMulti.defaultProps = {
  renderToken: function renderToken(option, props, idx) {
    return _react["default"].createElement(_Token["default"], {
      disabled: props.disabled,
      key: idx,
      onRemove: props.onRemove,
      tabIndex: props.tabIndex
    }, (0, _utils.getOptionLabel)(option, props.labelKey));
  }
};

var _default = (0, _inputContainer["default"])(TypeaheadInputMulti);

exports["default"] = _default;

/***/ }),

/***/ 553:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var _default = function () {
  // HTML DOM and SVG DOM may have different support levels,
  // so we need to check on context instead of a document root element.
  return _inDOM.default ? function (context, node) {
    if (context.contains) {
      return context.contains(node);
    } else if (context.compareDocumentPosition) {
      return context === node || !!(context.compareDocumentPosition(node) & 16);
    } else {
      return fallback(context, node);
    }
  } : fallback;
}();

exports.default = _default;

function fallback(context, node) {
  if (node) do {
    if (node === context) return true;
  } while (node = node.parentNode);
  return false;
}

module.exports = exports["default"];

/***/ }),

/***/ 554:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (node, event, handler, capture) {
  (0, _on2.default)(node, event, handler, capture);

  return {
    remove: function remove() {
      (0, _off2.default)(node, event, handler, capture);
    }
  };
};

var _on = __webpack_require__(555);

var _on2 = _interopRequireDefault(_on);

var _off = __webpack_require__(556);

var _off2 = _interopRequireDefault(_off);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports['default'];

/***/ }),

/***/ 555:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var on = function on() {};

if (_inDOM.default) {
  on = function () {
    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.addEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.attachEvent('on' + eventName, function (e) {
        e = e || window.event;
        e.target = e.target || e.srcElement;
        e.currentTarget = node;
        handler.call(node, e);
      });
    };
  }();
}

var _default = on;
exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 556:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(142);

exports.__esModule = true;
exports.default = void 0;

var _inDOM = _interopRequireDefault(__webpack_require__(278));

var off = function off() {};

if (_inDOM.default) {
  off = function () {
    if (document.addEventListener) return function (node, eventName, handler, capture) {
      return node.removeEventListener(eventName, handler, capture || false);
    };else if (document.attachEvent) return function (node, eventName, handler) {
      return node.detachEvent('on' + eventName, handler);
    };
  }();
}

var _default = off;
exports.default = _default;
module.exports = exports["default"];

/***/ }),

/***/ 557:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classnames = _interopRequireDefault(__webpack_require__(2));

var _react = _interopRequireDefault(__webpack_require__(1));

var _hintContainer = _interopRequireDefault(__webpack_require__(326));

var _inputContainer = _interopRequireDefault(__webpack_require__(327));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TypeaheadInputSingle =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TypeaheadInputSingle, _React$Component);

  function TypeaheadInputSingle() {
    _classCallCheck(this, TypeaheadInputSingle);

    return _possibleConstructorReturn(this, _getPrototypeOf(TypeaheadInputSingle).apply(this, arguments));
  }

  _createClass(TypeaheadInputSingle, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          className = _this$props.className,
          inputRef = _this$props.inputRef,
          props = _objectWithoutProperties(_this$props, ["className", "inputRef"]);

      return _react["default"].createElement("input", _extends({}, props, {
        className: (0, _classnames["default"])('rbt-input-main', 'form-control', className),
        ref: inputRef
      }));
    }
  }]);

  return TypeaheadInputSingle;
}(_react["default"].Component);

var _default = (0, _inputContainer["default"])((0, _hintContainer["default"])(TypeaheadInputSingle));

exports["default"] = _default;

/***/ }),

/***/ 558:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = isRequiredForA11y;
function isRequiredForA11y(validator) {
  return function validate(props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] == null) {
      return new Error('The ' + location + ' `' + propFullNameSafe + '` is required to make ' + ('`' + componentNameSafe + '` accessible for users of assistive ') + 'technologies such as screen readers.');
    }

    for (var _len = arguments.length, args = Array(_len > 5 ? _len - 5 : 0), _key = 5; _key < _len; _key++) {
      args[_key - 5] = arguments[_key];
    }

    return validator.apply(undefined, [props, propName, componentName, location, propFullName].concat(args));
  };
}
module.exports = exports['default'];

/***/ }),

/***/ 559:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _head = _interopRequireDefault(__webpack_require__(242));

var _isEqual = _interopRequireDefault(__webpack_require__(241));

var _noop = _interopRequireDefault(__webpack_require__(222));

var _uniqueId = _interopRequireDefault(__webpack_require__(478));

var _propTypes = _interopRequireDefault(__webpack_require__(0));

var _deprecated = _interopRequireDefault(__webpack_require__(560));

var _react = _interopRequireDefault(__webpack_require__(1));

var _RootCloseWrapper = _interopRequireDefault(__webpack_require__(325));

var _contextContainer = _interopRequireDefault(__webpack_require__(561));

var _propTypes2 = __webpack_require__(315);

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function genId() {
  var prefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return prefix + Math.random().toString(36).substr(2, 12);
}

function isBodyMenuClick(e, props) {
  if (!props.bodyContainer && !props.positionFixed) {
    return false;
  }

  var target = e.target;

  while (target && target !== document.body) {
    if (target.className && typeof target.className === 'string' && target.className.indexOf('rbt-menu') > -1) {
      return true;
    }

    target = target.parentNode;
  }

  return false;
}

function getInitialState(props) {
  var defaultInputValue = props.defaultInputValue,
      defaultOpen = props.defaultOpen,
      defaultSelected = props.defaultSelected,
      maxResults = props.maxResults,
      multiple = props.multiple;
  var selected = props.selected ? props.selected.slice() : defaultSelected.slice();
  var text = defaultInputValue;

  if (!multiple && selected.length) {
    // Set the text if an initial selection is passed in.
    text = (0, _utils.getOptionLabel)((0, _head["default"])(selected), props.labelKey);

    if (selected.length > 1) {
      // Limit to 1 selection in single-select mode.
      selected = selected.slice(0, 1);
    }
  }

  return {
    activeIndex: -1,
    activeItem: null,
    initialItem: null,
    isFocused: false,
    selected: selected,
    showMenu: defaultOpen,
    shownResults: maxResults,
    text: text
  };
}

function skipDisabledOptions(results, activeIndex, keyCode) {
  var newActiveIndex = activeIndex;

  while (results[newActiveIndex] && results[newActiveIndex].disabled) {
    newActiveIndex += keyCode === _constants.UP ? -1 : 1;
  }

  return newActiveIndex;
}

function typeaheadContainer(Component) {
  var Typeahead = (0, _contextContainer["default"])(Component);

  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "state", getInitialState(_this.props));

      _defineProperty(_assertThisInitialized(_this), "_menuId", genId('rbt-menu-'));

      _defineProperty(_assertThisInitialized(_this), "blur", function () {
        _this.getInput().blur();

        _this._hideMenu();
      });

      _defineProperty(_assertThisInitialized(_this), "clear", function () {
        _this.setState(function (state, props) {
          return _objectSpread({}, getInitialState(props), {
            isFocused: state.isFocused,
            selected: [],
            text: ''
          });
        });
      });

      _defineProperty(_assertThisInitialized(_this), "focus", function () {
        _this.getInput().focus();
      });

      _defineProperty(_assertThisInitialized(_this), "getInput", function () {
        return _this._input;
      });

      _defineProperty(_assertThisInitialized(_this), "getInstance", function () {
        return _assertThisInitialized(_this);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActiveIndexChange", function (activeIndex) {
        var newState = {
          activeIndex: activeIndex
        };

        if (activeIndex === -1) {
          // Reset the active item if there is no active index.
          newState.activeItem = null;
        }

        _this.setState(newState);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleActiveItemChange", function (activeItem) {
        // Don't update the active item if it hasn't changed.
        if (!(0, _utils.areEqual)(activeItem, _this.state.activeItem, _this.props.labelKey)) {
          _this.setState({
            activeItem: activeItem
          });
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleBlur", function (e) {
        e.persist();

        _this.setState({
          isFocused: false
        }, function () {
          return _this.props.onBlur(e);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleClear", function () {
        _this.clear();

        _this._updateSelected([]);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleFocus", function (e) {
        e.persist();

        _this.setState({
          isFocused: true,
          showMenu: true
        }, function () {
          return _this.props.onFocus(e);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInitialItemChange", function (initialItem) {
        // Don't update the initial item if it hasn't changed.
        if (!(0, _utils.areEqual)(initialItem, _this.state.initialItem, _this.props.labelKey)) {
          _this.setState({
            initialItem: initialItem
          });
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleInputChange", function (e) {
        e.persist();
        var text = e.target.value;

        var _getInitialState = getInitialState(_this.props),
            activeIndex = _getInitialState.activeIndex,
            activeItem = _getInitialState.activeItem,
            shownResults = _getInitialState.shownResults;

        var _this$props = _this.props,
            multiple = _this$props.multiple,
            onInputChange = _this$props.onInputChange;

        _this.setState({
          activeIndex: activeIndex,
          activeItem: activeItem,
          showMenu: true,
          shownResults: shownResults,
          text: text
        }, function () {
          return onInputChange(text, e);
        }); // Clear any selections if text is entered in single-select mode.


        if (_this.state.selected.length && !multiple) {
          _this._updateSelected([]);
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e, results, isMenuShown) {
        var activeItem = _this.state.activeItem;
        var activeIndex = _this.state.activeIndex;

        switch (e.keyCode) {
          case _constants.UP:
          case _constants.DOWN:
            if (!isMenuShown) {
              _this._showMenu();

              break;
            } // Prevents input cursor from going to the beginning when pressing up.


            e.preventDefault(); // Increment or decrement index based on user keystroke.

            activeIndex += e.keyCode === _constants.UP ? -1 : 1; // Skip over any disabled options.

            activeIndex = skipDisabledOptions(results, activeIndex, e.keyCode); // If we've reached the end, go back to the beginning or vice-versa.

            if (activeIndex === results.length) {
              activeIndex = -1;
            } else if (activeIndex === -2) {
              activeIndex = results.length - 1; // Skip over any disabled options.

              activeIndex = skipDisabledOptions(results, activeIndex, e.keyCode);
            }

            _this._handleActiveIndexChange(activeIndex);

            break;

          case _constants.ESC:
            isMenuShown && _this._hideMenu();
            break;

          case _constants.RETURN:
            if (!isMenuShown) {
              break;
            } // Prevent form submission while menu is open.


            e.preventDefault();
            activeItem && _this._handleMenuItemSelect(activeItem, e);
            break;

          case _constants.RIGHT:
          case _constants.TAB:
            if (!isMenuShown) {
              break;
            }

            if (activeItem && !activeItem.paginationOption) {
              // Prevent blurring when selecting the active item.
              e.keyCode === _constants.TAB && e.preventDefault();

              _this._handleSelectionAdd(activeItem);

              break;
            }

            if (e.keyCode === _constants.TAB) {
              _this._hideMenu();
            }

            break;

          default:
            break;
        }

        _this.props.onKeyDown(e);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleMenuItemSelect", function (option, e) {
        if (option.paginationOption) {
          _this._handlePaginate(e);
        } else {
          _this._handleSelectionAdd(option);
        }
      });

      _defineProperty(_assertThisInitialized(_this), "_handlePaginate", function (e) {
        e.persist();

        _this.setState(function (_ref, _ref2) {
          var shownResults = _ref.shownResults;
          var maxResults = _ref2.maxResults;
          return {
            shownResults: shownResults + maxResults
          };
        }, function () {
          return _this.props.onPaginate(e, _this.state.shownResults);
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSelectionAdd", function (selection) {
        var _this$props2 = _this.props,
            multiple = _this$props2.multiple,
            labelKey = _this$props2.labelKey;
        var selected;
        var text;

        if (multiple) {
          // If multiple selections are allowed, add the new selection to the
          // existing selections.
          selected = _this.state.selected.concat(selection);
          text = '';
        } else {
          // If only a single selection is allowed, replace the existing selection
          // with the new one.
          selected = [selection];
          text = (0, _utils.getOptionLabel)(selection, labelKey);
        }

        _this._hideMenu();

        _this.setState({
          initialItem: selection,
          text: text
        }); // Text must be updated before the selection to fix #211.
        // TODO: Find a more robust way of solving the issue.


        _this._updateSelected(selected);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleSelectionRemove", function (selection) {
        var selected = _this.state.selected.filter(function (option) {
          return !(0, _isEqual["default"])(option, selection);
        }); // Make sure the input stays focused after the item is removed.


        _this.focus();

        _this._hideMenu();

        _this._updateSelected(selected);
      });

      _defineProperty(_assertThisInitialized(_this), "_handleRootClose", function (e) {
        if (isBodyMenuClick(e, _this.props) || !_this.state.showMenu) {
          return;
        }

        _this._hideMenu();
      });

      _defineProperty(_assertThisInitialized(_this), "_hideMenu", function () {
        var _getInitialState2 = getInitialState(_this.props),
            activeIndex = _getInitialState2.activeIndex,
            activeItem = _getInitialState2.activeItem,
            shownResults = _getInitialState2.shownResults;

        _this.setState({
          activeIndex: activeIndex,
          activeItem: activeItem,
          showMenu: false,
          shownResults: shownResults
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_showMenu", function () {
        _this.setState({
          showMenu: true
        });
      });

      _defineProperty(_assertThisInitialized(_this), "_updateSelected", function (selected) {
        _this.setState({
          selected: selected
        }, function () {
          _this.props.onChange && _this.props.onChange(selected);
        });
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this.props.autoFocus && this.focus();
      }
    }, {
      key: "componentWillReceiveProps",
      value: function componentWillReceiveProps(nextProps) {
        var labelKey = nextProps.labelKey,
            multiple = nextProps.multiple,
            selected = nextProps.selected; // If new selections are passed via props, treat as a controlled input.

        if (selected && !(0, _isEqual["default"])(selected, this.state.selected)) {
          this.setState({
            selected: selected
          });

          if (multiple) {
            return;
          }

          this.setState({
            text: selected.length ? (0, _utils.getOptionLabel)((0, _head["default"])(selected), labelKey) : ''
          });
        } // Truncate selections when in single-select mode.


        var newSelected = selected || this.state.selected;

        if (!multiple && newSelected.length > 1) {
          newSelected = newSelected.slice(0, 1);
          this.setState({
            selected: newSelected,
            text: (0, _utils.getOptionLabel)((0, _head["default"])(newSelected), labelKey)
          });
          return;
        }

        if (multiple !== this.props.multiple) {
          this.setState({
            text: ''
          });
        }
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var mergedPropsAndState = _objectSpread({}, this.props, {}, this.state);

        var filterBy = mergedPropsAndState.filterBy,
            labelKey = mergedPropsAndState.labelKey,
            minLength = mergedPropsAndState.minLength,
            options = mergedPropsAndState.options,
            paginate = mergedPropsAndState.paginate,
            paginationText = mergedPropsAndState.paginationText,
            shownResults = mergedPropsAndState.shownResults,
            text = mergedPropsAndState.text;
        var results = [];

        if (text.length >= minLength) {
          var cb = Array.isArray(filterBy) ? _utils.defaultFilterBy : filterBy;
          results = options.filter(function (option) {
            return cb(option, mergedPropsAndState);
          });
        } // This must come before results are truncated.


        var shouldPaginate = paginate && results.length > shownResults; // Truncate results if necessary.

        results = (0, _utils.getTruncatedOptions)(results, shownResults); // Add the custom option if necessary.

        if ((0, _utils.addCustomOption)(results, mergedPropsAndState)) {
          results.push(_defineProperty({
            customOption: true,
            id: (0, _uniqueId["default"])('new-id-')
          }, (0, _utils.getStringLabelKey)(labelKey), text));
        } // Add the pagination item if necessary.


        if (shouldPaginate) {
          var _results$push2;

          results.push((_results$push2 = {}, _defineProperty(_results$push2, (0, _utils.getStringLabelKey)(labelKey), paginationText), _defineProperty(_results$push2, "paginationOption", true), _results$push2));
        } // This must come after checks for the custom option and pagination.


        var isMenuShown = (0, _utils.isShown)(results, mergedPropsAndState);
        return _react["default"].createElement(_RootCloseWrapper["default"], {
          disabled: this.props.open,
          onRootClose: this._handleRootClose
        }, _react["default"].createElement(Typeahead, _extends({}, mergedPropsAndState, {
          bodyContainer: this.props.positionFixed || this.props.bodyContainer,
          inputRef: function inputRef(input) {
            return _this2._input = input;
          },
          isMenuShown: isMenuShown,
          menuId: this.props.id || this.props.menuId || this._menuId,
          onActiveItemChange: this._handleActiveItemChange,
          onAdd: this._handleSelectionAdd,
          onBlur: this._handleBlur,
          onChange: this._handleInputChange,
          onClear: this._handleClear,
          onFocus: this._handleFocus,
          onInitialItemChange: this._handleInitialItemChange,
          onKeyDown: function onKeyDown(e) {
            return _this2._handleKeyDown(e, results, isMenuShown);
          },
          onMenuItemClick: this._handleMenuItemSelect,
          onRemove: this._handleSelectionRemove,
          results: results
        })));
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  WrappedTypeahead.displayName = "TypeaheadContainer(".concat((0, _utils.getDisplayName)(Typeahead), ")");
  WrappedTypeahead.propTypes = {
    /**
     * For localized accessibility: Should return a string indicating the number
     * of results for screen readers. Receives the current results.
     */
    a11yNumResults: _propTypes["default"].func,

    /**
     * For localized accessibility: Should return a string indicating the number
     * of selections for screen readers. Receives the current selections.
     */
    a11yNumSelected: _propTypes["default"].func,

    /**
     * Specify menu alignment. The default value is `justify`, which makes the
     * menu as wide as the input and truncates long values. Specifying `left`
     * or `right` will align the menu to that side and the width will be
     * determined by the length of menu item values.
     */
    align: _propTypes["default"].oneOf(['justify', 'left', 'right']),

    /**
     * Allows the creation of new selections on the fly. Note that any new items
     * will be added to the list of selections, but not the list of original
     * options unless handled as such by `Typeahead`'s parent.
     *
     * If a function is specified, it will be used to determine whether a custom
     * option should be included. The return value should be true or false.
     */
    allowNew: _propTypes["default"].oneOfType([_propTypes["default"].bool, _propTypes["default"].func]),

    /**
     * Autofocus the input when the component initially mounts.
     */
    autoFocus: _propTypes["default"].bool,

    /**
     * Whether to render the menu inline or attach to `document.body`.
     */
    bodyContainer: (0, _deprecated["default"])(_propTypes["default"].bool, 'Use `positionFixed` instead'),

    /**
     * Whether or not filtering should be case-sensitive.
     */
    caseSensitive: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.caseSensitiveType),

    /**
     * Displays a button to clear the input when there are selections.
     */
    clearButton: _propTypes["default"].bool,

    /**
     * The initial value displayed in the text input.
     */
    defaultInputValue: (0, _propTypes2.checkPropType)(_propTypes["default"].string, _propTypes2.defaultInputValueType),

    /**
     * Whether or not the menu is displayed upon initial render.
     */
    defaultOpen: _propTypes["default"].bool,

    /**
     * Specify any pre-selected options. Use only if you want the component to
     * be uncontrolled.
     */
    defaultSelected: _propTypes2.optionType,

    /**
     * Whether to disable the component.
     */
    disabled: _propTypes["default"].bool,

    /**
     * Specify whether the menu should appear above the input.
     */
    dropup: _propTypes["default"].bool,

    /**
     * Message to display in the menu if there are no valid results.
     */
    emptyLabel: (0, _propTypes2.checkPropType)(_propTypes["default"].node, _propTypes2.emptyLabelType),

    /**
     * Either an array of fields in `option` to search, or a custom filtering
     * callback.
     */
    filterBy: _propTypes["default"].oneOfType([_propTypes["default"].arrayOf(_propTypes["default"].string.isRequired), _propTypes["default"].func]),

    /**
     * Whether or not to automatically adjust the position of the menu when it
     * reaches the viewport boundaries.
     */
    flip: _propTypes["default"].bool,

    /**
     * Highlights the menu item if there is only one result and allows selecting
     * that item by hitting enter. Does not work with `allowNew`.
     */
    highlightOnlyResult: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.highlightOnlyResultType),

    /**
     * An html id attribute, required for assistive technologies such as screen
     * readers.
     */
    id: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string]), _propTypes2.idType),

    /**
     * Whether the filter should ignore accents and other diacritical marks.
     */
    ignoreDiacritics: (0, _propTypes2.checkPropType)(_propTypes["default"].bool, _propTypes2.ignoreDiacriticsType),

    /**
     * Props to be applied directly to the input. `onBlur`, `onChange`,
     * `onFocus`, and `onKeyDown` are ignored.
     */
    inputProps: (0, _propTypes2.checkPropType)(_propTypes["default"].object, _propTypes2.inputPropsType),

    /**
     * Bootstrap 4 only. Adds the `is-invalid` classname to the `form-control`.
     */
    isInvalid: _propTypes["default"].bool,

    /**
     * Indicate whether an asynchronous data fetch is happening.
     */
    isLoading: _propTypes["default"].bool,

    /**
     * Bootstrap 4 only. Adds the `is-valid` classname to the `form-control`.
     */
    isValid: _propTypes["default"].bool,

    /**
     * Specify the option key to use for display or a function returning the
     * display string. By default, the selector will use the `label` key.
     */
    labelKey: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].func]), _propTypes2.labelKeyType),

    /**
     * Maximum number of results to display by default. Mostly done for
     * performance reasons so as not to render too many DOM nodes in the case of
     * large data sets.
     */
    maxResults: _propTypes["default"].number,

    /**
     * Id applied to the top-level menu element. Required for accessibility.
     */
    menuId: (0, _propTypes2.checkPropType)(_propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string]), _propTypes2.idType),

    /**
     * Number of input characters that must be entered before showing results.
     */
    minLength: _propTypes["default"].number,

    /**
     * Whether or not multiple selections are allowed.
     */
    multiple: _propTypes["default"].bool,

    /**
     * Invoked when the input is blurred. Receives an event.
     */
    onBlur: _propTypes["default"].func,

    /**
     * Invoked whenever items are added or removed. Receives an array of the
     * selected options.
     */
    onChange: _propTypes["default"].func,

    /**
     * Invoked when the input is focused. Receives an event.
     */
    onFocus: _propTypes["default"].func,

    /**
     * Invoked when the input value changes. Receives the string value of the
     * input.
     */
    onInputChange: _propTypes["default"].func,

    /**
     * Invoked when a key is pressed. Receives an event.
     */
    onKeyDown: _propTypes["default"].func,

    /**
     * Invoked when the menu is hidden.
     */
    onMenuHide: (0, _deprecated["default"])(_propTypes["default"].func, 'Use `onMenuToggle` instead'),

    /**
     * Invoked when the menu is shown.
     */
    onMenuShow: (0, _deprecated["default"])(_propTypes["default"].func, 'Use `onMenuToggle` instead'),

    /**
     * Invoked when menu visibility changes.
     */
    onMenuToggle: _propTypes["default"].func,

    /**
     * Invoked when the pagination menu item is clicked. Receives an event.
     */
    onPaginate: _propTypes["default"].func,

    /**
     * Whether or not the menu should be displayed. `undefined` allows the
     * component to control visibility, while `true` and `false` show and hide
     * the menu, respectively.
     */
    open: _propTypes["default"].bool,

    /**
     * Full set of options, including pre-selected options. Must either be an
     * array of objects (recommended) or strings.
     */
    options: _propTypes2.optionType.isRequired,

    /**
     * Give user the ability to display additional results if the number of
     * results exceeds `maxResults`.
     */
    paginate: _propTypes["default"].bool,

    /**
     * Prompt displayed when large data sets are paginated.
     */
    paginationText: _propTypes["default"].string,

    /**
     * Placeholder text for the input.
     */
    placeholder: _propTypes["default"].string,

    /**
     * Callback for custom menu rendering.
     */
    renderMenu: _propTypes["default"].func,

    /**
     * The selected option(s) displayed in the input. Use this prop if you want
     * to control the component via its parent.
     */
    selected: (0, _propTypes2.checkPropType)(_propTypes2.optionType, _propTypes2.selectedType),

    /**
     * Allows selecting the hinted result by pressing enter.
     */
    selectHintOnEnter: _propTypes["default"].bool
  };
  WrappedTypeahead.defaultProps = {
    a11yNumResults: function a11yNumResults(results) {
      var resultString = (0, _utils.pluralize)('result', results.length);
      return "".concat(resultString, ". Use up and down arrow keys to navigate.");
    },
    a11yNumSelected: function a11yNumSelected(selected) {
      return (0, _utils.pluralize)('selection', selected.length);
    },
    align: 'justify',
    allowNew: false,
    autoFocus: false,
    caseSensitive: false,
    clearButton: false,
    defaultInputValue: '',
    defaultOpen: false,
    defaultSelected: [],
    disabled: false,
    dropup: false,
    emptyLabel: 'No matches found.',
    filterBy: [],
    flip: false,
    highlightOnlyResult: false,
    ignoreDiacritics: true,
    inputProps: {},
    isInvalid: false,
    isLoading: false,
    isValid: false,
    labelKey: _constants.DEFAULT_LABELKEY,
    maxResults: 100,
    minLength: 0,
    multiple: false,
    onBlur: _noop["default"],
    onFocus: _noop["default"],
    onInputChange: _noop["default"],
    onKeyDown: _noop["default"],
    onPaginate: _noop["default"],
    paginate: true,
    paginationText: 'Display additional results...',
    placeholder: '',
    selectHintOnEnter: false
  };
  return WrappedTypeahead;
}

var _default = typeaheadContainer;
exports["default"] = _default;

/***/ }),

/***/ 560:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = deprecated;

var _warning = __webpack_require__(7);

var _warning2 = _interopRequireDefault(_warning);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var warned = {};

function deprecated(validator, reason) {
  return function validate(props, propName, componentName, location, propFullName) {
    var componentNameSafe = componentName || '<<anonymous>>';
    var propFullNameSafe = propFullName || propName;

    if (props[propName] != null) {
      var messageKey = componentName + '.' + propName;

      (0, _warning2.default)(warned[messageKey], 'The ' + location + ' `' + propFullNameSafe + '` of ' + ('`' + componentNameSafe + '` is deprecated. ' + reason + '.'));

      warned[messageKey] = true;
    }

    for (var _len = arguments.length, args = Array(_len > 5 ? _len - 5 : 0), _key = 5; _key < _len; _key++) {
      args[_key - 5] = arguments[_key];
    }

    return validator.apply(undefined, [props, propName, componentName, location, propFullName].concat(args));
  };
}

/* eslint-disable no-underscore-dangle */
function _resetWarned() {
  warned = {};
}

deprecated._resetWarned = _resetWarned;
/* eslint-enable no-underscore-dangle */

module.exports = exports['default'];

/***/ }),

/***/ 561:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _pick = _interopRequireDefault(__webpack_require__(243));

var _react = _interopRequireDefault(__webpack_require__(1));

var _TypeaheadContext = _interopRequireDefault(__webpack_require__(279));

var _utils = __webpack_require__(155);

var _constants = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function contextContainer(Typeahead) {
  var WrappedTypeahead =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WrappedTypeahead, _React$Component);

    function WrappedTypeahead() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, WrappedTypeahead);

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WrappedTypeahead)).call.apply(_getPrototypeOf2, [this].concat(args)));

      _defineProperty(_assertThisInitialized(_this), "_handleKeyDown", function (e) {
        var _this$props = _this.props,
            initialItem = _this$props.initialItem,
            onKeyDown = _this$props.onKeyDown,
            onAdd = _this$props.onAdd;

        switch (e.keyCode) {
          case _constants.RETURN:
            if ((0, _utils.getIsOnlyResult)(_this.props) && initialItem) {
              onAdd(initialItem);
            }

            break;

          default:
            break;
        }

        onKeyDown(e);
      });

      return _this;
    }

    _createClass(WrappedTypeahead, [{
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps, prevState) {
        var _this$props2 = this.props,
            allowNew = _this$props2.allowNew,
            onInitialItemChange = _this$props2.onInitialItemChange,
            results = _this$props2.results; // Clear the initial item when there are no results.

        if (!(allowNew || results.length)) {
          onInitialItemChange(null);
        }
      }
    }, {
      key: "render",
      value: function render() {
        var contextValues = (0, _pick["default"])(this.props, ['activeIndex', 'initialItem', 'onActiveItemChange', 'onAdd', 'onInitialItemChange', 'onMenuItemClick', 'selectHintOnEnter']);
        return _react["default"].createElement(_TypeaheadContext["default"].Provider, {
          value: _objectSpread({}, contextValues, {
            hintText: (0, _utils.getHintText)(this.props),
            isOnlyResult: (0, _utils.getIsOnlyResult)(this.props)
          })
        }, _react["default"].createElement(Typeahead, _extends({}, this.props, {
          onKeyDown: this._handleKeyDown
        })));
      }
    }]);

    return WrappedTypeahead;
  }(_react["default"].Component);

  return WrappedTypeahead;
}

var _default = contextContainer;
exports["default"] = _default;

/***/ }),

/***/ 671:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(52);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(80);
/* harmony import */ var react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_typeahead_css_Typeahead_css__WEBPACK_IMPORTED_MODULE_7__);






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var timeLine = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(timeLine, _Component);

  var _super = _createSuper(timeLine);

  function timeLine(props) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, timeLine);

    return _super.call(this, props);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(timeLine, [{
    key: "render",
    value: function render() {
      var bottomChange = this.props.data == true ? "timelineBotton" : "fixed-bottom";
      var iconChange = this.props.data == true ? " icon-serviceGreen" : " icon-serviceGrey";
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding display-sm ",
        style: {
          display: localStorage.page === "3" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 pt-2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "".concat(iconChange)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "mr-5",
        style: {
          color: "#102a73"
        }
      }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#102a73"
        }
      }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3",
        style: {
          color: "#8a8a8a"
        }
      }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "my-auto mx-auto NoPadding display-sm",
        style: {
          display: localStorage.page === "2" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 pt-2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-serviceBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3 mr-5",
        style: {
          color: "#102a73"
        }
      }, "3"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-3",
        style: {
          color: "#102a73"
        }
      }, "4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "mx-auto NoPadding display-large ".concat(bottomChange),
        style: {
          display: localStorage.page === "3" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "".concat(iconChange)
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-5 mr-5",
        style: {
          color: "#102a73"
        }
      }, "RUTA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#8a8a8a"
        }
      }, "SERVICIOS EXTRAS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        style: {
          color: "#8a8a8a"
        }
      }, "PRESUPUESTO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_6__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "pt-2 mx-auto NoPadding display-large",
        style: {
          display: localStorage.page === "2" ? "none" : "",
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine"
      }, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "MARITÍMO" ? "" : "none"
        },
        className: "icon-shipBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        style: {
          display: localStorage.transporte === "AÉREO" ? "" : "none"
        },
        className: "icon-airplainBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-worldBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea3"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-serviceBlue"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "linea4"
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("i", {
        className: "icon-moneyGrey"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "my-auto mx-auto timeLine "
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-5 mr-5",
        style: {
          color: "#102a73"
        }
      }, "RUTA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        className: "ml-4 mr-5",
        style: {
          color: "#102a73"
        }
      }, "SERVICIOS EXTRAS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", {
        style: {
          color: "#8a8a8a"
        }
      }, "PRESUPUESTO")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("div", {
        className: "linea2 mb-3"
      })));
    }
  }]);

  return timeLine;
}(react__WEBPACK_IMPORTED_MODULE_5__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (timeLine);

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ Beforeunload; });

// UNUSED EXPORTS: useBeforeunload

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(1);

// CONCATENATED MODULE: ./node_modules/use-isomorphic-layout-effect/dist/use-isomorphic-layout-effect.browser.esm.js


var index =  react["useLayoutEffect"] ;

/* harmony default export */ var use_isomorphic_layout_effect_browser_esm = (index);

// CONCATENATED MODULE: ./node_modules/use-latest/dist/use-latest.esm.js



var use_latest_esm_useLatest = function useLatest(value) {
  var ref = Object(react["useRef"])(value);
  use_isomorphic_layout_effect_browser_esm(function () {
    ref.current = value;
  });
  return ref;
};

/* harmony default export */ var use_latest_esm = (use_latest_esm_useLatest);

// CONCATENATED MODULE: ./node_modules/react-beforeunload/lib/index.esm.js




var index_esm_useBeforeunload = function useBeforeunload(handler) {
  if (false) {}

  var handlerRef = use_latest_esm(handler);
  Object(react["useEffect"])(function () {
    var handleBeforeunload = function handleBeforeunload(event) {
      var returnValue;

      if (handlerRef.current != null) {
        returnValue = handlerRef.current(event);
      } // Chrome requires `returnValue` to be set.


      if (event.defaultPrevented) {
        event.returnValue = '';
      }

      if (typeof returnValue === 'string') {
        event.returnValue = returnValue;
        return returnValue;
      }
    };

    window.addEventListener('beforeunload', handleBeforeunload);
    return function () {
      window.removeEventListener('beforeunload', handleBeforeunload);
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
};

var Beforeunload = function Beforeunload(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      onBeforeunload = _ref.onBeforeunload;
  index_esm_useBeforeunload(onBeforeunload);
  return children;
};

if (false) {}


//# sourceMappingURL=index.esm.js.map


/***/ })

}]);
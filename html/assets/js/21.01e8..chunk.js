(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[21],{

/***/ 1503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(24);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(30);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(29);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(10);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(31);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(32);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(23);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3);
/* harmony import */ var Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(52);








function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var comment = /*#__PURE__*/function (_Component) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(comment, _Component);

  var _super = _createSuper(comment);

  function comment(props) {
    var _this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, comment);

    _this = _super.call(this, props);
    _this.SelectedOnceImportant = _this.SelectedOnceImportant.bind(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3___default()(_this));
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(comment, [{
    key: "SelectedOnceImportant",
    value: function SelectedOnceImportant() {
      sessionStorage.ImportantNone = "1";
    }
  }, {
    key: "render",
    value: function render() {
      var _React$createElement, _React$createElement2;

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Modal */ "D"], {
        isOpen: this.props.active,
        toggle: this.props.toggle,
        backdrop: true,
        style: {
          borderRadius: "2.1rem",
          maxWidth: "27em !important"
        },
        className: "fontAtentionStyle"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ModalBody */ "E"], {
        className: "NoPadding",
        style: {
          padding: "0rem"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        onClick: this.props.toggle,
        className: "plusLump  simple-icon-close ",
        style: {
          fontSize: "2em",
          position: "absolute",
          right: "0.5em",
          top: "0.5em",
          cursor: "pointer"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: "arial mb-4 mt-3 mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        className: "color-white",
        style: {
          fontSize: "24px",
          fontWeight: "bold"
        }
      }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroupItem */ "C"], {
        className: "color-white",
        style: {
          textAlign: "center",
          backgroundColor: "#0d5084"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "4",
        md: "3",
        className: "arial mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        className: "icon-atencion"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "8",
        md: "9",
        className: "arial mx-auto"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("h2", null, "IMPORTANTE | \xBFC\xF3mo interpretar mi oferta?")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        md: "12",
        lg: "4",
        sm: "12",
        className: " NoPaddingCost mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " my-auto mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        className: "icon-ICO-1"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " arial mb-2 mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroupItem */ "C"], {
        className: "listTittle"
      }, "Escenario m\xEDnimo, medio y m\xE1ximo"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " my-auto mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        style: {
          margin: "0.5em",
          borderRadius: "1em",
          padding: "0.5em",
          height: "8em"
        }
      }, "\xA0\xA0La", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          color: "red"
        }
      }, " ", "\xA0UNICA DIFERENCIA \xA0"), "entre un escenario y otro, es el valor de los", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold",
          color: "red"
        }
      }, " ", "\xA0IMPUESTOS DE ADUANA", " "), ". ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, " Nota:"), " El Servicio Logistico se mantiene igual en los 3 escenarios."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        md: "12",
        lg: "4",
        sm: "12",
        className: " NoPaddingCost mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " arial mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        className: "icon-ICO-2 "
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " arial mb-2 mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroupItem */ "C"], {
        className: "listTittle"
      }, "\xBFDe que depende un Escenario M\xCDNIMO o M\xC1XIMO?"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " my-auto mx-auto",
        style: {
          textAlign: "justify"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        style: {
          margin: "0.5em",
          borderRadius: "1em",
          padding: "0.5em",
          height: "8em"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", null, "\xA0\xA0Depender\xE1 de los siguientes detalles de su producto:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("ul", {
        style: {
          fontWeight: "bold"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, "Descripci\xF3n"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, "Composici\xF3n"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, "Uso final")), " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        md: "12",
        lg: "4",
        sm: "12",
        className: "NoPaddingCost mx-auto",
        style: {
          textAlign: "center",
          display: localStorage.paisDestino == "PERU" ? "" : "none"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " arial mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("i", {
        className: "icon-Sunat "
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " arial mb-2 mx-auto",
        style: {
          textAlign: "center"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroup */ "B"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* ListGroupItem */ "C"], {
        className: "listTittle"
      }, "Los Impuestos fueron calculados de la siguiente mandera:"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], {
        xxs: "12",
        md: "12",
        sm: "12",
        className: " my-auto mx-auto",
        style: {
          textAlign: "justify"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("div", {
        style: {
          margin: "0.5em",
          borderRadius: "1em",
          padding: "0.5em"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Table */ "O"], {
        style: {
          fontSize: "0.7em",
          textAlign: "center"
        },
        striped: true
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("thead", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", null, "CONCEPTO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", null, "ESCENARIO M\xCDNIMO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", null, "ESCENARIO MEDIO"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", null, "ESCENARIO M\xC1XIMO"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tbody", {
        style: {
          fontSize: "0.6em"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", {
        scope: "row"
      }, "IGV"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "16%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "16%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "16%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", {
        scope: "row"
      }, "IPM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "2%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "2%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "2%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", {
        scope: "row"
      }, "PERCEPCI\xD3N"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, localStorage.value4 == "false" ? "10%" : "3,50%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, localStorage.value4 == "false" ? "10%" : "3,50%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, localStorage.value4 == "false" ? "10%" : "3,50%")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("tr", {
        style: {
          fontSize: "0.7rem",
          color: "red"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("th", {
        scope: "row"
      }, "AD VALOREM"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "0%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "6%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("td", null, "11%")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("ul", {
        className: "pt-1",
        style: {
          fontSize: "0.75rem",
          paddingInlineStart: "0px",
          listStyle: "none"
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota 1"), ": La percepci\xF3n aumenta a 10% cuando es la primera importanci\xF3n."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota 2"), ": Algunos productos, como zapatillas, vehiculos o telas tienen un c\xE1lculo diferente de impuestos dependiendo de su clasificaci\xF3n."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("li", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, "Nota 3"), ": Los valores son referenciales para la toma de decisiones r\xE1pidas.")), " "))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Row */ "L"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], (_React$createElement = {
        md: "12"
      }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "md", "12"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement, "lg", "6"), _React$createElement), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* CustomInput */ "o"], {
        id: "notShow",
        type: "checkbox",
        onClick: this.SelectedOnceImportant,
        label: "No volver a mostrar este mensaje"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(Components_CustomBootstrap__WEBPACK_IMPORTED_MODULE_9__[/* Colxx */ "a"], (_React$createElement2 = {
        md: "12"
      }, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement2, "md", "12"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement2, "lg", "6"), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_React$createElement2, "style", {
        cursor: "pointer"
      }), _React$createElement2), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__[/* Button */ "c"], {
        color: "primary",
        className: "mb-2 mr-2 float-right btn-style",
        onClick: this.props.toggle
      }, "IR A PRESUPUESTO"))))));
    }
  }]);

  return comment;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (comment);

/***/ })

}]);
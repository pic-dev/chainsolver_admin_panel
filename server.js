//server.js
const express = require("express");
const path = require("path");
const host = "172.17.0.2";
const shrinkRay = require('shrink-ray-current');
//const compression = require("compression");
const localhost = 'localhost';

const port = process.env.PORT || 5000;
const app = express();
const http = require('http');

app.use(shrinkRay());
//app.use(compression());

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "build")));

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});


var httpServer = http.createServer(app);
httpServer.listen(port, localhost);




"use strict";
const path = require("path");
const fs = require("fs");

const publicPath = "/";
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    index: ["./src/index.js"],
  },
  output: {
    path: resolveApp("build"),
    filename: "assets/js/[name].[hash:4]..js",
    chunkFilename: "assets/js/[name].[hash:4]..chunk.js",
    publicPath: publicPath,
    // hotUpdateChunkFilename: 'hot/hot-update.js',
    // hotUpdateMainFilename: 'hot/hot-update.json'
  },

  resolve: {
    alias: {
      Components: path.resolve(__dirname, "../src/components/"),
      Assets: path.resolve(__dirname, "../src/assets/"),
      Util: path.resolve(__dirname, "../src/util/"),
      Routes: path.resolve(__dirname, "../src/routes/"),
      Hooks: path.resolve(__dirname, "../src/hooks/"),
      Services: path.resolve(__dirname, "../src/services/"),
      Context: path.resolve(__dirname, "../src/context/"),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-url-loader",
            options: {
              limit: 10000,
            },
          },
        ],
      },
      {
        test: /\.csv$/,
        loader: 'csv-loader',
        options: {
          dynamicTyping: true,
          header: true,
          skipEmptyLines: true
        }
      },
      {
        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        loader: "url-loader?limit=100000",
      },
    ],
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve("public/index.html"),
      favicon: "./public/manifest/favicon.ico",
      title: "PIC",
      meta: {
        viewport: "width=device-width, initial-scale=1.0",
        charset: "UTF-8",
      },
    }),

    new CopyWebpackPlugin([
      { from: "src/assets/img", to: "assets/img" },
      { from: "src/assets/fonts", to: "assets/fonts" },
      { from: "public/manifest", to: "manifest" },
      { from: "node_modules/pdfjs-dist/cmaps/", to: "cmaps/" },
      {
        from: "src/OneSignalSDKUpdaterWorker.js",
        to: "./OneSignalSDKUpdaterWorker.js",
      },
      { from: "src/OneSignalSDKWorker.js", to: "./OneSignalSDKWorker.js" },
      { from: "src/sw.js", to: "./sw.js" },
    ]),
  ],
};

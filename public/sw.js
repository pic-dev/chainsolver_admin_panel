importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js");

firebase.initializeApp({
        messagingSenderId: "1034730132272",
});

const messaging = firebase.messaging();

messaging.usePublicVapidKey( "BEkCcughSP9wENEzUP8hWoQJ35aSV5ZDqyxBRloa617Lr-Dg6BHa90SFJdxKl59YSXCvJb--4vB5645V3Fy-1_I")

messaging.setBackgroundMessageHandler(function(payload) {
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true
    })
    .then(windowClients => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        windowClient.postMessage(payload);
      }
    })
    .then(() => {
      return registration.showNotification("my notification title");
    });
  return promiseChain;
});

self.addEventListener('notificationclick', function(event) {
 console.log(event);
});

let version = 'VERSION'

workbox.setConfig({ debug: SW_DEBUG })
workbox.skipWaiting()
workbox.clientsClaim()

// for precache we dont want to set versioning since every new version will start with empty cache
// workbox already has a mechanism to update resources in precache based on their revision
// this way only the resources that changed since last build will be replaced in cache
workbox.core.setCacheNameDetails({ prefix: 'hn-pwa', precache: 'precache', runtime: `runtime-v2.${version}` })

// Application
workbox.routing.registerRoute(/^\/images\/.*/, workbox.strategies.cacheFirst())
workbox.routing.registerRoute(
  /\/api\/.*/,
  workbox.strategies.networkFirst({
    cacheName: `hn-pwa-api-v2.${version}`,
  })
)

// Precaching
workbox.precaching.precache([
  {
    url: '/',
    revision: version,
  },
])
workbox.precaching.precacheAndRoute(self.__WB_MANIFEST || [])

// cleanup old caches except precache
self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys().then(cacheKeys => {
      const oldKeys = cacheKeys.filter(key => key.indexOf('hn-pwa-precache') === -1 && key.indexOf(version) === -1)
      const deletePromises = oldKeys.map(oldKey => caches.delete(oldKey))
      return Promise.all(deletePromises)
    })
  )
})
